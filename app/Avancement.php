<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avancement extends Model
{
    protected $table="programmes_avancements";

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function corpsetat()
    {
        return $this->belongsTo('App\ProgrammeCorpsetat','corpsetat_id');
    }

    public function lot()
    {
        return $this->belongsTo('App\ProgrammeLot','lot_id');
    }

    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
