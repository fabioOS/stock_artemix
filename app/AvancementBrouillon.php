<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvancementBrouillon extends Model
{
    protected $table="programmes_avancements_brouillons";

    public function lot()
    {
        return $this->belongsTo('App\ProgrammeLot','lot_id');
    }

    public function corpsetat()
    {
        return $this->belongsTo('App\ProgrammeCorpsetat','corpsetat_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

}
