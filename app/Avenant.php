<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Avenant extends Model
{
    use SoftDeletes;
    protected $table = 'programmes_avenants';

    public function programme()
    {
        return $this->belongsTo('App\Programme');
    }

    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant');
    }

    public function contrat()
    {
        return $this->belongsTo('App\ProgrammeSoustraitantcontrat','soustraitantcontrat_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }




}
