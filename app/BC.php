<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BC extends Model
{
    use SoftDeletes;
    protected $table="bon_commandes";
    protected $appends = ['full_montant'];

    public function items()
    {
        return $this->hasMany('App\BCitem','bon_commande_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function demande()
    {
        return $this->belongsTo('App\Commande','commande_id');
    }

    public function fournisseur()
    {
        return $this->belongsTo('App\ProgrammeFournisseur','fournisseur_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function bonreception()
    {
        return $this->hasOne('App\StockFournisseur','bc_id');
    }

    public function factureFournisseur()
    {
        return $this->hasMany('App\FactureFournisseur','bc_id');
    }


    // FAIRE UNE FONCTION QUI CALCULE LE MONTANT TTC
    public function getMontantTTC()
    {
        if($this->tva == "AVEC"){
            $mtt = ($this->montant *0.18) + $this->montant;
        }elseif($this->retenu){
            $mtt = $this->montant - $this->retenu;
        }else{
            $mtt = $this->montant;
        }
        return $mtt;
    }

    public function getFullMontantAttribute()
    {
        $mttc = $this->getMontantTTC();
        return $mttc;
    }
}
