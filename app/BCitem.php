<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BCitem extends Model
{
    use SoftDeletes;
    protected $table="bon_commandes_items";

    public function bc()
    {
        return $this->belongsTo('App\BC','bon_commande_id');
    }
}
