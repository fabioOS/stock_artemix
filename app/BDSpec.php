<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BDSpec extends Model
{
    use SoftDeletes;
    protected $table="bon_demandes_speciales";

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function transaction(){
        return $this->hasMany('App\BDSpecTrans','demande_id');
    }
}
