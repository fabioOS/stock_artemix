<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bon_demande extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'programme_id',
        'soustraitant_id',
        'user_id',
        'ref',
        'file',
        'status',
        'motif'
    ];

    /*
    * Relationship
    */
    public function items()
    {
        return $this->hasMany('App\Bon_demande_item');
    }
    public function bon_demanbde_items()
    {
        return $this->hasMany('App\Bon_demande_item');
    }
    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant', 'soustraitant_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function contrat()
    {
        return $this->belongsTo('App\ProgrammeSoustraitantcontrat', 'contrat_id');
    }
    public function programme()
    {
        return $this->belongsTo('App\Programme', 'programme_id');
    }
}
