<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bon_demande_historique extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bon_demande_id',
        'user_id',
        'action',
        'rapport'
    ];

    /*
    * Relationship
    */
    public function bon_demande()
    {
        return $this->belongsTo('App\Bon_demande', 'bon_demande_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
