<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bon_demande_item extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bon_demande_id',
        'soustraitant_id',
        'lot_id',
        'produit_id',
        'corpsetats_id',
        'scorpsetats_id',
        'qte'
    ];

    /*
    * Relationship
    */
    public function demande()
    {
        return $this->belongsTo('App\Bon_demande','bon_demande_id');
    }
    public function produit()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }
}
