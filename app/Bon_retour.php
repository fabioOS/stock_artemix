<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bon_retour extends Model
{

    public function items()
    {
        return $this->hasMany('App\Bon_retour_item');
    }

    public function demande()
    {
        return $this->belongsTo('App\Bon_demande', 'bon_id');
    }

    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant', 'soustraitant_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function contrat()
    {
        return $this->belongsTo('App\ProgrammeSoustraitantcontrat', 'contrat_id');
    }
    public function programme()
    {
        return $this->belongsTo('App\Programme', 'programme_id');
    }
}
