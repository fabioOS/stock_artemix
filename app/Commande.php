<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commande extends Model
{
    use SoftDeletes;
    protected $table="commandes";

    public function produits()
    {
        return $this->hasMany('App\CommandeItem','commande_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function programmesous()
    {
        return $this->belongsTo('App\ProgrammeSous','programmesous_id');
    }

    public function boncommande()
    {
        return $this->hasOne('App\BC','commande_id');
    }

    public function lignebudget() {
        return $this->belongsTo('App\ProgrammeLigneBudgetaire','ligne_id');
    }
}
