<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandeItem extends Model
{
    protected $table="commande_items";

    public function produit()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }
}
