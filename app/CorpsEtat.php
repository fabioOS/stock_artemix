<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CorpsEtat extends Model
{
    use SoftDeletes;
    protected $table = 'corps_etats';
}
