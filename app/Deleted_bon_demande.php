<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_bon_demande extends Model
{
    protected $table ='deleted_bon_demandes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'programme_id',
        'soustraitant_id',
        'user_id',
        'ref',
        'file',
        'status',
        'motif'
    ];

    /*
    * Relationship
    */
    public function items()
    {
        return $this->hasMany('App\Deleted_bon_demande_item');
    }
    public function bon_demanbde_items()
    {
        return $this->hasMany('App\Deleted_bon_demande_item');
    }
    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant', 'soustraitant_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
