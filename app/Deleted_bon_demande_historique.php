<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_bon_demande_historique extends Model
{
    protected $table ='deleted_bon_demande_historiques';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deleted_bon_demande_id',
        'user_id',
        'action',
        'rapport'
    ];

    /*
    * Relationship
    */
    public function bon_demande()
    {
        return $this->belongsTo('App\Deleted_bon_demande', 'deleted_bon_demande_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
