<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_bon_demande_item extends Model
{
    protected $table ='deleted_bon_demande_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deleted_bon_demande_id',
        'soustraitant_id',
        'lot_id',
        'produit_id',
        'qte'
    ];

    /*
    * Relationship
    */
    public function demande()
    {
        return $this->belongsTo('App\Deleted_bon_demande','deleted_bon_demande_id');
    }
    public function produit()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }
}
