<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class BcAllExport implements FromView, WithTitle
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view() : View
    {
        $data = $this->data;

        return view('service_achat.etats.bc.excel_all',compact('data'));
    }

    public function title(): string
    {
        return 'BON DE COMMANDE';
    }
}
