<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BcManyExport implements WithMultipleSheets
{
    use Exportable;

    public $dataexp;

    public function __construct($dataexp)
    {
        $this->dataexp = $dataexp;
    }

    public function sheets(): array
    {
        $sheets = [];
        $datas = $this->dataexp;
        $sheets[] = new BcAllExport($datas);
        
        foreach($datas as $data) {
            $sheets[] = new BcItemExport($data);
        }

        return $sheets;
    }
}
