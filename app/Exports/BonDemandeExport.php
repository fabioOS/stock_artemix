<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class BonDemandeExport implements FromView, WithTitle
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view() : View
    {
        $data = $this->data;

        return view('chef_projet.etats.demandes.excel', compact('data'));
    }

    public function title(): string
    {
        return 'ETAT DU BON DE DEMANDE '. $this->data->ref;;
    }
}
