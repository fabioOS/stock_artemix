<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BonDemandeManyExport implements WithMultipleSheets
{
    use Exportable;

    public $dataexp;

    public function __construct($dataexp)
    {
        $this->dataexp = $dataexp;
    }

    public function sheets(): array
    {
        $sheets = [];
        $datas = $this->dataexp;

        foreach($datas as $data) {
            $sheets[] = new BonDemandeExport($data);
        }

        return $sheets;
    }
}
