<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class BonSortieExport implements FromView, WithTitle
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view() : View
    {
        $data = $this->data;

        return view('magasinier.etats.bs.excel', compact('data'));
    }

    public function title(): string
    {
        return 'ETAT DU BON DE SORTIR '. $this->data->ref;;
    }
}
