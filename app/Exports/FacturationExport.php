<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class FacturationExport implements FromView, WithTitle
{
    public $data;
    public $typexport;

    public function __construct($data,$typexport)
    {
        $this->data = $data;
        $this->typexport = $typexport;
    }

    public function view() : View
    {
        $datas = $this->data;
        return view('_etats.excell_facturation',compact('datas'));
    }

    public function title(): string
    {
        return $this->typexport;
    }
}
