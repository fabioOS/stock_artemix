<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class FacturationItemExport implements FromView, WithTitle
{
    public $data;
    public $type;

    public function __construct($data,$type)
    {
        $this->data = $data;
        $this->type = $type;
    }

    public function view() : View
    {
        $data = $this->data;
        $type = $this->type;
        //dd($data,$type);
        return view('_etats.excell_facturation_item',compact('data','type'));
    }

    public function title(): string
    {
        $data = $this->data;
        $libel = $this->type == 'fournisseur' ? $data->fournisseur->nom : 'NA';
        return 'STATISTIQUE : ' . $libel;
    }
}
