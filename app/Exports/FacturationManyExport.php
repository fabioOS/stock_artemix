<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class FacturationManyExport implements WithMultipleSheets
{
    use Exportable;

    public $data;
    public $typexport;
    public $typmodel;

    public function __construct($data,$typexport,$typmodel)
    {
        $this->data = $data;
        $this->typexport = $typexport;
        $this->typmodel = $typmodel;
    }

    public function sheets(): array
    {
        $sheets = [];
        $datas = $this->data;
        $sheets[] = new FacturationExport($datas,$this->typexport);

        if($this->typmodel == 'fournisseur') {
            $fournisseursUniques = $datas->unique('fournisseur_id');
            foreach($fournisseursUniques as $data) {
                $sheets[] = new FacturationItemExport($data,$this->typmodel);
            }
        }

        return $sheets;
    }
}
