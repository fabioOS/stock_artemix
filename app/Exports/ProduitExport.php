<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProduitExport implements FromView, WithTitle
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view() : View
    {
        $datas = $this->data;

        return view('service_achat.etats.produits.excel', compact('datas'));
    }

    public function title(): string
    {
        return 'ETAT DES PRODUITS';
    }
}
