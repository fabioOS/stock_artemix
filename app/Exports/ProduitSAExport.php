<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProduitSAExport implements FromView
{
    public $data;
    // public $dated;
    // public $datef;

    public function __construct($data)
    {
        $this->data = $data;
        // $this->dated = $date1;
        // $this->datef = $date2;
    }

    public function view() : View
    {
        $datas = $this->data;
        // $date = $this->dated;
        // $date2 = $this->datef;

        return view('service_achat.produits.model_excel', compact('datas'));
    }
}
