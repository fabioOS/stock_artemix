<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class RotationExport implements FromView, WithTitle
{
    public $data;
    public $day;

    public function __construct($data,$day)
    {
        $this->data = $data;
        $this->day = $day;
    }

    public function view() : View
    {
        $datas = $this->data;

        return view('_rotation_excel', compact('datas'));
    }

    public function title(): string
    {
        return 'MATIERE NON UTILISE DEPUIS '. $this->day.' JOURS';
    }
}
