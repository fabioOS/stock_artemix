<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;

class TestExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    protected $results;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $this->results = $this->getActionItems();
        return $this->results;
    }

    public function headings(): array
    {
        $columns = [
            'Column 1',
            'Column 2',
            'Column 3',
            'Column 4',
            'Column 5',
        ];
        return $columns;
    }

    private function getActionItems()
    {
        $select = 'id, name, email, actif';

        $query = \DB::table('users')->select(\DB::raw($select));

        $ai = $query->orderBy('id')->get();
        return $ai;
    }

    public function registerEvents(): array
    {
        return [
            // handle by a closure.
            AfterSheet::class => function(AfterSheet $event) {

                // get layout counts (add 1 to rows for heading row)
                $row_count = $this->results->count() + 1;
                $column_count = count($this->results[0]->toArray());

                // set dropdown column
                $drop_column = 'E';

                // set dropdown options
                $options = [
                    'option 1',
                    'option 2',
                    'option 3',
                ];

                // set dropdown list for first data row
                $validation = $event->sheet->getCell("{$drop_column}2")->getDataValidation();
                $validation->setType(DataValidation::TYPE_LIST );
                $validation->setErrorStyle(DataValidation::STYLE_INFORMATION );
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1(sprintf('"%s"',implode(',',$options)));

                // clone validation to remaining rows
                // for ($i = 3; $i <= $row_count; $i++) {
                //     $event->sheet->getCell("{$drop_column}{$i}")->setDataValidation(clone $validation);
                // }

                // // set columns to autosize
                // for ($i = 1; $i <= $column_count; $i++) {
                //     $column = Coordinate::stringFromColumnIndex($i);
                //     $event->sheet->getColumnDimension($column)->setAutoSize(true);
                // }
            },
        ];
    }
}
