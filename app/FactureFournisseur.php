<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FactureFournisseur extends Model
{
    use SoftDeletes;

    protected $table = 'facture_fournisseurs';

    public static function getRefFactFourns($programme_id)
    {
        $code = "FT-FSS".date('ym');
        $count = FactureFournisseur::where('programme_id',$programme_id)->withTrashed()->count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$programme_id.'-'.$num;

        return $val;
    }

    public function getLibelleStatut()
    {
        $statut = $this->status;
        if($statut == 0){
            return '<span class="badge badge-pill badge-soft-warning">En attente</span>';
        }elseif($statut == 1){
            return '<span class="badge badge-pill badge-soft-warning">En attente</span>';
        }elseif($statut == 2){
            return '<span class="badge badge-pill badge-soft-success">Payée</span>';
        }elseif($statut == 3){
            return '<span class="badge badge-pill badge-soft-danger">Rejetée</span>';
        }
    }

    public function getLibelleStatutEntite()
    {
        $statut = $this->status;
        if($statut == 0){
            return 'En attente';
        }elseif($statut == 1){
            return 'En attente';
        }elseif($statut == 2){
            return 'Payée';
        }elseif($statut == 3){
            return 'Rejetée';
        }
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function fournisseur()
    {
        return $this->belongsTo('App\ProgrammeFournisseur','fournisseur_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function boncommande()
    {
        return $this->belongsTo('App\BC','bc_id');
    }
}
