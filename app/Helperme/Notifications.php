<?php
/**
 * Created by PhpStorm.
 * User: fabienOS
 * Date: 11/09/2020
 * Time: 14:04
 */

namespace App\Helperme;


use App\Bon_demande;
use App\ProgrammeSoustraitant;
use App\ProgrammeSoustraitantcontrat;
use App\Transactiontest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Notifications
{
    public static function CountNotifsWainting()
    {
        $idprogram = Session::get('program');
        Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;
        $nbNotif = Bon_demande::where('programme_id', $idprogram)->where('nivalid',$niv)->where('status', 1)->count();

        return $nbNotif;
    }

    public static function CountStraitantWainting()
    {
        $idprogram = Session::get('program');
        Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;

        // $nbNotif = ProgrammeSoustraitant::where('programme_id', $idprogram)->where('nivalid',$niv)->where('etat', 1)->count();
        $nbNotif = ProgrammeSoustraitantcontrat::with('lots')->with('straitant')->where('nivalid',$niv)->where('programme_id',$idprogram)->where('etat', 1)->count();

        return $nbNotif;
    }

    public static function CountTransacValid()
    {
        $idprogram = Session::get('program');
        Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;

        $nbNotif = Transactiontest::where('programme_id', $idprogram)->where('nivalid',$niv)->where('etat', 1)->count();

        return $nbNotif;
    }

    public static function CountTransacChefp()
    {
        $idprogram = Session::get('program');
        $nbNotif = Transactiontest::where('programme_id', $idprogram)->count();

        return $nbNotif;
    }
}
