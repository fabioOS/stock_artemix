<?php

namespace App\Http\Controllers\Admin;

use App\CorpsEtat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CorpsEtatsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $corpsetats = CorpsEtat::orderBy('libelle','asc')->get();
        return view('admin.corps_etats.index',compact('corpsetats'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
        ])->validate();

        //check if exist in db before insert
        $slug = Str::slug($request->nom);
        $corpsetat = CorpsEtat::where('slug', $slug)->first();
        if ($corpsetat) {
            return redirect()->route('gcorpetat')->with('error' ,'Le corps d\'etat existe déjà');
        }

        $corpsetat = new CorpsEtat();
        $corpsetat->slug = $slug;
        $corpsetat->libelle = Str::upper($request->nom);
        $corpsetat->save();

        return redirect()->route('gcorpetat')->with('success' ,'Le corps d\'etat a bien été ajouté');
    }

    public function update(Request $request){
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'slug' => ['required'],
        ])->validate();

        //verifier s'il existe un autre corps d'etat avec le meme slug avant de modifier
        $corpsetat = CorpsEtat::where('slug', $request->slug)->first();
        if ($corpsetat->slug != $request->slug) {
            $slug = Str::slug($request->libelle);
            $corpsetat = CorpsEtat::where('slug', $slug)->first();
            if ($corpsetat) {
                return redirect()->route('gcorpetat')->with('error' ,'Le corps d\'etat existe déjà');
            }
        }
        $corpsetat->slug = Str::slug($request->libelle);
        $corpsetat->libelle = Str::upper($request->libelle);
        $corpsetat->save();

        return redirect()->route('gcorpetat')->with('success' ,'Le corps d\'etat a bien été modifié');

    }
}
