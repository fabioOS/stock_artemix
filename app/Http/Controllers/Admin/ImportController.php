<?php

namespace App\Http\Controllers\Admin;

use App\Avancementest;
use App\Bon_demande_item;
use App\Programme;
use App\ProgrammeCorpsetat;
use App\ProgrammeLot;
use App\Transaction;
use App\Transactiontest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Excel;
use Illuminate\Support\Str;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $programmes = Programme::get();
        return view('admin.imports.index',compact('programmes'));
    }

    public function store(Request $request)
    {
        dd($request->all());
        Validator::make($request->all(), [
            'id_type_lot' => ['required'],
            'userfile' => ['required'],
        ])->validate();

        $program = Programme::where('id',$request->id_type_lot)->first();
        if(isset($program->id)){
            foreach ($request->userfile as $fil){
                try {
                    //dd($fil);
                    $results = Excel::selectSheetsByIndex(0)->load($fil)->toArray();
                    //dd($results[0]);
                    $ttaux = 0;
                    $ttmt = 0;
                    $idtrans = 0;

                    if(count($results)>0){
                        $trans = new Transactiontest();
                        $trans->programme_id = 3;
                        $trans->soustraitant_id = 1;
                        $trans->lot_id = $results[0]['lots'];
                        $trans->taux = 0;
                        $trans->montant = 0;
                        $trans->save();
                        $idtrans = $trans->id ;

                        foreach($results as $value=>$key ){
                            //dd($key);
                            if($key['type'] == 1){
                                $check = Bon_demande_item::where('lot_id',$key['lots'])->where('corpsetats_id',$key['corps_id'])->where('scorpsetats_id',$key['scorps_id'])->where('produit_id',$key['produit_id'])->where('qte',$key['qte'])->first();

                                if(!$check){
                                    if($key['qte'] > 0){
                                        $save = new Bon_demande_item();
                                        $save->bon_demande_id = 1 ;
                                        $save->soustraitant_id = 1 ;
                                        $save->lot_id = $key['lots'] ?: 0 ;
                                        $save->corpsetats_id = $key['corps_id'] ?: 0 ;
                                        $save->scorpsetats_id = $key['scorps_id'] ?: 0 ;
                                        $save->produit_id = $key['produit_id'] ?: 0 ;
                                        $save->qte = $key['qte'] ?: 0 ;
                                        $save->save();
                                    }
                                }

                            }else{
                                //corps d'etat save transaction

                                if(!empty($key['montant'])){
                                    //calcule de taux
                                    $cc = ProgrammeCorpsetat::where('id',$key['corps_id'])->first();
                                    if($cc){
                                        $total = $cc->prix ;
                                        $depense = $total - $key['montant'];
                                        $tau = ($depense * 100) / $total;

                                        $new = new Avancementest();
                                        $new->programme_id = $program->id;
                                        $new->lot_id = $key['lots'] ?: 0 ;
                                        $new->soustraitant_id = 1;
                                        $new->corpsetat_id = $key['corps_id'];
                                        $new->taux = $tau;
                                        $new->taux_execute = $tau;
                                        $new->cout = $depense;
                                        $new->transac_id = $idtrans;
                                        $new->save();

                                        $ttmt = $ttmt + $depense;
                                        $ttaux = $ttaux + $tau;
                                    }
                                }
                            }
                        }

                        $trans = Transactiontest::where('id',$idtrans)->first();
                        $trans->taux = $ttaux;
                        $trans->montant = $ttmt;
                        $trans->save();

                    }
                }catch (\Exception $e){
                    dd($e->getMessage());
                }

            }
            return redirect()->route('importe')->with('success' ,"L'importation a bien été importer");

            //return redirect()->back();
        }

        return redirect()->back();
    }


    public function export(Request $request)
    {
        dd($request->all());
        Validator::make($request->all(), [
            'id_type_lot' => ['required'],
            'id_programme' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->id_programme)->first();
        $lot = ProgrammeLot::where('id',$request->id_type_lot)->first();

        if(isset($program->id) and isset($lot->id)){
            $customerArray[] = array('LOTS','PROGRAMME','LIBELLE','PRODUIT_ID','SCORPS_ID','CORPS_ID','QTE','TYPE','MONTANT');
            $corpsetats = ProgrammeCorpsetat::where('programme_id',$program->id)->where('actif_id',$lot->actif_id)->get();

            foreach ($corpsetats as $corps){
                $scorps = \App\ProgrammeSouscorpsetat::where([['corpsetat_id',$corps->id],['actif_id',$corps->actif_id]])->with('matieres')->get();
                $customerArray[]= array(
                    'LOTS'=> $lot->id,
                    'PROGRAMME'=> $program->id,
                    'LIBELLE'=> Str::upper($corps->libelle),
                    'PRODUIT_ID'=> '',
                    'SCORPS_ID'=> '',
                    'CORPS_ID'=> $corps->id,
                    'QTE'=> '',
                    'TYPE'=> 2,
                    'MONTANT'=> ''
                );

                foreach($scorps as $scorp){
                    $customerArray[]= array(
                        'LOTS'=> $lot->id,
                        'PROGRAMME'=> $program->id,
                        'LIBELLE'=> $scorp->matieres->libelle,
                        'PRODUIT_ID'=> $scorp->matieres->id,
                        'SCORPS_ID'=> $scorp->id,
                        'CORPS_ID'=> $corps->id,
                        'QTE'=> '',
                        'TYPE'=> 1,
                        'MONTANT'=> ''
                    );
                }

            }
            //dd($customerArray);
            try {
                $slug = $lot->lot;

                Excel::create("V-".$slug,function($excel) use($customerArray,$slug){
                    $excel->setTitle("V-".$slug);
                    $excel->sheet("V-".$slug,function ($sheet) use ($customerArray){
                        $sheet->fromArray($customerArray,null,'A1',false,false);
                    });

                })->download('xls');
            } catch (\Exception $e){
                dd($e->getMessage());
            }


            //dd($program,$lot,$corpsetats);

        }

        return redirect()->back();
    }
}
