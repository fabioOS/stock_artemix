<?php

namespace App\Http\Controllers\Admin;

use App\Produit;
use App\Typeproduit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\ProduitSAImport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ProduitsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->look == 1){
            return redirect()->route('home')->with('error','Vous n\'avez pas accès à cette page');
        }

        $produits =Produit::with('type')->get();
        $types = Typeproduit::get();
        return view('admin.produits.index',compact('produits','types'));
    }

    public function create()
    {

    }

    public function edit()
    {

    }

    public function destroy()
    {

    }

    public function store(Request $request)
    {
        if(Auth::user()->look == 1){
            return redirect()->route('home')->with('error','Vous n\'avez pas accès à cette page');
        }
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
            'code' => ['required'],
            'type' => ['required'],
        ])->validate();

        $slug = Str::slug($request->nom);
        $check = Produit::where('slug',$slug)->first();

        if(isset($check->id)){
            return redirect()->back()->with('error','La matière existe déjà');
        }

        $type = new Produit();
        $type->slug = $slug;
        $type->code = $request->code;
        $type->libelle = $request->nom;
        $type->seuil = $request->seuil ?? 10;
        $type->type_id = $request->type;
        $type->save();

        return redirect()->route('gproduits')->with('success','La matière a bien été ajouté');

    }

    public function update(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
            'type' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $slug = Str::slug($request->nom);
        $check = Produit::where('slug',$slug)->first();

        $type = Produit::where('slug',$request->slug)->first();

        $checktwo = Produit::where('id',"<>",$type?$type->id:0)->where('code',$request->code)->first();
        if(isset($checktwo->id)){
            return redirect()->back()->with('error','Le code saisie exite déjà');
        }

        if(isset($check->id)){
            $type->type_id = $request->type;
        }else{
            $type->slug = $slug;
            $type->libelle = $request->nom;
            $type->type_id = $request->type;
        }
        $type->code = $request->code;
        // $type->seuil = $request->seuil;
        $type->save();

        return redirect()->route('gproduits')->with('success','La matière a bien été modifié');
    }

    public function import(Request $request)
    {
        $this->getProgramExist();
        $programId = $this->getIdProgram();
        //dd($request->all());

        Validator::make($request->all(), [
            'ufile' => ['required','mimes:xlsx,xls,csv'],
        ])->validate();

        if(isset($programId)){
            $file = $request->file('ufile');
            $filedata = Excel::toCollection(new ProduitSAImport, $request->file('ufile'));
            $filedata = $filedata[0]->toArray();
            if(count($filedata) > 0) {
                foreach($filedata as $key => $row) {
                    if($key == 0 ) {
                        //dd($row);
                        if($row[0] != 'PRODUIT' || $row[1] != "CODE" || $row[2] != "TYPE" ) {
                            return redirect()->back()->with('error' ,"le format de fichier n'est pas conforme au modèle de base.");
                        }
                    }else{
                        $slug = Str::slug($row[0]);
                        $check = Produit::where('slug',$slug)->first();
                        if(isset($check->id)){
                            return redirect()->back()->with('error','La matière existe déjà');
                        }

                        $check = Produit::where('code',$row[1])->first();
                        if(isset($check->id)){
                            return redirect()->back()->with('error','La matière existe déjà');
                        }

                        $type = new Produit();
                        $type->slug = $slug;
                        $type->code = $row[1];
                        $type->libelle = $row[0];
                        $type->seuil = 10;
                        $type->type_id = $row[2];
                        $type->save();

                    }
                }

                return redirect()->route('sa.gproduits')->with('success' ,"Le fichier a bien été importé.");

            }else {
                return redirect()->route('sa.gproduits')->with('error' ,"Veuillez importer le fichier d'origine.");
            }

            // if(count($results)>0){
            //     foreach($results as $value=>$key ){
            //         $check = Produit::where('code',$key['code'])->first();

            //         if(isset($check->id)){
            //             $idpdt = $check->id;
            //             $check2 = ProgrammeProduit::where('programme_id',$programId)->where('produit_id',$idpdt)->first();

            //             if(!isset($check2->id)){
            //                 $type = new ProgrammeProduit();
            //                 $type->produit_id = $idpdt;
            //                 $type->programme_id = $programId;
            //                 $type->prix = $key['prix'];
            //                 $type->save();
            //             }

            //         }
            //     }
            // }
        }

    }

    //TYPES //TYPES //TYPES //TYPES

    public function types()
    {
        if(Auth::user()->look == 1){
            return redirect()->route('home')->with('error','Vous n\'avez pas accès à cette page');
        }

        $types = Typeproduit::get();
        return view('admin.produits.type',compact('types'));
    }

    public function gettype($id)
    {
        $type = Produit::where('slug',$id)->has('type')->with('type')->first();
        return $type ? $type->type->libelle : '-' ;
    }

    public function storeType(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
        ])->validate();

        $slug = Str::slug($request->nom);
        $check = Typeproduit::where('slug',$slug)->first();
        if(isset($check->id)){
            return redirect()->back()->with('error','Le type existe déjà');
        }
        $type = new Typeproduit();
        $type->slug = $slug;
        $type->libelle = $request->nom;
        $type->save();

        return redirect()->route('gproduits.types')->with('success','Le type a bien été ajouté');
    }

    public function updateType(Request $request)
    {
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $slug = Str::slug($request->libelle);
        $check = Typeproduit::where('slug',$slug)->first();
        if(isset($check->id)){
            return redirect()->back()->with('error','Le type existe déjà');
        }
        $type = Typeproduit::where('slug',$request->slug)->first();
        $type->slug = $slug;
        $type->libelle = $request->libelle;
        $type->save();

        return redirect()->route('gproduits.types')->with('success','Le type a bien été modifié');
    }
}
