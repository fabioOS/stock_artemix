<?php

namespace App\Http\Controllers\Admin;

use App\CorpsEtat;
use App\Programme;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProgrammesController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $programmes = Programme::orderBy('id','desc')->get();
        return view('admin.programmes.index',compact('programmes'));
    }

    public function store(Request $request)
    {
        //dd($request->all());

        Validator::make($request->all(), [
            'nom' => ['required'],
            'localisation' => ['required'],
        ])->validate();

        $program = new Programme();
        $program->slug = Str::slug($request->nom);
        $program->libelle = $request->nom;
        $program->nom = $request->nom;
        $program->localisation = $request->localisation;
        $program->description = $request->description;

        if($request->file('userfile')){
            $file = $request->file('userfile');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'assets/uploads/programmes/';
            $picture = Str::random(6).'.'. $extension;
            if (!empty($program->img)) {
                unlink($folderName.$program->img);
            }
            $file->move($folderName,$picture);
            $program->img = $picture;
        }

        $program->save();

        return redirect()->route('gprogramme')->with('success' ,'Le programme a bien été ajouté');
    }

    public function edit($slug)
    {
        $programe = Programme::where('slug',$slug)->first();
        return $programe ;
    }

    public function update(Request $request)
    {
        //dd($request->all());

        Validator::make($request->all(), [
            'nom' => ['required'],
            'localisation' => ['required'],
            'idslug'=>['required']
        ])->validate();

        $program = Programme::where('slug',$request->idslug)->first();
        if(!isset($program->id)){
            return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
        }
        $program->slug = Str::slug($request->nom);
        $program->libelle = $request->nom;
        $program->nom = $request->nom;
        $program->localisation = $request->localisation;
        $program->description = $request->description;

        if($request->file('userfile')){
            $file = $request->file('userfile');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'assets/uploads/programmes/';
            $picture = Str::random(6).'.'. $extension;
            if ($program->img && file_exists($folderName.$program->img)) {
                unlink($folderName.$program->img);
            }
            $file->move($folderName,$picture);
            $program->img = $picture;
        }

        $program->save();

        return redirect()->route('gprogramme')->with('success' ,'Le programme a bien été modifié');
    }


    public function destroy($id)
    {
        if($id){
            $program = Programme::where('slug',$id)->first();
            $program->delete();
            return redirect()->route('gprogramme')->with('success','Le programme a bien été supprimé.');
        }

        return redirect()->back();
    }


    public function show($slug)
    {
        $program = Programme::where('slug',$slug)->with(['users'=>function($e) use($slug){
            $e->with(['programmPivot'=>function($q) use($slug){
                $idpg = Programme::where('slug',$slug)->first()->id;
                $q->where('programme_id',$idpg);
            }]);
        }
        ])->with('actifs')->with('lots')->with('sousprogramme')->with('ligneBudgetaires')->first();
        //dd($program->users->sortBy('role_id'));
        if(isset($program->id)){
            $idsusers = $program->users->pluck('id')->toArray();
            $users = User::whereNotIn('id',$idsusers)->where('role_id','<>',1)->with('role')->has('role')->get();

            return view('admin.programmes.show',compact('program','users'));
        }

        return redirect()->back();
    }
}
