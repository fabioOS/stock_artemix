<?php

namespace App\Http\Controllers\Admin;

use App\Avancement;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\CorpsEtat;
use App\Produit;
use App\Programme;
use App\ProgrammeActif;
use App\ProgrammeCorpsetat;
use App\ProgrammeCorpsetatLibelle;
use App\ProgrammeLot;
use App\ProgrammePivotUser;
use App\ProgrammeSouscorpsetat;
use App\ProgrammeSouscorpsetatLibelle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\LotActifImport;
use App\ProgrammeLigneBudgetaire;
use App\ProgrammeSous;
use App\ProgrammeSouscorpsetatLot;
use App\Transaction;
use Illuminate\Support\Facades\Validator;
use Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel as FacadesExcel;

class ProgrammesSettingController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    //UTILISATEUR
    public function storeUser(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'user' => ['required'],
            'id_programme' => ['required'],
            'secteur' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->id_programme)->first();
        if(isset($program->id)){
            foreach ($request->user as $user){
                $check = ProgrammePivotUser::where('programme_id',$program->id)->where('user_id',$user)->first();
                if(!$check){
                    $programUser = new ProgrammePivotUser();
                    $programUser->programme_id = $program->id;
                    $programUser->user_id = $user;
                    $programUser->save();

                    //save secteur user
                    foreach ($request->secteur as $sect) {
                        if($sect == 1){
                            $programUser->batiment = 1;
                        }else{
                            $programUser->vrd = 1;
                        }
                        $programUser->save();
                    }
                }
            }

            return redirect()->route('gprogramme.show',$request->id_programme)->with('success' ,'Les utilisateurs ont bien été ajoutés');
        }

        return redirect()->back();

    }

    public function delUser($user,$slug)
    {
        //dd($user,$slug);
        $check = ProgrammePivotUser::where('programme_id',$slug)->where('user_id',$user)->first();

        if($check){
            $check->delete();
            $program = Programme::find($slug);
            return redirect()->route('gprogramme.show',$program->slug)->with('success' ,'L\'utilisateur a bien été supprimé');
        }

        return redirect()->back();
    }

    //ACTIFS
    public function storeActifs(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle_type_lot' => ['required'],
            'id_programme' => ['required'],
            'secteur' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->id_programme)->first();
        //dd($program);
        if(isset($program->id)){
            $check = ProgrammeActif::where('programme_id',$program->id)->where('libelle',$request->libelle_type_lot)->first();
            if(!$check){
                $actif = new ProgrammeActif();
                $actif->programme_id = $program->id;
                $actif->libelle = $request->libelle_type_lot;
                $actif->type = $request->secteur;
                $actif->save();
            }

            return redirect()->route('gprogramme.show',$request->id_programme)->with('success' ,'L\'actif a bien été ajouté');
        }

        return redirect()->back();
    }

    public function updateActifs(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle_type_lot' => ['required'],
            'slug' => ['required'],
            'secteur' => ['required'],
        ])->validate();

        $actif = ProgrammeActif::where('id',$request->slug)->first();
        //dd($actif);
        if(isset($actif->id)){
            $program = Programme::where('id',$actif->programme_id)->first();
            //$actif->programme_id = $program->id;
            $actif->libelle = $request->libelle_type_lot;
            $actif->type = $request->secteur;
            $actif->save();

            return redirect()->route('gprogramme.show',$program->slug)->with('success' ,'L\'actif a bien été modifié');
        }

        return redirect()->back();
    }

    public function delActifs($slug)
    {
        $actif = ProgrammeActif::where('id',$slug)->first();
        //dd($actif);
        if(isset($actif->id)){
            $program = Programme::where('id',$actif->programme_id)->first();
            $actif->delete();

            return redirect()->route('gprogramme.show',$program->slug)->with('success' ,'L\'actif a bien été supprimé');
        }

        return redirect()->back();
    }

    //LOTS
    public function storeLots(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'id_type_lot' => ['required'],
            'userfile' => ['required'],
            'id_programme' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->id_programme)->first();
        if(isset($program->id)){
            $filedata = Excel::toCollection(new LotActifImport, $request->file('userfile'));
            $filedata = $filedata[0]->toArray();

            if(count($filedata) > 0) {
                //dd($filedata);
                foreach($filedata as $key => $row) {
                    if($key == 0 ) {
                        //dd($row);
                        if($row[0] != 'LOT' || $row[1] != "ILOT" || $row[2] != "TAILLE LOTS" || $row[3] != "FONCIER DE BASE M2" ||
                            $row[4] != "FONCIER SUPP M2" || $row[5] != "COUT DU FONCIER BRUT" || $row[6] != "COUT DU FONCIER BRUT/M2" || $row[7] != "HONORAIRES" ||
                            $row[8] != "COMMERCIALISATIONS" || $row[9] != "FRAIS GENERAUX" || $row[10] != "COUT UNITAIRE DU FONCIER" || $row[11] != "COUT DU FONCIER DE BASE" ||
                            $row[12] != "COUT TOTAL DU FONCIER" || $row[13] != "POURCENTAGE" || $row[14] != "COUT DU VRD" || $row[15] != "COUT DU REMBLAIS HYDRAULIQUE" ||
                            $row[16] != "BLOC" || $row[17] != "TRANCHE" || $row[18] != "COUT FONCIER SUPP"  )
                            {
                            return redirect()->back()->with('error' ,"le format de fichier n'est pas conforme au modèle de base.");
                            }
                    }else{
                        $check = ProgrammeLot::where('programme_id',$program->id)->where('lot',$row[0])->first();
                        if(!isset($check)){
                            //return redirect()->back()->with('error','Cet lot existe déjà');
                            $lot = new ProgrammeLot();
                            $lot->programme_id = $program->id;
                            $lot->actif_id = $request->id_type_lot;
                            $lot->lot = $row[0] ?: null;
                            $lot->ilot = $row[1] ?: null;
                            $lot->taille_lots = $row[2] ?: null;
                            $lot->foncier_de_base_m2 = $row[3] ?: null;
                            $lot->foncier_supp_m2 = $row[4] ?: null;
                            $lot->cout_du_foncier_brut = $row[5] ?: null;
                            $lot->cout_du_foncier_brutm2 = $row[6] ?: null;
                            $lot->honoraires = $row[7] ?: null;
                            $lot->commercialisations = $row[8] ?: null;
                            $lot->frais_generaux = $row[9] ?: null;
                            $lot->cout_unitaire_du_foncier = $row[10] ?: null;
                            $lot->cout_du_foncier_de_base = $row[11] ?: null;
                            $lot->cout_total_du_foncier = $row[12] ?: null;
                            $lot->pourcentage = $row[13] ?: null;
                            $lot->cout_du_vrd = $row[14] ?: null;
                            $lot->cout_du_remblais_hydraulique = $row[15] ?: null;
                            $lot->bloc = $row[16] ?: null;
                            $lot->tranche = $row[17] ?: null;
                            $lot->cout_foncier_supp = $row[18] ?: null;
                            $lot->save();
                        }


                    }
                }
                return redirect()->route('gprogramme.show',$request->id_programme)->with('success' ,'Les lots ont bien été ajoutés');
            }else {
                return redirect()->back()->with('error' ,"Veuillez importer le fichier d'origine.");
            }


            // $results = Excel::selectSheetsByIndex(0)->load($request->userfile)->toArray();

            // foreach($results as $value=>$key ){
            //     //dd($key);
            //     $check = ProgrammeLot::where('programme_id',$program->id)->where('actif_id',$request->id_type_lot)->where('lot',$key['lot'])->first();
            //     if(!$check){
            //         $lot = new ProgrammeLot();
            //         $lot->programme_id = $program->id;
            //         $lot->actif_id = $request->id_type_lot;
            //         $lot->lot = $key['lot'] ?: 0;
            //         $lot->ilot = $key['ilot'] ?: 0;
            //         $lot->taille_lots = $key['taille_lots'] ?: 0;
            //         $lot->foncier_de_base_m2 = $key['foncier_de_base_m2'] ?: 0;
            //         $lot->foncier_supp_m2 = $key['foncier_supp_m2'] ?: 0;
            //         $lot->cout_du_foncier_brut = $key['cout_du_foncier_brut'] ?: 0;
            //         $lot->cout_du_foncier_brutm2 = $key['cout_du_foncier_brutm2'] ?: 0;
            //         $lot->honoraires = $key['honoraires'] ?: 0;
            //         $lot->commercialisations = $key['commercialisations'] ?: 0;
            //         $lot->frais_generaux = $key['frais_generaux'] ?: 0;
            //         $lot->cout_unitaire_du_foncier = $key['cout_unitaire_du_foncier'] ?: 0;
            //         $lot->cout_du_foncier_de_base = $key['cout_du_foncier_de_base'] ?: 0;
            //         $lot->cout_total_du_foncier = $key['cout_total_du_foncier'] ?: 0;
            //         $lot->pourcentage = $key['pourcentage'] ?: 0;
            //         $lot->cout_du_vrd = $key['cout_du_vrd'] ?: 0;
            //         $lot->cout_du_remblais_hydraulique = $key['cout_du_remblais_hydraulique'] ?: 0;
            //         $lot->bloc = $key['bloc'] ?: 0;
            //         $lot->tranche = $key['tranche'] ?: 0;
            //         $lot->cout_foncier_supp = $key['cout_foncier_supp'] ?: 0;
            //         $lot->save();
            //     }
            // }
            // return redirect()->route('gprogramme.show',$request->id_programme)->with('success' ,'Les lots ont bien été ajoutés');
        }

        return redirect()->back();
    }


    //DQE
    public function createDqe($program,$actif)
    {
        //dd($program,$actif);
        $programme = Programme::where('slug',$program)->first();
        $actifs = ProgrammeActif::where('id',$actif)->first();
        $matieres = Produit::with('type')->get();
        //dd($programme,$actifs);
        if(isset($programme->id) and isset($actifs->id)){
            $corpsetats = ProgrammeCorpsetat::where('programme_id',$programme->id)->where('actif_id',$actifs->id)->get();
            $corpsetats->map(function($item){
                $item->lignebudget = ProgrammeLigneBudgetaire::where('id',$item->ligne_budget_id)->first();
                // $item->lignebudget = $item->with('lignebudget')->first();
                return $item;
            });
            //dd($corpsetats);
            $lignebg = ProgrammeLigneBudgetaire::where('programme_id',$programme->id)->orderBy('libelle','asc')->get();
            return view('admin.programmes.dqe',compact('programme','actifs','corpsetats','matieres','lignebg'));
        }

        return redirect()->back();
    }

    public function storeDqeCe(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'ligne' => ['required'],
            'corps_etat' => ['required'],
            'programme' => ['required'],
            'actif' => ['required'],
            'montant_corps_etat' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->programme)->first();
        if(isset($program->id)){
            $slugce = Str::slug($request->corps_etat);
            $check = ProgrammeCorpsetat::where('programme_id',$program->id)->where('actif_id',$request->actif)->where('slug',$slugce)->first();
            if(!$check){
                $ce = new ProgrammeCorpsetat();
                $ce->ligne_budget_id = $request->ligne;
                $ce->slug = $slugce;
                $ce->programme_id = $program->id;
                $ce->actif_id = $request->actif;
                $ce->libelle = Str::upper($request->corps_etat);
                $ce->prix = $request->montant_corps_etat * config('settingstock.taux');
                $ce->save();

                return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$request->actif])->with('success' ,'Le corps d\'etat a bien été ajouté');
            }
            return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$request->actif])->with('error' ,'Le corps d\'etat a déjà été ajouté.');

        }

        return redirect()->back();

    }

    public function updateDqeCe(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'ligne' => ['required'],
            'libelle' => ['required'],
            'idce' => ['required'],
            'montant_corps_etat' => ['required'],
        ])->validate();

        $check = ProgrammeCorpsetat::where('id',$request->idce)->first();
        //dd($check);
        if(isset($check->id)){
            $program = Programme::where('id',$check->programme_id)->first();
            $actif = ProgrammeActif::where('id',$check->actif_id)->first();

            //verifier s'il existe un autre corps d'etat avec le meme slug avant de modifier
            $slugce = Str::slug($request->libelle);
            if($check->slug != $slugce) {
                $check2 = ProgrammeCorpsetat::where('slug',$slugce)->where('actif_id',$check->actif_id)->where('programme_id',$check->programme_id)->first();
                if($check2) {
                    return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$actif->id])->with('error' ,'Le corps d\'etat existe déjà');
                }
            }

            $check->ligne_budget_id = $request->ligne;
            $check->slug = $slugce;
            $check->libelle = Str::upper($request->libelle);
            $check->prix = $request->montant_corps_etat;
            $check->save();

            return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$actif->id])->with('success' ,'Le corps d\'etat a bien été modifié');

        }

        return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
    }

    public function storeDqeMatiere(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'matiere' => ['required'],
            'quantite' => ['required'],
            'idcemat' => ['required'],
            'idlibel' => ['required'],
        ])->validate();

        $produit = Produit::where('slug',$request->matiere)->first();
        $ce = ProgrammeCorpsetat::where('id',$request->idcemat)->with('programme')->first();

        if(isset($produit->id) and isset($ce->id)){
            //check existant du produit
            $libdt = $request->idlibel == "0" ? null : $request->idlibel;
            $check = ProgrammeSouscorpsetat::where('programme_id',$ce->programme_id)->where('corpsetat_id',$ce->id)->where('actif_id',$ce->actif_id)->where('corpsetatlibel_id',$libdt)->where('produit_id',$produit->id)->first();
            if(!$check){
                $sce = new ProgrammeSouscorpsetat();
                $sce->programme_id = $ce->programme_id;
                $sce->actif_id = $ce->actif_id;
                $sce->corpsetat_id = $ce->id;
                $sce->produit_id = $produit->id;
                $sce->qte = $request->quantite;
                $sce->corpsetatlibel_id = $libdt;
                //$sce->prefix = $request->prefix;
                $sce->save();

                //Store DQE QTT BY LOT
                $this->saveDQEqttLot($request);

                return redirect()->route('gprogramme.setting.create.dqe',[$ce->programme->slug,$ce->actif_id])->with('success' ,'La matière a bien été ajoutée');
            }

            return redirect()->route('gprogramme.setting.create.dqe',[$ce->programme->slug,$ce->actif_id])->with('error' ,'La matière a déjà été ajoutée');

        }

        return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
    }

    public function saveDQEqttLot($request)
    {
        $produit = Produit::where('slug',$request->matiere)->first();
        $ce = ProgrammeCorpsetat::where('id',$request->idcemat)->with('programme')->first();
        //dd($ce);
        if($ce){
            $listlots = ProgrammeLot::where('programme_id',$ce->programme_id)->where('actif_id',$ce->actif_id)->get();
            //dd($listlots);
            foreach($listlots as $lot){
                $libdt = $request->idlibel == "0" ? null : $request->idlibel;
                $check = ProgrammeSouscorpsetatLot::where('programme_id',$ce->programme_id)->where('corpsetat_id',$ce->id)
                        ->where('actif_id',$ce->actif_id)->where('corpsetatlibel_id',$libdt)
                        ->where('produit_id',$produit->id)->where('lot_id',$lot->id)
                        ->first();

                if(!$check){
                    $scelot = new ProgrammeSouscorpsetatLot();
                    $scelot->programme_id = $ce->programme_id;
                    $scelot->actif_id = $ce->actif_id;
                    $scelot->corpsetat_id = $ce->id;
                    $scelot->produit_id = $produit->id;
                    $scelot->qte = $request->quantite;
                    $scelot->corpsetatlibel_id = $libdt;
                    $scelot->lot_id = $lot->id;
                    $scelot->save();
                }
            }
        }

    }

    public function updateDqeMatiere(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'matiere' => ['required'],
            'quantite' => ['required'],
            'idscorps' => ['required'],
            'idlibel' => ['required'],
        ])->validate();

        $produit = Produit::where('slug',$request->matiere)->first();
        $sce = ProgrammeSouscorpsetat::where('id',$request->idscorps)->with('programme')->first();
        //dd($sce);
        if(isset($produit->id) and isset($sce->id)){

            $check = ProgrammeSouscorpsetat::where('programme_id',$sce->programme_id)->where('corpsetat_id',$sce->corpsetat_id)->where('actif_id',$sce->actif_id)->where('corpsetatlibel_id',$sce->corpsetatlibel_id)->where('produit_id',$produit->id)
            ->where('qte',$request->quantite)->first();
            if(!$check){
                $sce->produit_id = $produit->id;
                $sce->qte = $request->quantite;
                //$sce->prefix = $request->prefix;

                //Delete DQE QTT BY LOT
                $this->updateDQEqttLot($request);

                $sce->save();
                return redirect()->route('gprogramme.setting.create.dqe',[$sce->programme->slug,$sce->actif_id])->with('success' ,'La matière a bien été modifiée');
            }
            return redirect()->route('gprogramme.setting.create.dqe',[$sce->programme->slug,$sce->actif_id])->with('error' ,'La matière exite déjà');

        }
        return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
    }

    public function updateDQEqttLot($request)
    {
        //dd($request->all());
        $produit = Produit::where('slug',$request->matiere)->first();
        $scelot = ProgrammeSouscorpsetat::where('id',$request->idscorps)->with('programme')->first();
        //dd($sce);
        if(isset($produit->id) and isset($scelot->id)){
            $listlots = ProgrammeLot::where('programme_id',$scelot->programme_id)->where('actif_id',$scelot->actif_id)->get();
            foreach($listlots as $lot){
                $check = ProgrammeSouscorpsetatLot::where('programme_id',$scelot->programme_id)
                        ->where('corpsetat_id',$scelot->corpsetat_id)
                        ->where('actif_id',$scelot->actif_id)
                        ->where('corpsetatlibel_id',$scelot->corpsetatlibel_id)
                        ->where('produit_id',$scelot->produit_id)
                        ->where('lot_id',$lot->id)
                        ->where('type_edit',0)->first();

                if($check){
                    $check->produit_id = $produit->id;
                    $check->qte = $request->quantite;
                    $check->save();
                }
            }
        }

    }

    public function deleteDqeMatiere($id)
    {
        $sce = ProgrammeSouscorpsetat::where('id',$id)->with('programme')->first();
        //dd($sce);
        if (isset($sce->id)){
            //Delete DQE QTT BY LOT
            $this->delDQEqttLot($id);

            //Faire les verification avant suppression apres
            //dd($sce);
            $sce->delete();

            return redirect()->route('gprogramme.setting.create.dqe',[$sce->programme->slug,$sce->actif_id])->with('success' ,'La matière a bien été supprimée.');
        }
        return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');

    }

    public function delDQEqttLot($id)
    {
        $sce = ProgrammeSouscorpsetat::where('id',$id)->with('programme')->first();
        $listlots = ProgrammeLot::where('programme_id',$sce->programme_id)->where('actif_id',$sce->actif_id)->get();
        //dd($sce,$listlots);
        foreach($listlots as $lot){
            $dqelot = ProgrammeSouscorpsetatLot::where('programme_id',$sce->programme_id)
                    ->where('actif_id',$sce->actif_id)
                    ->where('corpsetat_id',$sce->corpsetat_id)
                    ->where('produit_id',$sce->produit_id)
                    ->where('qte',$sce->qte)
                    ->where('corpsetatlibel_id',$sce->corpsetatlibel_id)
                    ->where('lot_id',$lot->id)
                    ->first();

            if($dqelot){
                $dqelot->delete();
            }
        }
    }

    public function storeDqeCeLibel(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle_corps_etat' => ['required'],
            'programme' => ['required'],
            'actif' => ['required'],
            'ce' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->programme)->first();
        if(isset($program->id)){
            $slugce = Str::slug($request->libelle_corps_etat);
            $check = ProgrammeCorpsetatLibelle::where('programme_id',$program->id)->where('corpsetat_id',$request->ce)->where('actif_id',$request->actif)->where('slug',$slugce)->first();
            if(!$check){
                $ce = new ProgrammeCorpsetatLibelle();
                $ce->programme_id = $program->id;
                $ce->actif_id = $request->actif;
                $ce->libelle = Str::upper($request->libelle_corps_etat);
                $ce->slug = $slugce;
                $ce->corpsetat_id = $request->ce;
                $ce->save();

                return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$request->actif])->with('success' ,'Le libelle a bien été ajouté');
            }
            return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$request->actif])->with('error' ,'Le libelle a déjà été ajouté.');

        }

        return redirect()->back();
    }

    public function updateDqeCeLibel(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle_corps_etat' => ['required'],
            'programme' => ['required'],
            'actif' => ['required'],
            'idlibel' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->programme)->first();
        if(isset($program->id)){
            $check = ProgrammeCorpsetatLibelle::where('programme_id',$program->id)->where('actif_id',$request->actif)->where('id',$request->idlibel)->first();
            $slugce = Str::slug($request->libelle_corps_etat);
            if($check){
                $check->libelle = Str::upper($request->libelle_corps_etat);
                $check->slug = $slugce;
                $check->save();

                return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$request->actif])->with('success' ,'Le libelle a bien été édité');
            }
            return redirect()->route('gprogramme.setting.create.dqe',[$program->slug,$request->actif])->with('error' ,'Le libelle est introuvable.');

        }

        return redirect()->back();
    }


    //DQE LIBELLE
    public function createDqeLibelle($program,$actif)
    {
        //dd($program,$actif);
        $programme = Programme::where('slug',$program)->first();
        $actifs = ProgrammeActif::where('id',$actif)->first();
        $matieres = Produit::with('type')->get();
        //dd($programme,$actifs);
        if(isset($programme->id) and isset($actifs->id)){
            $corpsetats = ProgrammeCorpsetat::where('programme_id',$programme->id)->where('actif_id',$actifs->id)->get();
            return view('admin.programmes.dqe_libelle',compact('programme','actifs','corpsetats','matieres'));
        }

        return redirect()->back();
    }

    public function getCorpsSmatiere($corpsid)
    {
        //dd($corpsid);
        $matieres = ProgrammeSouscorpsetat::where('corpsetat_id',$corpsid)->with('matieres')->has('matieres')->get();
        return $matieres ;
    }

    public function storeDqeLibelleMatiere(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'prefix' => ['required'],
            'matiere' => ['required'],
            'programme' => ['required'],
            'idcemat' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->programme)->first();
        if(isset($program->id)){
            $checkslug = Str::slug($request->prefix);
            $check = ProgrammeSouscorpsetatLibelle::where('programme_id',$program->id)->where('corpsetat_id',$request->idcemat)->where('slug',$checkslug)->first();
            if($check){
                $corpsOld = ProgrammeSouscorpsetat::where('programme_id',$program->id)->where('corpsetat_id',$request->idcemat)->where('prefix_id',$check->id)->update(['prefix_id' => NULL]);
                //UPDATE
                $corps = ProgrammeSouscorpsetat::whereIn('id',$request->matiere)->where('programme_id',$program->id)->where('corpsetat_id',$request->idcemat)->update(['prefix_id' => $check->id]);
            }else{
                $ce = new ProgrammeSouscorpsetatLibelle();
                $ce->programme_id = $program->id;
                $ce->corpsetat_id = $request->idcemat;
                $ce->slug = Str::slug($request->prefix);
                $ce->libelle = $request->prefix;
                $ce->save();

                //all martieres
                $corps = ProgrammeSouscorpsetat::whereIn('id',$request->matiere)->where('programme_id',$program->id)->where('corpsetat_id',$request->idcemat)->update(['prefix_id' => $ce->id]);
                //dd($corps);
            }
            $corps = ProgrammeCorpsetat::where('id',$request->idcemat)->first();

            return redirect()->route('gprogramme.setting.create.dqe.libelle',[$program->slug,$corps->actif_id])->with('success' ,'Le libelle a bien été ajouté');

        }

        return redirect()->back();

    }

    public function storeDqeLibelleCe(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'corps_etat' => ['required'],
            'ce' => ['required'],
            'programme' => ['required'],
            'actif' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->programme)->first();
        if(isset($program->id)){
            $checkslug = Str::slug($request->corps_etat);
            $check = ProgrammeCorpsetatLibelle::where('programme_id',$program->id)->where('actif_id',$request->actif)->where('slug',$checkslug)->first();
            if($check){
                $corpsOld = ProgrammeCorpsetat::where('programme_id',$program->id)->where('actif_id',$request->actif)->where('prefix_id',$check->id)->update(['prefix_id' => NULL]);
                //UPDATE
                $corps = ProgrammeCorpsetat::whereIn('id',$request->ce)->where('programme_id',$program->id)->where('actif_id',$request->actif)->update(['prefix_id' => $check->id]);
            }else{
                $ce = new ProgrammeCorpsetatLibelle();
                $ce->programme_id = $program->id;
                $ce->actif_id = $request->actif;
                $ce->slug = Str::slug($request->corps_etat);
                $ce->libelle = $request->corps_etat;
                $ce->save();

                //all martieres
                $corps = ProgrammeCorpsetat::whereIn('id',$request->ce)->where('programme_id',$program->id)->where('actif_id',$request->actif)->update(['prefix_id' => $ce->id]);
                //dd($corps);
            }

            return redirect()->route('gprogramme.setting.create.dqe.libelle',[$program->slug,$request->actif])->with('success' ,'Le libelle a bien été ajouté');

        }

        return redirect()->back();

    }

    public function getSmatiere($id)
    {
        $smatiere = ProgrammeSouscorpsetat::where('id',$id)->with('matieres')->first();
        return $smatiere;
    }

    //DQE PAR LOT

    public function lotDqe($idlot)
    {
        $lot = ProgrammeLot::where('id',$idlot)->first();
        if(!$lot){
            return redirect()->back()->with('error','Une erreur est survenue veuillez recommencer plus tard.');
        }

        $corpsetats = ProgrammeCorpsetat::where('programme_id',$lot->programme_id)->where('actif_id',$lot->actif_id)->get();
        //dd($corpsetats);
        $programme = Programme::where('id',$lot->programme_id)->first();
        $actifs = ProgrammeActif::where('id',$lot->actif_id)->first();
        $matieres = Produit::with('type')->get();

        return view('admin.programmes.dqe_lot',compact('lot','programme','actifs','corpsetats','matieres'));
    }

    public function storeDqeMatiereLot(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'matiere' => ['required'],
            'quantite' => ['required'],
            'idcemat' => ['required'],
            'idlibel' => ['required'],
            'lotid'=>['required'],
        ])->validate();

        $produit = Produit::where('slug',$request->matiere)->first();
        $ce = ProgrammeCorpsetat::where('id',$request->idcemat)->with('programme')->first();

        if(isset($produit->id) and isset($ce->id)){
            //check existant du produit
            $libdt = $request->idlibel == "0" ? null : $request->idlibel;
            $check = ProgrammeSouscorpsetatLot::where('programme_id',$ce->programme_id)
                    ->where('corpsetat_id',$ce->id)
                    ->where('lot_id',$request->lotid)
                    ->where('actif_id',$ce->actif_id)
                    ->where('corpsetatlibel_id',$libdt)
                    ->where('produit_id',$produit->id)->first();

            if(!$check){
                $sce = new ProgrammeSouscorpsetatLot();
                $sce->programme_id = $ce->programme_id;
                $sce->actif_id = $ce->actif_id;
                $sce->corpsetat_id = $ce->id;
                $sce->produit_id = $produit->id;
                $sce->qte = $request->quantite;
                $sce->corpsetatlibel_id = $libdt;
                $sce->lot_id = $request->lotid;
                $sce->save();

                return redirect()->route('gprogramme.n-setting.dqe.lot',$request->lotid)->with('success' ,'La matière a bien été ajoutée');
            }

            return redirect()->route('gprogramme.n-setting.dqe.lot',$request->lotid)->with('error' ,'La matière a déjà été ajoutée');

        }

        return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
    }

    public function updateDqeMatiereLot(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'matiere' => ['required'],
            'quantite' => ['required'],
            'idscorps' => ['required'],
            'idlibel' => ['required'],
            'lotid'=>['required'],
        ])->validate();

        $produit = Produit::where('slug',$request->matiere)->first();
        $sce = ProgrammeSouscorpsetatLot::where('id',$request->idscorps)->with('programme')->first();
        //dd($sce);
        if(isset($produit->id) and isset($sce->id)){

            $check = ProgrammeSouscorpsetatLot::where('programme_id',$sce->programme_id)->where('corpsetat_id',$sce->corpsetat_id)->where('actif_id',$sce->actif_id)->where('corpsetatlibel_id',$sce->corpsetatlibel_id)->where('produit_id',$produit->id)
            ->where('lot_id',$sce->lot_id)->first();
            if($check){
                $sce->produit_id = $produit->id;
                $sce->qte = $request->quantite;
                $sce->type_edit = 1;
                $sce->save();

                return redirect()->route('gprogramme.n-setting.dqe.lot',$request->lotid)->with('success' ,'La matière a bien été modifiée');
            }
            return redirect()->route('gprogramme.n-setting.dqe.lot',$request->lotid)->with('error' ,'La matière exite déjà');

        }
        return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
    }

    public function deleteDqeMatiereLot($id)
    {
        $sce = ProgrammeSouscorpsetatLot::where('id',$id)->with('programme')->first();
        //dd($sce);
        if (isset($sce->id)){
            //Faire les verification avant suppression apres
            //dd($sce);
            $sce->delete();

            return redirect()->route('gprogramme.n-setting.dqe.lot',$sce->lot_id)->with('success' ,'La matière a bien été supprimée.');
        }
        return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');

    }

    public function getSmatiereLot($id)
    {
        $smatiere = ProgrammeSouscorpsetatLot::where('id',$id)->with('matieres')->first();
        return $smatiere;
    }

     //majCorpEtats
     public function majCorpEtats(Request $request)
     {
         //dd($request->all());
         $program = Programme::where('id',$request->program)->first();
         $programme_id = $request->program;
         // save bon de demande
         $demande = new Bon_demande();
         $demande->status = 1;
         $demande->programme_id = $programme_id;
         $demande->soustraitant_id = 1;
         $demande->user_id = Auth::user()->id;
         $demande->ref = mb_strtoupper(Str::random(7));
         $demande->file = '';
         //ICI A REMTTRE EN PLACE EN DESSOUS
         //A retirer apres les mises a niveau
         $demande->status = 2;
         $demande->nivalid = 2;
         //A retirer apres les mises a niveau fin
         $demande->save();

         //save items bon de demande
         $i=0;

         //$actifs = ProgrammeActif::where('programme_id',$programme_id)->where('id', $request->actifs)->get();
         $ProgrammeLot = ProgrammeLot::where('programme_id',$programme_id)->where('actif_id', $request->actifs)->get();
         //dd($ProgrammeLot);
         foreach($ProgrammeLot as $lot){
             //dd($lot);
             $ttaux = 0 ;
             $ttmt = 0 ;
             if($request->corps[0] == 'tout')
                 $corpsetats = ProgrammeCorpsetat::where('programme_id',$programme_id)->where('actif_id',$lot->actif_id)->get();
             else
                 $corpsetats = ProgrammeCorpsetat::where('programme_id',$programme_id)->where('actif_id',$lot->actif_id)->whereIn('id', $request->corps)->get();
             //dd($corpsetats);
             foreach($corpsetats as $corps){
                //  $souscorpetat = ProgrammeSouscorpsetat::where('corpsetat_id',$corps->id)->where('programme_id',$programme_id)->with('matieres')->with('actif')->with('corpsetat')->get();
                $souscorpetat = ProgrammeSouscorpsetatLot::where('corpsetat_id',$corps->id)->where('lot_id',$lot->id)->where('programme_id',$programme_id)->with('matieres')->with('actif')->with('corpsetat')->get();
                //dd($souscorpetat);
                foreach ($souscorpetat as $e=>$sscorp) {
                    //dump('id-'.$e ,$sscorp->produit_id,$lot->id,$corps->id,$sscorp->id);

                    $produit_id = $sscorp->matieres->id;
                    $sous_corps_id = $sscorp->id;
                    //$unite = mb_strtoupper(Produit::where('produits.id', $sscorp->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                    $total_livre = \App\Bon_demande::where('status', 3)
                    ->where('programme_id',$programme_id)
                    ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                    ->where('bon_demande_items.produit_id', $sscorp->produit_id)
                    ->where('bon_demande_items.lot_id', $lot->id)
                    ->where('bon_demande_items.corpsetats_id', $corps->id)
                    ->where('bon_demande_items.scorpsetats_id', $sscorp->id)
                    ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                    ->pluck('total_livre')->first();
                    //dump('ttliber '.$e.' '.$total_livre);
                    //$total_livre = Bon_demande::where('status',4)->where('programme_id', $programme_id)->where('bon_demandes.soustraitant_id', $demande->soustraitant_id)->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')->where('bon_demande_items.produit_id', $sscorp->produit_id)->where('bon_demande_items.lot_id', $lot->lot_id)->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)->where('bon_demande_items.scorpsetats_id', $sscorp->id)->selectRaw('SUM(bon_demande_items.qte) AS total_livre')->pluck('total_livre')->first();
                    $restant = $sscorp->qte - $total_livre;
                    //$stock_dispo = Stock::where('produit_id',$sscorp->produit_id)->where('programme_id',$programme_id)->pluck('qte')->first();
                    //
                    $livre = ($total_livre != '') ? $total_livre : 0;
                    //$disponible = ($stock_dispo != '') ? $stock_dispo : 0;
                    if($restant > 0) {
                        //dd($restant,$sscorp->qte,$total_livre);
                        $qte = $restant;
                        Bon_demande_item::create([
                            'bon_demande_id' => $demande->id,
                            'soustraitant_id' => $demande->soustraitant_id,
                            'corpsetats_id' => $sscorp->corpsetat_id,
                            'scorpsetats_id' => $sous_corps_id,
                            'lot_id' => $lot->id,
                            'produit_id' => $produit_id,
                            'qte' => $qte
                        ]);
                        $i++;
                    }
                }
                //gestion des avancements et transactions
                 $taux = 100 ;
                 //dd($corps);
                 $old = Avancement::where([['programme_id',$programme_id],['lot_id',$lot->id],['corpsetat_id',$corps->id]])->orderBy('id','desc')->first();
                if(isset($old->id)){
                     //veririfier si le total est atteint
                     if($old->taux >= 100){
                         $taux = 0 ;
                     }else{
                         $taux = 100 - $old->taux;
                     }
                }

                if($taux > 0){
                     $allavc = Avancement::where([['programme_id',$programme_id],['lot_id',$lot->id],['corpsetat_id',$corps->id]])->sum('taux_execute');
                    if($allavc < 100){
                         $corps->prix ? $cout = ($taux/100) * $corps->prix  : $cout = 0;
                         $ttaux = $ttaux + $taux;
                         $ttmt = $ttmt + $cout;
                         //dd($cout);
                         //dd($taux,$old,$cout);

                         $new = new Avancement();
                         $new->programme_id = $programme_id;
                         $new->lot_id = $lot->id;
                         $new->soustraitant_id = 1;
                         $new->corpsetat_id = $corps->id;
                         $new->taux = $taux;
                         $new->taux_execute = $taux;
                         $new->cout = $cout;
                         $new->save() ;
                    }
                }
                 //dd($old,$taux,$corps->id);
             }

             //save transactions
             if($ttmt > 0){
                 $trans = new Transaction();
                 $trans->programme_id = $programme_id;
                 $trans->soustraitant_id = 1;
                 $trans->lot_id = $lot->id;
                 $trans->taux = $ttaux;
                 $trans->montant = $ttmt;
                 $trans->save();
             }
         }

        //dd('icic');
         if($i != 0){
             /////
             $demande->update(['status' => 3]);
             Bon_demande_historique::create([
                 'bon_demande_id' => $demande->id,
                 'user_id' => Auth::user()->id,
                 'action' => 'Livré'
             ]);
             return back()->with('success' ,'Mise à jour de corps d\'état effectuée avec succès !');

         }else{
             return back()->with('error' ,'Aucune données n\'a été enregistrée !');
         }

     }

    public function GetListeCorpEtat(Request $request){
         $corps = ProgrammeCorpsetat::where('programme_id',$request->programme_id)->where('actif_id',$request->actif_id)->get();
         $opt = '<option value="tout"> Tout sélectionner </option>';
         foreach($corps as $corp) {
             $opt .= '<option value="'.$corp->id.'">'.Str::upper($corp->libelle).'</option>';
         }
         return $opt;
    }

    public function editTypeRattachement($pgt,$type){
        $programme = Programme::find($pgt);
        $programme->type_rattachement = $type;
        $programme->save();
        return back()->with('success' ,'Type d\'attachement a été modifié avec succès !');
    }

    //STORE SOUS-PROGRAMME
    public function storeSousprogramme(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'id_programme' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->id_programme)->first();
        //dd($program);
        if(isset($program->id)){
            $check = ProgrammeSous::where('programme_id',$program->id)->where('libelle',$request->libelle)->first();
            if(!$check){
                $actif = new ProgrammeSous();
                $actif->programme_id = $program->id;
                $actif->libelle = $request->libelle;
                $actif->save();
            }

            return redirect()->route('gprogramme.show',$request->id_programme)->with('success' ,'Le sous-programme a bien été ajouté');
        }

        return redirect()->back();
    }

    public function updateSousprogramme(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $sous = ProgrammeSous::where('id',$request->slug)->first();
        //dd($sous);
        if(isset($sous->id)){
            $program = Programme::where('id',$sous->programme_id)->first();
            $sous->libelle = $request->libelle;
            $sous->save();

            return redirect()->route('gprogramme.show',$program->slug)->with('success' ,'Le sous-programme a bien été modifié');
        }

        return redirect()->back();
    }

    public function deleteSousprogramme($id) {
        $sous = ProgrammeSous::where('id',$id)->first();
        //dd($sous);
        if(isset($sous->id)){
            $program = Programme::where('id',$sous->programme_id)->first();
            $programslug = $program->slug;
            //$sous->delete();

            return redirect()->route('gprogramme.show',$programslug)->with('success' ,'Le sous-programme a bien été supprimé');
        }

        return redirect()->back();
    }


    //LIGNE BUDGETAIRE
    public function storeLignes(Request $request) {
        //dd($request->all());
        Validator::make($request->all(), [
            'corps' => ['required'],
            'montant' => ['required'],
            'id_programme' => ['required'],
            'type' => ['required'],
        ])->validate();

        $program = Programme::where('slug',$request->id_programme)->first();
        //dd($program);
        if(isset($program->id)){
            $slug = Str::slug($request->corps);
            $check = ProgrammeLigneBudgetaire::where('programme_id',$program->id)->where('slug',$slug)->first();
            if(!$check){
                $actif = new ProgrammeLigneBudgetaire();
                $actif->programme_id = $program->id;
                $actif->libelle = $request->corps;
                $actif->type = $request->type;
                $actif->slug = $slug;
                if(isset($request->hors_ds) and $request->hors_ds == 'oui'){
                    //check si il y a un autre hors_ds
                    $check = ProgrammeLigneBudgetaire::where('programme_id',$program->id)->where('hors_ds',1)->first();
                    if($check){
                        ProgrammeLigneBudgetaire::where('programme_id',$program->id)->where('hors_ds',1)->update(['hors_ds' => 0]);
                    }
                    $actif->hors_ds = 1;
                }
                $actif->montant = $this->deleteSpacePrice($request->montant);
                $actif->save();
            }

            return redirect()->route('gprogramme.show',$request->id_programme)->with('success' ,'La ligne budgétaire a bien été ajoutée');
        }

        return redirect()->back();
    }

    public function updateLignes(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'corps' => ['required'],
            'montant' => ['required'],
            // 'id_programme' => ['required'],
            'idligne' => ['required'],
            'type' => ['required'],
        ])->validate();

        $ligne = ProgrammeLigneBudgetaire::where('id',$request->idligne)->first();
        $program = Programme::where('id',$ligne->programme_id)->first();
        $programslug = $program->slug;
        //dd($ligne);
        if(isset($ligne->id)){
            $slug = Str::slug($request->corps);
            //verifier s'il existe un autre ProgrammeLigneBudgetaire avec le meme corps_etat_id avant de modifier
            $check = ProgrammeLigneBudgetaire::where('programme_id',$ligne->programme_id)->where('slug',$slug)->first();
            if(!$check){
                $ligne->libelle = $request->corps;
                $ligne->slug = $slug;
                $ligne->type = $request->type;
                $ligne->montant = $this->deleteSpacePrice($request->montant);
                $ligne->save();

                return redirect()->route('gprogramme.show',$programslug)->with('success' ,'La ligne budgétaire a bien été modifiée');
            }else{
                $ligne->montant = $this->deleteSpacePrice($request->montant);
                $ligne->type = $request->type;
                $ligne->save();
                return redirect()->route('gprogramme.show',$programslug)->with('success' ,'La ligne budgétaire a bien été modifiée');
            }
        }

        return redirect()->back();
    }

    public function delLignes($id) {
        $ligne = ProgrammeLigneBudgetaire::where('id',$id)->first();
        //dd($ligne);
        if(isset($ligne->id)){
            $program = Programme::where('id',$ligne->programme_id)->first();
            $programslug = $program->slug;
            //Faire des verification avant suppression
            //A FAIRE
            $ligne->delete();

            return redirect()->route('gprogramme.show',$programslug)->with('success' ,'La ligne budgétaire a bien été supprimée');
        }

        return redirect()->back();
    }
}
