<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $roles = Role::get();
        $users = User::with('role')->get();
        return view('admin.users.index',compact('roles','users'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
            'role' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ])->validate();

        $role = Role::where('slug',$request->role)->first();

        $user = new User();
        $user->role_id = $role? $role->id : 0 ;
        $user->name = $request->nom;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;

        if($request->file('userfile')){
            $file = $request->file('userfile');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'assets/uploads/users/';
            $picture = Str::random(6).'.'. $extension;
            if (!empty($user->img)) {
                unlink($folderName.$user->img);
            }
            $file->move($folderName,$picture);
            $user->img = $picture;
        }

        $user->save();

        return redirect()->route('gusers')->with('success' ,'L\'utilisateur a bien été ajouté');

    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get();
        return view('admin.users.edit',compact('roles','user'));
    }

    public function destroy($id)
    {
        if($id){
            $lot = User::find($id);
            $lot->delete();
            return redirect()->route('gusers')->with('success','L\'utilisateur a bien été supprimé.');
        }

        return redirect()->back();
    }

    public function updateProfil(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
            'role' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $role = Role::where('slug',$request->role)->first();

        $user = User::where('id',$request->slug)->first();
        $user->role_id = $role ? $role->id : 0 ;
        $user->name = $request->nom;
        //$user->password = Hash::make($request->password);
        //$user->email = $request->email;

        if($request->file('userfile')){
            $file = $request->file('userfile');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'assets/uploads/users/';
            $picture = Str::random(6).'.'. $extension;
            if (!empty($user->img)) {
                unlink($folderName.$user->img);
            }
            $file->move($folderName,$picture);
            $user->img = $picture;
        }

        $user->save();

        return redirect()->route('gusers')->with('success' ,'L\'utilisateur a bien été ajouté');
    }

    public function updatePass(Request $request)
    {
        Validator::make($request->all(), [
            'password' => ['required', 'string', 'min:6'],
            'password_confirm' => ['required', 'string', 'min:6'],
            'slug' => ['required'],
        ])->validate();


        if($request->password === $request->password_confirm){
            $user = User::where('id',$request->slug)->first();
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->route('gusers')->with('success','Le mot de passe a été mis à jour.');
        }

        return redirect()->back()->with('error','Les mots de passe sont différents.');
    }

    public function status(Request $request)
    {
        //dd($request->all());
        $user = User::find($request->id);
        if($user){
            $user->actif = $request->status;
            $user->save();
            return 1 ;
        }
        return 0 ;
    }



    /// ROLE ACTIONS
    /// ROLE ACTIONS
    /// ROLE ACTIONS
    public function roleIndex()
    {
        $roles = Role::get();
        return view('admin.roles.index',compact('roles'));
    }

    public function roleUpdade(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $role = Role::where('slug',$request->slug)->first();
        if($role){
            $role->slug = Str::slug($request->libelle);
            $role->libelle = $request->libelle;
            $role->save();

            return redirect()->route('gusers.roles')->with('success' ,'Le rôle a bien été modifié');
        }
        return redirect()->route('gusers.roles')->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
    }
}
