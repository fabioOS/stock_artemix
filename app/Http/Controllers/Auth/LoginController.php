<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Login_activity;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        return array_merge(
            $request->only($this->username(), 'password'),
            ['actif' => 1 ]
        );
    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if(isset(Auth::user()->role_id)) {
            //SAVE HISTORY USER
            $user = User::find($user->id);
            if($user != null) {
                $user->last_login_ip = $request->ip();
                $user->last_login_at = Carbon::now()->toDateTimeString();
                $user->save();
            }
            // update user login activity
            $activity = new Login_activity();
            $activity->user_id = $user->id;
            $activity->user_name = $user->name;
            $activity->login_at = Carbon::now()->format('Y-m-d H:i:s');
            $activity->ip_address = $request->ip();
            $activity->user_agent = $request->header('User-Agent');
            $activity->save();

            if(Auth::user()->role_id == 1)     return redirect()->route('home');
            elseif(Auth::user()->role_id == 2) return redirect()->route('chefchantier.cpprogram');
            elseif(Auth::user()->role_id == 3 OR Auth::user()->role_id == 5) return redirect()->route('cpprogram');
            elseif(Auth::user()->role_id == 4) return redirect()->route('magasinier.cpprogram');
            elseif(Auth::user()->role_id == 6) return redirect()->route('sa.home');
            elseif(in_array(Auth::user()->role_id, [7,8,9])) return redirect()->route('daf.home');
            else return redirect()->route('forbiden');
        }
        else {
            return redirect()->route('forbiden');
        }
    }

    public function logout()
    {
        try{
            //activity log
            if(Auth::check()) {
                // update user login info
                $user = User::find(Auth::user()->id);
                if($user != null) {
                    $user->update([
                        'last_logout_at' => Carbon::now()->toDateTimeString(),
                    ]);
                }
                // update user login activity
                $activity_id = Login_activity::where('user_id', '=', Auth::user()->id)->orderByDesc('id')->pluck('id')->first();
                $activity = Login_activity::where('id',$activity_id)->first();
                if($activity != null) {
                    $activity->update([
                        'logout_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);
                }
            }
            //logout
            Auth::logout();
            return redirect( route('login') );
        }
        catch(\Exception $e) {
            exit($e->getMessage());
        }
    }



}
