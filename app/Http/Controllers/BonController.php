<?php

namespace App\Http\Controllers;

use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Produit;
use App\ProgrammeSoustraitantcontrat;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        try{
            $programme_id = Session::get('program');
            return Bon_demande::where('programme_id', $programme_id)->with('bon_demanbde_items')->with('soustraitant')->with('user')->get();
            // return Bon_demande::where('programme_id', $programme_id)->with('bon_demanbde_items')->with('soustraitant')->with('user')->where('updated_at','desc')->get();
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getByStatus($status)
    {
        try{
            $programme_id = Session::get('program');
            return Bon_demande::where('programme_id', $programme_id)->where('status', $status)->with('bon_demanbde_items')->with('soustraitant')->with('user')->with('contrat')->orderBy('updated_at','desc')->get();
            // return Bon_demande::where('programme_id', $programme_id)->where('status', $status)->with('bon_demanbde_items')->with('soustraitant')->with('user')->with('contrat')->where('updated_at','desc')->get();
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getById($id)
    {
        try{
            return Bon_demande::with('bon_demanbde_items')->with('soustraitant')->with('contrat')->with('user')->findOrFail($id);
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getByFields($fields_row = [])
    {
        try{
            $programme_id = Session::get('program');
            $qry = Bon_demande::where('programme_id', $programme_id)->with('bon_demanbde_items')->with('soustraitant')->with('user')->with('contrat');
            if( count($fields_row)>0) {
                foreach($fields_row as $attr => $val) {
                    $qry->where($attr, $val);
                }
            }
            $data = $qry->get();
            return $data;
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDataTable($data, $route_prefix=null)
    {
        try{
            return view('bon.datatable', compact('data', 'route_prefix'));
        }
        catch(\Exception $e) {
            exit($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getItemDetails($item, $route_prefix=null)
    {
        try{
            return view('bon.itemdetails', compact('item', 'route_prefix'));
        }
        catch(\Exception $e) {
            exit($e->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validating($id)
    {
        try{
            $item = Bon_demande::findOrFail($id);
            $item->update(['status', 2]);
            Bon_demande_historique::create([
                'bon_demande_id' => $id,
                'user_id' => Auth::user()->id,
                'action' => 'Validé'
            ]);
            return back()->with('success', "Bon de demande N° <b class='text-primary'>".$item->ref."</b> validé avec succès ! ");
        }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delivering($id)
    {
        try{
            $demande = Bon_demande::findOrFail($id);
            $error = 0;
            $message = '';
            $items = Bon_demande_item::where('bon_demande_id',$id)->get();
            // on vérifie si qte demandée est dispo en stock
            foreach($items as $item) {
                $qte_demande = $item->qte;
                $qte_en_stock = Stock::where('produit_id', $item->produit_id)->where('programme_id', Session::get('program'))->pluck('qte')->first();
                if($qte_en_stock == null) $qte_en_stock = 0;
                if( $qte_en_stock < $qte_demande) {
                    $error++;
                    $info_produit = Produit::with('type')->find($item->produit_id);
                    $message .= "<li>Impossible de livrer <b>".$info_produit->libelle.'</b> : Qté disponible (<b>'.$qte_en_stock.'<sup>'.$info_produit->type->libelle.'</sup></b>) est inférieur à la demande (<b>'.$qte_demande.'<sup>'.$info_produit->type->libelle.'</sup></b>)</li>';
                }
            }
            if( $error > 0 ) {
                return back()->with('error', $message);
            }
            foreach($items as $item) {
                $stocks = Stock::where('produit_id', $item->produit_id)->where('programme_id', Session::get('program'))->get();
                foreach($stocks as $line) {
                    $stock = Stock::findOrFail($line->id);
                    $new_qte = $line->qte - $item->qte;
                    $stock->update(['qte' => $new_qte]);
                }
            }
            $demande->update(['status', 3]);
            Bon_demande_historique::create([
                'bon_demande_id' => $id,
                'user_id' => Auth::user()->id,
                'action' => 'Livré'
            ]);
            return back()->with('success', "Bon de demande N° <b class='text-primary'>".$demande->ref."</b> livré avec succès ! ");
        }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancelling($id)
    {
        try{
            $item = Bon_demande::findOrFail($id);
            $item->update(['status', 4]);
            Bon_demande_historique::create([
                'bon_demande_id' => $id,
                'user_id' => Auth::user()->id,
                'action' => 'Rejeté'
            ]);
            return back()->with('success', "Bon de demande N° <b class='text-primary'>".$item->ref."</b> rejeté avec succès ! ");
        }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function getByStatusContratSt($status)
    {
        try{
            $idprogram = Session::get('program');
            Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;
            $nbNotif = ProgrammeSoustraitantcontrat::with('lots')->with('straitant')->where('nivalid',$niv)->where('programme_id',$idprogram)->where('etat', $status)->count();
            if($status== 0){
                $nbNotif = ProgrammeSoustraitantcontrat::with('lots')->with('straitant')->where('programme_id',$idprogram)->where('etat', $status)->count();
            }
            return $nbNotif;
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }
}
