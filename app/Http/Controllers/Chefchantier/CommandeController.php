<?php

namespace App\Http\Controllers\Chefchantier;

use App\BC;
use App\BCitem;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Commande;
use App\CommandeItem;
use App\Http\Controllers\BonController;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produit;
use App\Programme;
use App\ProgrammeLigneBudgetaire;
use App\ProgrammeProduit;
use App\ProgrammeSous;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }


    public function getByStatus($status)
    {
        $programme_id = $this->getIdProgram();
        return Commande::where('programme_id',$programme_id)->where('status',$status)->count();
    }


    public function index()
    {
        return view('chefchantier.demande.index', compact('data', 'types'));
    }


    public function create()
    {
        $programme_id = $this->getIdProgram();
        $types = Typeproduit::with('produits')->get();
        $listproduits = ProgrammeProduit::where('programme_id',$programme_id)->has('produit')->with(['produit'=>function($q){$q->with('type');}])->get();
        return view('chefchantier.commandes.create',compact('types','listproduits'));
    }

    // public function getRefBss()
    // {
    //     $programme_id = $this->getIdProgram();

    //     $code = "C".date('ym');
    //     $count = Commande::where('programme_id',$programme_id)->withTrashed()->count() + 1;
    //     $num=sprintf("%04d",$count);
    //     $val = $code.'-'.$programme_id.'-'.$num;

    //     return $val;
    // }

    // public function store(Request $request)
    // {
    //     //dd($request->all());
    //     $valider = Validator::make($request->all(),[
    //         'description' =>'required',
    //         'datasession' =>'required',
    //     ]);

    //     if($valider->fails()){
    //         return redirect()->back()->withErrors($valider->errors());
    //     }

    //     try {
    //         $programme_id = $this->getIdProgram();

    //         if(count($request->datasession)>0) {
    //             $i=0;
    //             //creer la commande
    //             $command = new Commande();
    //             $command->programme_id = $programme_id;
    //             $command->description = $request->description;
    //             $command->ref = $this->getRefBss();
    //             $command->user_id = Auth::user()->id;
    //             $command->save();

    //             foreach($request->datasession as $data) {
    //                 $item = explode(';',$data);
    //                 //Save demande item
    //                 if((float) $item[3] > 0) {
    //                     //check exitance
    //                     $check1 = CommandeItem::where('commande_id',$command->id)->where('produit_id',$item[0])->first();
    //                     if(!$check1){
    //                         $cmditem = new CommandeItem();
    //                         $cmditem->programme_id = $programme_id;
    //                         $cmditem->commande_id = $command->id;
    //                         $cmditem->produit_id = $item[0];
    //                         $cmditem->qte = $item[3];
    //                         $cmditem->save();
    //                     }

    //                     $i++;
    //                 }
    //             }
    //         }

    //         if($i>0){
    //             $pgg = Programme::where('id',$programme_id)->first();
    //             //NOTIFIER LE CHEF PROJET QUE LA DEMANDE EST EN ATTENTE
    //             $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Vous avez une nouvelle commande en attente de traitement.</p>";
    //             $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$command->ref."<br><b>NOMBRE DE MATIERE DEMANDEE : </b>".$i."</p>";
    //             $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

    //             $sujet = "NOUVELLE COMMANDE EN ATTENTE - ".$pgg->libelle." - GSTOCK";

    //             $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
    //             $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
    //             $this->sendMailNotificaton($mess,$sujet,$user);

    //             return redirect()->route('ccommande.waiting')->with('success', "Demande de <b>$i</b> matières ajouté avec succès !");
    //         }else{
    //             return back()->with('error', "Aucune matières n'a été ajouté dû à une erreur de valeur(s) !");
    //         }

    //     }catch(\Exception $e) {
    //         return back()->with('error', $e->getMessage());
    //     }

    // }

    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);
        $demande = Commande::where('id',$id)->with('produits')->with('lignebudget')->with('user')->first();
        // dd($demande);
        // $demande = Commande::where('id',$id)->with(['produits' =>function($q) {$q->with('type');}])->with('user')->first();

        return view('chefchantier.commandes.show',compact('demande'));

    }

    public function delete($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);
        //verifier si la commande à un bon de commande
        $check = BC::where('commande_id',$id)->first();
        if($check){
            return redirect()->back()->with('error','Impossible de supprimer cette demande car elle a déjà un bon de commande.');
        }

        $demande = Commande::where('id',$id)->with('produits')->with('user')->first();
        //dd($demande);
        //delete item commande
        $item = CommandeItem::where('commande_id',$id)->delete();
        $demande->delete();

        return redirect()->back()->with('success','La demande a bien été supprimée.');
    }

    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',1)->where('programme_id',$programme_id)->orderBy('updated_at','desc')->with('produits')->with('user')->get();

        return view('chefchantier.commandes.waiting',compact('demandes'));
    }

    public function rejet()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',4)->where('programme_id',$programme_id)->with('produits')->with('user')->orderBy('updated_at','desc')->get();

        return view('chefchantier.commandes.reject',compact('demandes'));
    }

    public function valider()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',2)->where('programme_id',$programme_id)->with('produits')->with('user')->with('boncommande')->orderBy('updated_at','desc')->get();
        $demandes->map(function ($q) {
            // Récupérer toutes les bon de commande associées à ce cette demande
            $idsItem = $q->produits->pluck('id');
            $bcitems = BCitem::whereIn('commande_item_id', $idsItem)->sum('qte');
            $q->qtecmde = $bcitems;
        });

        return view('chefchantier.commandes.valid',compact('demandes'));
    }
}
