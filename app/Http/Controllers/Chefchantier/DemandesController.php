<?php

namespace App\Http\Controllers\Chefchantier;

use App\BDSpec;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;

class DemandesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function create()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        return view('chefchantier.demandes.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        Validator::make($request->all(),[
            'titre' =>'required',
            'montant' =>'required',
            'des' =>'required',
        ])->validate();

        try {

            $newbds = new BDSpec();
            $newbds->slug = $this->getRefDS();
            $newbds->programme_id = $programme_id;
            $newbds->titre = $request->titre;
            $newbds->montant = str_replace(' ', '', $request->montant);
            $newbds->description = $request->des;
            $newbds->user_id = Auth::user()->id;
            $newbds->save();

            //NOTIFIER LE SA
            $pgg = Programme::where('id',$programme_id)->first();
            $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Une demande spéciale est en attente de traitement.</p>";
            $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$newbds->slug."<br>";
            $mess .= "<b>AGENT : </b>".Auth::user()->name."<br>";
            $mess .= "<b>MONTANT : </b>".$request->montant." FCFA<br><b>MOTIF : </b>".nl2br($request->des)."</p>";
            $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "DEMANDE SPECIALE EN ATTENTE - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',6)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($mess,$sujet,$user);

            return redirect()->route('cdemande.waiting')->with('success','Votre demande a bien été envoyée.');
        }catch(\Exception $e){
            return back()->with('error', $e->getMessage());
        }

    }

    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('user_id',Auth::user()->id)->where('status',1)->orderBy('id','desc')->get();

        return view('chefchantier.demandes.waiting',compact('demandes'));
    }

    public function valid()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('user_id',Auth::user()->id)->where('status',2)->orderBy('id','desc')->get();

        return view('chefchantier.demandes.valid',compact('demandes'));
    }

    public function rejet()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('user_id',Auth::user()->id)->where('status',4)->orderBy('id','desc')->get();

        return view('chefchantier.demandes.reject',compact('demandes'));
    }

    public function delete($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('id',$id)->first();
        $demandes->delete();

        return redirect()->route('cdemande.waiting')->with('success','Votre demande a bien été supprimée.');

    }

    public function pdf($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $data = BDSpec::where('programme_id',$programme_id)->where('id',$id)->first();
        //dd($data);
        $pdf = PDF::loadView('service_achat.demandes.pdf',compact('data'));
        //return view('service_achat.demandes.pdf',compact('data'));
        return $pdf->download('demande-speciale-'.$data->slug.'.pdf');
    }

}
