<?php

namespace App\Http\Controllers\Chefchantier;

use App\Avancement;
use App\AvancementBrouillon;
use App\Avancementest;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Deleted_bon_demande;
use App\Deleted_bon_demande_historique;
use App\Deleted_bon_demande_item;
use App\Http\Controllers\BonController;
use App\LotSoustraitant;
use App\LotSoustraitantCorpsetat;
use App\Programme;
use App\ProgrammeActif;
use App\ProgrammeCorpsetat;
use App\ProgrammeLot;
use App\ProgrammePivotUser;
use App\ProgrammeSoustraitant;
use App\Stock;
use App\Transaction;
use App\Transactiontest;
use App\Typeproduit;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produit;
use App\ProgrammeSouscorpsetatLot;
use App\ProgrammeSoustraitantcontrat;
use App\ProgrammeSoustraitantcontratMontant;
use App\ProgrammeSoustraitantcontratTampon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('chefchantier.cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function listpgram()
    {
        Session::forget(['program']);

        $user = User::where('id',Auth::id())->with('programmes')->first();
        if(count($user->programmes)==1){
            return redirect()->route('chefchantier.cpprogram.item',$user->programmes[0]->slug);
        }
        return view('chefchantier.listpgram',compact('user'));
    }

    public function itempgram($slug)
    {
        $program = Programme::where('slug',$slug)->first();
        if(isset($program->id)){
            $check = ProgrammePivotUser::where('user_id',Auth::id())->where('programme_id',$program->id)->first();

            if($check){
                Session::put('program', $program->id);
                return redirect()->route('chefchantier.home');
            }

            return redirect()->back()->with('error','Vous n\'avez pas doit à ce programme');
        }

        return redirect()->back();
    }

    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //$lists = Avancement::where('programme_id',$idprogram)->select('lot_id','soustraitant_id',DB::raw('round(SUM(cout)) as coutt'))->groupBy('lot_id')->groupBy('soustraitant_id')->with('lot')->with('soustraitant')->get();
        $lists = Transaction::where('programme_id',$idprogram)->with('lot')->with('soustraitant')->get();

        $stats =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            $data= Avancement::whereYear('created_at',date('Y'))->whereMonth('created_at',$month)->where('programme_id',$idprogram)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
            $stats[$month] =$data->coutpaye;
        }
        return view('chefchantier.index',compact('lists','stats'));
    }



    public function stockList()
    {
        $programme_id = Session::get('program');
        $data = Produit::with('type')->with(['stocks'=>function($query) {
            $query->where('programme_id', Session::get('program'));
        }])->get();
        //$types = Typeproduit::with('produits')->get();
        $filtre = '';

        return view('chefchantier.stock',compact('data', 'filtre'));
    }

    public function stockBudget()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $produits = Produit::orderBy('libelle','asc')->with('type')->get();
       //dd($produits);
        return view('chefchantier.stock_budget',compact('produits'));
    }

    public function stockFilter(Request $request)
    {
        //filtre ==> tous/suffisant/moyen/rupture
        if($request->filtre == null){
            return back()->with('error', "Aucune données n'a été transmise !");
        }
        $programme_id = Session::get('program');
        $types = Typeproduit::with('produits')->get();
        $filtre = $request->filtre;
        $data = [];
        if($request->filtre == 'tous') {
            $data = Produit::with('type')->with(['stocks'=>function($query) {
                $query->where('programme_id', Session::get('program'));
            }])->get();
        }
        else {
            $qry = Produit::leftJoin('stocks', 'stocks.produit_id', '=', 'produits.id')->where('stocks.programme_id', Session::get('program'));
            if($filtre == 'suffisant') {
                $qry->whereRaw('stocks.qte >= produits.seuil');
            }
            if($filtre == 'moyen') {
                $qry->whereRaw('stocks.qte < produits.seuil');
                $qry->where('stocks.qte', '!=', 0);
            }
            if($filtre == 'rupture') {
                $qry->where('stocks.qte', 0);
            }
            $prod_id = $qry->selectRaw('produits.id AS id')->pluck('id');
            $data = Produit::whereIn('id', $prod_id)->with('type')->with(['stocks'=>function($query) {
                $query->where('programme_id', Session::get('program'));
            }])->get();
        }
        return view('chefchantier.stock', compact('data', 'types', 'filtre'));
    }




    /*
    *
    *   GESTION DE BON DE DEMANDE
    *
    */





    public function destroyDemande($id)
    {
        try{
            $id = decrypt($id);
            $bon = Bon_demande::findOrFail($id);
            // save last history before destroy all data of current Bon;
            //save before deleting bon items
            Deleted_bon_demande_historique::create([
                'deleted_bon_demande_id' => $id,
                'user_id' => Auth::user()->id,
                'action' => 'Supprimer',
                'rapport' => ''
            ]);
            // begin deleting
            //
            // save before deleting bon de demande
            $deleted_bon = Deleted_bon_demande::create([
                'programme_id' => $bon->programme_id,
                'soustraitant_id' => $bon->soustraitant_id,
                'user_id' => $bon->user_id,
                'ref' => $bon->ref,
                'file' => $bon->file,
                'status' => $bon->status,
                'motif' => $bon->motif
            ]);
            //ITEMS
            $bon_items = Bon_demande_item::where('bon_demande_id', $id)->select('id')->get();
            foreach($bon_items as $item){
                $line = Bon_demande_item::findOrFail($item->id);
                if($line != null) {
                    //save before deleting bon items
                    Deleted_bon_demande_item::create([
                        'deleted_bon_demande_id' => $deleted_bon->id,
                        'soustraitant_id' => $line->soustraitant_id,
                        'lot_id' => $line->lot_id,
                        'produit_id' => $line->produit_id,
                        'qte' => $line->qte
                    ]);
                    //delete bon items
                    $line->delete();
                }
            }
            $bon_history = Bon_demande_historique::where('bon_demande_id', $id)->select('id')->get();
            foreach($bon_history as $history){
                $line = Bon_demande_historique::findOrFail($history->id);
                if($line != null) {
                    //save before deleting bon items
                    Deleted_bon_demande_historique::create([
                        'deleted_bon_demande_id' => $deleted_bon->id,
                        'user_id' => $line->user_id,
                        'action' => $line->action,
                        'rapport' => $line->rapport
                    ]);
                    //delete bon items
                    $line->delete();
                }
            }
            //
            //delete bon
            $bon->delete();
            //return
            return back()->with('success', "Bon de demande N° ".$deleted_bon->ref." supprimer avec succès !");
        }
        catch(\Exception $e) {
            exit($e->getMessage());
        }
    }

    public function editDemande(Request $request, $id)
    {
        try{
            $id = decrypt($id);
            $bon = new BonController();
            $bon = $bon->getById($id);
            $lot_id = Bon_demande_item::where('bon_demande_id', $id)->selectRaw('DISTINCT(lot_id)')->pluck('lot_id');
            $vars = [
                'programme_id' => $bon->programme_id,
                'lot_id' => $lot_id,
                'soustraitant_id' => $bon->soustraitant_id
            ];
            $soustraitant = ProgrammeSoustraitant::with(['lots' =>function($query) use($lot_id) {
                $query->whereIn('lot_id', $lot_id)->with('lot');
            }])
            ->where('id', $bon->soustraitant_id)
            ->where('programme_id', $bon->programme_id)
            ->first();
            return view('chefchantier.bc_edit', compact('soustraitant', 'vars', 'bon'));
        }
        catch(\Exception $e) {
            exit($e->getMessage());
        }

    }

    public function updateDemande(Request $request, $id)
    {
        $id = decrypt($id);
        $programme_id = Session::get('program');
        // select bon de demande
        $demande = Bon_demande::findOrFail($id);
        //update items before bon de demande
        $i=$j=0;
        foreach($request->lot_id as $lot_id) {
            if( isset($request->corps_id[$lot_id]) && count($request->corps_id[$lot_id]) > 0 ) {
                foreach( $request->corps_id[$lot_id] as $corps_id) {
                    if( isset($request->sous_corps_id[$lot_id][$corps_id]) && count($request->sous_corps_id[$lot_id][$corps_id]) > 0) {
                        foreach( $request->sous_corps_id[$lot_id][$corps_id] as $key=>$sous_corps_id) {
                            if( isset($request->produit_id[$lot_id][$corps_id][$key]) && $request->produit_id[$lot_id][$corps_id][$key] != null ) {
                                if( isset($request->qte_sscorps[$lot_id][$corps_id][$key]) && $request->qte_sscorps[$lot_id][$corps_id][$key] != null ) {
                                    //var
                                    $produit_id = $request->produit_id[$lot_id][$corps_id][$key];
                                    $qte        = $request->qte_sscorps[$lot_id][$corps_id][$key];
                                    // checking
                                    $check = Bon_demande_item::where('bon_demande_id', $request->bon_id)
                                    ->where('lot_id', $lot_id)
                                    ->where('produit_id', $produit_id)
                                    ->pluck('qte')
                                    ->first();
                                    //update or delete if exist
                                    if( $check != null && (float)$check > 0 ) {
                                        $item = Bon_demande_item::where('bon_demande_id', $request->bon_id)
                                        ->where('lot_id', $lot_id)
                                        ->where('produit_id', $produit_id)
                                        ->first();
                                        if($qte == 0) {
                                            $item->delete();
                                            $i++;
                                        }
                                        else {
                                            if($qte > 0) {
                                                $item->update(['qte' => $qte]);
                                                $i++;
                                            }
                                        }
                                    }
                                    else { // create if not exist
                                        if($qte > 0) {
                                            $check = LotSoustraitantCorpsetat::findOrFail($corps_id);

                                            Bon_demande_item::create([
                                                'bon_demande_id' => $request->bon_id,
                                                'soustraitant_id' => $request->soustraitant_id,
                                                'lot_id' => $lot_id,
                                                'produit_id' => $produit_id,
                                                'corpsetats_id' => $check->corpsetats_id,
                                                'scorpsetats_id' => $sous_corps_id,
                                                'qte' => $qte
                                            ]);
                                            $i++;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        // update bon de demande
        if($request->file('demandefile')){
            $file = $request->file('demandefile');
            $extension = $file->getClientOriginalExtension() ?: 'pdf';
            $folderName = 'assets/uploads/bondemandes/';
            $document = Str::random(6).'.'. $extension;
            $file->move($folderName,$document);
            $demande->update(['file' => $document]);
            $j++;
        }
        if($i > 0){
            //A retirer apres les mises a niveau
            //$demande->update(['user_id' => Auth::user()->id, 'status'=>2,'nivalid'=>2]);
            //A retirer apres les mises a niveau fin
            //ICI A REMTTRE EN PLACE EN DESSOUS
            $demande->update(['user_id' => Auth::user()->id, 'status'=>1,'nivalid'=>1]);
        }
        if($i > 0 || $j > 0) {
            $message = "<p>Bonjour,</p><p>Vous avez un bon de demande en attente de traitement.</p>";
            $sujet = "BON DE DEMANDE EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";
            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            //ICI A REMTTRE EN PLACE EN DESSOUS
            $this->sendMailNotificaton($message,$sujet,$user);
            return redirect()->route('chefchantier.demande.canceled')->with('success' ,'Bon de demande modifier avec succès !');
        }
        else{
            return back()->with('error' ,'Vous n\'avez rien modifier !');
        }
    }



    public function createDemande()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $soutraitants = ProgrammeSoustraitant::with(['lots' =>function($query) {
            $query->with('lot');
        }])
        ->where('programme_id', $idprogram)
        ->get();
        return view('chefchantier.bc_create_step1', compact('soutraitants'));
    }



    public function step2createDemande(Request $request)
    {
        //dd($request->all());
        $programme_id = Session::get('program');
        $contrat = $request->contrat;
        $soustraitant = ProgrammeSoustraitant::with(['lots' =>function($query) use($contrat){
            $query->with('lot');
            $query->where('soustraitantcontrat_id',$contrat);
        }])
        ->where('id', $request->soustraitant_id)
        ->where('programme_id', $programme_id)
        ->first();
        $ctt = ProgrammeSoustraitantcontrat::where('id',$contrat)->where('programme_id',$programme_id)->first();

        $soustraitant->itemcontrat = $ctt;
        //dd($soustraitant);

        return view('chefchantier.bc_create_step2', compact('soustraitant'));
    }


    public function step3createDemande(Request $request)
    {
        //dd($request->all());
        $programme_id = Session::get('program');
        $lot_id = $request->lot_id;
        $contrat = $request->contrat_id;
        $vars = [
            'programme_id' => $programme_id,
            'lot_id' => $lot_id,
            'soustraitant_id' => $request->soustraitant_id,
            'contrat_id' => $request->contrat_id,
        ];

        $soustraitant = ProgrammeSoustraitant::with(['lots' =>function($query) use($lot_id,$contrat) {
            $query->whereIn('lot_id', $lot_id)->with('lot');
            $query->where('soustraitantcontrat_id',$contrat);
        }])
        ->where('id', $request->soustraitant_id)
        ->where('programme_id', $programme_id)
        ->first();

        $ctt = ProgrammeSoustraitantcontrat::where('id',$contrat)->where('programme_id',$programme_id)->first();
        $soustraitant->itemcontrat = $ctt;

        return view('chefchantier.bc_create_step3', compact('soustraitant', 'vars'));
    }

    public function storeDemande(Request $request)
    {
        // dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        // save bon de demande
        $demande = new Bon_demande();
        $demande->status = 2;
        $demande->programme_id = $programme_id;
        $demande->soustraitant_id = $request->soustraitant_id;
        $demande->user_id = Auth::user()->id;
        $demande->ref = $this->getRefBss();

        if($request->file('demandefile')){
            $file = $request->file('demandefile');
            $extension = $file->getClientOriginalExtension() ?: 'pdf';
            $folderName = 'assets/uploads/bondemandes/';
            $document = Str::random(6).'.'. $extension;
            $file->move($folderName,$document);
            $demande->file = $document;
        }
        else
            $demande->file = '';

        $demande->contrat_id = $request->contrat_id;
        $demande->save();

        //save items bon de demande
        $i=0;
        foreach($request->lot_id as $lot_id) {
            if(isset($request->corps_id[$lot_id]) && count($request->corps_id[$lot_id]) > 0 ) {
                foreach( $request->corps_id[$lot_id] as $corps_id) {
                    if(isset($request->sous_corps_id[$lot_id][$corps_id]) && count($request->sous_corps_id[$lot_id][$corps_id]) > 0) {
                        foreach( $request->sous_corps_id[$lot_id][$corps_id] as $key=>$sous_corps_id) {
                            if( isset($request->produit_id[$lot_id][$corps_id][$key]) && $request->produit_id[$lot_id][$corps_id][$key] != null ) {
                                if( isset($request->qte_sscorps[$lot_id][$corps_id][$key]) && $request->qte_sscorps[$lot_id][$corps_id][$key] != null ) {
                                    $produit_id = $request->produit_id[$lot_id][$corps_id][$key];
                                    $qte        = $request->qte_sscorps[$lot_id][$corps_id][$key];
                                    $check = ProgrammeSouscorpsetatLot::where('corpsetat_id',$corps_id)->where('produit_id',$produit_id)->where('lot_id',$lot_id)->first();
                                    if($check){
                                        Bon_demande_item::create([
                                            'bon_demande_id' => $demande->id,
                                            'soustraitant_id' => $request->soustraitant_id,
                                            'corpsetats_id' => $check->corpsetat_id,
                                            'scorpsetats_id' => $sous_corps_id,
                                            'lot_id' => $lot_id,
                                            'produit_id' => $produit_id,
                                            'qte' => $qte
                                        ]);
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if($i != 0){
            $pgg = Programme::where('id',$programme_id)->first();
            $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Un nouveau bon de sortie est en attente de traitement.</p>";
            $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$demande->ref."<br><b>NOMBRE DE PRODUIT DEMANDE : </b>".$i."</p>";
            $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "BON DE SORTIE DE STOCK EN ATTENTE DE TRAITEMENT - ".$pgg->libelle." - G-STOCK";;
            //send le mail au magasinier
            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',4)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($mess,$sujet,$user);
            return redirect()->route('chefchantier.demande.create')->with('success' ,'Bon de sortie ajouter avec succès !');

        }else{
            //supprimer les demandes creer
            $iddmd = $demande->id;
            $demande = Bon_demande::where('id',$iddmd)->delete();

            return back()->with('error' ,'Aucune données n\'a été enregistrée !');
        }
    }


    public function detailsDemande($id)
    {
        $id = decrypt($id);
        $data = new BonController();
        $data = $data->getById($id);
        return view('chefchantier.bc_details', compact('data'));
    }

    public function waitingDemande()
    {
        $data = new BonController();
        $data = $data->getByStatus(2);
        return view('chefchantier.bc_waiting', compact('data'));
    }

    public function validatedDemande()
    {
        $data = new BonController();
        $data = $data->getByStatus(2);
        return view('chefchantier.bc_validated', compact('data'));
    }

    public function delivredDemande()
    {
        $data = new BonController();
        $data = $data->getByStatus(3);
        return view('chefchantier.bc_delivred', compact('data'));
    }

    public function canceledDemande()
    {
        $data = new BonController();
        $data = $data->getByStatus(4);
        return view('chefchantier.bc_cancel', compact('data'));
    }

    public function bl()
    {
        $this->getProgramExist();
        return view('chefchantier.bl');
    }

    public function showBc($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        return view('chefchantier.bc_show');
    }

    //PROFILE
    public function profil()
    {
        $this->getProgramExist();
        $user = User::where('id',Auth::id())->with('role')->with('programmes')->first();
        return view('chefchantier.profile',compact('user'));
    }

    public function profilUpdate(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'name' =>'required',
            'contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->name = $request->name;
            $user->contact = $request->contact;
            $user->save();
            return redirect()->back()->with('success','✔ Votre profil a été modifié');
        }

    }

    public function profilUpdatePass(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $oldpass = $request->motpass;
            $newpass = $request->newpass;
            $newpassconfirm = $request->confirmpass;
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->back()->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->back()->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->back()->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function profilUpdateAvatar(Request $request)
    {
        $this->getProgramExist();
        //d($request->all());
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        try{
            if($fileSize <= 4000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName ='assets/uploads/users/';
                $picture = Str::random(6).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->back()->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 4Mb');
            endif;

        }catch (\Exception $e){
            //dd($e->getMessage());
        }

    }


    public function indexLivraison()
    {
        $data = new BonController();
        $data = $data->getByStatus(3);
        return view('chefchantier.bc_delivred', compact('data'));
    }


    public function detailsLivraison($id)
    {
        $id = decrypt($id);
        $data = new BonController();
        $data = $data->getById($id);
        return view('chefchantier.bc_details', compact('data'));
    }


    public function exportLivraison($id)
    {
        $id = decrypt($id);
        $data = new BonController();
        $data = $data->getById($id);
        return $data;
        //return view('chefchantier.bc_details', compact('data'));
    }

    ///MES ACTIONS DANS CE NEW CONTROLLERS
    ///
    //SOUS-TRAITANT

    public function straitant()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //$lots = ProgrammeLot::where('programme_id',$idprogram)->orderBy('lot','asc')->get();
        //$soustraitants = ProgrammeSoustraitant::with('lots')->where('programme_id',$idprogram)->get();
        $soustraitants = ProgrammeSoustraitant::with('contrats')->where('programme_id',$idprogram)->get();
        //dd($soustraitants);

        return view('chefchantier.sous_traitant.index',compact('soustraitants'));
    }

    public function getLotStraitant($id)
    {
        $soustraitants = ProgrammeSoustraitantcontrat::where('id',$id)->with('lots')->first();
        $array = [];
        foreach ($soustraitants->lots as $k=>$lot){
            $l=ProgrammeLot::where('id',$lot->lot_id)->first();
            $actif=ProgrammeActif::where('id',$l->actif_id)->first();
            $array[$k]['lot'] = $l ? $l->lot : '';
            $array[$k]['type'] = $actif ? $actif->libelle :'';
        }

        return $array;
    }

    public function storeStraitantOld(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'lot' =>'required',
            'phone' =>'required',
            'contrat' =>'required',
        ]);

        $idprogram = $this->getIdProgram();
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $soustraitant = new ProgrammeSoustraitant();
            $soustraitant->programme_id = $idprogram;
            $soustraitant->nom = $request->nom;
            $soustraitant->contact = $request->phone;

            if($request->file('contrat')){
                $file = $request->file('contrat');
                $extension = $file->getClientOriginalExtension() ?: 'pdf';
                $folderName = 'assets/uploads/contrats/';
                $picture = Str::random(6).'.'. $extension;
                if (!empty($soustraitant->contrat)) {
                    unlink($folderName.$soustraitant->contrat);
                }
                $file->move($folderName,$picture);
                $soustraitant->contrat = $picture;
            }

            //ICI A REMTTRE EN PLACE EN DESSOUS
            //A retirer apres les mises a niveau
            //$soustraitant->etat = 2;
            //$soustraitant->nivalid = 2;
            //A retirer apres les mises a niveau fin

            $soustraitant->save();

            //save for lot
            foreach ($request->lot as $lot){
                $check = LotSoustraitant::where('lot_id',$lot)->where('soustraitant_id',$soustraitant->id)->first();
                if(!$check){
                    $newlot = new LotSoustraitant();
                    $newlot->lot_id = $lot;
                    $newlot->soustraitant_id = $soustraitant->id;
                    $newlot->save();
                }
            }

            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour,</p><p>Vous avez un sous-taitant en attente de traitement.</p>";
            $sujet = "SOUS-TRAITANT EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            //ICI A REMTTRE EN PLACE EN DESSOUS
            $this->sendMailNotificaton($message,$sujet,$user);


            return redirect()->route('chefchantier.st')->with('success' ,'Le sous-traitant a bien été ajouté');
        }

    }

    public function storeStraitant(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            //'lot' =>'required',
            'phone' =>'required',
            //'contrat' =>'required',
        ]);

        $idprogram = $this->getIdProgram();
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $soustraitant = new ProgrammeSoustraitant();
            $soustraitant->programme_id = $idprogram;
            $soustraitant->nom = $request->nom;
            $soustraitant->contact = $request->phone;
            $soustraitant->etat = 2;
            $soustraitant->nivalid = 2;
            $soustraitant->save();

            return redirect()->route('chefchantier.st')->with('success' ,'Le sous-traitant a bien été ajouté');
        }

    }

    public function editStraitant($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $soustraitant = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();

        if (isset($soustraitant->id)){
            $lots = ProgrammeLot::where('programme_id',$idprogram)->get();
            $stlot = LotSoustraitant::where('soustraitant_id',$slug)->pluck('lot_id')->toArray();

            return view('chefchantier.sous_traitant.edit',compact('lots','soustraitant','stlot'));
        }

        return redirect()->back();
    }

    public function updateStraitant(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'slug' =>'required',
            'phone' =>'required',
            //'lot' =>'required',
        ]);

        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $soustraitant = ProgrammeSoustraitant::where('id',$request->slug)->first();

            if($soustraitant){
                $soustraitant->programme_id = $idprogram;
                $soustraitant->nom = $request->nom;
                $soustraitant->contact = $request->phone;
                $soustraitant->save();

                return redirect()->route('chefchantier.st')->with('success' ,'Le sous-traitant a bien été modifié');
            }

            return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
        }
    }

    public function deltraitantOld($slug)
    {
        $check = ProgrammeSoustraitant::where('id',$slug)->first();
        if($check){
            $check->delete();
        }
        return redirect()->back()->with('success' ,'Le sous-traitant a bien été supprimé');
    }

    public function deltraitant($slug)
    {
        //dd($slug);
        $slug = decrypt($slug);
        $check = ProgrammeSoustraitantcontrat::where('id',$slug)->first();
        if($check){
            //verifier si tous les transactionson ete valider avant supp
            $checktrans = Transaction::where('soustraitant_id',$check->soustraitant_id)->where('soustraitantcontrat_id',$slug)->where('status',1)->count();
            if($checktrans > 0){
                return redirect()->back()->with('error' ,'Impossible de supprimer ce contrat car il a des transactions en cours');
            }

            //supprimer les lotsoutraitcorpsetat
            DB::table('lotsoustraitans_corpsetats')->where('soustraitantcontrat_id',$slug)->update(['etatdel'=>0]);
            $check->delete();
        }
        return redirect()->back()->with('success' ,'Le contrat du sous-traitant a bien été supprimé');
    }

    //CONTRAT
    public function contratStraitant($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $sous = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();

        if($sous->etat != 2){
            return redirect()->back()->with('error',"Vous n'avez pas de droit à cette page");
        }

        $lotid = $sous->lots ? $sous->lots->pluck('lot_id') : [];
        //get type lot
        $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lotid)->groupBy('actif_id')->with('actifs')->get();
        //dd($lotid,$sous->lots,$actifs);

        if (isset($sous->id)){
            //$lots = ProgrammeLot::where('programme_id',$idprogram)->get();
            //$stlot = LotSoustraitant::where('soustraitant_id',$slug)->pluck('lot_id')->toArray();
            $contrats = ProgrammeSoustraitantcontrat::where('soustraitant_id',$sous->id)->with('straitant')->orderBy('id','desc')->get();
            $lots = ProgrammeLot::where('programme_id',$idprogram)->orderBy('lot','asc')->get();

            return view('chefchantier.sous_traitant.new_contrats',compact('lots','sous','actifs','contrats'));
        }

        return redirect()->back();
    }

    public function contratStraitantEnd($id)
    {
        $idprogram = $this->getIdProgram();
        $stcontrat = ProgrammeSoustraitantcontratTampon::where('id',$id)->with('straitant')->first();
        if(!$stcontrat){
            return redirect()->route('st')->with('error','Veuillez selectionnez un sous-traitant actif');
        }
        $lotid = json_decode($stcontrat->motif);
        $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lotid)->groupBy('actif_id')->with('actifs')->get();
        //dd($actifs);

        return view('chefchantier.sous_traitant.new_contrats_create2',compact('stcontrat','actifs'));
    }

    public function storeStraitantContratOld(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'lot' =>'required',
            'slugst' =>'required',
            'contrat' =>'required',
        ]);

        $idprogram = $this->getIdProgram();
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $st = ProgrammeSoustraitant::where('id',$request->slugst)->first();

            if(!$st){
                return redirect()->back()->with('error','Veuillez selectionnez un sous-traitant actif');
            }

            $stcontrat = new ProgrammeSoustraitantcontrat();
            $stcontrat->libelle = $request->libelle;
            $stcontrat->programme_id = $idprogram;
            $stcontrat->soustraitant_id = $st->id;
            $stcontrat->slug = 'ST-'.date('di').'-'.rand(1,99);

            if($request->file('contrat')){
                $file = $request->file('contrat');
                $extension = $file->getClientOriginalExtension() ?: 'pdf';
                $folderName = 'assets/uploads/contrats/';
                $picture = Str::random(6).'.'. $extension;
                if (!empty($stcontrat->contrat)) {
                    unlink($folderName.$stcontrat->contrat);
                }
                $file->move($folderName,$picture);
                $stcontrat->contrat = $picture;
            }

            $stcontrat->save();

            //save for lot
            foreach ($request->lot as $lot){
                $check = LotSoustraitant::where('lot_id',$lot)->where('soustraitantcontrat_id',$stcontrat->id)->where('soustraitant_id',$stcontrat->soustraitant_id)->first();
                if(!$check){
                    $newlot = new LotSoustraitant();
                    $newlot->lot_id = $lot;
                    $newlot->soustraitantcontrat_id = $stcontrat->id;
                    $newlot->soustraitant_id = $stcontrat->soustraitant_id;
                    $newlot->save();
                }
            }

            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour,</p><p>Vous avez un contrat d'un sous-taitant en attente de traitement.</p>";
            $sujet = "CONTRAT SOUS-TRAITANT EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            //ICI A REMTTRE EN PLACE EN DESSOUS
            $this->sendMailNotificaton($message,$sujet,$user);


            return redirect()->route('chefchantier.st.n-contrat',$st->id)->with('success' ,'Le contrat a bien été ajouté');
        }
    }

    public function storeStraitantContrat(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'lot' =>'required',
            'slugst' =>'required',
            'contrat' =>'required',
        ]);

        $idprogram = $this->getIdProgram();
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $st = ProgrammeSoustraitant::where('id',$request->slugst)->first();

            if(!$st){
                return redirect()->back()->with('error','Veuillez selectionnez un sous-traitant actif');
            }

            $stcontrat = new ProgrammeSoustraitantcontratTampon();
            $stcontrat->libelle = $request->libelle;
            $stcontrat->programme_id = $idprogram;
            $stcontrat->soustraitant_id = $st->id;
            $stcontrat->slug = 'ST-'.date('di').'-'.rand(1,99);

            if($request->file('contrat')){
                $file = $request->file('contrat');
                $extension = $file->getClientOriginalExtension() ?: 'pdf';
                $folderName = 'assets/uploads/contrats/';
                $picture = Str::random(6).'.'. $extension;
                if (!empty($stcontrat->contrat)) {
                    unlink($folderName.$stcontrat->contrat);
                }
                $file->move($folderName,$picture);
                $stcontrat->contrat = $picture;
            }

            $stcontrat->motif = json_encode($request->lot);
            $stcontrat->save();

            return redirect()->route('chefchantier.st.n-contrat.end',$stcontrat->id);
        }
    }

    public function storeStraitantContratEnd(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $valider = Validator::make($request->all(),[
            'firststep' =>'required',
        ]);

        $idprogram = $this->getIdProgram();
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $itemst = ProgrammeSoustraitantcontratTampon::where('id',$request->firststep)->first();
            if(!$itemst){
                return redirect()->back()->with('error','Une erreur est survenue, veuillez recommencez');
            }
            $st = ProgrammeSoustraitant::where('id',$itemst->soustraitant_id)->first();

            if(!$st){
                return redirect()->back()->with('error','Veuillez selectionnez un sous-traitant actif');
            }
            //dd($itemst);

            $stcontrat = new ProgrammeSoustraitantcontrat();
            $stcontrat->libelle = $itemst->libelle;
            $stcontrat->programme_id = $idprogram;
            $stcontrat->soustraitant_id = $itemst->soustraitant_id;
            $stcontrat->slug = $itemst->slug;
            $stcontrat->contrat = $itemst->contrat;

            $stcontrat->save();

            //SAVE LES LOTS
            $lots = json_decode($itemst->motif);
            foreach($lots as $lot){
                $check = LotSoustraitant::where('lot_id',$lot)->where('soustraitantcontrat_id',$stcontrat->id)->where('soustraitant_id',$stcontrat->soustraitant_id)->first();
                if(!$check){
                    $newlot = new LotSoustraitant();
                    $newlot->lot_id = $lot;
                    $newlot->soustraitantcontrat_id = $stcontrat->id;
                    $newlot->soustraitant_id = $stcontrat->soustraitant_id;
                    $newlot->save();
                }
            }

            //SAVE LES CE PAR
            $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lots)->groupBy('actif_id')->with('actifs')->get();
            foreach($actifs as $itemactif){
                $rq ='ce_'.$itemactif->actif_id;
                if(isset($request->$rq) and count($request->$rq)>0){

                    foreach ($request->$rq as $corp){
                        $check = LotSoustraitantCorpsetat::where('actif_id',$itemactif->actif_id)->where('etatdel',1)->where('soustraitant_id',$stcontrat->soustraitant_id)->where('corpsetats_id',$corp)->first();
                        if(!$check){
                            $new = new LotSoustraitantCorpsetat();
                            $new->actif_id = $itemactif->actif_id;
                            $new->soustraitant_id = $stcontrat->soustraitant_id;
                            $new->corpsetats_id = $corp;
                            $new->save();
                        }
                    }
                }
            }

            //dd($lots);
            $itemst->delete();

            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour,</p><p>Vous avez un contrat d'un sous-taitant en attente de traitement.</p>";
            $sujet = "CONTRAT SOUS-TRAITANT EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            //ICI A REMTTRE EN PLACE EN DESSOUS
            $this->sendMailNotificaton($message,$sujet,$user);


            return redirect()->route('chefchantier.st.n-contrat',$st->id)->with('success' ,'Le contrat a bien été ajouté');
        }
    }

    public function getCorps($id,$typeid)
    {
        //dd($id,$typeid);
        $sous = ProgrammeSoustraitant::where('id',$id)->with('lots')->first();
        $corps = ProgrammeCorpsetat::where('programme_id',$sous->programme_id)->where('actif_id',$typeid)->get();

        if(count($corps)>0){
            return $corps;
        }
        return $data = [];
    }

    public function storeStraitantLotDqe(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'corpsetats' =>'required',
            'typeid' =>'required',
            'stid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            foreach ($request->corpsetats as $corp){
                $check = LotSoustraitantCorpsetat::where('actif_id',$request->typeid)->where('soustraitant_id',$request->stid)->where('corpsetats_id',$corp)->first();
                if(!$check){
                    $new = new LotSoustraitantCorpsetat();
                    $new->actif_id = $request->typeid;
                    $new->soustraitant_id = $request->stid;
                    $new->corpsetats_id = $corp;
                    $new->save();
                }
            }
            return redirect()->route('chefchantier.st.contrat',$request->stid)->with('success' ,'Le corps etat a bien été ajouté au contrat');
        }

    }

    public function deleteStraitantLotDqe($item)
    {
        $check = LotSoustraitantCorpsetat::where('id',$item)->first();
        //dd($check);
        if(isset($check->id)){
            $stid = $check->soustraitant_id;
            //check si il a deja eu un paiement pour ce corps d'etat avant supression
            $newcheck = Avancement::where([['soustraitant_id',$check->soustraitant_id],['corpsetat_id',$check->corpsetats_id]])->first();
            if(isset($newcheck->id)){
                return redirect()->route('chefchantier.st.contrat',$stid)->with('error' ,'Impossible de supprimer le corps etat car il a déjà faire objet de paiment');
            }

            $check->delete();
            return redirect()->route('chefchantier.st.contrat',$stid)->with('success' ,'Le corps etat a bien été ajouté au contrat');
        }
        return redirect()->back();
    }

    public function getContratSt($slug)
    {
        return $contrats = ProgrammeSoustraitantcontrat::where('soustraitant_id',$slug)->where('etat',2)->get();
    }

    //PAYES

    public function listctst()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $contrats = ProgrammeSoustraitantcontrat::where('programme_id',$programme_id)->has('straitant')->with('straitant')->with('lots')->where('etat',2)->orderBy('id','desc')->get();
        //dd($demandes);
        return view('chefchantier.transaction_liste_create',compact('contrats'));
    }

    public function payeStraitantOld($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $sous = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();

        if($sous->etat != 2){
            return redirect()->back()->with('error',"Vous n'avez pas de droit à cette page");
        }

        //$oldavances = AvancementBrouillon::where('soustraitant_id',$slug)->where('programme_id',$idprogram)->orderBy('lot_id','asc')->with('lot')->with('corpsetat')->get();
        //dd($oldavances);
        if (isset($sous->id)){
            return view('chefchantier.sous_traitant.paye',compact('sous'));
        }

        return redirect()->back();
    }

    public function payeStraitant($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $sous = ProgrammeSoustraitantcontrat::where('id',$slug)->with('straitant')->with('lots')->first();
        // dump($sous);
        if($sous->etat != 2){
            return redirect()->back()->with('error',"Vous n'avez pas de droit à cette page");
        }

        //$oldavances = AvancementBrouillon::where('soustraitant_id',$slug)->where('programme_id',$idprogram)->orderBy('lot_id','asc')->with('lot')->with('corpsetat')->get();
        //dd($oldavances);
        if (isset($sous->id)){
            return view('chefchantier.sous_traitant.paye',compact('sous'));
        }

        return redirect()->back();
    }

    public function payeLotStraitantOld($slug,$lotid)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        //$sous = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();
        $sous = LotSoustraitant::where('soustraitant_id',$slug)->where('lot_id',$lotid)->with('lot')->with('soustraitant')->first();
        //dd($sous);
        $oldavances = AvancementBrouillon::where('soustraitant_id',$slug)->where('programme_id',$idprogram)->orderBy('lot_id','asc')->with('lot')->with('corpsetat')->get();
        //dd($slug,$idprogram,$oldavances);
        if (isset($sous->id)){
            return view('chefchantier.sous_traitant.paye_item',compact('sous','oldavances'));
        }

        return redirect()->back();
    }

    public function payeLotStraitant($slug,$lotid)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        //dd($slug,$lotid);

        $straitcontrat = ProgrammeSoustraitantcontrat::where('id',$slug)->first();
        //dd($strait);
        //$sous = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();
        $sous = LotSoustraitant::where('soustraitantcontrat_id',$straitcontrat->id)->where('soustraitant_id',$straitcontrat->soustraitant_id)->where('lot_id',$lotid)->with('lot')->with('soustraitant')->first();
        //dd($sous);
        $oldavances = AvancementBrouillon::where('soustraitant_id',$straitcontrat->soustraitant_id)->where('programme_id',$idprogram)->orderBy('lot_id','asc')->with('lot')->with('corpsetat')->get();
        //dd($slug,$idprogram,$oldavances);
        if (isset($sous->id)){
            return view('chefchantier.sous_traitant.paye_item',compact('sous','oldavances','straitcontrat'));
        }

        return redirect()->back();
    }

    public function storepayeStraitant(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'taux' =>'required|numeric|min:0|max:100',
            'lotid' =>'required',
            'corpsid' =>'required',
            'stid' =>'required',
            'idcontrat'=>'required'
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            //dd($request->all());
            //$straitant = ProgrammeSoustraitant::where('id',$request->stid)->first();
            //check old taux renseigner
            $old = Avancement::where([['programme_id',$idprogram],['lot_id',$request->lotid],['corpsetat_id',$request->corpsid]])->orderBy('id','desc')->first();
            //$old = Avancement::where([['programme_id',$idprogram],['lot_id',$request->lotid],['soustraitant_id',$request->stid],['corpsetat_id',$request->corpsid]])->orderBy('id','desc')->first();
            //dd($old);
            if(isset($old->id)){
                //veririfier si le total est atteint
                if($old->taux >= 100){
                    return redirect()->back()->with('error','Le taux d\'avancement de corps d\'etat est terminer');
                }
                //veririfier si le taux saisir est superieur au taux précedent
                if($old->taux >= $request->taux){
                    return redirect()->back()->with('error','Le taux saisir est inférieur à la dernière évaluation, veuillez vérifier le taux d\'avancement');
                }
            }
            //veririfier si l'avancement est deja saisir dans le brouillons
            // $check = AvancementBrouillon::where([['programme_id',$idprogram],['lot_id',$request->lotid],['soustraitant_id',$request->stid],['corpsetat_id',$request->corpsid]])->first();
            $check = AvancementBrouillon::where([['programme_id',$idprogram],['lot_id',$request->lotid],['corpsetat_id',$request->corpsid]])->first();
            if(!$check){
                //verifier si le taux est conforme avec les avancements test
                $checkAvances = Avancementest::where([['programme_id',$idprogram],['lot_id',$request->lotid],['corpsetat_id',$request->corpsid]])->with('transactiontest')->orderBy('id','desc')->first();
                //dd($checkAvances);
                if(isset($checkAvances->id)){
                    if($checkAvances->transactiontest->etat == 1){
                        // return redirect()->back()->with('error','Un taux d\'avancement de ce corps d\'état est en processus de validation, veuillez patienter avant de saisir un nouveau taux ou contacter votre N+1');
                        if($checkAvances->taux >= 100){
                            return redirect()->back()->with('error','Un taux d\'avancement de ce corps d\'état est en processus de validation.');
                        }
                        if($checkAvances->taux >= $request->taux){
                            return redirect()->back()->with('error','Le taux saisir est inférieur à la dernière évaluation, veuillez vérifier le taux d\'avancement de ce corps d\'etat');
                        }
                    }
                }

                $new = new AvancementBrouillon();
                $new->programme_id = $idprogram;
                $new->lot_id = $request->lotid;
                $new->soustraitant_id = $request->stid;
                $new->corpsetat_id = $request->corpsid;
                $new->taux = $request->taux;
                $new->soustraitantcontrat_id = $request->idcontrat;
                $new->user_id = Auth::user()->id;
                $new->save() ;

                return redirect()->route('chefchantier.st.lot.paye',[$request->idcontrat,$request->lotid])->with('success','Taux d\'avancement a bien été ajouté');
            }
            return redirect()->back()->with('error','Vous avez déjà renseigner ce corps d\'etat');
        }
    }

    public function delpayeStraitant($slug)
    {
        $this->getProgramExist();
        $check = AvancementBrouillon::where('id',$slug)->first();
        if($check){
            $check->delete();
            return redirect()->back()->with('success','Taux d\'avancement a bien été supprimé');
        }
        return redirect()->back();
    }

    public function storeSavePayeStraitant($slug,$idcontrat)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $gets = AvancementBrouillon::where([['programme_id',$idprogram],['soustraitant_id',$slug],['soustraitantcontrat_id',$idcontrat]])->with('corpsetat')->get();
        //dd($gets);
        if(count($gets)>0){

            $trans = new Transactiontest();
            $trans->slug = $this->getNumTrans($idprogram);
            $trans->programme_id = $idprogram;
            $trans->soustraitant_id = $gets[0]->soustraitant_id;
            $trans->soustraitantcontrat_id = $idcontrat;
            $trans->lot_id = $gets[0]->lot_id;
            $trans->taux = 0;
            $trans->montant = 0;
            $trans->user_id = Auth::user()->id;
            $trans->save();

            $ttaux = 0;
            $ttmt = 0;
            $idtrans = $trans->id ;

            foreach ($gets as $get){
                $check = Avancementest::where([
                    ['programme_id',$idprogram],
                    ['lot_id',$get->lot_id],
                    ['soustraitant_id',$get->soustraitant_id],
                    ['corpsetat_id',$get->corpsetat_id],
                    ['soustraitantcontrat_id',$idcontrat]
                    ])
                    ->with('corpsetat')
                    ->orderBy('id','desc')
                    ->first();

                if($check){
                    $taux_exe = $get->taux - $check->taux ;
                }else{
                    $taux_exe = $get->taux;
                }
                $ttaux = $ttaux +$taux_exe;

                if($get->corpsetat){
                    $cout = ($taux_exe/100) * $get->corpsetat->prix ;
                }else{
                    $cout = 0;
                }
                $ttmt = $ttmt +$cout;
                //dd($cout);
                //get montant corps etat

                $new = new Avancementest();
                $new->programme_id = $idprogram;
                $new->lot_id = $get->lot_id;
                $new->soustraitant_id = $get->soustraitant_id;
                $new->corpsetat_id = $get->corpsetat_id;
                $trans->soustraitantcontrat_id = $idcontrat;
                $new->taux = $get->taux;
                $new->taux_execute = $taux_exe;
                $new->cout = $cout;
                $new->transac_id = $idtrans;
                $new->user_id = Auth::user()->id;
                $new->save() ;

                //delete brouillon
                $get->delete();
            }

            $trans = Transactiontest::where('id',$idtrans)->first();
            $trans->taux = $ttaux;
            $trans->montant = $ttmt;
            $trans->save();

            $ttmt = number_format($ttmt, 0, '', ' ');
            //SEND NOTIFICATION CHEF PROJET FOR CREATE PAIEMENT STRAITANT
            $pgg = Programme::where('id',$idprogram)->first();
            $namst = $trans->soustraitant ? $trans->soustraitant->nom : 'N/A';
            $namstct = $trans->contrat ? $trans->contrat->slug : 'N/A';

            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez une nouvelle transaction d'un sous-taitant en attente de traitement</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "TRANSACTION EN ATTENTE DE TRAITEMENT - GESTION DE STOCK - ".$pgg->libelle;

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('chefchantier.st.paye',$idcontrat)->with('success','Les Taux d\'avancement ont bien été enregistrés');

        }else{
            return redirect()->back();
        }

    }

    public function storeSavePayeStraitant2(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $idcontrat = $request->idcontrat;
        $slug = $request->slug;
        $gets = AvancementBrouillon::where([['programme_id',$idprogram],['soustraitant_id',$slug],['soustraitantcontrat_id',$idcontrat]])->with('corpsetat')->with('programme')->get();
        //dd($gets);
        if(count($gets)>0){

            $trans = new Transactiontest();
            $trans->slug = $this->getNumTrans($idprogram);
            $trans->programme_id = $idprogram;
            $trans->soustraitant_id = $gets[0]->soustraitant_id;
            $trans->soustraitantcontrat_id = $idcontrat;
            $trans->lot_id = $gets[0]->lot_id;
            $trans->taux = 0;
            $trans->montant = 0;
            $trans->user_id = Auth::user()->id;
            $trans->save();

            $ttaux = 0;
            $ttmt = 0;
            $idtrans = $trans->id ;

            foreach ($gets as $get){
                //connaitre le type de rattachement du programme
                $price = 0;
                //dd($get);
                if($get->programme->type_rattachement == 1){
                    $price = $get->corpsetat->prix;
                }else{
                    //first montant to soustraitant contrat montant
                    $ctmt = ProgrammeSoustraitantcontratMontant::where([
                        ['programme_id',$idprogram],
                        ['soustraitant_id',$get->soustraitant_id],
                        ['soustraitantcontrat_id',$idcontrat],
                        ['corpsetats_id',$get->corpsetat_id],
                        ['lot_id',$get->lot_id]
                        ])->first();

                    if($ctmt){
                        $price = $ctmt->montant;
                    }
                }

                //dd($price);
                $check = Avancementest::where([
                    ['programme_id',$idprogram],
                    ['lot_id',$get->lot_id],
                    ['soustraitant_id',$get->soustraitant_id],
                    ['corpsetat_id',$get->corpsetat_id],
                    // ['soustraitantcontrat_id',$idcontrat]
                    ])
                    ->with('corpsetat')
                    ->with('transactiontest')
                    ->whereHas('transactiontest',function($q){
                        $q->where('etat',1);
                    })
                    ->orderBy('id','desc')
                    ->first();

                if($check){
                    $taux_exe = $get->taux - $check->taux ;
                }else{
                    $taux_exe = $get->taux;
                }

                $old = Avancement::where([['programme_id',$idprogram],['lot_id',$get->lot_id],['corpsetat_id',$get->corpsetat_id]])->orderBy('id','desc')->first();
                if($old){
                    $taux_exe = $get->taux - $old->taux ;
                }

                $ttaux = $ttaux +$taux_exe;

                if($get->corpsetat){
                    $cout = ($taux_exe/100) * $price ;
                }else{
                    $cout = 0;
                }
                $ttmt = $ttmt +$cout;
                //dd($cout);
                //get montant corps etat
                $new = new Avancementest();
                $new->programme_id = $idprogram;
                $new->lot_id = $get->lot_id;
                $new->soustraitant_id = $get->soustraitant_id;
                $new->corpsetat_id = $get->corpsetat_id;
                $trans->soustraitantcontrat_id = $idcontrat;
                $new->taux = $get->taux;
                $new->taux_execute = $taux_exe;
                $new->cout = $cout;
                $new->transac_id = $idtrans;
                $new->user_id = Auth::user()->id;
                $new->save() ;


                //delete brouillon
                $get->delete();
            }

            $trans = Transactiontest::where('id',$idtrans)->first();
            $trans->taux = $ttaux;
            $trans->montant = $ttmt;
            if($request->file('rattachment')){
                $file = $request->file('rattachment');
                $extension = $file->getClientOriginalExtension() ?: 'pdf';
                $folderName = 'assets/uploads/rattachment/';
                $picture = Str::random(6).'.'. $extension;
                $file->move($folderName,$picture);
                $trans->rattachment = $picture;
            }
            $trans->save();

            $ttmt = number_format($ttmt, 0, '', ' ');
            //SEND NOTIFICATION CHEF PROJET FOR CREATE PAIEMENT STRAITANT
            $pgg = Programme::where('id',$idprogram)->first();
            $namst = $trans->soustraitant ? $trans->soustraitant->nom : 'N/A';
            $namstct = $trans->contrat ? $trans->contrat->slug : 'N/A';

            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez une nouvelle transaction d'un sous-taitant en attente de traitement</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "TRANSACTION EN ATTENTE DE TRAITEMENT - GESTION DE STOCK - ".$pgg->libelle;

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('chefchantier.st.paye',$idcontrat)->with('success','Les Taux d\'avancement ont bien été enregistrés');

        }else{
            return redirect()->back();
        }

    }

    public function transactions()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $transactions = Transactiontest::where('programme_id',$idprogram)->where('etat',1)->with('lot')->with('soustraitant')->with('contrat')->orderBy('updated_at','desc')->get();
        return view('chefchantier.transactions',compact('transactions'));
    }

    public function transactionsReject()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $transactions = Transactiontest::where('programme_id',$idprogram)->where('etat',0)->with('lot')->with('soustraitant')->with('contrat')->orderBy('updated_at','desc')->get();
        return view('chefchantier.transactions_reject',compact('transactions'));
    }

    public function transactionsValid()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $transactions = Transaction::where('programme_id',$idprogram)->with('lot')->with('soustraitant')->with('contrat')->orderBy('updated_at','desc')->get();
        return view('chefchantier.transactions_valid',compact('transactions'));
    }

    public function transactionShow($id)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $slug = decrypt($id);
        $trans = Transactiontest::where('id',$slug)->where('programme_id',$idprogram)->with('soustraitant')->with('contrat')->first();
        $lists = Avancementest::where('transac_id',$trans ? $trans->id : 0)->with('corpsetat')->with('lot')->with('soustraitant')->get();

        if(!isset($trans->id)){
            //non valider
            $trans = Transaction::where('id',$slug)->where('programme_id',$idprogram)->with('soustraitant')->with('contrat')->first();
            $lists = Avancement::where('transac_id',$trans->id)->with('corpsetat')->with('lot')->with('soustraitant')->get();
            //dd($trans);
        }
        return view('chefchantier.transactions_show',compact('trans','lists'));
    }

    public function transactionItem($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $trans = Transaction::where('programme_id',$idprogram)->where('id',$id)->with('lot')->with('soustraitant')->first();
        if($trans){
            $listrans = Avancement::where('programme_id',$idprogram)->where('lot_id',$trans->lot_id)->where('soustraitant_id',$trans->soustraitant_id)->whereDate('created_at',$trans->created_at->format('Y-m-d'))->with('lot')->with('soustraitant')->with('corpsetat')->get();
            return view('chefchantier.transactions_valid_show',compact('listrans','trans'));
        }
        return redirect()->back();
    }

    public function transactionsDel($id)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $slug = decrypt($id);
        $trans = Transactiontest::where('id',$slug)->where('programme_id',$idprogram)->first();

        if($trans){
            $old = Avancementest::where('transac_id',$trans->id)->delete();
            $trans->delete();
            return redirect()->back()->with('success','La transaction a bien été supprimé');
        }

        return redirect()->back();
    }

    public function getByStatusTransactions($status)
    {
        try{
            $idprogram = $this->getIdProgram();
            if($status == 2){
                return Transaction::where('programme_id',$idprogram)->orderBy('id','desc')->get();
            }
            return Transactiontest::where('etat',$status)->where('programme_id',$idprogram)->get();
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    //CE BY CONTRAT
    public function contratStraitantCe($idcontrat)
    {
        //dd($idcontrat);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $contrat = ProgrammeSoustraitantcontrat::where('programme_id',$idprogram)->where('id',$idcontrat)->has('straitant')->with('straitant')->with('lots')->first();
        //dd($contrat);
        if($contrat){
            $lotid = $contrat->lots ? $contrat->lots->pluck('lot_id') : [];
            //get type lot
            $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lotid)->groupBy('actif_id')->with('actifs')->get();
            // dd($actifs,$contrat);

            return view('chefchantier.sous_traitant.new_contrats_show',compact('contrat','actifs'));
        }

        return redirect()->back()->with('error',"Vous n'avez pas de droit à cette page");
    }


    //ETATS
    public function etats(){
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        $listlots = ProgrammeLot::where('programme_id',$idprogram)->with('actifs')->orderByRaw('ABS(lot)', 'asc')->get();
        $listlots->map(function($item) use($idprogram){
            if($item->programme->type_rattachement == 1){
                $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            }else{
                //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
                $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$item->id)->has('straitantcontrat')->sum('montant');
            }
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });

        //dd($blocks,$tranches,$soustraitants);
        return view('chefchantier.etats.index',compact('blocks','tranches','soustraitants','listlots'));
    }

    public function etatsItem($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $listlot = ProgrammeLot::where('programme_id',$idprogram)->where('id',$slug)->with('actifs')->first();
        if($listlot->programme->type_rattachement == 1){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$listlot->actif_id)->sum('prix');
        }else{
            //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
            $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$listlot->id)->has('straitantcontrat')->sum('montant');
        }

        $idtranspay = Transaction::where('programme_id',$idprogram)->where('status',2)->pluck('id')->toArray();
        $montbuy = Avancement::where('lot_id',$listlot->id)->whereIn('transac_id',$idtranspay)->sum('cout');
        //dump($montantLot,$montbuy);
        $stats =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            $data= Avancement::where('lot_id',$listlot->id)->whereYear('created_at',date('Y'))->whereMonth('created_at',$month)->where('programme_id',$idprogram)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
            $stats[$month] =$data->coutpaye;
        }
        //dd($listlot);
        return view('chefchantier.etats.show',compact('listlot','montantLot','montbuy','stats'));
    }

    public function etatsSearch(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $pg = ProgrammeLot::where('programme_id',$idprogram);

        if(isset($request->soustraitant)){

            $pgst = LotSoustraitant::where('soustraitant_id',$request->soustraitant)->pluck('lot_id')->toArray();
            $pg = ProgrammeLot::whereIn('id',$pgst)->with('actifs');

            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }

        }else{
            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }
        }

        $listlots = $pg->get();
        $listlots->map(function($item) use($idprogram){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });
        //dd($listlots);
        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        return view('chefchantier.etats.index',compact('blocks','tranches','soustraitants','listlots'));
    }
}
