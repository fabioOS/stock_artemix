<?php

namespace App\Http\Controllers\Chefprojet;

use App\Bon_demande_item;
use App\Bon_retour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\Stock;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BackstockController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function getProgramExist(){
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram(){
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getStatus($status){
        $nb = Bon_retour::where('status',$status)->count();
        if(!Auth::user()->role_id==3){
            return 0 ;
        }
        return $nb ;
    }

    public function index() {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $lists = Bon_retour::with('demande')->with('user')->where('programme_id',$programme_id)->where('status',1)->orderBy('id','desc')->get();
        return view('chef_projet.backstock.index' , compact('lists'));
    }

    public function show($id) {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $bon = Bon_retour::with('demande')->with('user')->where('programme_id',$programme_id)->findOrFail($id);
        return view('chef_projet.backstock.show' , compact('bon'));
    }

    public function valid($id) {
        //dd($id);
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $bon = Bon_retour::with('items')->where('programme_id',$programme_id)->with('demande')->findOrFail($id);

        //Reduit les quantités du bon de demande et ajoute les quantités au stock
        $items = $bon->items ;
        foreach ($items as $item) {
            $itemdemande = Bon_demande_item::where('id',$item->bon_demande_item_id)->where('produit_id',$item->produit_id)->first();

            if($itemdemande->qte < $item->qte){
                return redirect()->route('cphome.back.index')->with('error' ,'Le bon de retour n°'.$bon->ref.' n\'a pas pu être validé !');
                break ;
            }
            $itemdemande->qte = $itemdemande->qte - $item->qte ;
            $itemdemande->save();

            //Ajout au stock
            $itemdemande->produit->qte_stock = $itemdemande->produit->qte_stock + $item->qte ;
            $stocks = Stock::where('produit_id', $item->produit_id)->where('programme_id', $programme_id)->first();
            if($stocks) {
                $new_qte = $stocks->qte + $item->qte;
                $stocks->update(['qte' => $new_qte]);
            }
        }

        $bon->status = 2 ;
        $bon->save();

        //NOTIFICATION USER
        $pgg = Programme::where('id',$programme_id)->first();
        $message = "<p>Bonjour Monsieur/Madame </p><p>Votre demande de retour en stock n°<b>". $bon->ref."</b> a été validée. ";
        $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."</p>";
        $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
        $sujet = "BON DE RETOUR EN STOCK VALIDE - G-STOCK";

        $user = User::where('id',$bon->user_id)->pluck('email')->toArray();
        $user = array_merge($user,User::where('id',$bon->demande->user_id)->pluck('email')->toArray());
        $this->sendMailNotificaton($message,$sujet,$user);

        return redirect()->route('cphome.back.index')->with('success' ,'Le bon de retour a bien été validé !');
    }

    public function reject(Request $request) {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Validation
        $request->validate([
            'bonid' => 'required',
            'motif' => 'required|string',
        ],[
            'bonid.required' => 'Erreur de validation',
            'motif.required' => 'Erreur de validation',
        ]);

        $id = decrypt($request->bonid);

        $bon = Bon_retour::where('programme_id',$programme_id)->with('demande')->findOrFail($id);
        $bon->status = 4 ;
        $bon->motif = $request->motif ;
        //$bon->save();

        $pgg = Programme::where('id',$programme_id)->first();
        $message = "<p>Bonjour Monsieur/Madame </p><p>Votre demande de retour en stock n°<b>". $bon->ref."</b> a été rejetée. ";
        $message .= "<br> Veuillez trouver ci-dessous le motif de rejet : </p><p>“ ".$bon->motif." ”</p>";
        $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."</p>";
        $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
        $sujet = "BON DE RETOUR EN STOCK REJETEE - G-STOCK";

        $user = User::where('id',$bon->user_id)->pluck('email')->toArray();
        $user = array_merge($user,User::where('id',$bon->demande->user_id)->pluck('email')->toArray());
        //dd($user);
        $this->sendMailNotificaton($message,$sujet,$user);

        return redirect()->route('cphome.back.index')->with('success' ,'Le bon de retour a bien été rejeté !');

    }
}
