<?php

namespace App\Http\Controllers\Chefprojet;

use App\BC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;

class BonCommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getByStatus($status)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',$status)->count();
        return $bcs;
    }

    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);
        $demande = BC::where('id',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();

        return view('chef_projet.boncommande.show',compact('demande'));

    }

    public function showPdf($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($slug);
        $data = BC::where('programme_id',$programme_id)->where('ref',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        //dd($data);

        $pdf = PDF::loadView('service_achat.bc.bc_download',compact('data'));
        //return view('service_achat.bc.bc_download',compact('data'));
        return $pdf->download('bon-commande-'.$id.'.pdf');

    }

    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',1)->with('demande')->with('fournisseur')->with('user')->with('items')->orderBy('id','desc')->get();

        return view("chef_projet.boncommande.waiting",compact('bcs'));
    }

    public function valider()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',2)->with('demande')->with('fournisseur')->with('user')->with('items')->orderBy('id','desc')->get();

        return view('chef_projet.boncommande.valider',compact('bcs'));
    }

    public function rejeter(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'bonid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $data = BC::where('programme_id',$programme_id)->where('ref',$request->bonid)->where('status',1)->first();
            //dd($data);
            if($data){
                $data->status = 4;
                $data->motif = $request->motif;
                $data->save();

                $message = "<p>Bonjour Monsieur/Madame </p><p>Votre bon de commande été rejeté pour motif : </p><p>".$request->motif."</p>";
                $sujet = "BON DE COMMANDE REJETE - GESTION DE STOCK";

                $user = User::where('id',$data->user_id)->pluck('email')->toArray();
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cpcommande.waiting')->with('success' ,'Le bon de commande a bien été rejetée.');

            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }
    }

    public function valid($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($slug);
        //dd($id);
        $demandes = BC::where('id',$id)->where('programme_id',$programme_id)->first();
        if($demandes){
            $demandes->status=2;
            $demandes->save();

            $message = "<p>Bonjour Monsieur/Madame </p><p>Votre bon de commande ".$demandes->ref." a été validé.</p>";
            $sujet = "BON DE COMMANDE VALIDE - GESTION DE STOCK";

            $user = User::where('id',$demandes->user_id)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('cpbc.waiting')->with('success','Le bon de commande a bien été validée.');
        }

        return redirect()->back()->with('erro','Désolé ! Nous avons rencontré un souci.');

    }
}
