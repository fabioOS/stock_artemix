<?php

namespace App\Http\Controllers\Chefprojet;

use App\BCitem;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Commande;
use App\CommandeItem;
use App\Http\Controllers\BonController;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LigneBudgetaireController;
use App\Produit;
use App\Programme;
use App\ProgrammeLigneBudgetaire;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }


    public function getByStatus($status)
    {
        $programme_id = $this->getIdProgram();
        Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;
        $userpivot = $this->getTypeBatiment(Auth::user()->id,$programme_id);
        $lignids = ProgrammeLigneBudgetaire::whereIn('type',$userpivot)->where('programme_id',$programme_id)->pluck('id')->toArray();

        $comande1 = Commande::where('programme_id',$programme_id)->where('nivalid',$niv)->where('status',$status)->whereIn('ligne_id',$lignids)->count();
        // $comande2 = Commande::where('programme_id',$programme_id)->where('nivalid',$niv)->where('status',$status)->count();
        $comande2 = 0;
        return $comande1 + $comande2;
    }


    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);
        $demande = Commande::where('id',$id)->with('produits')->with('user')->with('lignebudget')->first();
        $demande->datalignebudget = null;
        if($demande->lignebudget){
            $demande->datalignebudget = (new LigneBudgetaireController)->getConsoLignBudget($demande->lignebudget->id);
        }
        $demande->actionbtn = false;
            //show action
            if($demande->nivalid == 1){
                Auth::user()->role_id==3 ? $demande->actionbtn = true : $demande->actionbtn = false;
            }else{
                Auth::user()->role_id==5 ? $demande->actionbtn = true : $demande->actionbtn = false;
            }

        return view('chef_projet.commandes.show',compact('demande'));

    }

    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;

        $userpivot = $this->getTypeBatiment(Auth::user()->id,$programme_id);
        $lignids = ProgrammeLigneBudgetaire::whereIn('type',$userpivot)->where('programme_id',$programme_id)->pluck('id')->toArray();
        $demandes1 = Commande::where('status',1)->where('nivalid',$niv)->where('programme_id',$programme_id)->whereIn('ligne_id',$lignids)->with('produits')->with('user')->orderBy('updated_at','desc')->get();
        //$demandes2 = Commande::where('status',1)->where('nivalid',$niv)->where('programme_id',$programme_id)->with('produits')->with('user')->orderBy('updated_at','desc')->get();
        $demandes2= [];
        $demandes = $demandes1->merge($demandes2);
        // dd($lignids,$demandes);

        return view('chef_projet.commandes.waiting',compact('demandes'));
    }

    public function valider()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',2)->where('programme_id',$programme_id)->with('produits')->with('user')->orderBy('updated_at','desc')->get();
        $demandes->map(function ($q) {
            // Récupérer toutes les bon de commande associées à ce cette demande
            $idsItem = $q->produits->pluck('id');
            $bcitems = BCitem::whereIn('commande_item_id', $idsItem)->sum('qte');
            $q->qtecmde = $bcitems;
        });

        return view('chef_projet.commandes.valid',compact('demandes'));
    }

    public function rejetees()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',4)->where('programme_id',$programme_id)->with('produits')->with('user')->orderBy('updated_at','desc')->get();

        return view('chef_projet.commandes.rejeter',compact('demandes'));
    }

    public function rejeter(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'bonid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $data = Commande::where('programme_id', $programme_id)->where('ref',$request->bonid)->where('status',1)->first();
            //dd($data);
            if($data){
                $data->status = 4;
                $data->motif = $request->motif;
                $data->save();

                $pgg = Programme::where('id',$programme_id)->first();

                $message = "<p>Bonjour Monsieur/Madame </p><p>Votre commande n°<b>". $data->ref."</b> a été rejetée.";
                $message .= "<br> Veuillez trouver ci-dessous le motif de rejet : </p><p>“ ".$request->motif." ”</p>";
                $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$data->ref."<br><b>NOMBRE DE MATIERE DEMANDEE : </b>".count($data->produits)."</p>";
                $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
                $sujet = "COMMANDE REJETEE - ".$pgg->libelle." - GSTOCK";

                $user = User::where('id',$data->user_id)->pluck('email')->toArray();
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cpcommande.waiting')->with('success' ,'La demande de commande a bien été rejetée.');

            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }
    }

    public function valid($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($slug);
        //dd($id);
        $demandes = Commande::where('id',$id)->where('programme_id',$programme_id)->first();
        if($demandes){
            $pgg = Programme::where('id',$programme_id)->first();
            if(Auth::user()->role_id == 3){
                $demandes->nivalid=2;
                $demandes->save();

                //NOTIFIER LE DO QUE LA DEMANDE EST EN ATTENTE
                $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Une nouvelle commande en attente de traitement.</p>";
                $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$demandes->ref."<br><b>NOMBRE DE MATIERE DEMANDEE : </b>".count($demandes->produits)."</p>";
                $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
                $sujet = "NOUVELLE COMMANDE EN ATTENTE - ".$pgg->libelle." - G-STOCK";

                $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
                $user = User::where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
                $this->sendMailNotificaton($mess,$sujet,$user);

            }elseif (Auth::user()->role_id == 5) {

                $demandes->status=2;
                $demandes->save();

                //NOTIFIER LE SERVICE ACHAT QUE LA DEMANDE EST VALIDE
                $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Une nouvelle demande d'approvisionnement du stock vient d'être validée.</p>";
                $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$demandes->ref."<br><b>NOMBRE DE MATIERE DEMANDEE : </b>".count($demandes->produits)."</p>";
                $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

                $sujet = "COMMANDE VALIDEE - ".$pgg->libelle." - G-STOCK";

                $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
                $user = User::where('role_id',6)->whereIn('id',$pguser)->pluck('email')->toArray();
                $this->sendMailNotificaton($mess,$sujet,$user);

            }else{
                return redirect()->back()->with('error',"Désolé ! Nous avons rencontré un souci.");
            }

            return redirect()->route('cpcommande.waiting')->with('success','La demande de commande a bien été validée.');
        }

        return redirect()->back()->with('erro','Désolé ! Nous avons rencontré un souci.');

    }
}
