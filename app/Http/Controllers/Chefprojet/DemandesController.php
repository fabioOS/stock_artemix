<?php

namespace App\Http\Controllers\Chefprojet;

use App\BDSpec;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;

class DemandesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getByStatus($var)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',$var)->where('genere',0)->count();

        return $demandes;
    }

    public function create()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        return view('chef_projet.demandes.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        Validator::make($request->all(),[
            'titre' =>'required',
            'montant' =>'required',
            'des' =>'required',
        ])->validate();

        try {

            $newbds = new BDSpec();
            $newbds->slug = $this->getRefDS();
            $newbds->programme_id = $programme_id;
            $newbds->titre = $request->titre;
            $newbds->montant = str_replace(' ', '', $request->montant);
            $newbds->description = $request->des;
            $newbds->user_id = Auth::user()->id;
            $newbds->save();

            //NOTIFIER LE SA
            $pgg = Programme::where('id',$programme_id)->first();
            $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Une demande spéciale est en attente de traitement.</p>";
            $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$newbds->slug."<br>";
            $mess .= "<b>AGENT : </b>".Auth::user()->name."<br>";
            $mess .= "<b>MONTANT : </b>".$request->montant." FCFA<br><b>MOTIF : </b>".nl2br($request->des)."</p>";
            $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "DEMANDE SPECIALE EN ATTENTE - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',6)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($mess,$sujet,$user);

            return redirect()->route('cpdemande.waiting')->with('success','Votre demande a bien été envoyée.');
        }catch(\Exception $e){
            return back()->with('error', $e->getMessage());
        }

    }


    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        if(Auth::user()->role_id==3){
            $demandes = BDSpec::where('programme_id',$programme_id)->where('status',1)->where('user_id',Auth::user()->id)->where('status',1)->orderBy('updated_at','desc')->get();
        }else{
            $demandes = BDSpec::where('programme_id',$programme_id)->where('status',1)->where('genere',0)->orderBy('id','desc')->get();
        }
        //dd($demandes);
        return view('chef_projet.demandes.waiting',compact('demandes'));
    }

    public function valider()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        if(Auth::user()->role_id==3){
            $demandes = BDSpec::where('programme_id',$programme_id)->where('status',2)->where('user_id',Auth::user()->id)->where('genere',1)->orderBy('updated_at','desc')->get();
        }else{
            $demandes = BDSpec::where('programme_id',$programme_id)->where('status',2)->orderBy('id','desc')->get();
        }
        return view('chef_projet.demandes.valider',compact('demandes'));
    }

    public function rejet()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',4)->where('user_id',Auth::user()->id)->orderBy('updated_at','desc')->get();

        return view('chef_projet.demandes.reject',compact('demandes'));
    }

    public function traiter()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        if(Auth::user()->role_id==5){
            $demandes = BDSpec::where('programme_id',$programme_id)->where('status',3)->where('genere',1)->orderBy('updated_at','desc')->get();
        }else{
            return redirect()->back();
        }
        //dd($demandes);
        return view('chef_projet.demandes.traiter',compact('demandes'));
    }

    public function rejeter(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'bonid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $data = BDSpec::where('programme_id',$programme_id)->where('slug',$request->bonid)->where('status',1)->first();
            //dd($data);
            if($data){
                $data->status = 4;
                $data->motif = $request->motif;
                $data->save();

                $pgg = Programme::where('id',$programme_id)->first();
                $message = "<p>Bonjour Monsieur/Madame </p><p>Votre demande n°<b>". $data->slug."</b> (".$data->titre.") a été rejetée. ";
                $message .= "<br> Veuillez trouver ci-dessous le motif de rejet : </p><p>“ ".$data->motif." ”</p>";
                $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."</p>";
                $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
                $sujet = "DEMANDE SPECIALE REJETEE - G-STOCK";

                $user = User::where('id',$data->user_id)->pluck('email')->toArray();
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cpdemande.waiting')->with('success' ,'La demande a bien été rejetée.');

            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }
    }

    public function valid($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($slug);
        //dd($id);
        $demandes = BDSpec::where('id',$id)->where('programme_id',$programme_id)->first();
        if($demandes){
            $demandes->genere=0;
            $demandes->status=2;
            $demandes->save();

            $mtt = number_format(str_replace(' ', '', $demandes->montant), 0, '', ' ');
            //NOTIIFER LE SA
            $pgg = Programme::where('id',$programme_id)->first();
            $message = "<p>Bonjour Monsieur/Madame </p><p>La demande spéciale n°<b>". $demandes->slug."</b> (".$demandes->titre.") a été validée par le DO. Nous vous prions de bien vouloir faire le nécessaire pour la suite.</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br>";
            $message .= "<b>MONTANT : </b>".$mtt." FCFA</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "DEMANDE SPECIALE VALIDEE - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',6)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('cpdemande.waiting')->with('success','Le bon de commande a bien été validé.');
        }

        return redirect()->back()->with('error','Désolé ! Nous avons rencontré un souci.');
    }

    public function pdf($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $data = BDSpec::where('programme_id',$programme_id)->where('id',$id)->first();
        //dd($data);
        $pdf = PDF::loadView('service_achat.demandes.pdf',compact('data'));
        //$pdf->setPaper('a4', 'landscape');
        //return view('service_achat.demandes.pdf',compact('data'));
        //return $pdf->stream('etat_gestion_stock-'.date("d-m-y").'.pdf');
        return $pdf->download('demande-speciale-'.$data->slug.'.pdf');
    }
}
