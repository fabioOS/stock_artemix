<?php

namespace App\Http\Controllers\Chefprojet;

use App\Avancement;
use App\AvancementBrouillon;
use App\Avancementest;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Deleted_bon_demande;
use App\Deleted_bon_demande_historique;
use App\Deleted_bon_demande_item;
use App\LotSoustraitant;
use App\LotSoustraitantCorpsetat;
use App\Produit;
use App\Programme;
use App\ProgrammeActif;
use App\ProgrammeCorpsetat;
use App\ProgrammeFournisseur;
use App\ProgrammeLot;
use App\ProgrammePivotUser;
use App\ProgrammeSoustraitant;
use App\Stock;
use App\Transaction;
use App\Transactiontest;
use App\Typeproduit;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProgrammeSouscorpsetatLot;
use App\ProgrammeSoustraitantcontrat;
use App\ProgrammeSoustraitantcontratMontant;
use App\ProgrammeSoustraitantcontratTampon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PDF;

class HomeController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function listpgram()
    {
        Session::forget(['program']);

        $user = User::where('id',Auth::id())->with('programmes')->first();
        if(count($user->programmes)==1){
            return redirect()->route('cpprogram.item',$user->programmes[0]->slug);
        }
        return view('chef_projet.listpgram',compact('user'));
    }

    public function itempgram($slug)
    {
        //dd($slug);
        $program = Programme::where('slug',$slug)->first();
        if(isset($program->id)){
            $check = ProgrammePivotUser::where('user_id',Auth::id())->where('programme_id',$program->id)->first();

            if($check){
                Session::put('program', $program->id);
                return redirect()->route('cphome');
            }

            return redirect()->back()->with('error','Vous n\'avez pas doit à ce programme');
        }

        return redirect()->back();
    }

    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //$lists = Avancement::where('programme_id',$idprogram)->select('lot_id','soustraitant_id',DB::raw('round(SUM(cout)) as coutt'))->groupBy('lot_id')->groupBy('soustraitant_id')->with('lot')->with('soustraitant')->get();
        $lists = Transaction::where('programme_id',$idprogram)->with('lot')->with('soustraitant')->orderBy('id','desc')->take(10)->get();

        $stats =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            $data= Avancement::whereYear('created_at',date('Y'))->whereMonth('created_at',$month)->where('programme_id',$idprogram)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
            $stats[$month] =$data->coutpaye;
        }
        //dd($stats);
        //dd($stats,array_values($stats));

        return view('chef_projet.index',compact('lists','stats'));
    }

    public function stock()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $stocks = Stock::where('programme_id',$idprogram)->with('produits')->get();
        //dd($stocks);
        $stocks->map(function($item){
            if (isset($item->produits)){
                $type=Typeproduit::find($item->produits->type_id);
                $item->produits->libelle_type_pdt = $type->libelle;
            }else{
                $item->produits->libelle_type_pdt = '-';
            }
        });
        //dd($stocks);

        return view('chef_projet.stock',compact('stocks'));
    }

    public function stockBudget()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $produits = Produit::orderBy('libelle','asc')->with('type')->get();
       //dd($produits);
        return view('chef_projet.stock_budget',compact('produits'));
    }

    // BON DE COMMANDE

    public function createDemande()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $soutraitants = ProgrammeSoustraitant::with(['lots' =>function($query) {
            $query->with('lot');
        }])
        ->where('programme_id', $idprogram)
        ->get();
        return view('chef_projet.bc.bc_create_step1', compact('soutraitants'));
    }

    public function getContratSt($slug)
    {
        return $contrats = ProgrammeSoustraitantcontrat::where('soustraitant_id',$slug)->where('etat',2)->get();
    }

    public function step2createDemande(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $contrat = $request->contrat;
        $soustraitant = ProgrammeSoustraitant::with(['lots' =>function($query) use($contrat){
            $query->with('lot');
            $query->where('soustraitantcontrat_id',$contrat);
        }])
        ->where('id', $request->soustraitant_id)
        ->where('programme_id', $programme_id)
        ->first();
        $ctt = ProgrammeSoustraitantcontrat::where('id',$contrat)->where('programme_id',$programme_id)->first();

        $soustraitant->itemcontrat = $ctt;
        //dd($soustraitant);

        return view('chef_projet.bc.bc_create_step2', compact('soustraitant'));
    }

    public function step3createDemande(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $lot_id = $request->lot_id;
        $contrat = $request->contrat_id;
        $vars = [
            'programme_id' => $programme_id,
            'lot_id' => $lot_id,
            'soustraitant_id' => $request->soustraitant_id,
            'contrat_id' => $request->contrat_id,
        ];

        $soustraitant = ProgrammeSoustraitant::with(['lots' =>function($query) use($lot_id,$contrat) {
            $query->whereIn('lot_id', $lot_id)->with('lot');
            $query->where('soustraitantcontrat_id',$contrat);
        }])
        ->where('id', $request->soustraitant_id)
        ->where('programme_id', $programme_id)
        ->first();

        $ctt = ProgrammeSoustraitantcontrat::where('id',$contrat)->where('programme_id',$programme_id)->first();
        $soustraitant->itemcontrat = $ctt;

        return view('chef_projet.bc.bc_create_step3', compact('soustraitant', 'vars'));
    }

    public function getRefBss()
    {
        $code = "BS".date('ym');
        $count = Bon_demande::count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$num;

        return $val;
    }

    public function storeDemande(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        // save bon de demande
        $demande = new Bon_demande();
        $demande->status = 1;
        $demande->programme_id = $programme_id;
        $demande->soustraitant_id = $request->soustraitant_id;
        $demande->user_id = Auth::user()->id;
        $demande->ref = $this->getRefBss();

        if($request->file('demandefile')){
            $file = $request->file('demandefile');
            $extension = $file->getClientOriginalExtension() ?: 'pdf';
            $folderName = 'assets/uploads/bondemandes/';
            $document = Str::random(6).'.'. $extension;
            $file->move($folderName,$document);
            $demande->file = $document;
        }
        else
            $demande->file = '';

        $demande->contrat_id = $request->contrat_id;
        $demande->save();

        //save items bon de demande
        $i=0;
        foreach($request->lot_id as $lot_id) {
            if(isset($request->corps_id[$lot_id]) && count($request->corps_id[$lot_id]) > 0 ) {
                foreach( $request->corps_id[$lot_id] as $corps_id) {
                    if(isset($request->sous_corps_id[$lot_id][$corps_id]) && count($request->sous_corps_id[$lot_id][$corps_id]) > 0) {
                        foreach( $request->sous_corps_id[$lot_id][$corps_id] as $key=>$sous_corps_id) {
                            if( isset($request->produit_id[$lot_id][$corps_id][$key]) && $request->produit_id[$lot_id][$corps_id][$key] != null ) {
                                //dd($request->qte_sscorps[$lot_id][$corps_id] );
                                if( isset($request->qte_sscorps[$lot_id][$corps_id][$key]) && $request->qte_sscorps[$lot_id][$corps_id][$key] != null ) {
                                    $produit_id = $request->produit_id[$lot_id][$corps_id][$key];
                                    $qte        = $request->qte_sscorps[$lot_id][$corps_id][$key];
                                    $check = ProgrammeSouscorpsetatLot::where('corpsetat_id',$corps_id)->where('produit_id',$produit_id)->where('lot_id',$lot_id)->first();
                                    if($check){
                                        Bon_demande_item::create([
                                            'bon_demande_id' => $demande->id,
                                            'soustraitant_id' => $request->soustraitant_id,
                                            'corpsetats_id' => $check->corpsetat_id,
                                            'scorpsetats_id' => $sous_corps_id,
                                            'lot_id' => $lot_id,
                                            'produit_id' => $produit_id,
                                            'qte' => $qte
                                        ]);
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //dd($i);
        if($i != 0){
            $pgg = Programme::where('id',$programme_id)->first();
            $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Un nouveau bon de sortie est en attente de traitement.</p>";
            $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$demande->ref."<br><b>NOMBRE DE PRODUIT DEMANDE : </b>".$i."</p>";
            $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "BON DE SORTIE DE STOCK EN ATTENTE DE TRAITEMENT - ".$pgg->libelle." - G-STOCK";;
            //send le mail au magasinier
            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',4)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($mess,$sujet,$user);

            return redirect()->route('cphome.bc.create')->with('success' ,'Bon de sortie ajouter avec succès !');

        }else{
            //supprimer les demandes creer
            $iddmd = $demande->id;
            $demande = Bon_demande::where('id',$iddmd)->delete();

            return back()->with('error' ,'Aucune données n\'a été enregistrée !');
        }
    }

    public function editDemande($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        try{
            $id = decrypt($id);
            $bon = Bon_demande::with('bon_demanbde_items')->with('soustraitant')->with('contrat')->with('user')->where('ref',$id)->first();
            $lot_id = Bon_demande_item::where('bon_demande_id', $bon->id)->selectRaw('DISTINCT(lot_id)')->pluck('lot_id');

            $contrat = $bon->contrat_id;
            $vars = [
                'programme_id' => $bon->programme_id,
                'lot_id' => $lot_id,
                'soustraitant_id' => $bon->soustraitant_id
            ];
            $soustraitant = ProgrammeSoustraitant::with(['lots' =>function($query) use($lot_id,$contrat) {
                $query->whereIn('lot_id', $lot_id)->with('lot');
                $query->where('soustraitantcontrat_id',$contrat);
            }])
            ->where('id', $bon->soustraitant_id)
            ->where('programme_id', $bon->programme_id)
            ->first();
            //dd($soustraitant,$lot_id);

            $ctt = ProgrammeSoustraitantcontrat::where('id',$bon->contrat_id)->where('programme_id',$bon->programme_id)->first();
            $soustraitant->itemcontrat = $ctt;

            return view('chef_projet.bc_edit', compact('soustraitant', 'vars', 'bon'));
        }
        catch(\Exception $e) {
            exit($e->getMessage());
        }
    }

    public function updateDemande(Request $request, $id)
    {
        $id = decrypt($id);
        //dd($id,$request->all());
        $programme_id = Session::get('program');
        // select bon de demande
        $demande = Bon_demande::findOrFail($id);
        //update items before bon de demande
        $i=$j=0;
        foreach($request->lot_id as $lot_id) {
            if( isset($request->corps_id[$lot_id]) && count($request->corps_id[$lot_id]) > 0 ) {
                foreach( $request->corps_id[$lot_id] as $corps_id) {
                    if( isset($request->sous_corps_id[$lot_id][$corps_id]) && count($request->sous_corps_id[$lot_id][$corps_id]) > 0) {
                        foreach( $request->sous_corps_id[$lot_id][$corps_id] as $key=>$sous_corps_id) {
                            if( isset($request->produit_id[$lot_id][$corps_id][$key]) && $request->produit_id[$lot_id][$corps_id][$key] != null ) {
                                if( isset($request->qte_sscorps[$lot_id][$corps_id][$key]) && $request->qte_sscorps[$lot_id][$corps_id][$key] != null ) {
                                    //var
                                    $produit_id = $request->produit_id[$lot_id][$corps_id][$key];
                                    $qte        = $request->qte_sscorps[$lot_id][$corps_id][$key];
                                    // checking
                                    $check = Bon_demande_item::where('bon_demande_id', $demande->id)
                                    ->where('lot_id', $lot_id)
                                    ->where('produit_id', $produit_id)
                                    ->pluck('qte')
                                    ->first();
                                    //dd($check,$lot_id,$produit_id,$demande->id);

                                    //update or delete if exist
                                    if( $check != null && (float)$check > 0 ) {
                                        $item = Bon_demande_item::where('bon_demande_id', $demande->id)
                                        ->where('lot_id', $lot_id)
                                        ->where('produit_id', $produit_id)
                                        ->first();
                                        if($qte == 0) {
                                            $item->delete();
                                            $i++;
                                        }
                                        else {
                                            if($qte > 0) {
                                                $item->update(['qte' => $qte]);
                                                $i++;
                                            }
                                        }
                                    }
                                    else { // create if not exist
                                        //dd($qte);
                                        if($qte > 0) {
                                            //$check = ProgrammeSouscorpsetatLot::findOrFail($corps_id);
                                            $check = ProgrammeSouscorpsetatLot::where('corpsetat_id',$corps_id)->where('produit_id',$produit_id)->where('lot_id',$lot_id)->first();

                                            Bon_demande_item::create([
                                                'bon_demande_id' => $id,
                                                'soustraitant_id' => $request->soustraitant_id,
                                                'lot_id' => $lot_id,
                                                'produit_id' => $produit_id,
                                                'corpsetats_id' => $check->corpsetats_id,
                                                'scorpsetats_id' => $sous_corps_id,
                                                'qte' => $qte
                                            ]);
                                            $i++;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        // update bon de demande
        if($request->file('demandefile')){
            $file = $request->file('demandefile');
            $extension = $file->getClientOriginalExtension() ?: 'pdf';
            $folderName = 'assets/uploads/bondemandes/';
            $document = Str::random(6).'.'. $extension;
            $file->move($folderName,$document);
            $demande->update(['file' => $document]);
            $j++;
        }
        if($i > 0){
            $demande->update(['user_id' => Auth::user()->id, 'status'=>1,'nivalid'=>1]);
        }
        if($i > 0 || $j > 0) {
            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un bon de sortie de stock en attente de traitement.</p>";
            $sujet = "BON DE SORTIE DE STOCK EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";
            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            //send le mail au magasinier
            $user = User::where('role_id',4)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('cphome.bc.cancel')->with('success' ,'Bon de demande modifier avec succès !');
        }
        else{
            return back()->with('error' ,'Vous n\'avez rien modifier !');
        }
    }

    public function bcWaiting()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        //Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;
        $bons = Bon_demande::where('programme_id', $idprogram)->where('status', 1)->with('contrat')->with('bon_demanbde_items')->with('soustraitant')->with('user')->orderBy('updated_at','desc')->get();
        return view('chef_projet.bc_waiting',compact('bons'));
    }

    public function bcCancel()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $bons = Bon_demande::where('programme_id', $idprogram)->where('status', 4)->with('contrat')->with('bon_demanbde_items')->with('soustraitant')->with('user')->orderBy('updated_at','desc')->get();
        //dd($bons);
        return view('chef_projet.bc_cancel',compact('bons'));
    }

    public function bcValid()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $bons = Bon_demande::where('programme_id', $idprogram)->where('status', 3)->with('contrat')->with('bon_demanbde_items')->with('soustraitant')->with('user')->orderBy('updated_at','desc')->get();
        //dd($bons);
        return view('chef_projet.bc_valid',compact('bons'));
    }

    public function bl()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $bons = Bon_demande::where('programme_id', $idprogram)->where('status', 3)->with('contrat')->with('bon_demanbde_items')->with('soustraitant')->with('user')->get();
        //dd($bons);
        return view('chef_projet.bl',compact('bons'));
    }

    public function showBc($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $data = Bon_demande::where('programme_id', $idprogram)->where('ref',$slug)->with('contrat')->with('bon_demanbde_items')->with('soustraitant')->with('user')->first();
        //dd($data);
        return view('chef_projet.bc_show',compact('data'));
    }

    public function pdfBc($slug)
    {
        //dd($slug);

        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $data = Bon_demande::where('programme_id', $idprogram)->where('ref',$slug)->with('contrat')->with('bon_demanbde_items')->with('soustraitant')->with('user')->first();
        //dd($data);

        $pdf = PDF::loadView('chef_projet.bc_download',compact('data'));
        //$pdf->setPaper('a4', 'landscape');
        //return view('chef_projet.bc_download',compact('data'));
        //return $pdf->stream('etat_gestion_stock-'.date("d-m-y").'.pdf');
        return $pdf->download('bon-demande-'.$slug.'.pdf');
    }

    public function validBc($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $data = Bon_demande::where('programme_id', $idprogram)->where('status',1)->where('ref',$slug)->with('bon_demanbde_items')->first();
        if(!$data){
            return redirect()->route('cphome.bc.waiting')->with('error' ,'Le bon de demande est introuvable.');
        }

        if(Auth::user()->role_id == 5){
            $data->status = 2;

            $message = "<p>Bonjour,</p><p>Vous avez un bon de demande en attente de traitement.</p>";
            $sujet = "BON DE DEMANDE EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',4)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);

        }else{
            $data->nivalid = 2;

            $message = "<p>Bonjour,</p><p>Vous avez un bon de demande en attente de traitement.</p>";
            $sujet = "BON DE DEMANDE EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('look',0)->where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);
        }
        $data->save();


        return redirect()->route('cphome.bc.waiting')->with('success' ,'Le bon de demande a bien été validé');
        //dd($data);
    }

    public function rejetBc(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'bonid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $data = Bon_demande::where('programme_id', $idprogram)->where('ref',$request->bonid)->where('status',1)->with('bon_demanbde_items')->first();
            if($data){
                $data->status = 4;
                $data->motif = $request->motif;
                $data->save();

                $message = "<p>Bonjour,</p><p>Un bon de demande été rejeté pour : </p><p>".$request->motif."</p>";
                $sujet = "BON DE DEMANDE REJETE - GESTION DE STOCK";

                $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
                $user = User::where('role_id',2)->whereIn('id',$pguser)->pluck('email')->toArray();
                //dd($pguser,$user);
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cphome.bc.waiting')->with('success' ,'Le bon de demande a bien été rejeté');

            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }
    }

    public function destroyDemande($id)
    {
        try{
            $id = decrypt($id);
            $bon = Bon_demande::where('ref',$id)->firstOrFail();
            // save last history before destroy all data of current Bon;
            //save before deleting bon items
            Deleted_bon_demande_historique::create([
                'deleted_bon_demande_id' => $id,
                'user_id' => Auth::user()->id,
                'action' => 'Supprimer',
                'rapport' => ''
            ]);
            // begin deleting
            //
            // save before deleting bon de demande
            $deleted_bon = Deleted_bon_demande::create([
                'programme_id' => $bon->programme_id,
                'soustraitant_id' => $bon->soustraitant_id,
                'user_id' => $bon->user_id,
                'ref' => $bon->ref,
                'file' => $bon->file,
                'status' => $bon->status,
                'motif' => $bon->motif
            ]);
            //ITEMS
            $bon_items = Bon_demande_item::where('bon_demande_id', $bon->id)->select('id')->get();
            foreach($bon_items as $item){
                $line = Bon_demande_item::findOrFail($item->id);
                if($line != null) {
                    //save before deleting bon items
                    Deleted_bon_demande_item::create([
                        'deleted_bon_demande_id' => $deleted_bon->id,
                        'soustraitant_id' => $line->soustraitant_id,
                        'lot_id' => $line->lot_id,
                        'produit_id' => $line->produit_id,
                        'qte' => $line->qte
                    ]);
                    //delete bon items
                    $line->delete();
                }
            }
            $bon_history = Bon_demande_historique::where('bon_demande_id', $bon->id)->select('id')->get();
            foreach($bon_history as $history){
                $line = Bon_demande_historique::findOrFail($history->id);
                if($line != null) {
                    //save before deleting bon items
                    Deleted_bon_demande_historique::create([
                        'deleted_bon_demande_id' => $deleted_bon->id,
                        'user_id' => $line->user_id,
                        'action' => $line->action,
                        'rapport' => $line->rapport
                    ]);
                    //delete bon items
                    $line->delete();
                }
            }
            //
            //delete bon
            $bon->delete();
            //return
            return back()->with('success', "Bon de sortie N° ".$deleted_bon->ref." supprimer avec succès !");
        }
        catch(\Exception $e) {
            exit($e->getMessage());
        }
    }


    //SOUS-TRAITANT

    public function straitant()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $lots = ProgrammeLot::where('programme_id',$idprogram)->orderBy('lot','asc')->get();
        Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;
        //$soustraitants = ProgrammeSoustraitant::with('lots')->where('nivalid',$niv)->where('id','<>',1)->where('etat', 1)->get();
        $contratsousts = ProgrammeSoustraitantcontrat::with('lots')->with('straitant')->where('nivalid',$niv)->where('programme_id',$idprogram)->where('etat', 1)->get();
        //dd($soustraitants);

        return view('chef_projet.sous_traitant.index',compact('lots','contratsousts'));
    }

    public function straitantindex()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $soustraitants = ProgrammeSoustraitant::with('contrats')->where('programme_id',$idprogram)->get();
        //dd($soustraitants);

        return view('chef_projet.sous_traitant.index1',compact('soustraitants'));
    }

    public function showStraitant($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $nvalid = Auth::user()->role_id == 3 ? 1 : 2;
        $contrat = ProgrammeSoustraitantcontrat::where('slug',$slug)->where('programme_id',$idprogram)->with('lots')->with('straitant')->first();
        //dd($contrat);
        if(!$contrat){
            return redirect()->back()->with('error','Une erreur est survenue veuillez recommencez');
        }

        $lotid = $contrat->lots ? $contrat->lots->pluck('lot_id') : [];
        $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lotid)->groupBy('actif_id')->with('actifs')->get();
        //dd($actifs);
        return view('chef_projet.sous_traitant.show',compact('contrat','actifs'));
    }

    public function validStraitantOld($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //Auth::user()->role_id == 3 ? $niv = 2 : $niv = 3 ;

        $soustraitants = ProgrammeSoustraitant::where('id',$id)->first();
        $soustraitants->nivalid = 2;

        if(Auth::user()->role_id == 5){
            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour,</p><p>Le sous-traitant a bien été validé.</p>";
            $sujet = "SOUS-TRAITANT ACCEPTER - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',2)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);

            $soustraitants->etat = 2;
        }else{
            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour,</p><p>Vous avez un sous-taitant en attente de traitement.</p>";
            $sujet = "SOUS-TRAITANT EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('look',0)->where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);
        }
        $soustraitants->save();

        return redirect()->back()->with('success','Le sous-traitant a bie été traité');
    }

    public function validStraitant($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($id,$idprogram);
        //Auth::user()->role_id == 3 ? $niv = 2 : $niv = 3 ;

        $soustraitants = ProgrammeSoustraitantcontrat::with('straitant')->where('id',$id)->first();
        $soustraitants->nivalid = 2;

        $pgg = Programme::where('id',$idprogram)->first();
        $stnom = $soustraitants->straitant->nom;
        if(Auth::user()->role_id == 5){
            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour Monsieur/Madame</p><p>Le contrat du sous-traitant <b>".$stnom."</b> a bien été validé.<br>Voir les détails ci-dessous.</p><p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$stnom." <br><b>CONTRAT : </b>".$soustraitants->libelle."<br> <b>CODE : </b> ".$soustraitants->slug."</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "CONTRAT VALIDER - ".$pgg->libelle." - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);

            $soustraitants->etat = 2;
        }else{
            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un nouveau contrat d'un sous-taitant en attente de traitement.</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$stnom." <br><b>CONTRAT : </b>".$soustraitants->libelle." <br><b>CODE : </b> ".$soustraitants->slug."</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "NOUVEAU CONTRAT EN ATTENTE DE TRAITEMENT - ".$pgg->libelle." - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('look',0)->where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);
        }
        $soustraitants->save();

        return redirect()->route('cphome.st')->with('success','Le sous-traitant a bie été traité');
    }

    public function rejetStraitantOld(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'stid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $id = decrypt($request->stid);
            $data = ProgrammeSoustraitant::where('id',$id)->where('programme_id', $idprogram)->first();
            if($data){
                $data->etat = 0;
                $data->motif = $request->motif;
                $data->save();

                //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
                $message = "<p>Bonjour,</p><p>Votre sous-traitant a été rejeté pour :</p><p>".$request->motif."</p>";
                $sujet = "SOUS-TRAITANT REJETE - GESTION DE STOCK";

                $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
                $user = User::where('role_id',2)->whereIn('id',$pguser)->pluck('email')->toArray();
                //dd($pguser,$user);
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cphome.st')->with('success' ,'Le sous-traitant a bien été rejeté');
            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }


    }

    public function rejetStraitant(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'stid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $id = decrypt($request->stid);
            $data = ProgrammeSoustraitantcontrat::with('straitant')->where('id',$id)->where('programme_id', $idprogram)->first();
            if($data){
                $data->etat = 0;
                $data->motif = $request->motif;
                $data->save();

                DB::table('programmes_sous_traitant_contrat_montants')->where('soustraitantcontrat_id',$data->id)->update(['etatdel'=>0]);

                //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
                $nst = $data->straitant->nom;
                $pgg = Programme::where('id',$idprogram)->first();
                $message = "<p>Bonjour Monsieur/Madame</p><p>Le contrat du sous-traitant <b>".$nst."</b> a été rejeté.<br>Voir ci-dessous les détails.</p><p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$nst." <br><b>CONTRAT : </b>".$data->libelle."<br><b>CODE : </b>".$data->slug."</p>";
                $message .="<p><b>MOTIF : </b>".$request->motif."</p>";
                $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

                $sujet = "CONTRAT REJETE - GESTION DE STOCK - ".$pgg->libelle;

                $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
                $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
                //dd($pguser,$user);
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cphome.st')->with('success' ,'Le contrat a bien été rejeté');
            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }


    }

    public function getLotStraitant($id)
    {
        $soustraitants = ProgrammeSoustraitantcontrat::where('id',$id)->with('lots')->first();
        $array = [];
        foreach ($soustraitants->lots as $k=>$lot){
            $l=ProgrammeLot::where('id',$lot->lot_id)->first();
            $actif=ProgrammeActif::where('id',$l->actif_id)->first();
            $array[$k]['lot'] = $l ? $l->lot : '';
            $array[$k]['type'] = $actif ? $actif->libelle :'';
        }
        //dd($soustraitants)
        /*if (isset($soustraitants->lots)){
            $lot=ProgrammeLot::where('id',$soustraitants->lots->lot_id)->first();
            $actif=ProgrammeActif::where('id',$lot->actif_id)->first();
            $soustraitants->lots->lib = $lot->lot;
            $soustraitants->lots->actif = $actif->libelle;
        }else{
            $soustraitants->lots->lib = '-';
            $soustraitants->lots->actif = '-';
        };*/

        return $array;
    }

    public function storeStraitant(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            //'lot' =>'required',
            'phone' =>'required',
            //'contrat' =>'required',
        ]);

        $idprogram = $this->getIdProgram();
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            //check existant
            $check = ProgrammeSoustraitant::where('programme_id',$idprogram)->where('nom',$request->nom)->first();
            if($check){
                return redirect()->back()->with('error','Cet soustrait existe déjà');
            }

            $soustraitant = new ProgrammeSoustraitant();
            $soustraitant->programme_id = $idprogram;
            $soustraitant->nom = $request->nom;
            $soustraitant->contact = $request->phone;
            $soustraitant->etat = 2;
            $soustraitant->nivalid = 2;

            if($request->file('contrat')){
                $file = $request->file('contrat');
                $extension = $file->getClientOriginalExtension() ?: 'pdf';
                $folderName = 'assets/uploads/contrats/';
                $picture = Str::random(6).'.'. $extension;
                if (!empty($soustraitant->contrat)) {
                    unlink($folderName.$soustraitant->contrat);
                }
                $file->move($folderName,$picture);
                $soustraitant->contrat = $picture;
            }

            $soustraitant->save();

            //save for lot
            // foreach ($request->lot as $lot){
            //     $check = LotSoustraitant::where('lot_id',$lot)->where('soustraitant_id',$soustraitant->id)->first();
            //     if(!$check){
            //         $newlot = new LotSoustraitant();
            //         $newlot->lot_id = $lot;
            //         $newlot->soustraitant_id = $soustraitant->id;
            //         $newlot->save();
            //     }
            // }

            return redirect()->route('cphome.st.index')->with('success' ,'Le sous-traitant a bien été ajouté');
        }

    }

    public function editStraitant($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $soustraitant = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();

        if (isset($soustraitant->id)){
            $lots = ProgrammeLot::where('programme_id',$idprogram)->get();
            $stlot = LotSoustraitant::where('soustraitant_id',$slug)->pluck('lot_id')->toArray();

            return view('chef_projet.sous_traitant.edit',compact('lots','soustraitant','stlot'));
        }

        return redirect()->back();
    }

    public function updateStraitant(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'slug' =>'required',
            'phone' =>'required',
            //'lot' =>'required',
        ]);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram()
        ;
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $soustraitant = ProgrammeSoustraitant::where('id',$request->slug)->first();

            if($soustraitant){
                $soustraitant->programme_id = $idprogram;
                $soustraitant->nom = $request->nom;
                $soustraitant->contact = $request->phone;
                $soustraitant->save();

                return redirect()->route('cphome.st.index')->with('success' ,'Le sous-traitant a bien été modifié');
            }

            return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
        }
    }

    public function deltraitant($slug)
    {
        $slug = decrypt($slug);
        $check = ProgrammeSoustraitantcontrat::where('id',$slug)->first();

        if($check){
            //verifier si tous les transactionson ete valider avant supp
            //A faire // C'est fait mtn 😁
            $checktrans = Transaction::where('soustraitant_id',$check->soustraitant_id)->where('soustraitantcontrat_id',$slug)->where('status',1)->count();
            if($checktrans > 0){
                return redirect()->back()->with('error' ,'Impossible de supprimer ce contrat car il a des transactions en cours');
            }

            //supprimer les lotsoutraitcorpsetat
            DB::table('lotsoustraitans_corpsetats')->where('soustraitantcontrat_id',$slug)->update(['etatdel'=>0]);
            DB::table('programmes_sous_traitant_contrat_montants')->where('soustraitantcontrat_id',$slug)->update(['etatdel'=>0]);
            ProgrammeSoustraitantcontratMontant::where('soustraitantcontrat_id',$slug)->delete();
            //LotSoustraitant::where('soustraitantcontrat_id',$slug)->delete();
            //LotSoustraitantCorpsetat::where('soustraitantcontrat_id',$slug)->delete();
            $check->delete();
        }
        return redirect()->back()->with('success' ,'Le contrat du sous-traitant a bien été supprimé');
    }

    //CONTRAT
    public function contratStraitant($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $sous = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();

        if($sous->etat != 2){
            return redirect()->back()->with('error',"Vous n'avez pas de droit à cette page");
        }

        $lotid = $sous->lots ? $sous->lots->pluck('lot_id') : [];
        $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lotid)->groupBy('actif_id')->with('actifs')->get();

        if (isset($sous->id)){
            $contrats = ProgrammeSoustraitantcontrat::where('soustraitant_id',$sous->id)->with('straitant')->orderBy('id','desc')->get();
            $lots = ProgrammeLot::where('programme_id',$idprogram)->orderBy('lot','asc')->get();

            return view('chef_projet.sous_traitant.new_contrats',compact('lots','sous','actifs','contrats'));
        }

        return redirect()->back();
    }

    public function storeStraitantContrat(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'lot' =>'required',
            'slugst' =>'required',
            //'contrat' =>'required',
        ]);

        $idprogram = $this->getIdProgram();
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $st = ProgrammeSoustraitant::where('id',$request->slugst)->first();

            if(!$st){
                return redirect()->back()->with('error','Veuillez selectionnez un sous-traitant actif');
            }

            //rendre les lots unique
            $uniq = array_unique($request->lot);
            $lotuniq = array_values($uniq);
            $vars = [
                'lot_id' => $lotuniq,
                'stt_id' => $request->slugst,
                'libellctt' => $request->libelle,
            ];

            $stcontrat = $st;
            // $stcontrat = new ProgrammeSoustraitantcontratTampon();
            // $stcontrat->libelle = $request->libelle;
            // $stcontrat->programme_id = $idprogram;
            // $stcontrat->soustraitant_id = $st->id;
            // $stcontrat->slug = 'ST-'.date('di').'-'.rand(1,99);

            // if($request->file('contrat')){
            //     $file = $request->file('contrat');
            //     $extension = $file->getClientOriginalExtension() ?: 'pdf';
            //     $folderName = 'assets/uploads/contrats/';
            //     $picture = Str::random(6).'.'. $extension;
            //     if (!empty($stcontrat->contrat)) {
            //         unlink($folderName.$stcontrat->contrat);
            //     }
            //     $file->move($folderName,$picture);
            //     $stcontrat->contrat = $picture;
            // }

            // $stcontrat->motif = json_encode($request->lot);
            // $stcontrat->save();

            $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$request->lot)->groupBy('actif_id')->with('actifs')->get();
            return view('chef_projet.sous_traitant.new_contrats_create2',compact('stcontrat','actifs','vars'));
            // return redirect()->route('cphome.st.n-contrat.end',$slug,compact('vars'));
        }
    }

    public function contratStraitantEnd($id)
    {
        $idprogram = $this->getIdProgram();
        $stcontrat = ProgrammeSoustraitantcontratTampon::where('id',$id)->with('straitant')->first();
        if(!$stcontrat){
            return redirect()->route('cphome.st.index')->with('error','Veuillez selectionnez un sous-traitant actif');
        }
        $lotid = json_decode($stcontrat->motif);
        $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lotid)->groupBy('actif_id')->with('actifs')->get();
        //dd($actifs);

        return view('chef_projet.sous_traitant.new_contrats_create2',compact('stcontrat','actifs'));
    }

    public function getRefCtt()
    {
        $programme_id = $this->getIdProgram();

        $code = "CT".date('ym');
        $count = ProgrammeSoustraitantcontrat::withTrashed()->where('programme_id',$programme_id)->count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$programme_id.'-'.$num;

        return $val;
    }

    public function storeStraitantContratEnd(Request $request)
    {
        //dd($request->all());
        ini_set('max_execution_time', -1);

        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $st = ProgrammeSoustraitant::where('id',$request->firststep)->first();
        if(!$st){
            return redirect()->back()->with('error','Veuillez selectionnez un sous-traitant actif');
        }

        if($st->programme->type_rattachement==1){
            $valider = Validator::make($request->all(),[
                'libellectt' =>'required',
                'contrat' =>'required',
            ]);

        }else{
            $valider = Validator::make($request->all(),[
                'libellectt' =>'required',
            ]);
        }


        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $error = 0;
            $message = '';

            $st = ProgrammeSoustraitant::where('id',$request->firststep)->first();
            //dd($st);

            if(!$st){
                return redirect()->back()->with('error','Veuillez selectionnez un sous-traitant actif');
            }
            //dd($itemst);

            $error = 0;
            $message = '';

            //SAVE LES LOTS
            $lots = json_decode($request->listlots);
            $existe = false;
            $libellot = '';
            $lesceiputs = [];

            //verifier s'il exist ces CE dans un autre contrat.
            $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lots)->groupBy('actif_id')->with('actifs')->get();
            foreach($actifs as $itemactif){
                $rq ='ce_'.$itemactif->actif_id;
                if(isset($request->$rq) and count($request->$rq)>0){
                    $lesceiputs[$rq] = $request->$rq;

                    foreach ($request->$rq as $corp){
                        //dd($corp);
                        $check = LotSoustraitantCorpsetat::where('etatdel',1)->where('lot_id',$itemactif->id)->where('actif_id',$itemactif->actif_id)->where('corpsetats_id',$corp)->with('soustraitant')->with('corpslots')->first();
                        if($check){
                            $error++;
                            $cee = $check->corpslots?$check->corpslots->libelle :'-';
                            $sttt = $check->soustraitant?$check->soustraitant->nom :'-';
                            $message .= "<li>Le corps d'etat <b> ".$cee." </b> du lot <b>".$itemactif->lot."</b> est déjà sous contrat du sous-traitant <b>".$sttt."</b> ;</li>" ;

                        }
                        //Deuxiem verification si le lot est deja sous contrat avec un autre soustraitant
                        $check2 = ProgrammeSoustraitantcontratMontant::where('etatdel',1)->whereIn('lot_id',$lots)->where('programme_id',$idprogram)->where('corpsetats_id',$corp)->with('straitant')->with('corpslots')->with('lot')->first();
                        if($check2){
                            $error++;
                            $lotmss = $check2->lot?$check2->lot->lot :'-';
                            $cee = $check2->corpslots?$check2->corpslots->libelle :'-';
                            $sttt = $check2->straitant?$check2->straitant->nom :'-';
                            $message .= "<li>Le corps d'etat <b> ".$cee." </b> du lot <b>".$lotmss."</b> est déjà sous contrat du sous-traitant <b>".$sttt."</b> ;</li>" ;
                        }
                        //dd($itemactif->id,$itemactif->actif_id,$corp,$check,$check2,$message);
                    }
                }
            }
            //dd($message);
            if( $error > 0 ) {
                $messag = "Ce contrat ne peut pas être ajouté, car :<br> <ul>".$message.'</ul>';
                return redirect()->back()->with('error', $messag);
            }
            //end verification

            if($st->programme->type_rattachement==1){
                $this->saveCtrSt($request,$lots);

                //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
                $pgg = Programme::where('id',$idprogram)->first();
                $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un nouveau contrat d'un sous-taitant en attente de traitement.</p>";
                $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUSTRAITANT : </b>".$st->nom." <br>";
                $message .="<b>CONTRAT : </b>".$request->libellectt."<br>";
                $message .="<b>CODE : </b>".$this->getRefCtt()."</p>";
                $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
                $sujet = "NOUVEAU CONTRAT EN ATTENTE DE TRAITEMENT - ".$pgg->libelle ."- G-STOCK";

                $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
                $user = User::where('role_id',5)->whereIn('id',$pguser)->pluck('email')->where('look',0)->toArray();
                //dd($pguser,$user);
                //ICI A REMTTRE EN PLACE EN DESSOUS
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cphome.st.n-contrat',$st->id)->with('success' ,'Le contrat a bien été ajouté');
            }else{
                //parcourir les lots et assigner les corps d'etats dans un tableau
                $lots = json_decode($request->listlots);
                $lotsandce = [];
                foreach($lots as $item){
                    //Rechercher le lot
                    $lot = ProgrammeLot::where('id',$item)->first();
                    //dd($lot);
                    if($lot){
                        $rq ='ce_'.$lot->actif_id;
                        if(isset($request->$rq) and count($request->$rq)>0){
                            foreach ($request->$rq as $corp){
                                $lotsandce[$item][] = $corp;
                            }
                        }
                    }
                }

                $vars = [
                    'lot_id' => $lots,
                    'stt_id' => $request->firststep,
                    'libellctt' => $request->libellectt,
                    'lotsandce' => $lotsandce,
                    'lesceiputs'=>$lesceiputs,
                ];

                //dd($request->all(),$lesceiputs,$vars);
                $stcontrat = $st;
                $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lots)->groupBy('actif_id')->with('actifs')->get();
                return view('chef_projet.sous_traitant.new_contrats_create3',compact('stcontrat','actifs','vars'));

            }
        }
    }


    public function saveCtrSt($request,$lots)
    {
        //dump($request->all());
        $idprogram = $this->getIdProgram();
        $st = ProgrammeSoustraitant::where('id',$request->firststep)->first();

        //CREATION DU CONTRAT
        $slug = $this->getRefCtt();
        $stcontrat = new ProgrammeSoustraitantcontrat();
        $stcontrat->libelle = $request->libellectt;
        $stcontrat->programme_id = $idprogram;
        $stcontrat->soustraitant_id = $st->id;
        $stcontrat->slug = $slug;
        $stcontrat->montantctr = $request->mttctr? str_replace(' ', '', $request->mttctr):0;

        if($request->file('contrat')){
            $file = $request->file('contrat');
            $extension = $file->getClientOriginalExtension() ?: 'pdf';
            $folderName = 'assets/uploads/contrats/';
            $picture = $slug.'.'. $extension;
            $file->move($folderName,$picture);
            $stcontrat->contrat = $picture;
        }

        $stcontrat->nivalid = 2;
        $stcontrat->save();

        foreach($lots as $lot){
            $check = LotSoustraitant::where('lot_id',$lot)->where('soustraitantcontrat_id',$stcontrat->id)->where('soustraitant_id',$stcontrat->soustraitant_id)->first();
            if(!$check){
                $newlot = new LotSoustraitant();
                $newlot->lot_id = $lot;
                $newlot->soustraitantcontrat_id = $stcontrat->id;
                $newlot->soustraitant_id = $stcontrat->soustraitant_id;
                $newlot->save();
            }
        }

        //SAVE LES CE PAR
        $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lots)->groupBy('actif_id')->with('actifs')->get();
        foreach($actifs as $itemactif){
            $rq ='ce_'.$itemactif->actif_id;
            if(isset($request->$rq) and count($request->$rq)>0){

                foreach ($request->$rq as $corp){
                    $check = LotSoustraitantCorpsetat::where('etatdel',1)->where('actif_id',$itemactif->actif_id)->where('soustraitant_id',$stcontrat->soustraitant_id)->where('corpsetats_id',$corp)->where('soustraitantcontrat_id',$stcontrat->id)->first();
                    //dd($actifs);
                    if(!$check){
                        $new = new LotSoustraitantCorpsetat();
                        $new->lot_id = $itemactif->id;
                        $new->actif_id = $itemactif->actif_id;
                        $new->soustraitant_id = $stcontrat->soustraitant_id;
                        $new->corpsetats_id = $corp;
                        $new->soustraitantcontrat_id = $stcontrat->id;
                        $new->save();
                    }
                }
            }
        }
        return $stcontrat;
    }

    public function storeStraitantContratPhaseEnd(Request $request) {
        //dd($request->all());
        ini_set('max_execution_time', -1);

        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $valider = Validator::make($request->all(),[
            'libellectt' =>'required',
            'mttctr' =>'required',
            'contrat' =>'required',
            'listlots'=>'required',
            'vars'=>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $st = ProgrammeSoustraitant::where('id',$request->firststep)->first();
            if(!$st){
                return redirect()->back()->with('error','Veuillez selectionnez un sous-traitant actif');
            }

            $error = 0;
            $message = '';

            $lots = json_decode($request->listlots);
            $vars = json_decode($request->vars);
            $existe = false;
            $ceolinp = $vars->lesceiputs;
            //verifier s'il exist ces CE dans un autre contrat.
            $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lots)->groupBy('actif_id')->with('actifs')->get();
            foreach($actifs as $itemactif){
                $rq ='ce_'.$itemactif->actif_id;
                $itemCe = $ceolinp->$rq;
                $request[$rq] = $itemCe;
                // dd($ceolinp,$ceolinp->$rq);
                if(isset($itemCe) and count($itemCe)>0){
                    foreach ($itemCe as $corp){
                        // $checks = LotSoustraitantCorpsetat::where('etatdel',1)->where('actif_id',$itemactif->actif_id)->where('corpsetats_id',$corp)->with('soustraitant')->with('corpslots')->get();
                        $check = LotSoustraitantCorpsetat::where('etatdel',1)->where('lot_id',$itemactif->id)->where('actif_id',$itemactif->actif_id)->where('corpsetats_id',$corp)->with('soustraitant')->with('corpslots')->first();
                        if($check){
                            $error++;
                            $cee = $check->corpslots?$check->corpslots->libelle :'-';
                            $sttt = $check->soustraitant?$check->soustraitant->nom :'-';
                            $message .= "<li>Le corps d'etat <b> ".$cee." </b> du lot <b>".$itemactif->lot."</b> est déjà sous contrat du sous-traitant <b>".$sttt."</b> ;</li>" ;
                        }
                        //Deuxiem verification si le lot est deja sous contrat avec un autre soustraitant
                        $check2 = ProgrammeSoustraitantcontratMontant::where('etatdel',1)->whereIn('lot_id',$lots)->where('programme_id',$idprogram)->where('corpsetats_id',$corp)->with('straitant')->with('corpslots')->with('lot')->first();
                        if($check2){
                            $error++;
                            $lotmss = $check2->lot?$check2->lot->lot :'-';
                            $cee = $check2->corpslots?$check2->corpslots->libelle :'-';
                            $sttt = $check2->straitant?$check2->straitant->nom :'-';
                            $message .= "<li>Le corps d'etat <b> ".$cee." </b> du lot <b>".$lotmss."</b> est déjà sous contrat du sous-traitant <b>".$sttt."</b> ;</li>" ;
                        }
                    }
                }
            }

            if( $error > 0 ) {
                $messag = "Ce contrat ne peut pas être ajouté, car :<br> <ul>".$message.'</ul>';
                return redirect()->back()->with('error', $messag);
            }

            //dump($request->all(),$vars,$lots);
            //end verification

            $contratsav = $this->saveCtrSt($request,$lots);

            //Save montant par corps d'etat
            $lotsandce = $vars->lotsandce;
            foreach($lotsandce as $i=>$item){
                //Rechercher le lot
                $lot = ProgrammeLot::where('id',$i)->first();
                if($lot){
                    //dd($lot);
                    foreach ($item as $itemce){
                        $rqmt ='montant_'.$i.'_'.$itemce;
                        $montlot = $request->$rqmt? str_replace(' ', '', $request->$rqmt):0;
                        //dd($rqmt,$montlot);

                        $ctmt = new ProgrammeSoustraitantcontratMontant();
                        $ctmt->soustraitant_id = $request->firststep;
                        $ctmt->soustraitantcontrat_id = $contratsav->id;
                        $ctmt->programme_id = $idprogram;
                        $ctmt->lot_id = $i;
                        $ctmt->corpsetats_id = $itemce;
                        $ctmt->montant = $montlot;
                        $ctmt->save();
                    }
                }
            }

            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $pgg = Programme::where('id',$idprogram)->first();
            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un nouveau contrat d'un sous-taitant en attente de traitement.</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUSTRAITANT : </b>".$st->nom." <br>";
            $message .="<b>CONTRAT : </b>".$request->libellectt."<br>";
            $message .="<b>CODE : </b>".$this->getRefCtt()."</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "NOUVEAU CONTRAT EN ATTENTE DE TRAITEMENT - ".$pgg->libelle ."- G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',5)->whereIn('id',$pguser)->pluck('email')->where('look',0)->toArray();
            //dd($pguser,$user);
            //ICI A REMTTRE EN PLACE EN DESSOUS
            $this->sendMailNotificaton($message,$sujet,$user);


            return redirect()->route('cphome.st.n-contrat',$st->id)->with('success' ,'Le contrat a bien été ajouté');

        }
    }


    public function stcontratwaiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $contrats = ProgrammeSoustraitantcontrat::where('programme_id',$programme_id)->has('straitant')->with('straitant')->with('lots')->where('etat',1)->orderBy('id','desc')->get();
        //dd($demandes);
        return view('chef_projet.sous_traitant.new_contrats_waiting',compact('contrats'));
    }

    public function stcontratvalid()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $contrats = ProgrammeSoustraitantcontrat::where('programme_id',$programme_id)->has('straitant')->with('straitant')->with('lots')->where('etat',2)->orderBy('id','desc')->get();
        //dd($demandes);
        return view('chef_projet.sous_traitant.new_contrats_valider',compact('contrats'));
    }

    public function stcontratcancel()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $contrats = ProgrammeSoustraitantcontrat::where('programme_id',$programme_id)->has('straitant')->with('straitant')->with('lots')->where('etat',0)->orderBy('id','desc')->get();
        //dd($demandes);
        return view('chef_projet.sous_traitant.new_contrats_rejeter',compact('contrats'));
    }

    public function stcontratvalider($id)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $id = decrypt($id);
        $contrat = ProgrammeSoustraitantcontrat::with('straitant')->where('programme_id',$idprogram)->where('id',$id)->first();

        if(Auth::user()->role_id == 5){
            $contrat->etat = 2;
            $contrat->save();
            $nst = $contrat->straitant->nom;

            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $pgg = Programme::where('id',$idprogram)->first();
            $message = "<p>Bonjour Monsieur/Madame</p><p>Le contrat a bien été validé.</p><p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUSTRAITANT : </b>".$nst." <br><b>CONTRAT : </b>".$contrat->libelle." - ".$contrat->slug."</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "CONTRAT VALIDER - GESTION DE STOCK - ".$pgg->libelle;

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

        }

        return redirect()->back()->with('success','Le contrat a bien été validé.');

    }

    public function stcontratrejeter(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'stid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $id = decrypt($request->stid);
            $data = ProgrammeSoustraitantcontrat::with('straitant')->where('id',$id)->where('programme_id', $idprogram)->first();
            if($data){
                $data->etat = 0;
                $data->motif = $request->motif;
                $data->save();

                $nst = $data->straitant->nom;

                //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
                $pgg = Programme::where('id',$idprogram)->first();
                $message = "<p>Bonjour Monsieur/Madame</p><p>Le contrat a été rejeté.</p><p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUSTRAITANT : </b>".$nst." <br><b>CONTRAT : </b>".$data->libelle." - ".$data->slug."</p>";
                $message .="<p><b>MOTIF : </b>".$request->motif."</p>";
                $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

                $sujet = "CONTRAT REJETE - GESTION DE STOCK - ".$pgg->libelle;

                $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
                $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
                //dd($pguser,$user);
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cphome.st.contrat.cancel')->with('success' ,'Le sous-traitant a bien été rejeté');
            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }


    }

    //CE BY CONTRAT
    public function contratStraitantCe($idcontrat)
    {
        //dd($idcontrat);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $contrat = ProgrammeSoustraitantcontrat::where('programme_id',$idprogram)->where('id',$idcontrat)->has('straitant')->with('straitant')->with('lots')->first();
        //dd($contrat);
        if($contrat){
            $lotid = $contrat->lots ? $contrat->lots->pluck('lot_id') : [];
            //get type lot
            $actifs = ProgrammeLot::where('programme_id',$idprogram)->whereIn('id',$lotid)->groupBy('actif_id')->with('actifs')->get();
            //dd($actifs,$lotid,$contrat);

            return view('chef_projet.sous_traitant.new_contrats_show',compact('contrat','actifs'));
        }

        return redirect()->back()->with('error',"Vous n'avez pas de droit à cette page");
    }

    public function getCorps($id,$typeid)
    {
        //dd($id,$typeid);
        $sous = ProgrammeSoustraitant::where('id',$id)->with('lots')->first();
        $corps = ProgrammeCorpsetat::where('programme_id',$sous->programme_id)->where('actif_id',$typeid)->get();
        //dd($corps);
        //dd($lotsous,$corps);
        if(count($corps)>0){
            return $corps;
        }
        return $data = [];


        /*$sous = ProgrammeSoustraitant::where('id',$id)->with('lots')->first();
        $lotsous = LotSoustraitant::where('lot_id',$typeid)->where('soustraitant_id',$id)->with('lot')->first();
        if($lotsous){
            if($lotsous->lot){
                $corps = ProgrammeCorpsetat::where('programme_id',$lotsous->lot->programme_id)->where('actif_id',$lotsous->lot->actif_id)->get();
                //dd($lotsous,$corps);
                return $corps;

            }
            return $data = [] ;
        }
        return $data = [] ;*/
    }

    public function storeStraitantLotDqe(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'corpsetats' =>'required',
            'typeid' =>'required',
            'stid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            //$lotsous = LotSoustraitant::where('lot_id',$request->lotid)->where('soustraitant_id',$request->stid)->with('lot')->first();
            foreach ($request->corpsetats as $corp){
                $check = LotSoustraitantCorpsetat::where('actif_id',$request->typeid)->where('soustraitant_id',$request->stid)->where('corpsetats_id',$corp)->first();
                if(!$check){
                    $new = new LotSoustraitantCorpsetat();
                    //$new->lots_sous_traitants_id = $lotsous->id;
                    //$new->lot_id = $request->lotid;
                    $new->actif_id = $request->typeid;
                    $new->soustraitant_id = $request->stid;
                    $new->corpsetats_id = $corp;
                    $new->save();
                }
            }
            return redirect()->route('cphome.st.contrat',$request->stid)->with('success' ,'Le corps etat a bien été ajouté au contrat');
        }

    }

    public function deleteStraitantLotDqe($item)
    {
        $check = LotSoustraitantCorpsetat::where('id',$item)->first();
        //dd($check);
        if(isset($check->id)){
            $stid = $check->soustraitant_id;
            //check si il a deja eu un paiement pour ce corps d'etat avant supression
            $newcheck = Avancement::where([['soustraitant_id',$check->soustraitant_id],['corpsetat_id',$check->corpsetats_id]])->first();
            if(isset($newcheck->id)){
                return redirect()->route('cphome.st.contrat',$stid)->with('error' ,'Impossible de supprimer le corps etat car il a déjà faire objet de paiment');
            }

            $check->delete();
            return redirect()->route('cphome.st.contrat',$stid)->with('success' ,'Le corps etat a bien été ajouté au contrat');
        }
        return redirect()->back();
    }

    //PAYES
    public function payeStraitant($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $sous = ProgrammeSoustraitantcontrat::where('id',$slug)->with('straitant')->with('lots')->first();
        if($sous->etat != 2){
            return redirect()->back()->with('error',"Vous n'avez pas de droit à cette page");
        }

        //$oldavances = AvancementBrouillon::where('soustraitant_id',$slug)->where('programme_id',$idprogram)->orderBy('lot_id','asc')->with('lot')->with('corpsetat')->get();
        //dd($oldavances);
        if (isset($sous->id)){
            return view('chef_projet.sous_traitant.paye',compact('sous'));
        }

        return redirect()->back();
    }

    public function payeLotStraitant($slug,$lotid)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $straitcontrat = ProgrammeSoustraitantcontrat::where('id',$slug)->first();
        //dd($straitcontrat);
        //$sous = ProgrammeSoustraitant::where('id',$slug)->with('lots')->first();
        $sous = LotSoustraitant::where('soustraitant_id',$straitcontrat->soustraitant_id)->where('lot_id',$lotid)->with('lot')->with('soustraitant')->first();
        //dd($sous,$slug,$lotid);
        $oldavances = AvancementBrouillon::where('soustraitant_id',$straitcontrat->soustraitant_id)->where('programme_id',$idprogram)->orderBy('lot_id','asc')->with('lot')->with('corpsetat')->get();
        //dd($slug,$idprogram,$oldavances);
        if (isset($sous->id)){
            return view('chef_projet.sous_traitant.paye_item',compact('sous','oldavances','straitcontrat'));
        }

        return redirect()->back();
    }

    public function storepayeStraitant(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'taux' =>'required|numeric|min:0|max:100',
            'lotid' =>'required',
            'corpsid' =>'required',
            'stid' =>'required',
            'idcontrat' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            //dd($request->all());
            //$straitant = ProgrammeSoustraitant::where('id',$request->stid)->first();
            //check old taux renseigner
            $old = Avancement::where([['programme_id',$idprogram],['lot_id',$request->lotid],['soustraitant_id',$request->stid],['corpsetat_id',$request->corpsid]])->orderBy('id','desc')->first();
            if(isset($old->id)){
                //veririfier si le total est atteint
                if($old->taux >= 100){
                    return redirect()->back()->with('error','Le taux d\'avancement de corps d\'etat est terminer');
                }
                //veririfier si le taux saisir est superieur au taux précedent
                if($old->taux > $request->taux){
                    return redirect()->back()->with('error','Veuillez verifié le taux d\'avancement');
                }
            }
            //veririfier si l'avancement est deja saisir dans le brouillons
            $check = AvancementBrouillon::where([['programme_id',$idprogram],['lot_id',$request->lotid],['soustraitant_id',$request->stid],['corpsetat_id',$request->corpsid]])->first();
            if(!$check){
                $new = new AvancementBrouillon();
                $new->programme_id = $idprogram;
                $new->lot_id = $request->lotid;
                $new->soustraitant_id = $request->stid;
                $new->corpsetat_id = $request->corpsid;
                $new->soustraitantcontrat_id = $request->idcontrat;
                $new->taux = $request->taux;
                $new->user_id = Auth::user()->id;
                $new->save() ;

                return redirect()->route('cphome.st.lot.paye',[$request->idcontrat,$request->lotid])->with('success','Taux d\'avancement a bien été ajouté');
            }
            return redirect()->back()->with('error','Vous avez déjà renseigner corps d\'etat');
        }
    }

    /*public function storepayeStraitant(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'taux' =>'required|numeric|min:0|max:100',
            'lotid' =>'required',
            'corpsid' =>'required',
            'stid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            //dd($request->all());
            //$straitant = ProgrammeSoustraitant::where('id',$request->stid)->first();
            //check old taux renseigner
            $old = Avancement::where([['programme_id',$idprogram],['lot_id',$request->lotid],['soustraitant_id',$request->stid],['corpsetat_id',$request->corpsid]])->orderBy('id','desc')->first();
            if(isset($old->id)){
                //veririfier si le total est atteint
                if($old->taux >= 100){
                    return redirect()->back()->with('error','Le taux d\'avancement de corps d\'etat est terminer');
                }
                //veririfier si le taux saisir est superieur au taux précedent
                if($old->taux > $request->taux){
                    return redirect()->back()->with('error','Veuillez verifié le taux d\'avancement');
                }
            }
            //veririfier si l'avancement est deja saisir dans le brouillons
            $check = AvancementBrouillon::where([['programme_id',$idprogram],['lot_id',$request->lotid],['soustraitant_id',$request->stid],['corpsetat_id',$request->corpsid]])->first();
            if(!$check){
                $new = new AvancementBrouillon();
                $new->programme_id = $idprogram;
                $new->lot_id = $request->lotid;
                $new->soustraitant_id = $request->stid;
                $new->corpsetat_id = $request->corpsid;
                $new->taux = $request->taux;
                $new->save() ;

                return redirect()->route('cphome.st.paye',$request->stid)->with('success','Taux d\'avancement a bien été ajouté');
            }
            return redirect()->back()->with('error','Vous avez déjà renseigner corps d\'etat');
        }
    }*/

    public function delpayeStraitant($slug)
    {
        $this->getProgramExist();
        $check = AvancementBrouillon::where('id',$slug)->first();
        if($check){
            $check->delete();
            return redirect()->back()->with('success','Taux d\'avancement a bien été supprimé');
        }
        return redirect()->back();
    }

    public function storeSavePayeStraitantOld($slug,$idcontrat)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $gets = AvancementBrouillon::where([['programme_id',$idprogram],['soustraitant_id',$slug]])->with('corpsetat')->get();
        //dd($gets);
        if(count($gets)>0){

            $trans = new Transaction();
            $trans->programme_id = $idprogram;
            $trans->soustraitant_id = $gets[0]->soustraitant_id;
            $trans->lot_id = $gets[0]->lot_id;
            $ttaux = 0 ;
            $ttmt = 0 ;

            foreach ($gets as $get){
                $check = Avancement::where([['programme_id',$idprogram],['lot_id',$get->lot_id],['soustraitant_id',$get->soustraitant_id],['corpsetat_id',$get->corpsetat_id]])->with('corpsetat')->orderBy('id','desc')->first();
                if($check){
                    $taux_exe = $get->taux - $check->taux ;
                }else{
                    $taux_exe = $get->taux;
                }
                $ttaux = $ttaux +$taux_exe;

                if($get->corpsetat){
                    $cout = ($taux_exe/100) * $get->corpsetat->prix ;
                }else{
                    $cout = 0;
                }
                $ttmt = $ttmt +$cout;
                //dd($cout);
                //get montant corps etat

                $new = new Avancement();
                $new->programme_id = $idprogram;
                $new->lot_id = $get->lot_id;
                $new->soustraitant_id = $get->soustraitant_id;
                $new->corpsetat_id = $get->corpsetat_id;
                $new->taux = $get->taux;
                $new->taux_execute = $taux_exe;
                $new->cout = $cout;
                $new->save() ;

                //delete brouillon
                $get->delete();
            }

            $trans->taux = $ttaux;
            $trans->montant = $ttmt;
            $trans->save();

            //SEND NOTIFICATION TO DO(respo opc) FOR CREATE PAIEMENT STRAITANT
            $message = "<p>Bonjour,</p><p>Vous avez une transaction d'un sous-taitant en attente de traitement.</p>";
            $sujet = "TRANSACTION EN ATTENTE DE TRAITEMENT - GESTION DE STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('cphome.st.paye',$idcontrat)->with('success','Le taux d\'avancement enregistrés.');

        }else{
            return redirect()->back();
        }

    }


    public function storeSavePayeStraitant($slug,$idcontrat)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        // $gets = AvancementBrouillon::where([['programme_id',$idprogram],['soustraitant_id',$slug]])->with('corpsetat')->get();
        $gets = AvancementBrouillon::where([['programme_id',$idprogram],['soustraitant_id',$slug],['soustraitantcontrat_id',$idcontrat]])->with('corpsetat')->get();
        //dd($gets);
        if(count($gets)>0){

            $trans = new Transactiontest();
            $trans->slug = $this->getNumTrans($idprogram);
            $trans->programme_id = $idprogram;
            $trans->soustraitant_id = $gets[0]->soustraitant_id;
            $trans->soustraitantcontrat_id = $idcontrat;
            $trans->lot_id = $gets[0]->lot_id;
            $trans->taux = 0;
            $trans->montant = 0;
            $trans->user_id = Auth::user()->id;
            $trans->save();

            $ttaux = 0;
            $ttmt = 0;
            $idtrans = $trans->id ;

            foreach ($gets as $get){
                $check = Avancementest::where([
                    ['programme_id',$idprogram],
                    ['lot_id',$get->lot_id],
                    ['soustraitant_id',$get->soustraitant_id],
                    ['corpsetat_id',$get->corpsetat_id],
                    ['soustraitantcontrat_id',$idcontrat]
                    ])
                    ->with('corpsetat')
                    ->orderBy('id','desc')
                    ->first();

                if($check){
                    $taux_exe = $get->taux - $check->taux ;
                }else{
                    $taux_exe = $get->taux;
                }
                $ttaux = $ttaux +$taux_exe;

                if($get->corpsetat){
                    $cout = ($taux_exe/100) * $get->corpsetat->prix ;
                }else{
                    $cout = 0;
                }
                $ttmt = $ttmt +$cout;
                //dd($cout);
                //get montant corps etat

                $new = new Avancementest();
                $new->programme_id = $idprogram;
                $new->lot_id = $get->lot_id;
                $new->soustraitant_id = $get->soustraitant_id;
                $new->corpsetat_id = $get->corpsetat_id;
                $trans->soustraitantcontrat_id = $idcontrat;
                $new->taux = $get->taux;
                $new->taux_execute = $taux_exe;
                $new->cout = $cout;
                $new->transac_id = $idtrans;
                $new->user_id = Auth::user()->id;
                $new->save() ;

                //delete brouillon
                $get->delete();
            }

            $trans = Transactiontest::where('id',$idtrans)->with('soustraitant')->with('contrat')->first();
            $trans->taux = $ttaux;
            $trans->montant = $ttmt;
            $trans->nivalid = 2;
            $trans->save();

            //SEND NOTIFICATION TO DO(respo opc) FOR CREATE PAIEMENT STRAITANT
            $pgg = Programme::where('id',$idprogram)->first();
            $namst = $trans->soustraitant ? $trans->soustraitant->nom : 'N/A';
            $namstct = $trans->contrat ? $trans->contrat->slug : 'N/A';

            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez une nouvelle transaction d'un sous-taitant en attente de traitement</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "TRANSACTION EN ATTENTE DE TRAITEMENT - GESTION DE STOCK - ".$pgg->libelle;


            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('cphome.st.paye',$idcontrat)->with('success','Les Taux d\'avancement ont bien été enregistrés, elle est en cours de validation.');

        }else{
            return redirect()->back();
        }
    }

    //PROFILE
    public function profil()
    {
        $this->getProgramExist();
        $user = User::where('id',Auth::id())->with('role')->with('programmes')->first();
        return view('chef_projet.profile',compact('user'));
    }

    public function profilUpdate(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'name' =>'required',
            'contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->name = $request->name;
            $user->contact = $request->contact;
            $user->save();
            return redirect()->back()->with('success','✔ Votre profil a été modifié');
        }

    }

    public function profilUpdatePass(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $oldpass = $request->motpass;
            $newpass = $request->newpass;
            $newpassconfirm = $request->confirmpass;
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->back()->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->back()->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->back()->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function profilUpdateAvatar(Request $request)
    {
        $this->getProgramExist();
        //d($request->all());
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        try{
            if($fileSize <= 4000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName ='assets/uploads/users/';
                $picture = Str::random(6).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->back()->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 4Mb');
            endif;

        }catch (\Exception $e){
            //dd($e->getMessage());
        }

    }


    //FOURNISSEURS

    public function fours()
    {
        $fournisseurs = ProgrammeFournisseur::get();
        return view('chef_projet.fournisseurs.index',compact('fournisseurs'));
    }

    public function storeFours(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $new = new ProgrammeFournisseur();
            $new->programme_id = $idprogram;
            $new->nom = $request->nom;
            $new->contact = $request->contact;
            $new->adresse = $request->adresse;
            $new->save();

            return redirect()->back()->with('success','Le fournisseur a été ajouté');
        }

    }

    public function updateFours(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'slug' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $new = ProgrammeFournisseur::where('id',$request->slug)->where('programme_id',$idprogram)->first();
            if($new){
                //$new->programme_id = $idprogram;
                $new->nom = $request->nom;
                $new->contact = $request->contact;
                $new->adresse = $request->adresse;
                $new->save();
            }


            return redirect()->back()->with('success','Le fournisseur a été mise a jour');
        }
    }

    public function delFours($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $new = ProgrammeFournisseur::where('id',$slug)->where('programme_id',$idprogram)->first();
        if($new){
            $new->delete();
            return redirect()->back()->with('success','Le fournisseur a été supprimé');
        }
        return redirect()->back();
    }

    //TRANSACTIONS
    public function transaction()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //lists transactions
        $lists = Transaction::where('programme_id',$idprogram)->with('lot')->with('soustraitant')->orderBy('id','desc')->get();
        //$lists = Avancement::where('programme_id',$idprogram)->select('lot_id','soustraitant_id',DB::raw('round(SUM(cout)) as coutt'))->groupBy('lot_id')->groupBy('soustraitant_id')->with('lot')->with('soustraitant')->get();
        //dump($lists);

        return view('chef_projet.transactions',compact('lists'));
    }

    public function transactionWaiting()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;
        $lists = Transactiontest::where('programme_id',$idprogram)->where('nivalid',$niv)->where('etat', 1)->with('lot')->with('contrat')->with('soustraitant')->get();
        //dd($lists);
        return view('chef_projet.transactions_wainting',compact('lists'));
    }

    public function transactionRejeter()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $program = Programme::where('id',$idprogram)->first();

        //Auth::user()->role_id == 3 ? $niv = 1 : $niv = 2 ;
        $lists = Transactiontest::where('programme_id',$idprogram)->where('etat', 0)->with('lot')->with('contrat')->with('soustraitant')->get();
        //dd($lists);
        return view('chef_projet.transactions_rejeter',compact('lists'));
    }

    public function transactionWaitingShow($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $trans = Transactiontest::where('programme_id',$idprogram)->where('id',$id)->with('lot')->with('contrat')->with('soustraitant')->first();
        if($trans){
            $trans->actionbtn = false;
            //show action
            if($trans->nivalid == 1){
                Auth::user()->role_id==3 ? $trans->actionbtn = true : $trans->actionbtn = false;
            }else{
                Auth::user()->role_id==5 ? $trans->actionbtn = true : $trans->actionbtn = false;
            }
            $listrans = Avancementest::where('programme_id',$idprogram)->where('transac_id',$trans->id)->with('soustraitant')->with('corpsetat')->get();
            return view('chef_projet.transaction_waiting_detail',compact('listrans','trans'));
        }
        return redirect()->back();
    }

    public function transactionWaitingValid($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($slug);

        $pgg = Programme::where('id',$idprogram)->first();
        $tans = Transactiontest::where('id',$id)->with('soustraitant')->with('contrat')->first();

        $namst = $tans->soustraitant ? $tans->soustraitant->nom : 'N/A';
        $namstct = $tans->contrat ? $tans->contrat->slug : 'N/A';
        $ttmt = round($tans->montant);
        $ttaux = $tans->taux;

        if(Auth::user()->role_id == 5){
            $tans->etat = 2;
            //save transaction true
            $newtrans = new Transaction();
            $newtrans->slug = $tans->slug;
            $newtrans->programme_id = $tans->programme_id;
            $newtrans->soustraitant_id = $tans->soustraitant_id;
            $newtrans->soustraitantcontrat_id = $tans->soustraitantcontrat_id;
            $newtrans->lot_id = $tans->lot_id;
            $newtrans->taux = $tans->taux;
            $newtrans->montant = $tans->montant;
            $newtrans->user_id = $tans->user_id;
            $newtrans->rattachment = $tans->rattachment;
            $newtrans->save();

            //save avancement
            $avances = Avancementest::where('transac_id',$tans->id)->get();
            foreach ($avances as $avance){
                $new = new Avancement();
                $new->programme_id = $avance->programme_id;
                $new->lot_id = $avance->lot_id;
                $new->soustraitant_id = $avance->soustraitant_id;
                $new->soustraitantcontrat_id = $avance->soustraitantcontrat_id;
                $new->corpsetat_id = $avance->corpsetat_id;
                $new->taux = $avance->taux;
                $new->taux_execute = $avance->taux_execute;
                $new->cout = $avance->cout;
                $new->transac_id = $newtrans->id;
                $new->user_id = $newtrans->user_id;
                $new->save();
            }

            //delete si il le faut apres
            $avanc = Avancementest::where('transac_id',$tans->id)->delete();
            $tans->delete();

            //NOTIFIE BY EMAIL SOUS-TRAITANT PAYE VALIDATION ET NOTIFIER CONTROLER DE GESTION
            $message = "<p>Bonjour Monsieur/Madame</p><p>Votre transaction a bien été validé. Elle est en attente de paiement</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "TRANSACTION VALIDER - GESTION DE STOCK - ".$pgg->libelle;
            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('id',$newtrans->user_id)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);

            //NOTIF CONTROLER DE GESTION
            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un nouveau attachement en attente de paiement.</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "ATTACHEMENT EN ATTENTE DE PAIEMENT - GESTION DE STOCK - ".$pgg->libelle;
            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',8)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);

        }else{
            $tans->nivalid = 2;
            $tans->save();

            //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez une nouvelle transaction en attente de traitement.</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "TRANSACTION EN ATTENTE DE TRAITEMENT - GESTION DE STOCK - ".$pgg->libelle;

            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('look',0)->where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
            //dd($pguser,$user);
            $this->sendMailNotificaton($message,$sujet,$user);
        }

        return redirect()->route('cphome.transac.waiting')->with('success','La transaction a bie été traité');

    }

    public function rejetTransaction(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'stid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $id = decrypt($request->stid);
            $data = Transactiontest::where('id',$id)->with('soustraitant')->with('contrat')->where('programme_id', $idprogram)->first();
            if($data){
                $data->etat = 0;
                $data->motif = $request->motif;
                $data->save();

                $pgg = Programme::where('id',$idprogram)->first();
                $namst = $data->soustraitant ? $data->soustraitant->nom : 'N/A';
                $namstct = $data->contrat ? $data->contrat->slug : 'N/A';
                $ttmt = round($data->montant);
                $ttaux = $data->taux;

                //NOTIFIE BY EMAIL SOUS-TRAITANT VALIDATION
                $message = "<p>Bonjour Monsieur/Madame</p><p>Votre transaction a été rejeté.</p>";
                $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
                $message .= "<p><b>MOTIF : </b>".$request->motif."</p>";
                $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

                $sujet = "TRANSACTION REJETEE - GESTION DE STOCK - ".$pgg->libelle;

                $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
                $user = User::where('id',$data->user_id)->whereIn('id',$pguser)->pluck('email')->toArray();
                // $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
                //dd($pguser,$user);
                $this->sendMailNotificaton($message,$sujet,$user);

                return redirect()->route('cphome.transac.waiting')->with('success' ,'La transaction a bien été rejeté');
            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }


    }

    public function transactionItem($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $trans = Transaction::where('programme_id',$idprogram)->where('id',$id)->with('lot')->with('soustraitant')->first();
        if($trans){
            $listrans = Avancement::where('programme_id',$idprogram)->where('lot_id',$trans->lot_id)->where('soustraitant_id',$trans->soustraitant_id)->whereDate('created_at',$trans->created_at->format('Y-m-d'))->with('lot')->with('soustraitant')->with('corpsetat')->get();
            //dd($listrans);
            return view('chef_projet.transaction_detail',compact('listrans','trans'));
        }
        return redirect()->back();
    }

    public function transactionDel($id)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $id=decrypt($id);
        $lists = Transactiontest::where('programme_id',$idprogram)->where('etat', 0)->where('id',$id)->first();
        if($lists){
            $lists->delete();

            return redirect()->back()->with('success','Transaction supprimée.');
        }
        return redirect()->back()->with('error','Transaction introuvable.');
    }

        //MAIN DOEUVRE
    public function listctst()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $contrats = ProgrammeSoustraitantcontrat::where('programme_id',$programme_id)->has('straitant')->with('straitant')->with('lots')->where('etat',2)->orderBy('id','desc')->get();
        //dd($demandes);
        return view('chef_projet.transaction_liste_create',compact('contrats'));
    }

    //ETATS
    public function etats(){
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        $listlots = ProgrammeLot::where('programme_id',$idprogram)->with('actifs')->orderByRaw('ABS(lot)', 'asc')->get();
        $listlots->map(function($item) use($idprogram){
            if($item->programme->type_rattachement == 1){
                $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            }else{
                //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
                $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$item->id)->has('straitantcontrat')->sum('montant');
            }
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });

        //dd($blocks,$tranches,$soustraitants);
        return view('chef_projet.etats.index',compact('blocks','tranches','soustraitants','listlots'));
    }

    public function etatsItem($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $listlot = ProgrammeLot::where('programme_id',$idprogram)->where('id',$slug)->with('actifs')->first();
        //$montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$listlot->actif_id)->sum('prix');
        if($listlot->programme->type_rattachement == 1){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$listlot->actif_id)->sum('prix');
        }else{
            //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
            $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$listlot->id)->has('straitantcontrat')->sum('montant');
        }

        $idtranspay = Transaction::where('programme_id',$idprogram)->where('status',2)->pluck('id')->toArray();
        $montbuy = Avancement::where('lot_id',$listlot->id)->whereIn('transac_id',$idtranspay)->sum('cout');
        //dump($montantLot,$montbuy);
        $stats =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            $data= Avancement::where('lot_id',$listlot->id)->whereYear('created_at',date('Y'))->whereMonth('created_at',$month)->where('programme_id',$idprogram)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
            $stats[$month] =$data->coutpaye;
        }
        //dd($listlot);
        //
        return view('chef_projet.etats.show',compact('listlot','montantLot','montbuy','stats'));
    }

    public function etatsSearch(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        /*if(empty($request->tranche) and empty($request->bloc) and empty($request->soustraitant)){
            return redirect()->back();
        }*/
        $pg = ProgrammeLot::where('programme_id',$idprogram);

        if(isset($request->soustraitant)){

            $pgst = LotSoustraitant::where('soustraitant_id',$request->soustraitant)->pluck('lot_id')->toArray();
            $pg = ProgrammeLot::whereIn('id',$pgst)->with('actifs');

            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }

        }else{
            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }
        }

        $listlots = $pg->get();
        $listlots->map(function($item) use($idprogram){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });
        //dd($listlots);
        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        return view('chef_projet.etats.index',compact('blocks','tranches','soustraitants','listlots'));
    }

}
