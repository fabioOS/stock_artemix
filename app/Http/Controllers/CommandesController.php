<?php

namespace App\Http\Controllers;

use App\Commande;
use App\CommandeItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\ProgrammeLigneBudgetaire;
use App\ProgrammePivotUser;
use App\ProgrammeSous;
use App\Typeproduit;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;


class CommandesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getRefBss()
    {
        $programme_id = $this->getIdProgram();

        $code = "C".date('ym');
        $count = Commande::where('programme_id',$programme_id)->withTrashed()->count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$programme_id.'-'.$num;

        return $val;
    }

    public function create() {
        $programme_id = $this->getIdProgram();
        $blade = $this->getBlade(Auth::user()->role_id);

        $types = Typeproduit::with('produits')->get();
        $lignbudgets = ProgrammeLigneBudgetaire::where('programme_id',$programme_id)->orderBy('libelle','asc')->get();
        $sousprograms = ProgrammeSous::where('programme_id',$programme_id)->get();

        return view('commandes.create', compact('blade','sousprograms','types','lignbudgets'));
    }


    public function store(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'lignid' =>'required',
            'description' =>'required',
            'datasession' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }

        //Verifier si le programmeSous existe
        $programsous = ProgrammeSous::where('programme_id',$this->getIdProgram())->count();
        if($programsous>0){
            $valider = Validator::make($request->all(),[
                'sousprogramme_id' =>'required',
            ]);

            if($valider->fails()){
                return redirect()->back()->withErrors($valider->errors());
            }
        }

        try {
            $programme_id = $this->getIdProgram();

            if(count($request->datasession)>0) {
                $i=0;
                //creer la commande
                $command = new Commande();
                $command->ligne_id = $request->lignid;
                $command->programme_id = $programme_id;
                $command->programmesous_id = $request->sousprogramme_id ?? null;
                $command->description = $request->description;
                $command->ref = $this->getRefBss();
                $command->user_id = Auth::user()->id;
                $command->save();

                foreach($request->datasession as $data) {
                    $item = explode(';',$data);
                    //Save demande item
                    if((float) $item[3] > 0) {
                        //check exitance
                        $check1 = CommandeItem::where('commande_id',$command->id)->where('produit_id',$item[0])->first();
                        if(!$check1){
                            $cmditem = new CommandeItem();
                            $cmditem->programme_id = $programme_id;
                            $cmditem->commande_id = $command->id;
                            $cmditem->produit_id = $item[0];
                            $cmditem->qte = $item[3];
                            $cmditem->save();
                        }

                        $i++;
                    }
                }
            }

            if($i>0){
                if(Auth::user()->role_id == 6){
                    $command->status = 2;
                    $command->nivalid = 2;
                    $command->save();
                    return redirect()->back()->with('success', "Demande de <b>$i</b> matières ajouté avec succès !");
                }
                //type de batiment
                $lign = ProgrammeLigneBudgetaire::where('id',$request->lignid)->where('programme_id',$this->getIdProgram())->first();
                $typbtmt = $lign->type;

                $pgg = Programme::where('id',$programme_id)->first();
                //NOTIFIER LE CHEF PROJET QUE LA DEMANDE EST EN ATTENTE
                $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Vous avez une nouvelle commande en attente de traitement.</p>";
                $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br>";
                $mess .= "<b>REFERENCE : </b>".$command->ref."<br>";
                $mess .= "<b>NOMBRE DE MATIERE DEMANDEE : </b>".$i."</p>";
                $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

                $sujet = "NOUVELLE COMMANDE EN ATTENTE - ".$pgg->libelle." - GSTOCK";

                $table = $typbtmt == 1 ? 'batiment' : 'vrd';
                $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->where($table,1)->pluck('user_id');

                $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
                $this->sendMailNotificaton($mess,$sujet,$user);

                return redirect()->back()->with('success', "Demande de <b>$i</b> matières ajouté avec succès !");
            }else{
                return back()->with('error', "Aucune matières n'a été ajouté dû à une erreur de valeur(s) !");
            }

        }catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }

    }


}
