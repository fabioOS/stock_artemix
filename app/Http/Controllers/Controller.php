<?php

namespace App\Http\Controllers;

use App\BDSpec;
use App\Bon_demande;
use App\Bon_retour;
use App\Transactiontest;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendMailNotificaton($text,$sujet,$emails)
    {

        $data = array(
            'texts' => $text
        );
        try{

            if(count($emails)>0){
                Mail::send('mails.index',$data,function ($message) use($emails,$sujet){
                    $message->to($emails);
                    $message->subject($sujet);
                });
            }
        }catch (\Exception $e){
            Session::flash('error','Erreur lors de l\'envoi de mail');
        }
    }

    //GENERER UNE REF DEMANDE SPECIAL
    public function getRefDS()
    {
        $programme_id = $this->getIdProgram();

        $code = "DS".date('ym');
        $count = BDSpec::withTrashed()->where('programme_id',$programme_id)->count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$programme_id.'-'.$num;

        return $val;
    }

    //GENERER UNE REF BON DE SORTIR DU STOCK
    public function getRefBss()
    {
        $code = "BS".date('ym');
        $count = Bon_demande::count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$num;

        return $val;
    }

    //GENERER UNE REF POUR LES EVALUTIONS DE MAIN DOEUVRE
    public function getNumTrans($idpgr)
    {
        $programme_id = $idpgr;
        $code = "TR".date('ym');
        $count = Transactiontest::where('programme_id',$programme_id)->orderBy('id','desc')->first();
        $count = $count ? $count->id + 1 : 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$programme_id.'-'.$num;

        return $val;
    }

    //GENERER UNE REF BON DE RETOUR DU STOCK
    public function getRefBRetour()
    {
        $code = "BRT".date('ym');
        $count = Bon_retour::count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$num;

        return $val;
    }

    //GENERER UN FORMAT FRANCAIS POUR LES PRIX
    public function formatPrice($price = 0)
    {
        return number_format(str_replace(' ', '', $price), 0, '', ' ');
    }

    //SUPPRIMER LES ESPACES DANS LES PRIX
    public function deleteSpacePrice($price){
        if($price == null){
            return 0;
        }
        $price = str_replace(' ','',$price);
        $price = str_replace('FCFA','',$price);
        $price = str_replace('F CFA','',$price);
        $price = str_replace('F.CFA','',$price);
        $price = str_replace('F.C.F.A','',$price);

        return $price;
    }

    //RECUPERER LE BLADE EN FONCTION DU ROLE
    public function getBlade($roleid) {
        switch ($roleid) {
            case 1:
                //Administrateur
                return 'admin';
                break;
            case 2:
                //conducteur-des-travaux
                return 'chefchantier.layouts.app';
                break;
            case 3:
                //chef-projet
                return 'chef_projet.layouts.app';
                break;
            case 4:
                //gestionnaire-de-stock
                return 'magasinier.layouts.app';
                break;
            case 5:
                //DP
                return 'chef_projet.layouts.app';
                break;
            case 6:
                //service-achat
                return 'service_achat.layout';
                break;
            case 7:
                //daf
                return 'daf.layout';
                break;
            case 8:
                //controleur-gestion
                return 'daf.layout';
                break;
            case 9:
                //Comptabilite
                return 'daf.layout';
                break;
            default:
                return redirect()->route('login')->send();
                break;

        };
    }


    //RECUPERER LE TYPE DE BATIMENT OU VRD EN FONCTION DU PROGRAMME
    public function getTypeBatiment($userid,$programme_id)
    {
        $user = User::where('id',$userid)->with('programmPivot')->first();
        $userpivot = $user->programmPivot->where('programme_id',$programme_id)->first();

        if($userpivot->batiment== null && $userpivot->vrd == null){
            $typbtmt = [0];
        }elseif (isset($userpivot->batiment) && isset($userpivot->vrd)){
            $typbtmt = [1,2];
        }elseif ($userpivot->batiment == 1){
            $typbtmt = [1];
        }elseif ($userpivot->vrd == 1){
            $typbtmt = [2];
        }else{
            $typbtmt = [0];
        }

        return $typbtmt;
    }
}
