<?php

namespace App\Http\Controllers\Daf;

use App\Exports\ProduitAffectationExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\ProduitAffectationImport;
use App\Produit;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class AffectationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getAffectionCG(){
        $nb = Produit::whereNull('code_affect')->count();
        return $nb;
    }

    public function index() {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $affectations = Produit::with('type')->orderBy('code_affect','asc')->orderBy('libelle','asc')->get();
        return view('daf.affectations.index',compact('affectations'));
    }

    public function model()
    {
        $this->getProgramExist();
        $programId = $this->getIdProgram();

        try{
            //EXPORTATION
            $produits = Produit::with('type')->has('type')->orderBy('libelle','asc')->get();

            $date=new \DateTime('now');
            $date = $date->format('d-m-Y');
            $lib= "LISTE-AFFECTION-CG-".$date.'.xlsx';
            return Excel::download(new ProduitAffectationExport($produits), $lib);

            return redirect()->back()->with('success', 'Votre fichier a bien été téléchager.');

        }catch(\Exception $e){
            return redirect()->back()->with('error', 'une erreur s\'est produite lors de l\'envoi du fichier');
        }

    }

    public function import(Request $request)
    {
        $this->getProgramExist();
        $programId = $this->getIdProgram();
        // dd($request->all());

        $request->validate([
            'ufile' => 'required|mimes:xlsx,xls,csv',
        ],[
            'ufile.required' => 'Veuillez importer un fichier',
            'ufile.mimes' => 'Le fichier doit être de type: xlsx, xls ou csv',
        ]);

        if(isset($programId)){
            $filedata = Excel::toCollection(new ProduitAffectationImport, $request->file('ufile'));
            $filedata = $filedata[0]->toArray();
            if(count($filedata) > 0) {
                foreach($filedata as $key => $row) {
                    if($key == 0 ) {
                        //dd($row);
                        if($row[0] != 'NUM' || $row[1] != "CODE" || $row[2] != "CATEGORIE" || $row[3] != "PRODUIT" || $row[4] != "UNITE" || $row[5] != "AFFECTATION" ) {
                            return redirect()->back()->with('error' ,"le format de fichier n'est pas conforme au modèle de base.");
                        }
                    }else{
                        $check = Produit::where('id',$row[0])->first();
                        if(isset($check->id)){
                            $check->code_affect = $row[5]? $row[5] : null;
                            $check->save();
                        }
                    }
                }

                return redirect()->back()->with('success' ,"Le fichier a bien été importé.");

            }else {
                return redirect()->back()->with('error' ,"Veuillez importer le fichier d'origine.");
            }
        }

    }


    public function store(Request $request){
        $this->getProgramExist();
        $programId = $this->getIdProgram();
        // dd($request->all());
        $request->validate([
            'pdt' => 'required',
            'affectation' => 'required',
        ],[
            'pdt.required' => 'Veuillez choisir un produit',
            'affectation.required' => 'Veuillez choisir une affectation',
        ]);

        $produit = Produit::find($request->pdt);
        if(!$produit){
            return redirect()->back()->with('error','Produit introuvable');
        }

        $produit->code_affect = $request->affectation;
        $produit->save();

        return redirect()->back()->with('success','Affectation enregistrée avec succès');
    }

    public function update(Request $request){
        $this->getProgramExist();
        $programId = $this->getIdProgram();

        // dd($request->all());
        $request->validate([
            'slug' => 'required',
            'affectation' => 'required',
        ],[
            'slug.required' => 'Veuillez choisir un produit',
            'affectation.required' => 'Veuillez choisir une affectation',
        ]);

        $produit = Produit::find($request->slug);
        if(!$produit){
            return redirect()->back()->with('error','Produit introuvable');
        }

        $produit->code_affect = $request->affectation;
        $produit->save();

        return redirect()->back()->with('success','Affectation enregistrée avec succès');

    }
}
