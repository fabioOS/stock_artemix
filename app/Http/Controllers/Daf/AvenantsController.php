<?php

namespace App\Http\Controllers\Daf;

use App\Avenant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paiement;
use App\Programme;
use App\ProgrammePivotUser;
use App\ProgrammeSoustraitantcontrat;
use App\Transaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AvenantsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getAvenantStatus() {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $nb = Avenant::where('programme_id',$idprogram)->where('status',1)->count();
        return $nb ;
    }

    public function index($type = null) {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        if($type == 'waiting'){
            $typelibel = 'en attente';
            $avenants = Avenant::where('programme_id',$programme_id)->where('status',1)->get();
        }elseif($type == 'validate'){
            $typelibel = 'valider';
            $avenants = Avenant::where('programme_id',$programme_id)->where('status',2)->get();
        }elseif($type == 'rejected'){
            $typelibel = 'rejeter';
            $avenants = Avenant::where('programme_id',$programme_id)->where('status',0)->get();
        }else{
            $typelibel = '';
            $avenants = Avenant::where('programme_id',$programme_id)->get();
        }

        $avenants->map(function ($item) use ($programme_id) {
            $mtpaie = Transaction::where('programme_id', $programme_id)->where('soustraitantcontrat_id', $item->soustraitantcontrat_id)->where('soustraitant_id', $item->soustraitant_id)->where('status', 2)->sum('montant');
            $item->totalpayer = $mtpaie;
        });

        return view('daf.avenants.index',compact('avenants','typelibel'));
    }

    public function create() {
        //dd('dd');
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $contrats = ProgrammeSoustraitantcontrat::where('programme_id',$programme_id)->has('straitant')->with('straitant')->with('lots')->where('etat',2)->orderBy('id','desc')->get();
        $contrats->map(function ($item) use ($programme_id) {
            $mtpaie = Transaction::where('programme_id', $programme_id)->where('soustraitantcontrat_id', $item->id)->where('soustraitant_id', $item->soustraitant_id)->where('status', 2)->sum('montant');
            $item->totalpayer = $mtpaie;
        });

        return view('daf.avenants.create',compact('contrats'));
    }

    public function store(Request $request) {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        //dd($request->all());
        //make validation
        $this->validate($request, [
            'demande' => 'required',
            'file' => 'required',
            'price' => 'required',
        ], [
            'demande.required' => 'Le contrat est obligatoire',
            'file.required' => 'La fiche numrérique est obligatoire',
            'price.required' => 'Le montant est obligatoire',
        ]);

        //Récupérer le contrat
        $id = decrypt($request->demande);
        $contrat = ProgrammeSoustraitantcontrat::where('id',$id)->with('straitant')->first();
        //dd($contrat);
        if(!$contrat){
            return redirect()->back()->with('error','Le contrat n\'existe pas !');
        }

        $mttavenant = str_replace(' ', '', $request->price);
        //Récupérer le montant total payé
        $mtpaie = Transaction::where('programme_id', $programme_id)->where('soustraitantcontrat_id', $contrat->id)->where('soustraitant_id', $contrat->soustraitant_id)->where('status', 2)->sum('montant');
        if($mttavenant > ($contrat->montantctr - $mtpaie)){
            return redirect()->back()->with('error','Le montant de l\'avenant ne doit pas dépasser le montant restant du contrat !');
        }
        //Save avenant
        $avenant = new Avenant();
        $avenant->user_id = Auth::user()->id;
        $avenant->programme_id = $programme_id;
        $avenant->soustraitant_id = $contrat->soustraitant_id;
        $avenant->soustraitantcontrat_id = $contrat->id;
        $avenant->montant = $mttavenant;
        $avenant->note = $request->note?? null;
        if($request->hasFile('file')){
            $file=$request->file('file');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName ='assets/uploads/avenants/';
            $picture = date('dmy').'-'.Str::random(6).'.'. $extension;
            $file->move($folderName,$picture);

            $avenant->file = $picture;
        }
        $avenant->save();

        //notifier la daf d'un avenant en attente
        //NOTIF DAF PAIEMENT
        $pgg = Programme::where('id',$programme_id)->first();
        $namst = $contrat->straitant ? $contrat->straitant->nom : 'N/A';
        $namstct = $contrat->slug;
        $ttmt = $request->price;

        $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un nouveau avenant en attente de paiement.</p>";
        $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA</p>";
        $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
        $sujet = "AVENANT EN ATTENTE DE PAIEMENT - GESTION DE STOCK - ".$pgg->libelle;
        $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
        $user = User::where('role_id',7)->whereIn('id',$pguser)->pluck('email')->toArray();
        $this->sendMailNotificaton($message,$sujet,$user);

        return redirect()->route('daf.avenants','waiting')->with('success','Avenant enregistré avec succès !');
    }


    public function show($id) {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $item = Avenant::where('programme_id',$programme_id)->where('id',$id)->with("contrat")->with("soustraitant")->with("user")->first();
        if(!$item){
            return redirect()->back()-with('error',"L'avenant est introuvable");
        }
        $mtpaie = Transaction::where('programme_id', $programme_id)->where('soustraitantcontrat_id', $item->soustraitantcontrat_id)->where('soustraitant_id', $item->soustraitant_id)->where('status', 2)->sum('montant');
        $item->totalpayer = $mtpaie;

        $item->actionbtn = false;
        if(Auth::user()->role_id==7){
            $item->actionbtn = true;
        }

        return view('daf.avenants.show',compact('item'));
    }

    public function valid(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        Validator::make($request->all(),[
            'transid' =>'required',
            'moyen' =>'required',
        ],[
            'transid.required' =>'L\'avenant est obligatoire',
            'moyen.required' =>'Le moyen de paiement est obligatoire',
        ])->validate();

        $id = decrypt($request->transid);
        $tran = Avenant::where('programme_id',$idprogram)->where('id',$id)->first();
        $tran->status = 2;

        //Save transaction
        $newtrans = new Transaction();
        $newtrans->slug = 'TR-AVENANT-'.date('ym').'-'.Str::random(6);
        $newtrans->status = 2;
        $newtrans->programme_id = $idprogram;
        $newtrans->soustraitant_id = $tran->soustraitant_id;
        $newtrans->soustraitantcontrat_id = $tran->soustraitantcontrat_id;
        $newtrans->lot_id = 0;
        $newtrans->taux = 0;
        $newtrans->montant = $tran->montant;
        $newtrans->user_id = Auth::user()->id;
        $newtrans->save();
            //save paiement historique
            $paie = new Paiement();
            $paie->transac_id = $newtrans->id;
            $paie->programme_id = $idprogram;
            $paie->montant = $tran->montant;
            $paie->taux_execute = 0;
            $paie->user_id = Auth::user()->id;
            $paie->moyen = $request->moyen;
            $tran->modepaie = $request->moyen;

            if($request->hasFile('bordereau')){
                $file=$request->file('bordereau');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName ='assets/uploads/paiements/';
                $picture = 'avenant-'.Str::random(10).'.'. $extension;
                $file->move($folderName,$picture);

                $paie->file = $picture;
                $tran->filepaie = $picture;
            }
            $paie->save();

        $tran->save();

        //Notifier
        return redirect()->back()->with('success','Le paiement a bien été effectué.');
    }

    public function rejeter(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        Validator::make($request->all(),[
            'motif' =>'required',
            'stid' =>'required',
        ],[
            'motif.required' =>'Le motif est obligatoire',
            'stid.required' =>'L\'avenant est obligatoire',
        ])->validate();


        $id = decrypt($request->stid);
        $data = Avenant::where('programme_id',$programme_id)->where('id',$id)->first();
        //dd($data);
        if($data){
            $data->status = 0;
            $data->motif = $request->motif;
            $data->save();

            $pgg = Programme::where('id',$programme_id)->first();
            $message = "<p>Bonjour Monsieur/Madame </p><p>Votre demande d'avenant n°<b>". $data->id."</b> a été rejetée. ";
            $message .= "<br> Veuillez trouver ci-dessous le motif de rejet : </p><p>“ ".$data->motif." ”</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "AVENANT REJETE - G-STOCK";

            $user = User::where('id',$data->user_id)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->back()->with('success' ,'La demande a bien été rejetée.');

        }else{
            return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
        }
    }

}
