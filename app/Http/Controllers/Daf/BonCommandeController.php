<?php

namespace App\Http\Controllers\Daf;

use App\BC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use PDF;

class BonCommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getByStatus($status)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('typebc',2)->where('status',$status)->count();
        return $bcs;
    }


    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('typebc',2)->where('status',1)->with('demande')->with('fournisseur')->with('user')->with('items')->orderBy('id','desc')->get();

        return view("daf.bc.waiting",compact('bcs'));
    }

    public function valider()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',2)->with('demande')->with('fournisseur')->with('user')->with('items')->orderBy('id','desc')->get();

        return view("daf.bc.valider",compact('bcs'));
    }

    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($slug);
        $demande = BC::where('programme_id',$programme_id)->where('id',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        return view("daf.bc.show",compact('demande'));

    }

    public function showPdf($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($slug);
        $data = BC::where('programme_id',$programme_id)->where('ref',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        //dd($data);

        $pdf = PDF::loadView('service_achat.bc.bc_download',compact('data'));
        //$pdf->setPaper('a4', 'landscape');
        //return view('service_achat.bc.bc_download',compact('data'));
        //return $pdf->stream('etat_gestion_stock-'.date("d-m-y").'.pdf');
        return $pdf->download('bon-commande-'.$id.'.pdf');

    }

}
