<?php

namespace App\Http\Controllers\Daf;

use App\BC;
use App\BDSpec;
use App\FactureFournisseur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paiement;
use App\ProgrammeFournisseur;
use App\ProgrammeSoustraitant;
use App\Transaction;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FacturactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIdProgram()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }

        if($dataprogram){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function soustraitant()
    {
        $idprogram = $this->getIdProgram();
        $paiements = Transaction::with('soustraitant')->where('etat',2)->where('programme_id',$idprogram)->where('status',3)->orderBy('id','asc')->get();
        $last10 = Paiement::has('transaction')->with(['transaction'=>function($q){
            $q->with('soustraitant');
        }])->where('programme_id',$idprogram)->where('type_paie',1)->orderBy('id','desc')->take(6)->get();

        return view('daf.facturations.soustraitants.index',compact('paiements','last10'));
    }

    //FOURNISSEUR
    //FOURNISSEUR
    //FOURNISSEUR

    public function fournisseur()
    {
        $idprogram = $this->getIdProgram();
        $datas = FactureFournisseur::where('programme_id',$idprogram)->with('user','fournisseur','boncommande')->orderBy('id','desc')->get();
        $last10 = Paiement::has('facture')->with(['facture'=>function($q){
            $q->with('fournisseur');
        }])->where('programme_id',$idprogram)->where('type_paie',2)->orderBy('id','desc')->take(6)->get();

        return view('daf.facturations.fournisseurs.index',compact('datas','last10'));
    }

    public function createFactFourns() {
        $idprogram = $this->getIdProgram();
        $boncommandes = BC::where('programme_id',$idprogram)->where('status_fourns',0)->where('status',2)->orderBy('id','desc')->get();
        return view('daf.facturations.fournisseurs.create',compact('boncommandes'));
    }

    public function storeFactFourns(Request $request){
        // dd($request->all());
        $request->validate([
            'demande' => 'required',
            'numfacture' => 'required',
            'datefacture' => 'required | date',
            'montant' => 'required',
            'file' => 'required',
        ],[
            'demande.required' => 'Veuillez selectionner une demande',
            'numfacture.required' => 'Veuillez entrer le numero de la facture',
            'datefacture.required' => 'Veuillez entrer la date de la facture',
            'datefacture.date' => 'La date de la facture est invalide',
            'montant.required' => 'Veuillez entrer le montant de la facture',
            'file.required' => 'Veuillez selectionner un fichier',
        ]);

        $idprogram = $this->getIdProgram();
        $montt_facture = str_replace(' ','',$request->montant);

        //Verification de l'existence de la demande et si le montant de la facture est inferieur ou egal au montant de la demande
        $demande = BC::where('programme_id',$idprogram)->where('id',$request->demande)->where('status_fourns',0)->where('status',2)->with('factureFournisseur')->first();
        // dd($demande);
        if(!$demande){
            return redirect()->back()->with('error','Cette demande est introuvable');
        }

        if($demande->ibution_id){
            $mtt_bc = $demande->montant;
        }else{
            if($demande->tva == "AVEC"){
                $mtt_bc = ($demande->montant *0.18) + $demande->montant;
            }elseif($demande->retenu){
                $mtt_bc = $demande->montant - $demande->retenu;
            }else{
                $mtt_bc = $demande->montant;
            }
        }

        //Recuperation du montant total des factures deja enregistrées pour cet bon de commande
        $montantfactures = $demande->factureFournisseur->sum('montant');
        $montantfacturesrecu = $montantfactures + $montt_facture;

        if($montantfacturesrecu > $mtt_bc){
            $msg = 'Le montant de la facture ne doit pas depasser le montant TTC du BC <br> Montant du BC: '.$this->formatPrice($mtt_bc).' <br> Montant des factures: '.$this->formatPrice($montantfactures) .' <br> Montant restant: '.$this->formatPrice($mtt_bc - $montantfactures);
            return redirect()->back()->with('error',$msg);
        }

        $facture = new FactureFournisseur();
        $facture->programme_id = $idprogram;
        $facture->ref = FactureFournisseur::getRefFactFourns($idprogram);
        $facture->bc_id = $demande->id;
        $facture->fournisseur_id = $demande->fournisseur_id;
        $facture->user_id = auth()->user()->id;
        $facture->numfacture = $request->numfacture;
        $facture->datefacture = $request->datefacture;
        $facture->montant = $montt_facture;
        // dd($facture);
        if($request->hasFile('file')){
            $file=$request->file('file');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName ='assets/uploads/factures/fournisseurs/';
            $picture = Str::random(10).'.'. $extension;
            $file->move($folderName,$picture);
            $facture->fichier = $picture;
        }
        $facture->save();

        //Mise a jour du statut du BC si le montant des factures est egal au montant du BC
        if($montantfacturesrecu >= $mtt_bc){
            $demande->status_fourns = 1;
            $demande->save();
        }

        return redirect()->route('daf.facturation.fournisseur')->with('success','La facture a été enregistrée avec succès');

    }


    //FOURNISSEUR END

    public function waiting(Request $request)
    {
        $idprogram = $this->getIdProgram();

        if(!$request->type){
            return redirect()->back()->with('error','Veuillez selectionner un type de paiement');
        }

        $type = $request->type;
        $datas = [];

        if($type == 'soustraitant'){
            $paiements = Transaction::where('etat',2)->where('programme_id',$idprogram)->where('status',3)->orderBy('id','asc')->get();
            //Selectionnez les soustraitants qui n'ont pas encore été payés
            $usersid = $paiements->pluck('soustraitant_id');
            $users = ProgrammeSoustraitant::where('programme_id',$idprogram)->whereIn('id',$usersid)->get();
            // dd($users);
            foreach ($users as $k=>$user){
                $datas[$k]['id'] = $user->id;
                $datas[$k]['nom'] = $user->nom;
                $datas[$k]['contact'] = $user->contact;
            }

        }else{
            $factures = FactureFournisseur::where('programme_id',$idprogram)->where('status',1)->with('fournisseur')->groupBy('fournisseur_id')->get();
            foreach ($factures as $k=>$facture){
                $datas[$k]['id'] = $facture->fournisseur->id;
                $datas[$k]['nom'] = $facture->fournisseur->nom;
                $datas[$k]['contact'] = $facture->fournisseur->contact;
            }

        }

        return view('daf.facturations.paiement',compact('datas','type'));
    }

    public function getAppel(Request $request){
        // dd($request->all());
        $idprogram = $this->getIdProgram();

        if(!$request->type && !$request->userid){
            return redirect()->back()->with('error','Veuillez selectionner un type de paiement');
        }

        $type = $request->type;
        $datas = [];
        $iduser = decrypt($request->userid);

        if($type == 'soustraitant'){
            $paiements = Transaction::where('etat',2)->where('programme_id',$idprogram)->where('status',3)->orderBy('id','asc')->get();
            $usersid = $paiements->pluck('soustraitant_id');
            $paiements = $paiements->where('soustraitant_id',$iduser);
            $users = ProgrammeSoustraitant::where('programme_id',$idprogram)->whereIn('id',$usersid)->get();

            foreach ($users as $k=>$user){
                $datas[$k]['id'] = $user->id;
                $datas[$k]['nom'] = $user->nom;
                $datas[$k]['contact'] = $user->contact;
            }

        }else{
            //Fournisseur
            $paiements = FactureFournisseur::where('status',1)->where('programme_id',$idprogram)->where('fournisseur_id',$iduser)->with('fournisseur')->orderBy('id','asc')->get();
            $factures = FactureFournisseur::where('programme_id',$idprogram)->where('status',1)->with('fournisseur')->groupBy('fournisseur_id')->get();
            foreach ($factures as $k=>$facture){
                $datas[$k]['id'] = $facture->fournisseur->id;
                $datas[$k]['nom'] = $facture->fournisseur->nom;
                $datas[$k]['contact'] = $facture->fournisseur->contact;
            }
        }

        return view('daf.facturations.paiement',compact('datas','type','paiements'));
    }

    public function storepaiefourn(Request $request){
        // dd($request->all());
        $request->validate([
            'transid' =>'required',
            'moyen' =>'required',
        ],[
            'transid.required' => 'Veuillez selectionner une facture',
            'moyen.required' => 'Veuillez selectionner un moyen de paiement',
        ]);

        $idprogram = $this->getIdProgram();
        $id = decrypt($request->transid);
        $facture = FactureFournisseur::where('id',$id)->where('status',1)->where('programme_id',$idprogram)->first();
        if(!$facture){
            return redirect()->back()->with('error','Cette facture est introuvable');
        }


        //Save paiement historique
        $paie = new Paiement();
        $paie->transac_id = $facture->id;
        $paie->programme_id = $idprogram;
        $paie->montant = $facture->montant;
        $paie->taux_execute = 0;
        $paie->user_id = auth()->user()->id;
        $paie->moyen = $request->moyen;
        $paie->type_paie = 2;

        if($request->hasFile('bordereau')){
            $file=$request->file('bordereau');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName ='assets/uploads/paiements/';
            $picture = Str::random(10).'.'. $extension;
            $file->move($folderName,$picture);

            $paie->file = $picture;
        }

        $paie->save();

        $facture->status = 2;
        $facture->save();

        return redirect()->route('daf.facturation.fournisseur')->with('success','Le paiement a été enregistré avec succès');

    }


    //HISTORIQUE PAIEMENT
    //HISTORIQUE PAIEMENT
    //HISTORIQUE PAIEMENT

    public function historique($type){
        $idprogram = $this->getIdProgram();

        if($type=="soustraitant"){
            $datas = Paiement::where('programme_id',$idprogram)->with('user')->has('transaction')
            ->with(['transaction'=>function($q){$q->with('soustraitant');}])
            ->where('type_paie',1)->orderBy('id','desc')->get();

        }elseif($type=="demandespec"){
            $datas = BDSpec::where('programme_id',$idprogram)->has('transaction')->with('transaction')->where('status',3)->orderBy('id','asc')->get();
        }else{
            $datas = Paiement::where('programme_id',$idprogram)->with('user')
            ->has('facture')->with(['facture'=>function($q){$q->with('fournisseur');}])
            ->where('type_paie',2)->orderBy('id','desc')->get();
        }

        return view('daf.facturations.historique',compact('datas','type'));
    }


    //DEMANDE SPECIALE
    //DEMANDE SPECIALE
    //DEMANDE SPECIALE
    public function dmdSpec(){
        $idprogram = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$idprogram)->where('status',2)->where('genere',1)->orderBy('id','asc')->get();
        return view('daf.facturations.demandespeciales.index',compact('demandes'));
    }


}
