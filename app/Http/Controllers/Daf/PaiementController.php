<?php

namespace App\Http\Controllers\Daf;

use App\Avancement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paiement;
use App\Programme;
use App\ProgrammeCorpsetat;
use App\ProgrammeLot;
use App\ProgrammePivotUser;
use App\ProgrammeSoustraitantcontrat;
use App\Transaction;
use App\TransactionPrelevement;
use App\TypePrelevement;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PaiementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $paiements = Transaction::where('etat',2)->where('programme_id',$idprogram)->where('status',1)->orderBy('id','asc')->get();

        return view('daf.paiements.index',compact('paiements'));
    }

    public function show($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $trans = Transaction::where('programme_id',$idprogram)->where('id',$id)->with('lot')->with('contrat','soustraitant')->with(['prelevement'=>function($q){
            $q->with('typePrelevement');
        }])->first();
        if($trans){
            $listrans = Avancement::where('programme_id',$idprogram)->where('transac_id',$trans->id)->with('soustraitant')->with('corpsetat')->get();
            return view('daf.paiements.show',compact('listrans','trans'));
        }
        return redirect()->back();
    }

    public function validPaie($slug)
    {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $pgg = Programme::where('id',$idprogram)->first();

        $trans = Transaction::where('programme_id',$idprogram)->where('id',$id)->first();

        if($trans and Auth::user()->role_id==8){
            $trans->status= 3;
            $trans->save();

            $namst = $trans->soustraitant ? $trans->soustraitant->nom : 'N/A';
            $namstct = $trans->contrat ? $trans->contrat->slug : 'N/A';
            $ttmt = $trans->montant;
            $ttaux = $trans->taux;

            //notifier la daf d'un paiement en attente
            //NOTIF DAF PAIEMENT
            $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un nouveau attachement en attente de paiement.</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br><b>MONTANT : </b>".$ttmt." FCFA<br><b>TAUX : </b>".$ttaux." %</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "ATTACHEMENT EN ATTENTE DE PAIEMENT - GESTION DE STOCK - ".$pgg->libelle;
            $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
            $user = User::where('role_id',7)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('daf.paie')->with('success','Attachement a bien été traité');

        }

        return redirect()->back();
    }

    public function waiting()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $paiements = Transaction::with('prelevement')->where('etat',2)->where('programme_id',$idprogram)->where('status',3)->orderBy('id','asc')->get();

        return view('daf.paiements.waiting',compact('paiements'));
    }

    public function valid()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $paiements = Transaction::where('programme_id',$idprogram)->where('status',2)->orderBy('id','desc')->get();

        return view('daf.paiements.valid',compact('paiements'));
    }

    public function validspaie(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        Validator::make($request->all(),[
            'transid' =>'required',
            'moyen' =>'required',
        ])->validate();

        $id = decrypt($request->transid);
        $tran = Transaction::where('programme_id',$idprogram)->where('id',$id)->first();
        $tran->status = 2;

        //save paiement historique
        $paie = new Paiement();
        $paie->transac_id = $tran->id;
        $paie->programme_id = $tran->programme_id;
        $paie->montant = $tran->montant;
        $paie->taux_execute = $tran->taux;
        $paie->user_id = Auth::user()->id;
        $paie->moyen = $request->moyen;

        if($request->hasFile('bordereau')){
            $file=$request->file('bordereau');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName ='assets/uploads/paiements/';
            $picture = Str::random(10).'.'. $extension;
            $file->move($folderName,$picture);

            $paie->file = $picture;
        }

        $paie->save();
        $tran->save();

        //Notifier
        return redirect()->back()->with('success','Le paiement a bien été effectué.');
    }

    public function statctr() {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $contrats = ProgrammeSoustraitantcontrat::where('programme_id',$idprogram)->has('straitant')->with('straitant')->with('lots')->where('etat',2)->orderBy('id','desc')->get();
        $contrats->map(function($item) use($idprogram){
            $mtpaie = Transaction::where('programme_id',$idprogram)->where('soustraitantcontrat_id',$item->id)->where('soustraitant_id',$item->soustraitant_id)->where('status',2)->sum('montant');
            $item->totalpaye = $mtpaie;
        });

        // dd($contrats);
        return view('daf.contrats.index',compact('contrats'));
    }

    public function statctrshow($slug) {
        $id = decrypt($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $contrat = ProgrammeSoustraitantcontrat::where('programme_id',$idprogram)->where('id',$id)->with('straitant')->with('lots')->first();
        $mtpaie = Transaction::where('programme_id',$idprogram)->where('soustraitantcontrat_id',$contrat->id)->where('soustraitant_id',$contrat->soustraitant_id)->where('status',2)->sum('montant');
        $contrat->totalpaye = $mtpaie;

        //list des paiements
        $paiements = Transaction::with('soustraitant')->with('lot')->with('user')->where('programme_id',$idprogram)->where('soustraitantcontrat_id',$id)->where('soustraitant_id',$contrat->soustraitant_id)->orderBy('id','desc')->get();
        return view('daf.contrats.show',compact('contrat','paiements'));
    }

    //FONTION CONTROLLER GESTION
    public function etbpaie(){
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $paiements = Transaction::where('etat',2)->where('programme_id',$idprogram)->where('status',1)->orderBy('id','asc')->get();
        $typeprelements = TypePrelevement::orderBy('libelle','asc')->get();

        $check = null;
        if(Request('code')){
            $code = decrypt(Request('code'));
            $check = Transaction::where('etat',2)->where('programme_id',$idprogram)->where('status',1)->where('id',$code)->first();
            if(!$check){
                return redirect()->back()->with('error','Cet attachement est introuvable.');
            }
        }

        return view('daf.etbpaie.create',compact('paiements','check','typeprelements'));
    }

    public function getTrans($idemende)
    {
        $trans = Transaction::where('etat',2)->where('status',1)->where('id',$idemende)->with('lot','contrat','soustraitant','user','paiement')->first();

        if($trans){
            $trans->datefr = date('d/m/Y h:i',strtotime($trans->created_at));
            return $trans;
        }
        return [];
    }

    public function etbpaieStore(Request $request) {
        // dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $valider = Validator::make($request->all(), [
            'demande' => ['required'],
            // 'datasession' =>['required'],
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }

        //Check transaction
        $trans = Transaction::where('etat',2)->where('status',1)->where('id',$request->demande)->first();
        if(!$trans){
            return redirect()->back()->with('error','La demande de paiement n\'existe pas.');
        }

        $mttsm = $trans->montant;
        if(isset($request->datasession)){
            foreach ($request->datasession as $data){
                $item = explode(';',$data);

                $check = TransactionPrelevement::where('type_prelevement_id',$item[0])->where('transaction_id',$trans->id)->first();
                if(!$check){
                    $mttble = floatval($item[2]);
                    $mttsm = $mttsm - $mttble;

                    $prelv = new TransactionPrelevement();
                    $prelv->transaction_id = $trans->id;
                    $prelv->type_prelevement_id = $item[0];
                    $prelv->programme_id = $idprogram;
                    $prelv->montant = $item[2];
                    $prelv->save();
                }
            }
        }

        $trans->status= 3;
        $trans->montant_retenue = $mttsm;
        $trans->save();

        $namst = $trans->soustraitant ? $trans->soustraitant->nom : 'N/A';
        $namstct = $trans->contrat ? $trans->contrat->slug : 'N/A';
        $ttmtnormal = number_format(str_replace(' ', '', $trans->montant), 0, '', ' ');
        $ttmt = number_format(str_replace(' ', '', $mttsm), 0, '', ' ');
        $mrestant = $trans->montant - $mttsm;
        $mrestant = number_format(str_replace(' ', '', $mrestant), 0, '', ' ');
        $ttaux = $trans->taux;
        //notifier la daf d'un paiement en attente
        $pgg = Programme::where('id',$idprogram)->first();
        //NOTIF DAF PAIEMENT
        $message = "<p>Bonjour Monsieur/Madame</p><p>Vous avez un nouveau attachement en attente de paiement.</p>";
        $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle." <br><b>SOUS-TRAITANT : </b>".$namst." <br><b>CONTRAT : </b>".$namstct."<br>";
        $message .= "<b>TAUX D'AVANCEMENT: </b>".$ttaux." %<br>";
        $message .= "<b>MONTANT DE L'ATTACHEMENT : </b>".$ttmtnormal." FCFA<br>";
        $message .= "<b>MONTANT DES RETENUES  : </b>".$ttmt." FCFA<br>";
        $message .= "<b>MONTANT RESTANT A PAYER  : </b>".$mrestant." FCFA<br></p>";
        $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
        $sujet = "ATTACHEMENT EN ATTENTE DE PAIEMENT - GESTION DE STOCK - ".$pgg->libelle;
        $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
        $user = User::where('role_id',7)->whereIn('id',$pguser)->pluck('email')->toArray();
        $this->sendMailNotificaton($message,$sujet,$user);

        return redirect()->route('daf.paie')->with('success','Attachement <b>'.$trans->slug.'</b> a bien été traité');

    }

}
