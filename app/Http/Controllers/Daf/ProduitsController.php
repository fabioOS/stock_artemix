<?php

namespace App\Http\Controllers\Daf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produit;
use App\ProgrammeProduit;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Session;

class ProduitsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('chefchantier.cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $produits = Produit::with('type')->orderBy('libelle','asc')->get();
        $listproduits = ProgrammeProduit::where('programme_id',$idprogram)->with(['produit'=>function($q){$q->with('type');}])->get();
        $listproduits->map(function($item){
            $data = Stock::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->qte = $data ? $data->qte : 0;
        });
        $types = Typeproduit::get();
        return view('daf.produits.index',compact('listproduits','produits','types'));
    }
}
