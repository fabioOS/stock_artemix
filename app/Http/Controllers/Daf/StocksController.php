<?php

namespace App\Http\Controllers\Daf;

use App\Exports\RotationExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProgrammeActif;
use App\ProgrammeLot;
use App\ProgrammeProduit;
use App\ProgrammeSouscorpsetat;
use App\Stock;
use App\StockFournisseurItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class StocksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }


    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $produits = Stock::where('programme_id',$idprogram)->with('programme')->has('produits')->with(['produits'=>function($q){$q->with('type');}])->get();
        $produits->map(function($item){
            $pgmpdt = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->price = $pgmpdt ? $pgmpdt->prix : 0;

            $types = ProgrammeActif::where('programme_id',$item->programme_id)->get();
            $ttpdtype = 0 ;
            foreach ($types as $type){
                $nbtyp = ProgrammeSouscorpsetat::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->where('actif_id',$type->id)->sum('qte');
                $nblot = ProgrammeLot::where('programme_id',$item->programme_id)->where('actif_id',$type->id)->count();
                $tt = $nblot * $nbtyp ;
                $ttpdtype = $ttpdtype + $tt ;
            }
            $qtecmd = StockFournisseurItem::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->sum('qte');
            $taux = $ttpdtype != 0  ? $qtecmd * 100 / $ttpdtype : 0;

            $item->qwaiting = $ttpdtype;
            $item->montant_qwaiting = round($ttpdtype,2) * $item->price;
            $item->qdelivre = $qtecmd;
            $item->montant_qdelivre = round($qtecmd,2) * $item->price;
            $item->qrestant = round($ttpdtype-$qtecmd,2);
            $item->taux = $taux;
            $item->mstock = $item->qte * $item->price;
        });

        //dump($produits);
        return view('daf.stocks.index', compact('produits'));
    }


    public function rotations()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        # Liste des produits en stock non utiliser depuis 15 jours
        $DaysAgo = Carbon::now()->subDays(15);
        $rotations = Stock::where('programme_id',$idprogram)->has('produits')->with(['produits'=>function($q){$q->with('type');}])->where('updated_at', '<=', $DaysAgo)->get();
        $rotations->map(function($item){
            $pgmpdt = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->price = $pgmpdt ? $pgmpdt->prix : 0;
        });

        $day = 15;

        return view('daf.rotation.index', compact('rotations','day'));
    }

    public function rotationsSearch(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $valider = Validator::make($request->all(),[
            'nbjrs' =>'required',
            'btn' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $day = $request->nbjrs;
            $DaysAgo = Carbon::now()->subDays($day);

            $rotations = Stock::where('programme_id',$idprogram)->with('programme')->has('produits')->with(['produits'=>function($q){$q->with('type');}])->where('updated_at', '<=', $DaysAgo)->get();
            $rotations->map(function($item){
                $pgmpdt = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
                $item->price = $pgmpdt ? $pgmpdt->prix : 0;
            });

            if($request->btn == 'trie'){
                return view('daf.rotation.index', compact('rotations','day'));

            }else{
                $libpgrm = count($rotations)>0 ? $rotations[0]->programme->libelle : '-';

                try{
                    $date=new \DateTime('now');
                    $date = $date->format('d-m-Y');
                    $lib= "rotation_produit-".$libpgrm."-".$date.".xlsx";

                    return Excel::download(new RotationExport($rotations,$day), $lib);

                }catch (\Exception $e){
                    return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                    //dd($e->getMessage());
                }
            }
        }

    }

}
