<?php

namespace App\Http\Controllers;

use App\BDSpec;
use App\BDSpecTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;


class DemandeSpecialsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getByStatus($status)
    {
        $programme_id = $this->getIdProgram();
        return BDSpec::where('programme_id',$programme_id)->where('user_id',Auth::user()->id)->where('status',$status)->get();
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        Validator::make($request->all(),[
            'titre' =>'required',
            'montant' =>'required',
            'des' =>'required',
        ])->validate();

        try {
            $newbds = new BDSpec();
            $newbds->slug = $this->getRefDS();
            $newbds->programme_id = $programme_id;
            $newbds->titre = $request->titre;
            $newbds->montant = str_replace(' ', '', $request->montant);
            $newbds->description = $request->des;
            $newbds->user_id = Auth::user()->id;
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $file->move(base_path('assets/uploads/demandespeciale/'),$filename);
                $newbds->preuve = $filename;
            }
            $newbds->save();

            if(Auth::user()->role_id == 6){
                $newbds->genere=0;
                $newbds->status=2;
                $newbds->save();

                return redirect()->back()->with('success','Votre demande a bien été envoyée et est en attente de traitement.');
            }

            //NOTIFIER LE SA
            $pgg = Programme::where('id',$programme_id)->first();
            $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Une demande spéciale est en attente de validation.</p>";
            $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$newbds->slug."<br>";
            $mess .= "<b>AGENT : </b>".Auth::user()->name."<br>";
            $mess .= "<b>MONTANT : </b>".$request->montant." FCFA<br><b>MOTIF : </b>".nl2br($request->des)."</p>";
            $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "DEMANDE SPECIALE EN ATTENTE - ".$pgg->libelle." - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('look',0)->where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($mess,$sujet,$user);

            return redirect()->back()->with('success','Votre demande a bien été envoyée.');
        }catch(\Exception $e){
            return back()->with('error', $e->getMessage());
        }

    }

    public function pdf($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $data = BDSpec::where('programme_id',$programme_id)->where('id',$id)->first();
        //dd($data);
        $pdf = PDF::loadView('service_achat.demandes.pdf',compact('data'));
        return $pdf->download('demande-speciale-'.$data->slug.'.pdf');
    }

    public function delDmdSpecial($slug)
    {
        $id = decrypt($slug);
        $dmd = BDSpec::where('id',$id)->first();
        if($dmd){
            $dmd->delete();
            return redirect()->back()->with('success','La demande a bien été supprimée.');
        }
        return redirect()->back()->with('error','Impossible de traiter votre demande.');
    }

    //PAIEMENT
    public function paiement(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        //dd($request->all());
        Validator::make($request->all(),[
            'moyen' =>'required',
            'bonid' =>'required',
            'bordereau' =>'required',
        ])->validate();

        $data = BDSpec::where('programme_id',$programme_id)->where('slug',$request->bonid)->where('status',2)->first();
        //dd($data);
        if($data){
            $trans = new BDSpecTrans();
            $trans->user_id = Auth::user()->id;
            $trans->montant = $data->montant;
            $trans->moyen = $request->moyen;
            $trans->programme_id = $data->programme_id;
            $trans->demande_id = $data->id;

            if($request->file('bordereau')){
                $file = $request->file('bordereau');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'assets/demandespecials/';
                $picture = 'bordereau-'.date('dmYhis').'.'. $extension;
                $file->move($folderName,$picture);
                $trans->file = $picture;
            }
            $trans->save();

            $data->status = 3;
            $data->save();

            return redirect()->back()->with('success' ,'La demande a bien été traitée.');

        }else{
            return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
        }
    }

}
