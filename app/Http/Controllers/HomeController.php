<?php

namespace App\Http\Controllers;

use App\BC;
use App\Bon_demande;
use App\Produit;
use App\Programme;
use App\ProgrammeActif;
use App\ProgrammeCorpsetat;
use App\ProgrammeLigneBudgetaire;
use App\ProgrammeLot;
use App\ProgrammeProduit;
use App\ProgrammeSouscorpsetatLot;
use App\ProgrammeSoustraitantcontrat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PDF;

class HomeController extends Controller
{

    public function sendmemail(){
        $data=array(
            "texts"=>"Lorem ipsum test",
        );
        $email = 'ouregalaurent7@gmail.com';

        try{
            Mail::send('mails.index',$data,function ($message) use($email){
                $message->to($email);
                $message->subject('TEST SEND MAIL');
            });
        }catch (\Exception $e){
            dump('error send mail');
            dd($e->getMessage());
        }
        dd("Mail envoyé");
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //dd('dd');
        if(Auth::user()->role_id == 1){

            $programmes = Programme::get();
            $users = User::with('programmes')->get();
            return view('admin/home',compact('programmes','users'));

        } elseif(Auth::user()->role_id == 2)
            return redirect()->route('chefchantier.cpprogram');
        elseif(Auth::user()->role_id == 3 OR Auth::user()->role_id == 5)
            return redirect()->route('cpprogram');
        elseif(Auth::user()->role_id == 4)
            return redirect()->route('magasinier.cpprogram');
        elseif(Auth::user()->role_id == 6)
            return redirect()->route('sa.home');
        elseif(Auth::user()->role_id == 7 OR Auth::user()->role_id == 8)
            return redirect()->route('daf.home');
        else return redirect()->route('forbiden');


    }

    //PROFILE
    public function profil()
    {
        $user = User::where('id',Auth::id())->with('role')->first();
        return view('admin.profile',compact('user'));
    }

    public function profilUpdate(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'name' =>'required',
            'contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->name = $request->name;
            $user->contact = $request->contact;
            $user->save();
            return redirect()->back()->with('success','✔ Votre profil a été modifié');
        }

    }

    public function profilUpdatePass(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $oldpass = $request->motpass;
            $newpass = $request->newpass;
            $newpassconfirm = $request->confirmpass;
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->back()->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->back()->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->back()->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function profilUpdateAvatar(Request $request)
    {
        //d($request->all());
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        try{
            if($fileSize <= 4000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName ='assets/uploads/users/';
                $picture = Str::random(6).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->back()->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 4Mb');
            endif;

        }catch (\Exception $e){
            //dd($e->getMessage());
        }

    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function pdfBc($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $data = Bon_demande::where('programme_id', $idprogram)->where('ref',$slug)->with('contrat')->with('bon_demanbde_items')->with('soustraitant')->with('user')->first();
        //dd($data);

        $pdf = PDF::loadView('chef_projet.bc_download',compact('data'));
        //$pdf->setPaper('a4', 'landscape');
        //return view('chef_projet.bc_download',compact('data'));
        return $pdf->download('bon-demande-'.$slug.'.pdf');
    }


    //FONCTION POUR AVOIR LES VILLA A CHANGE DU SOUSTRAITANT
    public function getLotStraitant($id)
    {
        $soustraitants = ProgrammeSoustraitantcontrat::where('id',$id)->with('lots')->first();
        $array = [];
        foreach ($soustraitants->lots as $k=>$lot){
            $l=ProgrammeLot::where('id',$lot->lot_id)->first();
            $actif=ProgrammeActif::where('id',$l->actif_id)->first();
            $array[$k]['lot'] = $l ? $l->lot : '';
            $array[$k]['type'] = $actif ? $actif->libelle :'';
        }

        return $array;
    }

    //FONCTION POUR AVOIR LA QUANTITE DEJA LIVRE
    public function getQteDispo($qte,$id) {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $prod = \App\Produit::where('id',$id)->first();
        if($prod and $prod->type_materiel == 'materiel'){
            return 'ok';
        }

        $cmdval = \App\Commande::whereIn('status',[2,3])->where('commandes.programme_id',$idprogram)
        ->join('commande_items', 'commande_items.commande_id', '=', 'commandes.id')
        ->where('commande_items.produit_id',$id)
        ->sum('commande_items.qte');

        $listcmd = \App\Commande::with('produits')->whereIn('status',[2,3])->where('commandes.programme_id',$idprogram)
        ->join('commande_items', 'commande_items.commande_id', '=', 'commandes.id')
        ->where('commande_items.produit_id',$id)
        ->select('commandes.id','commande_items.produit_id as idpdt','commande_items.qte')
        // ->pluck('id')->toArray();
        ->get();

        //dump($prod,$cmdval,$listcmd);
        $qtencss = \App\ProgrammeSouscorpsetatLot::where('produit_id',$id)->where('programme_id',$idprogram)->sum('qte');
        //return $cmdval;
        $rest = $qtencss - $cmdval;
        $restant = $rest - $qte;

        if($rest >= $qte){
            return 'ok';
        }
        return $rest;

    }

    //AVOIR LA LISTE DES BS LIVRER
    public function getBsLivre($id) {
        //$id = decrypt($id);
        $bons = new BonController();
        $bon = $bons->getById($id);

        if($bon){
            //Selectionner les matiers distincts du bon de commande et faire la somme des quantités
            $distinct_matiers = \App\Bon_demande_item::where('bon_demande_id', $id)
            ->join('produits', 'produits.id', '=', 'bon_demande_items.produit_id')
            ->selectRaw('produits.id AS id, SUM(qte) AS qte, produits.libelle AS libelle')
            ->groupBy('produits.id')
            ->get();

            return $distinct_matiers;
        }

        return null;
    }

    //FONCTION QUI PERMET DE SELECTION LA LISTE DES PRODUITS a COMMANDER a PARTIR DE LA LIGNES BUDGETAIRE ID
    public function getProduitByLigne($id) {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $list = "<option selected disabled>Sélectionner un produit</option>";
        $listproduits = [];

        $lign = ProgrammeLigneBudgetaire::where('id',$id)->where('programme_id',$idprogram)->first();
        //dd($lign);
        if($lign && $lign->hors_ds == 1){
            // $prod->type_materiel == 'materiel'
            $produitsid = Produit::where('type_materiel','materiel')->pluck('id')->toArray();
            $listproduits = ProgrammeProduit::whereIn('produit_id',$produitsid)->where('programme_id',$idprogram)->has('produit')->with(['produit'=>function($q){$q->with('type');}])->get();

        }else{
            $ligne = ProgrammeLigneBudgetaire::where('id',$id)->where('programme_id',$idprogram)->first();
            if($ligne){
                $ceids = ProgrammeCorpsetat::where('ligne_budget_id',$ligne->id)->where('programme_id',$idprogram)->pluck('id')->toArray();
                $pdtids = ProgrammeSouscorpsetatLot::whereIn('corpsetat_id',$ceids)->where('programme_id',$idprogram)->groupBy('produit_id')->pluck('produit_id')->toArray();
                $listproduits = ProgrammeProduit::whereIn('produit_id',$pdtids)->where('programme_id',$idprogram)->has('produit')->with(['produit'=>function($q){$q->with('type');}])->get();
                //dd($listproduits);
                // return $list;
            }
        }

        foreach ($listproduits as $item) {
            $list .= "<option value='".$item->produit->id."' data-typepdt='".$item->produit->type? $item->produit->type->libelle : '-'."' data-libelle='".$item->produit->libelle."'>".$item->produit->libelle."</option>";
            //dump($list);
        }

        return $listproduits;

    }

    //FONCTION QUI RETOURNE LA VUE D'UN BON DE COMMANDE A PARTIR DE SON ID
    public function getBCView($id)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $demande = BC::where('programme_id',$idprogram)->where('id',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        if(!$demande){
            return "<div class='alert alert-warning'>Aucun bon de commande trouvé</div>";
        }

        if($demande->ibution_id){
            $mtt = $demande->montant;
        }else{
            $mtt = $demande->full_montant;
        }

        $data = [
            'mtt' => $mtt,
            'vues' => view("_bon_commande_detail",compact('demande'))->render(),
        ];

        return $data;
    }
}
