<?php

namespace App\Http\Controllers;

use App\BC;
use App\ProgrammeLigneBudgetaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LigneBudgetaireController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIdProgram(){
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
        return Session::get('program') ?: null ;
    }


    public function getConsoLignBudget($id)  {
        $programme_id = $this->getIdProgram();
        $lign = ProgrammeLigneBudgetaire::where('programme_id', $programme_id)->where('id',$id)->with('commandes')->first();
        $ids = $lign->commandes->pluck('id');
        $bc = BC::whereIn('commande_id',$ids)->sum('montant');
        $consommer = $bc;
        $pcent = $lign->montant > 0 ? round(($bc / $lign->montant) * 100, 2) : 0;

        if($pcent >= 100){
            $color = 'danger';
        }elseif($pcent >= 50){
            $color = 'warning';
        }else{
            $color = 'success';
        }

        return response()->json(['consommer' => $consommer, 'pcent' => $pcent, 'color' => $color]);
    }

    public function index()
    {
        $programme_id = $this->getIdProgram();
        $blade = $this->getBlade(Auth::user()->role_id);

        //Liste des lignes budgetaires
        $lignes = ProgrammeLigneBudgetaire::where('programme_id', $programme_id)->with('commandes')->orderBy('libelle','asc')->get();
        $lignes->map(function($ligne){
            $data = $this->getConsoLignBudget($ligne->id);
            $ligne->consommer = $data->original['consommer'];
            $ligne->pcent = $data->original['pcent'];
            $ligne->color = $data->original['color'];
            return $ligne;
        });

        //Groupe des lignes budgetaires par type
        $lignes = $lignes->groupBy('type');
        // dd($lignes);
        // dump($lignes);

        return view('_ligne_budgetaire.index', compact('blade', 'lignes'));
    }

    public function show($id){
        $programme_id = $this->getIdProgram();
        $blade = $this->getBlade(Auth::user()->role_id);
        $id = decrypt($id);

        $ligne = ProgrammeLigneBudgetaire::where('programme_id', $programme_id)->where('id',$id)->with('commandes')->first();
        $data = $this->getConsoLignBudget($ligne->id);
        $ligne->consommer = $data->original['consommer'];
        $ligne->pcent = $data->original['pcent'];
        $ligne->color = $data->original['color'];

        $ligne->bcs = BC::whereIn('commande_id',$ligne->commandes->pluck('id'))->with('demande')->with('fournisseur')->with('user')->with('items')->with('bonreception')->orderBy('id','desc')->get();

        return view('_ligne_budgetaire.show', compact('blade', 'ligne'));
    }

    public function redirect($id=null,Request $request) {
        if($id == null){
            return redirect()->route('lignebudgetaire.index');
        }
        //dd($request->all());
        $roleid = Auth::user()->role_id;

        switch ($request->web) {
            case 'commande':
                switch ($roleid) {
                    case 2:
                        //conducteur-des-travaux
                        return redirect()->route('ccommande.show', $id);
                        break;
                    case 3:
                        //chef-projet
                        return redirect()->route('cpcommande.show', $id);
                        break;
                    case 4:
                        //gestionnaire-de-stock
                        return redirect()->route('mcommande.show', $id);
                        break;
                    case 5:
                        //DP
                        return redirect()->route('cpcommande.show', $id);
                        break;
                    case 6:
                        //service-achat
                        return redirect()->route('sacommande.show', $id);
                        break;
                    default:
                        return redirect()->route('lignebudgetaire.index');
                        break;
                }
            case 'bcmmande':
                switch ($roleid) {
                    case 6:
                        //service-achat
                        return redirect()->route('sabc.show', $id);
                        break;
                    case 7:
                        //daf
                        return redirect()->route('daf.show', $id);
                        break;
                    case 8:
                        //controleur-gestion
                        return redirect()->route('daf.show', $id);
                        break;
                    default:
                        return redirect()->route('lignebudgetaire.index');
                        break;
                }
            default:
                return redirect()->route('lignebudgetaire.index');
                break;
        }

    }
}
