<?php

namespace App\Http\Controllers\Magasinier;

use App\Bon_demande;
use App\Bon_retour;
use App\Bon_retour_item;
use App\Http\Controllers\BonController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\ProgrammePivotUser;
use App\ProgrammeSouscorpsetatLot;
use App\ProgrammeSoustraitant;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class BackstockController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function getProgramExist(){
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram(){
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function index() {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $lists = Bon_retour::with('demande')->with('user')->where('programme_id',$programme_id)->orderBy('id','desc')->get();
        return view('magasinier.backstock.index' , compact('lists'));
    }

    public function show($id) {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $bon = Bon_retour::with('demande')->with('user')->where('programme_id',$programme_id)->findOrFail($id);
        return view('magasinier.backstock.show' , compact('bon'));
    }

    public function create (){
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bons = new BonController();
        $bons = $bons->getByStatus(3);

        return view('magasinier.backstock.create' , compact('bons'));
    }

    public function create_step2(Request $request) {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Validation
        $request->validate([
            'description' => 'required',
            'bs_id' => 'required',
        ], [
            'description.required' => 'La description est obligatoire',
            'bs_id.required' => 'Le bon de sortie est obligatoire',
        ]);

        // dd($request->all());
        $vars = [
            'des' => $request->description,
            'bs_id' => $request->bs_id,
        ];

        $id = $request->bs_id;
        $bon = new BonController();
        $bon = $bon->getById($id);
        if(!isset($bon)){
            return redirect()->back()->with('error' , 'Le bon de sortie n\'existe pas');
        }

        $contrat = $bon->contrat_id;
        $lotsIds = $bon->items()->pluck('lot_id')->toArray();
        $soustraitant = ProgrammeSoustraitant::with(['lots' =>function($query) use($lotsIds,$contrat) {
            $query->whereIn('lot_id', $lotsIds)->with('lot');
            $query->where('soustraitantcontrat_id',$contrat);
        }])
        ->where('id', $bon->soustraitant_id)
        ->where('programme_id', $programme_id)
        ->first();

        //dd($soustraitant);
        return view('magasinier.backstock.create_step2' , compact('bon','vars','soustraitant'));

    }

    public function store(Request $request){
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $bs = decrypt($request->slug);

        $bon = Bon_demande::where('id',$bs)->first();
        if(!isset($bon)){
            return redirect()->back()->with('error' , 'Le bon de sortie n\'existe pas');
        }

        // save bon de demande
        $demande = new Bon_retour();
        $demande->status = 1;
        $demande->programme_id = $programme_id;
        $demande->soustraitant_id = $bon->soustraitant_id;
        $demande->user_id = Auth::user()->id;
        $demande->ref = $this->getRefBRetour();
        $demande->bon_id = $bs;

        if($request->file('demandefile')){
            $file = $request->file('demandefile');
            $extension = $file->getClientOriginalExtension() ?: 'pdf';
            $folderName = 'assets/uploads/bonretours/';
            $document = Str::random(6).'.'. $extension;
            $file->move($folderName,$document);
            $demande->file = $document;
        }
        else
            $demande->file = '';

        $demande->contrat_id = $bon->contrat_id;
        $demande->save();

        //save items bon de retour
        $i=0;
        foreach($request->lot_id as $lot_id) {
            if(isset($request->corps_id[$lot_id]) && count($request->corps_id[$lot_id]) > 0 ) {
                foreach( $request->corps_id[$lot_id] as $corps_id) {
                    if(isset($request->sous_corps_id[$lot_id][$corps_id]) && count($request->sous_corps_id[$lot_id][$corps_id]) > 0) {
                        foreach( $request->sous_corps_id[$lot_id][$corps_id] as $key=>$sous_corps_id) {
                            if( isset($request->produit_id[$lot_id][$corps_id][$key]) && $request->produit_id[$lot_id][$corps_id][$key] != null ) {
                                if( isset($request->qte_sscorps[$lot_id][$corps_id][$key]) && $request->qte_sscorps[$lot_id][$corps_id][$key] != null ) {
                                    $produit_id = $request->produit_id[$lot_id][$corps_id][$key];
                                    $qte        = $request->qte_sscorps[$lot_id][$corps_id][$key];
                                    $itembs        = $request->itemdmd_id[$lot_id][$corps_id][$key];
                                    $check = ProgrammeSouscorpsetatLot::where('corpsetat_id',$corps_id)->where('produit_id',$produit_id)->where('lot_id',$lot_id)->first();
                                    if($check){
                                        if($qte != 0 && $qte != null && $qte != ''){
                                            $bonretour = new Bon_retour_item();
                                            $bonretour->bon_retour_id = $demande->id;
                                            $bonretour->soustraitant_id = $bon->soustraitant_id;
                                            $bonretour->lot_id = $lot_id;
                                            $bonretour->produit_id = $produit_id;
                                            $bonretour->corpsetats_id = $corps_id;
                                            $bonretour->scorpsetats_id = $sous_corps_id;
                                            $bonretour->qte = $qte;
                                            $bonretour->bon_demande_item_id = $itembs;
                                            $bonretour->save();

                                            $i++;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if($i != 0){
            $pgg = Programme::where('id',$programme_id)->first();
            $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Un nouveau bon de retour en stock est en attente de traitement.</p>";
            $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$demande->ref."<br><b>NOMBRE DE PRODUIT RETOUNÉ : </b>".$i."</p>";
            $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "BON DE RETOUR EN STOCK EN ATTENTE DE TRAITEMENT - ".$pgg->libelle." - G-STOCK";;
            //send le mail au magasinier
            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',3)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($mess,$sujet,$user);
            return redirect()->route('magasinier.stock.back.index')->with('success' ,'Le bon de retour a bien été ajouté !');

        }else{
            //supprimer les demandes creer
            $iddmd = $demande->id;
            $demande = Bon_retour::where('id',$iddmd)->delete();

            return back()->with('error' ,'Aucune données n\'a été enregistrée !');
        }

    }
}
