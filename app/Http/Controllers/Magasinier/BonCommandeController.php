<?php

namespace App\Http\Controllers\Magasinier;

use App\BC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StockFournisseur;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BonCommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function MAJDelivrer($bcs)
    {
        foreach($bcs as $bc){
            $cmdtt = $bc->items->sum('qte');
            $alldelivery = StockFournisseur::where('bc_id', $bc->id)
                ->with(['items' => function ($query) {
                    $query->select('stock_fournisseur_id', DB::raw('SUM(qte) as total_qte'))->groupBy('stock_fournisseur_id');
                }])->get();

            $totalQteLivree = $alldelivery->sum(function ($delivery) {
                return $delivery->items->sum('total_qte');
            });

            if($cmdtt <= $totalQteLivree){
                $bcupdte = BC::where('id',$bc->id)->first();
                $bcupdte->close = 1;
                $bcupdte->save();
            }
        }
    }

    public function index()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $bcs = BC::where('programme_id', $programme_id)->where('status',2)->with(['demande', 'fournisseur', 'user', 'items'])->get();
        $bcs->map(function ($q) {
            // Récupérer toutes les livraisons associées à ce BC
            $alldelivery = StockFournisseur::where('bc_id', $q->id)
                ->with(['items' => function ($query) {
                    // Agréger les quantités d'items
                    $query->select('stock_fournisseur_id', DB::raw('SUM(qte) as total_qte'))->groupBy('stock_fournisseur_id');
                }])->get();

            // Calculer la quantité totale livrée
            $totalQteLivree = $alldelivery->sum(function ($delivery) {
                return $delivery->items->sum('total_qte');
            });
            // Assigner la quantité livrée au BC actuel
            $q->qtelivree = $totalQteLivree;
        });

        $this->MAJDelivrer($bcs);
        return view("magasinier.bc.index",compact('bcs'));
    }


    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($slug);
        $demande = BC::where('programme_id',$programme_id)->where('id',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        return view("magasinier.bc.show",compact('demande'));

    }


}
