<?php

namespace App\Http\Controllers\Magasinier;

use App\BC;
use App\BCitem;
use App\CommandeItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock;
use App\StockFournisseur;
use App\StockFournisseurItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BonReceptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function MAJDelivrerByID($idbcs)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->whereIn('id',$idbcs)->get();
        foreach($bcs as $bc){
            $cmdtt = $bc->items->sum('qte');
            // $alldelivery = StockFournisseur::where('bc_id',$bc->id)->with('items')->first();
            // $alldelivery = $alldelivery ? $alldelivery->items->sum('qte') : 0;
            $alldelivery = StockFournisseur::where('bc_id', $bc->id)
                ->with(['items' => function ($query) {
                    $query->select('stock_fournisseur_id', DB::raw('SUM(qte) as total_qte'))->groupBy('stock_fournisseur_id');
                }])->get();

            $totalQteLivree = $alldelivery->sum(function ($delivery) {
                return $delivery->items->sum('total_qte');
            });

            if($cmdtt <= $totalQteLivree){
                $bcupdte = BC::where('id',$bc->id)->first();
                $bcupdte->close = 1;
                $bcupdte->save();
            }
        }
    }

    public function index()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bons = StockFournisseur::where('programme_id',$programme_id)->orderBy('id','desc')->get();
        if(count($bons)>0){
            $bcs = $bons->pluck('bc_id');
            $this->MAJDelivrerByID($bcs);
        }
        //dd($bons);
        return view('magasinier.br.index',compact('bons'));
    }

    public function create ()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //$itembc = null
        $bcs = BC::where('programme_id',$programme_id)->where('close',0)->where('status',2)->with('demande')->with('fournisseur')->with('user')->with('items')->get();
        return view('magasinier.br.create',compact('bcs'));
    }

    public function getbc($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $itembc = BC::where('programme_id',$programme_id)->where('id',$id)->where('status',2)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        //dd($itembc);
        $bcs = BC::where('programme_id',$programme_id)->where('close',0)->where('status',2)->with('demande')->with('fournisseur')->with('user')->with('items')->get();
        return view('magasinier.br.create',compact('bcs','itembc'));
    }

    public function getDispo($qte,$item,$cmd)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        //dd($qte,$item,$cmd);
        $check = BCitem::where('id',$item)->where('bon_commande_id',$cmd)->first();
        //dd($check);
        if($check){
            $nblivre = StockFournisseurItem::where('programme_id',$programme_id)->where('bc_item_id',$check->id)->where('produit_id',$check->produit_id)->sum('qte');
            $int_value = (int) $check->qte;
            // dd($nblivre,$int_value);
            // if($nblivre > $int_value){
            //     return 'ok';
            // }

            $reste = $int_value - $nblivre;
            if($reste >= $qte){
                return 'ok';
            }
            return $reste;
            //return 'ok';

        }
        return "error";
    }

    public function getRefBR()
    {
        $code = "BR".date('ym');
        $count = StockFournisseur::count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$num;

        return $val;
    }


    public function store(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        Validator::make($request->all(), [
            'bdc' => ['required'],
            'nbl' => ['required'],
            'datasession' =>['required'],
            'file' =>['required','mimes:pdf'],
        ])->validate();

        $slug = $this->getRefBR();
        //check bc
        $bc = BC::where('id',$request->bdc)->where('programme_id',$programme_id)->first();
        //dd($bc);
        if(!$bc){
            return redirect()->back()->with('error','Le bon de commande est introuvable.');
        }
        //dd($bc);

        //Save BL(Bon de Reception)
        $br = new StockFournisseur();
        $br->programme_id = $programme_id;
        $br->ref = $slug;
        $br->bc_id = $bc->id;
        $br->nbl = $request->nbl;
        $br->fournisseur_id = $bc->fournisseur_id;
        $br->commande_id = $bc->commande_id;

        if($request->hasFile('file')){
            $file=$request->file('file');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName ='assets/uploads/stocks/';
            $picture = $slug.'-'.Str::random(6).'.'. $extension;
            $file->move($folderName,$picture);

            $br->file = $picture;
        }

        $br->user_id = Auth::user()->id;
        $br->save();
        $i = 0;

        foreach ($request->datasession as $data){
            $item = explode(';',$data);
            //dd($item);
            $bcitem = BCitem::where('id',$item[0])->where('programme_id',$programme_id)->first();
            //select Bstock
            $check = StockFournisseurItem::where('produit_id',$bcitem->produit_id)->where('programme_id',$programme_id)->where('stock_fournisseur_id',$br->id)->first();
            if(!$check){
                $britem = new StockFournisseurItem();
                $britem->stock_fournisseur_id = $br->id;
                $britem->bc_item_id = $item[0];
                $britem->programme_id = $programme_id;
                $britem->produit_id = $bcitem->produit_id??0;
                $britem->qte = $item[3];
                $britem->save();

                if(floatval($item[3])>0){
                    $itemstock = Stock::where('produit_id', $bcitem->produit_id)->where('programme_id', $programme_id)->first();
                    if($itemstock == null){
                        $newstk = new Stock();
                        $newstk->produit_id =$bcitem->produit_id;
                        $newstk->programme_id =$programme_id;
                        $newstk->qte =$item[3];
                        $newstk->save();
                    }else {
                        $old_stock = Stock::findOrFail($itemstock->id);
                        $new_qte = $old_stock['qte'] + $item[3];
                        $old_stock->update(['qte' =>$new_qte]);
                    }
                    $i++;
                }
            }
        }

        if($i>0){
            return redirect()->route('mbr.index')->with('success', "Bon de reception a bien été ajouté. Stock de $i matières ajouté avec succès !");
        }else{
            return back()->with('error', "Aucun stock n'a été modifié dû à une erreur de valeur(s) !");
        }

    }

    public function show($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($id);

        $bon = StockFournisseur::where('id',$id)->with('items')->where('programme_id',$programme_id)->first();
        //dd($bon);
        return view('magasinier.br.show',compact('bon'));
    }

}
