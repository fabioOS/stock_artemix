<?php

namespace App\Http\Controllers\Magasinier;

use App\BC;
use App\BCitem;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Commande;
use App\CommandeItem;
use App\Http\Controllers\BonController;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produit;
use App\Programme;
use App\ProgrammeProduit;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }


    public function getByStatus($status)
    {
        $programme_id = $this->getIdProgram();
        return Commande::where('programme_id',$programme_id)->where('status',$status)->count();
    }


    public function index()
    {
        return view('magasinier.demande.index', compact('data', 'types'));
    }


    public function create()
    {
        $programme_id = $this->getIdProgram();
        $types = Typeproduit::with('produits')->get();
        // $listproduits = ProgrammeProduit::where('programme_id',$programme_id)->has('produit')->with(['produit'=>function($q){$q->with('type');}])->get();
        $lignbudgets = Programme::where('id',$programme_id)->with('ligneBudgetaires')->first();
        //dd($listproduits);
        return view('magasinier.commandes.create',compact('types','lignbudgets'));
    }

    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);
        $demande = Commande::where('id',$id)->with('produits')->with('lignebudget')->with('user')->first();

        return view('magasinier.commandes.show',compact('demande'));

    }

    public function delete($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);
        //verifier si la commande à un bon de commande
        $check = BC::where('commande_id',$id)->first();
        if($check){
            return redirect()->back()->with('error','Impossible de supprimer cette demande car elle a déjà un bon de commande.');
        }

        $demande = Commande::where('id',$id)->with('produits')->with('user')->first();
        //dd($demande);
        //delete item commande
        $item = CommandeItem::where('commande_id',$id)->delete();
        $demande->delete();

        return redirect()->back()->with('success','La demande a bien été supprimée.');
    }

    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',1)->where('programme_id',$programme_id)->with('produits')->with('user')->orderBy('id','desc')->get();

        return view('magasinier.commandes.waiting',compact('demandes'));
    }

    public function rejet()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',4)->where('programme_id',$programme_id)->with('produits')->with('user')->orderBy('id','desc')->get();

        return view('magasinier.commandes.reject',compact('demandes'));
    }

    public function valider()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::where('status',2)->where('programme_id',$programme_id)->with('produits')->with('user')->with('boncommande')->orderBy('id','desc')->get();
        $demandes->map(function ($q) {
            // Récupérer toutes les bon de commande associées à ce cette demande
            $idsItem = $q->produits->pluck('id');
            $bcitems = BCitem::whereIn('commande_item_id', $idsItem)->sum('qte');
            $q->qtecmde = $bcitems;
        });

        return view('magasinier.commandes.valid',compact('demandes'));
    }
}
