<?php

namespace App\Http\Controllers\Magasinier;

use App\BDSpec;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Http\Controllers\BonController;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produit;
use App\Programme;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DemandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programme_id = Session::get('program');
        $data = Produit::with('type')->with(['stocks'=>function($query) {
            $query->where('programme_id', Session::get('program'));
        }])->get();
        $types = Typeproduit::with('produits')->get();
        return view('magasinier.demande.index', compact('data', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function indexDemande()
    {
        $data = new BonController();
        $data = $data->getByStatus(2);
        return view('magasinier.bc_validated', compact('data'));
    }

    public function detailsDemande($id)
    {
        $id = decrypt($id);
        $data = new BonController();
        $data = $data->getById($id);
        return view('magasinier.bc_details', compact('data'));
    }

    public function deliveringDemande($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        /* $data = new BonController();
        return $data->delivering($id); */
        try{
            $demande = Bon_demande::findOrFail($id);
            $error = 0;
            $message = '';
            $items = Bon_demande_item::where('bon_demande_id',$id)->get();
            // on vérifie si qte demandée est dispo en stock
            foreach($items as $item) {
                $qte_demande = $item->qte;
                $qte_en_stock = Stock::where('produit_id', $item->produit_id)->where('programme_id', $programme_id)->pluck('qte')->first();
                if($qte_en_stock == null) $qte_en_stock = 0;
                if( (float)$qte_en_stock < (float)$qte_demande) {
                    $error++;
                    $info_produit = Produit::with('type')->find($item->produit_id);
                    $message .= "<li><b>".$info_produit->libelle.'</b> : Qté disponible (<b>'.$qte_en_stock.'<sup>'.$info_produit->type->libelle.'</sup></b>) est inférieure à la demande (<b>'.$qte_demande.'<sup>'.$info_produit->type->libelle.'</sup></b>) ;</li>';
                }
            }

            if( $error > 0 ) {
                $message = "Impossible de livrer le bon de demande N° <b class='text-primary'>".$demande->ref."</b>. Merci de trouver ci-dessous les motifs :<br> <ul>".$message.'</ul>';
                return redirect()->back()->with('error', $message);
            }

            foreach($items as $item) {
                $stocks = Stock::where('produit_id', $item->produit_id)->where('programme_id', $programme_id)->get();
                foreach($stocks as $line) {
                    $stock = Stock::findOrFail($line->id);
                    $new_qte = $line->qte - $item->qte;
                    $stock->update(['qte' => $new_qte]);
                }
            }
            $demande->update(['status' => 3,'nvalid' => 2]);
            Bon_demande_historique::create([
                'bon_demande_id' => $id,
                'user_id' => Auth::user()->id,
                'action' => 'Livré'
            ]);

            //NOTIFIER LE DEMANDEUR A ETE VALIDER
            $pgg = Programme::where('id',$programme_id)->first();
            $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Votre bon de sortie n°<b>".$demande->ref."</b> a été approuvé avec succès.</p>";
            $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$demande->ref."</p>";
            $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "BON DE SORTIE TRAITER - ".$pgg->libelle." - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('id',$demande->user_id)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($mess,$sujet,$user);

            return redirect()->route('magasinier.demande.index')->with('success', "Bon de demande N° <b class='text-primary'>".$demande->ref."</b> délivré avec succès ! ");
        }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function rejetBc(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'motif' =>'required',
            'bonid' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $data = Bon_demande::where('programme_id', $idprogram)->where('ref',$request->bonid)->where('status',2)->with('bon_demanbde_items')->first();
            if($data){
                $data->status = 4;
                $data->motif = $request->motif;
                $data->save();

                //NOTIFIER LE CHEF PROJET QUE LE BON EST REJETE
                $pgg = Programme::where('id',$idprogram)->first();
                $mess = "<p>Bonjour Monsieur / Madame ,</p><p>Votre bon de sortie n°<b>". $request->bonid."</b> a été rejetée.";
                $mess .= "<br> Veuillez trouver ci-dessous le motif de rejet : </p><p>“ ".$request->motif." ”</p>";
                $mess .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$request->bonid."</p>";
                $mess .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
                $sujet = "BON DE SORTIE REJETE - ".$pgg->libelle ." - G-STOCK";

                $pguser = ProgrammePivotUser::where('programme_id',$idprogram)->pluck('user_id');
                $user = User::where('id',$data->user_id)->whereIn('id',$pguser)->pluck('email')->toArray();
                $this->sendMailNotificaton($mess,$sujet,$user);

                return redirect()->route('magasinier.demande.index')->with('success',"Le bon de sortie <b class='text-primary'>".$request->bonid."</b> a bien été rejeté");

            }else{
                return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
            }
        }

    }

    public function cancelingDemande($id)
    {
        /* $id = decrypt($id);
        try{
            $demande = Bon_demande::findOrFail($id);
            $error = 0;
            $message = '';
            $items = Bon_demande_item::where('bon_demande_id',$id)->get();
            // on vérifie si qte demandée est dispo en stock
            foreach($items as $item) {
                $qte_demande = $item->qte;
                $qte_en_stock = Stock::where('produit_id', $item->produit_id)->where('programme_id', Session::get('program'))->pluck('qte')->first();
                if($qte_en_stock == null) $qte_en_stock = 0;
                if( $qte_en_stock < $qte_demande) {
                    $error++;
                    $info_produit = Produit::with('type')->find($item->produit_id);
                    $message .= "<li>Impossible de livrer <b>".$info_produit->libelle.'</b> : Qté disponible (<b>'.$qte_en_stock.'<sup>'.$info_produit->type->libelle.'</sup></b>) est inférieur à la demande (<b>'.$qte_demande.'<sup>'.$info_produit->type->libelle.'</sup></b>)</li>';
                }
            }
            if( $error > 0 ) {
                return back()->with('error', $message);
            }
            foreach($items as $item) {
                $stocks = Stock::where('produit_id', $item->produit_id)->where('programme_id', Session::get('program'))->get();
                foreach($stocks as $line) {
                    $stock = Stock::findOrFail($line->id);
                    $new_qte = $line->qte - $item->qte;
                    $stock->update(['qte' => $new_qte]);
                }
            }
            $demande->update(['status' => 3]);
            Bon_demande_historique::create([
                'bon_demande_id' => $id,
                'user_id' => Auth::user()->id,
                'action' => 'Livré'
            ]);
            return redirect()->route('magasinier.demande.index')->with('success', "Bon de demande N° <b class='text-primary'>".$demande->ref."</b> délivré avec succès ! ");
        }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        } */
    }

    public function indexLivraison()
    {
        $data = new BonController();
        $data = $data->getByStatus(3);
        return view('magasinier.bc_delivered', compact('data'));
    }


    public function detailsLivraison($id)
    {
        $id = decrypt($id);
        $data = new BonController();
        $data = $data->getById($id);
        return view('magasinier.bc_details', compact('data'));
    }


    public function exportLivraison($id)
    {
        $id = decrypt($id);
        $data = new BonController();
        $data = $data->getById($id);
        return $data;
        //return view('magasinier.bc_details', compact('data'));
    }

    public function destroyDemande($id)
    {
        return redirect()->back();
    }


    //DEMANDE SPECIAL

    public function createDmspec()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        return view('magasinier.demandes.create');
    }

    public function waitingDmspec()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',1)->where('user_id',Auth::user()->id)->where('status',1)->orderBy('id','desc')->get();

        //dd($demandes);
        return view('magasinier.demandes.waiting',compact('demandes'));
    }

    public function validerDmspec()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',2)->where('user_id',Auth::user()->id)->where('genere',1)->orderBy('id','desc')->get();

        return view('magasinier.demandes.valider',compact('demandes'));
    }

    public function rejeterDmspec()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',4)->where('user_id',Auth::user()->id)->orderBy('id','desc')->get();

        return view('magasinier.demandes.rejeter',compact('demandes'));
    }


}
