<?php

namespace App\Http\Controllers\Magasinier;

use App\Avancement;
use App\Http\Controllers\Controller;
use App\LotSoustraitant;
use App\Programme;
use App\ProgrammeCorpsetat;
use App\ProgrammeFournisseur;
use App\ProgrammeLot;
use App\ProgrammePivotUser;
use App\ProgrammeProduit;
use App\ProgrammeSoustraitant;
use App\ProgrammeSoustraitantcontratMontant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Stock;
use App\StockFournisseurItem;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('magasinier.cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }



    public function listpgram()
    {
        Session::forget(['program']);

        $user = User::where('id',Auth::id())->with('programmes')->first();

        if(count($user->programmes)==1){
            return redirect()->route('magasinier.cpprogram.item',$user->programmes[0]->slug);
        }

        return view('magasinier.listpgram',compact('user'));
    }

    public function itempgram($slug)
    {
        //dd($slug);
        $program = Programme::where('slug',$slug)->first();
        if(isset($program->id)){
            $check = ProgrammePivotUser::where('user_id',Auth::id())->where('programme_id',$program->id)->first();

            if($check){
                Session::put('program', $program->id);
                return redirect()->route('magasinier.home');
            }

            return redirect()->back()->with('error','Vous n\'avez pas doit à ce programme');
        }

        return redirect()->back();
    }
    /**     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $top3cmd = StockFournisseurItem::select('programme_id','produit_id', DB::raw('sum(qte) as total'))->where('programme_id',$idprogram)->groupBy('produit_id')
                                        ->orderBy('total', 'desc')->limit(8)->with('produits')->has('produits')->get();
        $top3cmd->map(function ($item){
            $price = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            //$item->pu = $price ? $price->prix : 0;
            $item->price = $price ? $price->prix * $item->total : 0;
        });

        $mtlimit = Stock::where('programme_id',$idprogram)->where('qte','<=',10)->has('produits')->with(['produits'=>function($q){$q->with('type');}])->get();


        $this->getProgramExist();
        return view('magasinier.index',compact('top3cmd','mtlimit'));
    }


    //PROFILE
    public function profil()
    {
        $this->getProgramExist();
        $user = User::where('id',Auth::id())->with('role')->with('programmes')->first();
        return view('chefchantier.profile',compact('user'));
    }

    public function profilUpdate(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'name' =>'required',
            'contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->name = $request->name;
            $user->contact = $request->contact;
            $user->save();
            return redirect()->back()->with('success','✔ Votre profil a été modifié');
        }

    }

    public function profilUpdatePass(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $oldpass = $request->motpass;
            $newpass = $request->newpass;
            $newpassconfirm = $request->confirmpass;
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->back()->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->back()->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->back()->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function profilUpdateAvatar(Request $request)
    {
        $this->getProgramExist();
        //d($request->all());
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        try{
            if($fileSize <= 4000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName ='assets/uploads/users/';
                $picture = Str::random(6).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->back()->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 4Mb');
            endif;

        }catch (\Exception $e){
            //dd($e->getMessage());
        }

    }



    //FOURNISSEURS

    public function fours()
    {
        $fournisseurs = ProgrammeFournisseur::get();
        return view('magasinier.fournisseurs.index',compact('fournisseurs'));
    }

    public function storeFours(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $new = new ProgrammeFournisseur();
            $new->programme_id = $idprogram;
            $new->nom = $request->nom;
            $new->contact = $request->contact;
            $new->adresse = $request->adresse;
            $new->save();

            return redirect()->back()->with('success','Le fournisseur a été ajouté');
        }

    }

    public function updateFours(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'slug' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $new = ProgrammeFournisseur::where('id',$request->slug)->where('programme_id',$idprogram)->first();
            if($new){
                //$new->programme_id = $idprogram;
                $new->nom = $request->nom;
                $new->contact = $request->contact;
                $new->adresse = $request->adresse;
                $new->save();
            }


            return redirect()->back()->with('success','Le fournisseur a été mise a jour');
        }
    }

    public function delFours($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $new = ProgrammeFournisseur::where('id',$slug)->where('programme_id',$idprogram)->first();
        if($new){
            $new->delete();
            return redirect()->back()->with('success','Le fournisseur a été supprimé');
        }
        return redirect()->back();
    }


    //ETATS
    public function etats(){
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        $listlots = ProgrammeLot::where('programme_id',$idprogram)->with('actifs')->orderByRaw('ABS(lot)', 'asc')->get();
        $listlots->map(function($item) use($idprogram){
            if($item->programme->type_rattachement == 1){
                $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            }else{
                //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
                $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$item->id)->has('straitantcontrat')->sum('montant');
            }
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });

        //dd($blocks,$tranches,$soustraitants);
        return view('magasinier.etats.index_stat',compact('blocks','tranches','soustraitants','listlots'));
    }

    public function etatsItem($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $listlot = ProgrammeLot::where('programme_id',$idprogram)->where('id',$slug)->with('actifs')->first();
        if($listlot->programme->type_rattachement == 1){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$listlot->actif_id)->sum('prix');
        }else{
            //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
            $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$listlot->id)->has('straitantcontrat')->sum('montant');
        }

        $idtranspay = Transaction::where('programme_id',$idprogram)->where('status',2)->pluck('id')->toArray();
        $montbuy = Avancement::where('lot_id',$listlot->id)->whereIn('transac_id',$idtranspay)->sum('cout');
        //dump($montantLot,$montbuy);
        $stats =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            $data= Avancement::where('lot_id',$listlot->id)->whereYear('created_at',date('Y'))->whereMonth('created_at',$month)->where('programme_id',$idprogram)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
            $stats[$month] =$data->coutpaye;
        }
        //dd($listlot);
        return view('magasinier.etats.show_stat',compact('listlot','montantLot','montbuy','stats'));
    }

    public function etatsSearch(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $pg = ProgrammeLot::where('programme_id',$idprogram);

        if(isset($request->soustraitant)){

            $pgst = LotSoustraitant::where('soustraitant_id',$request->soustraitant)->pluck('lot_id')->toArray();
            $pg = ProgrammeLot::whereIn('id',$pgst)->with('actifs');

            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }

        }else{
            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }
        }

        $listlots = $pg->get();
        $listlots->map(function($item) use($idprogram){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });
        //dd($listlots);
        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        return view('magasinier.etats.index_stat',compact('blocks','tranches','soustraitants','listlots'));
    }

}
