<?php

namespace App\Http\Controllers\Magasinier;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function update_validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:200'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function mini_update_validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:200'],
        ]);
    }

    public function showprofil()
    {
        $data = Auth::user();
        if($data->userprofil_id == 10) {
            return view('magasinier.profil.show', compact('data'));
        }
        return redirect(route('forbiden'));
    }

    public function editprofil()
    {
        $data = Auth::user();
        if($data->userprofil_id == 10) {
            return view('magasinier.profil.edit', compact('data'));
        }
        return redirect(route('forbiden'));
    }




    public function updateprofil(Request $request, $id)
    {
        $id = decrypt($id);
        $data = User::findOrFail($id);
        if($request->password != '') {
            //validation
            $this->update_validator($request->all())->validate();
            //update
            $data->update([
                'firstname' => mb_strtoupper ($request->firstname),
                'lastname' => ucwords($request->lastname),
                'fonction' => ucfirst($request->fonction),
                'phone' => $request->phone,
                'photo' => $request->photo,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
        }
        else {
            // Checking
            $isExist = User::where('firstname', mb_strtoupper ($request->firstname))
                    ->where('lastname', ucwords($request->lastname))
                    ->where('fonction', ucfirst($request->fonction))
                    ->where('phone', $request->phone)
                    ->where('email', $request->email)
                    ->pluck('id')->first();
            if($isExist != '') {
                return back()->withInput()->with('error', 'Vous n\'avez rien modifié !');
            }
            //validation
            $this->mini_update_validator($request->all())->validate();
            //update
            $data->update([
                'firstname' => mb_strtoupper ($request->firstname),
                'lastname' => ucwords($request->lastname),
                'fonction' => ucfirst($request->fonction),
                'phone' => $request->phone,
                'photo' => $request->photo,
                'email' => $request->email,
            ]);
        }
        // déconnexion si email ou mot de passe change
        if($request->password != '' || $request->email != Auth::user()->email) {
            return redirect( route('quicklogout' ));
        }
        // view
        return redirect( route('magasinier.showprofil' ))->with('message', 'Profil modifié avec succès !');
    }


}
