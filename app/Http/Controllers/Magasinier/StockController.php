<?php

namespace App\Http\Controllers\Magasinier;

use App\ProgrammeFournisseur;
use App\StockFournisseur;
use App\StockFournisseurItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produit;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class StockController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programme_id = Session::get('program');
        $data = Produit::with('type')->with(['stocks'=>function($query) {
            $query->where('programme_id', Session::get('program'));
        }])->orderBy('libelle','asc')->get();
        // $types = Typeproduit::with('produits')->get();
        $types = $data->groupBy('type_id');
        $filtre = '';
        $fournisseurs = ProgrammeFournisseur::where('programme_id',$programme_id)->get();
        $refbon = StockFournisseur::where('programme_id',$programme_id)->where('conform',1)->get();
        return view('magasinier.stock.index', compact('data', 'types', 'filtre','fournisseurs','refbon'));
    }

    public function referenceStock()
    {
        $programme_id = Session::get('program');
        $data = StockFournisseur::where('programme_id',$programme_id)->with('fournisseur')->get();
        //dd($data);
        return view('magasinier.stock.reference', compact('data'));
    }

    public function stockBudget()
    {
        $produits = Produit::orderBy('libelle','asc')->with('type')->get();
        return view('magasinier.stock.budget',compact('produits'));
    }

    public function referenceStockItem($slug)
    {
        $programme_id = Session::get('program');
        $reference = StockFournisseur::where('ref',$slug)->where('programme_id',$programme_id)->with('fournisseur')->first();
        $stockid =$reference ? $reference->id :0;
        //dd($reference->fournisseur->nom);
        $data = StockFournisseurItem::where('stock_fournisseur_id',$stockid)->with('produits')->get();
        //dd($data);
        return view('magasinier.stock.reference_detail', compact('data','reference'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filtrer(Request $request)
    {
        //filtre ==> tous/suffisant/moyen/rupture
        if($request->filtre == null){
            return back()->with('error', "Aucune données n'a été transmise !");
        }
        $programme_id = Session::get('program');
        $types = Typeproduit::with('produits')->get();
        $filtre = $request->filtre;
        $data = [];
        if($request->filtre == 'tous') {
            $data = Produit::with('type')->with(['stocks'=>function($query) {
                $query->where('programme_id', Session::get('program'));
            }])->get();
        }
        else {
            $qry = Produit::leftJoin('stocks', 'stocks.produit_id', '=', 'produits.id')->where('stocks.programme_id', Session::get('program'));
            if($filtre == 'suffisant') {
                $qry->whereRaw('stocks.qte >= produits.seuil');
            }
            if($filtre == 'moyen') {
                $qry->whereRaw('stocks.qte < produits.seuil');
                $qry->where('stocks.qte', '!=', 0);
            }
            if($filtre == 'rupture') {
                $qry->where('stocks.qte', 0);
            }
            $prod_id = $qry->selectRaw('produits.id AS id')->pluck('id');
            $data = Produit::whereIn('id', $prod_id)->with('type')->with(['stocks'=>function($query) {
                $query->where('programme_id', Session::get('program'));
            }])->get();
            /*
            if($filtre == 'rupture') {
                // Fusion du résultat avec les produit qui n'ont jamais eu de stock
                $none = Stock::where('programme_id', Session::get('program'))->selectRaw('produit_id AS id')->pluck('id');
                $data->merge(Produit::whereNotIn('id', $none)->with('type')->with(['stocks'=>function($query) {
                    $query->where('programme_id', Session::get('program'));
                }])->get());
            }
            */
        }
        return view('magasinier.stock.index', compact('data', 'types', 'filtre'));
    }


    public function appro(Request $request, $id)
    {
        // dd($request->all());
        try{
            $programme_id = Session::get('program');

            if(!isset($request->refbon)){
                return back()->with('error', "Veuillez selectionnez le reference du bon");
            }

            if( $request->qte == 0 || $request->qte < 0)
            return back()->with('error', "La quantité doit être supérieur à 0 !!!");

            $refbon = StockFournisseur::where('ref',$request->refbon)->where('programme_id',$programme_id)->first();

            $produit_id = decrypt($id);
            //Verification de l'existence du produit dans le bon de reception
            $nbstocks = StockFournisseurItem::where('stock_fournisseur_id',$refbon->id)->where('produit_id',$produit_id)->first();
            if(!$nbstocks){
                return back()->with('error', "Ce produit n'existe pas dans le bon de reception");
            }

            $data = Stock::where('produit_id', $produit_id)->where('programme_id', $programme_id)->first();
            $qttinput = $request->qte <= 0 ? 0 : $request->qte;
            if($data == null)
                Stock::create([
                    'produit_id' => $produit_id,
                    'programme_id' =>$programme_id,
                    'qte' => $qttinput,
                ]);
            else {
                $old_stock = Stock::findOrFail($data->id);
                $new_qte = $old_stock->qte + $qttinput ;
                $old_stock->update(['qte' =>$new_qte]);
            }

            //Update du item bon de reception
            $nbstocks->update(['qte' =>$nbstocks->qte + $qttinput]);

            // $nbstocks = StockFournisseurItem::where('stock_fournisseur_id',$refbon->id)->where('produit_id',$produit_id)->first();
            // if($nbstocks){
            //     $new_qte = $nbstocks->qte + $request->qte ;
            //     $nbstocks->qte = $new_qte;
            //     $nbstocks->save();
            // }

            return back()->with('success', "Approvisionnement effectué avec succès !");
        }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }



    public function reduire(Request $request, $id)
    {
        //dd($request->all());
        try{
            if(!isset($request->refbon)){
                return back()->with('error', "Veuillez selectionnez le reference du bon");
            }

            if( $request->qte == 0 || $request->qte < 0)
            return back()->with('error', "La quantité doit être supérieur à 0 !!!");
            $programme_id = Session::get('program');

            $refbon = StockFournisseur::where('ref',$request->refbon)->where('programme_id',$programme_id)->first();

            $produit_id = decrypt($id);

            $nbstocks = StockFournisseurItem::where('stock_fournisseur_id',$refbon->id)->where('produit_id',$produit_id)->first();
            if(!$nbstocks){
                return back()->with('error', "Ce produit n'existe pas dans le bon de reception");
            }

            $data = Stock::where('produit_id', $produit_id)->where('programme_id', $programme_id)->first();
            if($data == null)
                return back()->with('error', "Une erreur c'est produite, veuillez reprendre s'il vous plaît !");
            else {
                $qttinput = $request->qte <= 0 ? 0 : $request->qte;

                //SI la quantité à reduire est supérieur à la quantité en stock
                if($qttinput > $data->qte){
                    return back()->with('error', "La quantité à reduire est supérieur à la quantité du bon de reception");
                }

                $old_stock = Stock::findOrFail($data->id);
                $new_qte = $old_stock->qte - $qttinput ;
                $old_stock->update(['qte' =>$new_qte < 0 ? 0 : $new_qte]);

                //Update du item bon de reception
                $new_qte = $nbstocks->qte >= $qttinput ? $nbstocks->qte - $qttinput : 0;
                $nbstocks->update(['qte' =>$new_qte]);

            }
            return back()->with('success', "Reduction de stock effectuée avec succès !");
        }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBON(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'fournisseur' =>'required',
            'file' =>'required',
            'main_phase2' =>'required',
            'reference'=>'required'
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }

        try {
            $programme_id = Session::get('program');
            if(count($request->main_phase2)>0) {
                $i=0;

                $file=$request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName ='assets/uploads/stocks/';
                $picture = Str::random(6).'.'. $extension;

                $stockFour = StockFournisseur::create([
                    'programme_id' =>$programme_id,
                    'fournisseur_id' => $request->fournisseur,
                    'ref' => $request->reference,
                    'file' => $picture
                ]);

                foreach($request->main_phase2 as $line) {
                    //Save Stock fournisseur item
                    StockFournisseurItem::create([
                        'programme_id' =>$programme_id,
                        'stock_fournisseur_id' =>$stockFour->id,
                        'produit_id' => $line['produit_id'],
                        'qte' => $line['qte']
                    ]);

                    if((float) $line['qte'] > 0) {
                        $data = Stock::where('produit_id', $line['produit_id'])->where('programme_id', $programme_id)->first();
                        if($data == null)
                            Stock::create([
                                'produit_id' => $line['produit_id'],
                                'programme_id' =>$programme_id,
                                'qte' => $line['qte']
                            ]);
                        else {
                            $old_stock = Stock::findOrFail($data->id);
                            $new_qte = $old_stock['qte'] + $line['qte'] ;
                            $old_stock->update(['qte' =>$new_qte]);
                        }
                        $i++;
                    }
                }
            }
            if($i>0)
                return back()->with('success', "Stock de $i matières ajouté avec succès !");
            else
                return back()->with('error', "Aucun stock n'a été modifié dû à une erreur de valeur(s) !");
            }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'main_phase2' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }

        try {
            $programme_id = Session::get('program');
            if(count($request->main_phase2)>0) {
                $i=0;
                foreach($request->main_phase2 as $line) {
                    if((float) $line['qte'] > 0) {
                        $data = Stock::where('produit_id', $line['produit_id'])->where('programme_id', $programme_id)->first();
                        if($data == null)
                            Stock::create([
                                'produit_id' => $line['produit_id'],
                                'programme_id' =>$programme_id,
                                'qte' => $line['qte']
                            ]);
                        else {
                            $old_stock = Stock::findOrFail($data->id);
                            $new_qte = $old_stock['qte'] + $line['qte'] ;
                            $old_stock->update(['qte' =>$new_qte]);
                        }
                        $i++;
                    }
                }
            }
            if($i>0)
                return back()->with('success', "Stock de $i matières ajouté avec succès !");
            else
                return back()->with('error', "Aucun stock n'a été modifié dû à une erreur de valeur(s) !");
            }
        catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return false;
    }



}
