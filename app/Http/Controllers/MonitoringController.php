<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Login_activity;
use App\Models\ActionHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MonitoringController extends Controller
{
    private const LOG_FILE = 'monitoring.log';

    public function getMetrics(): JsonResponse
    {
        $totalUsers = User::count();
        // $activities = Login_activity::whereNotNull('logout_at')->get();
        $activities = Login_activity::whereExists(function ($query) {
            $query->select(DB::raw(1))
                  ->from('users')
                  ->whereColumn('users.id', 'login_activities.user_id');
        })
        ->whereNotNull('logout_at')
        ->whereIn('id', function($query) {
            $query->select(DB::raw('MAX(id)'))
                  ->from('login_activities')
                  ->whereNotNull('logout_at')
                  ->groupBy('user_id');
        })
        ->get();
        // dd($activities);

        $activeConnections = Login_activity::whereExists(function ($query) {
            $query->select(DB::raw(1))
                  ->from('users')
                  ->whereColumn('users.id', 'login_activities.user_id');
        })
        ->whereNull('logout_at')
        ->whereIn('id', function($query) {
            $query->select(DB::raw('MAX(id)'))
                  ->from('login_activities')
                  ->whereNull('logout_at')
                  ->groupBy('user_id');
        })
        ->count();

        // $activeConnections = Login_activity::whereExists(function ($query) {
        //     $query->select(DB::raw(1))
        //           ->from('users')
        //           ->whereColumn('users.id', 'login_activities.user_id');
        // })->whereNull('logout_at')->count();

        // $activeConnections = Login_activity::whereNull('logout_at')->count();

        $totalUsageTime = 0;
        $connectionDurations = [];

        foreach ($activities as $activity) {
            $duration = Carbon::parse($activity->logout_at)->diffInSeconds(Carbon::parse($activity->login_at));
            $totalUsageTime += $duration;
            $connectionDurations[] = $duration;
        }

        $avgUsageTime = count($activities) > 0 ? $totalUsageTime / count($activities) : 0;
        // // Récupérer les dernières activités
        $recentActivities = [];
        // $recentActivities = ActionHistory::whereDate('created_at', Carbon::today())
        //     ->latest()
        //     // ->take(10)
        //     ->get()
        //     ->map(function ($activity) {
        //         return [
        //             'user_id' => $activity->user_id ?? "",
        //             'name' => $activity->user->name ?? "",
        //             'action' => $activity->operation ?? "",
        //             'timestamp' => $activity->created_at->toIso8601String()
        //         ];
        //     });

        // Récupérer les erreurs depuis le fichier de log
        $recentErrors = $this->getRecentErrorLogs();
        // return response()->json($recentErrors);

        // Calculer le taux d'erreur
        $errorStats = $this->calculateErrorRate();
        // return response()->json($errorStats);

        // Construire la réponse JSON
        $response = [
            'application_name' => 'G-STOCK ARTEMIS',
            'timestamp' => now()->toIso8601String(),
            'metrics' => [
                'user_statistics' => [
                    'total_users' => $totalUsers,
                    'active_connections' => $activeConnections,
                    'average_usage_time' => $this->formatDuration($avgUsageTime),
                    'connection_duration' => [
                        'average' => array_sum($connectionDurations) > 0 ? $this->formatDuration(array_sum($connectionDurations) / count($connectionDurations)) : "00:00:00",
                        'min' => $connectionDurations ? $this->formatDuration(min($connectionDurations)) : "00:00:00",
                        'max' => $connectionDurations ? $this->formatDuration(max($connectionDurations)) : "00:00:00",
                    ],
                    'connection_interval' => $this->calculateAverageConnectionInterval($activities)

                    // 'active_connections' => $activeConnections,
                    // 'average_usage_time' => $this->formatDuration($avgDuration),
                    // 'connection_duration' => [
                    //     'average' => $this->formatDuration($avgDuration),
                    //     'min' => $this->formatDuration($minDuration),
                    //     'max' => $this->formatDuration($maxDuration)
                    // ],
                    // 'connection_interval' => '00:10:00'
                ],
                'logs' => [
                    'user_activities' => $recentActivities,
                    'error_logs' => $recentErrors
                ],
                'error_rate' => $errorStats
            ]
        ];

        return response()->json($response);
    }

    private function getRecentErrorLogs()
    {
        $logPath = storage_path('logs/' . self::LOG_FILE);
        if (!File::exists($logPath)) {
            return [];
        }
        // return $logPath;

        // Lire les dernières lignes du fichier de log
        $today = now()->format('Y-m-d');
        $logs = collect(file($logPath))
        ->reverse()
        ->filter(function ($line) use ($today) {
            return strpos($line, $today) !== false && strpos($line, "production.ERROR") !== false;
        })
        // ->take(10)
        ->map(function ($line) {
            // if (preg_match('/\[(.*?)\] production\.ERROR: (.+?) (?:\{.*?"Code":(\d+),"File":"(.*?)".*?\}|$)/', $line, $matches)) {
                if (preg_match('/\[(.*?)\] production\.ERROR: (.*?)(?:\s+\{.*?"Code":(\d+),"File":"(.*?)","Line":(\d+)\}|\s*$)/', $line, $matches)) {
                $errorMessage = trim($matches[2]);
                if (empty($errorMessage)) {
                    return null;
                }
                $errorData = [
                    'error_code' => trim($matches[3] ?? '500'),
                    'error_file' => trim($matches[4] ?? ''),
                    // 'error_line' => trim($matches[5] ?? ''),
                    'error_message' => $errorMessage ,
                    'occurred_at' => Carbon::parse($matches[1])->toIso8601String()
                ];
                 // Si le message d'erreur est JSON, on le décode
                if (substr($errorMessage, 0, 1) === '{' && substr($errorMessage, -1) === '}') {
                    $jsonData = json_decode($errorMessage, true);
                    if (json_last_error() === JSON_ERROR_NONE) {
                        $errorData = array_merge($errorData, $jsonData);
                    }
                }

                return $errorData;
            }
            return null;
        })
        ->filter()
        ->values()
        ->toArray();

        // $logs = collect(file($logPath))
        //     ->reverse()
        //     ->take(10)
        //     ->map(function ($line) {
        //         // Parser la ligne de log
        //         if (preg_match('/\[(.*?)\] production\.ERROR: (.+?) (?:\{.*?"Code":(\d+),"File":"(.*?)".*?\}|$)/', $line, $matches)) {
        //         // if (preg_match('/\[(.*?)\] production\.ERROR: (.+?) (?:\{.*?"Code":(\d+).*?\}|$)/', $line, $matches)) {
        //         // return $line;

        //             return [
        //                 'error_code' => trim($matches[3] ?? '500'),
        //                 'error_file' => trim($matches[4] ?? ''),
        //                 'error_message' => trim($matches[2] ?? 'Unknown error'),
        //                 'occurred_at' => Carbon::parse($matches[1])->toIso8601String()
        //             ];
        //         }
        //         return null;
        //     })
        //     ->filter()
        //     ->values()
        //     ->toArray();

        return $logs;
    }

    private function calculateErrorRate()
    {
        $logPath = storage_path('logs/' . self::LOG_FILE);
        $totalErrors = 0;

        // return now()->subDay()->format('Y-m-d');
        if (File::exists($logPath)) {
            // Récupérer les logs d'aujourd'hui
            $content = File::get($logPath);
            $today = now()->format('Y-m-d');
            $lines = explode("\n", $content);
            $todayErrors = array_filter($lines, function($line) use ($today) {
                return strpos($line, $today) !== false && strpos($line, "production.ERROR") !== false;
            });
             // Compter uniquement les logs avec un message d'erreur
            $totalErrors = count(array_filter($todayErrors, function($line) {
                return preg_match('/\[(.*?)\] production\.ERROR: (.*?)(?:\s+\{.*?\}|\s*$)/', $line, $matches) && !empty(trim($matches[2]));
            }));
            // $totalErrors = count($todayErrors);
        }
        // if (File::exists($logPath)) {
        //     // Compter les erreurs des dernières 24 heures
        //     $content = File::get($logPath);
        //     $yesterday = now()->subDay()->format('Y-m-d');
        //     $totalErrors = substr_count($content, "production.ERROR");
        //     // $totalErrors = substr_count($content, "$yesterday") &&
        //     //               substr_count($content, "production.ERROR");
        // }

        // Estimation du nombre total de requêtes (à adapter selon vos besoins)
        // $totalRequests = Activity::where('created_at', '>=', now()->subDay())->count();
        // $errorRate = $totalRequests > 0 ? ($totalErrors / $totalRequests) * 100 : 0;

        // $countActivities = ActionHistory::whereDate('created_at', Carbon::today())->count();
        return [
            'total_errors' => $totalErrors,
            'error_rate_percentage' => round(($totalErrors )/100 , 2)
            // 'error_rate_percentage' => $countActivities > 0 ?  round((($totalErrors / $countActivities) * 100)/100 , 2): 0
            // 'error_rate_percentage' => round($errorRate, 2)
        ];
    }


    private function calculateAverageConnectionInterval($activities)
    {
        $intervals = [];
        $activities = $activities->sortBy('login_at');

        for ($i = 1; $i < count($activities); $i++) {
            $interval = Carbon::parse($activities[$i]->login_at)->diffInSeconds(Carbon::parse($activities[$i-1]->login_at));
            $intervals[] = $interval;
        }

        $avgInterval = count($intervals) > 0 ? array_sum($intervals) / count($intervals) : 0;
        return $this->formatDuration($avgInterval);
    }


    private function formatDuration($seconds)
    {
        if (!$seconds) return '00:00:00';

        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds % 3600) / 60);
        $seconds = $seconds % 60;

        return sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);
    }

}
