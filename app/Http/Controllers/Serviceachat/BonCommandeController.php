<?php

namespace App\Http\Controllers\Serviceachat;

use App\BC;
use App\BCitem;
use App\Bon_demande;
use App\Commande;
use App\CommandeItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\ProgrammeFournisseur;
use App\ProgrammePivotUser;
use App\ProgrammeProduit;
use App\StockFournisseur;
use App\StockFournisseurItem;
use App\Typeproduit;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;
use Illuminate\Support\Str;

class BonCommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getDemande($idemende)
    {
        $produits = CommandeItem::where('commande_id',$idemende)->with(['produit'=>function($item){
            $item->with('type');
        }])->get();

        $produits->map(function($item){
            $pricepgr = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->programprice = $pricepgr ? number_format($pricepgr->prix, 0, '', ' ') : 0 ;

            //faire des controlle d'affichage ou pas
            $boncmditem = BCitem::where('commande_item_id',$item->id)->where('produit_id',$item->produit_id)->sum('qte');
            //$nbappro = StockFournisseurItem::whereIn('bc_item_id',$boncmditem)->where('produit_id',$item->produit_id)->sum('qte');
            $item->show = 0;
            if($boncmditem < $item->qte){
                $item->show = 1;
                $item->maxcmd = $item->qte - $boncmditem;
            }
        });

        if(count($produits)>0){
            return $produits;
        }
        return [];
    }

    public function getDispo($qte,$item,$cmd)
    {
        //dd($qte,$item,$cmd);
        $check = CommandeItem::where('id',$item)->where('commande_id',$cmd)->first();
        if($check){
            //savoir si toute la quatité a deja eté livre
            $nbcmde = BCitem::where('commande_item_id',$check->id)->where('produit_id',$check->produit_id)
                    //->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                    //->where('bon_commandes.status',2)
                    ->sum('qte');

            // a faire apres // C'est fait mtn 😁
            $int_value = (int) $check->qte;
            //dump($nbcmde,$int_value);
            if($nbcmde > $int_value){
                return 'ok';
            }

            $reste = $check->qte - $nbcmde;
            if($reste >= $qte){
                return 'ok';
            }
            return $reste;
            // return 'error';

        }
        return "error";
    }

    public function getByStatus($status)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',$status)->count();
        return $bcs;
    }

    public function create()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::has('user')->with('user')->where('status',2)->where('etat',1)->where('programme_id',$programme_id)->get();
        $fournisseurs = ProgrammeFournisseur::where('programme_id',$programme_id)->orderBy('nom','asc')->get();

        //traitement automatique des demandes totalement livres
        (new CommandeController)->autoTraitement($demandes->pluck('id'));

        $check = null;
        if(Request('code')){
            $check = Commande::has('user')->with('user')->where('id',Request('code'))->where('status',2)->where('etat',1)->first();
            if(!$check){
                return redirect()->back()->with('error','Cette commande est introuvable.');
            }
        }

        return view('service_achat.bc.create',compact('demandes','fournisseurs','check'));
    }

    public function getRefBC()
    {
        $programme_id = $this->getIdProgram();
        $code = "BC".date('ym');
        $count = BC::where('programme_id',$programme_id)->withTrashed()->count() + 1;
        $num=sprintf("%04d",$count);
        $val = $code.'-'.$programme_id.'-'.$num;

        return $val;
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $valider = Validator::make($request->all(), [
            'demande' => ['required'],
            'typebc' => ['required'],
            'fournisseur' => ['required'],
            'mode' =>['required'],
            'date' =>['required'],
            'datasession' =>['required'],
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }

        $slug = $this->getRefBC();
        //dd($slug);
        $bc = new BC();
        $bc->programme_id = $programme_id;
        $bc->ref = $slug;
        $bc->commande_id = $request->demande;
        $bc->fournisseur_id = $request->fournisseur;
        $bc->typebc = $request->typebc;
        $bc->mode = $request->mode;
        $bc->datepaiement = $request->date;
        $bc->user_id = Auth::user()->id;
        $bc->status = 2;
        $bc->proforma = $request->proforma;
        $bc->tva = $request->tva;
        $bc->priorite = $request->prioriter ?? null;
        $bc->save();

        $mttsm = 0;
        foreach ($request->datasession as $data){
            $item = explode(';',$data);
            //dump($item);
            //select Bstock
            $check = BCitem::where('commande_item_id',$item[0])->where('bon_commande_id',$bc->id)->first();
            if(!$check){
                //dump($item[4],is_int($item[5]) ? (($item[5] /100) * $item[4]) : 0,floatval($item[4]));
                //dump(!empty($item[5]) ? (($item[5] /100) * $item[4]):0);
                $pxht = $item[4] - (!empty($item[5]) ? (($item[5] /100) * $item[4]) : 0);
                //dd(floatval($item[4])- is_int($item[5]) ? ((floatval($item[5]) /100) * floatval($item[4])) : 0);
                $mttble = $pxht * floatval($item[3]);
                //dd($pxht,$mttble);
                $mttsm = $mttsm + $mttble;

                $itemdmd = CommandeItem::where('id',$item[0])->first();
                $bcitem = new BCitem();
                $bcitem->bon_commande_id = $bc->id;
                $bcitem->commande_item_id = $item[0];
                $bcitem->programme_id = $programme_id;
                $bcitem->produit_id = $itemdmd ? $itemdmd->produit_id : 0;
                $bcitem->qte = $item[3];
                $bcitem->price = $item[4];
                $bcitem->remise = !empty($item[5]) ? $item[5] : 0;
                $bcitem->mtt = $mttble;
                $bcitem->save();
            }
        }
        // dd($mttsm);
         $bcitem = BC::where('id',$bc->id)->first();
        //verifier si retenu existe
        if(isset($request->retenu) and $request->retenu=="OUI"){
            $retient = (2 * $mttsm)/100 ;
            $mttsmAvcRtn = $mttsm - $retient;

            $bcitem->retenu = $retient;
        }else{
            $mttsmAvcRtn = $mttsm;
        }
        //save somme tt BC

        $bcitem->montant = $mttsm;
        $bcitem->save();

        //notifie le OPC
        // $pgg = Programme::where('id',$programme_id)->first();
        // $message = "<p>Bonjour Monsieur/Madame ,</p><p>Vous avez un bon de commande en attente de traitement.</p>";
        // $message.= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$slug."<br><b>MONTANT : </b>".$mttsm."</p>";
        // $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

        // $sujet = "NOUVEAU BON DE COMMANDE EN ATTENTE DE TRAITEMENT - GESTION DE STOCK - ".$pgg->libelle;
        // $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
        // $user = User::where('role_id',5)->whereIn('id',$pguser)->pluck('email')->toArray();

        $mttsm = number_format(str_replace(' ', '', $mttsmAvcRtn), 0, '', ' ');

        //NOTIFIIER LE FOURNISSEUR SI EMAIL PRESENT
        // $forunis = ProgrammeFournisseur::where('id',$request->fournisseur)->first();
        // if($forunis and isset($forunis->email)){
        //     $email[] = $forunis->email;
        //     $message = "<p>Bonjour Monsieur/Madame ,</p><p>Vous avez un nouveau bon de commande.</p>";
        //     $message.= "<p><b>REFERENCE : </b>".$slug."<br><b>MONTANT TOTAL HT : </b>".$mttsm." FCFA</p>";
        //     $message .= (isset($request->retenu) and $request->retenu=="OUI") ? "<p><b>RETENUE :</b> 2%</p>" : "";
        //     $message .="<br>";
        //     // Show table of items
        //     $message .= "<table style='width:100%;border-collapse:collapse;border:1px solid #ddd'>";
        //     $message .= "<thead style='background-color:#ddd'>";
        //     $message .= "<tr>";
        //     $message .= "<th style='border:1px solid #ddd;padding:5px'>PRODUIT</th>";
        //     $message .= "<th style='border:1px solid #ddd;padding:5px'>QUANTITE</th>";
        //     $message .= "<th style='border:1px solid #ddd;padding:5px'>PRIX UNITAIRE</th>";
        //     $message .= "<th style='border:1px solid #ddd;padding:5px'>REMISE</th>";
        //     $message .= "<th style='border:1px solid #ddd;padding:5px'>MONTANT</th>";
        //     $message .= "</tr>";
        //     $message .= "</thead>";
        //     $message .= "<tbody>";
        //     foreach ($request->datasession as $data){
        //         $item = explode(';',$data);
        //         $pxht = $item[4] - (!empty($item[5]) ? (($item[5] /100) * $item[4]) : 0);
        //         $remis = !empty($item[5]) ? $item[5] : 0;
        //         $mtts = number_format(str_replace(' ', '', $pxht * floatval($item[3])), 0, '', ' ');
        //         $pu = number_format(str_replace(' ', '', $item[4]), 0, '', ' ');
        //         $message .= "<tr>";
        //         $message .= "<td style='border:1px solid #ddd;padding:5px'>".$item[1]."</td>";
        //         $message .= "<td style='border:1px solid #ddd;padding:5px'>".$item[3]."</td>";
        //         $message .= "<td style='border:1px solid #ddd;padding:5px'>".$pu."</td>";
        //         $message .= "<td style='border:1px solid #ddd;padding:5px'>".$remis." %</td>";
        //         $message .= "<td style='border:1px solid #ddd;padding:5px'>".$mtts."</td>";
        //         $message .= "</tr>";
        //     }
        //     $message .= "</tbody>";
        //     $message .= "</table>";
        //     $message .= "<br><br>";
        //     $message .= "<p>Cordialement,</p>";

        //     $sujet = "NOUVEAU BON DE COMMANDE - ARTEMIS";
        //     $this->sendMailNotificaton($message,$sujet,$email);
        // }

        //NOTIFIER LE DAF SI TYPEBC==DECAISSEMENT
        if($request->typebc==2){
            $pgg = Programme::where('id',$programme_id)->first();
            $message = "<p>Bonjour Monsieur/Madame ,</p><p>Vous avez un bon de commande en décaissement.</p>";
            $message.= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br><b>REFERENCE : </b>".$slug."<br><b>MONTANT : </b>".$mttsm." FCFA</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";

            $sujet = "NOUVEAU BON DE COMMANDE EN DECAISSEMENT - ".$pgg->libelle. " - G-STOCK";
            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',7)->whereIn('id',$pguser)->pluck('email')->toArray();

            $this->sendMailNotificaton($message,$sujet,$user);
        }


        return redirect()->route('sabc.create')->with('success','La bon de commande <b>'.$slug.'</b> a bien été ajouté.');

    }

    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($slug);
        $demande = BC::where('programme_id',$programme_id)->where('id',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        return view("service_achat.bc.show",compact('demande'));

    }

    public function showPdf($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($slug);
        $data = BC::where('programme_id',$programme_id)->where('ref',$id)->with('demande')->with('fournisseur')->with('user')->with('items')->first();
        //dd($data);
        //User Sa, Daf
        $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
        $sa = User::where('role_id',6)->whereIn('id',$pguser)->first();
        $daf = User::where('role_id',7)->whereIn('id',$pguser)->first();

        $pdf = PDF::loadView('service_achat.bc.bc_download',compact('data','sa','daf'));
        //$pdf->setPaper('a4', 'landscape');
        //return view('service_achat.bc.bc_download',compact('data','sa','daf'));
        $fournname = Str::slug($data->fournisseur ? $data->fournisseur->nom:'na');
        $prograname = $data->programme ? $data->programme->slug : 'na';
        $libel = $id.'_'.$fournname.'_'.$prograname;
        return $pdf->stream($libel.'-'.date("d-m-y").'.pdf');
        return $pdf->download($libel.'.pdf');

    }

    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',1)->with('demande')->with('fournisseur')->with('user')->with('items')->orderBy('id','desc')->get();

        return view("service_achat.bc.waiting",compact('bcs'));
    }

    public function valider()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',2)->with('demande')->with('fournisseur')->with('user')->with('items')->with('bonreception')->orderBy('id','desc')->get();
        return view("service_achat.bc.valider",compact('bcs'));
    }

    public function rejeter()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $bcs = BC::where('programme_id',$programme_id)->where('status',4)->with('demande')->with('fournisseur')->with('user')->with('items')->orderBy('id','desc')->get();

        return view("service_achat.bc.rejeter",compact('bcs'));
    }

    public function delete($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($slug);
        //verifier si le bon de commande à deja été receptionné
        $check = StockFournisseur::where('programme_id',$programme_id)->where('bc_id',$id)->first();
        if($check){
            return redirect()->back()->with('error','Ce bon de commande a déjà été receptionné.');
        }

        $bc = BC::where('programme_id',$programme_id)->where('id',$id)->first();
        if($bc){
            //delete les items
            BCitem::where('programme_id',$programme_id)->where('bon_commande_id',$bc->id)->delete();
            //liberer la commande
            Commande::where('id',$bc->commande_id)->update(['etat'=>1]);

            $bc->delete();

            return redirect()->back()->with('success','La bon de commande a bien été supprimé.');
        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez recommencez plus tard.');
    }
}
