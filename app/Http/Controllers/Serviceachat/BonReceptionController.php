<?php

namespace App\Http\Controllers\Serviceachat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StockFournisseur;
use Illuminate\Support\Facades\Session;

class BonReceptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $bons = StockFournisseur::where('programme_id',$idprogram)->where('conform',1)->orderBy('id','desc')->get();

        return view('service_achat.br.index',compact('bons'));
    }

    public function indexconform()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $bons = StockFournisseur::where('programme_id',$idprogram)->where('conform',2)->orderBy('id','desc')->get();

        return view('service_achat.br.index_conform',compact('bons'));
    }

    public function show($id){
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $id = decrypt($id);
        $bon = StockFournisseur::where('programme_id',$idprogram)->where('id',$id)->first();

        return view('service_achat.br.show',compact('bon'));
    }


    public function confirm($id) {
        // dd($id);
        $id = decrypt($id);

        $bon = StockFournisseur::where('id',$id)->first();
        $bon->conform = 2 ;
        $bon->save();

        return redirect()->route('sabr.index')->with('success','Bon de reception confirmé avec succès');
    }

    public function getBonStatus() {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        return StockFournisseur::where('programme_id',$idprogram)->where('conform',1)->count();
    }
}
