<?php

namespace App\Http\Controllers\Serviceachat;

use App\BCitem;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Commande;
use App\CommandeItem;
use App\Http\Controllers\BonController;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LigneBudgetaireController;
use App\Produit;
use App\Programme;
use App\ProgrammeProduit;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CommandeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }


    public function getByStatus($status,$etat,$lign=null)
    {
        $programme_id = $this->getIdProgram();
        if($lign && $lign=='ligne'){
            return Commande::where('programme_id',$programme_id)->where('status',$status)->where('ligne_id',0)->count();
        }
        return Commande::where('programme_id',$programme_id)->where('status',$status)->where('etat',$etat)->count();
    }

    public function majcmd()
    {
        $programme_id = $this->getIdProgram();
        $commandes = Commande::where('programme_id',$programme_id)->where('ligne_id',0)->where('status',2)->get();
        return view('service_achat.commandes.majcmd',compact('commandes'));
    }


    public function show($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);
        $demande = Commande::where('id',$id)->where('status',2)->with('produits')->with('user')->with('lignebudget')->first();
        if(!$demande){
            return redirect()->back();
        }
        $demande->datalignebudget = null;
        if($demande->lignebudget){
            $demande->datalignebudget = (new LigneBudgetaireController)->getConsoLignBudget($demande->lignebudget->id);
        }

        //check to show btn "cloturer"
        //Count all demande items
        $nbqtes = $demande->produits->sum('qte');
        $commandeItemIds = $demande->produits->pluck('id');
        $nbbc =BCitem::whereIn('commande_item_id',$commandeItemIds)
                ->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                ->where('bon_commandes.status',2)
                ->sum('qte');
        $showcloturer = ($nbqtes>$nbbc) ? true : false;

        return view('service_achat.commandes.show',compact('demande','showcloturer'));
    }

    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //traitement automatique des demandes totalement livres
        $demandes = Commande::where('status',2)->where('etat',1)->where('programme_id',$programme_id)->pluck('id')->toArray();
        $this->autoTraitement($demandes);
        $demandes = Commande::where('status',2)->where('etat',1)->where('programme_id',$programme_id)->with('produits')->with('user')->orderBy('updated_at','desc')->get();

        return view('service_achat.commandes.waiting',compact('demandes'));
    }

    public function traiter($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //Decrypte
        $id = decrypt($slug);

        //List total des demandes de produits
        $demandes = CommandeItem::where('commande_id',$id)->where('programme_id',$programme_id)->get();
        $ids = $demandes->pluck('id');
        $nbdemande = $demandes->sum('qte');

        $nbcmder =\App\BCitem::whereIn('commande_item_id',$ids)
        ->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
        ->where('bon_commandes.status',2)
        ->sum('qte');
        //dd($nbdemande,$nbcmder);

        if($nbdemande>$nbcmder){
            return redirect()->back()->with('error','Veuillez etablier les BC de la demande avant de la clôturé.');
        }

        $commande = Commande::where('id',$id)->first();
        if($commande){
            $commande->etat=2;
            $commande->save();
        }

        return redirect()->back()->with('success','Le bon de demande a bien été clôturé.');

    }

    public function autoTraitement($ids)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        if(count($ids)>0){
            foreach($ids as $id){

                //List total des demandes de produits
                $demandes = CommandeItem::where('commande_id',$id)->where('programme_id',$programme_id)->get();
                $ids = $demandes->pluck('id');
                $nbdemande = $demandes->sum('qte');

                $nbcmder =\App\BCitem::whereIn('commande_item_id',$ids)
                ->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                ->where('bon_commandes.status',2)
                ->sum('qte');
                //dd($nbdemande,$nbcmder);

                if($nbdemande>$nbcmder){
                    //return redirect()->back()->with('error','Veuillez etablier les BC de la demande avant de la clôturé.');
                }else{
                    $commande = Commande::where('id',$id)->first();
                    if($commande){
                        $commande->etat=2;
                        $commande->save();
                    }
                }
            }
        }

    }

    public function create() {
        $programme_id = $this->getIdProgram();
        $types = Typeproduit::with('produits')->get();
        $lignbudgets = Programme::where('id',$programme_id)->with('ligneBudgetaires')->first();
        // $listproduits = ProgrammeProduit::where('programme_id',$programme_id)->whereHas('produit',function($q){$q->where('type_materiel','materiel');})
        // ->with(['produit'=>function($q){$q->with('type');}])->get();
        //dd($listproduits);
        return view('service_achat.commandes.create',compact('types','lignbudgets'));
    }

    public function cloture() {
        if(Request('code')){
            $code = decrypt(Request('code'));
            //dd(Request('code'),$code);
            $check = Commande::has('produits')->with('produits')->where('id',$code)->where('status',2)->where('etat',1)->first();
            if(!$check){
                return redirect()->back()->with('error','Cette commande est introuvable.');
            }
            //dd($check);
            $listproduits = $check->produits;
            foreach($listproduits as $item){
                //Savoir si la quantité demandée a été commandée en BC
                $nbcmder =BCitem::where('commande_item_id',$item->id)
                        ->where('bon_commandes.programme_id',$check->programme_id)
                        ->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                        ->where('bon_commandes.status',2)
                        ->where('bon_commandes.close',0)
                        ->sum('qte');

                $reste = $item->qte - $nbcmder;
                //dd($reste , $item->qte , $nbcmder);
                if($reste!=0){
                    //mettre à jour la commande
                    $item->qte = $nbcmder == 0 ? 0 : $reste;
                    $item->save();
                }
            }
            //mettre à jour la commande
            $check->etat = 2;
            $check->save();
            return redirect()->route('sacommande.waiting')->with('success','La commande a bien été clôturée.');

        }else{
            return redirect()->back()->with('error','Une erreur est survenue.');
        }
    }

    public function majlign(Request $request) {
        //dd( $request->all());
        $id = decrypt($request->cmdid);
        $commande = Commande::where('id',$id)->first();
        if(!$commande){
            return redirect()->back()->with('error','Cette commande est introuvable.');
        }

        $commande->ligne_id = $request->lignid;
        $commande->save();

        return redirect()->back()->with('success','La ligne budgétaire a bien été mise à jour.');
    }


}
