<?php

namespace App\Http\Controllers\Serviceachat;

use App\BDSpec;
use App\BDSpecTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Programme;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;

class DemandesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function getByStatus($var,$etat=null)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',$var)->count();
        if(isset($etat)){
            $demandes = BDSpec::where('programme_id',$programme_id)->where('genere',$etat)->where('status',$var)->count();
        }
        return $demandes;
    }

    public function create()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        return view('service_achat.demandes.create');
    }


    public function waiting()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',2)->where('genere',0)->orderBy('updated_at','desc')->get();
        //dd($demandes);
        return view('service_achat.demandes.waiting',compact('demandes'));
    }

    public function valid()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',2)->where('genere',1)->orderBy('updated_at','desc')->get();

        return view('service_achat.demandes.valid',compact('demandes'));
    }

    public function traiter()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',3)->where('genere',1)->orderBy('updated_at','desc')->get();
        //dd($demandes);
        return view('service_achat.demandes.traiter',compact('demandes'));
    }

    public function rejet()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $demandes = BDSpec::where('programme_id',$programme_id)->where('status',4)->orderBy('updated_at','desc')->get();
        //dump($demandes);
        return view('service_achat.demandes.reject',compact('demandes'));
    }

    public function rejeter(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        //dd($request->all());
        Validator::make($request->all(),[
            'motif' =>'required',
            'bonid' =>'required',
        ])->validate();


        $data = BDSpec::where('programme_id',$programme_id)->where('slug',$request->bonid)->where('status',1)->first();
        //dd($data);
        if($data){
            $data->status = 4;
            $data->motif = $request->motif;
            $data->save();

            //NOTIFIER L'EMETTEUR DU REJET
            $pgg = Programme::where('id',$programme_id)->first();
            $message = "<p>Bonjour Monsieur/Madame </p><p>Votre demande n°<b>". $data->slug."</b> (".$data->titre.") a été rejetée par le Service Achat. ";
            $message .= "<br> Veuillez trouver ci-dessous le motif de rejet : </p><p>“ ".$data->motif." ”</p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "DEMANDE SPECIALE REJETEE - G-STOCK";

            $user = User::where('id',$data->user_id)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('sademande.waiting')->with('success' ,'La demande a bien été rejetée.');

        }else{
            return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
        }
    }

    public function validBC($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($slug);
        //dd($id);
        $demandes = BDSpec::where('id',$id)->where('programme_id',$programme_id)->first();
        if($demandes){
            $demandes->genere=1;
            $demandes->status=2;
            $demandes->save();

            $mtt = number_format(str_replace(' ', '', $demandes->montant), 0, '', ' ');

            //NOTIIFIER AUTEUR QUE BON VALIDER
            $pgg = Programme::where('id',$programme_id)->first();
            $message = "<p>Bonjour Monsieur/Madame </p><p>Le bon de décaissement de votre demande n°<b>". $demandes->slug."</b> (".$demandes->titre.") a été effectué par le Service Achat.";
            $message .= "<br> Veuillez trouver ci-dessous les informations : </p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br>";
            $message .= "<b>MONTANT : </b>".$mtt." FCFA</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "DEMANDE SPECIALE BS GENERER - G-STOCK";

            $user = User::where('id',$demandes->user_id)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            //NOTIFIER LA COMPTABILITE
            $message = "<p>Bonjour Monsieur/Madame </p><p>Une demande n°<b>". $demandes->slug."</b> (".$demandes->titre.") a été validée par le Service Achat. ";
            $message .= "<br> Veuillez trouver ci-dessous les informations : </p>";
            $message .= "<p><b>PROGRAMME : </b>".$pgg->libelle."<br>";
            $message .= "<b>MONTANT : </b>".$mtt." FCFA<br>";
            $message .= "<b>MOTIF : </b><br>“ ".$demandes->description." ”</p>";
            $message .= "<p><a class='btnbacksite' target='_blank' href='".route('index')."'> Aller au site</a></p>";
            $sujet = "DEMANDE SPECIALE BS GENERER - G-STOCK";

            $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
            $user = User::where('role_id',9)->whereIn('id',$pguser)->pluck('email')->toArray();
            $this->sendMailNotificaton($message,$sujet,$user);

            return redirect()->route('sademande.waiting')->with('success','Le bon de commande a bien été généré.');
        }

        return redirect()->back()->with('erro','Désolé ! Nous avons rencontré un souci.');
    }

    public function pdf($id)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $data = BDSpec::where('programme_id',$programme_id)->where('id',$id)->first();
        //dd($data);
        $pdf = PDF::loadView('service_achat.demandes.pdf',compact('data'));
        //$pdf->setPaper('a4', 'landscape');
        //return view('service_achat.demandes.pdf',compact('data'));
        //return $pdf->stream('etat_gestion_stock-'.date("d-m-y").'.pdf');
        return $pdf->download('demande-speciale-'.$data->slug.'.pdf');
    }

    public function paiement(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        dd($request->all());
        Validator::make($request->all(),[
            'moyen' =>'required',
            'bonid' =>'required',
            'bordereau' =>'required',
        ])->validate();

        $data = BDSpec::where('programme_id',$programme_id)->where('slug',$request->bonid)->where('status',2)->first();
        //dd($data);
        if($data){
            $trans = new BDSpecTrans();
            $trans->user_id = Auth::user()->id;
            $trans->montant = $data->montant;
            $trans->moyen = $request->moyen;
            $trans->programme_id = $data->programme_id;
            $trans->demande_id = $data->id;

            if($request->file('bordereau')){
                $file = $request->file('bordereau');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'assets/demandespecials/';
                $picture = 'bordereau-'.date('dmYhis').'.'. $extension;
                $file->move($folderName,$picture);
                $trans->file = $picture;
            }
            $trans->save();

            $data->status = 3;
            $data->save();

            return redirect()->back()->with('success' ,'La demande a bien été traitée.');

        }else{
            return redirect()->back()->with('error','Désolé! Nous avons rencontré un souci, veuillez réessayer les champs');
        }
    }

}
