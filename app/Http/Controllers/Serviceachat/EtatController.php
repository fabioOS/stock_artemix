<?php

namespace App\Http\Controllers\Serviceachat;

use App\BC;
use App\Bon_demande;
use App\Commande;
use App\Exports\BcManyExport;
use App\Exports\BonDemandeExport;
use App\Exports\BonDemandeManyExport;
use App\Exports\BonSortieManyExport;
use App\Exports\FacturationExport;
use App\Exports\FacturationManyExport;
use App\Exports\ProduitExport;
use App\FactureFournisseur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LotSoustraitant;
use App\ProgrammeFournisseur;
use App\ProgrammePivotUser;
use App\ProgrammeProduit;
use App\ProgrammeSoustraitant;
use App\ProgrammeSoustraitantcontrat;
use App\Stock;
use App\Transaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class EtatController extends Controller
{
    public $typereturn ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->typereturn = "pdf";
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function etatsBc()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::has('user')->with('user')->where('status',2)->get();
        $fournisseurs = ProgrammeFournisseur::where('programme_id',$programme_id)->orderBy('nom','asc')->get();
        return view('service_achat.etats.bc.index',compact('fournisseurs','demandes'));
    }

    public function etatsBcStore(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //dd($request->all());
        Validator::make($request->all(), [
            'typexport' => ['required'],
        ])->validate();

        $bondmd = $request->bondmd;
        $fournisseur = $request->fournisseur;
        $statu = $request->statu;
        $typebc = $request->typebc;
        $date = $request->datedebut;
        $date2 = $request->datefin;
        $typexport = $request->typexport;

        $requete = BC::where('programme_id',$programme_id)->with(['demande'=>function($q){$q->with('lignebudget');}])->with('fournisseur')->with('user')->with('items')->with('programme');

        if(isset($bondmd)){
            $requete->where('commande_id',$bondmd);
        }

        if(isset($fournisseur)){
            $requete->where('fournisseur_id',$fournisseur);
        }

        if(isset($typebc)){
            $requete->where('typebc',$typebc);
        }

        if(isset($statu)){
            $requete->where('status',$statu);
        }

        if(isset($date) and isset($date2)){
            $dateD = $date .' 00:00:00';
            $dateF = $date2 .' 23:59:59';

            $requete->whereBetween('created_at',[$dateD, $dateF]);
        }

        $datas = $requete->get();
        // dd($datas);
        if(isset($request->sousprogramme_id)){
            $datas = $datas->where('demande.programmesous_id',$request->sousprogramme_id);
        }
        // $data = $datas;
        // return view('service_achat.etats.bc.excel_all',compact('data'));

        if(count($datas)>0){

            if($typexport){
                $this->typereturn = $typexport;
            }

            if($this->typereturn == 'pdf'){
                $html = "";

                //VIEW LIST BC
                $view= view('service_achat.etats.bc.pdf',compact('datas','date','date2'));
                $html .= $view->render();

                foreach($datas as $data){
                    //VIEW ITEM BC
                    $view= view('service_achat.etats.bc.pdf_item',compact('data'));
                    $html .= $view->render();
                }

                $allhtml = '<html>'.$html.'</html>';

                //return $allhtml;

                $pdf = \PDF::loadHTML($allhtml);
                $sheet = $pdf->setPaper('a4','portrait');
                return $sheet->download('etat_bon_commande-'.date("d-m-Y").'.pdf');

                //$pdf = PDF::loadView('service_achat.etats.bc.pdf',compact('datas','date','date2'));
                //return view('service_achat.etats.bc.pdf',compact('datas','date','date2'));
                //return $pdf->download('etat_bon_commande-'.date("d-m-y").'.pdf');

            }else{
                try{
                    $date=new \DateTime('now');
                    $date = $date->format('d-m-Y');
                    $lib= "etat_bon_commande-".$date.'.xlsx';

                    return Excel::download(new BcManyExport($datas), $lib);

                }catch (\Exception $e){
                    dd($e->getMessage());
                    return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                }
            }

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');

    }

    // PRODUIT
    public function etatsPdt()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        return view('service_achat.etats.produits.index');
    }

    public function etatsPdtStore(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //dd($request->all());
        Validator::make($request->all(), [
            'nveau' => ['required'],
            'typexport' => ['required'],
        ])->validate();

        $nveau = $request->nveau;
        $typexport = $request->typexport;

        //$requete = ProgrammeProduit::where('programme_id',$programme_id)->with(['produit'=>function($q){$q->with('type');}]);
        $requete = Stock::where('programme_id',$programme_id)->with('programme')->has('produits')->with(['produits'=>function($q){$q->with('type');}]);

        if($nveau == 1){
            //FAIBLE
            $requete->where('qte','<=',10);
        }elseif ($nveau == 2) {
            //DISPONIBLE
            $requete->where('qte','>',10);
        }else{
            //INDISPONIBLE
            $requete->where('qte','<=',5);
        }

        $datas = $requete->get();
        $datas->map(function($item){
            $pgmpdt = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->price = $pgmpdt ? $pgmpdt->prix : 0;
        });
        //dd($datas);

        if(count($datas)>0){
            $libpgrm = $datas[0]->programme->libelle;

            if($typexport){
                $this->typereturn = $typexport;
            }

            if($this->typereturn == 'pdf'){
                $pdf = PDF::loadView('service_achat.etats.produits.pdf',compact('datas'));
                //return view('service_achat.etats.produits.pdf',compact('datas'));
                return $pdf->download('etat_produit-'.$libpgrm.'-'.date("d-m-y").'.pdf');

            }else{
                try{
                    $date=new \DateTime('now');
                    $date = $date->format('d-m-Y');
                    $lib= "etat_produit-".$libpgrm."-".$date.".xlsx";

                    return Excel::download(new ProduitExport($datas), $lib);

                }catch (\Exception $e){
                    return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                    //dd($e->getMessage());
                }
            }
        }
        return redirect()->back()->with('error','Aucun produit disponible.');

    }

    //DAF PRODUITS
    public function etatsPdtDaf()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        return view('daf.etats.produits.index');
    }

    //DAF BC
    public function etatsBcDaf()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $demandes = Commande::has('user')->with('user')->where('status',2)->get();
        $fournisseurs = ProgrammeFournisseur::where('programme_id',$programme_id)->orderBy('nom','asc')->get();
        return view('daf.etats.bc.index',compact('fournisseurs','demandes'));
    }

    //CHEF PROJET
    public function etatCmdCp()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
        $users = User::where('role_id',2)->whereIn('id',$pguser)->get();

        $blade = $this->getBlade(Auth::user()->role_id);

        return view('chef_projet.etats.demandes.demande',compact('users','blade'));
    }

    public function etatCmdCpStore(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //dd($request->all());
        Validator::make($request->all(), [
            'typexport' => ['required'],
        ])->validate();

        $demandeur = $request->demandeur;
        $statu = $request->statu;
        $date = $request->datedebut;
        $date2 = $request->datefin;
        $typexport = $request->typexport;

        $requete = Commande::where('programme_id',$programme_id)->with('produits')->with('programme')->with('user')->with('programmesous');

        if(isset($demandeur)){
            $requete->where('user_id',$demandeur);
        }

        if(isset($statu)){
            $requete->where('status',$statu);
        }

        if(isset($date) and isset($date2)){
            $dateD = $date .' 00:00:00';
            $dateF = $date2 .' 23:59:59';

            $requete->whereBetween('created_at',[$dateD, $dateF]);
        }

        if(isset($request->sousprogramme_id)){
            $datas = $requete->where('programmesous_id',$request->sousprogramme_id);
        }
        $datas = $requete->get();
        //dd($datas);
        if(count($datas)>0){
            $libpgrm = $datas[0]->programme->libelle;

            if($typexport){
                $this->typereturn = $typexport;
            }

            if($this->typereturn == 'pdf'){

                $pdf = PDF::loadView('chef_projet.etats.demandes.pdf',compact('datas'));
                //return view('chef_projet.etats.demandes.pdf',compact('datas'));
                return $pdf->download('etat_bon_commande-'.$libpgrm.'-'.date("d-m-y").'.pdf');

            }else{
                try{
                    $date=new \DateTime('now');
                    $date = $date->format('d-m-Y');
                    $lib= "etat_bon_demande-".$libpgrm."-".$date.".xlsx";

                    return Excel::download(new BonDemandeManyExport($datas), $lib);

                }catch (\Exception $e){
                    return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                    //dd($e->getMessage());
                }
            }

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');


    }

    //MAGASINIER
    public function etatsBsGs()
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        //dd('dd');
        $listBon = Bon_demande::where('programme_id',$programme_id)->groupBy('user_id')->get();
        //dd($listBon);

        $users = ProgrammeSoustraitantcontrat::where('programme_id',$programme_id)->where('etat',2)->groupBy('soustraitant_id')->get();
        //dd($users[0]->straitant);
        return view('magasinier.etats.index',compact('users','listBon'));
    }

    public function etatsBsGsStore(Request $request)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        //dd($request->all());
        Validator::make($request->all(), [
            'typexport' => ['required'],
        ])->validate();

        $agent = $request->agent;
        $demandeur = $request->straitant;
        $statu = $request->statu;
        $date = $request->datedebut;
        $date2 = $request->datefin;
        $typexport = $request->typexport;

        $requete = Bon_demande::where('programme_id',$programme_id);

        if(isset($demandeur)){
            $requete->where('soustraitant_id',$demandeur);
        }

        if(isset($agent)){
            $requete->where('user_id',$agent);
        }

        if(isset($statu)){
            $requete->where('status',$statu);
        }

        if(isset($date) and isset($date2)){
            $dateD = $date .' 00:00:00';
            $dateF = $date2 .' 23:59:59';

            $requete->whereBetween('created_at',[$dateD, $dateF]);
        }

        $datas = $requete->get();
        //dd($datas[0]->items);
        if(count($datas)>0){
            $libpgrm = $datas[0]->programme->libelle;
            //dd($libpgrm);
            if($typexport){
                $this->typereturn = $typexport;
            }

            if($this->typereturn == 'pdf'){
                $html = "";

                //VIEW LIST BC
                $view= view('magasinier.etats.bs.pdf',compact('datas','date','date2'));
                $html .= $view->render();

                foreach($datas as $data){
                    //VIEW ITEM BC
                    $view= view('magasinier.etats.bs.pdf_item',compact('data'));
                    $html .= $view->render();
                }

                $allhtml = '<html>'.$html.'</html>';
                $pdf = \PDF::loadHTML($allhtml);
                $sheet = $pdf->setPaper('a4','portrait');
                return $sheet->download('etat_bon_sortir-'.$libpgrm.'-'.date("d-m-y").'.pdf');

                //$pdf = PDF::loadView('magasinier.etats.bs.pdf',compact('datas'));
                //return view('magasinier.etats.bs.pdf',compact('datas'));
                //return $pdf->download('etat_bon_sortir-'.$libpgrm.'-'.date("d-m-y").'.pdf');

            }else{
                try{
                    $date=new \DateTime('now');
                    $date = $date->format('d-m-Y');
                    $lib= "etat_bon_de_sortir-".$libpgrm."-".$date.".xlsx";

                    return Excel::download(new BonSortieManyExport($datas), $lib);

                }catch (\Exception $e){
                    return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                    //dd($e->getMessage());
                }
            }

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');

    }

    //ETATS FACTURATIONS
    public function etatsFactus()
    {
        $programme_id = $this->getIdProgram();
        $blade = $this->getBlade(Auth::user()->role_id);
        $fournisseurs = ProgrammeFournisseur::where('programme_id',$programme_id)->orderBy('nom','asc')->get();
        $soustraitants = ProgrammeSoustraitant::where('etat',2)->where('nivalid',2)->where('programme_id',$programme_id)->get();

        return view('_etats.facturation', compact('blade','fournisseurs','soustraitants'));
    }

    public function etatsFactuStore(Request $request){
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        // dd($request->all());
        Validator::make($request->all(), [
            'typexport' => ['required'],
            'typetat' => ['required'],
        ],[
            'typexport.required' => 'Le type d\'exportation est requis.',
            'typetat.required' => 'Le type d\'état est requis.',
        ]
        )->validate();

        $statu = $request->statu;
        // $date = $request->datedebut;
        // $date2 = $request->datefin;
        $typexport = $request->typexport;

        if($request->typetat == "fournisseur"){
            $requete = FactureFournisseur::where('programme_id',$idprogram)->with('user','fournisseur','boncommande');
            if($statu != 0){
                $requete->where('status',$statu);
            }

            if($request->fournisseur){
                $requete->where('fournisseur_id',$request->fournisseur);
            }

            $requete->orderBy('id','desc');
            $requete = $requete->get();

            //MEttre les datas par fournisseur
            $requete->map(function($item) use($idprogram){
                if($item->fournisseur){
                    $nbbc = BC::where('fournisseur_id',$item->fournisseur->id)->where('programme_id',$idprogram)->where('status',2)->get();
                    $coutglobal = $nbbc->sum(function ($bc) {
                        return $bc->full_montant;
                    });
                    //Montant total payé aux fournisseurs
                    $nbfact = FactureFournisseur::where('fournisseur_id',$item->fournisseur->id)->where('programme_id',$idprogram)->where('status',2)->get();
                    $mttpay = $nbfact->sum('montant');

                    //exonoration tva
                    $exo = BC::where('fournisseur_id',$item->fournisseur->id)->where('programme_id',$idprogram)->where('status',2)->whereNotNull('ibution_id')->get()->sum(function ($bc) {
                        return $bc->full_montant;
                    });

                    $item->fournisseur->nb_bc = count($nbbc);
                    $item->fournisseur->nb_facture = count($nbfact);
                    $item->fournisseur->coutglobal = $coutglobal;
                    $item->fournisseur->mttpay = $mttpay;
                    $item->fournisseur->reste = $coutglobal - $mttpay;
                    $item->fournisseur->exo = $exo;
                }
            });

        }else{
            $requete = Transaction::where('etat',2)->where('status',3)->where('programme_id',$idprogram)->with('lot','soustraitant','contrat','paiement');
            if($statu == 2){
                $requete->where('status',2);
            }

            if($request->soustraitant){
                $requete->where('soustraitant_id',$request->soustraitant);
            }
            $requete->orderBy('id','asc');
            $requete = $requete->get();
        }

        // if(isset($date) and isset($date2)){
        //     $dateD = $date .' 00:00:00';
        //     $dateF = $date2 .' 23:59:59';

        //     $requete->whereBetween('created_at',[$dateD, $dateF]);
        // }

        $datas = $requete;
        if(count($datas)==0){
            return redirect()->back()->with('error','Aucune donnée disponible.');
        }

        $datas->map(function($item) use ($request){
            $item->type_export = $request->typetat;
        });

        // return view('_etats.excell_facturation',compact('datas'));
        // dd($datas);

        if($typexport){
            $this->typereturn = $typexport;
        }

        if($this->typereturn == 'pdf'){
        }else{
            try{
                $date=new \DateTime('now');
                $date = $date->format('d-m-Y');
                $typexport = $request->typetat == "fournisseur" ? "ETAT FOURNISSEUR" : "ETAT SOUSTRAITANT";
                $lib= $typexport."-".$date.'.xlsx';
                $typmodel = $request->typetat;

                return Excel::download(new FacturationManyExport($datas,$typexport,$typmodel), $lib);

            }catch (\Exception $e){
                dd($e->getMessage());
                return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
            }
        }
    }

}
