<?php

namespace App\Http\Controllers\Serviceachat;

use App\BC;
use App\FactureFournisseur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProgrammeFournisseur;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FournisseursController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $fournisseurs = ProgrammeFournisseur::where('programme_id',$idprogram)->get();
        return view('service_achat.fournisseurs.index',compact('fournisseurs'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $new = new ProgrammeFournisseur();
            $new->programme_id = $idprogram;
            $new->nom = $request->nom;
            $new->contact = $request->contact;
            $new->adresse = $request->adresse;
            $new->email = $request->email;
            $new->delais_paiement = $request->delais_paiement;
            $new->regime = $request->regime;
            $new->credit = $request->credit;
            $new->save();

            return redirect()->back()->with('success','Le fournisseur a été ajouté');
        }

    }

    public function update(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'slug' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $new = ProgrammeFournisseur::where('id',$request->slug)->where('programme_id',$idprogram)->first();
            if($new){
                //$new->programme_id = $idprogram;
                $new->nom = $request->nom;
                $new->contact = $request->contact;
                $new->adresse = $request->adresse;
                $new->email = $request->email;
                $new->delais_paiement = $request->delais_paiement;
                $new->regime = $request->regime;
                $new->credit = $request->credit;
                $new->save();
            }


            return redirect()->back()->with('success','Le fournisseur a été mise a jour');
        }
    }

    public function del($slug)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $new = ProgrammeFournisseur::where('id',$slug)->where('programme_id',$idprogram)->first();
        if($new){
            $new->delete();
            return redirect()->back()->with('success','Le fournisseur a été supprimé');
        }
        return redirect()->back();
    }

    public function show($id){
        $id = decrypt($id);
        // dd($id);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $fournisseur = ProgrammeFournisseur::where('id',$id)->where('programme_id',$idprogram)->first();

        if(!$fournisseur){
            return redirect()->back()->with('error','Fournisseur non trouvé');
        }

        $blade = $this->getBlade(Auth::user()->role_id);

        //stats montant
        $coutglobal = BC::where('fournisseur_id',$fournisseur->id)->where('programme_id',$idprogram)->where('status',2)->get()->sum(function ($bc) {
            return $bc->full_montant;
        });
        //Montant total payé aux fournisseurs
        $mttpay = FactureFournisseur::where('fournisseur_id',$fournisseur->id)->where('programme_id',$idprogram)->where('status',2)->sum('montant');

        $stats = [
            'coutglobal' => $coutglobal,
            'mttpay' => $mttpay,
            'reste' => $coutglobal - $mttpay,
        ];

        $factures = FactureFournisseur::where('fournisseur_id',$fournisseur->id)->where('programme_id',$idprogram)->with('user','fournisseur','boncommande')->orderBy('id','desc')->get();

        return view('_everyone.fournisseurs_stats',compact('fournisseur','blade','stats','factures'));
    }
}
