<?php

namespace App\Http\Controllers\Serviceachat;

use App\Avancement;
use App\Bon_demande;
use App\Bon_demande_historique;
use App\Bon_demande_item;
use App\Commande;
use App\CommandeItem;
use App\Http\Controllers\BonController;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LotSoustraitant;
use App\Produit;
use App\Programme;
use App\ProgrammeCorpsetat;
use App\ProgrammeLot;
use App\ProgrammeProduit;
use App\ProgrammeSoustraitant;
use App\ProgrammeSoustraitantcontrat;
use App\ProgrammeSoustraitantcontratMontant;
use App\Stock;
use App\StockFournisseurItem;
use App\Transaction;
use App\Typeproduit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function home()
    {
        Session::forget(['program']);

        $user = User::where('id',Auth::id())->with('programmes')->first();
        if(count($user->programmes)==1){
            return redirect()->route('sa.itempgram',$user->programmes[0]->slug);
        }
        return view('service_achat.listpgram',compact('user'));
    }

    public function itempgram($slug){
        //dd($slug);
        $program = Programme::where('slug',$slug)->first();
        if(isset($program->id)){
            $check = ProgrammePivotUser::where('user_id',Auth::id())->where('programme_id',$program->id)->first();

            if($check){
                Session::put('program', $program->id);
                return redirect()->route('sa.index');
            }

            return redirect()->back()->with('error','Vous n\'avez pas doit à ce programme');
        }

        return redirect()->back();
    }


    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $stock = Stock::where('programme_id',$idprogram)->get();
        $stock->map(function ($item){
            $price = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->price = $price ? $price->prix * $item->qte : 0;
        });

        $entree = StockFournisseurItem::where('programme_id',$idprogram)->get();
        $entree->map(function ($item){
            $price = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->price = $price ? $price->prix * $item->qte : 0;
        });

        $bondemandeIds = Bon_demande::where('programme_id',$idprogram)->where('status',3)->pluck('id')->toArray();
        $sortie = Bon_demande_item::whereIn('bon_demande_id',$bondemandeIds)->get();
        $sortie->map(function ($item) use($idprogram) {
            $price = ProgrammeProduit::where('programme_id',$idprogram)->where('produit_id',$item->produit_id)->first();
            $item->price = $price ? $price->prix * $item->qte  : 0;
        });

        $top3cmd = StockFournisseurItem::select('programme_id','produit_id', DB::raw('sum(qte) as total'))->where('programme_id',$idprogram)->groupBy('produit_id')
                                        ->orderBy('total', 'desc')->limit(8)->with('produits')->has('produits')->get();
        $top3cmd->map(function ($item){
            $price = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            //$item->pu = $price ? $price->prix : 0;
            $item->price = $price ? $price->prix * $item->total : 0;
        });

        $mtlimit = Stock::where('programme_id',$idprogram)->where('qte','<=',10)->has('produits')->with(['produits'=>function($q){$q->with('type');}])->get();

        $statsEntree =[];
        $statsSortie =[];
        $statsPaie =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            $entreeMois = StockFournisseurItem::where('programme_id',$idprogram)->whereYear('updated_at',date('Y'))->whereMonth('updated_at',$month)->get();
            $entreeMois->map(function ($item){
                $price = ProgrammeProduit::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
                $item->price = $price ? $price->prix * $item->qte : 0;
            });

            $statsEntree[$month]['qte'] = $entreeMois->sum('qte');
            $statsEntree[$month]['price'] = $entreeMois->sum('price');


            $sortieMois = Bon_demande_item::whereIn('bon_demande_id',$bondemandeIds)->whereYear('updated_at',date('Y'))->whereMonth('updated_at',$month)->get();
            $sortieMois->map(function ($item) use($idprogram) {
                $price = ProgrammeProduit::where('programme_id',$idprogram)->where('produit_id',$item->produit_id)->first();
                $item->price = $price ? $price->prix * $item->qte  : 0;
            });
            $statsSortie[$month]['qte'] = $sortieMois->sum('qte');
            $statsSortie[$month]['price'] = $sortieMois->sum('price');

            //PAIEMENT SSTRAITANT
            $data= Avancement::whereYear('created_at',date('Y'))->whereMonth('created_at',$month)->where('programme_id',$idprogram)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
            $statsPaie[$month] = $data->coutpaye ?? 0;
        }

        //SOUSTRAITATNT STAST
        $actifs = ProgrammeCorpsetat::where('programme_id',$idprogram)->select('actif_id',DB::raw('round(SUM(prix)) as sprix'))->groupBy('actif_id')->get();
        $som = 0;
        $program = Programme::where('id',$idprogram)->first();
        if($program->type_rattachement == 1){
            foreach ($actifs as $actif){
                $lot = ProgrammeLot::where('actif_id',$actif->actif_id)->count();
                $som = $som + ($lot * $actif->sprix);
            }
        }else{
            $som = ProgrammeSoustraitantcontrat::where('programme_id',$idprogram)->select('montantctr',DB::raw('round(SUM(montantctr)) as sprix'))->first();
            $som = $som->sprix;
        }

        $coutglobal = $som;

        $idtranspay = Transaction::where('programme_id',$idprogram)->where('status',2)->pluck('id')->toArray();
        $payes = Avancement::where('programme_id',$idprogram)->whereIn('transac_id',$idtranspay)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
        $lists = Transaction::where('programme_id',$idprogram)->with('lot')->with('soustraitant')->get();


        return view('service_achat.index',compact('stock','entree','sortie','top3cmd','mtlimit','statsEntree','statsSortie','coutglobal','payes','lists','statsPaie'));
    }


    //PROFILE
    public function profil()
    {
        $this->getProgramExist();
        $user = User::where('id',Auth::id())->with('role')->with('programmes')->first();
        return view('service_achat.profile',compact('user'));
    }

    public function profilUpdate(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'name' =>'required',
            'contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->name = $request->name;
            $user->contact = $request->contact;
            $user->save();
            return redirect()->back()->with('success','✔ Votre profil a été modifié');
        }

    }

    public function profilUpdatePass(Request $request)
    {
        $this->getProgramExist();
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $oldpass = $request->motpass;
            $newpass = $request->newpass;
            $newpassconfirm = $request->confirmpass;
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->back()->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->back()->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->back()->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function profilUpdateAvatar(Request $request)
    {
        $this->getProgramExist();
        //d($request->all());
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        try{
            if($fileSize <= 4000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName ='assets/uploads/users/';
                $picture = Str::random(6).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->back()->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 4Mb');
            endif;

        }catch (\Exception $e){
            //dd($e->getMessage());
        }

    }

    //ETATS
    public function etats(){
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        $listlots = ProgrammeLot::where('programme_id',$idprogram)->with('actifs')->orderByRaw('ABS(lot)', 'asc')->get();
        $listlots->map(function($item) use($idprogram){
            if($item->programme->type_rattachement == 1){
                $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            }else{
                //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
                $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$item->id)->has('straitantcontrat')->sum('montant');
            }
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });

        //dd($blocks,$tranches,$soustraitants);
        return view('service_achat.etats.index',compact('blocks','tranches','soustraitants','listlots'));
    }

    public function etatsItem($slug)
    {
        //dd($slug);
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        $listlot = ProgrammeLot::where('programme_id',$idprogram)->where('id',$slug)->with('actifs')->first();
        //$montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$listlot->actif_id)->sum('prix');
        if($listlot->programme->type_rattachement == 1){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$listlot->actif_id)->sum('prix');
        }else{
            //Selectionnez la somme des montants des contrats de chaque lot ou le contrat existe
            $montantLot = ProgrammeSoustraitantcontratMontant::where('programme_id',$idprogram)->where('lot_id',$listlot->id)->has('straitantcontrat')->sum('montant');
        }

        $idtranspay = Transaction::where('programme_id',$idprogram)->where('status',2)->pluck('id')->toArray();
        $montbuy = Avancement::where('lot_id',$listlot->id)->whereIn('transac_id',$idtranspay)->sum('cout');
        //dump($montantLot,$montbuy);
        $stats =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            $data= Avancement::where('lot_id',$listlot->id)
            ->whereYear('created_at',date('Y'))
            ->whereMonth('created_at',$month)
            ->where('programme_id',$idprogram)
            ->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
            $stats[$month] =$data->coutpaye;
        }
        //dd($listlot);
        //
        return view('service_achat.etats.show',compact('listlot','montantLot','montbuy','stats'));
    }

    public function etatsSearch(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        /*if(empty($request->tranche) and empty($request->bloc) and empty($request->soustraitant)){
            return redirect()->back();
        }*/
        $pg = ProgrammeLot::where('programme_id',$idprogram);

        if(isset($request->soustraitant)){

            $pgst = LotSoustraitant::where('soustraitant_id',$request->soustraitant)->pluck('lot_id')->toArray();
            $pg = ProgrammeLot::whereIn('id',$pgst)->with('actifs');

            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }

        }else{
            if(isset($request->tranche)){
                $pg->where('tranche',$request->tranche);
            }

            if(isset($request->bloc)){
                $pg->where('bloc',$request->bloc);
            }
        }

        $listlots = $pg->get();
        $listlots->map(function($item) use($idprogram){
            $montantLot = ProgrammeCorpsetat::where('programme_id',$idprogram)->where('actif_id',$item->actif_id)->sum('prix');
            $montbuy = Avancement::where('lot_id',$item->id)->sum('cout');

            $item->montantLot = $montantLot;
            $item->montbuy = $montbuy;
        });
        //dd($listlots);
        $blocks = ProgrammeLot::where('programme_id',$idprogram)->select('bloc')->groupBy('bloc')->get();
        $tranches = ProgrammeLot::where('programme_id',$idprogram)->select('tranche')->groupBy('tranche')->get();
        $soustraitants = ProgrammeSoustraitant::where('programme_id',$idprogram)->get();

        return view('service_achat.etats.index',compact('blocks','tranches','soustraitants','listlots'));
    }

}
