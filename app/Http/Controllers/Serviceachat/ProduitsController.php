<?php

namespace App\Http\Controllers\Serviceachat;

use App\Exports\ProduitSAExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\ProduitSAImport;
use App\Produit;
use App\ProgrammeProduit;
use App\Stock;
use App\Typeproduit;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ProduitsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('chefchantier.cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function index()
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        $produits = Produit::with('type')->orderBy('libelle','asc')->get();
        $listproduits = ProgrammeProduit::where('programme_id',$idprogram)->with(['produit'=>function($q){$q->with('type');}])->get();
        $listproduits->map(function($item){
            $data = Stock::where('programme_id',$item->programme_id)->where('produit_id',$item->produit_id)->first();
            $item->qte = $data ? $data->qte : 0;
        });
        $types = Typeproduit::get();
        return view('service_achat.produits.index',compact('listproduits','produits','types'));
    }

    public function model()
    {
        $this->getProgramExist();
        $programId = $this->getIdProgram();

        try{
            //EXPORTATION
            //$customerArray[] = array('NUM','CODE','PRODUIT','TYPE','PRIX');
            $produits = Produit::with('type')->has('type')->orderBy('libelle','asc')->get();
            $produits->map(function($item) use ($programId){
                $check2 = ProgrammeProduit::where('programme_id',$programId)->where('produit_id',$item->id)->first();
                $item->nprix = $check2 ? $check2->prix : '';
            });
            // foreach($produits as $k=>$export){
            //     $customerArray[]= array(
            //         'NUM'=>$k+1,
            //         'CODE'=>$export->code,
            //         'PRODUIT'=> $export->libelle,
            //         'TYPE'=> $export->type->libelle,
            //         'PRIX'=> ''
            //     );
            // }

            // Excel::create("PRODUIT-LISTE-".$date,function($excel) use($customerArray,$date){
            //     $excel->setTitle("PRODUIT-LISTE-".$date);
            //     $excel->sheet("PRODUIT-LISTE-".$date,function ($sheet) use ($customerArray){
            //         $sheet->fromArray($customerArray,null,'A1',false,false);
            //         $sheet->getComment('B1')->getText()->createTextRun('Merci de ne pas modifier cette colonne.');
            //         $sheet->getComment('E1')->getText()->createTextRun('Veuillez renseigner le prix du produit ici.');
            //     });

            // })->download('xlsx');
            $date=new \DateTime('now');
            $date = $date->format('d-m-Y');

            $date=new \DateTime('now');
            $date = $date->format('d-m-Y');
            $lib= "PRODUIT-LISTE-".$date.'.xlsx';
            return Excel::download(new ProduitSAExport($produits), $lib);

            return redirect()->route('sa.gproduits')->with('success', 'Votre fichier a bien été téléchager.');

        }catch(\Exception $e){
            return redirect()->route('sa.gproduits')->with('error', 'une erreur s\'est produite lors de l\'envoi du fichier');
        }

    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();

        Validator::make($request->all(), [
            'typdt' => ['required'],
            'prix' => ['required'],
        ])->validate();

        if($request->typdt == "materiel"){
            Validator::make($request->all(), [
                'codemateriel' => ['required'],
                'pdtmateriel' => ['required'],
                'typemateriel' => ['required'],
            ])->validate();
        }else{
            Validator::make($request->all(), [
                'pdt' => ['required'],
            ])->validate();
        }

        //dd($request->all());

        if($request->typdt == "materiel"){
            $code = $request->codemateriel;
            $checkmt = Produit::where('code',$code)->where('type_materiel','materiel')->first();
            if(!isset($checkmt->id)){
                $checkmt = new Produit();
                $checkmt->code = $request->codemateriel;
                $checkmt->libelle = $request->pdtmateriel;
                $checkmt->type_id = $request->typemateriel;
                $checkmt->slug = Str::slug($request->pdtmateriel);
                $checkmt->seuil = 10;
                $checkmt->type_materiel = 'materiel';
                $checkmt->user_id = auth()->user()->id;
                $checkmt->save();

                $idpdt = $checkmt->id;
            }else{
                if($checkmt){
                    $idpdt = $checkmt->id;
                }else{
                    return redirect()->back()->with('error','La matière est introuvable.');
                }
            }
        }else{
            $idpdt = $request->pdt;
            $check = Produit::where('id',$idpdt)->first();
            if(!isset($check->id)){
                return redirect()->back()->with('error','La matière est introuvable.');
            }
        }


        $check2 = ProgrammeProduit::where('programme_id',$idprogram)->where('produit_id',$idpdt)->first();
        if(isset($check2->id)){
            return redirect()->back()->with('error','La matière existe déjà.');
        }

        $type = new ProgrammeProduit();
        $type->produit_id = $idpdt;
        $type->programme_id = $idprogram;
        $type->prix = $request->prix;
        $type->save();

        return redirect()->route('sa.gproduits')->with('success','La matière a bien été ajouté');

    }

    public function update(Request $request)
    {
        $this->getProgramExist();
        $idprogram = $this->getIdProgram();
        //dd($request->all());
        Validator::make($request->all(), [
            // 'pdt' => ['required'],
            'prix' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $slug = $request->slug;
        $check = ProgrammeProduit::where('id',$slug)->with('produit')->first();

        $checktwo = ProgrammeProduit::where('id',"<>",$slug)->where('produit_id',$request->pdt)->where('programme_id',$idprogram)->first();
        if(isset($checktwo->id)){
            return redirect()->back()->with('error','La matière exite déjà');
        }

        if(isset($check->id)){
            $idpdt = $check->produit_id;
            $check->produit_id = $idpdt;
            $check->prix = $request->prix;
        }
        $check->save();

        if($check->produit->type_materiel == 'materiel'){
            //dd($check->produit->type_materiel);
            //$check->produit->code = $request->codemateriel;
            $check->produit->libelle = $request->pdtmateriel;
            $check->produit->type_id = $request->typemateriel;
            $check->produit->slug = Str::slug($request->pdtmateriel);
            $check->produit->save();
        }

        return redirect()->route('sa.gproduits')->with('success','La matière a bien été modifié');
    }

    public function import(Request $request)
    {
        $this->getProgramExist();
        $programId = $this->getIdProgram();
        //dd($request->all());

        Validator::make($request->all(), [
            'ufile' => ['required','mimes:xlsx,xls,csv'],
        ])->validate();

        if(isset($programId)){
            //$results = Excel::selectSheetsByIndex(0)->load($request->ufile)->toArray();
            $file = $request->file('ufile');
            //dd($results);

            $filedata = Excel::toCollection(new ProduitSAImport, $request->file('ufile'));
            $filedata = $filedata[0]->toArray();
            if(count($filedata) > 0) {
                foreach($filedata as $key => $row) {
                    if($key == 0 ) {
                        //dd($row);
                        if($row[0] != 'NUM' || $row[1] != "CODE" || $row[2] != "PRODUIT" || $row[3] != "TYPE" || $row[4] != "PRIX" ) {
                            return redirect()->route('sa.gproduits')->with('error' ,"le format de fichier n'est pas conforme au modèle de base.");
                        }
                    }else{
                        // $check = Produit::where('code',$row[1])->first();
                        $check = Produit::where('id',$row[0])->first();
                        if(isset($check->id)){
                            $idpdt = $check->id;
                            $check2 = ProgrammeProduit::where('programme_id',$programId)->where('produit_id',$idpdt)->first();

                            if(!isset($check2->id)){
                                $type = new ProgrammeProduit();
                                $type->produit_id = $idpdt;
                                $type->programme_id = $programId;
                                $type->prix = $row[4]? $row[4] : 0;
                                $type->save();
                            }else{
                                $check2->prix = $row[4]? $row[4] : 0;
                                $check2->save();
                            }

                            //update produit si le code existe
                            if(Produit::where('code',$row[1])->first() != null){
                                $slug = Str::slug($row[2]);
                                //si le slug existe déjà on l'enregistre pas
                                if($slug != $check->slug){
                                    $checkSlug = Produit::where('slug',$slug)->first();
                                    if(!$checkSlug){
                                        $check->libelle = $row[2];
                                        $check->slug = $slug;
                                    }
                                }
                                $check->code = $row[1];
                                $check->save();
                            }
                        }
                    }
                }

                return redirect()->route('sa.gproduits')->with('success' ,"Le fichier a bien été importé.");

            }else {
                return redirect()->route('sa.gproduits')->with('error' ,"Veuillez importer le fichier d'origine.");
            }

            // if(count($results)>0){
            //     foreach($results as $value=>$key ){
            //         $check = Produit::where('code',$key['code'])->first();

            //         if(isset($check->id)){
            //             $idpdt = $check->id;
            //             $check2 = ProgrammeProduit::where('programme_id',$programId)->where('produit_id',$idpdt)->first();

            //             if(!isset($check2->id)){
            //                 $type = new ProgrammeProduit();
            //                 $type->produit_id = $idpdt;
            //                 $type->programme_id = $programId;
            //                 $type->prix = $key['prix'];
            //                 $type->save();
            //             }

            //         }
            //     }
            // }
        }

    }

    //TYPES //TYPES //TYPES //TYPES

    public function types()
    {
        $types = Typeproduit::get();
        return view('service_achat.produits.type',compact('types'));
    }

    public function gettype($id)
    {
        $type = Produit::where('slug',$id)->has('type')->with('type')->first();
        return $type ? $type->type->libelle : '-' ;
    }

    public function storeType(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
        ])->validate();

        $slug = Str::slug($request->nom);
        $check = Typeproduit::where('slug',$slug)->first();
        if(isset($check->id)){
            return redirect()->back()->with('error','Le type existe déjà');
        }
        $type = new Typeproduit();
        $type->slug = $slug;
        $type->libelle = $request->nom;
        $type->save();

        return redirect()->route('sa.gproduits.types')->with('success','Le type a bien été ajouté');
    }

    public function updateType(Request $request)
    {
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $slug = Str::slug($request->libelle);
        $check = Typeproduit::where('slug',$slug)->first();
        if(isset($check->id)){
            return redirect()->back()->with('error','Le type existe déjà');
        }
        $type = Typeproduit::where('slug',$request->slug)->first();
        $type->slug = $slug;
        $type->libelle = $request->libelle;
        $type->save();

        return redirect()->route('sa.gproduits.types')->with('success','Le type a bien été modifié');
    }
}
