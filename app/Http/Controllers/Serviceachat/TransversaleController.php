<?php

namespace App\Http\Controllers\Serviceachat;

use App\BC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ibution;
use App\Programme;
use App\ProgrammePivotUser;
use App\User;
use Illuminate\Support\Facades\Session;
use PDF;
use Illuminate\Support\Str;


class TransversaleController extends Controller
{
    public function getProgramExist()
    {
        $dataprogram = Session::get('program');
        if($dataprogram == null){
            return redirect()->route('cpprogram')->send();
        }
    }

    public function getIdProgram()
    {
        if(Session::get('program')){
            return Session::get('program') ?: null ;
        }
        return null ;
    }

    public function make($id) {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        $id = decrypt($id);
        $bc = BC::find($id);
        if(!$bc) {
            return redirect()->back()->with('error', 'Bon de commande introuvable');
        }

        return view('service_achat.transversale.make', compact('bc'));
    }

    public function show(Request $request){
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();

        // dd($request->all());

        if(!$request->bc && !$request->typepjt) {
            return "<div class='alert alert-warning alert-dismissible'>Veuillez selectionner un bon de commande et un type de projet</div>";
        }

        $id = decrypt($request->bc);
        $data = BC::with('demande','fournisseur','user','items')->find($id);
        if(!$data) {
            return "<div class='alert alert-warning alert-dismissible'>Bon de commande introuvable</div>";
        }

        $programRea = Programme::where('id',$request->typepjt)->first();
        if(!$programRea) {
            return "<div class='alert alert-warning alert-dismissible'>Type de projet introuvable</div>";
        }

        $data->programm_rea = $programRea;

        $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
        $sa = User::where('role_id',6)->whereIn('id',$pguser)->first();
        $daf = User::where('role_id',7)->whereIn('id',$pguser)->first();

        return view('service_achat.transversale.show', compact('data','sa','daf'));
    }

    public function store(Request $request){
        // dd($request->all());
        $this->getProgramExist();

        $request->validate([
            'slug' => 'required',
            'typepjt' => 'required',
        ]);

        $id = decrypt($request->slug);
        $data = BC::with('demande','fournisseur','user','items')->find($id);
        if(!$data) {
            return redirect()->back()->with('error', 'Bon de commande introuvable');
        }

        $programRea = Programme::where('id',$request->typepjt)->first();
        if(!$programRea) {
            return redirect()->back()->with('error', 'Type de projet introuvable');
        }

        //Verifier si ce bon n'a pas deja été enregistré
        $check = Ibution::where('bc_id',$data->id)->first();
        if($check) {
            return redirect()->back()->with('error', 'Ce bon de commande a déjà été réatribué');
        }

        //Save Ibution
        $ibution = new Ibution();
        $ibution->bc_id = $data->id;
        $ibution->programme_first_id = $data->programme_id;
        $ibution->programme_last_id = $programRea->id;
        $ibution->save();

        //Save BC
        $data->ibution_id = $ibution->id;
        $data->save();

        //Notification
        $urldonwload = route('sa.transversale.pdf',encrypt($ibution->bc_id));
        $messag = "Le bon de commande N° <span class='text-primary'>".$data->ref."</span> a été réatribué  <a class='btn btn-primary btn-sm' target='_blank' href='".$urldonwload."'><i class='fa fa-file-pdf'></i> Télécharger</a>";

        return redirect()->route('sabc.valider')->with('success', $messag);
    }


    public function showPdf($slug)
    {
        $this->getProgramExist();
        $programme_id = $this->getIdProgram();
        $id = decrypt($slug);
        $data = BC::where('programme_id',$programme_id)->where('id',$id)->with('demande','fournisseur','user','items')->first();

        //Ibution
        $ibution = Ibution::where('bc_id',$data->id)->with('programmeLast')->first();
        if(!$ibution) {
            return redirect()->back()->with('error', 'Bon de commande introuvable');
        }
        $data->programm_rea = $ibution->programmeLast;

        $pguser = ProgrammePivotUser::where('programme_id',$programme_id)->pluck('user_id');
        $sa = User::where('role_id',6)->whereIn('id',$pguser)->first();
        $daf = User::where('role_id',7)->whereIn('id',$pguser)->first();

        $pdf = PDF::loadView('service_achat.transversale.bc_download',compact('data','sa','daf'));
        // return view('service_achat.transversale.bc_download',compact('data','sa','daf'));
        $id = $data->ref;
        $fournname = Str::slug($data->fournisseur ? $data->fournisseur->nom:'na');
        $prograname = $ibution->programmeLast ? $ibution->programmeLast->slug : 'na';
        $libel = $id.'_'.$fournname.'_'.$prograname;
        $libel = Str::upper($libel);

        return $pdf->stream($libel.'-'.date("d-m-y").'.pdf');
        return $pdf->download($libel.'.pdf');

    }

}
