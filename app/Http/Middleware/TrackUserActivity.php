<?php

namespace App\Http\Middleware;

use App\UserDailyActivity;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class TrackUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // On continue si l'utilisateur n'est pas connecté
        if (!Auth::check()) {
            return $next($request);
        }

        $user = Auth::user();
        $today = Carbon::today();
         // Mise à jour ou création de l'activité du jour
        UserDailyActivity::updateOrCreate(
            [
                'user_id' => $user->id,
                'activity_date' => $today,
            ],
            [
                'is_active' => true,
                'last_activity_at' => now(),
            ]
        );

        return $next($request);

    }
}
