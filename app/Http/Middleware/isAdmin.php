<?php

namespace App\Http\Middleware;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->user()->role_id);
        if( $request->user()->role_id <> 1) {
            return redirect()->route('forbiden');
        }

        return $next($request);
    }
}
