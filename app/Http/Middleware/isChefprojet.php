<?php

namespace App\Http\Middleware;

use Closure;

class isChefprojet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( in_array($request->user()->role_id ,[1,2,4])) {
            return redirect()->route('forbiden');
        }

        return $next($request);
    }
}
