<?php

namespace App\Http\Middleware;

use Closure;

class isDaf
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!in_array($request->user()->role_id,[7,8,9])) {
            return redirect()->route('forbiden');
        }

        return $next($request);
    }
}
