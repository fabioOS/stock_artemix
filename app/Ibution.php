<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ibution extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function programmeFirst()
    {
        return $this->belongsTo('App\Programme','programme_first_id');
    }

    public function programmeLast()
    {
        return $this->belongsTo('App\Programme','programme_last_id');
    }

    public function boncommande()
    {
        return $this->belongsTo('App\BC','bc_id');
    }
}
