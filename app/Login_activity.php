<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login_activity extends Model
{
    protected $fillable = [
        'user_id','user_name','login_at','logout_at','ip_address','user_agent',
    ];

    /*
     * Relationship
     */
    public function user() {
        return $this->belongsTo('App\User');
    }
}
