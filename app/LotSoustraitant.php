<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LotSoustraitant extends Model
{
    use SoftDeletes;
    protected $table="lots_sous_traitants";

    protected $fillable = [
        'lot_id',
        'soustraitant_id'
    ];


    public function lot()
    {
        return $this->belongsTo('App\ProgrammeLot','lot_id');
    }

    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }

    public function straitantcontrat()
    {
        return $this->belongsTo('App\ProgrammeSoustraitantcontrat','soustraitantcontrat_id');
    }
}
