<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LotSoustraitantCorpsetat extends Model
{
    use SoftDeletes;
    protected $table="lotsoustraitans_corpsetats";

    protected $fillable = [
        'lots_sous_traitants_id',
        'lot_id',
        'actif_id',
        'soustraitant_id',
        'corpsetats_id'
    ];


    public function corpslots()
    {
        return $this->belongsTo('App\ProgrammeCorpsetat','corpsetats_id');
    }

    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }
}
