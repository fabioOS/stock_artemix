<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paiement extends Model
{
    protected $table="programmes_paiements";

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function transaction (){
        return $this->belongsTo(Transaction::class,'transac_id');
    }

    public function facture (){
        return $this->belongsTo(FactureFournisseur::class,'transac_id');
    }

    public function programme(){
        return $this->belongsTo(Programme::class,'programme_id');
    }
}
