<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'libelle',
        'seuil',
        'type_id'
    ];

    /*
    * Relationship
    */
    public function type()
    {
        return $this->belongsTo('App\Typeproduit','type_id');
    }
    public function stocks(){
        return $this->hasMany('App\Stock');
    }

    public function stocksReference(){
        return $this->hasMany('App\StockFournisseurItem','produit_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
