<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programme extends Model
{
    use SoftDeletes;

    public function users()
    {
        return $this->belongsToMany('App\User','programme_pivot_user', 'programme_id', 'user_id');
    }

    public function actifs()
    {
        return $this->hasMany('App\ProgrammeActif','programme_id');
    }

    public function lots()
    {
        return $this->hasMany('App\ProgrammeLot','programme_id');
    }

    public function sousprogramme()
    {
        return $this->hasMany('App\ProgrammeSous','programme_id');
    }

    public function ligneBudgetaires()
    {
        return $this->hasMany('App\ProgrammeLigneBudgetaire','programme_id')->orderBy('id','asc');
    }
}
