<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeActif extends Model
{
    use SoftDeletes;
    protected $table="programmes_actifs";

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    //Lots
    public function lots()
    {
        return $this->hasMany('App\ProgrammeLot','actif_id');
    }

    //corps etat
    public function corpsetat()
    {
        return $this->hasMany('App\ProgrammeCorpsetat','actif_id');
    }
}
