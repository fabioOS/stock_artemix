<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeCorpsetat extends Model
{
    use SoftDeletes;
    protected $table="programmes_corpsetats";

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function lignebudget() {
        return $this->belongsTo('App\ProgrammeLigneBudgetaire','ligne_budget_id');
    }
}
