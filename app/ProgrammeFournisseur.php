<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeFournisseur extends Model
{
    use SoftDeletes;
    protected $table="programmes_fournisseurs";
}
