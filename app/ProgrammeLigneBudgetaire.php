<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeLigneBudgetaire extends Model
{
    use SoftDeletes;
    protected $table = 'programme_ligne_budgetaires';

    public function programme(){
        return $this->belongsTo('App\Programme');
    }

    public function corpsEtat(){
        return $this->belongsTo('App\CorpsEtat','corps_etat_id');
    }

    public function commandes() {
        return $this->hasMany('App\Commande','ligne_id')->where('etat',2)->where('nivalid',2);
    }
}
