<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeLot extends Model
{
    use SoftDeletes;
    protected $table="programmes_lots";

    public function actifs()
    {
        return $this->belongsTo('App\ProgrammeActif','actif_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }
}
