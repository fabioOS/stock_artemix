<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammePivotUser extends Model
{
    protected $table="programme_pivot_user";
}
