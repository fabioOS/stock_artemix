<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammeProduit extends Model
{
    protected $table="programme_produit";

    public function produit()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }
}
