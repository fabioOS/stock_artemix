<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeSous extends Model
{
    use SoftDeletes;

    protected $table = 'programme_sousprogramme';

    public function programme()
    {
        return $this->belongsTo('App\Programme');
    }
}
