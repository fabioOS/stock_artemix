<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammeSouscorpsetat extends Model
{
    protected $table="programmes_sous_corpsetats";

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function actif()
    {
        return $this->belongsTo('App\ProgrammeActif','actif_id');
    }


    public function corpsetat()
    {
        return $this->belongsTo('App\ProgrammeCorpsetat','corpsetat_id');
    }

    public function matieres()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }
}
