<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammeSouscorpsetatLot extends Model
{
    protected $table="programmes_sous_corpsetats_lots";

    public function libelles()
    {
        return $this->belongsTo('App\ProgrammeSouscorpsetatLibelle','corpsetatlibel_id');
    }

    public function matieres()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function actif()
    {
        return $this->belongsTo('App\ProgrammeActif','actif_id');
    }

    public function corpsetat()
    {
        return $this->belongsTo('App\ProgrammeCorpsetat','corpsetat_id');
    }
}
