<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeSoustraitant extends Model
{
    use SoftDeletes;
    protected $table="programmes_sous_traitant";

    protected $fillable = [
        'programme_id',
        'nom',
        'contact',
        'contrat',
        'deleted_at'
    ];

    public function lots()
    {
        return $this->hasMany('App\LotSoustraitant','soustraitant_id');
    }

    public function contrats()
    {
        return $this->hasMany('App\ProgrammeSoustraitantcontrat','soustraitant_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme');
    }
}
