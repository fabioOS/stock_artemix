<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeSoustraitantcontrat extends Model
{
    use SoftDeletes;
    protected $table="programmes_sous_traitant_contrat";

    public function straitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }

    public function lots()
    {
        return $this->hasMany('App\LotSoustraitant','soustraitantcontrat_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }
}
