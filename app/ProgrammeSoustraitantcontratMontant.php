<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgrammeSoustraitantcontratMontant extends Model
{
    use SoftDeletes;
    
    protected $table = 'programmes_sous_traitant_contrat_montants';

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function lot()
    {
        return $this->belongsTo('App\ProgrammeLot','lot_id');
    }

    public function straitantcontrat()
    {
        return $this->belongsTo('App\ProgrammeSoustraitantcontrat','soustraitantcontrat_id');
    }

    public function straitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }

    public function corpslots()
    {
        return $this->belongsTo('App\ProgrammeCorpsetat','corpsetats_id');
    }

}
