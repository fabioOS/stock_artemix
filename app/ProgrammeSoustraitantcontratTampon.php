<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammeSoustraitantcontratTampon extends Model
{
    protected $table="programmes_sous_traitant_contrat_tampons";

    public function straitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }
}
