<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'produit_id',
            'programme_id',
            'qte'
        ];

        /*
        * Relationship
        */
        public function produits() {
            return $this->belongsTo('App\Produit','produit_id');
        }

        public function programme(){
            return $this->belongsTo('App\Programme','programme_id');
        }
}
