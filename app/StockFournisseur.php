<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockFournisseur extends Model
{
    protected $table="stocks_fournisseurs";

    protected $fillable = [
        'programme_id',
        'fournisseur_id',
        'ref',
        'file'
    ];


    public function fournisseur()
    {
        return $this->belongsTo('App\ProgrammeFournisseur','fournisseur_id');
    }

    public function items()
    {
        return $this->hasMany('App\StockFournisseurItem','stock_fournisseur_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme','programme_id');
    }

    public function bc()
    {
        return $this->belongsTo('App\BC','bc_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
