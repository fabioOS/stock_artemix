<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockFournisseurItem extends Model
{
    protected $table="stocks_fournisseurs_items";

    protected $fillable = [
        'stock_fournisseur_id',
        'produit_id',
        'qte'
    ];

    public function produits()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }

}
