<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table="programmes_transactions";

    public function lot()
    {
        return $this->belongsTo('App\ProgrammeLot','lot_id');
    }

    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }

    public function contrat()
    {
        return $this->belongsTo('App\ProgrammeSoustraitantcontrat','soustraitantcontrat_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function paiement()
    {
        return $this->belongsTo('App\Paiement','transac_id');
    }

    public function prelevement(){
        return $this->hasMany('App\TransactionPrelevement','transaction_id');
    }

    public function getLibelleStatut(){
        $statut = $this->status;
        if($statut == 1){
            return 'En attente de validation';
        }elseif($statut == 2){
            return 'Payé';
        }elseif($statut == 3){
            return 'En attente de paiement';
        }
    }

}
