<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionPrelevement extends Model
{
    protected $table="programmes_transactions_prelevements";

    public function transaction()
    {
        return $this->belongsTo('App\Transaction','transaction_id');
    }

    public function typePrelevement()
    {
        return $this->belongsTo('App\TypePrelevement','type_prelevement_id');
    }
}
