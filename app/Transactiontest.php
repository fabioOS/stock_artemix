<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactiontest extends Model
{
    protected $table="programmes_transactions_test";

    public function lot()
    {
        return $this->belongsTo('App\ProgrammeLot','lot_id');
    }

    public function soustraitant()
    {
        return $this->belongsTo('App\ProgrammeSoustraitant','soustraitant_id');
    }

    public function contrat()
    {
        return $this->belongsTo('App\ProgrammeSoustraitantcontrat','soustraitantcontrat_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

}
