<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserDailyActivity extends Model
{
    protected $fillable = [
        'user_id',
        'activity_date',
        'is_active',
        'last_activity_at'
    ];

    protected $casts = [
        'activity_date' => 'date',
        'is_active' => 'boolean',
        'last_activity_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
