// disabled checkbox before dom's ready
$('input[type="checkbox"]').attr('disabled','disabled');
$(document).ready(function() {
    // init checkbox
    $('.checkAll, .checkItem').prop("checked", false);
    //
    $('#btn-show-import-modal').removeClass('disabled');
    $('.select2').select2();
    $(".rating-css").barrating({theme:"css-stars",showSelectedRating:!1});
    //datatable
    $('.cb-datatable').DataTable({
        paging: false,
        select: true,
        "ordering": true,
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }],
        'pageLength':20
    });
    // reset form-updater input value
    $('.btn-editer').each(function(index, element) {
        $(this).click(function() {
            $('.form-editer:eq('+index+') .current-val').each(function() {
                Nm = $(this).attr('name');
                Old = $('.old_'+Nm+':eq('+index+') ');
                if( $(this).val() != Old.val() )
                    $(this).val(Old.val());
            });
        });
    });


    // Upload via Ajax..
    //$('#importfile').val('');
    $("#form-import-doc").on('submit', function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            type: 'POST',
            url: $("#form-import-doc").attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData:false,
            beforeSend: function(){
                $("#form-import-doc input, #form-import-doc button, #btn-reseter").attr('disabled','disabled');
                //$(".progress-bar").width('0%').css('padding', '10px 20px;');
                $('#uploadStatus').html($('#bloc-img').html());
            },
            error:function(xhr, ajaxOptions, thrownError){
                if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                    document.location.reload(true);
                }
                else {
                    alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
                }
                $('#uploadStatus').html('<h5 style="color:#EA4335;">Erreur d\'importation, veuillez réessayer.</h5>');
            },
            success: function(resp){
                //resp = JSON.parse(resp);
                $("#form-import-doc input, #form-import-doc button, #btn-reseter").removeAttr('disabled');
                $('#importfile').val('');
                $('#uploadStatus').html(resp);
                $('#btn-reseter').html("Fermer");
                //ajax to reload corbeille bloc
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: $("#reload-url").val(),
                    data: '',
                    cache: false,
                    contentType: false,
                    processData:false,
                    beforeSend: function(){
                        $('#bloc-corbeille').fadeTo(10,0.3);
                    },
                    error:function(xhr, ajaxOptions, thrownError){
                        if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                            document.location.reload(true);
                        }
                        else {
                            alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
                        }
                        $('#bloc-corbeille').fadeTo(10,1);
                    },
                    success: function(data){
                        $('#bloc-corbeille').html(data).fadeTo(100,1);
                    }
                });
            }
        });
    });


    /**
        Check box  management
    */
    $('#checkAll').click(function() {
        if($(this).is(':checked')) {
            $('.checkItem').prop("checked", true);
            $('.tr').addClass('item-checked');
        }
        else{
            $('.checkItem').prop("checked", false);
            $('.tr').removeClass('item-checked');
        }
    });

    $('.checkItem-left').each(function(index, element) {
        $(this).click(function() {
            $('.checkAll').prop("checked", false);
            //
            if($(this).is(':checked')) {
                $(this).parent('div').parent('td').parent('tr').addClass('item-checked');
            }
            else{
                $(this).parent('div').parent('td').parent('tr').removeClass('item-checked');
            }
        });
    });
    //
    $('.checkItem, .checkAll').click(function() {
        if($('.checkItem:checked').length > 0) {
            $('#btnAttr').fadeIn();
        }
        else{
            $('#btnAttr').fadeOut();
        }
    });
    //endof check box




    /**
        Attribution management
    */

    /**
    * dossier
    */
    $('#Reset-Attr').click(function() {
        $('#Bloc-Attr').html('');
    });
    //
    $('#btnAttr').click(function() {
        //ajax to reload corbeille bloc
        var formData = new FormData($('#Form-Attr')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: $('#Form-Attr').attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData:false,
            beforeSend: function(){
                $('#Bloc-Attr').html('<div class="text-center">'+$('#bloc-img-2').html()+'</div>');
            },
            error:function(xhr, ajaxOptions, thrownError){
                if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                    document.location.reload(true);
                }
                else {
                    alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
                }
            },
            success: function(data){
                $('#Bloc-Attr').html(data);
            }
        });
        //
    });


    // initiate checkbox
    $('input[type="checkbox"]').removeAttr('disabled');

});
