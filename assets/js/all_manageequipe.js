
$(function () {

    /**
     * GESTION EQUIPE
     */
     $('#form-add-gc #prestataire_id').val('');
     $('#form-add-gc #prestataire_id').change(function() {
         $('.fird-role').parent('div').hide();
         $('#form-add-gc #liste_collabo').html('').val('');
         $('#form-add-gc #bloc-btn-submit:visible, #form-add-gc #bloc-collabo').hide(10);
         $('#form-add-gc #btn-add-collabo').removeClass('disabled');
         if($(this).val() != '') {
             URL = $(this).attr('rel');
             presta_id = $(this);
             $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
             });
             $.ajax({
                 type: 'GET',
                 url: URL,
                 data: 'prestataire_id='+presta_id.val(),
                 cache: false,
                 contentType: false,
                 processData:false,
                 beforeSend: function(){
                     $(".field").attr('disabled', 'disabled');
                     $("#form-add-gc #technicien_id").fadeTo(10,0.5);
                 },
                 error:function(xhr, ajaxOptions, thrownError){
                    if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                        document.location.reload(true);
                    }
                    else {
                        alert(xhr.status + ' : ' + thrownError  + ' : ' + URL+ ' : ' + presta_id.val());
                    }
                     $('#form-add-gc #technicien_id').parent('div').hide();
                     $('.field').removeAttr('disabled');
                 },
                 success: function(resp){
                     $('#form-add-gc #technicien_id').parent('div').fadeIn();
                     $('#form-add-gc #technicien_id').html(resp).fadeTo(10,1);
                     $('.field').removeAttr('disabled');
                 }
             });
         }
         else {
             $('.second-role, .fird-role').parent('div').hide();
             $('.second-role').val('');
             $('#form-add-gc #bloc-btn-submit:visible').hide(10);
         }
     });

     $('#form-add-gc #technicien_id').change(function() {
         $('#form-add-gc #liste_collabo').html('').val('');
         $('#form-add-gc #bloc-collabo').hide(10);
         $('#form-add-gc #btn-add-collabo').removeClass('disabled');
         if($(this).val() != '') {
             $('#form-add-gc #libelle_equipe').parent('div').fadeIn();
             $('#form-add-gc #libelle_equipe').val( 'EQUIPE '+$('#form-add-gc #technicien_id option:selected').text() ).show().fadeTo(10,1);
             //
             if($('#form-add-gc #technicien_id').val()!='' && $('#form-add-gc #prestataire_id').val()!='' && $('#form-add-gc #libelle_equipe').val()!='') {
                 $('#form-add-gc #bloc-btn-submit:hidden, #form-add-gc #btn-add-collabo:hidden').show(10);
                 $('#form-add-gc #btn-submit, #form-add-gc #btn-add-collabo').removeAttr('disabled');
             }
         }
         else {
             $('#form-add-gc #libelle_equipe').val('');
             $('.fird-role').parent('div').hide(10);
             $('#form-add-gc #bloc-btn-submit').hide(10);
         }
     });

     $('#form-add-gc #btn-add-collabo').click(function() {
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $.ajax({
             type: 'GET',
             url: $(this).attr('rel'),
             data: 'prestataire_id='+$('#form-add-gc #prestataire_id').val()+'&technicien_id='+$('#form-add-gc #technicien_id').val()+'&service_id='+$('#form-add-gc #service_id').val(),
             cache: false,
             contentType: false,
             processData:false,
             beforeSend: function(){
                 $(".field").attr('disabled', 'disabled');
                 $('#form-add-gc #btn-add-collabo').addClass('disabled');
                 $("#form-add-gc #liste_collabo").fadeTo(10,0.5);
             },
             error:function(xhr, ajaxOptions, thrownError){
                if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                    document.location.reload(true);
                }
                else {
                    alert(xhr.status + ' : ' + thrownError + ' : ' );
                }
                 $('#form-add-gc #liste_collabo').parent('div').hide();
                 $('#form-add-gc #btn-add-collabo').removeClass('disabled');
             },
             success: function(resp){
                 $(".field").removeAttr('disabled');
                 $('#form-add-gc #liste_collabo').parent('div').fadeIn();
                 $('#form-add-gc #liste_collabo').html(resp).fadeTo(10,1);
             }
         });
     });

     /**
      * FIN GESTION EQUIPE
      */


});

