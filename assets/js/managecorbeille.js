// disabled checkbox before dom's ready
$('input[type="checkbox"]').attr('disabled','disabled');
$(document).ready(function() {
    $('#btn-show-import-modal').removeClass('disabled');
    $('.select2').select2();
    $(".rating-css").barrating({theme:"css-stars",showSelectedRating:!1});
    //datatable
    $('.cb-datatable').DataTable({
        paging: false,
        select: true,
        "ordering": true,
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }],
        'pageLength':20
    });
    // reset form-updater input value
    $('.btn-editer').each(function(index, element) {
        $(this).click(function() {
            $('.form-editer:eq('+index+') .current-val').each(function() {
                Nm = $(this).attr('name');
                Old = $('.old_'+Nm+':eq('+index+') ');
                if( $(this).val() != Old.val() )
                    $(this).val(Old.val());
            });
        });
    });


    // Upload via Ajax..
    //$('#importfile').val('');
    $("#form-import-doc").on('submit', function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            type: 'POST',
            url: $("#form-import-doc").attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData:false,
            beforeSend: function(){
                $("#form-import-doc input, #form-import-doc button, #btn-reseter").attr('disabled','disabled');
                //$(".progress-bar").width('0%').css('padding', '10px 20px;');
                $('#uploadStatus').html($('#bloc-img').html());
            },
            error:function(xhr, ajaxOptions, thrownError){
                if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                    document.location.reload(true);
                }
                else {
                    alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
                }
                $('#uploadStatus').html('<h5 style="color:#EA4335;">Erreur d\'importation, veuillez réessayer.</h5>');
            },
            success: function(resp){
                //resp = JSON.parse(resp);
                $("#form-import-doc input, #form-import-doc button, #btn-reseter").removeAttr('disabled');
                $('#importfile').val('');
                $('#uploadStatus').html(resp);
                $('#btn-reseter').html("Fermer");
                //ajax to reload corbeille bloc
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: $("#reload-url").val(),
                    data: '',
                    cache: false,
                    contentType: false,
                    processData:false,
                    beforeSend: function(){
                        $('#bloc-corbeille').fadeTo(10,0.3);
                    },
                    error:function(xhr, ajaxOptions, thrownError){
                        if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                            document.location.reload(true);
                        }
                        else {
                            alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
                        }
                        $('#bloc-corbeille').fadeTo(10,1);
                    },
                    success: function(data){
                        $('#bloc-corbeille').html(data).fadeTo(100,1);
                    }
                });
            }
        });
    });

    /**
        Check box Raccord management
    */
    $('#checkAll-Raccord').click(function() {
        if($(this).is(':checked')) {
            $('#checkAll-2-Raccord').prop("checked", true);
            $('.checkItemRaccord').prop("checked", true);
            $('.tr-Raccord').addClass('item-checked');
        }
        else{
            $('#checkAll-2-Raccord').prop("checked", true);
            $('.checkItemRaccord').prop("checked", false);
            $('.tr-Raccord').removeClass('item-checked');
        }
    });
    $('#checkAll-2-Raccord').click(function() {
        if($(this).is(':checked')) {
            $('#checkAll-Raccord').prop("checked", true);
            $('.checkItemRaccord').prop("checked", true);
            $('.tr-Raccord').addClass('item-checked');
        }
        else{
            $('#checkAll-Raccord').prop("checked", false);
            $('.checkItemRaccord').prop("checked", false);
            $('.tr-Raccord').removeClass('item-checked');
        }
    });

    $('.checkItemRaccord-left').each(function(index, element) {
        $(this).click(function() {
            $('.checkAllRaccord').prop("checked", false);
            //
            if($(this).is(':checked')) {
                $('.checkItemRaccord-right:eq('+index+')').prop("checked", true);
                $('.checkItemRaccord-right:eq('+index+')').parent('div').parent('td').parent('tr').addClass('item-checked');
            }
            else{
                $('.checkItemRaccord-right:eq('+index+')').prop("checked", false);
                $('.checkItemRaccord-right:eq('+index+')').parent('div').parent('td').parent('tr').removeClass('item-checked');
            }
        });
    });
    //
    $('.checkItemRaccord-right').each(function(index, element) {
        $(this).click(function() {
            $('.checkAllRaccord').prop("checked", false);
            //
            if($(this).is(':checked')) {
                $('.checkItemRaccord-left:eq('+index+')').prop("checked", true);
                $('.checkItemRaccord-left:eq('+index+')').parent('div').parent('td').parent('tr').addClass('item-checked');
            }
            else{
                $('.checkItemRaccord-left:eq('+index+')').prop("checked", false);
                $('.checkItemRaccord-left:eq('+index+')').parent('div').parent('td').parent('tr').removeClass('item-checked');
            }
        });
    });
    //
    $('.checkItemRaccord, .checkAllRaccord').click(function() {
        if($('.checkItemRaccord:checked').length > 0) {
            $('#btnAttr-Raccord').fadeIn();
        }
        else{
            $('#btnAttr-Raccord').fadeOut();
        }
    });


    /**
        Check box Etude management
    */
    $('#checkAll-Etude').click(function() {
        if($(this).is(':checked')) {
            $('#checkAll-2-Etude').prop("checked", true);
            $('.checkItemEtude').prop("checked", true);
            $('.tr-Etude').addClass('item-checked');
        }
        else{
            $('#checkAll-2-Etude').prop("checked", true);
            $('.checkItemEtude').prop("checked", false);
            $('.tr-Etude').removeClass('item-checked');
        }
    });
    $('#checkAll-2-Etude').click(function() {
        if($(this).is(':checked')) {
            $('#checkAll-Etude').prop("checked", true);
            $('.checkItemEtude').prop("checked", true);
            $('.tr-Etude').addClass('item-checked');
        }
        else{
            $('#checkAll-Etude').prop("checked", false);
            $('.checkItemEtude').prop("checked", false);
            $('.tr-Etude').removeClass('item-checked');
        }
    });

    $('.checkItemEtude-left').each(function(index, element) {
        $(this).click(function() {
            $('.checkAllEtude').prop("checked", false);
            //
            if($(this).is(':checked')) {
                $('.checkItemEtude-right:eq('+index+')').prop("checked", true);
                $('.checkItemEtude-right:eq('+index+')').parent('div').parent('td').parent('tr').addClass('item-checked');
            }
            else{
                $('.checkItemEtude-right:eq('+index+')').prop("checked", false);
                $('.checkItemEtude-right:eq('+index+')').parent('div').parent('td').parent('tr').removeClass('item-checked');
            }
        });
    });
    //
    $('.checkItemEtude-right').each(function(index, element) {
        $(this).click(function() {
            $('.checkAllEtude').prop("checked", false);
            //
            if($(this).is(':checked')) {
                $('.checkItemEtude-left:eq('+index+')').prop("checked", true);
                $('.checkItemEtude-left:eq('+index+')').parent('div').parent('td').parent('tr').addClass('item-checked');
            }
            else{
                $('.checkItemEtude-left:eq('+index+')').prop("checked", false);
                $('.checkItemEtude-left:eq('+index+')').parent('div').parent('td').parent('tr').removeClass('item-checked');
            }
        });
    });
    //
    $('.checkItemEtude, .checkAllEtude').click(function() {
        if($('.checkItemEtude:checked').length > 0) {
            $('#btnAttr-Etude').fadeIn();
        }
        else{
            $('#btnAttr-Etude').fadeOut();
        }
    });
    //endof check box




    /**
        Attribution management
    */

    /**
    * Etude
    */
    $('#Reset-Attr-Etude').click(function() {
        $('#Bloc-Attr-Etude').html('');
    });
    //
    $('#btnAttr-Etude').click(function() {
        //ajax to reload corbeille bloc
        var formData = new FormData($('#Form-Attr-Etude')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: $('#Form-Attr-Etude').attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData:false,
            beforeSend: function(){
                $('#Bloc-Attr-Etude').html('<div class="text-center">'+$('#bloc-img-2').html()+'</div>');
            },
            error:function(xhr, ajaxOptions, thrownError){
                if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                    document.location.reload(true);
                }
                else {
                    alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
                }
            },
            success: function(data){
                $('#Bloc-Attr-Etude').html(data);
            }
        });
        //
    });


    /**
    * Raccord
    */
    $('#Reset-Attr-Raccord').click(function() {
        $('#Bloc-Attr-Raccord').html('');
    });
    //
    $('#btnAttr-Raccord').click(function() {
        //ajax to reload corbeille bloc
        var formData = new FormData($('#Form-Attr-Raccord')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: $('#Form-Attr-Raccord').attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData:false,
            beforeSend: function(){
                $('#Bloc-Attr-Raccord').html('<div class="text-center">'+$('#bloc-img-2').html()+'</div>');
            },
            error:function(xhr, ajaxOptions, thrownError){
                if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                    document.location.reload(true);
                }
                else {
                    alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
                }
            },
            success: function(data){
                $('#Bloc-Attr-Raccord').html(data);
            }
        });
        //
    });

    // initiate checkbox
    $('input[type="checkbox"]').removeAttr('disabled');

});
