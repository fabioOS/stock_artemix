
$(function () {

    /**
     * GESTION EQUIPE
     */
     $('#form-add-equip #prestataire_id').val('');
     $('#form-add-equip #prestataire_id').change(function() {
         $('.fird-role').parent('div').hide();
         $('#form-add-equip #liste_collabo').html('').val('');
         $('#form-add-equip #bloc-btn-submit:visible, #form-add-equip #bloc-collabo').hide(10);
         $('#form-add-equip #btn-add-collabo').removeClass('disabled');
         if($(this).val() != '') {
             $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
             });
             $.ajax({
                 type: 'GET',
                 url: $(this).attr('rel'),
                 data: 'prestataire_id='+$(this).val()+'&service_id='+$('#form-add-equip #service_id').val(),
                 cache: false,
                 contentType: false,
                 processData:false,
                 beforeSend: function(){
                     $(".field").attr('disabled', 'disabled');
                     $("#form-add-equip #technicien_id").fadeTo(10,0.5);
                 },
                 error:function(xhr, ajaxOptions, thrownError){
                    if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                        document.location.reload(true);
                    }
                    else {
                        alert(xhr.status + ' : ' + thrownError + ' : ' );
                    }
                     $('#form-add-equip #technicien_id').parent('div').hide();
                     $('.field').removeAttr('disabled');
                 },
                 success: function(resp){
                     $('#form-add-equip #technicien_id').parent('div').fadeIn();
                     $('#form-add-equip #technicien_id').html(resp).fadeTo(10,1);
                     $('.field').removeAttr('disabled');
                 }
             });
         }
         else {
             $('.second-role, .fird-role').parent('div').hide();
             $('.second-role').val('');
             $('#form-add-equip #bloc-btn-submit:visible').hide(10);
         }
     });

     $('#form-add-equip #technicien_id').change(function() {
         $('#form-add-equip #liste_collabo').html('').val('');
         $('#form-add-equip #bloc-collabo').hide(10);
         $('#form-add-equip #btn-add-collabo').removeClass('disabled');
         if($(this).val() != '') {
             $('#form-add-equip #libelle_equipe').parent('div').fadeIn();
             $('#form-add-equip #libelle_equipe').val( 'EQUIPE '+$('#form-add-equip #technicien_id option:selected').text() ).show().fadeTo(10,1);
             //
             if($('#form-add-equip #technicien_id').val()!='' && $('#form-add-equip #prestataire_id').val()!='' && $('#form-add-equip #libelle_equipe').val()!='') {
                 $('#form-add-equip #bloc-btn-submit:hidden, #form-add-equip #btn-add-collabo:hidden').show(10);
                 $('#form-add-equip #btn-submit, #form-add-equip #btn-add-collabo').removeAttr('disabled');
             }
         }
         else {
             $('#form-add-equip #libelle_equipe').val('');
             $('.fird-role').parent('div').hide(10);
             $('#form-add-equip #bloc-btn-submit').hide(10);
         }
     });

     $('#form-add-equip #btn-add-collabo').click(function() {
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $.ajax({
             type: 'GET',
             url: $(this).attr('rel'),
             data: 'prestataire_id='+$('#form-add-equip #prestataire_id').val()+'&technicien_id='+$('#form-add-equip #technicien_id').val()+'&service_id='+$('#form-add-equip #service_id').val(),
             cache: false,
             contentType: false,
             processData:false,
             beforeSend: function(){
                 $(".field").attr('disabled', 'disabled');
                 $('#form-add-equip #btn-add-collabo').addClass('disabled');
                 $("#form-add-equip #liste_collabo").fadeTo(10,0.5);
             },
             error:function(xhr, ajaxOptions, thrownError){
                if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                    document.location.reload(true);
                }
                else {
                    alert(xhr.status + ' : ' + thrownError + ' : ' );
                }
                 $('#form-add-equip #liste_collabo').parent('div').hide();
                 $('#form-add-equip #btn-add-collabo').removeClass('disabled');
             },
             success: function(resp){
                 $(".field").removeAttr('disabled');
                 $('#form-add-equip #liste_collabo').parent('div').fadeIn();
                 $('#form-add-equip #liste_collabo').html(resp).fadeTo(10,1);
             }
         });
     });

     /**
      * FIN GESTION EQUIPE
      */


});

