/*
$(document).ready(function() {
    $('.btn-data-action').show(10);
});
*/

$(function () {

    function AJAX_FUNCT(METHOD, DATA, URL, CONTAINER) {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: METHOD,
            url: URL,
            data: DATA,
            error: function (xhr, ajaxOptions, thrownError){
                alert(xhr.status + ' : ' + thrownError + ' : ' +URL );
            },
            success: function(data) {
                CONTAINER.html(data);
            }
        });
    }

    $('.text-temporaire:visible').fadeTo(6000, 1, function() {
        $(this).fadeTo(3000, 0.9, function() {
            $(this).fadeTo(10,0.01);
        });
        $(this).hover(function() {
            $(this).fadeTo(10, 0.01);
        });
    });



    /**
     * search form
     */
    // Upload via Ajax Rupture
    $("#btn-submit-search").click(function(e){
        //e.preventDefault();
        var formData = new FormData($('#search-form')[0]);
        var nbFields = 0;
        $('#search-form .search-field').each(function(index, element){
            if( $(this).val() != '' && $(this).val() != null ){
                nbFields = nbFields+1;
            }
        });
        $('#search-form .required').each(function(index, element){
            if( $(this).val() == '' ){
                nbFields = -1;
            }
        });
        if( nbFields < 0 ) {
            $('#data-container').html('<h5 class="text-warning"><i class="fas fa-info-circle"></i> &nbsp;  Veuillez renseigner tous les champs !</h5>');
        }
        else if( nbFields == 0 ) {
            $('#data-container').html('<h5 class="text-warning"><i class="fas fa-info-circle"></i> &nbsp;  Veuillez sélectionner au moins un champ !</h5>');
        }
        else {
            $('#data-container').html('');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: $("#search-form").attr('action'),
                data: formData, //$('#search-form').serialize(),
                cache: false,
                contentType: false,
                processData:false,
                beforeSend: function(){
                    $("#search-form :input").attr('disabled','disabled');
                    $("#bloc-img-loader").show();
                },
                error:function(xhr, ajaxOptions, thrownError){
                    if((xhr.status == 401 && thrownError == 'Unauthorized') || (xhr.status == 419 && thrownError == 'unknown status')) {
                        document.location.reload(true);
                    }
                    else {
                        alert(xhr.status + ' : ' + thrownError + ' : ' );
                    }
                    $('#uploadStatus').html('<h5 style="color:#EA4335;">Erreur de recherche, veuillez réessayer.</h5>');
                    $("#search-form :input").removeAttr('disabled');
                    $("#bloc-img-loader").hide();
                },
                success: function(resp){
                    $('#data-container').html(resp);
                    $("#search-form :input").removeAttr('disabled');
                    $("#bloc-img-loader").hide();
                }
            });

        }//endof else

    });


    $('.datatable').datatables();


});

