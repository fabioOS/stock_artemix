<style>
    .table td, .table th{
        padding: 0.5rem;
    }
</style>
<div class="row mb-4">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">BC N°</h5>
                        <p>{{strtoupper($demande->ref)}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Agent</h5>
                        <p>{{strtoupper($demande->user->name)}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Fournisseur</h5>
                        <p>{{$demande->fournisseur->nom}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Montant total</h5>
                        <p>@price($demande->montant - $demande->retenu??0) Fcfa</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Type de bon</h5>
                        <p>{{$demande->typebc==1? "A LIVRER" : "A DECAISSER"}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Date de demande</h5>
                        <p>{{Illuminate\Support\Carbon::parse($demande->created_at)->format('d/m/Y')}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Date de paiement</h5>
                        <p>{{Illuminate\Support\Carbon::parse($demande->datepaiement)->format('d/m/Y')}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Mode paiement</h5>
                        <p>{{$demande->mode}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">N°Proforma</h5>
                        <p>{{$demande->proforma ?? '-'}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">TVA</h5>
                        <p>{{$demande->tva ?? '-'}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">RETENUE 2%</h5>
                        <p>{{$demande->retenu ? 'OUI' :'NON'}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        @if($demande->status == 1)
                            <div class="row gstock">
                            </div>
                        @elseif($demande->status== 2)
                            <div class="row gstock">
                                <div class="mr-1">
                                    <a href="{{route('daf.show.pdf',encrypt($demande->ref))}}" class="btn btn-info" title="Imprimer le bon"> <i class="fa fa-file-pdf"></i> </a>
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>

    @if($demande->status== 4)
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <h5 class="font-size-16">Motif du rejet</h5>
                        <p>{{$demande->motif}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="col-xl-12">
        <div class="card border shadow-none">
            <div class="card-header bg-transparent border-bottom py-3 px-4">
                <h5 class="font-size-16 mb-0">Matières commandées </h5>
            </div>
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>Num</td>
                                <td>Code</td>
                                <td>Matière</td>
                                <td>U</td>
                                <td>Quantité</td>
                                <td>P.U</td>
                                <td>Remise</td>
                                <td>Prix U.HT</td>
                                <td>Montant</td>
                            </tr>
                            @foreach ($demande->items as $i=>$item)
                            @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                                {{-- @dump((($item->remise /100) * $item->price)) --}}
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$pdt->code}}</td>
                                    <td><b>{{$pdt->libelle}}</b></td>
                                    <td>{{$pdt->type->libelle}}</td>
                                    <td>{{$item->qte}} </td>
                                    <td>@price($item->price)</td>
                                    <td>{{$item->remise}} %</td>
                                    <td>@price($item->price - (($item->remise /100) * $item->price))</td>
                                    <td>@price($item->mtt)</td>
                                </tr>
                            @endforeach
                            <tr><td colspan="8"></td></tr>

                            <tr>
                                <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL HT</td>
                                <td class="font-weight-bold">@price($demande->montant)</td>
                            </tr>
                            @if($demande->tva == "AVEC")
                            <tr>
                                <td colspan="8" class="text-right font-weight-bold">TVA 18%</td>
                                <td class="font-weight-bold">@price($demande->montant *0.18)</td>
                            </tr>
                            <tr>
                                <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL TTC</td>
                                <td class="font-weight-bold">@price(($demande->montant *0.18)+$demande->montant)</td>
                            </tr>
                            @else
                                @if($demande->retenu)
                                    <tr>
                                        <td colspan="8" class="text-right font-weight-bold">RETENUE 2%</td>
                                        <td class="font-weight-bold">@price($demande->retenu)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL TTC</td>
                                        <td class="font-weight-bold">@price($demande->montant-$demande->retenu)</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL TTC</td>
                                        <td class="font-weight-bold">@price($demande->montant)</td>
                                    </tr>
                                @endif
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- end table-responsive -->
            </div>
        </div>
    </div>

    @if($demande->ibution_id)
    <div class="col-xl-12">
        <div class="alert alert-danger text-center" role="alert">
            Le bon de commande <span class="font-weight-bold">{{$demande->ref}}</span> fait l'objet d'une exonération de TVA. <br>
            Le montant total TTC est de : <b>@price($demande->montant) {{'FCFA'}}</b>
        </div>
    </div> @endif
</div>
