@if(count($datas)>0)
@php $type = $datas->first()->type_export; @endphp

    @if($type == "soustraitant")
        {{-- ETAT DES SOUSTRAITANTS --}}
        <table>
            <thead>
                <tr>
                    <th>NUM</th>
                    <th>TRANS.ID</th>
                    <th>SOUS-TRAITANT</th>
                    <th>CONTRAT</th>
                    <th>VILLA</th>
                    <th>MONTANT</th>
                    <th>DATE</th>
                    <th>STATUS</th>
                </tr>
            </thead>

            <tbody>
                @foreach($datas as $k=>$list)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td>{{$list->slug}}</td>
                        <td>{{$list->soustraitant->nom}}</td>
                        <td>{{$list->contrat->slug}}</td>
                        <td>{{$list->lot->lot}}</td>
                        <td>@price($list->montant)</td>
                        <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                        <td>{{$list->getLibelleStatut()}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        {{-- ETAT DES FOURNISSEURS --}}
        <table>
            <thead>
                <tr>
                    <th>NUM</th>
                    <th>FACT.ID</th>
                    <th>BC</th>
                    <th>FACTURE</th>
                    <th>FOURNISSEUR</th>
                    <th>MONTANT</th>
                    <th>DATE</th>
                    <th>STATUS</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($datas as $key => $facture)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$facture->ref}}</td>
                        <td>{{$facture->boncommande->ref}}</td>
                        <td>{{$facture->numfacture}}</td>
                        <td>{{$facture->fournisseur->nom}}</td>
                        <td>@price($facture->montant)</td>
                        <td>{{$facture->created_at->format('d/m/Y H:i')}}</td>
                        <td>{{$facture->getLibelleStatutEntite()}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    @endif
@endif
