@if($data)
    @if($type == "soustraitant")

    @else
        {{-- ETAT DES FOURNISSEURS --}}
        <table>
            <thead>
                <tr>
                    {{-- <th>NUM</th> --}}
                    <th>FOURNISSEUR</th>
                    <th>NOMBRE DE BC</th>
                    <th>NOMBRE DE FACTURE</th>
                    <th>MONTANT TOTAL BC </th>
                    <th>MONTANT PAYE</th>
                    <th>MONTANT RESTANT A PAYE</th>
                    <th>EXONERATION TVA</th>
                </tr>
            </thead>

            <tbody>
                {{-- @foreach ($data as $item) --}}
                @php $fournisseur = $data->fournisseur; @endphp
                    <tr>
                        {{-- <td>{{$key+1}}</td> --}}
                        <td>{{$fournisseur->nom}}</td>
                        <td>{{$fournisseur->nb_bc}}</td>
                        <td>{{$fournisseur->nb_facture}}</td>
                        <td>@price($fournisseur->coutglobal)</td>
                        <td>@price($fournisseur->mttpay)</td>
                        <td>@price($fournisseur->reste)</td>
                        <td>@price($fournisseur->exo)</td>
                    </tr>
                {{-- @endforeach --}}

            </tbody>
        </table>

    @endif
@endif
