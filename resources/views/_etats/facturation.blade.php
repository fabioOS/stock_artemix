@extends($blade)

@section('page_title') Etats des facturations et paiements @stop

@section('content_title') Etats des facturations et paiements @stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Etats des facturations et paiements</li>
    </ol>
@stop

@section('content')
<div class="row">
    <div class="col-lg-8 offset-2">
        <form action="{{route('etats.facturations.store')}}" class="" method="post" id="etatsbc">
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4 text-primary">Edition d'un etat </h4>
                    <div class="form-group">
                        <label for="">TYPE D'ETAT <span class="text-danger">*</span></label>
                        <select name="typetat" id="typetat" class="form-control select2" style="min-width: 300px;" required>
                            <option selected disabled>Sélectionner le fournisseur</option>
                            <option value="fournisseur">FOURNISSEUR</option>
                            <option value="soustraitant">SOUSTRAITANT</option>
                        </select>
                    </div>

                    <div id="fourniss" style="display: none">
                        <div class="form-group">
                            <label for="">FOURNISSEUR</label>
                            <select name="fournisseur" id="fournisseur" class="form-control select2" style="width: 100%">
                                <option selected disabled>Sélectionner le fournisseur</option>
                                @foreach ($fournisseurs as $fournisseur)
                                    <option value="{{$fournisseur->id}}">{{$fournisseur->nom}} - {{$fournisseur->contact}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="soustrait" style="display: none">
                        <div class="form-group">
                            <label for="">SOUSTRAITANT</label>
                            <select name="soustraitant" id="soustraitant" class="form-control select2" style="width: 100%">
                                <option selected disabled>Sélectionner le soustraitant</option>
                                @foreach ($soustraitants as $sous)
                                    <option value="{{$sous->id}}">{{$sous->nom}} - {{$sous->contact}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">STATUS </label>
                        <select name="statu" id="statu" class="form-control" style="width:100%;">
                            <option value="0">Sélectionner un status</option>
                            <option value="1">EN ATTENTE PAIEMENT</option>
                            <option value="2">PAYER</option>
                        </select>
                    </div>

                    {{-- <div class="form-group">
                        <label for="">TYPE </label>
                        <select name="typebc" id="typebc" class="form-control" style="width:100%;">
                            <option selected disabled>Sélectionner un type</option>
                            <option value="1">A LIVRER</option>
                            <option value="2">A DECAISSER</option>
                        </select>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="date">DATE DEBUT</label>
                            <input type="date" name="datedebut" id="datedebut" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="date">DATE FIN</label>
                            <input type="date" name="datefin" id="datefin" class="form-control">
                        </div>
                    </div> --}}

                    <div class="row mt-4">
                        {{-- <div class="col-md-6">
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio4" value="pdf" name="typexport" checked="">
                                <label for="inlineRadio4"> PDF </label>
                            </div>
                        </div> --}}
                        <div class="col-md-">
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio5" value="excel" name="typexport" checked="">
                                <label for="inlineRadio5"> EXCEL </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div id="loader" class="dispayNone">
                    <div class="spinner-border text-primary m-1" role="loader">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Générer</button>
            </div>
        </form>

    </div>
</div>
@endsection


@section('other_js')
<script>
    $(document).ready(function(){
        $(".select2").select2();

        //On change du type d'etat (#typetat)
        $('#typetat').change(function(){
            var val = $(this).val();
            if(val == 'fournisseur'){
                $('#fourniss').show();
                $('#soustrait').hide();
            }else{
                $('#fourniss').hide();
                $('#soustrait').show();
            }
        });
    });

    function submitForm() {
        event.preventDefault();
        document.getElementById('etatsbc').submit();
    }

</script>
@endsection
