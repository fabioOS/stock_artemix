@extends($blade)

@section('page_title') Statistique fournisseur @stop

@section('content_title') Statistique fournisseur @stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Statistique fournisseur</li>
    </ol>
@stop

@section('content')
<div class="row mb-4">
    <div class="col-xl-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-5 text-primary">IDENTIFIANT FOURNISSEUR</h4>
                <div class="row">
                    <div class="text-muted col-lg-12">
                        <h5 class="font-size-15 mb-2">FOURNISSEUR</h5>
                        <p>{{$fournisseur->nom}}</p>
                    </div>

                    <div class="text-muted col-lg-12">
                        <h5 class="font-size-16 mb-2">EMAIL</h5>
                        <p>{{$fournisseur->email ?? 'Non renseigné'}}</p>
                    </div>

                    <div class="text-muted col-lg-12">
                        <h5 class="font-size-16 mb-2">CONTACT</h5>
                        <p>{{$fournisseur->contact ?? 'Non renseigné'}}</p>
                    </div>

                    <div class="text-muted col-lg-12">
                        <h5 class="font-size-16 mb-2">ADRESSE</h5>
                        <p> {{$fournisseur->adresse ?? 'Non renseigné'}} </p>
                    </div>

                    <div class="text-muted col-lg-12">
                        <h5 class="font-size-16 mb-2">REGISME FISCAL</h5>
                        <p>{{$fournisseur->regime ?? 'Non renseigné'}}</p>
                    </div>

                    <div class="text-muted col-lg-12">
                        <h5 class="font-size-16 mb-2">LIGNE CREDIT</h5>
                        <p>{{$fournisseur->credit ?? 'Non renseigné'}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-9">
        <div class="row">
            <div class="col-md-4 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h4 class="mb-1 mt-1 font-size-24">@if($stats['coutglobal'] == null) 0 @else @price($stats['coutglobal']) @endif<sup>FCFA</sup></h4>
                            <p class="text-muted mb-0">Coût global des bons de commande</p>
                        </div>

                    </div>
                </div>
            </div> <!-- end col-->

            <div class="col-md-4 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h4 class="mb-1 mt-1 font-size-24">@if($stats['mttpay'] == null) 0 @else @price($stats['mttpay']) @endif<sup>FCFA</sup></h4>
                            <p class="text-muted mb-0">Montant global payé</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end col-->

            <div class="col-md-4 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h4 class="mb-1 mt-1 font-size-24">@if($stats['reste'] == null) 0 @else @price($stats['reste']) @endif<sup>FCFA</sup></h4>
                            <p class="text-muted mb-0">Montant exigible</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12">
                            <div class="table-responsive mb-4 gstock">
                                <table id="datatable-buttons" class="table table-centered datatable " >
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>REF</th>
                                            <th>BC</th>
                                            <th>N° Facture</th>
                                            <th>Montant</th>
                                            <th>Document</th>
                                            <th>Statut</th>
                                            <th>Agent</th>
                                            <th>Date reception</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($factures as $key => $facture)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td class="text-dark font-weight-bold">{{$facture->ref}}</td>
                                                <td><a target="_blank" href="{{route('daf.show', encrypt($facture->boncommande->id))}}" class="font-weight-bold">{{$facture->boncommande->ref}}</a></td>
                                                <td>{{$facture->numfacture}}
                                                    <br>
                                                    <span class="badge badge-soft-dark font-size-10">{{$facture->created_at->format('d/m/Y')}}</span>
                                                </td>
                                                <td>@price($facture->montant) Fcfa</td>
                                                <td>
                                                    @if($facture->fichier != null)
                                                        <a href="{{asset('assets/uploads/factures/fournisseurs/'.$facture->fichier)}}" target="_blank" class="btn btn-primary btn-sm waves-effect waves-light"><i class="mdi mdi-file-pdf"></i> Voir</a>
                                                    @else
                                                        <span class="badge bg-danger">Aucun document</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {!! $facture->getLibelleStatut() !!}
                                                </td>
                                                <td>{{$facture->user->name}} </td>
                                                <td>{{$facture->created_at->format('d/m/Y H:i')}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('other_js')

@endsection
