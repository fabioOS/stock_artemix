@extends($blade)

@section('page_title') Ligne budgétaire @stop

@section('content_title') Ligne budgétaire @stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        {{-- <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li> --}}
        <li class="breadcrumb-item active">Ligne budgétaire</li>
    </ol>
@stop

@section('content')

    <div class="row">
        @foreach ($lignes as $ii=>$sousligne)
            <div class="col-xl-12 col-sm-12">
                <div class="card bg-primary text-center">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <h2 class="font-size-20 text-white text-strong mb-1 text-uppercase">
                            {{$ii== 1 ? 'BATIMENT' : 'VRD'}}
                        </h2>
                    </div>
                </div>
            </div>

            @forelse ($sousligne as $k=>$item)
                <div class="col-xl-4 col-sm-6">
                    <div style="position: relative;top: 30px;z-index: 9;left: 10px;background: #ef7e12;width: 20px;text-align: center;border-radius: 50%;color: #fff;opacity: 0.4;"> <span>{{$k+1}}</span> </div>
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="clearfix"></div>
                            <h2 class="font-size-20 text-strong mb-1 text-uppercase">{{$item->libelle}}</h2>
                            <p class="small mb-2">{{count($item->commandes)}} commande{{count($item->commandes)>1 ? 's':''}}</p>

                            <div class="progress mt-3" style="height: 20px;">
                                <div class="progress-bar progress-bar bg-{{$item->color}}" role="progressbar" style="width: {{$item->pcent}}%" aria-valuenow="{{$item->pcent}}" aria-valuemin="0" aria-valuemax="{{$item->pcent}}">
                                    {{$item->pcent}}%
                                </div>
                            </div>
                            <span class="card-title font-weight-bolder text-{{$item->color}}">@price($item->consommer) / @price($item->montant)</span>
                        </div>

                        <div class="btn-group" role="group">
                            <a href="{{route('lignebudgetaire.show',encrypt($item->id))}}" class="btn btn-outline-light text-truncate"><i class="uil uil-eye mr-1"></i> Voir plus</a>
                            {{-- <button type="button" class="btn btn-outline-light text-truncate"><i class="fas fa-file-export mr-1"></i> Exporter</button> --}}
                        </div>
                    </div>
                </div>

            @empty
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Aucune ligne budgétaire</h4>
                            <p class="card-text">Aucune ligne budgétaire n'a été enregistrée pour le moment.</p>
                        </div>
                    </div>
                </div>
            @endforelse
        @endforeach
    </div>

@endsection


@section('other_js')

@endsection
