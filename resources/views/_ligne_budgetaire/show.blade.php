@extends($blade)

@section('page_title') Ligne budgétaire {{$ligne->libelle}} @stop

@section('content_title') Ligne budgétaire <span class="text-primary">{{$ligne->libelle}}</span> @stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détail de la Ligne budgétaire</li>
    </ol>
@stop

@section('content')

<div class="row mb-4">
    <div class="col-xl-2">
        <div class="card h-100">
            <div class="card-body">
                <div class="text-center">
                    <h5 class="mt-3 mb-1">
                        <span class="text-primary">{{$ligne->libelle}}</span>
                        <br>
                        <span class="mt-3 badge badge-pill badge-danger">{{$ligne->type==1 ? 'BATIMENT' : 'VRD'}}</span>
                    </h5>
                    {{-- <div class="mt-4">
                        <button type="button" class="btn btn-light btn-sm"><i class="uil uil-envelope-alt mr-2"></i> Message</button>
                    </div> --}}
                </div>

                <hr class="my-4">

                <div class="text-muted">
                    <div class="table-responsive mt-4">
                        <div>
                            <p class="mb-1">Montant total</p>
                            <h5 class="font-size-16">@price($ligne->montant)</h5>
                        </div>
                        <div class="mt-4">
                            <p class="mb-1">Consommé </p>
                            <h5 class="font-size-16 text-{{$ligne->color}}">@price($ligne->consommer)</h5>
                        </div>
                        <div class="mt-4">
                            <p class="mb-1">Taux de consommation</p>
                            <h5 class="font-size-16">@price($ligne->pcent) %</h5>
                        </div>
                        <div class="mt-4">
                            <p class="mb-1">Reste à consommer</p>
                            <h5 class="font-size-16">@price($ligne->montant-$ligne->consommer) <span class="badge badge-pill badge-danger">@price(100 - $ligne->pcent) %</span></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-10">
        <div class="card mb-0">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#about" role="tab" aria-selected="false">
                        <i class="uil-file-alt font-size-20"></i>
                        <span class="d-none d-sm-block">Commandes</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tasks" role="tab" aria-selected="false">
                        <i class="uil-file-alt font-size-20"></i>
                        <span class="d-none d-sm-block">Bon de commande</span>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-selected="true">
                        <i class="uil uil-envelope-alt font-size-20"></i>
                        <span class="d-none d-sm-block">Messages</span>
                    </a>
                </li> --}}
            </ul>
            <!-- Tab content -->
            <div class="tab-content p-4">
                <div class="tab-pane active" id="about" role="tabpanel">
                    <div>
                        <h5 class="font-size-14 font-weight-bold text-uppercase my-4">Liste des commandes</h5>

                            <div class="table-responsive">
                                <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                    <tr>
                                        <th scope="col">N°</th>
                                        <th scope="col">Référence</th>
                                        <th scope="col">Demandeur</th>
                                        <th scope="col">Nombre de matière</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="listLot">
                                    @foreach($ligne->commandes as $k=>$dt)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td><a href="{{route('lignebudgetaire.redirect', encrypt($dt->id))}}?web=commande" target="_blank" class="text-primary font-weight-bold">{{$dt->ref}}</a> </td>
                                            <td>{{$dt->user->name}}</td>
                                            <td>{{count($dt->produits)}}</td>
                                            <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                            <td>
                                                <a href="{{route('lignebudgetaire.redirect', encrypt($dt->id))}}?web=commande" target="_blank" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                            </td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>

                <div class="tab-pane" id="tasks" role="tabpanel">
                    <div>
                        <h5 class="font-size-14 font-weight-bold text-uppercase my-4">Liste des bons de commandes</h5>

                            <div class="table-responsive">
                                <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>BC N°</th>
                                        <th>Agent</th>
                                        <th>Bon demande</th>
                                        <th>Fournisseur</th>
                                        <th>Montant total</th>
                                        <th>Date</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="listLot">
                                    @foreach($ligne->bcs as $k=>$bc)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td><a href="!#" class="text-dark font-weight-bold">{{$bc->ref}}</a> </td>
                                            <td>{{$bc->user->name}}</td>
                                            <td><a target="_blank" href="{{route('lignebudgetaire.redirect', encrypt($bc->demande->id))}}?web=commande" class="">{{$bc->demande->ref}}</a></td>
                                            <td>{{$bc->fournisseur->nom}}</td>
                                            <td>@price($bc->montant - $bc->retenu ??0) Fcfa</td>
                                            <td>{{Illuminate\Support\Carbon::parse($bc->created_at)->format('d/m/Y H:i')}}</td>
                                            <td>
                                                <a target="_blank" href="{{route('lignebudgetaire.redirect', encrypt($bc->id))}}?web=bcmmande" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>

                <div class="tab-pane" id="messages" role="tabpanel">
                    <div>
                        <div data-simplebar="init" style="max-height: 430px;"><div class="simplebar-wrapper" style="margin: 0px;"><div class="simplebar-height-auto-observer-wrapper"><div class="simplebar-height-auto-observer"></div></div><div class="simplebar-mask"><div class="simplebar-offset" style="right: -20px; bottom: 0px;"><div class="simplebar-content-wrapper" style="height: auto; padding-right: 20px; padding-bottom: 0px; overflow: hidden scroll;"><div class="simplebar-content" style="padding: 0px;">
                            <div class="media border-bottom py-4">
                                <img class="mr-2 rounded-circle avatar-xs" src="../assets/images/users/avatar-3.jpg" alt="">
                                <div class="media-body">
                                    <h5 class="font-size-15 mt-0 mb-1">Marion Walker <small class="text-muted float-right">1 hr ago</small></h5>
                                    <p class="text-muted">If several languages coalesce, the grammar of the resulting .</p>

                                    <a href="javascript: void(0);" class="text-muted font-13 d-inline-block"><i class="mdi mdi-reply"></i> Reply</a>

                                    <div class="media mt-4">
                                        <img class="mr-2 rounded-circle avatar-xs" src="../assets/images/users/avatar-4.jpg" alt="">
                                        <div class="media-body">
                                            <h5 class="font-size-15 mt-0 mb-1">Shanon Marvin <small class="text-muted float-right">1 hr ago</small></h5>
                                            <p class="text-muted">It will be as simple as in fact, it will be Occidental. To it will seem like simplified .</p>


                                            <a href="javascript: void(0);" class="text-muted font-13 d-inline-block">
                                                <i class="mdi mdi-reply"></i> Reply
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="media border-bottom py-4">
                                <img class="mr-2 rounded-circle avatar-xs" src="../assets/images/users/avatar-5.jpg" alt="">
                                <div class="media-body">
                                    <h5 class="font-size-15 mt-0 mb-1">Janice Morgan <small class="text-muted float-right">2 hrs ago</small></h5>
                                    <p class="text-muted">To achieve this, it would be necessary to have uniform pronunciation.</p>

                                    <a href="javascript: void(0);" class="text-muted font-13 d-inline-block"><i class="mdi mdi-reply"></i> Reply</a>

                                </div>
                            </div>

                            <div class="media border-bottom py-4">
                                <img class="mr-2 rounded-circle avatar-xs" src="../assets/images/users/avatar-7.jpg" alt="">
                                <div class="media-body">
                                    <h5 class="font-size-15 mt-0 mb-1">Patrick Petty <small class="text-muted float-right">3 hrs ago</small></h5>
                                    <p class="text-muted">Sed ut perspiciatis unde omnis iste natus error sit </p>

                                    <a href="javascript: void(0);" class="text-muted font-13 d-inline-block"><i class="mdi mdi-reply"></i> Reply</a>

                                </div>
                            </div>
                        </div></div></div></div><div class="simplebar-placeholder" style="width: auto; height: 496px;"></div></div><div class="simplebar-track simplebar-horizontal" style="visibility: hidden;"><div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;"></div></div><div class="simplebar-track simplebar-vertical" style="visibility: visible;"><div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: block; height: 388px;"></div></div></div>

                        <div class="border rounded mt-4">
                            <form action="#">
                                <div class="px-2 py-1 bg-light">

                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-sm btn-link text-dark text-decoration-none"><i class="uil uil-link"></i></button>
                                        <button type="button" class="btn btn-sm btn-link text-dark text-decoration-none"><i class="uil uil-smile"></i></button>
                                        <button type="button" class="btn btn-sm btn-link text-dark text-decoration-none"><i class="uil uil-at"></i></button>
                                      </div>

                                </div>
                                <textarea rows="3" class="form-control border-0 resize-none" placeholder="Your Message..."></textarea>

                            </form>
                        </div> <!-- end .border-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('other_js')

@endsection
