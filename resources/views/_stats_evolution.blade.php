@php
    $som = 0;
    $idprogram = Session::get('program') ?: 0 ;
    $program = \App\Programme::where('id',$idprogram)->first();
    if($program->type_rattachement == 1){
        $actifs = \App\ProgrammeCorpsetat::where('programme_id',$idprogram)->select('actif_id',DB::raw('round(SUM(prix)) as sprix'))->groupBy('actif_id')->get();
        foreach ($actifs as $actif){
            $lot = \App\ProgrammeLot::where('actif_id',$actif->actif_id)->count();
            $som = $som + ($lot * $actif->sprix);
        }
    }else{
        $som = \App\ProgrammeSoustraitantcontrat::where('programme_id',$idprogram)->select('montantctr',DB::raw('round(SUM(montantctr)) as sprix'))->first();
        $som = $som->sprix;
    }

    $coutglobal = $som;

    $idtranspay = \App\Transaction::where('programme_id',$idprogram)->where('status',2)->pluck('id')->toArray();
    // $payes = \App\Avancement::where('programme_id',$idprogram)->whereIn('transac_id',$idtranspay)->select('cout',DB::raw('round(SUM(cout)) as coutpaye'))->first();
    //$mttpaylot = $payes->coutpaye;
    $mttpaylot = \App\Avancement::where([['programme_id',$idprogram]])->sum('cout');

    $actifs = \App\ProgrammeActif::where('programme_id',$idprogram)->with('lots')->with('corpsetat')->get();
    $actifs->map(function ($item){
        $item->nbcelot = count($item->lots) * count($item->corpsetat);
    });

    $nbcorpetat = $actifs->sum('nbcelot');
    $tteval = \App\Avancement::where([['programme_id',$idprogram]])->sum('taux_execute');
    // dump($tteval,$nbcorpetat);
    $evol = round($tteval / $nbcorpetat,2);
@endphp

<div class="row">
    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24">@if($coutglobal == null) 0 @else @price($coutglobal) @endif<sup>FCFA</sup></h4>
                    <p class="text-muted mb-0">Coût global de construction</p>
                </div>

            </div>
        </div>
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24">@if($mttpaylot == null) 0 @else @price($mttpaylot) @endif<sup>FCFA</sup></h4>
                    <p class="text-muted mb-0">Montant global payé au sous-traitant</p>
                </div>
            </div>
        </div>
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24">@if($coutglobal-$mttpaylot == null) 0 @else @price($coutglobal-$mttpaylot) @endif<sup>FCFA</sup></h4>
                    <p class="text-muted mb-0">Montant exigible au sous-traitant</p>
                </div>
            </div>
        </div>
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-3">

        <div class="card">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24">{{ $evol}}%</h4>
                    <p class="text-muted mb-0">Evolution des travaux</p>
                </div>
            </div>
        </div>
    </div> <!-- end col-->
</div> <!-- end row-->
