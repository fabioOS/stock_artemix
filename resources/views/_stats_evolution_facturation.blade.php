@php
    $som = 0;
    $idprogram = Session::get('program') ?: 0 ;
    $program = \App\Programme::where('id',$idprogram)->first();
    if($program->type_rattachement == 1){
        $actifs = \App\ProgrammeCorpsetat::where('programme_id',$idprogram)->select('actif_id',DB::raw('round(SUM(prix)) as sprix'))->groupBy('actif_id')->get();
        foreach ($actifs as $actif){
            $lot = \App\ProgrammeLot::where('actif_id',$actif->actif_id)->count();
            $som = $som + ($lot * $actif->sprix);
        }
    }else{
        $som = \App\ProgrammeSoustraitantcontrat::where('programme_id',$idprogram)->select('montantctr',DB::raw('round(SUM(montantctr)) as sprix'))->first();
        $som = $som->sprix;
    }

    $coutglobal = $som;

    $mttpaylot = \App\Paiement::where('programme_id',$idprogram)->where('type_paie',1)->sum('montant');

@endphp

<div class="row">
    <div class="col-md-4 col-xl-4">
        <div class="card">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24">@if($coutglobal == null) 0 @else @price($coutglobal) @endif<sup>FCFA</sup></h4>
                    <p class="text-muted mb-0">Coût global de construction</p>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card bg-success">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24 text-white">@if($mttpaylot == null) 0 @else @price($mttpaylot) @endif<sup>FCFA</sup></h4>
                    <p class="text-white mb-0">Montant global payé au sous-traitant</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card bg-danger">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24 text-white">@if($coutglobal-$mttpaylot == null) 0 @else @price($coutglobal-$mttpaylot) @endif<sup>FCFA</sup></h4>
                    <p class="text-white mb-0">Montant exigible au sous-traitant</p>
                </div>
            </div>
        </div>
    </div>
</div>
