@php
    $idprogram = Session::get('program') ?: 0 ;
    $program = \App\Programme::where('id',$idprogram)->first();

    //Cout global des bons de commandes
    $coutglobal = \App\BC::where('programme_id',$idprogram)->where('status',2)->get()->sum(function ($bc) {
        return $bc->full_montant;
    });

    //Montant total payé aux fournisseurs
    $mttpay = \App\FactureFournisseur::where('programme_id',$idprogram)->where('status',2)->sum('montant');

@endphp

<div class="row">
    <div class="col-md-4 col-xl-4">
        <div class="card">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24">@if($coutglobal == null) 0 @else @price($coutglobal) @endif<sup>FCFA</sup></h4>
                    <p class="text-muted mb-0">Coût global des bons de commande</p>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card bg-success">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24 text-white">@if($mttpay == null) 0 @else @price($mttpay) @endif<sup>FCFA</sup></h4>
                    <p class="text-white mb-0">Montant global payé aux fournisseurs</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card bg-danger">
            <div class="card-body">
                <div>
                    <h4 class="mb-1 mt-1 font-size-24 text-white">@if($coutglobal-$mttpay == null) 0 @else @price($coutglobal-$mttpay) @endif<sup>FCFA</sup></h4>
                    <p class="text-white mb-0">Montant exigible</p>
                </div>
            </div>
        </div>
    </div>
</div>
