@extends('layouts.app')

@section('page_title')
    Tableau de bord
@stop

@section('content_title')
    Tableau de bord
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Tableau de bord</li>
    </ol>
@stop

@section('content')
    <div class="row">

        <div class="col-md-6 col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="float-right mt-2">
                        <i class="uil-building font-size-24"></i>
                    </div>
                    <div>
                        <h1 class="mb-1 mt-1"><span data-plugin="counterup">{{count($programmes)}}</span></h1>
                        <p class="text-muted font-size-18 mb-0">Programme{{count($programmes)>1 ? 's' : ''}} ajouté{{count($programmes)>1 ? 's' : ''}}</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="float-right mt-2">
                        <i class="uil-users-alt font-size-24"></i>
                    </div>
                    <div>
                        <h1 class="mb-1 mt-1"><span data-plugin="counterup">{{count($users)}}</span></h1>
                        <p class="text-muted font-size-18 mb-0">Utilisateur{{count($users)>1 ? 's' : ''}} ajouté{{count($users)>1 ? 's' : ''}}</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

    </div> <!-- end row-->


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Programmes ajoutés</h4>
                    <div class="table-responsive">
                        <table class="table datatables table-centered table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Nom</th>
                                <th>Localisation</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($programmes as $program)
                                <tr>
                                    <td>
                                        <img class="rounded" src="{{isset($program->img) ? asset('assets/uploads/programmes/'.$program->img) : asset('assets/user.jpeg')}}" alt="photo" height="80">
                                    </td>
                                    <td  style="width: 200px;"><a href="{{route('gprogramme.show',$program->slug)}}">{{$program->libelle}}</a></td>
                                    <td  style="width: 200px;">{{$program->localisation}}</td>
                                    <td>{!! $program->description !!}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Utilisateurs ajoutés</h4>
                    <div class="table-responsive">
                        <table class="table datatables table-centered table-striped table-bordered datatable">
                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Nom & Prenom</th>
                                <th>Role</th>
                                <th>Actif</th>
                                <th>Programmes</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        <img class="rounded" src="{{isset($user->img) ? asset('assets/uploads/users/'.$user->img) : asset('assets/user.jpeg')}}" alt="photo" height="80">
                                    </td>
                                    <td  style="width: 200px;">{{$user->name}}</td>
                                    <td  style="width: 200px;">{{$user->role ? $user->role->libelle : ''}}</td>
                                    <td>
                                        <div class="custom-control custom-switch mb-3" dir="ltr">
                                            <input {{$user->actif== 1 ? 'checked' : ''}} type="checkbox" class="custom-control-input" id="customSwitch1" disabled>
                                            <label class="custom-control-label" for="customSwitch1"></label>
                                        </div>
                                    </td>
                                    <td>
                                        @foreach($user->programmes as $pgram)
                                            <span class="badge badge-pill badge-soft-info font-size-12">{{$pgram->libelle}}</span>
                                        @endforeach
                                    </td>
                                </tr>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection


@section('other_js')
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
@endsection
