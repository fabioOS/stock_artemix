@extends('layouts.app')

@section('page_title')
    Importation
@stop

@section('content_title')
    Importation
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Importation</li>
    </ol>
@stop

@section('content')
    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 my-2">
                            <h5 class="font-size-14 font-weight-bold text-uppercase mb-2">Ajout des fichier par lot</h5>
                            <hr class="mb-3">
                            <form action="{{route('importe.store')}}" enctype="multipart/form-data" method="POST" id="uploadLot">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-lg-12">
                                        <label class="control-label">Programmes <span class="text-danger">*</span></label>
                                        <br>
                                        <select class="form-control select_plugin" style="width: 100%;" id="id_type_lot" name="id_type_lot" required="">
                                            <option selected="" value="">Selectionnez un programme</option>
                                            @foreach($programmes as $programme)
                                                <option value="{{$programme->id}}" {{$programme->id==3 ? 'selected' : ''}}>{{$programme->libelle}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-lg-12">
                                        <label for="userfile">Chargez le fichier (Format XLSX accepté) <span class="text-danger">*</span></label>
                                        <input type="file" class="form-control" name="userfile[]" id="userfile" accept=".XLSX" required="" multiple>
                                        <p class="help-block">Taille maximum : 100 Mo</p>
                                    </div>
                                </div>
                                <img src="{{asset('assets/images/loading.gif')}}" id="loader" style="display: none">
                                <button onclick="sendform()" class="btn btn-primary" id="btnsend">IMPORTER</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection


@section('other_js')
    <!-- apexcharts -->

    <script>
        function sendform(){
            event.preventDefault();
            var pgrm = $("#id_type_lot").val();
            var userfile = $('#userfile').val();

            if(pgrm == null || pgrm== ''){
                swal("Veuillez selectionnez le programme");
                return null;
            }

            if(userfile == null || userfile== ''){
                swal("Veuillez charger le fichier");
                return null;
            }

            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment importer cet fichier",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#btnsend').hide();
                    $('#loader').show();
                    document.getElementById("uploadLot").submit();
                }
            });
        }
    </script>
@endsection
