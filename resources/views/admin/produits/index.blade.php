@extends('layouts.app')

@section('page_title')
    Gestions des matières
@stop

@section('content_title')
    Gestions des matières
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Administrateur</a></li>
        <li class="breadcrumb-item"><a href="#">Matières</a></li>
        <li class="breadcrumb-item active">Liste des matières</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-5">
                        <a href="#!" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#addProg">
                            <i class="mdi mdi-plus mr-2"></i> Ajouter une matière
                        </a>
                    </div>

                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Code</th>
                            <th>Categorie</th>
                            <th>Nom</th>
                            <th>Seuil</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($produits as $k=>$produit)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$produit->code}}</td>
                                <td>
                                    @if($produit->type_materiel == "materiel")
                                        <span class="badge badge-primary badge-sm">Matériel</span>
                                    @else
                                        <span class="badge badge-success badge-sm">Matériau</span>
                                    @endif
                                </td>
                                <td>{{$produit->libelle}}</td>
                                <td>{{$produit->seuil}}</td>
                                <td>{{$produit->type ? $produit->type->libelle : ''}}</td>
                                <td>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <a href="#" onclick="displayModalE('{{$produit->code}}','{{$produit->slug}}',`{{$produit->libelle}}`,'{{$produit->type_id}}','{{$produit->seuil}}')" class="editerProgramme px-2 text-primary" data-toggle="modal" data-target="#updateProg" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    <!-- modal add user -->
    <div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="addProglLabel">Ajouter une matières</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('gproduits.store')}}" method="post" id="progForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Code <span class="text-danger">*</span></label>
                            <input type="text" name="code" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label>Désignation <span class="text-danger">*</span></label>
                            <input type="text" name="nom" class="form-control" required="">
                        </div>

                        {{-- <div class="form-group">
                            <label>Seuil <span class="text-danger">*</span></label>
                            <input type="number" value="0" step="any" name="seuil" class="form-control" required="">
                        </div> --}}

                        <div class="form-group">
                            <label>Type <span class="text-danger">*</span></label>
                            <select name="type" class="select2 form-control" required style="width: 100%">
                                <option value="" disabled selected>Selectionnez un type</option>
                                @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->libelle}}</option>
                                @endforeach

                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal add user-->
    </div>

    <div id="updateProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateProglLabel">Modifier une matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('gproduits.update')}}" method="post" id="formEdit">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Code <span class="text-danger">*</span></label>
                            <input type="text" name="code" id="code" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label>Désignation <span class="text-danger">*</span></label>
                            <input type="text" name="nom" id="libelle" class="form-control" required="">
                        </div>

                        {{-- <div class="form-group">
                            <label>Seuil <span class="text-danger">*</span></label>
                            <input type="number" id="seuil" step="any" name="seuil" class="form-control" required="">
                        </div> --}}

                        <div class="form-group">
                            <label>Type <span class="text-danger">*</span></label>
                            <select name="type" id="type" class="select2 form-control" required style="width: 100%">
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="idslug" name="slug">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>


    <script>
        $(document).ready(function(){


            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet utilisateur",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalE(code,id,libel,type,seil){
            //console.log(id,libel, type);
            $("#formEdit")[0].reset();
            $("#code").val(code);
            $("#idslug").val(id);
            $("#libelle").val(libel);
            $("#seuil").val(seil);
            var optionData ='';

            var data ={!! json_encode($types) !!};
            for (var i = 0; i < data.length; i++){
                var select = '';
                if(data[i].id == type){
                    select = 'selected';
                }
                optionData+='<option value="'+data[i].id+'" '+select+'>'+data[i].libelle +'</option>';
            }
            $('#type').html(optionData);

        }

    </script>
@endsection
