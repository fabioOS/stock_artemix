@extends('layouts.app')

@section('page_title')
    Gestions des produits
@stop

@section('content_title')
    Gestions des types produits
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Administrateur</a></li>
        <li class="breadcrumb-item"><a href="#">Produits</a></li>
        <li class="breadcrumb-item active">Type des produits</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-4">
            <div class="card">
                <h4 class="card-title m-4">Ajouter un type</h4>

                <form enctype="multipart/form-data" action="{{route('gproduits.types.store')}}" method="post" autocomplete="off" id="progForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Libelle <span class="text-danger">*</span></label>
                            <input type="text" name="nom" class="form-control" required="">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>
                </form>
            </div>
        </div> <!-- end col -->
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Libelle</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($types as $k=>$type)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$type->libelle}}</td>

                                <td>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <a href="#" class="editerProgramme px-2 text-primary" data-toggle="modal" data-target="#updateProg" onclick="displayModalEdt('{{$type->slug}}','{{$type->libelle}}')" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    <div id="updateProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateProglLabel">Modifier un type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('gproduits.types.update')}}" method="post" id="formEdit">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Libelle <span class="text-danger">*</span></label>
                            <input type="text" id="libelle" name="libelle" class="form-control" required="" value="">
                        </div>

                        <input type="hidden" name="slug" id="idslug">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal add programme-->
    </div>

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>

    <script>
        $(document).ready(function(){

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet utilisateur",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalEdt(id,libel){
            $("#formEdit")[0].reset();
            $("#idslug").val(id);
            $("#libelle").val(libel);
        }

    </script>
@endsection
