@extends('layouts.app')

@section('page_title')
    Configuration du DQE
@stop

@section('content_title')
    Configuration du DQE <br>
    <span class="text-muted">« {{$programme->libelle}} ({{$actifs->libelle}}) »</span>
@stop

@section('content_breadcrumb')
@stop

@section('other_css')
    <!-- DataTables -->
    <link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{asset('assets/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="mb-5">
                @if(Auth::user()->look == 0)
                <a href="#!" class="btn btn-success waves-effect waves-light mr-2" data-toggle="modal" data-target="#addCE">
                    <i class="mdi mdi-plus mr-2"></i> Ajouter un corps d'état
                </a> @endif
                <a href="{{route('gprogramme.show',$programme->slug)}}" class="btn btn-outline-info waves-effect waves-light">
                    <i class="uil uil-arrow-left font-size-15"></i> Retour
                </a>
            </div>
        </div><!-- end col -->
    </div>

    <div class="row gstock">
        <div class="col-lg-12">
            @foreach($corpsetats as $k=>$corps)
                <div id="phase1_accordeon_{{$k}}" class="custom-accordion">
                    <div class="card">
                        <a href="#main_phase_{{$k}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase_{{$k}}">
                            <div class="p-4">
                                <div class="media align-items-center">
                                    <div class="mr-3">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded-circle bg-soft-primary text-primary"> {{$k+1}} </div>
                                        </div>
                                    </div>
                                    <div class="media-body  overflow-hidden">
                                        <h5 class="font-size-16 mb-1">{{Str::upper($corps->libelle)}}
                                            @if(Auth::user()->look == 0)
                                            <a href="#!" onclick='displayModalUpCe("{{$corps->id}}","{{$corps->libelle}}","{{$corps->prix}}","{{$corps->ligne_budget_id}}")' data-toggle="modal" data-target="#updateCE" class="editePhase1DQE px-2 text-primary" data-placement="top" title="" data-original-title="Editer">
                                                <i class="uil uil-pen font-size-18"></i>
                                            </a> @endif
                                        </h5>
                                        <p class="text-muted mb-0">Ligne budgetaire: <span class="text-uppercase text-primary">{{$corps->lignebudget?$corps->lignebudget->libelle : '-'}}</span> ~~ Montant : <span class="text-primary">@price($corps->lignebudget?$corps->lignebudget->montant :0) Fcfa</span></p>
                                        <h6 class="font-size-12 mb-1">@price($corps->prix) Fcfa</h6>
                                    </div>
                                    <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                </div>
                            </div>
                        </a>
                        <!-- action delete update Main Phase -->
                        {{-- <div class="action a-absolute" style="top: 30px; right: 5%;">
                            <li class="list-inline-item">
                                <a href="#!" onclick='displayModalUpCe("{{$corps->id}}","{{$corps->libelle}}","{{$corps->prix}}")' data-toggle="modal" data-target="#updateCE" class="editePhase1DQE px-2 text-primary" data-placement="top" title="" data-original-title="Editer">
                                    <i class="uil uil-pen font-size-18"></i>
                                </a>
                            </li>
                        </div> --}}

                        <div id="main_phase_{{$k}}" class="collapse show" data-parent="#main_phase_{{$k}}">
                        <div class="p-4 border-top">
                            @if(Auth::user()->look == 0)
                            <a href="#" onclick="addMatiere('{{$corps->id}}')" data-toggle="modal" data-target="#addMatiere" class="btn btn-primary btn-sm waves-effect waves-light mb-3"> <i class="mdi mdi-plus mr-2"></i>Ajouter une matière</a>
                            <a href="#" onclick="addLibell('{{$corps->id}}')" data-toggle="modal" data-target="#addLE" class="btn btn-outline-info btn-sm waves-effect waves-light mb-3"> <i class="mdi mdi-plus mr-2"></i>Ajouter un libelle</a>
                            @endif

                            @php $libelcorpsetat = \App\ProgrammeCorpsetatLibelle::where([['corpsetat_id',$corps->id],['actif_id',$corps->actif_id]])->orderBy('id','asc')->get(); @endphp

                            <div id="nv1" class="ml-3">
                                @foreach($libelcorpsetat as $libel)
                                    <div class="sub_sub_2 border-top d-flex align-items-center mb-1" style="background-color: #a8a8a8;color: #fff;">
                                        <div class="tilte mr-5 p-2">{{$libel->libelle}}</div>

                                        @if(Auth::user()->look == 0)
                                        <a href="#" onclick="addMatiere('{{$corps->id}}','{{$libel->id}}')" data-toggle="modal" data-target="#addMatiere" class="btn btn-primary btn-sm waves-effect waves-light  mr-auto"> <i class="mdi mdi-plus mr-2"></i>Ajouter une matière</a>
                                        <a href="#!" onclick="editLibel(`{{$libel->libelle}}`,`{{$libel->id}}`)" data-toggle="modal" data-target="#editLE" class="px-2 text-primary" data-placement="top" title="Editer" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                        @endif
                                    </div>

                                    @php $scorps = \App\ProgrammeSouscorpsetat::where([['corpsetat_id',$corps->id],['actif_id',$corps->actif_id],['corpsetatlibel_id',$libel->id]])->with('matieres')->get(); @endphp
                                    @foreach($scorps as $scorp)
                                        @php $typ = \App\Typeproduit::where('id',$scorp->matieres->type_id)->first(); @endphp
                                        <div class="sub_sub_2 border-top d-flex align-items-center">
                                            <div class="tilte mr-auto p-2">
                                                <small class="text-muted">{{$scorp->prefix}} </small>
                                                {{$scorp->matieres->libelle}}
                                            </div>
                                            <div class="somme p-2">{{$scorp->qte}} <sup>{{$typ->libelle}}</sup></div>
                                            @if(Auth::user()->look == 0)
                                            <div class="sub_sub_2_action p-2">
                                                <ul class="list-inline mb-0">
                                                    <li class="list-inline-item">
                                                        <a href="#!" onclick="updateMatiere('{{$scorp->id}}','{{$typ->libelle}}')" data-toggle="modal" data-target="#updateMatiere" class="pl-3 text-primary" data-placement="top" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <a href="{{route('gprogramme.setting.delete.dqe.matiere',$scorp->id)}}" class="deletematiere text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"><i class="uil uil-trash-alt font-size-18"></i></a>
                                                    </li>
                                                </ul>
                                            </div> @endif
                                        </div>
                                    @endforeach

                                @endforeach
                            </div>

                            <!--for-->
                            @php $scorps = \App\ProgrammeSouscorpsetat::where([['corpsetat_id',$corps->id],['actif_id',$corps->actif_id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                            @foreach($scorps as $scorp)
                                @php $typ = \App\Typeproduit::where('id',$scorp->matieres->type_id)->first(); @endphp
                                <div class="sub_sub_2 border-top d-flex align-items-center">
                                    <div class="tilte mr-auto p-2">
                                        <small class="text-muted">{{$scorp->prefix}} </small><br>
                                        {{$scorp->matieres->libelle}}
                                    </div>
                                    <div class="somme p-2">{{$scorp->qte}} <sup>{{$typ->libelle}}</sup></div>
                                    @if(Auth::user()->look == 0)
                                    <div class="sub_sub_2_action p-2">
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a href="#!" onclick="updateMatiere('{{$scorp->id}}','{{$typ->libelle}}')" data-toggle="modal" data-target="#updateMatiere" class="pl-3 text-primary" data-placement="top" title="" data-original-title="Editer">
                                                    <i class="uil uil-pen font-size-18"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{route('gprogramme.setting.delete.dqe.matiere',$scorp->id)}}" class="deletematiere text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"><i class="uil uil-trash-alt font-size-18"></i></a>
                                            </li>
                                        </ul>
                                    </div> @endif
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div id="addCE" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un corps d'etat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.n-setting.store.dqe.ce')}}" method="post" class="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Ligne budgetaire <span class="text-danger">*</span></label>
                            <select class="form-control" name="ligne" id="corpslign" required>
                                <option selected disabled>Selectionnez un corps d'etat</option>

                               @php
                                    $batiments = $lignebg->where('type',1);
                                    $vrds = $lignebg->where('type',2);
                                @endphp

                                <optgroup label="BATIMENTS">
                                    @foreach($batiments as $line)
                                        <option value="{{$line->id}}"><span class="text-uppercase">{{$line->libelle}}</span></option>
                                    @endforeach
                                </optgroup>

                                <optgroup label="VRD">
                                    @foreach($vrds as $line)
                                        <option value="{{$line->id}}"><span class="text-uppercase">{{$line->libelle}}</span></option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Libellé du corps d'état <span class="text-danger">*</span></label>
                            <input type="text" name="corps_etat" required style="text-transform:uppercase" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">Montant <span class="text-danger">*</span></label>
                            <input type="number" name="montant_corps_etat" required style="text-transform:uppercase" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                        <input type="hidden" name="programme" value="{{$programme->slug}}">
                        <input type="hidden" name="actif" value="{{$actifs->id}}">
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="updateCE" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Modification du corps d'etat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.update.dqe.ce')}}" method="post" class="updatePhase1DQE" id="updatePhase1DQE">
                    <div class="modal-body">@csrf
                        <div class="form-group">
                            <label for="">Ligne budgetaire <span class="text-danger">*</span></label>
                            <select class="form-control" name="ligne" id="corpslignedit" required>
                                <option selected disabled>Selectionnez un corps d'etat</option>
                                @foreach($lignebg as $line)
                                    <option value="{{$line->id}}"><span class="text-uppercase">{{$line->libelle}} @if($line->hors_ds==1)(hors DS)@endif</span></option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Libellé du corps d'état <span class="text-danger">*</span></label>
                            <input type="text" name="libelle" id="libel" required class="form-control" style="text-transform: uppercase">
                        </div>
                        <div class="form-group">
                            <label for="">Montant <span class="text-danger">*</span></label>
                            <input type="number" name="montant_corps_etat" id="mttce" required style="text-transform:uppercase" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <input type="hidden" name="idce" id="idce">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="addMatiere" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter une matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.store.dqe.matiere')}}" method="post" id="addmatierece">
                    <div class="modal-body">@csrf
                        <div class="form-group">
                            <label for="">Matière <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select_plugin select2" style="width: 100%;" name="matiere" id="fmatiere" required>
                                <option selected value="">Selectionnez la matière</option>
                                @foreach($matieres as $matiere)
                                    <option value="{{$matiere->slug}}">{{$matiere->libelle}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Unité de mésure</label>
                            <input type="text" id="unitemesure" readonly disabled class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Quantité <span class="text-danger">*</span></label>
                            <input type="number" min="0" step="any" name="quantite" required="" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                        <input type="hidden" name="idcemat" id="idcemat">
                        <input type="hidden" name="idlibel" id="idlibel">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="updateMatiere" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Modification de la matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.update.dqe.matiere')}}" method="post" id="updatematierece">
                    <div class="modal-body">@csrf
                        <input type="text" name="prefix" id="prefix" class="form-control" style="display: none">


                        <div class="form-group">
                            <label for="">Matière <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select_plugin select2" style="width: 100%;" name="matiere" id="umatiere" required>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Unité de mésure</label>
                            <input type="text" id="unitemesureupdate" readonly disabled class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Quantité <span class="text-danger">*</span></label>
                            <input type="number" min="0" step="any" name="quantite" id="qtes" required="" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                        <input type="hidden" name="idscorps" id="idscorps">
                        <input type="hidden" name="idlibel" id="idupdatelibel">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="addLE" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un libelle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="addlibellece" action="{{route('gprogramme.n-setting.store.dqe.ce.libel')}}" method="post" class="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Libelle <span class="text-danger">*</span></label>
                            <input type="text" name="libelle_corps_etat" required style="text-transform:uppercase" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                        <input type="hidden" name="programme" value="{{$programme->slug}}">
                        <input type="hidden" name="actif" value="{{$actifs->id}}">
                        <input type="hidden" name="ce" id="idcelibell">
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="editLE" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Editer un libelle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="editlibellece" action="{{route('gprogramme.n-setting.update.dqe.ce.libel')}}" method="post" class="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Libelle <span class="text-danger">*</span></label>
                            <input type="text" id="libelle_corps_etat" name="libelle_corps_etat" required style="text-transform:uppercase" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Editer</button>
                        <input type="hidden" name="programme" value="{{$programme->slug}}">
                        <input type="hidden" name="actif" value="{{$actifs->id}}">
                        <input type="hidden" name="idlibel" id="ideditlibel">
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.deletematiere', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet matière",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalUpCe(id,libel,prix,lign){
            $("#updatePhase1DQE")[0].reset();
            $("#idce").val(id);
            $("#libel").val(libel);
            $("#mttce").val(prix);
            $('#corpslignedit').val(lign).change();
        }

        function addMatiere(id,idlibel=0){
            $("#addmatierece")[0].reset();
            $("#idcemat").val(id);
            $("#idlibel").val(idlibel);
        }

        function addLibell(id){
            $("#addlibellece")[0].reset();
            $("#idcelibell").val(id);
        }

        function editLibel(libelle,idlibel){
            $("#editlibellece")[0].reset();
            $("#ideditlibel").val(idlibel);
            $("#libelle_corps_etat").val(libelle);
        }

        function updateMatiere(id,typ,idlibel=0){
            $("#updatematierece")[0].reset();
            var url ="{{ url('get/sous/'.'id'.'/matiere')}}";
            url=url.replace('id',id);
            $.get(url, function (data) {
                var optionData = '';
                var matiere ={!! json_encode($matieres) !!};
                //console.log(matiere);
                //console.log(data)

                for (var i = 0; i < matiere.length; i++){
                    var select = '';
                    if(data.produit_id == matiere[i].id){
                        select = 'selected';
                    }
                    optionData+='<option value="'+matiere[i].slug+'" '+select+'>'+matiere[i].libelle +'</option>';
                }

                $('#umatiere').html(optionData);
                $('#unitemesureupdate').val(typ);
                $('#qtes').val(data.qte);
                $('#idscorps').val(data.id);
                $("#prefix").val(data.prefix);

                var idlibel = data.corpsetatlibel_id;
                if(idlibel =='' || idlibel == null ){
                    idlibel = 0;
                }
                $("#idupdatelibel").val(idlibel);

            });

        }

        function displayModalEdt(id){
            $("#formEdit")[0].reset();

            var url ="{{ url('get/'.'id'.'/programme')}}";
            url=url.replace('id',id);
            $.get(url, function (data) {
                console.log(data);
                $("#idslug").val(id);
                $("#nom").val(data.libelle);
                $("#localisation").val(data.localisation);
                $("#summernote_editor").val(data.description);
            });
        }

        $('#fmatiere').change(function() {
            //alert('dd');
            var idsecteur = $('#fmatiere :selected').val();
            var url ="{{ url('get/produit/'.'id'.'/type')}}";
            url=url.replace('id',idsecteur);
            $.get(url, function (data) {
                //console.log(data)
                $('#unitemesure').val(data);
            });

        });

        $('#umatiere').change(function() {
            //alert('dd');
            var idsecteur = $('#umatiere :selected').val();
            var url ="{{ url('get/produit/'.'id'.'/type')}}";
            url=url.replace('id',idsecteur);
            $.get(url, function (data) {
                $('#unitemesureupdate').val(data);
            });

        });

    </script>
@endsection
