@extends('layouts.app')

@section('page_title')
    Configuration des libelles du DQE
@stop

@section('content_title')
    Configuration des libelles DQE <br>
    <span class="text-muted">« {{$programme->libelle}} ({{$actifs->libelle}}) »</span>
@stop

@section('content_breadcrumb')
@stop

@section('other_css')
    <!-- DataTables -->
    <link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{asset('assets/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .me_ml_2{
            margin-left: 20px;
        }
    </style>

@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="mb-5">
                <a href="#!" class="btn btn-success waves-effect waves-light mr-2" data-toggle="modal" data-target="#addCE">
                    <i class="mdi mdi-plus mr-2"></i> Ajouter un libelle
                </a>
                <a href="{{route('gprogramme.show',$programme->slug)}}" class="btn btn-outline-info waves-effect waves-light">
                    <i class="uil uil-arrow-left font-size-15"></i> Retour
                </a>
            </div>
        </div><!-- end col -->
    </div>

    <div class="row gstock">
        <div class="col-lg-12">
            @foreach($corpsetats as $k=>$corps)

                @if(isset($corps->prefix_id))
                    @if($k-1 != -1)
                        @if($corpsetats[$k-1]->prefix_id != $corps->prefix_id)
                            @php $libellscorps = \App\ProgrammeCorpsetatLibelle::where('id',$corps->prefix_id)->first(); @endphp
                            <div class="card bg-primary text-white-50">
                                <div class="card-body">
                                    <h5 class="mt-0 text-white"> {{$libellscorps->libelle}}</h5>
                                </div>
                            </div>
                        @endif
                    @elseif($k==0)
                        @if($corpsetats[$k]->prefix_id == $corps->prefix_id)
                            @php $libellscorps = \App\ProgrammeCorpsetatLibelle::where('id',$corps->prefix_id)->first(); @endphp
                            <div class="card bg-primary text-white-50">
                                <div class="card-body">
                                    <h5 class="mt-0 text-white"> {{$libellscorps->libelle}}</h5>
                                </div>
                            </div>
                        @endif
                    @endif
                @endif


<!--                <div class="card bg-primary text-white-50">
                    <div class="card-body">
                        <h5 class="mt-0 text-white"> LIBELLE CORPS</h5>
                    </div>
                </div>-->

                <div id="phase1_accordeon_{{$k}}" class="custom-accordion">
                    <div class="card">
                        <a href="#main_phase_{{$k}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase_{{$k}}">
                            <div class="p-4">
                                <div class="media align-items-center">
                                    <div class="mr-3">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded-circle bg-soft-primary text-primary"> {{$k+1}} </div>
                                        </div>
                                    </div>
                                    <div class="media-body  overflow-hidden">
                                        <h5 class="font-size-16 mb-1">{{Str::upper($corps->libelle)}}</h5>
{{--                                        <h6 class="font-size-12 mb-1">@price($corps->prix) Fcfa</h6>--}}
                                    </div>
                                    <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                </div>
                            </div>
                        </a>
                        <!-- action delete update Main Phase -->
                        <div class="action position-absolute" style="top: 30px; right: 5%;">
                            <li class="list-inline-item">

                            </li>
                        </div>

                        <div id="main_phase_{{$k}}" class="collapse show" data-parent="#main_phase_{{$k}}">
                            <div class="p-4 border-top">
                                <a href="#" onclick="addMatiere('{{$corps->id}}')" data-toggle="modal" data-target="#addMatiere" class="btn btn-outline-primary btn-sm waves-effect waves-light mb-3"> <i class="mdi mdi-plus mr-2"></i>Ajouter un prefixe</a>

                                <!--for-->
                                @php
                                    $scorps = \App\ProgrammeSouscorpsetat::where([['corpsetat_id',$corps->id],['actif_id',$corps->actif_id]])->with('matieres')->get();
                                @endphp

                                @foreach($scorps as $i=>$scorp)

                                    @if(isset($scorp->prefix_id))
                                        @if($i-1 != -1)
                                            @if($scorps[$i-1]->prefix_id != $scorp->prefix_id)
                                                @php $libellscorps = \App\ProgrammeSouscorpsetatLibelle::where('id',$scorp->prefix_id)->first(); @endphp
                                                <div class="sub_sub_2 border-top d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                                    <div class="tilte mr-auto p-2">{{$libellscorps->libelle}}</div>
                                                </div>
                                            @endif
                                        @elseif($i==0)
                                            @if($scorps[$i]->prefix_id == $scorp->prefix_id)
                                                @php $libellscorps = \App\ProgrammeSouscorpsetatLibelle::where('id',$scorp->prefix_id)->first(); @endphp
                                                <div class="sub_sub_2 border-top d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                                    <div class="tilte mr-auto p-2">{{$libellscorps->libelle}}</div>
                                                </div>
                                            @endif
                                        @endif
                                    @endif

                                    @php $typ = \App\Typeproduit::where('id',$scorp->matieres->type_id)->first(); @endphp
                                    <div class="sub_sub_2 border-top d-flex align-items-center me_ml_2">
                                    <div class="tilte mr-auto p-2">
    {{--                                    <small class="text-muted">{{$scorp->prefix}} </small><br>--}}
                                        {{$scorp->matieres->libelle}}
                                    </div>
    {{--                                <div class="somme p-2">{{$scorp->qte}} <sup>{{$typ->libelle}}</sup></div>--}}
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div id="addCE" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un corps d'etat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.store.dqe.libelle.ce')}}" method="post" class="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Libellé du corps d'état <span class="text-danger">*</span></label>
                            <input type="text" name="corps_etat" required style="text-transform:uppercase" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">Selectionnez les corps d'etat <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select_plugin select2" multiple style="width: 100%;" name="ce[]" id="ce" required>
                                @foreach($corpsetats as $corps)
                                    <option value="{{$corps->id}}">{{$corps->libelle}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    <input type="hidden" name="programme" value="{{$programme->slug}}">
                    <input type="hidden" name="actif" value="{{$actifs->id}}">
                </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="updateCE" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Modification du corps d'etat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.update.dqe.ce')}}" method="post" class="updatePhase1DQE" id="updatePhase1DQE">
                    <div class="modal-body">@csrf
                        <div class="form-group">
                            <label for="">Libellé du corps d'état <span class="text-danger">*</span></label>
                            <input type="text" name="libelle" id="libel" required class="form-control" style="text-transform: uppercase">
                        </div>
                        <div class="form-group">
                            <label for="">Montant <span class="text-danger">*</span></label>
                            <input type="number" name="montant_corps_etat" id="mttce" required style="text-transform:uppercase" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <input type="hidden" name="idce" id="idce">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="addMatiere" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un prefixe</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.store.dqe.libelle.matiere')}}" method="post" id="addmatierece">
                    <div class="modal-body">@csrf
                        <div class="form-group">
                            <label for="">Prefixe <span class="text-danger">*</span></label>
                            <input type="text" name="prefix" required class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Selectionnez les matières <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select_plugin select2" multiple style="width: 100%;" name="matiere[]" id="fmatierece" required>

                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="programme" value="{{$programme->slug}}">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                        <input type="hidden" name="idcemat" id="idcemat">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="updateMatiere" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Modification de la matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.update.dqe.matiere')}}" method="post" id="updatematierece">
                    <div class="modal-body">@csrf
                        <div class="form-group">
                            <label for="">Prefixe</label>
                            <input type="text" name="prefix" id="prefix" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Matière <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select_plugin select2" style="width: 100%;" name="matiere" id="umatiere" required>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Unité de mésure</label>
                            <input type="text" id="unitemesureupdate" readonly disabled class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Quantité <span class="text-danger">*</span></label>
                            <input type="number" step="any" name="quantite" id="qtes" required="" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                        <input type="hidden" name="idscorps" id="idscorps">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.deletematiere', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet matière",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalUpCe(id,libel,prix){
            $("#updatePhase1DQE")[0].reset();
            $("#idce").val(id);
            $("#libel").val(libel);
            $("#mttce").val(prix);
        }

        function addMatiere(id){
            $("#addmatierece")[0].reset();
            $("#idcemat").val(id);
            var url = '{{route("get.corpsetat.scorpsetat.matiere",":id")}}'
            url=url.replace(':id',id);

            $.get(url, function (data) {
                var optionData = '<option disabled value="">Selectionnez les matières</option>';
                //console.log(data)
                for (var i = 0; i < data.length; i++){
                    var select = '';
                    optionData+='<option value="'+data[i].id+'" '+select+'>'+data[i].matieres.libelle +'</option>';
                }
                $('#fmatierece').html(optionData)

            });

        }

        function updateMatiere(id,typ){
            $("#updatematierece")[0].reset();
            var url ="{{ url('get/sous/'.'id'.'/matiere')}}";
            url=url.replace('id',id);
            $.get(url, function (data) {
                var optionData = '';
                var matiere ={!! json_encode($matieres) !!};
                //console.log(matiere);
                //console.log(data)

                for (var i = 0; i < matiere.length; i++){
                    var select = '';
                    if(data.produit_id == matiere[i].id){
                        select = 'selected';
                    }
                    optionData+='<option value="'+matiere[i].slug+'" '+select+'>'+matiere[i].libelle +'</option>';
                }

                $('#umatiere').html(optionData);
                $('#unitemesureupdate').val(typ);
                $('#qtes').val(data.qte);
                $('#idscorps').val(data.id);
                $("#prefix").val(data.prefix);

            });

        }

        function displayModalEdt(id){
            $("#formEdit")[0].reset();

            var url ="{{ url('get/'.'id'.'/programme')}}";
            url=url.replace('id',id);
            $.get(url, function (data) {
                console.log(data);
                $("#idslug").val(id);
                $("#nom").val(data.libelle);
                $("#localisation").val(data.localisation);
                $("#summernote_editor").val(data.description);
            });
        }

        $('#fmatiere').change(function() {
            //alert('dd');
            var idsecteur = $('#fmatiere :selected').val();
            var url ="{{ url('get/produit/'.'id'.'/type')}}";
            url=url.replace('id',idsecteur);
            $.get(url, function (data) {
                //console.log(data)
                $('#unitemesure').val(data);
            });

        });

        $('#umatiere').change(function() {
            //alert('dd');
            var idsecteur = $('#umatiere :selected').val();
            var url ="{{ url('get/produit/'.'id'.'/type')}}";
            url=url.replace('id',idsecteur);
            $.get(url, function (data) {
                $('#unitemesureupdate').val(data);
            });

        });

    </script>
@endsection
