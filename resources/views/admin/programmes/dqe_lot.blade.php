@extends('layouts.app')

@section('page_title')
    Configuration du DQE par LOT
@stop

@section('content_title')
    Configuration du DQE par LOT <br>
    <span class="text-muted">« LOT {{$lot->lot}} / ILOT {{$lot->ilot}} - {{$actifs->libelle}} » {{$programme->libelle}}</span>
@stop

@section('content_breadcrumb')
@stop

@section('other_css')
    <!-- DataTables -->
    <link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examplloes -->
    <link href="{{asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{asset('assets/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="mb-5">
                <a href="{{route('gprogramme.show',$programme->slug)}}" class="btn btn-outline-info waves-effect waves-light">
                    <i class="uil uil-arrow-left font-size-15"></i> Retour
                </a>
            </div>
        </div><!-- end col -->
    </div>

    <div class="row gstock">
        <div class="col-lg-12">
            @php
            //$lotssme = [311,312,313,314,315,316];
            //$lotssme = [317,318,319,320,321,322];
            //$lotssme = [323,324,325,326,327,328];
            //$lotssme = [329,330,331,332,333,334];
            // $lotssme = [335,336,337,338,339,340];
            // $lotssme = [341,342,343,344,345,346];
            // $lotssme = [347,348,349,350,351,352];
            // $lotssme = [353,354,355,356,357,358];
            // $lotssme = [359,360,361,362,363,364,365];
            // $lotssme = [311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365];
            @endphp
            @foreach($corpsetats as $k=>$corps)
                <div id="phase1_accordeon_{{$k}}" class="custom-accordion">
                    <div class="card">
                        <a href="#main_phase_{{$k}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase_{{$k}}">
                            <div class="p-4">
                                <div class="media align-items-center">
                                    <div class="mr-3">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded-circle bg-soft-primary text-primary"> {{$k+1}} </div>
                                        </div>
                                    </div>
                                    <div class="media-body  overflow-hidden">
                                        <h5 class="font-size-16 mb-1">{{Str::upper($corps->libelle)}}</h5>
                                        <h6 class="font-size-12 mb-1">@price($corps->prix) Fcfa</h6>
                                    </div>
                                    <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                </div>
                            </div>
                        </a>

                        <div id="main_phase_{{$k}}" class="collapse show" data-parent="#main_phase_{{$k}}">
                        <div class="p-4 border-top">
                            <a href="#" onclick="addMatiere('{{$corps->id}}')" data-toggle="modal" data-target="#addMatiere" class="btn btn-primary btn-sm waves-effect waves-light mb-3"> <i class="mdi mdi-plus mr-2"></i>Ajouter une matière</a>

                            @php $libelcorpsetatids = \App\ProgrammeSouscorpsetatLot::where([['corpsetat_id',$corps->id],['lot_id',$lot->id]])->orderBy('id','asc')->pluck('corpsetatlibel_id')->toArray();
                                $libelcorpsetat = \App\ProgrammeCorpsetatLibelle::whereIn('id',$libelcorpsetatids)->orderBy('id','asc')->get();
                            @endphp
                            {{-- @dd($libelcorpsetatids,$libelcorpsetat) --}}
                            <div id="nv1" class="ml-3">
                                @foreach($libelcorpsetat as $libel)
                                    <div class="sub_sub_2 border-top d-flex align-items-center mb-1" style="background-color: #a8a8a8;color: #fff;">
                                        <div class="tilte mr-5 p-2">{{$libel->libelle}}</div>

                                        <a href="#" onclick="addMatiere('{{$corps->id}}','{{$libel->id}}')" data-toggle="modal" data-target="#addMatiere" class="btn btn-primary btn-sm waves-effect waves-light  mr-auto"> <i class="mdi mdi-plus mr-2"></i>Ajouter une matière</a>
                                    </div>

                                    @php $scorps = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$lot->id],['corpsetat_id',$corps->id],['actif_id',$corps->actif_id],['corpsetatlibel_id',$libel->id]])->with('matieres')->get(); @endphp
                                    @foreach($scorps as $scorp)
                                        {{-- JE DOIS METTRE UN LOT A JOUR ICI --}}
                                        {{-- @php
                                            //dump($corps->programme_id);
                                            $lotchecks = \App\ProgrammeLot::where('programme_id',$corps->programme_id)->where('actif_id',$corps->actif_id)->whereIn('lot',$lotssme)->get();
                                            foreach ($lotchecks as $lotcheck) {
                                                //dd($lotcheck);
                                                if($lotcheck){
                                                    $libdt = $libel->id;
                                                    $check = \App\ProgrammeSouscorpsetatLot::where('programme_id',$corps->programme_id)->where('corpsetat_id',$corps->id)
                                                            ->where('actif_id',$corps->actif_id)->where('corpsetatlibel_id',$libdt)
                                                            ->where('produit_id',$scorp->produit_id)->where('lot_id',$lotcheck->id)
                                                            ->first();
                                                    // dd($check);
                                                    if(!$check){
                                                        $scelot = new \App\ProgrammeSouscorpsetatLot();
                                                        $scelot->programme_id = $corps->programme_id;
                                                        $scelot->actif_id = $corps->actif_id;
                                                        $scelot->corpsetat_id = $corps->id;
                                                        $scelot->produit_id = $scorp->produit_id;
                                                        $scelot->qte = $scorp->qte;
                                                        $scelot->corpsetatlibel_id = $libdt;
                                                        $scelot->lot_id = $lotcheck->id;
                                                        $scelot->save();
                                                    }
                                                }
                                            }

                                        @endphp --}}
                                        {{-- JE DOIS METTRE UN LOT A JOUR ICI FIN --}}

                                        @php $typ = \App\Typeproduit::where('id',$scorp->matieres->type_id)->first(); @endphp
                                        <div class="sub_sub_2 border-top d-flex align-items-center">
                                            <div class="tilte mr-auto p-2">
                                                <small class="text-muted">{{$scorp->prefix}} </small>
                                                {{$scorp->matieres->libelle}}
                                            </div>
                                            <div class="somme p-2">{{$scorp->qte}} <sup>{{$typ->libelle}}</sup></div>
                                            <div class="sub_sub_2_action p-2">
                                                <ul class="list-inline mb-0">
                                                    <li class="list-inline-item">
                                                        <a href="#!" onclick="updateMatiere('{{$scorp->id}}','{{$typ->libelle}}')" data-toggle="modal" data-target="#updateMatiere" class="pl-3 text-primary" data-placement="top" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <a href="{{route('gprogramme.setting.delete.dqe.matiere.lot',$scorp->id)}}" class="deletematiere text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"><i class="uil uil-trash-alt font-size-18"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach

                                @endforeach
                            </div>

                            <!--for-->
                            @php $scorps = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$lot->id],['corpsetat_id',$corps->id],['actif_id',$corps->actif_id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                            @foreach($scorps as $scorp)
                                    {{-- @php
                                            //dump($corps->programme_id);
                                            // $lotcheck = \App\ProgrammeLot::where('programme_id',$corps->programme_id)->where('actif_id',$corps->actif_id)->where('lot','14')->first();
                                            $lotchecks = \App\ProgrammeLot::where('programme_id',$corps->programme_id)->where('actif_id',$corps->actif_id)->whereIn('lot',$lotssme)->get();
                                            foreach ($lotchecks as $lotcheck) {
                                                // dd($lotcheck);
                                                if($lotcheck){
                                                    $libdt = $libel->id;
                                                    $check = \App\ProgrammeSouscorpsetatLot::where('programme_id',$corps->programme_id)->where('corpsetat_id',$corps->id)
                                                            ->where('actif_id',$corps->actif_id)->whereNull('corpsetatlibel_id')
                                                            ->where('produit_id',$scorp->produit_id)->where('lot_id',$lotcheck->id)
                                                            ->first();
                                                    // dd($check);
                                                    if(!$check){
                                                        $scelot = new \App\ProgrammeSouscorpsetatLot();
                                                        $scelot->programme_id = $corps->programme_id;
                                                        $scelot->actif_id = $corps->actif_id;
                                                        $scelot->corpsetat_id = $corps->id;
                                                        $scelot->produit_id = $scorp->produit_id;
                                                        $scelot->qte = $scorp->qte;
                                                        $scelot->corpsetatlibel_id = null;
                                                        $scelot->lot_id = $lotcheck->id;
                                                        $scelot->save();
                                                    }
                                                }
                                            }

                                    @endphp --}}

                                @php $typ = \App\Typeproduit::where('id',$scorp->matieres->type_id)->first(); @endphp
                                <div class="sub_sub_2 border-top d-flex align-items-center">
                                    <div class="tilte mr-auto p-2">
                                        <small class="text-muted">{{$scorp->prefix}} </small><br>
                                        {{$scorp->matieres->libelle}}
                                    </div>
                                    <div class="somme p-2">{{$scorp->qte}} <sup>{{$typ->libelle}}</sup></div>
                                    <div class="sub_sub_2_action p-2">
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a href="#!" onclick="updateMatiere('{{$scorp->id}}','{{$typ->libelle}}')" data-toggle="modal" data-target="#updateMatiere" class="pl-3 text-primary" data-placement="top" title="" data-original-title="Editer">
                                                    <i class="uil uil-pen font-size-18"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{route('gprogramme.setting.delete.dqe.matiere.lot',$scorp->id)}}" class="deletematiere text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"><i class="uil uil-trash-alt font-size-18"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>


    <div id="addMatiere" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter une matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.store.dqe.matiere.lot')}}" method="post" id="addmatierece">
                    <div class="modal-body">@csrf
                        <div class="form-group">
                            <label for="">Matière <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select_plugin select2" style="width: 100%;" name="matiere" id="fmatiere" required>
                                <option selected value="">Selectionnez la matière</option>
                                @foreach($matieres as $matiere)
                                    <option value="{{$matiere->slug}}">{{$matiere->libelle}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Unité de mésure</label>
                            <input type="text" id="unitemesure" readonly disabled class="form-control">
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="lotid" value="{{$lot->id}}">
                            <label for="">Quantité <span class="text-danger">*</span></label>
                            <input type="number" min="0" step="any" name="quantite" required="" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                        <input type="hidden" name="idcemat" id="idcemat">
                        <input type="hidden" name="idlibel" id="idlibel">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="updateMatiere" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Modification de la matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.update.dqe.matiere.lot')}}" method="post" id="updatematierece">
                    <div class="modal-body">@csrf
                        <input type="text" name="prefix" id="prefix" class="form-control" style="display: none">


                        <div class="form-group">
                            <label for="">Matière <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select_plugin select2" style="width: 100%;" name="matiere" id="umatiere" required>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="lotid" value="{{$lot->id}}">
                            <label for="">Unité de mésure</label>
                            <input type="text" id="unitemesureupdate" readonly disabled class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Quantité <span class="text-danger">*</span></label>
                            <input type="number" min="0" step="any" name="quantite" id="qtes" required="" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                        <input type="hidden" name="idscorps" id="idscorps">
                        <input type="hidden" name="idlibel" id="idupdatelibel">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.deletematiere', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet matière",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalUpCe(id,libel,prix){
            $("#updatePhase1DQE")[0].reset();
            $("#idce").val(id);
            $("#libel").val(libel);
            $("#mttce").val(prix);
        }

        function addMatiere(id,idlibel=0){
            $("#addmatierece")[0].reset();
            $("#idcemat").val(id);
            $("#idlibel").val(idlibel);
        }

        function updateMatiere(id,typ,idlibel=0){
            $("#updatematierece")[0].reset();
            var url ="{{ url('get/sous/'.'id'.'/matiere/lot')}}";
            url=url.replace('id',id);
            $.get(url, function (data) {
                var optionData = '';
                var matiere ={!! json_encode($matieres) !!};
                //console.log(matiere);
                //console.log(data)

                for (var i = 0; i < matiere.length; i++){
                    var select = '';
                    if(data.produit_id == matiere[i].id){
                        select = 'selected';
                    }
                    optionData+='<option value="'+matiere[i].slug+'" '+select+'>'+matiere[i].libelle +'</option>';
                }

                $('#umatiere').html(optionData);
                $('#unitemesureupdate').val(typ);
                $('#qtes').val(data.qte);
                $('#idscorps').val(data.id);
                $("#prefix").val(data.prefix);

                var idlibel = data.corpsetatlibel_id;
                if(idlibel =='' || idlibel == null ){
                    idlibel = 0;
                }
                $("#idupdatelibel").val(idlibel);

            });

        }

        function displayModalEdt(id){
            $("#formEdit")[0].reset();

            var url ="{{ url('get/'.'id'.'/programme')}}";
            url=url.replace('id',id);
            $.get(url, function (data) {
                console.log(data);
                $("#idslug").val(id);
                $("#nom").val(data.libelle);
                $("#localisation").val(data.localisation);
                $("#summernote_editor").val(data.description);
            });
        }

        $('#fmatiere').change(function() {
            //alert('dd');
            var idsecteur = $('#fmatiere :selected').val();
            var url ="{{ url('get/produit/'.'id'.'/type')}}";
            url=url.replace('id',idsecteur);
            $.get(url, function (data) {
                //console.log(data)
                $('#unitemesure').val(data);
            });

        });

        $('#umatiere').change(function() {
            //alert('dd');
            var idsecteur = $('#umatiere :selected').val();
            var url ="{{ url('get/produit/'.'id'.'/type')}}";
            url=url.replace('id',idsecteur);
            $.get(url, function (data) {
                $('#unitemesureupdate').val(data);
            });

        });

    </script>
@endsection
