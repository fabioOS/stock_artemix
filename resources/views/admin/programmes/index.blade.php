@extends('layouts.app')

@section('page_title')
    Gestions des programmes
@stop

@section('content_title')
    Gestions des programmes
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Administrateur</a></li>
        <li class="breadcrumb-item"><a href="#">Programmes</a></li>
        <li class="breadcrumb-item active">Liste des programmes</li>
    </ol>
@stop

@section('other_css')
    <!-- DataTables -->
    <link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{asset('assets/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if(Auth::user()->look == 0)
                    <div class="mb-5">
                        <a href="#!" class="btn btn-success waves-effect waves-light"
                           data-toggle="modal" data-target="#addProg">
                            <i class="mdi mdi-plus mr-2"></i> Ajouter un programme
                        </a>
                    </div>
                    @endif

                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Nom</th>
                            <th>Localisation</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($programmes as $program)
                            <tr>
                                <td>
                                    <img class="rounded" src="{{isset($program->img) ? asset('assets/uploads/programmes/'.$program->img) : asset('assets/user.jpeg')}}" alt="photo" height="80">
                                </td>
                                <td  style="width: 200px;"><a href="{{route('gprogramme.show',$program->slug)}}">{{$program->libelle}}</a></td>
                                <td  style="width: 200px;">{{$program->localisation}}</td>
                                <td>{!! $program->description !!}</td>
                                <td style="width: 150px;">
                                    <ul class="list-inline mb-0">
                                        @if(Auth::user()->look == 0)
                                        <li class="list-inline-item">
                                            <a href="#" data-toggle="modal" data-target="#updateProg" onclick="displayModalEdt('{{$program->slug}}')"  class="editerProgramme px-2 text-primary" data-placement="top" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                        </li>

                                        <li class="list-inline-item">
                                            <a href="{{route('gprogramme.destroy',$program->slug)}}" class="deleteProgramme px-2 text-danger delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"><i class="uil uil-trash-alt font-size-18"></i></a>
                                        </li>
                                        @endif

                                        <li class="list-inline-item">
                                            <a href="{{route('gprogramme.show',$program->slug)}}" class="px-2 text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Paramètre"><i class="uil uil-cog  font-size-18"></i></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    <!-- modal add user -->
    <div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="addProglLabel">Ajouter un utilisateur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('gprogramme.store')}}" method="post" id="progForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Libelle du programme <span class="text-danger">*</span></label>
                            <input type="text" name="nom" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label>Localisation <span class="text-danger">*</span></label>
                            <input type="text" name="localisation" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label for="">Description <span class="text-danger">*</span></label>
                            <textarea name="description" id="summernote-editor" class="form-control" cols="30" rows="10"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="photo">Photo d'illustration</label>
                            <input type="file" class="form-control-file" name="userfile" id="userfile">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- modal update programme -->
    <div id="updateProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateProglLabel">Modifier un programme</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('gprogramme.update')}}" method="post" id="formEdit">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Libelle du programme <span class="text-danger">*</span></label>
                            <input type="text" name="nom" id="nom" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label>Localisation <span class="text-danger">*</span></label>
                            <input type="text" name="localisation" id="localisation" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label for="">Description <span class="text-danger">*</span></label>
                            <textarea name="description" id="summernote_editor" class="form-control" cols="30" rows="10"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="photo">Photo d'illustration</label>
                            <input type="file" class="form-control-file" name="userfile" id="userfile">
                        </div>
                        <input type="hidden" id="idslug" name="idslug">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>


            </div><!-- /.modal-content -->
        </div><!-- /.modal add programme-->
    </div><!-- /.modal update programme  -->

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>

    <!-- Summernote js -->
    <script src="{{asset('assets/libs/summernote/summernote-bs4.min.js')}}"></script>
    <!-- init js -->
    <script src="{{asset('assets/js/pages/form-editor.init.js')}}"></script>

    <script>
        $(document).ready(function(){

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet programme",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalEdt(id){
            $("#formEdit")[0].reset();

            var url ="{{ url('get/'.'id'.'/programme')}}";
            url=url.replace('id',id);
            $.get(url, function (data) {
                console.log(data);
                $("#idslug").val(id);
                $("#nom").val(data.libelle);
                $("#localisation").val(data.localisation);
                $("#summernote_editor").val(data.description);
            });
        }

    </script>
@endsection
