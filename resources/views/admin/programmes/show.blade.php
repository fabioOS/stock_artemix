@extends('layouts.app')

@section('page_title')
    Paramétrage du programme
@stop

@section('content_title')
    Paramétrage du programme : {{$program->libelle}}
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Administrateur</a></li>
        <li class="breadcrumb-item"><a href="#">Programmes</a></li>
        <li class="breadcrumb-item active">Paramétrage d'un programme</li>
    </ol>
@stop

@section('other_css')
    <!-- DataTables -->
    <link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="row mb-4">
        <div class="col-xl-12 mb-2">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2">
                            <div class="product-detail">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-content position-relative" id="v-pills-tabContent">
                                            <img src="{{asset('assets/uploads/programmes/'.$program->img)}}" alt="" class="img-fluid mx-auto d-block" data-zoom="{{asset('assets/uploads/programmes/'.$program->img)}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7">
                            <div class="">
                                <h4 class="font-size-20 mb-3">{{$program->libelle}}</h4>
                                <div>
                                    <div class="mt-3">
                                        <h5 class="font-size-14 mb-3"><i class="uil uil-location-pin-alt font-size-20 text-primary align-middle mr-2"></i> {{$program->localisation}}</h5>
                                    </div>
                                </div>
                                <p class="mt-4 text-muted">
                                    {!! $program->description !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="card mb-0">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#users" role="tab">
                            <i class="uil uil-user font-size-20"></i>
                            <span class="d-none d-sm-block">Utilisateurs</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#actif" role="tab">
                            <i class="uil uil-home-alt font-size-20"></i>
                            <span class="d-none d-sm-block">Actifs</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lots" role="tab">
                            <i class="uil uil-home font-size-20"></i>
                            <span class="d-none d-sm-block">Lots</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#corpsbudget" role="tab">
                            <i class="uil-right-indent-alt font-size-20"></i>
                            <span class="d-none d-sm-block">Ligne budgetaire</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#dqe" role="tab">
                            <i class="uil uil-list-ul font-size-20"></i>
                            <span class="d-none d-sm-block">DQE Matière</span>
                        </a>
                    </li>
                    @if(Auth::user()->look == 0)
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#ratachmt" role="tab">
                            <i class="uil-money-bill font-size-20"></i>
                            <span class="d-none d-sm-block">Type d'Attachement</span>
                        </a>
                    </li>
                    @endif

                    {{-- <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#ajust" role="tab">
                            <i class="dripicons-toggles font-size-20"></i>
                            <span class="d-none d-sm-block">M.A.J. Corps d'état</span>
                        </a>
                    </li> --}}

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#sprogram" role="tab">
                            <i class="uil-home-alt font-size-20"></i>
                            <span class="d-none d-sm-block">Sous-programme</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab content -->
                <div class="tab-content p-4">
                    <div class="tab-pane active" id="users" role="tabpanel">
                        <div>
                            @if(Auth::user()->look == 0)
                            <div class="my-2">
                                <h5 class="font-size-14 font-weight-bold text-uppercase mb-4">Ajouter les utlisateurs au programme</h5>
                                <form class="repeater" action="{{route('gprogramme.setting.store.users')}}" method="post" id="addType">
                                    @csrf
                                    <div data-repeater-list="type">
                                        <div data-repeater-item class="row">
                                            <div class="form-group col-lg-5">
                                                <label for="name">Utilisateurs <span class="text-danger">*</span></label>
                                                <select class="select2 form-control select2-multiple" name="user[]" multiple="multiple" required>
                                                    @foreach($users as $user)
                                                        <option value="{{$user->id}}">{{$user->name}} - {{$user->role->libelle}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="name">Secteur </label>
                                                <select class="select2 form-control select2-multiple" name="secteur[]" multiple="multiple" required>
                                                    <option value="1">Batiments</option>
                                                    <option value="2">VRD</option>
                                                </select>
                                            </div>
                                            <input type="hidden" name="id_programme" value="{{$program->slug}}">

                                            <div class="col-lg-4 mt-2 align-self-center">
                                                <button data-repeater-delete type="submit" class="btn btn-success">
                                                    <i class="uil uil-check font-size-10"></i> Autoriser
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            @endif
                            <div>
                                <h5 class="font-size-14 font-weight-bold text-uppercase my-4">Liste des utilisateurs autorisés</h5>

                                <div class="table-responsive users">
                                    <table class="table table-bordered table-striped table-nowrap table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th scope="col">Nom & Prénom</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Rôle</th>
                                            <th scope="col">Secteurs</th>
                                            @if(Auth::user()->look == 0)
                                            <th scope="col" style="width: 120px;">Action</th> @endif
                                        </tr>
                                        </thead>
                                        <tbody id="listTypeBien">
                                        @foreach($program->users->sortBy('role_id') as $userpivot)
                                        {{-- @dump($userpivot->programmPivot) --}}
                                        <tr>
                                            <td>{{$userpivot->name}}</td>
                                            <td>{{$userpivot->email}}</td>
                                            @php $role = \App\Role::where('id',$userpivot->role_id)->first(); @endphp
                                            <td>{{$role ? $role->libelle : ''}}</td>
                                            <td>
                                                @if($userpivot->programmPivot[0]->batiment)<span class="badge badge-primary">Batiments</span> @endif
                                                @if($userpivot->programmPivot[0]->vrd)<span class="badge badge-dark">VRD</span> @endif
                                            </td>
                                            @if(Auth::user()->look == 0)
                                            <td>
                                                <ul class="list-inline mb-0">
                                                    <li class="list-inline-item">
                                                        <a href="{{route('gprogramme.setting.delete.users',[$userpivot->id,$program->id])}}" class="px-2 text-danger delete" data-toggle="tooltip" data-placement="top" title="Supprimer">
                                                            <i class="uil uil-trash-alt font-size-18"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="actif" role="tabpanel">
                        <div>
                            @if(Auth::user()->look == 0)
                            <div class="my-2">
                                <h5 class="font-size-14 font-weight-bold text-uppercase mb-4">Ajouter un type de bien</h5>
                                <form class="repeater" action="{{route('gprogramme.setting.store.actifs')}}" method="post" id="addType" enctype="multipart/form-data">
                                    @csrf
                                    <div data-repeater-list="type">
                                        <div data-repeater-item class="row">
                                            <div  class="form-group col-lg-5">
                                                <label for="name">Libellé du type de bien <span class="required">*</span></label>
                                                <input type="text" name="libelle_type_lot" required class="form-control"/>
                                            </div>

                                            <div class="form-group col-lg-3">
                                                <label for="name">Secteur </label>
                                                <select class="form-control select_plugin" style="width: 100%;" name="secteur" required>
                                                    <option value="1">Batiments</option>
                                                    <option value="2">VRD</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4 mt-2 align-self-center">
                                                <button type="submit" class="btn btn-success">
                                                    <input type="hidden" name="id_programme" value="{{$program->slug}}">
                                                    <i class="uil uil-check font-size-10"></i> Ajouter
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            @endif
                            <div>
                                <h5 class="font-size-14 font-weight-bold text-uppercase my-4">Liste des types de bien ajoutés</h5>

                                <div class="table-responsive tactifs">
                                    <table class="table table-bordered table-striped table-nowrap table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th scope="col">Libellé du bien</th>
                                            <th scope="col">Type</th>
                                            @if(Auth::user()->look == 0)
                                            <th scope="col" style="width: 120px;">Action</th> @endif
                                        </tr>
                                        </thead>
                                        <tbody id="listTypeBien">
                                        @foreach($program->actifs as $actif)
                                            <tr>
                                                <td>{{$actif->libelle}}</td>
                                                <td>
                                                    @if($actif->type==1)
                                                        <span class="badge badge-primary">BATIMENTS</span>
                                                    @else
                                                        <span class="badge badge-dark">VRD</span>
                                                    @endif
                                                </td>
                                                @if(Auth::user()->look == 0)
                                                <td>
                                                    <ul class="list-inline mb-0">
                                                        <li class="list-inline-item">
                                                            <a href="#!" onclick="displayModalEdt('{{$actif->id}}','{{$actif->libelle}}','{{$actif->type}}')" class="px-2 text-primary" data-toggle="modal" data-target="#updateType" data-placement="top" title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a href="{{route('gprogramme.setting.delete.actifs',$actif->id)}}" class="px-2 text-danger delete" data-toggle="tooltip" data-placement="top" title="Supprimer">
                                                                <i class="uil uil-trash-alt font-size-18"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td> @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="lots" role="tabpanel">
                        @if(Auth::user()->look == 0)
                        <div class="row">
                            <div class="col-lg-6 my-2">
                                <h5 class="font-size-14 font-weight-bold text-uppercase mb-2">Ajout des lots par actifs</h5>
                                <hr class="mb-3">
                                <form action="{{route('gprogramme.setting.store.lots')}}" enctype="multipart/form-data" method="POST" id="uploadLot">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label class="control-label">Type de bien <span class="text-danger">*</span></label>
                                            <br>
                                            <select class="form-control select_plugin" style="width: 100%;" name="id_type_lot" required>
                                                <option selected value="">Selectionnez un actif</option>
                                                @foreach($program->actifs as $actif)
                                                    <option value="{{$actif->id}}">{{$actif->libelle}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label for="userfile">Chargez le fichier (Format XLSX accepté) <span class="text-danger">*</span></label>
                                            <input type="file" class="form-control" name="userfile" id="userfile" accept=".XLSX" required>
                                            <p><i>Télécharge le modèle d'importation <a href="{{asset('assets/lotmodel.xlsx')}}" download="model d'importation">ici</a></i></p>
                                            <p class="help-block">Taille maximum : 100 Mo</p>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id_programme" value="{{$program->slug}}">
                                    <button type="submit" class="btn btn-primary">Ajouter</button>
                                </form>
                            </div>
                        </div>
                        <hr>
                        @endif

                        <div>
                            <h5 class="font-size-14 font-weight-bold text-uppercase my-4">Liste des lots ajoutés</h5>

                            <div class="table-responsive">
                                <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                    <tr>
                                        <th scope="col">Lot</th>
                                        <th scope="col">Ilot</th>
                                        <th scope="col">Type de bien (Actif)</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Bloc</th>
                                        <th scope="col">Tranche</th>
                                        @if(Auth::user()->look == 0)
                                        <th scope="col">Action</th> @endif
                                    </tr>
                                    </thead>
                                    <tbody id="listLot">
                                    @foreach($program->lots as $lot)
                                        @php
                                            $typ= \App\ProgrammeActif::where('id',$lot->actif_id)->first();
                                            $dqe = \App\ProgrammeSouscorpsetatLot::where('lot_id',$lot->id)->first();
                                        @endphp
                                    <tr>
                                        <td>{{$lot->lot}}</td>
                                        <td>{{$lot->ilot}}</td>
                                        <td>{{$typ ? $typ->libelle : '-'}}</td>
                                        <td>
                                            @if($typ)
                                                @if($typ->type==1)
                                                    <span class="badge badge-primary">BATIMENTS</span>
                                                @else
                                                    <span class="badge badge-dark">VRD</span>
                                                @endif
                                            @endif
                                        </td>
                                        <td>{{$lot->bloc}}</td>
                                        <td>{{$lot->tranche}}</td>
                                        @if(Auth::user()->look == 0)
                                        <td>
                                            @isset($dqe->id)
                                            <a href="{{route('gprogramme.n-setting.dqe.lot',$lot->id)}}" class="btn btn-primary btn-sm waves-effect waves-light">
                                                <i class="uil uil-cog font-size-10"></i> Configurer le DQE
                                            </a>
                                            @endisset
                                        </td> @endif
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="dqe" role="tabpanel">
                        <div>
                            <div class="py-2">
                                <h5 class="font-size-14 font-weight-bold text-uppercase mb-5">Configuration du DQE par type de lot</h5>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($program->actifs as $k=>$actif)
                                    <li class="nav-item">
                                        <a class="nav-link {{$k==0 ? 'active' : ''}}" data-toggle="tab" href="#navtabs{{$actif->id}}" role="tab">
                                            <span class="d-none d-sm-block">{{$actif->libelle}}</span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content p-3 text-muted">
                                    @foreach($program->actifs as $k=>$actif)
                                        @php $corps = \App\ProgrammeCorpsetat::where('programme_id',$program->id)->where('actif_id',$actif->id)->get(); @endphp

                                        <div class="tab-pane {{$k==0 ? 'active' : ''}}" id="navtabs{{$actif->id}}" role="tabpanel">
                                        <div class="mb-5">
                                            <a href="{{route('gprogramme.n-setting.create.dqe',[$program->slug,$actif->id])}}" class="btn btn-success waves-effect waves-light">
                                                <i class="uil uil-cog font-size-20"></i> {{Auth::user()->look==0 ? "Configurer le DQE" : "Voir le DQE"}}  {{$actif->libelle}}
                                            </a>
                                            {{-- @if(count($corps)>0)
                                            <a href="{{route('gprogramme.setting.create.dqe.libelle',[$program->slug,$actif->id])}}" class="btn btn-primary waves-effect waves-light">
                                                <i class="uil uil-cog font-size-20"></i> Configurer les libelle DQE et prefix {{$actif->libelle}}
                                            </a>
                                            @endif --}}
                                        </div>
                                        <!-- Item -->
                                        <div id="phase1_accordeon1" class="custom-accordion">

                                            {{-- LISTE DES CORPS D'ETAT --}}
                                            @foreach($corps as $j=>$corp)
                                            <div class="card border border-primary">
                                                <a href="#main_phase{{$j}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$j}}">
                                                    <div class="p-4">
                                                        <div class="media align-items-center">
                                                            <div class="mr-3">
                                                                <div class="avatar-xs">
                                                                    <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                                                </div>
                                                            </div>
                                                            <div class="media-body  overflow-hidden">
                                                                <h5 class="font-size-16 mb-1">{{Str::upper($corp->libelle)}}</h5>
                                                                <h6 class="font-size-12 mb-1">@price($corp->prix) Fcfa</h6>
                                                            </div>
                                                            <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                                        </div>
                                                    </div>
                                                </a>

                                                <div id="main_phase{{$j}}" class="collapse" data-parent="#main_phase1">
                                                    <div class="p-4 border-top">
                                                        @php $libelcorpsetat = \App\ProgrammeCorpsetatLibelle::where('programme_id',$program->id)->where([['corpsetat_id',$corp->id],['actif_id',$actif->id]])->orderBy('id','asc')->get(); @endphp

                                                        {{-- LES LIBELLES ET PRODUIT DU CORPS D'ETAT --}}
                                                        @foreach ($libelcorpsetat as $libel)
                                                            <div class="sub_sub_2 border-top d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                                                <div class="tilte mr-auto p-2">{{$libel->libelle}}</div>
                                                            </div>

                                                            @php $scorps = \App\ProgrammeSouscorpsetat::where('programme_id',$program->id)->where([['corpsetat_id',$corp->id],['actif_id',$actif->id],['corpsetatlibel_id',$libel->id]])->with('matieres')->get(); @endphp
                                                            @foreach($scorps as $scorp)
                                                                @php $typ = \App\Typeproduit::where('id',$scorp->matieres->type_id)->first(); @endphp
                                                                <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                    <div class="tilte mr-auto p-2">
                                                                        <small class="text-muted">{{$scorp->prefix}} </small>
                                                                        {{$scorp->matieres->libelle}}
                                                                    </div>
                                                                    <div class="somme p-2">{{$scorp->qte}} <sup>{{$typ->libelle}}</sup></div>
                                                                </div>
                                                            @endforeach

                                                        @endforeach

                                                    {{-- LES PRODUITS DU CORPS D'ETAT UNIQUEMENT --}}
                                                        @php $scorps = \App\ProgrammeSouscorpsetat::where([['corpsetat_id',$corp->id],['actif_id',$actif->id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                                                        @foreach($scorps as $scorp)
                                                            @php $typ = \App\Typeproduit::where('id',$scorp->matieres->type_id)->first(); @endphp
                                                            <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                <div class="tilte mr-auto p-2">
                                                                    <small class="text-muted">{{$scorp->prefix}} </small><br>
                                                                    {{$scorp->matieres->libelle}}
                                                                </div>
                                                                <div class="somme p-2">{{$scorp->qte}} <sup>{{$typ->libelle}}</sup></div>
                                                            </div>
                                                        @endforeach

                                                    </div><!-- fin border-top -->
                                                </div> <!-- fin main_phase -->
                                            </div>
                                            @endforeach

                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                            </div>

                        </div>
                    </div>

                    @if(1==2)
                    <div class="tab-pane" id="ajust" role="tabpanel">
                        <div>
                            <h5 class="font-size-14 font-weight-bold text-uppercase mb-5">Mettre à jour des corps d'état</h5>
                            <div class="py-2 col-md-12">
                                <form action="{{route('gprogramme.setting.maj.corpetats')}}" method="post" id="updateTypeBien" autocomplete="off">
                                    @csrf
                                    <input type="hidden" name="program" id="programme_id" value="{{$program->id}}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Sélectionner les corsp d'états par actif <span class="required text-danger">*</span></label><br>
                                            <select link="{{route('gprogramme.setting.get_list_corpetat')}}" id="select_actif" style="width: 100%;" class="select2- form-control" name="actifs" required>
                                                <option value=""></option>
                                                @foreach($program->actifs as $k=>$actif)
                                                    <option value="{{$actif->id}}">{{$actif->libelle}}</option>
                                                    {{-- <optgroup label="----------------- {{$actif->libelle}} -----------------">
                                                        @php $corps = \App\ProgrammeCorpsetat::where('programme_id',$program->id)->where('actif_id',$actif->id)->get(); @endphp
                                                        @foreach($corps as $j=>$corp)
                                                            <option value="{{$actif->id}}**{{$corp->id}}">{{$actif->libelle}} --- {{Str::upper($corp->libelle)}}</option>
                                                        @endforeach
                                                    </optgroup> --}}
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Sélectionner les corsp d'états <span class="required text-danger">*</span></label><br>
                                            <select id="select_corpsetat" style="width: 100%;" class="select2 form-control select2-multiple" name="corps[]" multiple="multiple" required >
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Valider</button>
                                    </div>
                                </form>
                                <!-- Nav tabs -->

                            </div>

                        </div>
                    </div>
                    @endif

                    <div class="tab-pane" id="sprogram" role="tabpanel">
                        <div>
                            @if(Auth::user()->look == 0)
                            <div class="my-2">
                                <h5 class="font-size-14 font-weight-bold text-uppercase mb-4">Ajouter un sous-programme</h5>
                                <form class="repeater" action="{{route('gprogramme.n-setting.store.sprogramme')}}" method="post" id="addType" enctype="multipart/form-data">
                                    @csrf
                                    <div data-repeater-list="type">
                                        <div data-repeater-item class="row">
                                            <div  class="form-group col-lg-8">
                                                <label for="name">Libellé <span class="required">*</span></label>
                                                <input type="text" name="libelle" required class="form-control"/>
                                            </div>

                                            <div class="col-lg-4 mt-2 align-self-center">
                                                <button type="submit" class="btn btn-success">
                                                    <input type="hidden" name="id_programme" value="{{$program->slug}}">
                                                    <i class="uil uil-check font-size-10"></i> Ajouter
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            @endif

                            <div>
                                <h5 class="font-size-14 font-weight-bold text-uppercase my-4">Liste des sous-programmes ajoutés</h5>

                                <div class="table-responsive sprograms">
                                    <table class="table table-bordered table-striped table-nowrap table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th scope="col">Libellé</th>
                                            @if(Auth::user()->look == 0)
                                            <th scope="col" style="width: 120px;">Action</th> @endif
                                        </tr>
                                        </thead>
                                        <tbody id="listTypeBien">
                                        @foreach($program->sousprogramme as $sous)
                                            <tr>
                                                <td>{{$sous->libelle}}</td>
                                                @if(Auth::user()->look == 0)
                                                <td>
                                                    <ul class="list-inline mb-0">
                                                        <li class="list-inline-item">
                                                            <a href="#!" onclick="displayModalEdtSous('{{$sous->id}}','{{$sous->libelle}}')" class="px-2 text-primary" data-toggle="modal" data-target="#updateTypeSous" data-placement="top" title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a href="{{route('gprogramme.n-setting.delete.sprogramme',$sous->id)}}" class="px-2 text-danger delete" data-toggle="tooltip" data-placement="top" title="Supprimer">
                                                                <i class="uil uil-trash-alt font-size-18"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td> @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(Auth::user()->look == 0)
                    <div class="tab-pane" id="ratachmt" role="tabpanel">
                        <div>
                            <h5 class="font-size-14 font-weight-bold text-uppercase mb-5">Choisissez le type d'attachement.</h5>
                            <div class="py-2 col-md-12">

                                <div class="alert alert-border alert-border-{{$program->type_rattachement==2 ? 'secondary':'success'}} alert-dismissible" role="alert">
                                    @if($program->type_rattachement==1)<i class="uil uil-check font-size-16 text-success mr-2"></i>@endif
                                    Attachement par DQE
                                    @if($program->type_rattachement==2)
                                        <a href="{{route('gprogramme.n-setting.edit.type-rattachement',[$program->id,1])}}" class="btn btn-primary btn-sm float-right waves-effect waves-light">
                                            <i class="fa fa-check-square"></i> Activé
                                        </a>
                                    @endif
                                </div>

                                <div class="alert alert-border alert-border-{{$program->type_rattachement==1 ? 'secondary':'success'}} alert-dismissible" role="alert">
                                    @if($program->type_rattachement==2)<i class="uil uil-check font-size-16 text-success mr-2"></i>@endif
                                    Attachement par CONTRAT
                                    @if($program->type_rattachement==1)
                                        <a href="{{route('gprogramme.n-setting.edit.type-rattachement',[$program->id,2])}}" class="btn btn-primary btn-sm float-right waves-effect waves-light">
                                            <i class="fa fa-check-square"></i> Activé
                                        </a>
                                    @endif
                                </div>
                                <!-- Nav tabs -->

                            </div>

                        </div>
                    </div>
                    @endif

                    <div class="tab-pane" id="corpsbudget" role="tabpanel">
                        <div>
                            @if(Auth::user()->look == 0)
                            <div class="my-2">
                                <h5 class="font-size-14 font-weight-bold text-uppercase mb-4">Ajouter une ligne budgetaire</h5>
                                <form class="repeater" action="{{route('gprogramme.setting.lignes.store')}}" method="post" id="addLigne">
                                    @csrf
                                    <div data-repeater-list="type">
                                        <div data-repeater-item class="row">
                                            <div  class="form-group col-lg-3">
                                                <label for="name">Corps d'etat <span class="text-danger">*</span></label>
                                                <input type="text" name="corps" class="form-control" required class="form-control"/>
                                            </div>

                                            <div  class="form-group col-lg-3">
                                                <label for="name">Montant <span class="text-danger">*</span></label>
                                                <input type="text" name="montant" required class="form-control" onkeyup="formatNumber(this)"/>
                                            </div>

                                            <div class="form-group col-lg-2">
                                                <label for="name">Type </label>
                                                <select class="form-control select_plugin" style="width: 100%;" name="type" required>
                                                    <option value="1">Batiments</option>
                                                    <option value="2">VRD</option>
                                                </select>
                                            </div>

                                            <div  class="form-group col-lg-2">
                                                <label for="name">HORS DS <span class="text-danger">*</span></label>
                                                <select class="form-control" name="hors_ds" required>
                                                    <option value="non" selected>Non</option>
                                                    <option value="oui">Oui</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 mt-2 align-self-center">
                                                <button type="submit" class="btn btn-success">
                                                    <input type="hidden" name="id_programme" value="{{$program->slug}}">
                                                    <i class="uil uil-check font-size-10"></i> Ajouter
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr> @endif

                            <div>
                                <h5 class="font-size-14 font-weight-bold text-uppercase my-4">Liste des sous-programmes ajoutés</h5>

                                <div class="table-responsive budglign">
                                    <table class="table table-bordered table-striped table-nowrap table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th scope="col">Ligne budgetaire</th>
                                            <th scope="col">Montant</th>
                                            <th scope="col">Type corps d'etat</th>
                                            @if(Auth::user()->look == 0)
                                            <th scope="col" style="width: 120px;">Action</th> @endif
                                        </tr>
                                        </thead>
                                        <tbody id="listTypeBien">
                                        @foreach($program->ligneBudgetaires as $kk=>$sous)
                                            <tr>
                                                <td>{{$sous->libelle}} @if($sous->hors_ds==1)<sup class="text-danger small">hors DS</sup>@endif</td>
                                                <td>@price($sous->montant)</td>
                                                <td>
                                                    @if($sous->type==1)
                                                        <span class="badge badge-primary">BATIMENTS</span>
                                                    @else
                                                        <span class="badge badge-dark">VRD</span>
                                                    @endif
                                                </td>
                                                @if(Auth::user()->look == 0)
                                                <td>
                                                    <ul class="list-inline mb-0">
                                                        <li class="list-inline-item">
                                                            <a href="#!" onclick="displayModalEdtLign('{{$kk}}')" class="px-2 text-primary" data-toggle="modal" data-target="#updateLignBgd" data-placement="top" title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a href="{{route('gprogramme.setting.lignes.delt',$sous->id)}}" class="px-2 text-danger delete" data-toggle="tooltip" data-placement="top" title="Supprimer">
                                                                <i class="uil uil-trash-alt font-size-18"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td> @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="updateType" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateTypeLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateTypelLabel">Modifier un type de bien</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.update.actifs')}}" method="post" id="updateTypeBien" autocomplete="off">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Libellé du type de bien <span class="text-danger">*</span></label>
                            <input type="text" name="libelle_type_lot" id="updatelibeltype" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="name">Secteur <span class="text-danger">*</span></label>
                            <select class="form-control select_plugin" style="width: 100%;" name="secteur" id="updatetypeActif" required>
                                <option value="1">Batiments</option>
                                <option value="2">VRD</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" name="slug" id="updateslugtype" value="">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="updateTypeSous" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateTypeLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateTypelLabel">Modifier le sous-programme</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.n-setting.update.sprogramme')}}" method="post" id="updateTypeSouspgram" autocomplete="off">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Libellé <span class="required">*</span></label>
                            <input type="text" name="libelle" id="updatelibelsprogram" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="slug" id="updateslugsprogram">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="updateLignBgd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateLignBgd" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateTypelLabel">Modifier la ligne budgetaire </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('gprogramme.setting.lignes.update')}}" method="post" id="updateLignBgdForm" autocomplete="off">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Ligne budgetaire <span class="text-danger">*</span></label>
                            <input type="text" name="corps" id="corpslign" class="form-control" required/>
                        </div>

                        <div class="form-group">
                            <label for="name">Type </label>
                            <select class="form-control select_plugin" style="width: 100%;" id="typelign" name="type" required>
                                <option value="1">Batiments</option>
                                <option value="2">VRD</option>
                            </select>
                        </div>

                        <div class="form-group ">
                            <label for="name">Montant <span class="text-danger">*</span></label>
                            <input type="text" name="montant" id="montantlign" required class="form-control" onkeyup="formatNumber(this)"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="idligne" id="slugsprogramlign">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>

    <!-- Summernote js -->
    <script src="{{asset('assets/libs/summernote/summernote-bs4.min.js')}}"></script>
    <!-- init js -->
    <script src="{{asset('assets/js/pages/form-editor.init.js')}}"></script>

    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>


    <script>
        $(document).ready(function(){

            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet programme",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

            $('.users').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet utilisateur",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

            $('.tactifs').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet actif",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

            $('.sprograms').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet sous programme",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

            $('.budglign').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cette ligne budgetaire",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

            $('#select_actif').change(function(){
                $('#select_corpsetat').val('').html('');
                if($(this).val() != ''){
                    var id = $(this).val();
                    $.ajax({
                        url: $(this).attr('link'),
                        type: "GET",
                        //data: {actif_id:id,_token: '{{csrf_token()}}', programme_id:$('#programme_id').val()},
                        data: 'actif_id='+id+'&programme_id='+$('#programme_id').val(),
                        beforeSend: function(){
                            $('#select_corpetat').fadeTo('slow',0.2);
                        },
                        error: function (data) {
                            console.log(data);
                        },
                        success: function(data) {
                            $('#select_corpsetat').html(data).fadeTo('slow',1);
                        }
                    });
                }
                else
                    $('#select_corpsetat').val('').html('');
            });

        });

        function displayModalEdt(id,libel,type){
            $("#updateTypeBien")[0].reset();
            $("#updateslugtype").val(id);
            $("#updatelibeltype").val(libel);
            $("#updatetypeActif").val(type);
        }

        function displayModalEdtSous(id,libel){
            $("#updateTypeSouspgram")[0].reset();
            $("#updateslugsprogram").val(id);
            $("#updatelibelsprogram").val(libel);
        }

        function displayModalEdtLign(id){
            $("#updateLignBgdForm")[0].reset();
            var dattas = @json($program->ligneBudgetaires);
            //Select dattas key id
            var dattas = dattas[id];
            // console.log(dattas);
            $("#slugsprogramlign").val(dattas.id);
            $("#typelign").val(dattas.type);
            //make selected option for corps
            $('#corpslign').val(dattas.libelle);
            $("#montantlign").val(dattas.montant);
        }

        //Formatage du prix de vente
        function formatNumber(input) {
            input.value = input.value.replace(/[^\d]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }

    </script>
@endsection
