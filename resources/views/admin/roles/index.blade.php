@extends('layouts.app')

@section('page_title')
    Gestions des rôles
@stop

@section('content_title')
    Gestions des rôles
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Administrateur</a></li>
        <li class="breadcrumb-item"><a href="#">Utilisateurs</a></li>
        <li class="breadcrumb-item active">Gestions des rôles</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Libelle</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($roles as $k=>$role)
                            <tr>
                                <td  style="width: 200px;">{{$k+1}}</td>
                                <td  style="width: 200px;">{{$role->libelle}}</td>
                                <td style="width: 150px;">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <a href="#" class="editerProgramme px-2 text-primary" data-toggle="modal" data-target="#updateProg" onclick="displayModalEdt('{{$role->slug}}','{{$role->libelle}}')" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div>


    <!-- modal update programme -->
    <div id="updateProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateProglLabel">Modifier un rôle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('gusers.roles.update')}}" method="post" id="formEdit">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Libelle <span class="text-danger">*</span></label>
                            <input type="text" id="libelle" name="libelle" class="form-control" required="" value="">
                        </div>

                        <input type="hidden" name="slug" id="idslug">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal add programme-->
    </div><!-- /.modal update programme  -->

@endsection


@section('other_js')
    <script>
        $(document).ready(function(){

        });

        function displayModalEdt(id,libel){
            $("#formEdit")[0].reset();
            $("#idslug").val(id);
            $("#libelle").val(libel);
        }

    </script>
@endsection
