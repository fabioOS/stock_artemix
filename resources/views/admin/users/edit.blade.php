@extends('layouts.app')

@section('page_title')
    Gestions des utilisateurs
@stop

@section('content_title')
    Edité un utilisateur
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Administrateur</a></li>
        <li class="breadcrumb-item"><a href="#">Utilisateurs</a></li>
        <li class="breadcrumb-item active">Edité un utilisateur</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link active" data-toggle="tab" href="#navpills-home" role="tab" aria-selected="true">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">Profil</span>
                            </a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" data-toggle="tab" href="#navpills-profile" role="tab" aria-selected="false">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">Mot de passe</span>
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content p-3 text-muted">
                        <div class="tab-pane active" id="navpills-home" role="tabpanel">
                            <form enctype="multipart/form-data" action="{{route('gusers.update.profil')}}" method="post" id="progForm">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Nom&Prenom <span class="text-danger">*</span></label>
                                        <input type="text" name="nom" class="form-control" value="{{$user->name}}" required="">
                                    </div>

                                    <div class="form-group">
                                        <label>Rôle <span class="text-danger">*</span></label>
                                        <select name="role" id="role" class="form-control" required>
                                            <option value="" disabled selected>Selectionnez un rôle</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->slug}}" {{$user->role_id == $role->id ? 'selected' : ''}}>{{$role->libelle}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="hidden" name="slug" id="slug" value="{{$user->id}}">
                                    <div class="form-group">
                                        <label for="photo">Photo d'illustration</label>
                                        <input type="file" class="form-control-file" name="userfile" id="userfile">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="navpills-profile" role="tabpanel">
                            <form action="{{route('gusers.update.pass')}}" method="post" id="progForm">
                                @csrf
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label for="">Mot de passe <span class="text-danger">*</span></label>
                                        <input type="password" name="password" class="form-control" required>
                                    </div>
                                    <input type="hidden" name="slug" id="slug" value="{{$user->id}}">

                                    <div class="form-group">
                                        <label for="">Confirmer le mot de passe <span class="text-danger">*</span></label>
                                        <input type="password" name="password_confirm" class="form-control" required>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection


@section('other_js')
@endsection
