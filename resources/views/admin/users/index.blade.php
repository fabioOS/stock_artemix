@extends('layouts.app')

@section('page_title')
    Gestions des utilisateurs
@stop

@section('content_title')
    Gestions des utilisateurs
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Administrateur</a></li>
        <li class="breadcrumb-item"><a href="#">Utilisateurs</a></li>
        <li class="breadcrumb-item active">Liste des utilisateurs</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/css/bootstrap4-toggle.min.css')}}" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
        .toggle.ios .toggle-handle { border-radius: 20rem; }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="mb-5">
                        <a href="#!" class="btn btn-success waves-effect waves-light"
                           data-toggle="modal" data-target="#addProg">
                            <i class="mdi mdi-plus mr-2"></i> Ajouter un utilisateur
                        </a>
                    </div>

                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Nom & Prenom</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Actif</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                <img class="rounded" src="{{isset($user->img) ? asset('assets/uploads/users/'.$user->img) : asset('assets/user.jpeg')}}" alt="photo" height="80">
                            </td>
                            <td  style="width: 200px;">{{$user->name}}</td>
                            <td  style="width: 200px;">{{$user->email}}</td>
                            <td  style="width: 200px;">{{$user->role ? $user->role->libelle : ''}}</td>
                            <td>
                                <input type="checkbox" onchange="update_status(this)"  value="{{$user->id}}" {{$user->actif== 1 ? 'checked' : ''}} data-toggle="toggle" data-on="Actif" data-off="Inactif" data-onstyle="success" data-offstyle="danger" data-size="sm" data-style="ios">
                            </td>
                            <td style="width: 150px;">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="{{route('gusers.edit',$user->id)}}" class="editerProgramme px-2 text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                    </li>

                                    <li class="list-inline-item">
                                        <a href="{{route('gusers.destroy',$user->id)}}" class="deleteProgramme px-2 text-danger delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"><i class="uil uil-trash-alt font-size-18"></i></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    <!-- modal add user -->
    <div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="addProglLabel">Ajouter un utilisateur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('gusers.store')}}" method="post" id="progForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nom&Prenom <span class="text-danger">*</span></label>
                            <input type="text" name="nom" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label>Email <span class="text-danger">*</span></label>
                            <input type="email" name="email" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label for="">Mot de passe <span class="text-danger">*</span></label>
                            <input type="password" name="password" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Rôle <span class="text-danger">*</span></label>
                            <select name="role" id="role" class="form-control" required>
                                <option value="" disabled selected>Selectionnez un rôle</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->slug}}">{{$role->libelle}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="photo">Photo d'illustration</label>
                            <input type="file" class="form-control-file" name="userfile" id="userfile">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal add user-->
    </div><!-- /.modal add user  -->

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap4-toggle.min.js')}}"></script>

    <script>
        $(document).ready(function(){

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet utilisateur",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function update_status(el){
            if(el.checked){var status = 1;}else{var status = 0;}
            var token =$("meta[property='csrf-token']").attr('content');
            $.post("{{route('gusers.status')}}", {_token:token, id:el.value, status:status}, function(data){
                if(data == 1){
                    swal("Succès!","L'utilisateur mis à jour avec succès","success");
                }else{
                    swal("Erreur!","Un problème est survenu","error");
                }
            });
        }

    </script>
@endsection
