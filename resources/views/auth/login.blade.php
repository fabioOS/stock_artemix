<!doctype html>
<html lang="fr_FR">
<head>
    <meta charset="utf-8" />
    <title>Connexion </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}">

    <!-- Bootstrap Css -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{asset('assets/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />
    <!-- Scripts -->
    <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>

</head>

<body class="authentication-bg" style="background: #f2f2f2 url({{asset('assets/images/dots.png')}}) top center;background-size: contain; background-repeat: no-repeat;  background-attachment: fixed;">

<div class="main_content">
    <div class="account-pages my-3">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <a class="mb-3 mt-2 d-block auth-logo">
                            <img src="{{asset('assets/images/logo-dark.png')}}" alt="" height="40" class="logo logo-dark">
                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="40" class="logo logo-light">
                        </a>
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div>
                        @include('includes.messages')
                    </div>

                    <div class="card login_form">
                        <div class="card-body p-4">
                            <div class="text-center mt-2">
                                <h5 class="text-primary">Bienvenue !</h5>
                                <p>Connectez-vous pour continuer.</p>
                            </div>
                            <div class="p-2 mt-4">

                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"  placeholder="Entrez votre email" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">

                                        <label for="password">Mot de passe</label>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Entrez votre mot de passe">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="remember">
                                            {{ __('Se souvenir de moi') }}
                                        </label>
                                        {{-- <div class="float-right">
                                            @if (Route::has('password.request'))
                                                <a class="" href="{{ route('password.request') }}">
                                                    {{ __('Mot de passe oublié ?') }}
                                                </a>
                                            @endif
                                        </div> --}}
                                    </div>

                                    <div class="mt-5 text-center">
                                        <button type="submit" class="btn btn-primary btn-rounded w-sm waves-effect waves-light col-sm-6">
                                            {{ __('Connexion') }}
                                        </button>
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>

                    <div class="mt-4 text-center">

                        <p>© <script>document.write('2022')</script> {{env('APP_NAME')}}. Créer avec <i class="mdi mdi-heart text-danger"></i> par <a href="https://datarium.tech" target="_blank" class="text-reset">DATARIUM</a></p>
                    </div>

                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
</div>


<!-- JAVASCRIPT -->
</body>
</html>
