<div class="row">
    <div class="col-lg-12">
        @if (count($data) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Fichier</th>
                            <th>Demandeur</th>
                            <th>Sous-traitant</th>
                            <th>Contrat</th>
                            <th>Statut</th>
                            @if($data[0]->status ==4)
                            <th>Motif</th>
                            @endif
                            <th>Date de demande</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="#" class="text-dark font-weight-bold">{{$dt->ref}}</a> </td>
                                <td>
                                    @if(isset($dt->file) and $dt->file != "")
                                        <a href="{{asset('assets/uploads/bondemandes/'.$dt->file)}}" target="_blank" download>bon_de_demande.pdf</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{mb_strtoupper($dt->soustraitant->nom)}}</td>
                                <td>{{$dt->contrat ? $dt->contrat->libelle : '-'}}</td>
                                <td>
                                    @if ($dt->status == 1)
                                        <span class="badge badge-pill badge-warning">En attente</span>
                                    @endif
                                    @if ($dt->status == 2)
                                        <span class="badge badge-pill badge-info">Validé</span>
                                    @endif
                                    @if ($dt->status == 3)
                                        <span class="badge badge-pill badge-success">Livré</span>
                                    @endif
                                    @if ($dt->status == 4)
                                        <span class="badge badge-pill badge-danger">Rejété</span>
                                    @endif
                                </td>
                                @if($dt->status ==4)
                                <td>{{str_limit($dt->motif, 15)}}
                                    <button type="button" data-toggle="modal" onclick='displayModalEdt("{{$dt->motif}}")' data-target="#rejectbon" class="btn btn-primary btn-sm" title="Voir plus"> <i class="fas fa-comment-dots"></i></button>
                                </td>
                                @endif
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @if($dt->status == 4)
                                        @if (Auth::user()->role_id == 2) <!-- SI Chefchantier -->
                                            {{-- <a href="{{route($route_prefix.'.edit', encrypt($dt->id))}}" class="btn btn-info btn-sm waves-effect waves-light" title="Rééditer"><i class="fa fa-pencil-alt"></i></a> --}}
                                            {{-- <button type="button" class="btn btn-danger btn-sm waves-effect waves-light mr-1" data-toggle="modal" data-target="#delPrevent-{{$dt->id}}" title="Supprimer"> <i class="fas fa-trash"></i> </button> --}}
                                        @endif
                                    @endif

                                    @if($dt->status==3)
                                    <a href="{{route('pdfbsvalider',$dt->ref)}}" title="Télécharger le bon" class="btn btn-success btn-sm waves-effect waves-light">
                                        <i class="bx bxs-file-pdf"></i>
                                    </a>
                                    @endif

                                    <a href="{{route($route_prefix.'.details', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                    <!--  modal ITEM -->

                                    <div id="delPrevent-{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0">Suppression</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-center-">
                                                    <h5>Voulez-vous supprimer <br> le bon de demande N° <b class="text-warning">{{$dt->ref}}</b> &nbsp; ?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{route($route_prefix.'.destroy', encrypt($dt->id))}}" class="btn btn-primary btn-rounded waves-effect waves-light">
                                                        &nbsp; &nbsp; Oui &nbsp; &nbsp;
                                                    </a> &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <button type="button" class="btn btn-rounded" data-dismiss="modal">Annuler</button>
                                                </div>

                                            </div><!-- /.modal-content -->
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucun bon de demande pour l'instant !</h5>
        @endif
    </div>
</div>
<!-- end row -->



<div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Motif du rejet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="momotif"></p>
            </div>
        </div><!-- /.modal-content -->
    </div>
</div>

<script>
    function displayModalEdt(id){
        $('#momotif').html(id);
    }
</script>
