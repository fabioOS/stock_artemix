@extends('chef_projet.layouts.app')

@section('page_title')
    Créer un bon de sortie en stock
@stop

@section('content_title')
    Créer un bon sortie en stock <br>&nbsp; <br> Etape 1/3
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Créer un bon de sortir</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('cphome.bc.step2create')}}" class="" method="get" id="DemForm">
                        @csrf
                        <div class="form-group">
                            <label for=""> Sélectionner un sous traitant <span class="text-danger">*</span></label>
                            <select name="soustraitant_id" id="soustraitant_id" onchange="selectSt()" class="form-control select_plugin" style="min-width: 300px;" required ref="{{route('chefchantier.select_lots_soustraitant')}}">
                                <option selected disabled>Liste des sous-traitants</option>
                                @foreach ($soutraitants as $sous)
                                    <option value="{{$sous->id}}">{{$sous->nom}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div id="loader" class="dispayNone">
                            <div class="spinner-border text-primary m-1" role="loader">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>

                        <div id="restform">
                            <div class="form-group">
                                <label for=""> Sélectionner le contrat <span class="text-danger">*</span></label>
                                <select name="contrat" id="contrat" class="form-control" style="min-width: 300px;" required >
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Suivant &nbsp; <i class="fas fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- end row -->

@endsection


@section('other_js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>


<script>
    $('#soustraitant_id').val('');

    function selectSt(){
        $("#contrat").empty();
        $("#restform").hide();
        $("#loader").show();

        var idst = $('#soustraitant_id :selected').val();
        var url = '{{route("cphome.get.contrat.st",":id")}}'
        url=url.replace(':id',idst);

        $.get(url, function (data) {
            var optionData = '<option disabled value="">Selectionnez un contrat</option>';
            // console.log(data)
            if(data.length != 0){
                for (var i = 0; i < data.length; i++){
                    optionData+='<option value="'+data[i].id+'">'+data[i].libelle +' ('+ data[i].slug +')</option>';
                }
                $('#contrat').html(optionData)
                $("#restform").show();
                $("#loader").hide();
            }else{
                swal("Oups!", "Aucun contrat disponible !");
                $("#loader").hide();
                return false;
            }

        });
    }
</script>
@endsection
