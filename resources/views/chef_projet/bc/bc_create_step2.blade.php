@extends('chef_projet.layouts.app')

@section('page_title')
    Créer un bon de sortie en stock
@stop

@section('content_title')
    Créer un bon de sortie en stock <br>&nbsp; <br> Etape 2/3

@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Créer un bon de sortie</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{route('cphome.bc.create')}}" class="btn btn-info btn-sm">
                <i class="fas fa-angle-double-left"></i> &nbsp; Retour
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('cphome.bc.step3create')}}" method="get" id="DemForm">
                        @csrf
                        <div class="form-group niveau-1">
                            <label for="">Sous traitant : </label>
                            <br>
                            <select class="form-control select_plugin" disabled style="width: 100%;">
                                <option value="{{$soustraitant->id}}">{{$soustraitant->nom}}</option>
                            </select>
                            <input type="hidden" name="soustraitant_id" value="{{$soustraitant->id}}">
                        </div>

                        <div class="form-group niveau-1">
                            <label for="">Contrat : </label>
                            <br>
                            <select class="form-control select_plugin" disabled style="width: 100%;">
                                <option value="{{$soustraitant->itemcontrat->id}}">{{$soustraitant->itemcontrat->libelle}} ({{$soustraitant->itemcontrat->slug}})</option>
                            </select>
                            <input type="hidden" name="contrat_id" value="{{$soustraitant->itemcontrat->id}}">
                        </div>

                        <div class="form-group niveau-2">
                            <label for="">Sélectionner les villas <span class="required" style="color: :red;">*</span></label>
                            <br>
                            <select required multiple="multiple" size="10" name="lot_id[]" id="lot_id" title="duallistbox_demo1[]">
                                @if(isset($soustraitant->lots))
                                    @foreach($soustraitant->lots as $lots)
                                        <option value="{{$lots->lot_id}}">Villa {{$lots->lot->lot}}</option>
                                    @endforeach
                                @else
                                    <option> Aucune villa n'est disponible pour cet prestataire</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="ml-4 btn btn-primary waves-effect waves-light pull-right">Suivant  &nbsp; <i class="fas fa-angle-double-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
         </div>

    </div>
    <!-- end row -->

@endsection


@section('other_css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
@endsection

@section('other_js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/slim.js')}}"></script>
<script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>

<script>
    var demo1 = $('select[name="lot_id[]"]').bootstrapDualListbox({
        nonSelectedListLabel: 'Liste des villas',
        selectedListLabel: 'Villas Sélectionnés',
        preserveSelectionOnMove: 'moved',
        moveAllLabel: 'Tout déplacer',
        removeAllLabel: 'Enlever tout'
    });
</script>

@endsection
