@extends('chef_projet.layouts.app')

@section('page_title')
    Bons de sortie rejetés
@stop

@section('content_title')
    Bons de sortie rejetés
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bons de sortie</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="gstock table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>Num</th>
                        <th>N°Référence</th>
                        <th>Fichier</th>
                        <th>Chef chantier</th>
                        <th>Sous-traitant</th>
                        <th>Contrat</th>
                        <th>Statut</th>
                        <th>Motif</th>
                        <th>Date</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bons as $k=>$bon)
                        <tr class="font-size-16">
                            <td>{{$k+1}}</td>
                            <td><a class="text-dark font-weight-bold">{{Str::upper($bon->ref)}}</a> </td>
                            <td>@if($bon->file)<a href="{{asset('assets/uploads/bondemandes/'.$bon->file)}}" target="_blank" download>bon_de_demande.pdf</a>@else - @endif</td>
                            <td>{{$bon->user->name}}</td>
                            <td>{{mb_strtoupper($bon->soustraitant->nom)}}</td>
                            <td>{{$bon->contrat? $bon->contrat->libelle : ''}}</td>
                            <td>
                                <span class="badge badge-pill badge-danger">Rejété</span>
                            </td>
                            <td>{{str_limit($bon->motif, 15)}}
                                <button type="button" data-toggle="modal" onclick='displayModalEdt("{{$bon->motif}}")' data-target="#rejectbon" class="btn btn-primary btn-sm" title="Voir plus"> <i class="fa fa-eye"></i></button>
                            </td>
                            <td>{{$bon->created_at->format('d/m/Y H:i')}}</td>
                            <td>
                                {{-- <a href="{{route('cphome.bc.edit', encrypt($bon->ref))}}" class="btn btn-success btn-sm waves-effect waves-light" title="Rééditer"><i class="fa fa-pencil-alt"></i></a> --}}

                                <a href="{{route('cphome.bc.show',$bon->ref)}}" title="Détail" class="btn btn-primary btn-sm waves-effect waves-light">
                                    <i class="uil uil-eye"></i>
                                </a>
                                {{-- <a href="{{route('cphome.bc.destroy',encrypt($bon->ref))}}" title="Supprimer" class="btn btn-danger btn-sm waves-effect waves-light delete"><i class="fas fa-trash"></i> Supprimer</a> --}}
                                {{-- <a href="{{route('cphome.bc.pdf',$bon->ref)}}" class="btn btn-success waves-effect waves-light">
                                    <i class="bx bxs-file-pdf mr-2"></i> PDF
                                </a> --}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>


    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Mofif du rejet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="momotif"></p>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
@endsection


@section('other_js')
    <script>
        $(document).ready(function(){
            // $(".select2").select2();
            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet bon",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalEdt(id){
            $('#momotif').html(id);

        }


    </script>
@endsection
