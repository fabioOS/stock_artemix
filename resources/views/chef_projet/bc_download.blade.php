<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

    .card {
        display: inline-block;
        width: 100%;
        margin-bottom: 2px;
        box-shadow: 0 2px 4px rgba(15,34,58,.12);
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid #f6f6f6;
        border-radius: .25rem;
        font-size: 12px;
    }

    .card-body {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-height: 1px;
        /*padding: 1.25rem;*/
    }

</style>
<body>
<div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="col">
                    <img src="{{asset('assets/images/logo-dark.png')}}" data-holder-rendered="true" height="30px" />

                </div>
            </td>

        </tr>
    </table>
    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                @if ($data->status == 3)
                    <p style="font-size: 18px;font-weight: bold">BON DE SORTIE</p>
                @else
                    <p style="font-size: 18px;font-weight: bold">BON DE SORTIE</p>
                @endif
            </td>
        </tr>

    </table>
    <br>

    <table class="table table-bordered" width="100%" style="background-color: #b5b1b1;background-clip: border-box;border: 0 solid #b5b1b1;border-radius: .25rem;padding: 10px;">
        <tbody>
        <tr>
            <td><h5 class="font-size-16" style="text-transform: uppercase">Référence</h5></td>
            <td><h5 class="font-size-16" style="text-transform: uppercase">Demandeur</h5></td>
            <td><h5 class="font-size-16" style="text-transform: uppercase">Sous-traitant</h5></td>
            <td><h5 class="font-size-16" style="text-transform: uppercase">Contrat</h5></td>
            <td><h5 class="font-size-16" style="text-transform: uppercase">Date de demande</h5></td>
            <td><h5 class="font-size-16" style="text-transform: uppercase">Status</h5></td>
        </tr>
        <tr>
            <td>{{strtoupper($data->ref)}}</td>
            <td><p>{{strtoupper($data->user->name)}}</p></td>
            <td>{{strtoupper($data->soustraitant->nom)}}<br></td>
            <td>{{$data->contrat ? $data->contrat->libelle : ''}}</td>
            <td>{{$data->created_at->format('d/m/Y')}}</td>
            @if ($data->status == 1)
               <td style="color: #ec7015">En attente</td>
            @elseif($data->status == 2)
                <td style="color: #ec7015">Validé</td>
            @elseif($data->status == 3)
                <td style="color: #ec7015">Livré</td>
            @else
            <!--$data->status == 4-->
                <td style="color: #ec7015">Rejété</td>
            @endif
        </tr>
        </tbody>
    </table>

    <br>
    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="">
                <p style="font-size: 18px;font-weight: bold">DETAIL DE LA DEMANDE</p>
            </td>
        </tr>

    </table>
    <br>

    @php
        $row_lot_id = \App\Bon_demande_item::where('bon_demande_id', $data->id)->selectRaw('DISTINCT(lot_id) AS lot_id')->pluck('lot_id');
        $programme_id = Session::get('program');
        $vars = [
            'programme_id' => $data->programme_id,
            'lot_id' => $row_lot_id,
            'soustraitant_id' => $data->soustraitant_id
        ];

        $lots = \App\ProgrammeLot::whereIn('id',$row_lot_id)->with('actifs')->get();
    @endphp


    @foreach($lots as $k=>$item)
        @php
            $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$item->actif_id)->where('soustraitant_id',$data->soustraitant_id)->with('corpslots')->get();
        @endphp

        <div class="card">
            <div class="card-body">
                <p style="width: 25%;border: 1px solid #b5b1b1;padding: 10px;text-transform: uppercase;font-size: 14px;">LOT {{$item->lot}} <br> <span>{{$item->actifs ?$item->actifs->libelle:''}}</span></p>
            </div>
        </div>
        @foreach($corpsetats as $j=>$corps)
        <div class="card">
            <div class="card-body">
                <h4> {{$j+1}} -  {{$corps->corpslots->libelle}} </h4>

                <div style="padding: 5px">
                    @php
                        $libelcorpsetatids = \App\ProgrammeSouscorpsetatLot::where([['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['lot_id',$item->id]])->orderBy('id','asc')->pluck('corpsetatlibel_id')->toArray();
                        $souscorpetat = \App\ProgrammeCorpsetatLibelle::whereIn('id',$libelcorpsetatids)->orderBy('id','asc')->get();
                    @endphp

                    @foreach ($souscorpetat as $i=>$sscorp)
                            <div class="tilte mr-auto p-2"><i>{{$sscorp->libelle}}</i></div>
                        <br>

                        @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',$sscorp->id]])->with('matieres')->get(); @endphp
                        @foreach ($scorpsmtt as $scorps)
                            @php
                                $qte_demande = \App\Bon_demande::where('bon_demandes.id', $data->id)
                                ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                ->where('bon_demande_items.lot_id', $item->id)
                                ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                ->pluck('qte')
                                ->first();
                            @endphp

                            @if(isset($qte_demande) and $qte_demande != 0)
                                @php
                                    $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                    $total_livre = \App\Bon_demande::where('status', 3)
                                                ->where('programme_id', Session::get('program'))
                                                // ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                ->where('bon_demande_items.lot_id', $item->id)
                                                ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                ->pluck('total_livre')
                                                ->first();
                                    $restant = $scorps->qte - $total_livre;
                                    $stock_dispo = \App\Stock::where('produit_id',$scorps->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                                @endphp

                                <div class="tilte mr-auto p-2 text-body">
                                    <b>{{$scorps->matieres->libelle}}</b>
                                    <div style="margin-left: 10px">
                                        <span class="text-default font-size-13">
                                            <i>
                                            Stock déjà livré: <b> {{$total_livre != '' ? $total_livre : 0}} </b> / {{$sscorp->qte}} <sup>{{$unite}}</sup><br>
                                            Stock actuel du programme: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} <sup>{{$unite}}</sup></b> <br>
                                            @if ($data->status == 3)
                                            <b>Qté livrée : {{$qte_demande}}</b> <br>
                                            @else
                                            <b>Qté demandée : {{$qte_demande}}</b> <br>
                                            @endif
                                            </i>
                                        </span>
                                        <br>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endforeach

                    {{-- Ceux qui nont pas de LIBELLE --}}
                    @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                    @foreach ($scorpsmtt as $scorps)
                        @php
                            $qte_demande = \App\Bon_demande::where('bon_demandes.id', $data->id)
                            ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                            ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                            ->where('bon_demande_items.produit_id', $scorps->produit_id)
                            ->where('bon_demande_items.lot_id', $item->id)
                            ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                            ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                            ->pluck('qte')
                            ->first();
                        @endphp

                        @if(isset($qte_demande) and $qte_demande != 0)
                            @php
                                $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                $total_livre = \App\Bon_demande::where('status', 3)
                                            ->where('programme_id', Session::get('program'))
                                            // ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                            ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                            ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                            ->where('bon_demande_items.lot_id', $item->id)
                                            ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                            ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                            ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                            ->pluck('total_livre')
                                            ->first();
                                $restant = $scorps->qte - $total_livre;
                                $stock_dispo = \App\Stock::where('produit_id',$scorps->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                            @endphp

                            <div class="tilte mr-auto p-2 text-body">
                                <b>{{$scorps->matieres->libelle}}</b>
                                <div style="margin-left: 10px">
                                    <br>
                                    <span class="text-default font-size-13">
                                        <i>
                                        Stock déjà livré: <b> {{$total_livre != '' ? $total_livre : 0}} </b> / {{$scorps->qte}} <sup>{{$unite}}</sup><br>
                                        Stock actuel du programme: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} <sup>{{$unite}}</sup></b> <br>
                                        @if ($data->status == 3)
                                        <b>Qté livrée : {{$qte_demande}}</b> <br>
                                        @else
                                        <b>Qté demandée : {{$qte_demande}}</b> <br>
                                        @endif
                                        </i>
                                    </span>
                                    <br>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach


    <br>
    @endforeach


    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="">
                <p style="font-size: 18px;font-weight: bold">RESUME PAR MATIERE</p>
            </td>
        </tr>

    </table>
    <br>
    <br>

    @php
        $distinct_matiers = \App\Bon_demande_item::where('bon_demande_id', $data->id)
        ->join('produits', 'produits.id', '=', 'bon_demande_items.produit_id')
        ->selectRaw('DISTINCT(produits.id) AS id')
        ->pluck('id');
        //dd($distinct_matiers);
    @endphp

    <table cellspacing="0" cellpadding="0" width="100%">
        @foreach ($distinct_matiers as $matiere_id)
            @php
                $libpdt = \App\Produit::where('id',$matiere_id)->with('type')->first();
                $mat_qte_total = \App\Bon_demande_item::where('bon_demande_id', $data->id)->where('produit_id', $matiere_id)->sum('qte');
                //dump($mat_qte_total);
            @endphp
            <tr style="line-height: 2">
                <td><b>{{$libpdt->libelle}}</b> : {{$mat_qte_total ? $mat_qte_total : '0'}} <sup>{{$libpdt->type->libelle}}</sup></td>
            </tr>
        @endforeach

    </table>

@if($data->status == 4)
    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="">
                <p style="font-size: 18px;font-weight: bold">MOTIF DE REJET</p>
            </td>
        </tr>
    </table>
    <br>
    <p>{{$data->motif}}</p>
    <br>
@endif

</div>
<footer>
</footer>
</body>
</html>
