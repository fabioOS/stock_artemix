@extends('chef_projet.layouts.app')

@section('page_title')
    Rééditer le bon de demance N° {{$bon->ref}}
@stop

@section('content_title')
    Rééditer le bon de demance N° {{$bon->ref}}
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Rééditer le bon de demance N° {{$bon->ref}}</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{URL::previous()}}" class="btn btn-info btn-sm">
                <i class="fas fa-angle-double-left"></i> &nbsp; Retour
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form enctype="multipart/form-data" action="{{route('cphome.bc.update', encrypt($bon->id))}}" method="post" id="DemForm">
                        @csrf
                        <div class="form-group niveau-1">
                            <label for="">Sous traitant : </label>
                            <br>
                            <select class="form-control select_plugin" disabled style="width: 100%;">
                                <option value="{{$soustraitant->id}}">{{$soustraitant->nom}}</option>
                            </select>
                            <input type="hidden" name="soustraitant_id" value="{{$soustraitant->id}}">
                        </div>

                        <div class="form-group niveau-1">
                            <label for="">Contrat : </label>
                            <br>
                            <select class="form-control select_plugin" disabled style="width: 100%;">
                                <option value="{{$soustraitant->itemcontrat->id}}">{{$soustraitant->itemcontrat->libelle}} ({{$soustraitant->itemcontrat->slug}})</option>
                            </select>
                            <input type="hidden" name="contrat_id" value="{{$soustraitant->itemcontrat->id}}">
                        </div>


                        <hr>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    @foreach($soustraitant->lots as $k=>$lot)
                                        @php $item = \App\ProgrammeLot::where('id',$lot->lot_id)->first();@endphp
                                        <a class="nav-link mb-2 border {{$k==0 ? 'active' :''}}" id="v-pills-{{$k}}-tab" data-toggle="pill" href="#v-pills-{{$k}}" role="tab" aria-controls="v-pills-{{$k}}" aria-selected="{{$k==0 ? 'true' : 'false'}}">
                                            Lot {{$item->lot}}
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-9">

                            <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabCentent">
                                @foreach($soustraitant->lots as $k=>$lot)
                                    @php
                                        $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$lot->lot->actif_id)->where('soustraitant_id',$lot->soustraitant_id)->where('etatdel',1)->with('corpslots')->get();
                                        //dd($corpsetats);
                                    @endphp
                                        <div class="tab-pane fade {{$k==0 ? 'active show' :''}}" id="v-pills-{{$k}}" role="tabpanel" aria-labelledby="v-pills-{{$k}}-tab">
                                            <input type="hidden" value="{{$lot->lot_id}}" name="lot_id[]">
                                            @if(count($corpsetats)>0)
                                                @foreach($corpsetats as $j=>$corps)

                                                    <input type="hidden" name="corps_id[{{$lot->lot_id}}][]" value="{{$corps->corpsetats_id}}">

                                                    <div class="card border border-primary">
                                                        <a href="#main_phase{{$corps->id}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$corps->id}}">
                                                            <div class="p-4">
                                                                <div class="media align-items-center">
                                                                    <div class="mr-3">
                                                                        <div class="avatar-xs">
                                                                            <div class="avatar-title rounded-circle bg-soft-primary text-primary">0{{$j+1}}</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="media-body  overflow-hidden">
                                                                        <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                                    </div>
                                                                    <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div id="main_phase{{$corps->id}}" class="collapse show" data-parent="#main_phase{{$corps->id}}">
                                                            <div class="p-4 border-top">
                                                                <!-- item sous phase  -->
                                                                @php
                                                                    $souscorpetat = \App\ProgrammeSouscorpsetatLot::where('corpsetat_id',$corps->corpsetats_id)->where('lot_id',$lot->lot_id)->where('programme_id',Session::get('program'))->with('matieres')->with('actif')->with('corpsetat')->get();
                                                                    //dd($souscorpetat,$lot->lot_id);
                                                                @endphp
                                                                @foreach ($souscorpetat as $sscorp)
                                                                    <div class="sub_sub_2 border-top d-flex align-items-center">

                                                                        <input type="hidden" name="sous_corps_id[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="{{$sscorp->id}}">

                                                                        <div class="tilte mr-auto p-2 text-body">
                                                                            {{$sscorp->matieres->libelle}}
                                                                            <input type="hidden" name="produit_id[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="{{$sscorp->matieres->id}}">
                                                                            <div class="form-label mr-auto p-2 text-warning">
                                                                                <span class="text-default font-size-13">
                                                                                    @php
                                                                                        $unite = mb_strtoupper(\App\Produit::where('produits.id', $sscorp->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                                        //$total_livre = \App\Bon_demande::where('status', 3)->where('programme_id', Session::get('program'))->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')->where('bon_demande_items.produit_id', $sscorp->produit_id)->where('bon_demande_items.lot_id', $lot->lot_id)->selectRaw('SUM(bon_demande_items.qte) AS total_livre')->pluck('total_livre')->first();
                                                                                        $total_livre = \App\Bon_demande::where('status', 3)
                                                                                                    ->where('programme_id', Session::get('program'))
                                                                                                    //->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                                    ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                                    ->where('bon_demande_items.produit_id', $sscorp->produit_id)
                                                                                                    ->where('bon_demande_items.lot_id', $lot->lot_id)
                                                                                                    ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                                    ->where('bon_demande_items.scorpsetats_id', $sscorp->id)
                                                                                                    ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                                                                    ->pluck('total_livre')
                                                                                                    ->first();
                                                                                        $restant = $sscorp->qte - $total_livre;
                                                                                        $stock_dispo = \App\Stock::where('produit_id',$sscorp->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                                                                                    @endphp

                                                                                    Livré: <b> {{$total_livre != '' ? $total_livre : 0}}
                                                                                         <sup>{{$unite}}</sup></b> &nbsp; | &nbsp;

                                                                                    Restant: <b> {{$restant}}
                                                                                        <sup>{{$unite}}</sup></b> (<span class="text-warning">@php if($sscorp->qte > 0) echo round((($total_livre*100)/$sscorp->qte), 2) ; else echo 0; @endphp %</span>) &nbsp; | &nbsp;

                                                                                    Stock disponible: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} </b>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="somme p-2">
                                                                            @if($restant > 0)
                                                                                @php
                                                                                    $min_val = '0.001';
                                                                                    $current_qte = \App\Bon_demande::where('bon_demandes.id', $bon->id)->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')->where('bon_demande_items.produit_id', $sscorp->produit_id)->where('bon_demande_items.lot_id', $lot->lot_id)->selectRaw('bon_demande_items.qte AS current_qte')->pluck('current_qte')->first();
                                                                                    if ($current_qte != null && (float)$current_qte > 0) {
                                                                                        $min_val = '0';
                                                                                    }
                                                                                @endphp
                                                                                @if ($stock_dispo == 0)
                                                                                    <input type="number" step="any" value="{{$current_qte}}" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" min="{{$min_val}}" max="{{$restant}}" placeholder="Quantité">
                                                                                @else
                                                                                    @if ($stock_dispo >= $restant)
                                                                                        <input type="number" step="any" value="{{$current_qte}}" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" min="{{$min_val}}" max="{{$restant}}" placeholder="Quantité">
                                                                                    @else
                                                                                        <input type="number" step="any" value="{{$current_qte}}" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" min="{{$min_val}}" max="{{$stock_dispo}}" placeholder="Quantité">
                                                                                    @endif
                                                                                @endif
                                                                            @else
                                                                                <input type="number" step="any" name="qte_sscorps[]" placeholder="Quantité" disabled>
                                                                                <input type="hidden" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="0">
                                                                            @endif
                                                                            <input type="hidden" name="item_id[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="{{\App\Bon_demande_item::where('bon_demande_id', $bon->id)->where('produit_id', $sscorp->produit_id)->where('lot_id', $lot->lot_id)->pluck('id')->first()}}">
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div><!-- fin border-top -->
                                                        </div> <!-- fin main_phase -->
                                                    </div><!-- card -->
                                                @endforeach
                                            @else
                                            Ce prestataire n'a aucune matière pour ce lot {{$lot->lot->lot}} !
                                            @endif

                                        </div>

                                @endforeach
                            </div>



                            </div>
                        </div>

                        <hr>
                        <div class="form-group niveau-1">
                            <label for="demandefile">Charger le fichier numerique du bon</label>
                            <input type="file" class="form-control file" name="demandefile" accept=".pdf" id="demandefile"> <br>
                            <input type="hidden" name="old_demandefile" value="{{$bon->file}}"> <br>
                            @if($bon->file)<a href="{{asset('assets/uploads/bondemandes/'.$bon->file)}}" target="_blank" download>bon_de_demande_actuel.pdf</a>@endif
                        </div>
                        <br>
                        <div class="form-group text-right">
                            <a href="{{URL::previous()}}" class="btn btn-light btn-lg mr-4">
                                &nbsp; Annuler &nbsp;
                            </a>
                            <button type="submit" class="ml-4 btn btn-lg btn-primary waves-effect waves-light pull-right">Modifier </button>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection


@section('other_css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
@endsection

@section('other_js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/slim.js')}}"></script>
<script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>

<script>
    var demo1 = $('select[name="lot_id[]"]').bootstrapDualListbox({
        nonSelectedListLabel: 'Liste des lots',
        selectedListLabel: 'Lots Sélectionnés',
        preserveSelectionOnMove: 'moved',
        moveAllLabel: 'Tout déplacer',
        removeAllLabel: 'Enlever tout'
    });
</script>

@endsection
