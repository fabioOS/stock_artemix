@extends('chef_projet.layouts.app')

@section('page_title')
    Bon de livraison
@stop

@section('content_title')
    Bon de livraison
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de livraison</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>N°Ref</th>
                        <th>Fichier</th>
                        <th>Chef chantier</th>
                        <th>Sous-traitant</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bons as $bon)
                        <tr class="font-size-16">
                            <td><a href="#" class="text-dark font-weight-bold">{{Str::upper($bon->ref)}}</a> </td>
                            <td>@if($bon->file)<a href="{{asset('assets/uploads/bondemandes/'.$bon->file)}}" target="_blank" download>bon_de_demande.pdf</a>@else - @endif</td>
                            <td>{{$bon->user->name}}</td>
                            <td>{{mb_strtoupper($bon->soustraitant->nom)}}</td>
                            <td>
                                @if ($bon->status == 1)
                                    <span class="badge badge-pill badge-warning">En attente</span>
                                @elseif($bon->status == 2)
                                    <span class="badge badge-pill badge-info">Validé</span>
                                @elseif($bon->status == 3)
                                    <span class="badge badge-pill badge-success">Livré</span>
                                @else
                                <!--$bon->status == 4-->
                                    <span class="badge badge-pill badge-danger">Rejété</span>
                                @endif
                            </td>
                            <td>{{$bon->updated_at->format('d/m/Y')}}</td>
                            <td>
                                <a href="{{route('cphome.bc.show',$bon->ref)}}" class="btn btn-primary waves-effect waves-light">
                                    <i class="uil uil-eye mr-2"></i> Détails
                                </a>
                                <a href="{{route('cphome.bc.pdf',$bon->ref)}}" class="btn btn-success waves-effect waves-light">
                                    <i class="bx bxs-file-pdf mr-2"></i> PDF
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>
@endsection


@section('other_js')
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
@endsection
