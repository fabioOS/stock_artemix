@extends('chef_projet.layouts.app')

@section('page_title')
    Détails du bon de commande
@stop

@section('content_title')
Bon de commande N°  <span class="text-body">{{strtoupper($demande->ref)}}</span>
    @if ($demande->status == 1)
        <span class="badge badge-pill badge-warning">En attente</span>
    @endif
    @if ($demande->status == 2)
        <span class="badge badge-pill badge-info">Validé</span>
    @endif
    @if ($demande->status == 3)
        <span class="badge badge-pill badge-success">Livré</span>
    @endif
    @if ($demande->status == 4)
        <span class="badge badge-pill badge-danger">Rejété</span>
    @endif
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails du bon de commande</li>
    </ol>
@stop

@section('content')

    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">BC N°</h5>
                            <p>{{strtoupper($demande->ref)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Agent</h5>
                            <p>{{strtoupper($demande->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Bon de demande</h5>
                            <p><a target="_blank" href="{{route('cpcommande.show', encrypt($demande->demande->id))}}">{{$demande->demande->ref}}</a></p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Fournisseur</h5>
                            <p>{{$demande->fournisseur->nom}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Montant total</h5>
                            <p>@price($demande->montant) Fcfa</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            @if($demande->status == 1)
                                <div class="row gstock" id="btnaction">
                                    <div class="mr-1">
                                        <a href="{{route('cpbc.valid',encrypt($demande->id))}}" class="btn btn-success valid" title="Valider la demande"> <i class="fa fa-check-circle"></i> </a>
                                    </div>
                                    <div>
                                        <button type="button" data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter la demande"> <i class="fa fa-trash"></i> </button>
                                    </div>
                                </div>

                                <div id="loader" class="dispayNone">
                                    <div class="spinner-border text-primary m-1" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>

                            @elseif($demande->status== 2)
                                <div class="row gstock">
                                    <div class="mr-1">
                                        <a href="{{route('cpbc.show.pdf',encrypt($demande->ref))}}" class="btn btn-info" title="Imprimer le bon"> <i class="fa fa-print"></i> </a>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de demande</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->created_at)->format('d/m/Y')}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de paiement</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->datepaiement)->format('d/m/Y')}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Mode paiement</h5>
                            <p>{{$demande->mode}}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Matières commandées </h5>
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Num</td>
                                    <td>Matière</td>
                                    <td>U</td>
                                    <td>Quantité</td>
                                    <td>P.U</td>
                                    <td>Remise</td>
                                    <td>Prix U.HT</td>
                                    <td>Montant</td>
                                </tr>
                                @foreach ($demande->items as $i=>$item)
                                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                                    {{-- @dump((($item->remise /100) * $item->price)) --}}
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td><b>{{$pdt->libelle}}</b></td>
                                        <td>{{$pdt->type->libelle}}</td>
                                        <td>{{$item->qte}} </td>
                                        <td>@price($item->price)</td>
                                        <td>{{$item->remise}} %</td>
                                        <td>@price($item->price - (($item->remise /100) * $item->price))</td>
                                        <td>@price($item->mtt)</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter le BC</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEdit" action="{{route('cpbc.rejeter')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="bonid" value="{{$demande->ref}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="blockbtn">
                            <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                        </div>
                        <div id="loader1" class="dispayNone">
                            <div class="spinner-border text-primary m-1" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.valid', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment valider cet bon de commande ?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#btnaction').hide();
                    $('#loader').show();
                    window.location = href;
                }
            });
        });

    });

    function submitform(){
        var desc = $('#motif').val();
        if(desc == ''){
            swal("Oups!", "Veuillez renseignez la motif.");
            return false;
        }
        $('#blockbtn').hide();
        $('#loader1').show();
        // $('#submitform').submit();
        document.getElementById('formEdit').submit();

    }
</script>
@endsection
