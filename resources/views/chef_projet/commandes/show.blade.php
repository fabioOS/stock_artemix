@extends('chef_projet.layouts.app')

@section('page_title')
    Détails de la commande
@stop

@section('content_title')
    @if ($demande->status == 3)
        Commande N° <span class="text-body">{{strtoupper($demande->ref)}}</span>
    @else
        Commande N°  <span class="text-body">{{strtoupper($demande->ref)}}</span>
    @endif
    @if ($demande->status == 1)
        <span class="badge badge-pill badge-warning">En attente</span>
    @endif
    @if ($demande->status == 2)
        <span class="badge badge-pill badge-info">Validée</span>
    @endif
    @if ($demande->status == 3)
        <span class="badge badge-pill badge-success">Livrée</span>
    @endif
    @if ($demande->status == 4)
        <span class="badge badge-pill badge-danger">Rejétée</span>
    @endif

@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails de la commande</li>
    </ol>
@stop

@section('content')
    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Référence</h5>
                            <p>{{strtoupper($demande->ref)}}</p>
                        </div>

                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Demandeur</h5>
                            <p>{{strtoupper($demande->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Date de commande</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->created_at)->format('d/m/Y')}}</p>
                        </div>

                        <div class="col-lg-3">
                            @if($demande->status == 1)
                                @if($demande->actionbtn)
                                <div class="row gstock" id="btnaction">
                                    <div class="">
                                        <a href="{{route('cpcommande.valid',encrypt($demande->id))}}" class="btn btn-success valid" title="Valider la demande"> <i class="fa fa-check-circle"></i> </a>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter la demande"> <i class="fa fa-trash"></i> </button>
                                    </div>
                                </div>
                                <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                                    <span class="sr-only">Chargement...</span>
                                </div>

                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16">Description</h5>
                            <p>{{$demande->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16">Ligne budgetaire</h5>
                            @php $lignb = $demande->lignebudget @endphp
                            @if($lignb)
                                <p>{{strtoupper($lignb->libelle)}}
                                    @if($lignb->hors_ds == 1)
                                    <span class="badge badge-pill badge-warning">Hors DS</span>
                                    @elseif ($lignb->type == 1)
                                        <span class="badge badge-pill badge-primary">Batiments</span>
                                    @elseif ($lignb->type == 2)
                                        <span class="badge badge-pill badge-dark">VRD</span>
                                    @else
                                        <span class="badge badge-pill badge-success">-</span>
                                    @endif

                                @if($demande->datalignebudget)
                                <div class="progress mt-1" style="height: 20px;">
                                    <div class="progress-bar progress-bar bg-danger" role="progressbar" style="width: {{$demande->datalignebudget->original['pcent']}}%" aria-valuenow="{{$demande->datalignebudget->original['pcent']}}" aria-valuemin="0" aria-valuemax="{{$demande->datalignebudget->original['pcent']}}">
                                        @price($demande->datalignebudget->original['pcent']) %
                                    </div>
                                </div>
                                <span class="card-title font-weight-bolder text-{{$demande->datalignebudget->original['color']}}">@price($demande->datalignebudget->original['consommer']) / @price($demande->lignebudget->montant)</span>
                                @endif
                            </p>
                            @else
                            -
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @if($demande->status == 4)
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="font-size-16">Motif</h5>
                                <p>{{$demande->motif}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        <div class="col-xl-12">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Matières demandées </h5>
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr style="background-color: #c6baba2b;">
                                    <td>Code</td>
                                    <td>Matières</td>
                                    <td class="text-right">Qte totale déjà commandée</td>
                                    <td class="text-right">Qte totale restante</td>
                                    <td class="text-right table-success">Qte demandée</td>
                                    <td class="text-right">Qte totale commandée</td>
                                </tr>
                                @foreach ($demande->produits as $item)
                                @php $produit = \App\Produit::with('type')->where('id',$item->produit_id)->first();
                                    //deja commander
                                    $cmdval = \App\Commande::whereIn('status',[2,3])->where('commandes.programme_id',Session::get('program'))
                                    ->join('commande_items', 'commande_items.commande_id', '=', 'commandes.id')
                                    ->where('commande_items.produit_id',$item->produit_id)
                                    ->sum('commande_items.qte');
                                    $qtencss = \App\ProgrammeSouscorpsetatLot::where('produit_id',$item->produit_id)->where('programme_id',Session::get('program'))->sum('qte');
                                    $rest = round($qtencss - $cmdval,2);
                                @endphp

                                    <tr>
                                        <td>{{$produit->code}}</td>
                                        <td><b>{{$produit->libelle}}</b> <sup class="text-primary">{{$produit->type->libelle}}</sup> </td>
                                        <td class="text-right"> {{$cmdval}} </td>
                                        <td class="text-right"> {{$rest}} </td>
                                        <td class="text-right text-success"> {{$item->qte}} </td>
                                        @php
                                            $nb =\App\BCitem::where('commande_item_id',$item->id)->where('produit_id',$item->produit_id)
                                                ->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                                                ->where('bon_commandes.status',2)
                                                ->sum('qte')
                                        @endphp
                                        <td class="text-right"><span class="{{$nb>=$item->qte ? 'text-success' : 'text-warning'}}">{{$nb}}</span></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

    @if($demande->actionbtn)
    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter la commande</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEdit" action="{{route('cpcommande.rejeter')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="bonid" value="{{$demande->ref}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="loader2" class="spinner-border text-primary m-1 dispayNone" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                        <div id="blockbtn">
                            <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                        </div>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>
    @endif

@endsection


@section('other_js')
<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.valid', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment valider cette commande ?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#btnaction').hide();
                    $('#loader').show();
                    window.location = href;
                }
            });
        });

    });

    function submitform(){
        var desc = $('#motif').val();
        if(desc == ''){
            swal("Oups!", "Veuillez renseignez la motif.");
            return false;
        }
        $('#blockbtn').hide();
        $('#loader2').show();
        // $('#submitform').submit();
        document.getElementById('formEdit').submit();

    }

</script>
@endsection
