@extends('chef_projet.layouts.app')

@section('page_title')
    Demande de commande validée
@stop

@section('content_title')
    Demande de commande validée
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demande de commande validée</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($demandes) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Ligne Budgetaire</th>
                            <th>Demandeur</th>
                            <th>Nombre de produit</th>
                            <th>Statut</th>
                            <th>Date de commande</th>
                            <th title="Qantités demandées / quantités commandées">DMD / CMD</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="{{route('cpcommande.show', encrypt($dt->id))}}" class="text-primary font-weight-bold">{{$dt->ref}}</a> </td>
                                <td>
                                    @if($dt->lignebudget)
                                    @php $lignb = $dt->lignebudget @endphp
                                        {{strtoupper($lignb->libelle)}} <br>
                                        @if($lignb->hors_ds == 1)
                                            <span class="badge badge-pill badge-warning">Hors DS</span>
                                        @elseif ($lignb->type == 1)
                                            <span class="badge badge-pill badge-primary">Batiments</span>
                                        @elseif ($lignb->type == 2)
                                            <span class="badge badge-pill badge-dark">VRD</span>
                                        @else
                                            <span class="badge badge-pill badge-success">-</span>
                                        @endif
                                    @else
                                        <span class="badge badge-pill badge-danger">Budget non défini</span>
                                    @endif
                                </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{count($dt->produits)}}</td>
                                <td>
                                    <span class="badge badge-pill badge-info">Validé</span>
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @php $dmds = $dt->produits->sum('qte'); @endphp
                                    <span class="{{$dmds<=$dt->qtecmde ? "text-success" : "text-danger"}}">{{$dmds}} / {{$dt->qtecmde}}</span>
                                </td>
                                <td>
                                    <a href="{{route('cpcommande.show', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
