@extends('chef_projet.layouts.app')

@section('page_title')
    Demandes spéciales cloturées
@stop

@section('content_title')
    Demandes spéciales cloturées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciales cloturées</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if(count($demandes)>0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th style="width: 100px">Montant</th>
                            <th>Moyen</th>
                            <th>Bordereau</th>
                            <th>Date de traitement</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="#" class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>@price($dt->montant)</td>
                                @php $trans = App\BDSpecTrans::where('demande_id',$dt->id)->first(); @endphp
                                <td>
                                    <span class="badge badge-pill badge-info">{{$trans->moyen}}</span>
                                </td>
                                <td><a href="{{asset('assets/demandespecials/'.$trans->file)}}" target="_blank" class="text-primary">Voir le bordereau</a></td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->updated_at)->format('d/m/Y H:i')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>
@endsection


@section('other_js')
<script>

</script>
@endsection
