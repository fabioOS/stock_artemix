@extends('chef_projet.layouts.app')

@section('page_title')
    Demandes spéciales en attente de traitement
@stop

@section('content_title')
    Demandes spéciales en attente de traitement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciales en attente de traitement</li>
    </ol>
@stop

@section('content')

<div class="row gstock">
    <div class="col-lg-12">
        @if (count($demandes) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th>Motif</th>
                            <th style="width: 100px">Montant</th>
                            <th>Statut</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="#" class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>{!! nl2br($dt->description) !!}
                                    @if($dt->preuve != null)<br>
                                    <a href="{{asset('assets/uploads/demandespeciale/'.$dt->preuve)}}" target="_blank">Voir Pièce-Jointe</a>@endif
                                </td>
                                <td>@price($dt->montant)</td>
                                <td>
                                    <span class="badge badge-pill badge-warning">En attente <br>de validation du DP</span>
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @if($dt->user_id == Auth::user()->id and $dt->genere == 0)
                                        <a href="{{route('delDmdSpecial',encrypt($dt->id))}}" class="delet btn btn-danger btn-sm" title="Supprimer la demande"> <i class="fa fa-trash"></i></a>
                                    @endif

                                    @if(Auth::user()->role_id==5)
                                        {{-- @if($dt->genere==1) --}}
                                            <a href="{{route('cpdemande.valid', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light generate" title="Validé le BC"><i class="fa fa-check-circle"></i></a>
                                            @if($dt->status == 1)
                                            <button type="button" onclick="showrejet('{{$dt->slug}}')" data-toggle="modal" data-target="#rejectbon" class="btn btn-danger btn-sm" title="Rejeter la demande"> <i class="fa fa-trash"></i> </button>
                                            @endif
                                        {{-- @endif --}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

<div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Rejeter la demande</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formEdit" action="{{route('cpdemande.rejeter')}}" method="post" enctype="multipart/form-data">@csrf
                <div class="modal-body">
                    <div  class="form-group">
                        <label class="control-label">Motif <span class="text-danger">*</span></label>
                        <br>
                        <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                        <input type="hidden" name="bonid" id="bonid" value="">
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                        <span class="sr-only">Chargement...</span>
                    </div>

                    <button onclick="sendForm()" id="addbtn" type="button" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div>
</div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.generate', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment valider le bon de commande.",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('.gstock').on('click', '.delet', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cette demande.",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

    });

    function showrejet(id){
        $("#formEdit")[0].reset();
        $("#bonid").val(id);
    }

    function sendForm() {
        var motigg = document.getElementById('motif').value;

        if(!motigg){
            swal("Oups!", "Veuillez renseignez le champs Motif");
            return false;
        }else{
            document.getElementById('addbtn').disabled = true;
            $('#addbtn').hide();
            $('#loader').show();
            document.getElementById('formEdit').submit();
        }
    }
</script>
@endsection
