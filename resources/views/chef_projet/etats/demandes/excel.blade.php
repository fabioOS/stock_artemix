{{--<table>
     <thead>
        <tr>
            <th>NUM</th>
            <th>N°REF</th>
            <th>DEMANDEUR</th>
            <th>NOMBRE DE PRODUIT</th>
            <th>STATUS</th>
            <th>DATE DE DEMANDE</th>
        </tr>
    </thead>

    <tbody>
        @foreach($datas as $k=>$demande)
            <tr class="">
                <td>{{$k+1}}</td>
                <td><p><strong>{{$demande->ref}}</strong></p></td>
                <td>{{$demande->user ? $demande->user->name : '-'}}</td>
                <td>{{count($demande->produits)}}</td>
                <td>@if($demande->status==1) EN ATTENTE @elseif ($demande->status==2) VALIDER @else REJETER @endif</td>
                <td>{{$demande->created_at->format('d/m/Y')}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<br><br>
--}}

<table>
    <thead>
        <tr>
            <td>NUM</td>
            <td>MATIERE</td>
            <td>QUANTITE DEMANDEE</td>
            <td>QUANTITE COMMANDEE</td>
            <td>DATE DE DEMANDE</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($data->produits as $e=>$item)
        <tr class="">
            <td>{{$e+1}}</td>
            <td><p><strong>{{\App\Produit::where('id',$item->produit_id)->pluck('libelle')->first()}}</strong></p></td>
            <td>{{$item->qte}}</td>
            @php
                $nb =\App\BCitem::where('commande_item_id',$item->id)->where('produit_id',$item->produit_id)
                    ->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                    ->where('bon_commandes.status',2)
                    ->sum('qte')
            @endphp
            <td>{{$nb}}</td>
            <td>{{$item->created_at->format('d/m/Y')}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
