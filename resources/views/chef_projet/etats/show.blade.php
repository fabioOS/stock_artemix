@extends('chef_projet.layouts.app')

@section('page_title')
    Statistiques villa
@stop

@section('content_title')
    Statistiques villa <span class="text-muted">{{$listlot->lot}} <small>({{$listlot->actifs? $listlot->actifs->libelle : ''}})</small></span>
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Statistiques par villa</li>
    </ol>
@stop

@section('other_css')
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    {{-- @php $pcent = $montantLot ? round((($montbuy)*100) / $montantLot,1) : 0 @endphp --}}
    @php
        $corpsetats = \App\ProgrammeCorpsetat::where('actif_id',$listlot->actif_id)->orderBy('id','asc')->get();
        //$corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$listlot->actif_id)->with('corpslots')->get();
        $programq = \App\Programme::where('id',$listlot->programme_id)->first();
        $nbcoretat = count($corpsetats);
        $tteval = \App\Avancement::where([['programme_id',$listlot->programme_id],['lot_id',$listlot->id]])->get();
        $mttpaylot = $tteval->sum('cout');
        $tteval = $tteval->sum('taux_execute');

        $pcent = round($tteval / $nbcoretat,2);
    @endphp

    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24"><span>@price($montantLot)</span> <sup>FCFA</sup></h4>
                        <p class="text-muted mb-0">Coût de la main d'oeuvre de la villa</p>
                    </div>

                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24"><span>@price($mttpaylot)</span> <sup>FCFA</sup></h4>
                        <p class="text-muted mb-0">Main d'oeuvre payée</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24"><span>@price($montantLot-$mttpaylot)</span> <sup>FCFA</sup></h4>
                        <p class="text-muted mb-0">Montant main d'oeuvre exigible</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">

            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24"><span data-plugin="counterup">{{ $pcent }}</span>%</h4>
                        <p class="text-muted mb-0">Evolution global de la villa</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->
    </div>

    <div class="row">
        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title mb-4">Paiement de la main d'oeuvre du mois</h4>

                    <canvas id="lineChartMe" height="300"></canvas>

                </div>
            </div>
        </div> <!-- end col -->

        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title mb-4">Evolution</h4>

                    <canvas id="pie" height="295"></canvas>

                </div>
            </div>
        </div> <!-- end col -->

    </div>

    <a href="#" onclick="imprimer_page('print')" class="btn btn-success float-right"><i class="fa fa-print"></i> Imprimer</a>

    <div id="print">
    <div id="showing" class="mt-4 mb-2">
    <h4 class="mb-0">Evolution des corps d'état</h4>
    <h5 class="mt-2">Statistiques villa <span class="text-muted">{{$listlot->lot}}</span></h5>
    </div>


    <div class="row">
        <div class="col-lg-12">
            @foreach($corpsetats as $j=>$corps)
                @php
                    $check = \App\Avancement::where([['programme_id',$listlot->programme_id],['lot_id',$listlot->id],['corpsetat_id',$corps->id]])->get();
                    $tau = $check->sum('taux_execute');
                    $couttotal = $corps->prix;
                    if($programq->type_rattachement == 2){
                        $montant = \App\ProgrammeSoustraitantcontratMontant::where([['lot_id',$listlot->id],['programme_id',$listlot->programme_id],['corpsetats_id',$corps->id]])->sum('montant');
                        $couttotal =$montant;
                    }
                    //Montant payé
                    $montbuy = $check->sum('cout');
                @endphp

                <div class="card">
                    <a href="#main_phase_{{$j}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase_{{$j}}">
                        <div class="p-4">
                            <div class="media align-items-center">
                                <div class="mr-3">
                                    <div class="avatar-xs">
                                        <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                    </div>
                                </div>
                                <div class="media-body overflow-hidden">
                                    <h5 class="font-size-16 mb-1">{{Str::upper($corps->libelle)}}</h5>
                                    {{--<p class="text-muted mb-0"><span class="mr-3">COÛT: @price($corps->prix) <sup>FCFA</sup></span></p>--}}
                                    <div class="tilte mr-auto p -2">
                                        <span class="text-default font-size-13">
                                            Coût Total: <b>@price($couttotal) <sup>FCFA</sup></b> &nbsp; | &nbsp;
                                            Coût payé: <b>@price($montbuy)  <sup>FCFA</sup></b> &nbsp; | &nbsp;
                                            Taux d'avancement: <b class="text-warning">{{round($tau,2)}}%</b>
                                        </span>
                                    </div>
                                </div>
                                <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                            </div>
                        </div>
                    </a>
                    <!-- action delete update Main Phase -->
                    <div id="main_phase_{{$j}}" class="collapse show" data-parent="#main_phase_{{$j}}">
                        <div class="p-4 border-top">
                            @php
                                //$souscorps = \App\ProgrammeSouscorpsetat::where([['programme_id',$listlot->programme_id],['corpsetat_id',$corps->id]])->with('matieres')->get();
                            @endphp
                            @php
                                $libelcorpsetatids = \App\ProgrammeSouscorpsetatLot::where([['programme_id',$listlot->programme_id],['corpsetat_id',$corps->id],['actif_id',$listlot->actif_id],['lot_id',$listlot->id]])->orderBy('id','asc')->pluck('corpsetatlibel_id')->toArray();
                                $souscorps = \App\ProgrammeCorpsetatLibelle::whereIn('id',$libelcorpsetatids)->orderBy('id','asc')->get();
                                //dd($souscorpetat);
                            @endphp

                            @foreach($souscorps as $i=>$souscorp)
                                <div class="sub_sub_2 border-top d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                    <div class="tilte mr-auto p-2">{{$souscorp->libelle}}</div>
                                </div>

                                @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$listlot->id],['corpsetat_id',$corps->id],['actif_id',$listlot->actif_id],['corpsetatlibel_id',$souscorp->id]])->with('matieres')->get(); @endphp

                                @foreach ($scorpsmtt as $scorps)
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">{{$scorps->matieres->libelle}}</div>

                                        @php $type = \App\Typeproduit::where('id',$scorps->matieres->type_id)->first();
                                            $total_livre = \App\Bon_demande::where('status', 3)
                                            ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                            ->where('programme_id', $listlot->programme_id)
                                            ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                            ->where('bon_demande_items.lot_id', $listlot->id)
                                            ->where('bon_demande_items.corpsetats_id', $corps->id)
                                            ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                            ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                            ->pluck('total_livre')
                                            ->first();
                                            //dump($total_livre);
                                        @endphp

                                        <div class="somme p-2">
                                            <span class="text-default font-size-13">
                                                <b>{{$total_livre ?: 0}} <sup>{{$type->libelle}}</sup> </b> utilisées &nbsp; / &nbsp;
                                                <b>{{$scorps->qte}} <sup>{{$type->libelle}}</sup></b>
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach

                            {{-- Ceux qui nont pas de LIBELLE --}}
                            @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$listlot->id],['corpsetat_id',$corps->id],['actif_id',$listlot->actif_id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                            @foreach ($scorpsmtt as $scorps)
                                <div class="sub_sub_2 border-top d-flex align-items-center">
                                    <div class="tilte mr-auto p-2">{{$scorps->matieres->libelle}}</div>

                                    @php $type = \App\Typeproduit::where('id',$scorps->matieres->type_id)->first();
                                        $total_livre = \App\Bon_demande::where('status', 3)
                                        ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                        ->where('programme_id', $listlot->programme_id)
                                        ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                        ->where('bon_demande_items.lot_id', $listlot->id)
                                        ->where('bon_demande_items.corpsetats_id', $scorps->corpsetat_id)
                                        ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                        ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                        ->pluck('total_livre')
                                        ->first();
                                        //dump($total_livre.' pg '.$listlot->programme_id.' pdt '.$scorps->produit_id.' lot '.$listlot->id.' cpid '.$scorps->corpsetat_id.' scpid '. $scorps->id);
                                    @endphp

                                    <div class="somme p-2">
                                        <span class="text-default font-size-13">
                                            <b>{{$total_livre ?: 0}} <sup>{{$type->libelle}}</sup> </b> utilisées &nbsp; / &nbsp;
                                            <b>{{$scorps->qte}} <sup>{{$type->libelle}}</sup></b>
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
{{--    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>--}}

    <script>
        !(function (l) {
            "use strict";
            function r() {}
            (r.prototype.respChart = function (r, o, e, a) {
                (Chart.defaults.global.defaultFontColor = "#9295a4"), (Chart.defaults.scale.gridLines.color = "rgba(166, 176, 207, 0.1)");
                var t = r.get(0).getContext("2d"),
                    n = l(r).parent();
                function i() {
                    r.attr("width", l(n).width());
                    switch (o) {
                        case "Line":
                            new Chart(t, { type: "line", data: e, options: a });
                            break;
                        case "Doughnut":
                            new Chart(t, { type: "doughnut", data: e, options: a });
                            break;
                        case "Pie":
                            new Chart(t, { type: "pie", data: e, options: a });
                            break;
                        case "Bar":
                            new Chart(t, { type: "bar", data: e, options: a });
                            break;
                        case "Radar":
                            new Chart(t, { type: "radar", data: e, options: a });
                            break;
                        case "PolarArea":
                            new Chart(t, { data: e, type: "polarArea", options: a });
                    }
                }
                l(window).resize(i), i();
            }),
                (r.prototype.init = function () {
                    this.respChart(
                        l("#lineChartMe"),
                        "Line",
                        {
                            labels: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Novembre", "Decembre"],
                            datasets: [
                                {
                                    label: "Paiement mensuel de la main d'oeuvre",
                                    fill: !0,
                                    lineTension: 0.5,
                                    backgroundColor: "rgba(91, 140, 232, 0.2)",
                                    borderColor: "#5b73e8",
                                    borderCapStyle: "butt",
                                    borderDash: [],
                                    borderDashOffset: 0,
                                    borderJoinStyle: "miter",
                                    pointBorderColor: "#5b73e8",
                                    pointBackgroundColor: "#fff",
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "#5b73e8",
                                    pointHoverBorderColor: "#fff",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data: [@php foreach($stats as $k=>$stat){ if($stat==null) echo '0,'; else echo $stat.',';}@endphp],
                                },
                            ],
                        },

                    );
                    this.respChart(l("#pie"), "Pie", { labels: ["Achevé", "Non achevé"], datasets: [{ data: [{{$pcent}}, {{100-$pcent}}], backgroundColor: ["#34c38f", "#ebeff2"], hoverBackgroundColor: ["#34c38f", "#ebeff2"], hoverBorderColor: "#fff" }] });

                }),
                (l.ChartJs = new r()),
                (l.ChartJs.Constructor = r);
        })(window.jQuery),
            (function () {
                "use strict";
                window.jQuery.ChartJs.init();
            })();


        function imprimer_page(id){
            //window.print();
            $('#showing').show();
            var restorepage = document.body.innerHTML;
            var printcontent = document.getElementById(id).innerHTML;
            document.body.innerHTML = printcontent;
            window.print();
            $('#showing').hide();
            document.body.innerHTML = restorepage;
        }
    </script>
@endsection
