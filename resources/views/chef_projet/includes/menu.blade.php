<style>
    .text-bleu{
        color: #234699
    }
</style>

<ul class="metismenu list-unstyled" id="side-menu">

    <li>
        <a href="{{route('cphome')}}">
            <i class="uil-home-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li>

    <li class="menu-title mt-3 text-bleu">Gestion du stock</li>

    <li class="{{Route::is('cphome.etats') || Route::is('cphome.etats.search') || Route::is('cphome.etats.item') ? 'mm-active' : ''}}">
        <a href="{{route('cphome.etats')}}" class="{{Route::is('cphome.etats') || Route::is('cphome.etats.search') || Route::is('cphome.etats.item') ? 'active' : ''}}">
            <i class="uil-home"></i>
            <span>Etats par villa</span>
        </a>
    </li>

    @if(1==2)
    <li>
        <a href="{{route('cphome.fours')}}">
            <i class="uil-user-circle"></i>
            <span>Fournisseurs</span>
        </a>
    </li>
    @endif

    <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-notes"></i>
            <span>Stock</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('cphome.stock')}}" class="{{Route::is('cphome.stock') ? 'active' : ''}}">Stock magasin</a></li>
            <li><a href="{{route('cphome.stock.budget')}}" class="{{Route::is('cphome.stock.budget') ? 'active' : ''}}">Stock budget</a></li>
        </ul>
    </li>

    @if(Auth::user()->role_id==3)
    @inject('nbnotif','App\Helperme\Notifications')
    <li class="{{Route::is('cphome.bc.show') ?'mm-active' : ''}}">
        @php
            //$validated = (new \App\Http\Controllers\BonController())->getByStatus(2)->count();
            // $canceled = (new \App\Http\Controllers\BonController())->getByStatus(4)->count();
            //$waiting = (new \App\Http\Controllers\BonController())->getByStatus(1)->count();
            //$delivered = (new \App\Http\Controllers\BonController())->getByStatus(3)->count();
        @endphp
        <a href="javascript: void(0);" class="has-arrow waves-effect {{Route::is('cphome.bc.show') ?'mm-active' : ''}}">
            <i class="uil-comment-alt-edit"></i>
            <span>Bons de sortie</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            {{-- <li><a href="{{route('cphome.bc.create')}}" class="{{Route::is('cphome.bc.create')||Route::is('cphome.bc.step2create')||Route::is('cphome.bc.step3create') ? 'active' : ''}}">Créer un bon de sortie</a></li> --}}
            <li><a href="{{route('cphome.bc.waiting')}}" class="{{Route::is('cphome.bc.waiting') ? 'active' : ''}}">En attente
                    @if($nbnotif::CountNotifsWainting()>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$nbnotif::CountNotifsWainting()}}</span>
                    @endif
                </a>
            </li>
            <li><a href="{{route('cphome.bc.cancel')}}" class="{{Route::is('cphome.bc.cancel')  ? 'active' : ''}}">Rejetés
                {{-- @if($canceled >0 )
                    <span class="badge badge-pill badge-danger float-right">{{$canceled}}</span>
                @endif --}}
                </a>
            </li>
            <li><a href="{{route('cphome.bc.valider')}}" class="{{Route::is('cphome.bc.valider') ? 'active' : ''}}">Validés </a></li>
        </ul>
    </li>
    @endif

    <li class="{{Route::is('cphome.back.index') ||Route::is('cphome.back.show') ? 'mm-active' : ''}}">
        @php
            $waiting = (new \App\Http\Controllers\Chefprojet\BackstockController())->getStatus(1);
        @endphp
        <a class="{{Route::is('cphome.back.index') ||Route::is('cphome.back.show') ? 'active' : ''}}" href="{{route('cphome.back.index')}}">
            <i class="uil-step-backward-alt"></i>
            <span>Retour en stock
                @if($waiting != 0 )
                <span class="badge badge-pill badge-warning float-right">{{$waiting}}</span>
                @endif
            </span>
        </a>
    </li>

    {{-- <li>
        <a href="{{route('cphome.bl')}}">
            <i class="uil-comment-alt-check"></i>
            <span>Bon de livraison</span>
        </a>
    </li> --}}

    @inject('CountStraitantWainting','App\Helperme\Notifications')
    <li>
        @php
            $waiting = (new \App\Http\Controllers\BonController())->getByStatusContratSt(1);
            $canceled = (new \App\Http\Controllers\BonController())->getByStatusContratSt(0);
            //dump($waiting)
            // $validated = (new \App\Http\Controllers\BonController())->getByStatusContratSt(2)->count();
        @endphp
        <a href="javascript: void(0);" class="{{$waiting==0 ? 'has-arrow' : ''}} {{Route::is('cphome.st.index')||Route::is('cphome.st.n-contrat')||Route::is('cphome.st.show') ? 'mm-active' : ''}} waves-effect">
            <i class="uil-users-alt"></i>
            @if($waiting>0 )
            <span class="badge badge-pill badge-warning float-right">{{$waiting}}</span>
            @endif
            <span>Sous-traitant</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('cphome.st.index')}}" class="{{Route::is('cphome.st.index')||Route::is('cphome.st.n-contrat')||Route::is('cphome.st.n-contrat.ce')||Route::is('cphome.st.edit')||Route::is('cphome.st.paye')||Route::is('cphome.st.lot.paye') ?'active' : ''}}">Liste des sous-traitants</a></li>
            <li><a href="{{route('cphome.st.contrat.waiting')}}" class="">Contrat en attente
                    @if($waiting>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$waiting}}</span>
                    @endif
                </a>
            </li>
            <li><a href="{{route('cphome.st.contrat.cancel')}}" class="{{Route::is('cphome.bc.cancel')  ? 'active' : ''}}">Contrat rejeté
                @if($canceled >0 )
                    <span class="badge badge-pill badge-danger float-right">{{$canceled}}</span>
                @endif
                </a>
            </li>
            <li><a href="{{route('cphome.st.contrat.valider')}}" class="{{Route::is('cphome.bc.valider') ? 'active' : ''}}">Contrat validé </a></li>
        </ul>
    </li>


    {{-- CHEF PROJET --}}
    <li class="menu-title mt-3 text-bleu">Gestion des commandes</li>

    <li>
        <a href="{{route('lignebudgetaire.index')}}">
            <i class="uil-chart"></i>
            <span>Ligne budgetaire</span>
        </a>
    </li>
    
        @php $waiting = (new \App\Http\Controllers\Chefprojet\CommandeController())->getByStatus(1); @endphp

        <li>
            <a href="javascript: void(0);" class="{{$waiting==0 ? 'has-arrow' : ''}} waves-effect">
                <i class="uil-file-alt"></i>
                @if($waiting>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$waiting}}</span>
                @endif
                <span>Commandes</span>
            </a>
            <ul class="sub-menu" aria-expanded="false">
                <li><a href="{{route('cpcommande.waiting')}}" class="{{Route::is('cpcommande.waiting') ? 'active' : ''}}">En attente
                    @if($waiting>0 )
                        <span class="badge badge-pill badge-warning float-right">{{$waiting}}</span>
                    @endif
                </a></li>
                <li><a href="{{route('cpcommande.rejetees')}}" class="{{Route::is('cpcommande.rejetees') ? 'active' : ''}}">Rejetées
                </a></li>
                <li><a href="{{route('cpcommande.valider')}}" class="{{Route::is('cpcommande.valider') ? 'active' : ''}}">Validées
                </a></li>

                <li>
                    <a href="{{route('cp.etats.cmd')}}" class="{{Route::is('cp.etats.cmd') ? 'active' : ''}}">Etats</a>
                </li>
            </ul>
        </li>

    @if(Auth::user()->role_id==3)
        <li class="menu-title mt-3 text-bleu">Gestion des demandes</li>
            @php
            $waitingspe = (new \App\Http\Controllers\DemandeSpecialsController())->getByStatus(1);
            @endphp
        <li>
            <a href="javascript: void(0);" class="{{count($waitingspe) > 0 ? '' : 'has-arrow'}} waves-effect">
                <i class="uil-align-letter-right"></i>
                <span>Demandes spéciales</span>
                @if(count($waitingspe) > 0)
                    <span class="badge badge-pill badge-warning float-right">{{count($waitingspe)}}</span>
                @endif
            </a>

            <ul class="sub-menu" aria-expanded="false">
                <li><a href="{{route('cpdemande.create')}}" class="">Créer une demande </a></li>
                <li><a href="{{route('cpdemande.waiting')}}" class="">En attente
                    @if(count($waitingspe)>0 )
                        <span class="badge badge-pill badge-warning float-right">{{count($waitingspe)}}</span>
                    @endif
                </a></li>
                <li><a href="{{route('cpdemande.rejet')}}" class="">Rejetées </a></li>
                <li><a href="{{route('cpdemande.valider')}}" class="">Validées </a></li>
            </ul>

        </li>

        @inject('nbtranswaiting','App\Helperme\Notifications')
        <li class="{{Route::is('cphome.st.mainoeuvre') || Route::is('cphome.st.paye')|| Route::is('cphome.st.lot.paye') ? 'mm-active' : ''}}">
            <a href="javascript: void(0);" class=" {{$nbtranswaiting::CountTransacValid()==0 ? 'has-arrow' : ''}} waves-effect">
                <i class="uil-money-withdrawal"></i>
                @if($nbtranswaiting::CountTransacValid()>0 )
                    <span class="badge badge-pill badge-warning mr-3 float-right">{{$nbtranswaiting::CountTransacValid()}}</span>
                @endif
                <span>Main d'oeuvre</span>
            </a>
            <ul class="sub-menu" aria-expanded="false">
                {{-- <li><a href="{{route('cphome.st.mainoeuvre')}}" class="{{Route::is('cphome.st.mainoeuvre') || Route::is('cphome.st.paye')|| Route::is('cphome.st.lot.paye') ? 'active' : ''}}">Faire une demande </a></li> --}}
                <li><a href="{{route('cphome.transac.waiting')}}" class="{{Route::is('cphome.transac.waiting') ? 'active' : ''}}">En attente @if($nbtranswaiting::CountTransacValid()>0 )<span class="badge badge-pill badge-warning">{{$nbtranswaiting::CountTransacValid()}}</span>@endif</a></li>
                <li><a href="{{route('cphome.transac')}}" class="{{Route::is('cphome.transac') ? 'active' : ''}}">Validées</a></li>
                <li><a href="{{route('cphome.transac.rejeter')}}" class="{{Route::is('cphome.transac.rejeter') ? 'active' : ''}}">Rejetées</a></li>
            </ul>
        </li>

    @endif

    {{-- RESPO OPC --}}
    @if(Auth::user()->role_id==5)
        @php
            $waitingct = (new \App\Http\Controllers\Chefprojet\BonCommandeController())->getByStatus(1);
        @endphp
        {{-- <li class="{{Route::is('cpbc.show') ? 'mm-active' : ''}}">
            <a href="javascript: void(0);" class="{{$waitingct==0 ? 'has-arrow' : ''}} waves-effect {{Route::is('cpbc.show') ? 'mm-active' : ''}}">
                <i class="uil-file-alt"></i>
                @if($waitingct>0 )
                    <span class="badge badge-pill badge-primary float-right">{{$waitingct}}</span>
                @endif
                <span>Bon de commande</span>
            </a>
            <ul class="sub-menu" aria-expanded="false">
                <li><a href="{{route('cpbc.waiting')}}" class="{{Route::is('cpbc.waiting') ? 'active' : ''}}">BC en attente
                    @if($waitingct>0 )
                        <span class="badge badge-pill badge-primary float-right">{{$waitingct}}</span>
                    @endif
                </a></li>
                <li><a href="{{route('cpbc.valider')}}" class="{{Route::is('cpbc.valider') ? 'active' : ''}}">BC validé
                </a></li>
            </ul>
        </li> --}}

        <li class="menu-title mt-3 text-bleu">Gestion des demandes</li>
        @php
            $waitingct = (new \App\Http\Controllers\Chefprojet\DemandesController())->getByStatus(1);
        @endphp
        <li>
            <a href="javascript: void(0);" class="{{$waitingct==0 ? 'has-arrow' : ''}} waves-effect">
                <i class="uil-file-alt"></i>
                @if($waitingct>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$waitingct}}</span>
                @endif
                <span>Demandes spéciales</span>
            </a>
            <ul class="sub-menu" aria-expanded="false">
                <li><a href="{{route('cpdemande.waiting')}}" class="{{Route::is('cpdemande.waiting') ? 'active' : ''}}">En attente
                    @if($waitingct>0 )
                        <span class="badge badge-pill badge-warning float-right">{{$waitingct}}</span>
                    @endif
                </a></li>
                <li><a href="{{route('cpdemande.valider')}}" class="{{Route::is('cpdemande.valider') ? 'active' : ''}}">Validées
                </a></li>
                @if(Auth::user()->role_id==5)<li><a href="{{route('cpdemande.traiter')}}" class="{{Route::is('cpdemande.traiter') ? 'active' : ''}}">Cloturées / Payées
                </a></li>@endif
            </ul>
        </li>

        @inject('nbtranswaiting','App\Helperme\Notifications')
        <li class="{{Route::is('cphome.st.mainoeuvre') || Route::is('cphome.st.paye')|| Route::is('cphome.st.lot.paye') ? 'mm-active' : ''}}">
            <a href="javascript: void(0);" class=" {{$nbtranswaiting::CountTransacValid()==0 ? 'has-arrow' : ''}} waves-effect">
                <i class="uil-money-withdrawal"></i>
                @if($nbtranswaiting::CountTransacValid()>0 )
                    <span class="badge badge-pill badge-warning mr-3 float-right">{{$nbtranswaiting::CountTransacValid()}}</span>
                @endif
                <span>Main d'oeuvre</span>
            </a>
            <ul class="sub-menu" aria-expanded="false">
                {{-- <li><a href="{{route('cphome.st.mainoeuvre')}}" class="{{Route::is('cphome.st.mainoeuvre') || Route::is('cphome.st.paye')|| Route::is('cphome.st.lot.paye') ? 'active' : ''}}">Faire une demande </a></li> --}}
                <li><a href="{{route('cphome.transac.waiting')}}" class="{{Route::is('cphome.transac.waiting') ? 'active' : ''}}">En attente @if($nbtranswaiting::CountTransacValid()>0 )<span class="badge badge-pill badge-warning">{{$nbtranswaiting::CountTransacValid()}}</span>@endif</a></li>
                <li><a href="{{route('cphome.transac')}}" class="{{Route::is('cphome.transac') ? 'active' : ''}}">Validées</a></li>
                <li><a href="{{route('cphome.transac.rejeter')}}" class="{{Route::is('cphome.transac.rejeter') ? 'active' : ''}}">Rejetées</a></li>
            </ul>
        </li>
    @endif

</ul>
