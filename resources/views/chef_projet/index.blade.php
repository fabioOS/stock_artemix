@extends('chef_projet.layouts.app')

@section('page_title')
    Tableau de bord
@stop

@section('content_title')
    Tableau de bord
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Tableau de bord</li>
    </ol>
@stop

@section('content')
    @include('_stats_evolution')

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title mb-4">Paiement de la main d'oeuvre</h4>

                    <canvas id="lineChartMe" height="300"></canvas>

                </div>
            </div>
        </div> <!-- end col -->

    </div> <!-- end row -->


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="card-title mb-4">Transactions par sous-traitant</h4>
                    <table id="datatable-buttons" class="table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Trans. ID</th>
                            <th>Sous-traitant</th>
                            <th>Villa</th>
                            <th>Montant à payer</th>
                            <th>Taux d'avancement</th>
                            <th>Date</th>
                            <th>Etat paiement</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lists as $k=>$list)
                            @php
                                $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                            @endphp
                            <tr>
                                <td><a href="javascript: void(0);" class="text-body font-weight-bold">{{$list->slug}}</a> </td>
                                <td>{{$list->soustraitant->nom}}</td>
                                <td>{{$list->lot->lot}}</td>
                                <td>@price($list->montant) <sup>FCFA</sup></td>
                                <td>
                                    <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats != 0 ? round($list->taux/$corpsetats).'%' : '-' !!}</span>
                                </td>
                                <td>{{$list->created_at->format("d/m/Y h:i:s")}}</td>
                                <td>
                                    @if($list->status==2)
                                        <span class="badge badge-pill badge-success font-size-12">Payer</span>
                                    @else
                                        <span class="badge badge-pill badge-danger font-size-12">En attente de paiement</span>
                                    @endif
                                </td>
                                <td><a href="{{route('cphome.transac.item',encrypt($list->id))}}" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light"><i class="fa fa-eye"></i> Détail</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        !(function (l) {
            "use strict";
            function r() {}
            (r.prototype.respChart = function (r, o, e, a) {
                (Chart.defaults.global.defaultFontColor = "#9295a4"), (Chart.defaults.scale.gridLines.color = "rgba(166, 176, 207, 0.1)");
                var t = r.get(0).getContext("2d"),
                    n = l(r).parent();
                function i() {
                    r.attr("width", l(n).width());
                    switch (o) {
                        case "Line":
                            new Chart(t, { type: "line", data: e, options: a });
                            break;
                        case "Doughnut":
                            new Chart(t, { type: "doughnut", data: e, options: a });
                            break;
                        case "Pie":
                            new Chart(t, { type: "pie", data: e, options: a });
                            break;
                        case "Bar":
                            new Chart(t, { type: "bar", data: e, options: a });
                            break;
                        case "Radar":
                            new Chart(t, { type: "radar", data: e, options: a });
                            break;
                        case "PolarArea":
                            new Chart(t, { data: e, type: "polarArea", options: a });
                    }
                }
                l(window).resize(i), i();
            }),
                (r.prototype.init = function () {
                    this.respChart(
                        l("#lineChartMe"),
                        "Line",
                        {
                            labels: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Novembre", "Decembre"],
                            datasets: [
                                {
                                    label: "Paiement mensuel de la main d'oeuvre",
                                    fill: !0,
                                    lineTension: 0.5,
                                    backgroundColor: "rgba(91, 140, 232, 0.2)",
                                    borderColor: "#5b73e8",
                                    borderCapStyle: "butt",
                                    borderDash: [],
                                    borderDashOffset: 0,
                                    borderJoinStyle: "miter",
                                    pointBorderColor: "#5b73e8",
                                    pointBackgroundColor: "#fff",
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "#5b73e8",
                                    pointHoverBorderColor: "#fff",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data: [ @php foreach($stats as $k=>$stat) if($stat==null) echo '0,'; else echo $stat.',';@endphp],
                                },
                            ],
                        },

                    );
                }),
                (l.ChartJs = new r()),
                (l.ChartJs.Constructor = r);
        })(window.jQuery),
            (function () {
                "use strict";
                window.jQuery.ChartJs.init();
            })();


    </script>
@endsection
