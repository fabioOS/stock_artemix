<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Mes programmes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- CSRF Token -->
    <meta property="csrf-token" id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}">

    <link href="{{asset('assets/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />

    <style>
        .maintenance-box{
            border-radius: 10px;
            background: transparent !important;
            overflow: hidden;
        }

        .prog-zone{
            height: 210px;
            background: transparent  !important;
        }

        .prog-zone img{
            width: 100%;
            height: 210px;
            object-fit: cover;
            border-radius: 10px;
            z-index: -1;
            transition: all .3s ease;
        }

        .prog-lib-zone{
            bottom: 0;
            background: #000;
            padding: 20px 10px 10px 20px;
            width: 100%;
            text-align: left;
            background: linear-gradient(180deg, rgba(0,0,0,0.01) 0%, rgba(0,0,0,.6) 100%);
        }

        .prog-lib-zone h3{
            font-size: 1.3em;
            font-weight: bold;
        }

        .prog-zone:hover img{
            transform: scale(1.1);
        }
    </style>
</head>

<body class="authentication-bg">

    <div class="d-flex float-right">

        <div class="dropdown d-inline-block pr-3">

            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="rounded-circle header-profile-user" src="{{isset(Auth::user()->img) ? asset('assets/uploads/users/'.Auth::user()->img) : asset('assets/user.jpeg')}}" alt="{{Auth::user()->name}}">
                <span class="d-none d-xl-inline-block ml-1 font-weight-medium font-size-15">{{Auth::user()->name}}</span><i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="uil uil-sign-out-alt font-size-18 align-middle mr-1 text-muted text-danger"></i>
                    <span class="align-middle">Déconnexion</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </div>

        </div>

    </div>

    <div class="my-5 pt-sm-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="home-wrapper">

                    @if(count($user->programmes)>0)
                        <h3 class="mt-5">Programme(s) immobilier(s) autorisé</h3>
                        <p>Choisissez un programme pour continuer.</p>

                        <div class="row">
                            @foreach($user->programmes as $program)
                            <div class="col-md-4">
                                <div class="card mt-4 maintenance-box">
                                    <div class="card-body prog-zone p-0 m-0">
                                        <img src="{{isset($program->img) ? asset('assets/uploads/programmes/'.$program->img) : asset('assets/user.jpeg')}}" alt="{{$program->libelle}}">
                                        <a href="{{route('cpprogram.item',$program->slug)}}" class="stretched-link"></a>
                                        <div class="prog-lib-zone position-absolute">
                                            <h3 class="text-white">{{$program->libelle}}</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- Simple card -->
                            </div>
                            @endforeach
                        </div>
                    @else
                        <h3 class="mt-5">Aucun programme(s)immobilier(s) autorisé.</h3>
                        <p>Veuillez contactez l'administrateur !</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->
</div>

<!-- JAVASCRIPT -->
<script src="{{asset('assets/libs/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('assets/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{asset('assets/libs/node-waves/waves.min.js')}}"></script>
<script src="{{asset('assets/libs/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery.counterup/jquery.counterup.min.js')}}"></script>

<!-- owl.carousel js -->
<script src="{{asset('assets/libs/owl.carousel/owl.carousel.min.js')}}"></script>
<!-- init js -->
<script src="{{asset('assets/js/pages/auth-carousel.init.js')}}"></script>

<script src="{{asset('assets/js/app.js')}}"></script>

</body>
</html>
