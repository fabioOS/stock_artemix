@extends('chef_projet.layouts.app')

@section('page_title')
    Sous traitant contrat
@stop

@section('content_title')
    Gestion des contrats
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Sous-traitant</li>
        <li class="breadcrumb-item active">Contrat</li>
    </ol>
@stop

@section('other_css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row mb-4">
        <div class="col-xl-3">
            <div class="card h-100">
                <div class="card-body">
                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Nom</h5>
                        <p>{{Str::upper($sous->nom)}}</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Téléphone</h5>
                        <p>{{Str::upper($sous->contact)}}</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Contrat</h5>
                        <p><a href="{{asset('assets/uploads/contrats/'.$sous->contrat)}}" target="_blank" download="Contract_{{$sous->nom}}.pdf">Contract_{{$sous->nom}}.pdf</a></p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Date d'ajout</h5>
                        <p>{{$sous->created_at->format('d/m/Y')}}</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Villas assignés</h5>
                        @foreach($sous->lots as $itemlot)
                            @php $lot = \App\ProgrammeLot::where('id',$itemlot->lot_id)->with('actifs')->first(); @endphp
                            <div class="badge badge-soft-primary font-size-12"> Lot {{$lot->lot}} <br> {{$lot->actifs->libelle}}</div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>

        <div class="col-xl-9 gstock">
            <div class="card mb-0">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    @foreach($actifs as $k=>$actif)
                    @php //$item = \App\ProgrammeLot::where('id',$lot->lot_id)->first();@endphp
                        <li class="nav-item">
                            <a class="nav-link {{$k==0 ? 'active' :''}}" data-toggle="tab" href="#lot{{$k}}" role="tab">
                                <i class="uil uil-home font-size-20"></i>
                                <span class="d-none d-sm-block">{{$actif->actifs->libelle}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <!-- Tab content -->
                <div class="tab-content p-4">
                    @foreach($actifs as $k=>$actif)
                        @php //$item = \App\ProgrammeLot::where('id',$lot->lot_id)->first();
                            $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$actif->actif_id)->where('soustraitant_id',$sous->id)->with('corpslots')->orderBy('corpsetats_id','asc')->get();
                            //dd($corpsetats);
                        @endphp
                        <div class="tab-pane {{$k==0 ? 'active' :''}}" id="lot{{$k}}" role="tabpanel">

                            <div class="mb-5">
                                <a href="#" data-toggle="modal" data-target="#addct_straitant" onclick="displayModalEdt('{{$actif->actif_id}}')" class="btn btn-success waves-effect waves-light">
                                    <i class="uil uil-cog font-size-20"></i> Affecter les corps d'etat
                                </a>
                            </div>

                            @foreach($corpsetats as $j=>$corps)
                            <div class="card border border-primary">
                                <a href="#main_phase{{$k}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$k}}">
                                    <div class="p-4">
                                        <div class="media align-items-center">
                                            <div class="mr-3">
                                                <div class="avatar-xs">
                                                    <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                                </div>
                                            </div>
                                            <div class="media-body  overflow-hidden">
                                                <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="action position-absolute" style="top: 30px; right: 5%;">
                                    <li class="list-inline-item">
                                        <a href="{{route('cphome.st.lot.dqe.delete',$corps->id)}}" class="delete editePhase1DQE px-2 text-primary" data-placement="top" title="" data-original-title="Supprimer">
                                            <i class="uil uil-trash font-size-18"></i>
                                        </a>
                                    </li>
                                </div>

                            </div><!-- card -->
                            @endforeach
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>

    <div id="addct_straitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Affecter les corps d'etat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEdit" action="{{route('cphome.st.lot.dqe.store')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Corps d'etat <span class="text-danger">*</span></label>
                            <br>
                            <select multiple="multiple" size="10" name="corpsetats[]" id="corpsetats" title="duallistbox_demo1[]">
{{--                            <select class="form-control select2-multiple select2" multiple="multiple" style="width: 100%;" id="corpsetats" name="corpsetats[]" required>--}}
                            </select>
                            <input type="hidden" id="typeid" name="typeid">
                            <input type="hidden" id="ctid" name="stid">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet corps d\'etat",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalEdt(id){
            $("#formEdit")[0].reset();
            //console.log(id);
            var ctid = {{$sous->id}};
            $("#typeid").val(id);
            $("#ctid").val(ctid);

            var url = '{{route("cphome.get.corps", [":id",":lots"])}}'
            url=url.replace(':id',ctid);
            url=url.replace(':lots',id);
            $.get(url, function (data) {
                //console.log(data);
                var optionData ='';
                for (var i = 0; i < data.length; i++){
                    optionData+='<option value="'+data[i].id+'">'+data[i].libelle +'</option>';
                }
                $('#corpsetats').html(optionData);

                var demo1 = $('select[name="corpsetats[]"]').bootstrapDualListbox({
                    nonSelectedListLabel: 'Liste des corps d\'etat',
                    selectedListLabel: 'Corps d\'etat Sélectionnés',
                    preserveSelectionOnMove: 'moved',
                    moveAllLabel: 'Tout déplacer',
                    removeAllLabel: 'Enlever tout'
                });

            });

        }


    </script>
@endsection
