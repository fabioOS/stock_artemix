@extends('chef_projet.layouts.app')

@section('page_title')
    Contrat sous-traitant en attente de traitement
@stop

@section('content_title')
    Contrat sous-traitant en attente de traitement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Sous-traitant</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        @if(1==2)
        <div>
            <button type="button" data-toggle="modal" data-target="#addSous_traitant" class="btn btn-success waves-effect waves-light mb-5"><i class="mdi mdi-plus mr-1"></i> Ajouter un sous-traitant</button>
        </div>
        @endif

        <div class="table-responsive mb-4 gstock">
            <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                <tr class="bg-transparent">
                    <th>Prestataire</th>
                    <th>Contrat</th>
                    <th>Villa à charge</th>
                    {{-- <th>Téléphone</th> --}}
                    <th>Date d'ajout</th>
                    <th>Etat</th>
                    <th style="width: 220px!important;">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contratsousts as $k=>$contrat)
                    <tr class="font-size-16">
                        <td class="font-weight-bold">{{Str::upper($contrat->straitant->nom)}}</td>
                        <td>{{$contrat->libelle}} <a href="{{asset('assets/uploads/contrats/'.$contrat->contrat)}}" target="_blank" title="Voir le contrat"><i class="fa fa-file-pdf"></i></a></td>
                        <td>
                            <button type="button" onclick='displayModalLot("{{$contrat->id}}","{{Str::upper($contrat->straitant->nom)}}")' data-toggle="modal" data-target="#lots_straitant" class="btn btn-success waves-effect waves-light">
                                <i class="fa fa-eye mr-1"></i>
                                Voir les villas {{$contrat->lots ? '('.$contrat->lots->count().')': ''}}
                            </button>
                            {{--@foreach($contrat->lots as $lot)
                                @php $item = \App\ProgrammeLot::where('id',$lot->lot_id)->first();@endphp
                                <span class="badge badge-pill badge-primary font-size-14">{{$item->lot}}</span>
                            @endforeach--}}
                        </td>
                        {{-- <td>{{$contrat->contact}}</td> --}}
                        <td>{{$contrat->created_at->format('d/m/Y')}}</td>
                        <td>
                            @if($contrat->etat == 0)
                                <span class="text-danger"> Rejeter</span>
                            @elseif($contrat->etat == 1)
                                <span class="text-warning"> En attente de validation</span>
                            @else
                                <span class="text-success"> Actif</span>
                            @endif
                        </td>
                        <td>
                            {{-- <a href="{{route("cphome.st.valid",encrypt($contrat->id))}}" class="btn btn-success waves-effect waves-light btn-sm">
                                <i class="uil uil-check mr-1"></i> Validé
                            </a> --}}

                            <a href="{{route("cphome.st.show",$contrat->slug)}}" class="btn btn-success waves-effect waves-light btn-sm">
                                <i class="uil uil-eye mr-1"></i> Voir plus
                            </a>

                            {{-- <button type="button" onclick='displayModalRejet("{{encrypt($contrat->id)}}")' data-toggle="modal" data-target="#rejectbon" class="btn btn-danger waves-effect waves-light btn-sm" title="Rejeter le sous-traitant"> <i class="fa fa-trash mr-1"></i> Rejeter</button> --}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- end table -->
    </div>
</div>

    @if(1==2)
    <div id="addSous_traitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un sous-traitant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('cphome.st.store')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nom du sous-traitant <span class="text-danger">*</span></label>
                            <input type="text" name="nom" required class="form-control"/>
                        </div>

<!--                        <div  class="form-group">
                            <label class="control-label">Lots à charge <span class="text-danger">*</span></label>
                            <br>
                            <select class="form-control select2-multiple select2" multiple="multiple" style="width: 100%;" name="lot[]" required>
                                foreach($lots as $lot)
                                    <option value="{$lot->id}}">lot {$lot->lot}}</option>
                                endforeach
                            </select>
                        </div>-->
                        <div class="form-group">
                            <select multiple="multiple" size="10" name="lot[]" title="duallistbox_demo1[]">
                                @foreach($lots as $lot)
                                    <option value="{{$lot->id}}">Lot {{$lot->lot}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Téléphone du responsable <span class="text-danger">*</span></label>
                            <input type="text" name="phone" required class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="contrat">Contrat <span class="text-danger">*</span></label>
                            <input type="file" class="form-control" required name="contrat" id="contrat" accept=".pdf">
                            <p class="help-block">Format accepté: .PDF</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>
    @endif

    <div id="lots_straitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Sous-traitant <span class="text-muted" id="sstraitantlb"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="listlot">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter du sous-traitant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEditRejet" action="{{route('cphome.st.reject')}}" method="post">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif du rejet <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="stid" id="stid" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
    {{--select soustraitant select --}}
{{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet sous-traitant",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des lots',
            selectedListLabel: 'Lots Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });
        /*$("#demoform").submit(function() {
            alert($('[name="duallistbox_demo1[]"]').val());
            return false;
        });*/

        function displayModalLot(id,prest){
            //console.log(id);
            var url = '{{route("getLotStraitant",":id")}}'
            url=url.replace(':id',id);

            $('#sstraitantlb').html(prest);

            $.get(url, function (data) {
                //console.log(data);
                var optionData ='';
                for (var i = 0; i < data.length; i++){
                    //optionData+='<option value="'+data[i].id+'">'+data[i].libelle +'</option>';
                    optionData += '<div class="col-xl-4 col-sm-6">' +
                        '<div class="card text-center">' +
                        '<div class="card-body">' +
                        '<h5 class="font-size-16 mb-1">Lot '+data[i].lot+'</h5> ' +
                        '<p class="text-muted mb-2">'+data[i].type+'</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                $('#listlot').html(optionData);
            });

        }

        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

    </script>

@endsection
