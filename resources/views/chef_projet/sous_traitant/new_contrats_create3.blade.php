@extends('chef_projet.layouts.app')

@section('page_title')
    Contrat 3/3
@stop

@section('content_title')
    Contrat 3/3
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Sous-traitant</li>
        <li class="breadcrumb-item">Contrat</li>
        <li class="breadcrumb-item active">Etape 3/3</li>
    </ol>
@stop

@section('other_css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

    <style>
        .border-color{
            background: #ff0101;
        }
    </style>
@endsection

@section('content')

<div class="row mb-4">
    <div class="col-xl-3">
        <div class="card h-100">
            <div class="card-body">
                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Nom</h5>
                    <p>{{$stcontrat->nom}}</p>
                </div>

                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Téléphone</h5>
                    <p>{{$stcontrat->contact}}</p>
                </div>

                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Libelle du contrat</h5>
                    <p>{{$vars['libellctt']}}</p>
                </div>

                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Villas assignés</h5>
                    @php $lots = $vars['lot_id'] @endphp
                    @foreach($lots as $itemlot)
                        @php $lot = \App\ProgrammeLot::where('id',$itemlot)->with('actifs')->first(); @endphp
                        <div class="badge badge-soft-primary font-size-12"> Villa {{$lot->lot}} <br> {{$lot->actifs->libelle}}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-9 gstock">
        <div class="card mb-0">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="p-2">Montant par villas assignés </h4>
                    <form action="{{route('cphome.st.n-contrat.store.end')}}" method="post" enctype="multipart/form-data" id="formid">@csrf
                        <input type="hidden" name="firststep" value="{{$vars['stt_id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="contrat">Contrat <span class="text-danger">*</span></label>
                                <input type="file" class="form-control" required name="contrat" id="contrat" accept=".pdf">
                                <p class="help-block">Format accepté: .PDF</p>
                            </div>

                            <div class="form-group">
                                <label for="mttctr">Montant total du contrat <span class="text-danger">*</span></label>
                                <input type="text" name="mttctr" id="mttctr" class="form-control idmttctr" required>
                            </div>

                            <hr>
                            @foreach ($vars['lotsandce'] as $k=>$allce)
                            {{-- @dump($vars['lotsandce'],$k,$itemlot) --}}

                                @php $lot = \App\ProgrammeLot::where('id',$k)->with('actifs')->first(); @endphp

                                <div class="media align-items-center bg-light p-3">
                                    <div class="mr-3">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded-circle font-size-22">
                                                <i class="uil uil-home"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="media-body overflow-hidden">
                                        <h6 class="font-size-14 mb-0">Villa {{$lot->lot}}</h6>
                                        <small class="text-muted text-truncate mb-0">{{$lot->actifs->libelle}}</small>
                                    </div>
                                </div>

                                <div class="p-4">
                                    @foreach ($allce as $itemce)
                                        @php $ce = \App\ProgrammeCorpsetat::where('id',$itemce)->where('programme_id',$lot->programme_id)->where('actif_id',$lot->actif_id)->select('id','libelle')->first();@endphp

                                        @if($ce)
                                        <div class="form-group">
                                            <label for="montant_{{$ce->id}}">{{$ce->libelle}} <span class="text-danger">*</span></label>
                                            <input type="text" required name="montant_{{$k}}_{{$ce->id}}" class="form-control idmttctr_me" id="montant_{{$ce->id}}">
                                        </div>
                                        @endif

                                    @endforeach
                                </div>
                            @endforeach
                        </div>

                        <input type="hidden" name="listlots" value="{{json_encode($vars['lot_id'])}}">
                        <input type="hidden" name="vars" value="{{json_encode($vars)}}">
                        <div id="loader" class="dispayNone float-right">
                            <div class="spinner-border text-primary m-1" role="loader">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>

                        <div id="sectbtn" class="modal-footer">
                            <input type="hidden" name="libellectt" value="{{$vars['libellctt']}}">
                            <a href="{{route('cphome.st.index')}}" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</a>
                            <button type="button" onclick="sendForm()" class="btn btn-primary waves-effect waves-light"> Enregistrer </button>
                        </div>

                    </form>
                    <!-- end table -->
                </div>
            </div>

        </div>
    </div>
</div>

@endsection


@section('other_js')
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{ asset('assets/js/imask.js') }}"></script>
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.idmttctr').each(function(){
                IMask(this, {
                    mask: Number,
                    min: 0,
                    max: 100000000000,
                    thousandsSeparator: ' '
                });
            });

            $('.idmttctr_me').each(function(){
                IMask(this, {
                    mask: Number,
                    min: 0,
                    max: 100000000000,
                    thousandsSeparator: ' '
                });
            });
        });

        function sendForm(){
            var contrat = $('#contrat').val();
            var iddd = 0;
            //verifier si tous les inputs du form sont remplis sinon on arrete

            if(!contrat){
                $('#contrat').focus();
                $('#contrat').css('border-color','red');
                swal("Oups!", "Veuillez renseignez le champs Contrat");
                return false;
            }else{
                $('#contrat').css('border-color','#ced4da');
            }

            //montant total
            var mttctr = $('#mttctr').val();
            if(!mttctr){
                iddd = iddd + 1;
                $('#mttctr').focus();
                $('#mttctr').css('border-color','red');
                swal("Oups!", "Veuillez renseignez le champs Montant total du contrat");
                return false;
            }else{
                $('#mttctr').css('border-color','#ced4da');
            }

            //verifier si la somme des montants des corps d'etat est egale au montant total du contrat
            var somme = 0;
            $('.idmttctr_me').each(function(){
                somme = somme + parseInt($(this).val().replace(/\s/g, ''));
            });

            mttctr = parseInt(mttctr.replace(/\s/g, ''));
            if(somme != mttctr){
                iddd = iddd + 1;
                swal("Erreur", "La somme des montants des corps d'état doit être égale au montant total du contrat", "error");
                return false;
            }else{
                $('#mttctr').css('border-color','#ced4da');
            }

            $('#formid').find('input[type="text"]').each(function(){
                // console.log($(this).val());
                if($(this).val() == ''){
                    iddd = iddd + 1;
                    $(this).focus();
                    $(this).css('border-color','red');
                    return false;
                }else{
                    $(this).css('border-color','#ced4da');
                }
            });

            if(iddd== 0){
                $('#sectbtn').hide();
                $('#loader').show();
                $('#formid').submit();
            }
        }


    </script>
@endsection
