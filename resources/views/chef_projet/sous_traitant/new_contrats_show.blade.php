@extends('chef_projet.layouts.app')

@section('page_title')
    Sous traitant contrat
@stop

@section('content_title')
    Gestion des contrats
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Sous-traitant</li>
        <li class="breadcrumb-item active">Contrat</li>
    </ol>
@stop

@section('other_css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row mb-4">
        <div class="col-xl-3">
            <div class="card h-100">
                <div class="card-body">
                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Nom</h5>
                        <p>{{Str::upper($contrat->straitant->nom)}}</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Téléphone</h5>
                        <p>{{Str::upper($contrat->straitant->contact)}}</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Contrat</h5>
                        <p><a href="{{asset('assets/uploads/contrats/'.$contrat->contrat)}}" target="_blank" download="Contract_{{$contrat->libelle}}.pdf">Contract_{{$contrat->libelle}}.pdf</a></p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Date contrat</h5>
                        <p>{{$contrat->straitant->created_at->format('d/m/Y')}}</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Villas assignés</h5>
                        @foreach($contrat->lots as $itemlot)
                            @php $lot = \App\ProgrammeLot::where('id',$itemlot->lot_id)->with('actifs')->first(); @endphp
                            <div class="badge badge-soft-primary font-size-12"> Villa {{$lot->lot}} <br> {{$lot->actifs->libelle}}</div>
                        @endforeach
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Statut</h5>
                        <p>
                            @if($contrat->etat == 0)
                                <span class="text-danger"> Rejeté</span>
                            @elseif($contrat->etat == 1)
                                <span class="text-warning"> En attente de validation</span>
                            @else
                                <span class="text-success"> Validé</span>
                            @endif
                        </p>
                    </div>
                    @if(Auth::user()->role_id==5)
                    <div class="text-muted mb-4">
                        @if($contrat->etat == 1)
                            <div class="row gstock" id="btnaction">
                                <div class="mr-1">
                                    <a href="{{route('cphome.st.contrat.valid',encrypt($contrat->id))}}" class="btn btn-success valid" title="Valider le contrat"> <i class="fa fa-check-circle"></i> </a>
                                </div>
                                <div>
                                    <button type="button" data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter le contrat"> <i class="fa fa-trash"></i> </button>
                                </div>
                            </div>

                            <div id="loader" class="dispayNone">
                                <div class="spinner-border text-primary m-1" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        @endif
                    </div>
                    @endif

                </div>
            </div>
        </div>

        <div class="col-xl-9 gstock">
            <div class="card mb-0">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    @foreach($actifs as $k=>$actif)
                        <li class="nav-item">
                            <a class="nav-link {{$k==0 ? 'active' :''}}" data-toggle="tab" href="#lot{{$k}}" role="tab">
                                <i class="uil uil-home font-size-20"></i>
                                <span class="d-none d-sm-block">{{$actif->actifs->libelle}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <!-- Tab content -->
                <div class="tab-content p-4">
                    @foreach($actifs as $k=>$actif)
                        @php //$item = \App\ProgrammeLot::where('id',$lot->lot_id)->first();
                            $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$actif->actif_id)->where('soustraitant_id',$contrat->soustraitant_id)->where('soustraitantcontrat_id',$contrat->id)->with('corpslots')->orderBy('corpsetats_id','asc')->get();
                        @endphp
                        <div class="tab-pane {{$k==0 ? 'active' :''}}" id="lot{{$k}}" role="tabpanel">
                            @foreach($corpsetats as $j=>$corps)
                                <div class="card border border-primary">
                                    <a href="#main_phase{{$k}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$k}}">
                                        <div class="p-4">
                                            <div class="media align-items-center">
                                                <div class="mr-3">
                                                    <div class="avatar-xs">
                                                        <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                                    </div>
                                                </div>
                                                <div class="media-body  overflow-hidden">
                                                    <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="action position-absolute" style="top: 30px; right: 5%;">
                                        <li class="list-inline-item">
                                            {{-- <a href="{{route('chefchantier.st.lot.dqe.delete',$corps->id)}}" class="delete editePhase1DQE px-2 text-primary" data-placement="top" title="" data-original-title="Supprimer">
                                                <i class="uil uil-trash font-size-18"></i>
                                            </a> --}}
                                        </li>
                                    </div>

                                </div><!-- card -->
                            @endforeach
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>

    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter le contrat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEdit" action="{{route('cphome.st.contrat.rejeter')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="stid" value="{{encrypt($contrat->id)}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="blockbtn">
                            <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                        </div>
                        <div id="loader1" class="dispayNone">
                            <div class="spinner-border text-primary m-1" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet corps d\'etat",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function submitform(){
            var desc = $('#motif').val();
            if(desc == ''){
                swal("Oups!", "Veuillez renseignez la motif.");
                return false;
            }
            $('#blockbtn').hide();
            $('#loader1').show();
            // $('#submitform').submit();
            document.getElementById('formEdit').submit();

        }


    </script>
@endsection
