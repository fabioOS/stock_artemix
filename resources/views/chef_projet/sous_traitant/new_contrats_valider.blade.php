@extends('chef_projet.layouts.app')

@section('page_title')
    Liste des contrats validés
@stop

@section('content_title')
    Gestion des contrats validés
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Sous-traitant</li>
        <li class="breadcrumb-item active">Contrat</li>
    </ol>
@stop

@section('other_css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row mb-4">
        <div class="col-xl-12 gstock">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive mb-4 gstock">
                        <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                            <thead>
                            <tr class="bg-transparent">
                                <th>Num</th>
                                <th>N°Référence</th>
                                <th>Sous-traitant</th>
                                <th>Contrat</th>
                                <th>Montant</th>
                                <th>Villa à charge</th>
                                <th>Date d'ajout</th>
                                <th>Statut</th>
                                <th style="width: 220px!important;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contrats as $k=>$contrat)
                                <tr class="font-size-16">
                                    <td>{{$k+1}}</td>
                                    <td class="font-weight-bold">{{Str::upper($contrat->slug)}}</td>
                                    <td>{{$contrat->straitant->nom}}</td>
                                    <td>{{$contrat->libelle}} - <a href="{{asset('assets/uploads/contrats/'.$contrat->contrat)}}" target="_blank" download="Contract_{{$contrat->nom}}.pdf"><i title="Voir le contrat" class="fa fa-file-pdf text-primary"></i></a></td>
                                    <td>@price($contrat->montantctr)</td>
                                    <td>
                                        <button type="button" onclick='displayModalLot("{{$contrat->id}}","{{Str::upper($contrat->straitant->nom)}}")' data-toggle="modal" data-target="#lots_straitant" class="btn btn-success waves-effect waves-light"><i class="fa fa-eye mr-1"></i>
                                            Voir les villas {{$contrat->lots ? '('.$contrat->lots->count().')': ''}}
                                        </button>
                                    </td>

                                    <td>{{$contrat->created_at->format('d/m/Y')}}</td>
                                    <td>
                                        @if($contrat->etat == 0)
                                            <span class="text-danger"> Rejeter</span>
                                            <button title="Motif" type="button" onclick='displayModalMotif("{{$contrat->motif}}")' data-toggle="modal" data-target="#motifetat" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-eye"></i></button>
                                        @elseif($contrat->etat == 1)
                                            <span class="text-warning"> En attente de validation</span>
                                        @else
                                            <span class="text-success"> Actif</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route("cphome.st.show",$contrat->slug)}}" title="Détail" class="btn btn-primary waves-effect waves-light btn-sm">
                                            <i class="uil uil-eye"></i> Voir plus
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- end table -->
                </div>
            </div>
        </div>
    </div>

    <div id="lots_straitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Sous-traitant <span class="text-muted" id="sstraitantlb"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="listlot">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="motifetat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Motif <span class="text-muted"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center mb-5" id="motifetatvalu">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('other_js')
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet corps d\'etat",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

            var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des villas',
            selectedListLabel: 'Villas Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });


        });

        function displayModalLot(id,prest){
            //console.log(id);
            var url = '{{route("getLotStraitant",":id")}}'
            url=url.replace(':id',id);

            $('#sstraitantlb').html(prest);

            $.get(url, function (data) {
                //console.log(data);
                var optionData ='';
                for (var i = 0; i < data.length; i++){
                    //optionData+='<option value="'+data[i].id+'">'+data[i].libelle +'</option>';
                    optionData += '<div class="col-xl-4 col-sm-6">' +
                        '<div class="card text-center">' +
                        '<div class="card-body">' +
                        '<h5 class="font-size-16 mb-1">Villa '+data[i].lot+'</h5> ' +
                        '<p class="text-muted mb-2">'+data[i].type+'</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                $('#listlot').html(optionData);
            });

        }

        function displayModalMotif(motif){
            $('#motifetatvalu').html(motif);
        }

    </script>
@endsection
