@extends('chef_projet.layouts.app')

@section('page_title')
    Faire une demande de main d'oeuvre
@stop

@section('content_title')
    Demande de main d'oeuvre
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Main d'oeuvre</li>
        <li class="breadcrumb-item active">Paiement</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <h4 class="card-title m-2">Sous-traitant</h4>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">Nom</h5>
                                <p>{{Str::upper($sous->straitant->nom)}}</p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">Téléphone</h5>
                                <p>{{Str::upper($sous->straitant->contact)}}</p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">REFERENCE</h5>
                                <p>{{Str::upper($sous->slug)}}</p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">Contrat</h5>
                                <p><a href="{{asset('assets/uploads/contrats/'.$sous->contrat)}}" target="_blank" download="Contract_{{$sous->libelle}}.pdf">Contract_{{$sous->libelle}}.pdf</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                @foreach($sous->lots as $k=>$lot)
                                    @php $item = \App\ProgrammeLot::where('id',$lot->lot_id)->with('actifs')->first();@endphp
                                    <a class="nav-link mb-2 {{$k==0 ? 'active' :''}}" id="lot{{$k}}-tab" data-toggle="pill" href="#lot{{$k}}" role="tab" aria-selected="{{$k==0 ? 'true' :''}}">
                                        <i class="uil uil-home font-size-20"></i>
                                        <span class="">Villa {{$item->lot}} <br><span class="font-size-12">{{$item->actifs ?$item->actifs->libelle:''}}</span> </span>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">

                                @foreach($sous->lots as $k=>$lot)

                                    @php $vlot = \App\ProgrammeLot::where('id',$lot->lot_id)->first();
                                        $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$vlot->actif_id)->where('soustraitant_id',$lot->soustraitant_id)->where('soustraitantcontrat_id',$lot->soustraitantcontrat_id)->with('corpslots')->orderBy('corpsetats_id','asc')->get();
                                        //dd($corpsetats);
                                    @endphp

                                    <div class="tab-pane fade {{$k==0 ? 'active show' :''}}" id="lot{{$k}}" role="tabpanel" aria-labelledby="lot{{$k}}-tab">
                                        @if(count($corpsetats)>0)
                                            <div class="btn-toolbar mb-3" role="toolbar">
                                                <div class="btn-group mr-2 mb-2 mb-sm-0">
                                                    <a href="{{route('cphome.st.lot.paye',[$sous->id,$lot->lot_id])}}" class="btn btn-primary waves-light waves-effect"><i class="fas fa-money-bill"></i> Effectuer un paiement</a>
                                                </div>
                                            </div>


                                            @foreach($corpsetats as $j=>$corps)
                                                @php
                                                    // $check = \App\Avancement::where([['programme_id',$sous->programme_id],['lot_id',$lot->lot_id],['soustraitant_id',$sous->id],['corpsetat_id',$corps->corpsetats_id]])->orderBy('id','desc')->first();
                                                    $check = \App\Avancement::where([['programme_id',$sous->programme_id],['lot_id',$lot->lot_id],['corpsetat_id',$corps->corpsetats_id]])->orderBy('id','desc')->first();
                                                    $tau = $check ? $check->taux : 0;
                                                @endphp
                                                <div class="card border border-{{$tau>=100 ? 'success' : 'primary'}}">
                                                    <a class="text-dark" aria-expanded="true" aria-controls="main_phase{{$k}}">
                                                        <div class="p-4">
                                                            <div class="media align-items-center">
                                                                <div class="mr-3">
                                                                    <div class="avatar-xs">
                                                                        <div class="avatar-title rounded-circle bg-soft-primary text-primary">0{{$j+1}}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="media-body  overflow-hidden">
                                                                    <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                                    <div class="tilte mr-auto p -2">
                                                                        <span class="text-default font-size-13">
                                                                            Coût Total: <b>@price($corps->corpslots->prix) <sup>FCFA</sup></b> &nbsp; | &nbsp;
                                                                            Coût payé: <b>@price(($tau/100) * $corps->corpslots->prix)  <sup>FCFA</sup></b> (<span class="text-warning">{{$tau}}%</span>) &nbsp; | &nbsp;
                                                                            Taux d'avancement: <b class="text-warning">{{$tau}}%</b>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="card border border-primary">
                                                <div class="p-4">
                                                    <div class="media align-items-center">
                                                        <div class="media-body overflow-hidden">
                                                            <a href="{{route('cphome.st.n-contrat',$sous->soustraitant_id)}}" class="btn btn-primary btn-rounded waves-effect waves-light" data-toggle="button" aria-pressed="false">
                                                                AJOUTER LES CORPS D'ETAT AU CONTRAT
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection


@section('other_js')
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });

        function displayModalEdt(corpid,lotid){
            $("#formEdit")[0].reset();
            //console.log(id);
            var ctid = {{$sous->id}};
            $("#lotid").val(lotid);
            $("#corpsid").val(corpid);
            $("#ctid").val(ctid);

        }

    </script>
@endsection
