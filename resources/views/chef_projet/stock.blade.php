@extends('chef_projet.layouts.app')

@section('page_title')
    Gestion du stock magasin
@stop

@section('content_title')
    Gestion du stock magasin
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item active">Stock magasin</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            {{-- <div>

                <div class="float-left">
                    <form class="form-inline mb-0">
                        <label class="my-1 mr-2" for="order-selectinput">Seuil</label>
                        <select class="custom-select my-1" id="order-selectinput">
                            <option selected>Tous</option>
                            <option value="1">suffisant</option>
                            <option value="2">Moyen</option>
                            <option value="2">Rupture</option>
                        </select>
                    </form>

                </div>
            </div> --}}
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>Matière</th>
                        <th>Disponibilité en stock</th>
                        <th>Unité de mésure</th>
<!--                        <th>Date dernière sortie</th>
                        <th>Qté dernière sortie</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stocks as $k=> $stock)
                        <tr class="font-size-16">
                        <td>{{$stock->produits? $stock->produits->libelle : ''}}</td>
                        <td>
                            @if($stock->qte > $stock->produits->seuil)
                                <span class="badge badge-pill badge-success font-size-14">{{$stock->qte}}</span>
                            @elseif($stock->qte <= $stock->produits->seuil)
                                <span class="badge badge-pill badge-warning font-size-14">{{$stock->qte}}</span>
                            @else
                                <span class="badge badge-pill badge-danger font-size-14">{{$stock->qte}}</span>
                            @endif

                        </td>
                        <td>{{$stock->produits ? $stock->produits->libelle_type_pdt : ''}}</td>

<!--                        <td>30/08/2021</td>
                        <td><span class="badge badge-pill badge-primary font-size-14">20</span></td>-->


                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>
@endsection


@section('other_js')

@endsection
