@extends('chef_projet.layouts.app')

@section('page_title')
    Transactions détail
@stop

@section('content_title')
    Transactions détail
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Transactions</li>
        <li class="breadcrumb-item active">Détail</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT SOUS-TRAITANT</h4>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="text-muted">
                                <h5 class="font-size-15 mb-2">SOUS-TRAITANT</h5>
                                <p>{{$trans->soustraitant->nom}}</p>

                                <div>
                                    <h5 class="font-size-16 mb-2">CONTACT SOUS-TRAITANT</h5>
                                    <p>{{$trans->soustraitant->contact}}</p>
                                </div>

                                @if($trans->contrat)
                                <div>
                                    <h5 class="font-size-16 mb-2">CONTRACT</h5>
                                    <p> {{$trans->contrat->slug}} <br><a title="Voir le contrat" href="{{asset('assets/uploads/contrats/'.$trans->contrat->contrat)}}" target="_blank">{{$trans->contrat->libelle}}.pdf</a></p>
                                </div>
                                @endif

                                <div>
                                    <h5 class="font-size-16 mb-1">LOT/ILOT</h5>
                                    @php $typ = \App\ProgrammeActif::where('id',$trans->lot->actif_id)->first() @endphp
                                    <p>{{$trans->lot->lot}} / {{$trans->lot->ilot}} ({{$typ->libelle}})</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">AGENT</h5>
                                    <p>{{$trans->user->name}}</p>
                                </div>
                            </div>

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">DATE</h5>
                                    <p>{{$trans->created_at->format('d-m-Y H:i')}}</p>
                                </div>
                            </div>

                            @if($trans->rattachment)
                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">FICHE D'ATTACHEMENT  :</h5>
                                    <p><a title="Voir la fiche d'attachement" href="{{asset('assets/uploads/rattachment/'.$trans->rattachment)}}"  target="_blank">Voir la fiche d'attachement</a></p>
                                </div>
                            </div>@endif

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">STATUS</h5>
                                    <p>
                                        @if($trans->etat==1)
                                            <span class="text-warning">En attente de traitement du<br>
                                                {!! $trans->nivalid==1 ? 'CHEF PROJET' : 'DP' !!}
                                            </span>
                                        @elseif($trans->etat==2)
                                            <span class="text-success">Validé</span>
                                        @else
                                            <span class="text-danger">Rejeté</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                            @if($trans->etat==2)
                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">ETAT DE PAIEMENT</h5>
                                    <p>
                                        @if($trans->status==1)
                                            <span class="text-danger">En attente de paiement</span>
                                        @else
                                            <span class="text-success">Payer</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                            @endif

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    @if($trans->etat == 1)
                                        @if($trans->actionbtn)
                                        <h5 class="font-size-15 mb-3 text-uppercase">Action </h5>

                                        <div class="gstock" id="btnaction">
                                            <a href="{{route("cphome.transac.waiting.valid",encrypt($trans->id))}}" class="btn btn-success valid mr-2" title="Valider la demande"> <i class="fa fa-check-circle"></i> </a>
                                            <button type="button" onclick='displayModalRejet("{{encrypt($trans->id)}}")' data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter la demande"> <i class="fa fa-trash"></i> </button>
                                        </div>
                                        <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status" style="float: right;">
                                            <span class="sr-only">Chargement...</span>
                                        </div>
                                        @endif

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">TAUX D'EVOLUTION</h4>
                    <hr>
                    <div class="py-2">
                        <div class="table-responsive">
                            <table class="table table-nowrap table-centered mb-0">
                                <thead>
                                <tr>
                                    <th style="width: 70px;">No.</th>
                                    <th>Corps d'etat</th>
                                    <th>Montant total</th>
                                    <th>Coût à payer</th>
                                    <th>Taux d'avancement</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listrans as $k=>$listran)
                                @php
                                    $couttotal = $listran->corpsetat->prix;
                                    if($listran->programme->type_rattachement == 2){
                                        $montant = \App\ProgrammeSoustraitantcontratMontant::where([['lot_id',$listran->lot_id],['soustraitant_id',$listran->soustraitant_id],['corpsetats_id',$listran->corpsetat_id]])->orderBy('id','desc')->first();
                                        $couttotal =$montant?$montant->montant : 0;
                                    }
                                @endphp

                                    <tr>
                                        <th scope="row">{{$k+1}}</th>
                                        <td>
                                            <h5 class="font-size-15 mb-1">{{$listran->corpsetat->libelle}}</h5>
                                        </td>
                                        <td>@price($couttotal) <sup>Fcfa</sup></td>
                                        <td>@price($listran->cout) <sup>Fcfa</sup></td>
                                        <td>{{round($listran->taux_execute,2)}} %</td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <th scope="row" colspan="4" class="border-0 text-right">Total</th>
                                    <td class="border-0"><h4 class="m-0">@price($trans->montant) Fcfa</h4></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="d-print-none mt-4">
                            <div class="float-right">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($trans->actionbtn)
    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter la transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEditRejet" action="{{route('cphome.transac.reject')}}" method="post">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif du rejet <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="stid" id="stid" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    @endif

@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        $(document).ready(function(){
            $('.gstock').on('click', '.valid', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment valider cette transaction.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#btnaction').hide();
                        $('#loader').show();
                        window.location = href;
                    }
                });
            });
        });


    </script>
@endsection
