@extends('chef_projet.layouts.app')

@section('page_title')
    Transactions
@stop

@section('content_title')
    Transactions
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Transactions</li>
    </ol>
@stop

@section('content')
    @include('_stats_evolution')


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="card-title mb-4">Transactions par sous-traitant</h4>
                    <table id="datatable-buttons" class="table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Trans. ID</th>
                            <th>Sous-traitant</th>
                            <th>Contrat</th>
                            <th>Villa</th>
                            <th>Montant payé</th>
                            <th>Taux d'avancement</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Etat de paiement</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lists as $k=>$list)
                            @php
                                $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->orderBy('corpsetats_id','asc')->where('etatdel',1)->count();
                            @endphp
                            <tr>
                                <td><a href="javascript: void(0);" class="text-body font-weight-bold">{{$list->slug}}</a> </td>
                                <td>{{$list->soustraitant->nom}}</td>
                                <td>
                                    @if ($list->contrat)
                                        <a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank">{{$list->contrat->slug}}</a>
                                    @endif
                                </td>
                                <td>{{$list->lot->lot}}</td>
                                <td>@price($list->montant) <sup>FCFA</sup></td>
                                <td>
                                    <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats != 0 ? round($list->taux/$corpsetats).'%' : '-'!!}</span>
                                </td>
                                <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                <td><span class="text-success">Validé</span></td>
                                <td>
                                    @if($list->status==2)
                                        <span class="badge badge-pill badge-success font-size-12">Payer</span>
                                    @else
                                        <span class="badge badge-pill badge-danger font-size-12">En attente de paiement</span>
                                    @endif
                                </td>
                                <td><a href="{{route('cphome.transac.item',encrypt($list->id))}}" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light"><i class="fa fa-eye"></i> Détail</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
@endsection
