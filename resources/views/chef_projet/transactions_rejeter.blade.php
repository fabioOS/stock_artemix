@extends('chef_projet.layouts.app')

@section('page_title')
    Transactions rejetées
@stop

@section('content_title')
    Transactions rejetées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Main d'oeuvre</li>
        <li class="breadcrumb-item active">Transactions rejetées</li>
    </ol>
@stop

@section('content')
    @include('_stats_evolution')


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table id="datatable-buttons" class="gstock table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Sous-traitant</th>
                            <th>Contrat</th>
                            <th>Villa</th>
                            <th>Montant payé</th>
                            <th>Taux d'avancement</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Motif</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lists as $k=>$list)
                            @php
                                $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                            @endphp
                            <tr>
                                <td>{{$list->soustraitant->nom}}</td>
                                <td><a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank">{{$list->contrat->slug}}</a></td>
                                <td>{{$list->lot->lot}}</td>
                                <td>@price($list->montant) <sup>FCFA</sup></td>
                                <td>
                                    <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats !=0 ? round($list->taux/$corpsetats).'%' : '-'!!}</span>
                                </td>
                                <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                <td><span class="text-danger">Rejeté</span></td>
                                <td>{{$list->motif}}</td>
                                <td>
                                    <div id="btnaction">
                                        <a href="{{route('cphome.transac.waiting.show',encrypt($list->id))}}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-eye"></i> Détail</a>
                                        @if(Auth::user()->role_id==3)
                                        <a href="{{route('cphome.transac.delete',encrypt($list->id))}}" class="btn btn-danger btn-sm waves-effect waves-light del"><i class="fa fa-trash"></i> Supprimer</a>
                                        @endif
                                    </div>
                                    <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                                        <span class="sr-only">Chargement...</span>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        $(document).ready(function(){

            $('.gstock').on('click', '.del', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment supprimer cette transaction.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#btnaction').hide();
                        $('#loader').show();
                        window.location = href;
                    }
                });
            });

        });
    </script>
@endsection
