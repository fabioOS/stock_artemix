@extends('chef_projet.layouts.app')

@section('page_title')
    Transactions en attente de traitement
@stop

@section('content_title')
    Transactions en attente de traitement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Transactions</li>
        <li class="breadcrumb-item active">Transactions en attente de traitement</li>
    </ol>
@stop

@section('content')
    @include('_stats_evolution')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table id="datatable-buttons" class="gstock table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                        <tr>
                            {{--<th>Trans. ID</th>--}}
                            <th>Sous-traitant</th>
                            <th>Contrat</th>
                            <th>Villa</th>
                            <th>Montant payé</th>
                            <th>Taux d'avancement</th>
                            <th>Date</th>
                            <th>Agent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lists as $k=>$list)
                            @php
                                $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                                // $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->with('corpslots')->where('etatdel',1)->count();
                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                            @endphp
                            <tr>
                                <td>{{$list->soustraitant->nom}}</td>
                                <td><a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank">{{$list->contrat->slug}}</a></td>
                                <td>{{$list->lot->lot}}</td>
                                <td>@price($list->montant) <sup>FCFA</sup></td>
                                <td>
                                    <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats !=0 ? round($list->taux/$corpsetats).'%' : '-'!!}</span>
                                </td>
                                <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                <td>{{$list->user->name}}</td>
                                <td>
                                    <span class="badge badge-pill badge-warning">En attente de validation par le<br>
                                        {{$list->nivalid==1 ? "CHEF PROJET" : "DP"}}
                                    </span>
                                </td>
                                <td>
                                    <a href="{{route('cphome.transac.waiting.show',encrypt($list->id))}}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-eye"></i> Détail</a>
                                    {{-- @if($list->etat == 1)
                                        @if(Auth::user()->role_id==5)
                                        <span id="btnaction">
                                            <a href="{{route("cphome.transac.waiting.valid",encrypt($list->id))}}" class="btn btn-success waves-effect waves-light btn-sm valid">
                                                <i class="uil uil-check mr-1"></i> Validé
                                            </a>

                                            <button type="button" onclick='displayModalRejet("{{encrypt($list->id)}}")' data-toggle="modal" data-target="#rejectbon" class="btn btn-danger waves-effect waves-light btn-sm" title="Rejeter la transaction"> <i class="fa fa-trash mr-1"></i> Rejeter</button>
                                        </span>
                                        <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                                            <span class="sr-only">Chargement...</span>
                                        </div>
                                        @endif
                                    @endif --}}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>


    {{-- @if(Auth::user()->role_id==5) --}}
    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter la transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEditRejet" action="{{route('cphome.transac.reject')}}" method="post">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif du rejet <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="stid" id="stid" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    {{-- @endif --}}
@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        $(document).ready(function(){

            $('.gstock').on('click', '.valid', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment valider cette transaction.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#btnaction').hide();
                        $('#loader').show();
                        window.location = href;
                    }
                });
            });

        });
    </script>
@endsection
