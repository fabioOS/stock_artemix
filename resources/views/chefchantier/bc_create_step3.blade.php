@extends('chefchantier.layouts.app')

@section('page_title')
    Créer un bon de sortie en stock
@stop

@section('content_title')
    Créer un bon de sortie en stock <br>&nbsp; <br> Etape 3/3
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Créer un bon de sortie en stock</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{route('chefchantier.demande.create')}}" class="btn btn-info btn-sm">
                <i class="fas fa-angle-double-left"></i> &nbsp; Retour
            </a>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form class="formdmd" enctype="multipart/form-data" action="{{route('chefchantier.demande.store')}}" method="post" id="DemForm" novalidate>
                        @csrf
                        <div class="form-group niveau-1">
                            <label for="">Sous traitant : </label>
                            <br>
                            <input class="form-control" disabled style="width: 100%;" value="{{$soustraitant->nom}}">
                            <input type="hidden" name="soustraitant_id" value="{{$soustraitant->id}}">
                        </div>

                        <div class="form-group niveau-1">
                            <label for="">Contrat : </label>
                            <br>
                            <input class="form-control" disabled style="width: 100%;" value="{{$soustraitant->itemcontrat->libelle}} ({{$soustraitant->itemcontrat->slug}})">
                            <input type="hidden" name="contrat_id" value="{{$soustraitant->itemcontrat->id}}">
                        </div>


                        <hr>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    @foreach($soustraitant->lots as $k=>$lot)
                                        @php $item = \App\ProgrammeLot::where('id',$lot->lot_id)->with('actifs')->first();@endphp
                                        <a class="nav-link mb-2 border {{$k==0 ? 'active' :''}}" id="v-pills-{{$k}}-tab" data-toggle="pill" href="#v-pills-{{$k}}" role="tab" aria-controls="v-pills-{{$k}}" aria-selected="{{$k==0 ? 'true' : 'false'}}">
                                            Villa {{$item->lot}} <br>
                                            <small>{{$item->actifs->libelle}}</small>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-9">

                            <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabCentent">
                                @foreach($soustraitant->lots as $k=>$lot)
                                    @php
                                        //$item = \App\ProgrammeLot::where('id',$lot->lot_id)->first();
                                        $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$lot->lot->actif_id)->where('soustraitant_id',$lot->soustraitant_id)->where('soustraitantcontrat_id',$lot->soustraitantcontrat_id)->where('etatdel',1)->with('corpslots')->get();
                                        // dd($corpsetats);
                                    @endphp
                                        <div class="tab-pane fade {{$k==0 ? 'active show' :''}}" id="v-pills-{{$k}}" role="tabpanel" aria-labelledby="v-pills-{{$k}}-tab">
                                            <input type="hidden" value="{{$lot->lot_id}}" name="lot_id[]">
                                            @if (count($corpsetats)>0)
                                                @foreach($corpsetats as $j=>$corps)

                                                    <input type="hidden" name="corps_id[{{$lot->lot_id}}][]" value="{{$corps->corpsetats_id}}">

                                                    <div class="card border border-primary">
                                                        <a href="#main_phase{{$corps->id}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$corps->id}}">
                                                            <div class="p-4">
                                                                <div class="media align-items-center">
                                                                    <div class="mr-3">
                                                                        <div class="avatar-xs">
                                                                            <div class="avatar-title rounded-circle bg-soft-primary text-primary">0{{$j+1}}</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="media-body  overflow-hidden">
                                                                        <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                                    </div>
                                                                    <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div id="main_phase{{$corps->id}}" class="collapse show" data-parent="#main_phase{{$corps->id}}">
                                                            {{-- Make datatables responsive by adding the <code>.table-responsive</code> --}}
                                                            <div class="table-responsive">
                                                                <div class="p-4 border-top">
                                                                <table class="medatatable" class="table table-centered table-nowrap mb-0" style="width: 100%">
                                                                    <thead class="thead-light" style="display: none">
                                                                        <tr>
                                                                            <th scope="col">Libelle</th>
                                                                            <th scope="col">Quantité</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {{-- LES LIBELLES ET PRODUIT DU CORPS D'ETAT --}}
                                                                        @php $libelcorpsetat = \App\ProgrammeCorpsetatLibelle::where('programme_id',Session::get('program'))->where([['corpsetat_id',$corps->corpsetats_id],['actif_id',$corps->actif_id]])->orderBy('id','asc')->get(); @endphp

                                                                        @foreach ($libelcorpsetat as $libel)
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <div class="sub_sub_2 d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                                                                        <div class="tilte mr-auto p-2">{{$libel->libelle}}</div>
                                                                                    </div>
                                                                                </td>
                                                                                <td></td>
                                                                            </tr>

                                                                            {{-- LES SOUS CORPS D'ETAT PAR LIBELLE --}}
                                                                            @php $souscorpetat = \App\ProgrammeSouscorpsetatLot::where([['corpsetat_id',$corps->corpsetats_id],['actif_id',$corps->actif_id],['lot_id',$lot->lot_id],['corpsetatlibel_id',$libel->id]])->where('programme_id',Session::get('program'))->with('matieres')->with('actif')->with('corpsetat')->get(); @endphp
                                                                            @foreach ($souscorpetat as $sscorp)
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="hidden" name="sous_corps_id[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="{{$sscorp->id}}">
                                                                                        <div class="tilte mr-auto p-2 text-body">
                                                                                            {{$sscorp->matieres->libelle}}
                                                                                            <input type="hidden" name="produit_id[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="{{$sscorp->matieres->id}}">
                                                                                            <div class="form-label mr-auto p-2 text-warning">
                                                                                                <span class="text-default font-size-13">
                                                                                                    @php
                                                                                                    $unite = mb_strtoupper(\App\Produit::where('produits.id', $sscorp->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                                                    $total_livre = \App\Bon_demande::where('status', 3)
                                                                                                                    ->where('programme_id', Session::get('program'))
                                                                                                                    //->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                                                    ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                                                    ->where('bon_demande_items.produit_id', $sscorp->produit_id)
                                                                                                                    ->where('bon_demande_items.lot_id', $lot->lot_id)
                                                                                                                    ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                                                    ->where('bon_demande_items.scorpsetats_id', $sscorp->id)
                                                                                                                    ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                                                                                    ->pluck('total_livre')
                                                                                                                    ->first();
                                                                                                    $restant = $sscorp->qte - $total_livre;
                                                                                                    $stock_dispo = \App\Stock::where('produit_id',$sscorp->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                                                                                                    @endphp

                                                                                                    Livré: <b> {{$total_livre != '' ? $total_livre : 0}}
                                                                                                        <sup>{{$unite}}</sup></b> &nbsp; | &nbsp;

                                                                                                    Restant: <b> {{$restant}}
                                                                                                        <sup>{{$unite}}</sup></b> (<span class="text-warning">@php if($sscorp->qte > 0) echo round((($total_livre*100)/$sscorp->qte), 2) ; else echo 0; @endphp %</span>) &nbsp; | &nbsp;

                                                                                                    Stock disponible: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} </b>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="somme p-2">
                                                                                            @if($restant > 0)
                                                                                                @if ($stock_dispo == 0)
                                                                                                    <input type="text" step="any" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" data-v-min="0.001" data-v-max="{{$restant}}" placeholder="Quantité">
                                                                                                @else
                                                                                                    @if ($stock_dispo >= $restant)
                                                                                                        <input type="text" step="any" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" data-v-min="0.001" data-v-max="{{$restant}}" placeholder="Quantité">
                                                                                                    @else
                                                                                                        <input type="text" step="any" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" data-v-min="0.001" data-v-max="{{$stock_dispo}}" placeholder="Quantité">
                                                                                                    @endif
                                                                                                @endif
                                                                                            @else
                                                                                                <input type="number" step="any" name="qte_sscorps[]" placeholder="Quantité" disabled>
                                                                                                <input type="hidden" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="">
                                                                                            @endif
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach

                                                                        @endforeach

                                                                        {{-- LES PRODUITS DU CORPS D'ETAT UNIQUEMENT --}}
                                                                        @php $souscorpetat = \App\ProgrammeSouscorpsetatLot::where([['corpsetat_id',$corps->corpsetats_id],['lot_id',$lot->lot_id],['programme_id',Session::get('program')],['corpsetatlibel_id',null]])->with('matieres')->with('actif')->with('corpsetat')->get(); @endphp
                                                                        @foreach ($souscorpetat as $sscorp)
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="hidden" name="sous_corps_id[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="{{$sscorp->id}}">
                                                                                    <div class="tilte mr-auto p-2 text-body">
                                                                                        {{$sscorp->matieres->libelle}}
                                                                                        <input type="hidden" name="produit_id[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="{{$sscorp->matieres->id}}">
                                                                                        <div class="form-label mr-auto p-2 text-warning">
                                                                                            <span class="text-default font-size-13">
                                                                                                @php
                                                                                                $unite = mb_strtoupper(\App\Produit::where('produits.id', $sscorp->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                                                $total_livre = \App\Bon_demande::where('status', 3)
                                                                                                                ->where('programme_id', Session::get('program'))
                                                                                                                //->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                                                ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                                                ->where('bon_demande_items.produit_id', $sscorp->produit_id)
                                                                                                                ->where('bon_demande_items.lot_id', $lot->lot_id)
                                                                                                                ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                                                ->where('bon_demande_items.scorpsetats_id', $sscorp->id)
                                                                                                                ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                                                                                ->pluck('total_livre')
                                                                                                                ->first();
                                                                                                $restant = $sscorp->qte - $total_livre;
                                                                                                $stock_dispo = \App\Stock::where('produit_id',$sscorp->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                                                                                                @endphp

                                                                                                Livré: <b> {{$total_livre != '' ? $total_livre : 0}}
                                                                                                    <sup>{{$unite}}</sup></b> &nbsp; | &nbsp;

                                                                                                Restant: <b> {{$restant}}
                                                                                                    <sup>{{$unite}}</sup></b> (<span class="text-warning">@php if($sscorp->qte > 0) echo round((($total_livre*100)/$sscorp->qte), 2) ; else echo 0; @endphp %</span>) &nbsp; | &nbsp;

                                                                                                Stock disponible: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} </b>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="somme p-2">
                                                                                        @if($restant > 0)
                                                                                            @if ($stock_dispo == 0)
                                                                                                <input type="text" step="any" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" data-v-min="0.001" data-v-max="{{$restant}}" placeholder="Quantité">
                                                                                            @else
                                                                                                @if ($stock_dispo >= $restant)
                                                                                                    <input type="text" step="any" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" data-v-min="0.001" data-v-max="{{$restant}}" placeholder="Quantité">
                                                                                                @else
                                                                                                    <input type="text" step="any" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" data-v-min="0.001" data-v-max="{{$stock_dispo}}" placeholder="Quantité">
                                                                                                @endif
                                                                                            @endif
                                                                                        @else
                                                                                            <input type="number" step="any" name="qte_sscorps[]" placeholder="Quantité" disabled>
                                                                                            <input type="hidden" name="qte_sscorps[{{$lot->lot_id}}][{{$corps->corpsetats_id}}][]" value="">
                                                                                        @endif
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                                </div>
                                                            </div>
                                                        </div> <!-- fin main_phase -->
                                                    </div><!-- card -->
                                                @endforeach
                                            @else
                                            Ce prestataire n'a aucune matière pour la villa {{$lot->lot->lot}} !
                                            @endif

                                        </div>

                                @endforeach
                            </div>



                            </div>
                        </div>


                        <hr>
                        <div class="form-group niveau-1">
                            <label for="demandefile">Charger le fichier numerique du bon <span class="text-danger">*</span></label>
                            <input type="file" class="form-control file" name="demandefile" accept="application/pdf" id="demandefile" required>
                        </div>
                        <br>
                        <br>
                        <div class="form-group text-right">
                            <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                                <span class="sr-only">Chargement...</span>
                            </div>
                            <a id="btncancel" href="{{route('chefchantier.demande.create')}}" class="btn btn-light btn-lg mr-4">
                                &nbsp; Annuler &nbsp;
                            </a>
                            <button id="addbtn" type="button" class="ml-4 btn btn-lg btn-primary waves-effect waves-light pull-right">Enregistrer </button>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- end row -->

@endsection


@section('other_css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
<style>
    .is-invalid {
        border-color: #dc3545;
    }
    td {
        border-bottom: 1px solid #f5f6f8 !important;
    }
</style>
@endsection

@section('other_js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/jbvalidator/jbvalidator.min.js')}}"></script>
{{-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> --}}
{{-- <script src="{{asset('assets/js/slim.js')}}"></script> --}}
<script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
<script>

    $(document).ready(function () {
        $(".medatatable").DataTable({
            paging: false,
            info: false,
            responsive: true,
            ordering: false,
        });
    });


    $(function (){

        let validator = $('form.formdmd').jbvalidator({
            errorMessage: true,
            successClass: false,
            language: "{{asset('assets/js/jbvalidator/lang/fr.json')}}",
        });

        //check form without submit
        $('#addbtn').click(function(){
            var nberror = validator.checkAll();
            if(nberror != 0) {
                swal({
                    title: "Erreur!",
                    text: "Veuillez renseigner correctement les champs en rouge",
                    icon: "error",
                    button: "OK",
                });
            }
            else {
                submitForm();
            }
        });

        //check form without submit
        $('#btncancel').click(function(){
            validator.reset();
        });

    })

    function submitForm() {
        swal({
            title: "Êtes-vous sûr?",
            text: "Vous êtes sur le point de faire une demande de sortie ! Cette action est irréversible ? ",
            icon: "warning",
            buttons: true,
            buttons: ["Annuler", "Oui"],
            dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    document.getElementById('addbtn').disabled = true;
                    $('#addbtn').hide();
                    $('#btncancel').hide();
                    $('#loader').show();
                    document.getElementById('DemForm').submit();
                }
            }
        );
    }
</script>

@endsection
