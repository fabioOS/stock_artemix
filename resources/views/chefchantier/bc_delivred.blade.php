@extends('chefchantier.layouts.app')

@section('page_title')
    Bon de livraison
@stop

@section('content_title')
    Bon de livraison
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de livraison</li>
    </ol>
@stop

@section('content')

    @php $prefix = 'chefchantier.livraison'; @endphp
    {!!(new \App\Http\Controllers\BonController())->getDataTable($data, $prefix)!!}

@endsection


@section('other_js')

@endsection
