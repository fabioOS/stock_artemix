@extends('chefchantier.layouts.app')

@section('page_title')
    Détails bon de sortie
@stop

@section('content_title')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">
                @if ($data->status == 3)
                    Détails du bon de livraison N° <span class="text-primary">{{strtoupper($data->ref)}}</span>
                @else
                    Détails du bon de sortie N°  <span class="text-primary">{{strtoupper($data->ref)}}</span>
                @endif
                @if ($data->status == 1)
                    <span class="badge badge-pill badge-warning">En attente</span>
                @endif
                @if ($data->status == 2)
                    <span class="badge badge-pill badge-info">Validé</span>
                @endif
                @if ($data->status == 3)
                    <span class="badge badge-pill badge-success">Livré</span>
                @endif
                @if ($data->status == 4)
                    <span class="badge badge-pill badge-danger">Rejété</span>
                @endif
            </h4>
            {{-- <a href="{{URL::previous()}}" class="btn btn-warning btn-sm"> <i class="fa fa-chevron-left"></i> Retour</a> --}}
        </div>
    </div>
</div>
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails bon de sortie</li>
    </ol>
@stop

@section('content')
    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Référence</h5>
                            <p>{{strtoupper($data->ref)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Chef chantier</h5>
                            <p>{{strtoupper($data->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Sous-traitant</h5>
                            <p>{{strtoupper($data->soustraitant->nom)}}</p>
                        </div>

                        @if($data->contrat)
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Contrat</h5>
                            <p>{{strtoupper($data->contrat->libelle)}}</p>
                        </div>
                        @endif

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Fichier</h5>
                            <p>@if(file_exists(asset('assets/uploads/bondemandes/'.$data->file)))<a href="{{asset('assets/uploads/bondemandes/'.$data->file)}}" target="_blank" download>bon_de_demande.pdf</a>@else - @endif</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date du bon</h5>
                            <p>{{$data->created_at->format('d/m/Y')}}</p>
                        </div>
                        <div class="col-lg-2">

                        </div>

                    </div>


                </div>
            </div>
        </div>

        @if($data->status == 4)
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="font-size-16">Motif</h5>
                                <p>{{$data->motif}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-xl-3">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Résumé par matière </h5>
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            @php
                                $distinct_matiers = \App\Bon_demande_item::where('bon_demande_id', $data->id)
                                ->join('produits', 'produits.id', '=', 'bon_demande_items.produit_id')
                                ->selectRaw('DISTINCT(produits.id) AS id')
                                ->pluck('id');
                            @endphp
                            <tbody>
                                @foreach ($distinct_matiers as $matiere_id)
                                @php $pdt = \App\Produit::where('id',$matiere_id)->with('type')->first() @endphp
                                    <tr>
                                        <td><b>{{$pdt->libelle}}</b></td>
                                        <td class="text-right">
                                            @php
                                                $mat_qte_total = \App\Bon_demande_item::where('bon_demande_id', $data->id)->where('produit_id', $matiere_id)->selectRaw('SUM(qte) AS qte')->pluck('qte')->first();
                                            @endphp
                                            {{$mat_qte_total ? $mat_qte_total : '0'}} @if($pdt->type)<sup>{{$pdt->type->libelle}}</sup>@endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            <div class="card mb-0">
               <div class="card-body">
                <h4 class="card-title mb-5">
                    @if ($data->status == 3)
                    Résumé du bon de livraison par lot
                    @else
                    Résumé du bon de sortie par lot
                    @endif
                </h4>

                @php
                    $row_lot_id = \App\Bon_demande_item::where('bon_demande_id', $data->id)->selectRaw('DISTINCT(lot_id) AS lot_id')->pluck('lot_id');
                    $programme_id = Session::get('program');
                    $vars = [
                        'programme_id' => $data->programme_id,
                        'lot_id' => $row_lot_id,
                        'soustraitant_id' => $data->soustraitant_id
                    ];
                    // $soustraitant = \App\ProgrammeSoustraitant::with(['lots' =>function($query) use($row_lot_id) {
                    //     $query->whereIn('lot_id', $row_lot_id)->with('lot');
                    // }])
                    // ->where('id', $data->soustraitant_id)
                    // ->where('programme_id', $data->programme_id)
                    // ->first();
                    $lots = \App\ProgrammeLot::whereIn('id',$row_lot_id)->with('actifs')->get();
                    //dump($lots);

                @endphp

                    <div class="row">
                        <div class="col-md-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                @foreach($lots as $k=>$item)
                                    {{-- @php $item = \App\ProgrammeLot::where('id',$lot->lot_id)->with('actifs')->first();@endphp --}}
                                    <a class="nav-link mb-2 border {{$k==0 ? 'active' :''}}" id="v-pills-{{$k}}-tab" data-toggle="pill" href="#v-pills-{{$k}}" role="tab" aria-controls="v-pills-{{$k}}" aria-selected="{{$k==0 ? 'true' : 'false'}}">
                                        <i class="uil uil-home font-size-20"></i>
                                        Lot {{$item->lot}} <br><span class="font-size-12">{{$item->actifs ?$item->actifs->libelle:''}}</span>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-9">

                        <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabCentent">
                            @foreach($lots as $k=>$item)
                                @php
                                    //$item = \App\ProgrammeLot::where('id',$lot->lot_id)->first();
                                    $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$item->actif_id)->where('soustraitant_id',$data->soustraitant_id)->where('soustraitantcontrat_id',$data->contrat_id)->where('etatdel',1)->with('corpslots')->get();
                                    //$corpsetats = \App\ProgrammeSouscorpsetatLot::where('actif_id',$item->actif_id)->where('lot_id',$item->id)->with('corpsetat')->get();
                                    //dd($corpsetats,$item);
                                @endphp
                                    <div class="tab-pane fade {{$k==0 ? 'active show' :''}}" id="v-pills-{{$k}}" role="tabpanel" aria-labelledby="v-pills-{{$k}}-tab">
                                        <input type="hidden" value="{{$item->id}}" name="lot_id[]">
                                        @if (count($corpsetats)>0)
                                            @foreach($corpsetats as $j=>$corps)
                                                    <input type="hidden" name="corps_id[{{$item->id}}][]" value="{{$corps->id}}">

                                                    <div class="card border border-primary">
                                                        <a href="#main_phase{{$corps->id}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$corps->id}}">
                                                            <div class="p-4">
                                                                <div class="media align-items-center">
                                                                    <div class="mr-3">
                                                                        <div class="avatar-xs">
                                                                            <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="media-body  overflow-hidden">
                                                                        <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                                    </div>
                                                                    <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div id="main_phase{{$corps->id}}" class="collapse show" data-parent="#main_phase{{$corps->id}}">
                                                            <div class="p-4 border-top">
                                                                <!-- item sous phase  -->
                                                                @php $libelcorpsetatids = \App\ProgrammeSouscorpsetatLot::where([['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['lot_id',$item->id]])->orderBy('id','asc')->pluck('corpsetatlibel_id')->toArray();
                                                                    $souscorpetat = \App\ProgrammeCorpsetatLibelle::whereIn('id',$libelcorpsetatids)->orderBy('id','asc')->get();
                                                                    //dd($souscorpetat);
                                                                @endphp

                                                                @foreach ($souscorpetat as $i=>$sscorp)

                                                                    <div class="sub_sub_2 border-top d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                                                        <div class="tilte mr-auto p-2">{{$sscorp->libelle}}</div>
                                                                    </div>

                                                                    @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',$sscorp->id]])->with('matieres')->get(); @endphp

                                                                    @foreach ($scorpsmtt as $scorps)
                                                                        @php
                                                                            $qte_demande = \App\Bon_demande::where('bon_demandes.id', $data->id)
                                                                            ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                            ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                            ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                            ->where('bon_demande_items.lot_id', $item->id)
                                                                            ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                            ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                            ->pluck('qte')
                                                                            ->first();
                                                                        @endphp
                                                                        @if(isset($qte_demande) and $qte_demande != 0)
                                                                        {{-- @dump($scorps) --}}
                                                                        <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                            <input type="hidden" name="sous_corps_id[{{$item->id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->id}}">

                                                                            <div class="tilte mr-auto p-2 text-body">
                                                                                {{$scorps->matieres->libelle}}
                                                                                <input type="hidden" name="produit_id[{{$scorps->produit_id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->matieres->id}}">
                                                                                <div class="form-label mr-auto p-2 text-warning">
                                                                                    <span class="text-default font-size-13">
                                                                                        @php
                                                                                            $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                                            $total_livre = \App\Bon_demande::where('status', 3)
                                                                                                                            ->where('programme_id', Session::get('program'))
                                                                                                                            // ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                                                            ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                                                            ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                                                                            ->where('bon_demande_items.lot_id', $item->id)
                                                                                                                            ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                                                            ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                                                                            ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                                                                                            ->pluck('total_livre')
                                                                                                                            ->first();

                                                                                            $restant = $scorps->qte - $total_livre;
                                                                                            $stock_dispo = \App\Stock::where('produit_id',$scorps->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                                                                                            $qte_demande = \App\Bon_demande::where('bon_demandes.id', $data->id)
                                                                                                                        ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                                                        ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                                                        ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                                                                        ->where('bon_demande_items.lot_id', $item->id)
                                                                                                                        ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                                                        ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                                                                        ->pluck('qte')
                                                                                                                        ->first();
                                                                                        @endphp

                                                                                        Stock déjà livré: <b> {{$total_livre != '' ? $total_livre : 0}}</b> / {{$scorps->qte}} <sup>{{$unite}}</sup> <br>
                                                                                        {{-- Restant: <b> {{$restant}}
                                                                                            <sup>{{$unite}}</sup></b> (<span class="text-warning">@php if($scorps->qte > 0) echo round((($total_livre*100)/$scorps->qte), 2) ; else echo 0; @endphp %</span>)<br> --}}
                                                                                        Stock actuel du programme: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} <sup>{{$unite}}</sup></b>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="somme p-2">
                                                                                <span class="text-body">Qté demandée:</span> {{$qte_demande}}
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                    @endforeach

                                                                @endforeach

                                                            </div>

                                                            <div class="p-2 border-top">
                                                                {{-- Ceux qui nont pas de LIBELLE --}}
                                                                @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                                                                @foreach ($scorpsmtt as $scorps)
                                                                @php
                                                                    $qte_demande = \App\Bon_demande::where('bon_demandes.id', $data->id)
                                                                                    ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                    ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                    ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                                    ->where('bon_demande_items.lot_id', $item->id)
                                                                                    ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                    ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                                    ->pluck('qte')
                                                                                    ->first();
                                                                @endphp
                                                                @if(isset($qte_demande) and $qte_demande != 0)
                                                                    {{-- @dump($scorps) --}}
                                                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                        <input type="hidden" name="sous_corps_id[{{$item->id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->id}}">

                                                                        <div class="tilte mr-auto p-2 text-body">
                                                                            {{$scorps->matieres->libelle}}
                                                                            <input type="hidden" name="produit_id[{{$scorps->produit_id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->matieres->id}}">
                                                                            <div class="form-label mr-auto p-2 text-warning">
                                                                                <span class="text-default font-size-13">
                                                                                    @php
                                                                                        $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                                        $total_livre = \App\Bon_demande::where('status', 3)
                                                                                                                        ->where('programme_id', Session::get('program'))
                                                                                                                        // ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                                                        ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                                                        ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                                                                        ->where('bon_demande_items.lot_id', $item->id)
                                                                                                                        ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                                                        ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                                                                        ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                                                                                        ->pluck('total_livre')
                                                                                                                        ->first();

                                                                                        $restant = $scorps->qte - $total_livre;
                                                                                        $stock_dispo = \App\Stock::where('produit_id',$scorps->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();

                                                                                    @endphp

                                                                                    Stock déjà livré: <b> {{$total_livre != '' ? $total_livre : 0}}</b> / {{$scorps->qte}} <sup>{{$unite}}</sup> <br>

                                                                                    {{-- Restant: <b> {{$restant}}
                                                                                        <sup>{{$unite}}</sup></b> (<span class="text-warning">@php if($scorps->qte > 0) echo round((($total_livre*100)/$scorps->qte), 2) ; else echo 0; @endphp %</span>)<br> --}}

                                                                                    Stock actuel du programme: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} <sup>{{$unite}}</sup></b>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="somme p-2">

                                                                            <span class="text-body">Qté demandée : </span> {{$qte_demande}} <br>&nbsp;<br>&nbsp;
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div><!-- card -->
                                            @endforeach
                                        @else
                                        Ce prestataire n'a aucune matière pour ce lot {{$item->lot}} !
                                        @endif

                                    </div>

                            @endforeach
                        </div>



                        </div>
                    </div>



               </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection


@section('other_js')

@endsection
