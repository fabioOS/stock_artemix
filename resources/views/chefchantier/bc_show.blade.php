@extends('chefchantier.layouts.app')

@section('page_title')
    Bon de sortie
@stop

@section('content_title')
    Bon de sortie
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de sortie</li>
    </ol>
@stop

@section('content')
    <div class="row mb-4">
        <div class="col-xl-3">
            <div class="card h-100">
                <div class="card-body">
                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Référence</h5>
                        <p>#MB2540</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Chef chantier</h5>
                        <p>Fabien OUREGA</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Sous-traitant</h5>
                        <p>READY WEAR</p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Fichier</h5>
                        <p><a href="#" download="">bon_de_demande.pdf</a></p>
                    </div>

                    <div class="text-muted mb-4">
                        <h5 class="font-size-16">Date de demande</h5>
                        <p>29/08/2021</p>
                    </div>
                    <hr>
                    <button type="button" class="btn btn-success waves-effect waves-light">
                        <i class="uil uil-check mr-2"></i> Valider
                    </button>
                    <button type="button" class="btn btn-danger waves-effect waves-light">
                        <i class="uil uil-exclamation-octagon mr-2"></i> Rejeter
                    </button>



                </div>
            </div>
        </div>

        <div class="col-xl-8">
            <div class="card mb-0">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#actif" role="tab">
                            <i class="uil uil-home font-size-20"></i>
                            <span class="d-none d-sm-block">Lot 1</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lots" role="tab">
                            <i class="uil uil-home font-size-20"></i>
                            <span class="d-none d-sm-block">Lots 2</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#dqe" role="tab">
                            <i class="uil uil-home font-size-20"></i>
                            <span class="d-none d-sm-block">Lots 10</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab content -->
                <div class="tab-content p-4">
                    <div class="tab-pane active" id="actif" role="tabpanel">
                        <div class="card border border-primary">
                            <a href="#main_phase1" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase1">
                                <div class="p-4">
                                    <div class="media align-items-center">
                                        <div class="mr-3">
                                            <div class="avatar-xs">
                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary"></div>
                                            </div>
                                        </div>
                                        <div class="media-body  overflow-hidden">
                                            <h5 class="font-size-16 mb-1">
                                                Corps d'etat 1
                                            </h5>

                                        </div>
                                        <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                    </div>
                                </div>
                            </a>

                            <div id="main_phase1" class="collapse show" data-parent="#main_phase1">
                                <div class="p-4 border-top">
                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Ciment</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Sable</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Gravier</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                </div><!-- fin border-top -->
                            </div> <!-- fin main_phase -->
                        </div><!-- card -->

                        <div class="card border border-primary">
                            <a href="#main_phase2" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase2">
                                <div class="p-4">
                                    <div class="media align-items-center">
                                        <div class="mr-3">
                                            <div class="avatar-xs">
                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary"></div>
                                            </div>
                                        </div>
                                        <div class="media-body  overflow-hidden">
                                            <h5 class="font-size-16 mb-1">
                                                Corps d'etat 2
                                            </h5>

                                        </div>
                                        <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                    </div>
                                </div>
                            </a>

                            <div id="main_phase2" class="collapse show" data-parent="#main_phase2">
                                <div class="p-4 border-top">
                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Ciment</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Sable</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Gravier</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                </div><!-- fin border-top -->
                            </div> <!-- fin main_phase -->
                        </div><!-- card -->
                    </div>
                    <div class="tab-pane" id="lots" role="tabpanel">
                        <div class="card border border-primary">
                            <a href="#main_phase1" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase1">
                                <div class="p-4">
                                    <div class="media align-items-center">
                                        <div class="mr-3">
                                            <div class="avatar-xs">
                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary"></div>
                                            </div>
                                        </div>
                                        <div class="media-body  overflow-hidden">
                                            <h5 class="font-size-16 mb-1">
                                                Corps d'etat 1
                                            </h5>

                                        </div>
                                        <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                    </div>
                                </div>
                            </a>

                            <div id="main_phase1" class="collapse show" data-parent="#main_phase1">
                                <div class="p-4 border-top">
                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Ciment</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Sable</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Gravier</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                </div><!-- fin border-top -->
                            </div> <!-- fin main_phase -->
                        </div><!-- card -->

                        <div class="card border border-primary">
                            <a href="#main_phase2" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase2">
                                <div class="p-4">
                                    <div class="media align-items-center">
                                        <div class="mr-3">
                                            <div class="avatar-xs">
                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary"></div>
                                            </div>
                                        </div>
                                        <div class="media-body  overflow-hidden">
                                            <h5 class="font-size-16 mb-1">
                                                Corps d'etat 2
                                            </h5>

                                        </div>
                                        <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                    </div>
                                </div>
                            </a>

                            <div id="main_phase2" class="collapse show" data-parent="#main_phase2">
                                <div class="p-4 border-top">
                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Ciment</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Sable</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Gravier</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                </div><!-- fin border-top -->
                            </div> <!-- fin main_phase -->
                        </div><!-- card -->
                    </div>
                    <div class="tab-pane" id="dqe" role="tabpanel">
                        <div class="card border border-primary">
                            <a href="#main_phase1" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase1">
                                <div class="p-4">
                                    <div class="media align-items-center">
                                        <div class="mr-3">
                                            <div class="avatar-xs">
                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary"></div>
                                            </div>
                                        </div>
                                        <div class="media-body  overflow-hidden">
                                            <h5 class="font-size-16 mb-1">
                                                Corps d'etat 1
                                            </h5>

                                        </div>
                                        <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                    </div>
                                </div>
                            </a>

                            <div id="main_phase1" class="collapse show" data-parent="#main_phase1">
                                <div class="p-4 border-top">
                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Ciment</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Sable</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Gravier</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                </div><!-- fin border-top -->
                            </div> <!-- fin main_phase -->
                        </div><!-- card -->

                        <div class="card border border-primary">
                            <a href="#main_phase2" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase2">
                                <div class="p-4">
                                    <div class="media align-items-center">
                                        <div class="mr-3">
                                            <div class="avatar-xs">
                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary"></div>
                                            </div>
                                        </div>
                                        <div class="media-body  overflow-hidden">
                                            <h5 class="font-size-16 mb-1">
                                                Corps d'etat 2
                                            </h5>

                                        </div>
                                        <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                    </div>
                                </div>
                            </a>

                            <div id="main_phase2" class="collapse show" data-parent="#main_phase2">
                                <div class="p-4 border-top">
                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Ciment</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Sable</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                    <!-- item sous phase  -->
                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                        <div class="tilte mr-auto p-2">Gravier</div>
                                        <div class="somme p-2">10<sup>Tonnes</sup></div>
                                    </div>

                                </div><!-- fin border-top -->
                            </div> <!-- fin main_phase -->
                        </div><!-- card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('other_js')

@endsection
