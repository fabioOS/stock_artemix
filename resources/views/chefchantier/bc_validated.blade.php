@extends('chefchantier.layouts.app')

@section('page_title')
    Bon de sortie validé
@stop

@section('content_title')
    Bon de sortie validé
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de sortie validé</li>
    </ol>
@stop

@section('content')

    @php $prefix = 'chefchantier.demande'; @endphp
    {!!(new \App\Http\Controllers\BonController())->getDataTable($data, $prefix)!!}

@endsection


@section('other_js')

@endsection
