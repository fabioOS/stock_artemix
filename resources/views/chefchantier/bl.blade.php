@extends('chefchantier.layouts.app')

@section('page_title')
    Bon de livraison
@stop

@section('content_title')
    Bon de livraison
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de livraison</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>N° Ref</th>
                        <th>Fichier</th>
                        <th>Chef chantier</th>
                        <th>Sous-traitant</th>
                        <th>lot(s)</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr class="font-size-16">
                        <td><a href="!#" class="text-dark font-weight-bold">#MN0132</a> </td>
                        <td><a href="#" download>bon_de_demande.pdf</a></td>
                        <td>Ibrahima SORO</td>
                        <td>READY WEAR</td>
                        <td>
                            <span class="badge badge-pill badge-info">1</span>
                            <span class="badge badge-pill badge-info">2</span>
                            <span class="badge badge-pill badge-info">10</span>
                        </td>
                        <td>
                            <span class="badge badge-pill badge-warning">En attente</span>
                            <span class="badge badge-pill badge-success">livré</span>
                            <span class="badge badge-pill badge-danger">Rejeté</span>
                        </td>
                        <td>29/08/2021</td>
                        <td>
                            <a href="details_bon_demande.html" class="btn btn-primary waves-effect waves-light">
                                <i class="uil uil-eye mr-2"></i> Détails
                            </a>

                        </td>
                    </tr>

                    <tr class="font-size-16">
                        <td><a href="!#" class="text-dark font-weight-bold">#MN0132</a> </td>
                        <td><a href="#" download>bon_de_demande.pdf</a></td>
                        <td>Fabien OUREGA</td>
                        <td>READY WEAR</td>
                        <td>
                            <span class="badge badge-pill badge-info">1</span>
                            <span class="badge badge-pill badge-info">2</span>
                            <span class="badge badge-pill badge-info">10</span>
                        </td>
                        <td>
                            <span class="badge badge-pill badge-warning">En attente</span>
                            <span class="badge badge-pill badge-success">livré</span>
                            <span class="badge badge-pill badge-danger">Rejeté</span>
                        </td>
                        <td>29/08/2021</td>
                        <td>
                            <a href="details_bon_demande.html" class="btn btn-primary waves-effect waves-light">
                                <i class="uil uil-eye mr-2"></i> Détails
                            </a>

                        </td>
                    </tr>



                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>
@endsection


@section('other_js')
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
@endsection
