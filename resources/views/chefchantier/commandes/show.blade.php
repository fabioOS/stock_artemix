@extends('chefchantier.layouts.app')

@section('page_title')
    Détails de la commande
@stop

@section('content_title')
    @if ($demande->status == 3)
    Commande N° <span class="text-primary">{{strtoupper($demande->ref)}}</span>
    @else
    Commande N°  <span class="text-primary">{{strtoupper($demande->ref)}}</span>
    @endif
    @if ($demande->status == 1)
    <span class="badge badge-pill badge-warning">En attente</span>
    @endif
    @if ($demande->status == 2)
    <span class="badge badge-pill badge-info">Validé</span>
    @endif
    @if ($demande->status == 3)
    <span class="badge badge-pill badge-success">Livré</span>
    @endif
    @if ($demande->status == 4)
    <span class="badge badge-pill badge-danger">Rejété</span>
    @endif
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails de la commande</li>
    </ol>
@stop

@section('content')
    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-4">
                            <h5 class="font-size-16">Référence</h5>
                            <p>{{strtoupper($demande->ref)}}</p>
                        </div>

                        @if($demande->programmesous_id != null)
                        <div class="text-muted col-lg-4">
                            <h5 class="font-size-16">Sous-programme</h5>
                            <p>{{strtoupper($demande->programmesous->libelle)}}</p>
                        </div>@endif

                        <div class="text-muted col-lg-4">
                            <h5 class="font-size-16">Ligne Budgetaire</h5>
                            <p>
                                @php $lignb = $demande->lignebudget @endphp
                                @if ($lignb)
                                    {{strtoupper($lignb->libelle)}}
                                    @if($lignb->hors_ds == 1)
                                            <span class="badge badge-pill badge-warning">Hors DS</span>
                                        @elseif ($lignb->type == 1)
                                            <span class="badge badge-pill badge-primary">Batiments</span>
                                        @elseif ($lignb->type == 2)
                                            <span class="badge badge-pill badge-dark">VRD</span>
                                        @else
                                            <span class="badge badge-pill badge-success">-</span>
                                        @endif
                                @else
                                    <span class="text-danger">BUDGET NON DEFINIE</span>
                                @endif
                            </p>
                        </div>

                        <div class="text-muted col-lg-4">
                            <h5 class="font-size-16">Demandeur</h5>
                            <p>{{strtoupper($demande->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-4">
                            <h5 class="font-size-16">Date de la commande</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->created_at)->format('d/m/Y')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @if($demande->status == 4)
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="font-size-16">Motif du rejet</h5>
                                <p>{!! nl2br($demande->motif) !!}</p>
                            </div>

                            <div class="col-lg-12">
                                <h5 class="font-size-16">Agent</h5>
                                @if($demande->nivalid == 1)
                                    <p>{{'CHEF PROJET'}}</p>
                                @else
                                    <p>{{'DIRECTEUR PROJET'}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16">Motif de la commande</h5>
                            <p>{!! nl2br($demande->description) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-12">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Matières demandées </h5>
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr style="background-color: #c6baba2b;">
                                    <td>Code</td>
                                    <td>Matières</td>
                                    <td>Type</td>
                                    <td class="text-right">Qte totale commandée</td>
                                    <td class="text-right">Qte totale restante</td>
                                    <td class="text-right table-success">Qte demandée</td>
                                </tr>
                                @foreach ($demande->produits as $item)
                                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first();
                                    //deja commander
                                    $cmdval = \App\Commande::whereIn('status',[2,3])->where('commandes.programme_id',Session::get('program'))
                                    ->join('commande_items', 'commande_items.commande_id', '=', 'commandes.id')
                                    ->where('commande_items.produit_id',$item->produit_id)
                                    ->sum('commande_items.qte');
                                    //dd($cmdval);
                                    //qte ncessaire
                                    // dump($item->produit_id);
                                    $qtencss = \App\ProgrammeSouscorpsetatLot::where('produit_id',$item->produit_id)->where('programme_id',Session::get('program'))->sum('qte');
                                    $rest = round($qtencss - $cmdval,2);
                                @endphp
                                    <tr>
                                        <td>{{$pdt->code}}</td>
                                        <td><b>{{$pdt->libelle}}</b></td>
                                        <td> {{$pdt->type?$pdt->type->libelle:'-'}} </td>
                                        <td class="text-right"> {{$cmdval}} </td>
                                        <td class="text-right"> {{$rest}} </td>
                                        <td class="text-right {{$rest < $item->qte ? 'text-danger' : 'text-success'}}"> {{$item->qte}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

@endsection


@section('other_js')

@endsection
