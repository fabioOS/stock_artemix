@extends('chefchantier.layouts.app')

@section('page_title')
    Demandes spéciales validées
@stop

@section('content_title')
    Demandes spéciales validées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciales validées</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if(count($demandes)>0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th style="width: 100px">Montant</th>
                            <th>Statut</th>
                            <th>Motif</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>@price($dt->montant)</td>
                                <td>
                                    @if ($dt->status == 2)
                                        <span class="badge badge-pill badge-success">Validé</span>
                                    @endif
                                </td>
                                <td>{!! nl2br($dt->description) !!}
                                    @if($dt->preuve != null)<br>
                                    <a href="{{asset('assets/uploads/demandespeciale/'.$dt->preuve)}}" target="_blank">Voir Pièce-Jointe</a>@endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @if($dt->status==2)
                                        @if($dt->genere==1)
                                        <a href="{{route('pdfDmdSpecial', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Télécharger le BC"><i class="uil-file-download"></i></a>
                                        @else
                                        <span class="badge badge-pill badge-warning">Bon de commande en attente du SA</span>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
