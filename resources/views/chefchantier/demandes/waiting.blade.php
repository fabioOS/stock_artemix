@extends('chefchantier.layouts.app')

@section('page_title')
    Demandes spéciales en attente de traitement
@stop

@section('content_title')
    Demandes spéciales en attente de traitement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciales en attente de traitement</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($demandes) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list gstock" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th style="width: 100px">Montant</th>
                            <th>Statut</th>
                            <th>Motif</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>@price($dt->montant) <sup>Fcfa</sup></td>
                                <td>
                                    <span class="badge badge-pill badge-warning">En attente <br>de validation du DP</span>

                                </td>
                                <td>{!! nl2br($dt->description) !!}
                                    @if($dt->preuve != null)<br>
                                    <a href="{{asset('assets/uploads/demandespeciale/'.$dt->preuve)}}" target="_blank">Voir Pièce-Jointe</a>@endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.delet', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cette demande",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

    });
</script>
@endsection
