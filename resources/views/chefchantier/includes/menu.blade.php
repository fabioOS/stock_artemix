<style>
    .text-bleu{
        color: #234699
    }
</style>

<ul class="metismenu list-unstyled" id="side-menu">

    <li>
        <a href="{{route('chefchantier.home')}}">
            <i class="uil-home-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li>

    <li class="menu-title mt-3 text-bleu">Gestion du stock</li>

    <li class="{{Route::is('chefchantier.etats') || Route::is('chefchantier.etats.search') || Route::is('chefchantier.etats.item') ? 'mm-active' : ''}}">
        <a href="{{route('chefchantier.etats')}}" class="{{Route::is('chefchantier.etats') || Route::is('chefchantier.etats.search') || Route::is('chefchantier.etats.item') ? 'active' : ''}}">
            <i class="uil-home"></i>
            <span>Etats par villa</span>
        </a>
    </li>

    <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-notes"></i>
            <span>Stock</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('chefchantier.stock')}}" class="{{Route::is('chefchantier.stock') ? 'active' : ''}}">Stock magasin</a></li>
            <li><a href="{{route('chefchantier.stock.budget')}}" class="{{Route::is('chefchantier.stock.budget') ? 'active' : ''}}">Stock budget</a></li>
        </ul>
    </li>

    <li class="{{Route::is('chefchantier.st')|| Route::is('chefchantier.st.n-contrat')  || Route::is('chefchantier.st.n-contrat.ce') ?'mm-active' : ''}}">
        <a href="{{route('chefchantier.st')}}" class="{{Route::is('chefchantier.st')|| Route::is('chefchantier.st.n-contrat')  || Route::is('chefchantier.st.n-contrat.ce') ?'mm-active' : ''}}">
            <i class="uil-users-alt"></i>
            <span>Sous-traitant</span>
        </a>
    </li>

    <li class="{{Route::is('chefchantier.demande.details') ?'mm-active' : ''}}">
        @php
            $validated = (new \App\Http\Controllers\BonController())->getByStatus(2)->count();
            // $canceled = (new \App\Http\Controllers\BonController())->getByStatus(4)->count();
            $waiting = (new \App\Http\Controllers\BonController())->getByStatus(1)->count();
            $delivered = (new \App\Http\Controllers\BonController())->getByStatus(3)->count();
        @endphp
        <a href="javascript: void(0);" class="has-arrow waves-effect {{Route::is('chefchantier.demande.details') ?'mm-active' : ''}}">
            <i class="uil-comment-alt-edit"></i>
            <span>Bon de sortie</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('chefchantier.demande.create')}}"    class="{{Route::is('chefchantier.demande.create')    ? 'active' : ''}}">Créer un bon de sortie</a></li>
            <li><a href="{{route('chefchantier.demande.waiting')}}"   class="{{Route::is('chefchantier.demande.waiting')   ? 'active' : ''}}">En attente <span class="badge badge-pill badge-warning float-right">{{$validated}}</span></a></li>
            <li><a href="{{route('chefchantier.demande.canceled')}}"  class="{{Route::is('chefchantier.demande.canceled')  ? 'active' : ''}}">Rejetés </a></li>
            {{-- <li><a href="{{route('chefchantier.demande.validated')}}" class="{{Route::is('chefchantier.demande.validated') ? 'active' : ''}}">Validés @if($validated != 0)<span class="badge badge-pill badge-info float-right">{{$validated}}</span>@endif</a></li> --}}
            {{-- <li><a href="{{route('chefchantier.demande.canceled')}}"  class="{{Route::is('chefchantier.demande.canceled')  ? 'active' : ''}}">Bon rejeté <span class="badge badge-pill badge-danger float-right">{{$canceled}}</span></a></li> --}}
        </ul>
    </li>

    <li class="{{Route::is('chefchantier.livraison.details') ?'mm-active' : ''}}">
        <a href="{{route('chefchantier.livraison.index')}}" class="{{Route::is('chefchantier.livraison.details') ?'mm-active' : ''}}">
            <i class="uil-comment-alt-check"></i>
            <span class="badge badge-pill badge-success float-right">{{$delivered}}</span>
            <span>Bon de livraison</span>
        </a>
    </li>

    {{-- @inject('nbtranscc','App\Helperme\Notifications') --}}
    {{-- <li>
        @php
            $twait = (new \App\Http\Controllers\Chefchantier\HomeController())->getByStatusTransactions(1)->count();
            // $treject = (new \App\Http\Controllers\Chefchantier\HomeController())->getByStatusTransactions(0)->count();
            // $tvalid = (new \App\Http\Controllers\Chefchantier\HomeController())->getByStatusTransactions(2)->count();
        @endphp

        <a href="{{route('chefchantier.transactions')}}">
            <i class="uil-money-withdrawal"></i>
            @if($nbtranscc::CountTransacChefp()>0 )
                <span class="badge badge-pill badge-primary mr-3 float-right">{{$nbtranscc::CountTransacChefp()}}</span>
            @endif
            <span>Transactions</span>
        </a>

        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-money-withdrawal"></i>
            <span>Transactions</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('chefchantier.transactions')}}" class="{{Route::is('chefchantier.transactions') ? 'active' : ''}}">En attente @if($twait>0)<span class="badge badge-pill badge-warning float-right">{{$twait}}</span>@endif</a></li>
            <li><a href="{{route('chefchantier.transactions.reject')}}" class="{{Route::is('chefchantier.transactions.reject')    ? 'active' : ''}}">Rejetées </a></li>
            <li><a href="{{route('chefchantier.transactions.reject')}}" class="{{Route::is('chefchantier.transactions.reject')    ? 'active' : ''}}">Transactions rejetées @if($treject>0)<span class="badge badge-pill badge-danger float-right">{{$treject}}</span>@endif</a></li>
            <li><a href="{{route('chefchantier.transactions.valid')}}" class="{{Route::is('chefchantier.transactions.valid')    ? 'active' : ''}}">Validées</a></li>
        </ul>

    </li> --}}


    <li class="menu-title mt-3 text-bleu">Gestion des commandes</li>
    @php
        //$validated = (new \App\Http\Controllers\Chefchantier\CommandeController())->getByStatus(2);
        // $canceled = (new \App\Http\Controllers\Chefchantier\CommandeController())->getByStatus(4);
        $waiting = (new \App\Http\Controllers\Chefchantier\CommandeController())->getByStatus(1);
        //$delivered = (new \App\Http\Controllers\Chefchantier\CommandeController())->getByStatus(3);
    @endphp

    <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect {{route::is('ccommande.show') ? 'mm-active' : ''}}">
            <i class="uil-file-alt"></i>
            <span>Commandes</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('commande.create')}}" class="{{Route::is('commande.create') ? 'active' : ''}}">Passer une commande</a></li>
            <li><a href="{{route('ccommande.waiting')}}" class="{{Route::is('ccommande.waiting') ? 'active' : ''}}">En attente
                @if($waiting>0 )
                    <span class="badge badge-pill badge-primary float-right">{{$waiting}}</span>
                @endif
            </a></li>
            <li><a href="{{route('ccommande.rejet')}}" class="{{Route::is('ccommande.rejet') ? 'active' : ''}}">Rejetées
                {{-- @if($canceled>0 )
                    <span class="badge badge-pill badge-danger float-right">{{$canceled}}</span>
                @endif --}}
            </a></li>
            <li><a href="{{route('ccommande.valider')}}" class="{{Route::is('ccommande.valider') ? 'active' : ''}}">Validées
                {{-- @if($canceled>0 )
                    <span class="badge badge-pill badge-danger float-right">{{$canceled}}</span>
                @endif --}}
            </a></li>
        </ul>
    </li>


    <li class="menu-title mt-3 text-bleu">Gestion des demandes</li>

    {{-- <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-align-left"></i>
            <span>Mains d'Œuvres</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="#" class="">Créer une demande </a></li>
            <li><a href="#" class="">En attente </a></li>
            <li><a href="#" class="">Rejeter </a></li>
            <li><a href="#" class="">Valider </a></li>
        </ul>

    </li> --}}

    <li>
        @php
            $waitingspe = (new \App\Http\Controllers\DemandeSpecialsController())->getByStatus(1);
        @endphp
        <a href="javascript: void(0);" class="{{count($waitingspe) > 0 ? '' : 'has-arrow'}} waves-effect">
            <i class="uil-align-letter-right"></i>
            <span>Demandes spéciales</span>
            @if(count($waitingspe) > 0)
                <span class="badge badge-pill badge-warning float-right">{{count($waitingspe)}}</span>
            @endif
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('cdemande.create')}}" class="">Créer une demande </a></li>
            <li><a href="{{route('cdemande.waiting')}}" class="">En attente
                @if(count($waitingspe) > 0)
                    <span class="badge badge-pill badge-warning float-right">{{count($waitingspe)}}</span>
                @endif
            </a></li>
            <li><a href="{{route('cdemande.rejet')}}" class="">Rejetées </a></li>
            <li><a href="{{route('cdemande.valid')}}" class="">Validées </a></li>
        </ul>

    </li>

    @inject('nbtranscc','App\Helperme\Notifications')
        @php
            $twait = (new \App\Http\Controllers\Chefchantier\HomeController())->getByStatusTransactions(1)->count();
            // $treject = (new \App\Http\Controllers\Chefchantier\HomeController())->getByStatusTransactions(0)->count();
            // $tvalid = (new \App\Http\Controllers\Chefchantier\HomeController())->getByStatusTransactions(2)->count();
        @endphp

    <li class="{{Route::is('chefchantier.st.mainoeuvre') || Route::is('chefchantier.st.paye')|| Route::is('chefchantier.st.lot.paye') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class=" {{$twait==0 ? 'has-arrow' : ''}} waves-effect">
            <i class="uil-money-withdrawal"></i>
            @if($twait>0 )
                <span class="badge badge-pill badge-warning mr-3 float-right">{{$twait}}</span>
            @endif
            <span>Main d'oeuvre</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('chefchantier.st.mainoeuvre')}}" class="{{Route::is('chefchantier.st.mainoeuvre') || Route::is('chefchantier.st.paye')|| Route::is('chefchantier.st.lot.paye') ? 'active' : ''}}">Faire une demande </a></li>
            <li><a href="{{route('chefchantier.transactions')}}" class="{{Route::is('chefchantier.transactions') ? 'active' : ''}}">En attente @if($twait>0 )<span class="badge badge-pill badge-warning">{{$twait}}</span>@endif</a></li>
            <li><a href="{{route('chefchantier.transactions.valid')}}" class="{{Route::is('chefchantier.transactions.valid') ? 'active' : ''}}">Validées</a></li>
            <li><a href="{{route('chefchantier.transactions.reject')}}" class="{{Route::is('chefchantier.transactions.reject') ? 'active' : ''}}">Rejetées</a></li>
        </ul>
    </li>

</ul>
