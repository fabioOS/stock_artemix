@extends('chefchantier.layouts.app')

@section('page_title')
    Editer un sous-traitant
@stop

@section('content_title')
    Editer un sous-traitant
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Sous-traitant</li>
    </ol>
@stop

@section('other_css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{route('chefchantier.st.update')}}" method="post" enctype="multipart/form-data">@csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nom du sous-traitant <span class="text-danger">*</span></label>
                        <input type="text" name="nom" value="{{$soustraitant->nom}}" required class="form-control"/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="slug" value="{{$soustraitant->id}}">
                        <label for="">Téléphone du responsable <span class="text-danger">*</span></label>
                        <input type="text" name="phone" value="{{$soustraitant->contact}}" required class="form-control"/>
                    </div>

                </div>
                <div class="modal-footer">
                    {{--                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>--}}
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                </div>

            </form>
        </div>
    </div>

@endsection


@section('other_js')
    {{--select soustraitant select --}}
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>

    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $(".select2").select2();

        });

        var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des villas',
            selectedListLabel: 'Villas Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });

    </script>

@endsection
