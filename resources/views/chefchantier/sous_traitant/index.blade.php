@extends('chefchantier.layouts.app')

@section('page_title')
    Sous-traitant
@stop

@section('content_title')
    Sous-traitant
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Sous-traitant</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if(1==2)
            <div>
                <button type="button" data-toggle="modal" data-target="#addSous_traitant" class="btn btn-success waves-effect waves-light mb-5"><i class="mdi mdi-plus mr-1"></i> Ajouter un sous-traitant</button>
            </div>
            @endif
            <div class="table-responsive mb-4 gstock">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>Prestataire</th>
                        <th>Contrat à charge</th>
                        <th>Téléphone</th>
                        <th>Date d'ajout</th>
                        <th>Etat</th>
                        <th style="width: 220px!important;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($soustraitants as $k=>$sous)
                        <tr class="font-size-16">
                            <td class="font-weight-bold">{{Str::upper($sous->nom)}}</td>
                            <td>
                                <button type="button" class="btn btn-success waves-effect waves-light"><i class="uil uil-file-medical-alt ml-1"></i> {{count($sous->contrats)}}</button>
                            </td>
                            <td>{{$sous->contact}}</td>
                            <td>{{$sous->created_at->format('d/m/Y')}}</td>
                            <td>
                                @if($sous->etat == 0)
                                    <span class="text-danger"> Rejeter</span>
                                    <button title="Motif" type="button" onclick='displayModalMotif("{{$sous->motif}}")' data-toggle="modal" data-target="#motifetat" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-eye"></i></button>
                                @elseif($sous->etat == 1)
                                    <span class="text-warning"> En attente de validation</span>
                                @else
                                    <span class="text-success"> Actif</span>
                                @endif
                            </td>
                            <td>
                                @if($sous->etat == 2)
                                    <a href="{{route("chefchantier.st.n-contrat",$sous->id)}}" class="btn btn-primary waves-effect waves-light btn-sm">
                                        <i class="uil uil-file-medical-alt mr-1"></i> Contrat
                                    </a>
                                    {{-- <a href="{{route('chefchantier.st.paye',$sous->id)}}" class="btn btn-info waves-effect waves-light btn-sm">
                                        <i class="uil uil-money-insert mr-1"></i> Payer
                                    </a>
                                    <a href="{{route('chefchantier.st.edit',$sous->id)}}" class="btn btn-success waves-effect waves-light btn-sm">
                                        <i class="uil uil-edit mr-1"></i> Editer
                                    </a>--}}
                                @else
                                    {{-- <a href="{{route('chefchantier.st.delete',$sous->id)}}" class="delete btn btn-danger waves-effect waves-light btn-sm">
                                        <i class="uil uil-trash mr-1"></i> Supprimer
                                    </a> --}}
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>

    <div id="addSous_traitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un sous-traitant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('chefchantier.st.store')}}" method="post" enctype="multipart/form-data" autocomplete="off">@csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nom du sous-traitant <span class="text-danger">*</span></label>
                            <input type="text" name="nom" required class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">Téléphone du responsable <span class="text-danger">*</span></label>
                            <input type="text" name="phone" required class="form-control"/>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <div id="lots_straitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Sous-traitant <span class="text-muted" id="sstraitantlb"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="listlot">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="motifetat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Motif <span class="text-muted"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center mb-5" id="motifetatvalu">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div id="addSous_traitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un sous-traitant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('chefchantier.st.store')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nom du sous-traitant <span class="text-danger">*</span></label>
                            <input type="text" name="nom" required class="form-control"/>
                        </div>
                        <div class="form-group">
                            <select multiple="multiple" size="10" name="lot[]" title="duallistbox_demo1[]">
                                @foreach($lots as $lot)
                                    <option value="{{$lot->id}}">Villa {{$lot->lot}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Téléphone du responsable <span class="text-danger">*</span></label>
                            <input type="text" name="phone" required class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="contrat">Contrat <span class="text-danger">*</span></label>
                            <input type="file" class="form-control" required name="contrat" id="contrat" accept=".pdf">
                            <p class="help-block">Format accepté: .PDF</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div> --}}
@endsection


@section('other_js')
    {{--select soustraitant select --}}
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){


            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet sous-traitant",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des villas',
            selectedListLabel: 'Villas Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });
        /*$("#demoform").submit(function() {
            alert($('[name="duallistbox_demo1[]"]').val());
            return false;
        });*/

        function displayModalLot(id,prest){
            //console.log(id);
            var url = '{{route("chefchantier.st.getlot",":id")}}'
            url=url.replace(':id',id);

            $('#sstraitantlb').html(prest);

            $.get(url, function (data) {
                //console.log(data);
                var optionData ='';
                for (var i = 0; i < data.length; i++){
                    //optionData+='<option value="'+data[i].id+'">'+data[i].libelle +'</option>';
                    optionData += '<div class="col-xl-4 col-sm-6">' +
                        '<div class="card text-center">' +
                        '<div class="card-body">' +
                        '<h5 class="font-size-16 mb-1">Villa '+data[i].lot+'</h5> ' +
                        '<p class="text-muted mb-2">'+data[i].type+'</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                $('#listlot').html(optionData);
            });

        }

        function displayModalMotif(motif){
            $('#motifetatvalu').html(motif);
        }

    </script>

@endsection

