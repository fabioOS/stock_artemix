@extends('chefchantier.layouts.app')

@section('page_title')
    Contrat 2/2
@stop

@section('content_title')
    Contrat 2/2
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Sous-traitant</li>
        <li class="breadcrumb-item">Contrat</li>
        <li class="breadcrumb-item active">Etape 2/2</li>
    </ol>
@stop

@section('other_css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="row mb-4">
    <div class="col-xl-3">
        <div class="card h-100">
            <div class="card-body">
                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Nom</h5>
                    <p>{{$stcontrat->straitant->nom}}</p>
                </div>

                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Téléphone</h5>
                    <p>{{$stcontrat->straitant->contact}}</p>
                </div>

                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Libelle du contrat</h5>
                    <p>{{$stcontrat->libelle}}</p>
                </div>

                <div class="text-muted mb-4">
                    <h5 class="font-size-16">Villas assignés</h5>
                    @php $lots = json_decode($stcontrat->motif) @endphp
                    @foreach($lots as $itemlot)
                        @php $lot = \App\ProgrammeLot::where('id',$itemlot)->with('actifs')->first(); @endphp
                        <div class="badge badge-soft-primary font-size-12"> Villa {{$lot->lot}} <br> {{$lot->actifs->libelle}}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-9 gstock">
        <div class="card mb-0">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="p-2">AFFECTER LES CORPS D'ETAT </h4>
                    <form action="{{route('chefchantier.st.n-contrat.store')}}" method="post" enctype="multipart/form-data">@csrf
                        <input type="hidden" name="firststep" value="{{$stcontrat->id}}">
                        <div class="modal-body">
                            @foreach($actifs as $k=>$actif)
                                @php
                                    $corpsetats = \App\ProgrammeCorpsetat::where('programme_id',$actif->programme_id)->where('actif_id',$actif->actif_id)->get();
                                    //dd($corpsetats,$actif->programme_id,$actif->id);
                                @endphp

                                @if(count($corpsetats)>0)
                                <div class="form-group">
                                    <label for="contrat">Affecter les corps d'etat <span class="text-primary">{{$actif->actifs->libelle}}</span> <span class="text-danger">*</span></label>
                                    <select class="form-control select2" name="ce_{{$actif->actifs->id}}[]" multiple required>
                                        <option disabled></option>
                                        @foreach($corpsetats as $j=>$corps)
                                            <option value="{{$corps->id}}">{{$corps->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                            @endforeach
                        </div>

                        <div class="modal-footer">
                            <a href="{{route('chefchantier.st')}}" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</a>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
                        </div>

                    </form>
                    <!-- end table -->
                </div>
            </div>

        </div>
    </div>
</div>

@endsection


@section('other_js')
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet corps d\'etat",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

            var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des villas',
            selectedListLabel: 'Villas Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });


        });


    </script>
@endsection
