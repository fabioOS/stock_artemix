@extends('chefchantier.layouts.app')

@section('page_title')
    Sous traitant payer
@stop

@section('content_title')
    Effectuer un paiement
@stop

@php $lot = $sous->lot ; $k = 0 ;
    $actif = \App\ProgrammeActif::where('id',$lot->actif_id)->first();
    // $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$lot->actif_id)->where('soustraitant_id',$sous->soustraitant_id)->with('corpslots')->orderBy('corpsetats_id','asc')->get();
    $corpsetats = \App\LotSoustraitantCorpsetat::where('etatdel',1)->where('actif_id',$lot->actif_id)->where('soustraitant_id',$sous->soustraitant_id)->where('soustraitantcontrat_id',$sous->soustraitantcontrat_id)->with('corpslots')->orderBy('corpsetats_id','asc')->get();
    $programq = \App\Programme::where('id',$sous->soustraitant->programme_id)->first();
@endphp

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Main d'oeuvre</li>
        <li class="breadcrumb-item active">Paiement</li>
        <li class="breadcrumb-item active">{{$actif ? $actif->libelle:''}}</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <h4 class="card-title m-2">Sous-traitant</h4>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">Nom</h5>
                                <p>{{Str::upper($sous->soustraitant->nom)}}</p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">Téléphone</h5>
                                <p>{{Str::upper($sous->soustraitant->contact)}}</p>
                            </div>
                        </div>

                        @if($programq->type_rattachement == 2)
                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">Montant total du contrat</h5>
                                <p>@price($sous->straitantcontrat->montantctr) <sup>Fcfa</sup></p>
                            </div>
                        </div>
                        @endif

                        <div class="col-md-3">
                            <div class="text-muted mb-4">
                                <h5 class="font-size-16">Contrat</h5>
                                <p><a href="{{asset('assets/uploads/contrats/'.$sous->soustraitant->contrat)}}" target="_blank" download="Contract_{{$sous->soustraitant->nom}}.pdf">Contract_{{$sous->soustraitant->nom}}.pdf</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link mb-2 active" id="lot0-tab" data-toggle="pill" href="#lot0" role="tab" aria-selected="true">
                                    <i class="uil uil-home font-size-20"></i>
                                    <span class="">Villa {{$lot->lot}} <br><span class="font-size-12">{{$actif ? $actif->libelle:''}}</span> </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">
                                <div class="tab-pane fade active show" id="lot0" role="tabpanel" aria-labelledby="lot0-tab">
                                    @if(count($corpsetats)>0)
                                        @foreach($corpsetats as $j=>$corps)
                                            @php
                                                $check = \App\Avancement::where([['programme_id',$sous->soustraitant->programme_id],['lot_id',$lot->id],['soustraitant_id',$sous->soustraitant->id],['corpsetat_id',$corps->corpsetats_id]])->orderBy('id','desc')->first();
                                                $tau = $check ? $check->taux : 0;
                                                $couttotal = $corps->corpslots->prix;

                                                if($programq->type_rattachement == 2){
                                                    $montant = \App\ProgrammeSoustraitantcontratMontant::where([['lot_id',$lot->id],['soustraitant_id',$corps->soustraitant_id],['soustraitantcontrat_id',$corps->soustraitantcontrat_id],['corpsetats_id',$corps->corpsetats_id]])->first();
                                                    $couttotal =$montant?$montant->montant : 0;
                                                }
                                            @endphp
                                            <div class="card border border-{{$tau>=100 ? 'success' : 'primary'}}">
                                                @if($tau>=100)
                                                    <a class="text-dark" aria-expanded="true" aria-controls="main_phase{{$k}}">
                                                        @else
                                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#addpaie_straitant" onclick="displayModalEdt('{{$corps->corpsetats_id}}','{{$lot->id}}')" aria-expanded="true" aria-controls="main_phase{{$k}}">
                                                                @endif
                                                                <div class="p-4">
                                                                    <div class="media align-items-center">
                                                                        <div class="mr-3">
                                                                            <div class="avatar-xs">
                                                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="media-body  overflow-hidden">
                                                                            <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                                            <div class="tilte mr-auto p -2">
                                                                        <span class="text-default font-size-13">
                                                                            Coût Total: <b>@price($couttotal) <sup>FCFA</sup></b> &nbsp; | &nbsp;
                                                                            Coût payé: <b>@price(($tau/100) * $couttotal)  <sup>FCFA</sup></b> (<span class="text-warning">{{round($tau,2)}}%</span>) &nbsp; | &nbsp;
                                                                            Taux d'avancement: <b class="text-warning">{{round($tau,2)}}%</b>
                                                                        </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="card border border-primary">
                                            <div class="p-4">
                                                <div class="media align-items-center">
                                                    <div class="media-body overflow-hidden">
                                                        <a href="{{route('chefchantier.st.contrat',$sous->soustraitant->id)}}" class="btn btn-primary btn-rounded waves-effect waves-light" data-toggle="button" aria-pressed="false">
                                                            AJOUTER LES CORPS D'ETAT AU CONTRAT
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 gstock">
            <div class="card h-100">
                <h5 class="card-header mt-0">Résumé de votre opération</h5>
                <div class="col-md-12 m-1 table-responsive">
                    <table class="table mb-0" width="100%">
                        <thead>
                        <tr>
                            <th>Libelle</th>
                            <th>Villa</th>
                            <th>Valeur</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($oldavances as $oldavance)
                            <tr>
                                <td>{{$oldavance->corpsetat ? $oldavance->corpsetat->libelle : '-' }}</td>
                                <td>{{$oldavance->lot ? $oldavance->lot->lot : '-' }}</td>
                                <td>{{$oldavance->taux}} %</td>
                                <td><a class="delteitem" href="{{route('chefchantier.st.paye.delete',$oldavance->id)}}"><i class="uil uil-trash mr-1 text-danger"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(count($oldavances)>0)
                    <form enctype="multipart/form-data" id="sendform" action="{{route('chefchantier.st.paye.save.store2')}}" method="post">
                        @csrf<div class="col-md-12 form-group mt-2">
                            <input type="hidden" value="{{$sous->soustraitant->id}}" name="slug">
                            <input type="hidden" value="{{$straitcontrat->id}}" name="idcontrat">
                            <label class="control-label">Attachement <span class="text-danger">*</span></label>
                            <input type="file" name="rattachment" id="rattachment" class="form-control" required accept="application/pdf">
                            <button type="button" id="btnsend" class="delete btn btn-primary waves-effect waves-light mt-2 mr-2 float-right">Appliquer</button>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div id="addpaie_straitant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Ajouter un taux d'avancement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEdit" action="{{route('chefchantier.st.paye.store')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Taux d'avancement (%) <span class="text-danger">*</span></label>
                            <br>
                            <input type="number" step="any" name="taux" id="taux" class="form-control" max="100" min="0" required>
                            <input type="hidden" id="lotid" name="lotid">
                            <input type="hidden" id="corpsid" name="corpsid">
                            <input type="hidden" id="ctid" name="stid">
                            <input type="hidden" id="idcontrat" name="idcontrat" value="{{$straitcontrat->id}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                if($('#rattachment').val()==''){
                    swal("Oups!", "Veuillez renseigner le champ attachement.");
                }else{
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez-vous vraiment enregistrer cette évaluation.",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            //mettre un loader
                            var btn = document.getElementById("btnsend");
                            btn.disabled = true;
                            btn.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> En cours...';
                            document.getElementById("sendform").submit();
                        }
                    });

                }
            });

        });

        function displayModalEdt(corpid,lotid){
            $("#formEdit")[0].reset();
            //console.log(id);
            var ctid = {{$sous->soustraitant->id}};
            $("#lotid").val(lotid);
            $("#corpsid").val(corpid);
            $("#ctid").val(ctid);

        }

    </script>
@endsection
