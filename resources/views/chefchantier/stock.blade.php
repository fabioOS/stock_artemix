@extends('chefchantier.layouts.app')

@section('page_title')
    Gestion du stock
@stop

@section('content_title')
    Gestion du stock
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Stock</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if ($data->count() > 0 || $filtre != '')
                <div>
                    <div class="float-right">
                        <form class="form-inline mb-3" id="form-filtre" action="{{route('chefchantier.stock.filtre')}}" method="get">
                            @csrf
                            <label class="my-1 mr-2" for="order-selectinput">Seuil</label>
                            <select class="custom-select my-1" id="order-selectinput" name="filtre">
                                <option value="tous" @if($filtre=='tous' || $filtre=='') selected @endif>Tous</option>
                                <option value="suffisant" {{$filtre=='suffisant' ? 'selected' : '' }}>Suffisant</option>
                                <option value="moyen" {{$filtre=='moyen' ? 'selected' : '' }}>Moyen</option>
                                <option value="rupture" {{$filtre=='rupture' ? 'selected' : '' }}>Rupture</option>
                            </select>
                        </form>
                    </div>
                </div>
            @endif
            @if ($data->count() > 0)
                <div class="table-responsive mb-4">
                    <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                        <thead>
                            <tr class="bg-transparent">
                                <th>Matière</th>
                                <th>Disponibilité en stock</th>
                                <th>Unité de mésure</th>
<!--                                <th>Date dernière sortie</th>
                                <th>Qté dernière sortie</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $dt)
                                <tr class="font-size-16">
                                    <td>{{$dt->libelle}}</td>
                                    <td>
                                        @php
                                            $seuil = $dt->seuil;
                                            if ( isset($dt->stocks[0]) && $dt->stocks[0]->qte != null) $stock = $dt->stocks[0]->qte;
                                            else $stock = 0;
                                        @endphp
                                        @if($stock  == 0)
                                            <span class="badge badge-pill badge-danger font-size-14">0</span>
                                        @else
                                            @if ($seuil == 0)
                                                <span class="badge badge-pill badge-success font-size-14">{{$stock}}</span>
                                            @else
                                                @if($stock > $seuil)
                                                    <span class="badge badge-pill badge-success font-size-14">{{$stock}}</span>
                                                @else
                                                    <span class="badge badge-pill badge-warning font-size-14">{{$stock}}</span>
                                                @endif
                                            @endif
                                        @endif
                                    </td>
                                    <td>{{$dt->type->libelle}}</td>
<!--                                    <td>30/08/2021</td>
                                    <td><span class="badge badge-pill badge-primary font-size-14">20</span></td>-->
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end table -->
            @else
                <h5 class="text-warning"> Il n'y a aucune matière pour l'instant !</h5>
            @endif

        </div>
    </div>
@endsection


@section('other_js')
    <script>
        // check last statut of dossier
        $('#order-selectinput').change(function(){
            $('#form-filtre').submit();
        });
    </script>
@endsection
