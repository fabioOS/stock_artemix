@extends('chefchantier.layouts.app')

@section('page_title')
    Transactions en attente de traitement
@stop

@section('content_title')
    Transactions en attente de traitement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Transactions</li>
        <li class="breadcrumb-item active">Transactions en attente de traitement</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table id="datatable-buttons" class="gstock table table-centered datatable table-card-list mb-0">
                    <thead class="thead-light">
                    <tr>
                        <th>Sous-traitant</th>
                        <th>Contrat</th>
                        <th>Villa</th>
                        <th>Montant payé</th>
                        <th>Taux d'avancement</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $k=>$list)
                        @php
                            $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                            // // $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->with('corpslots')->where('etatdel',1)->count();
                            $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                        @endphp
                        <tr>
                            <td>{{$list->soustraitant->nom}}</td>
                            <td><a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank">{{$list->contrat->slug}}</a></td>
                            <td>{{$list->lot->lot}}</td>
                            <td>@price($list->montant) <sup>FCFA</sup></td>
                            <td>
                                <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats !=0 ? round($list->taux/$corpsetats).'%' : '-'!!}</span>
                            </td>
                            <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                            <td>
                                <span class="badge badge-pill badge-warning">En attente de validation par le<br>
                                    {{$list->nivalid==1 ? "CHEF PROJET" : "DP"}}
                                </span>
                            </td>
                            <td class="btnacts">
                                <a href="{{route('chefchantier.transactions.show',encrypt($list->id))}}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-eye"></i> Détail</a>
                                {{-- @if(Auth::user()->id==$list->user_id)<a href="{{route('chefchantier.transactions.delete',encrypt($list->id))}}" class="delete btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash"></i> Supprimer</a>@endif --}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!-- end table-responsive -->
            </div>
        </div>
    </div>
</div>

    <div id="motifetat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Motif <span class="text-muted"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center mb-5" id="motifetatvalu">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
    {{--select soustraitant select --}}
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){


            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cette transaction",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('.btnacts').hide();
                        window.location = href;
                    }
                });
            });

        });

        var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des lots',
            selectedListLabel: 'Lots Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });
        /*$("#demoform").submit(function() {
            alert($('[name="duallistbox_demo1[]"]').val());
            return false;
        });*/

        function displayModalLot(id,prest){
            //console.log(id);
            var url = '{{route("chefchantier.st.getlot",":id")}}'
            url=url.replace(':id',id);

            $('#sstraitantlb').html(prest);

            $.get(url, function (data) {
                //console.log(data);
                var optionData ='';
                for (var i = 0; i < data.length; i++){
                    //optionData+='<option value="'+data[i].id+'">'+data[i].libelle +'</option>';
                    optionData += '<div class="col-xl-4 col-sm-6">' +
                        '<div class="card text-center">' +
                        '<div class="card-body">' +
                        '<h5 class="font-size-16 mb-1">Lot '+data[i].lot+'</h5> ' +
                        '<p class="text-muted mb-2">'+data[i].type+'</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                $('#listlot').html(optionData);
            });

        }

        function displayModalMotif(motif){
            $('#motifetatvalu').html(motif);
        }

    </script>

@endsection

