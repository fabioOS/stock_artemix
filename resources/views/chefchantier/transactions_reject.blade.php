@extends('chefchantier.layouts.app')

@section('page_title')
    Transactions rejetées
@stop

@section('content_title')
    Transactions rejetées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Transactions rejetées</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <div class="table-responsive mb-4 gstock">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>Num</th>
                        <th>Sous-traitant</th>
                        <th>Contrat</th>
                        <th>Villa</th>
                        <th>Montant à payé</th>
                        <th>Taux d'avancement</th>
                        <th>Date</th>
                        <th>Statut</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $k=>$trans)
                        @php
                            $typlot = \App\ProgrammeLot::where('id',$trans->lot_id)->first();
                            $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$trans->soustraitant->id)->where('soustraitantcontrat_id',$trans->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                        @endphp

                        <tr class="font-size-16">
                            <td class="font-weight-bold">{{$k+1}}</td>
                            <td>{{$trans->soustraitant->nom}}</td>
                            <td><a href="{{route('chefchantier.st.n-contrat',$trans->soustraitant->id)}}" target="_blank"> {{$trans->contrat->slug}}</a></td>
                            <td>{{$trans->lot->lot}}</td>
                            <td>@price($trans->montant) <sup>FCFA</sup></td>
                            <td>{{round($trans->taux/$corpsetats)}}%</td>
                            <td>{{$trans->created_at->format("d/m/Y")}}</td>
                            <td>
                                @if($trans->etat == 0)
                                    <span class="text-danger"> Rejeté</span>
                                    <button title="Motif" type="button" onclick='displayModalMotif("{{$trans->motif}}")' data-toggle="modal" data-target="#motifetat" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-eye"></i></button>
                                @elseif($trans->etat == 1)
                                    <span class="text-warning"> En attente de validation</span>
                                @else
                                    <span class="text-success"> Validé</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('chefchantier.transactions.show',encrypt($trans->id))}}" class="btn btn-success waves-effect waves-light btn-sm">
                                    <i class="uil uil-eye mr-1"></i> Voir
                                </a>

                                {{-- <a href="{{route('chefchantier.transactions.delete',encrypt($trans->id))}}" class="delete btn btn-danger waves-effect waves-light btn-sm">
                                    <i class="uil uil-trash mr-1"></i> Supprimer
                                </a> --}}
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>

    <div id="motifetat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Motif <span class="text-muted"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center mb-5" id="motifetatvalu">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
    {{--select soustraitant select --}}
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){


            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cette transaction",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des lots',
            selectedListLabel: 'Lots Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });
        /*$("#demoform").submit(function() {
            alert($('[name="duallistbox_demo1[]"]').val());
            return false;
        });*/

        function displayModalLot(id,prest){
            //console.log(id);
            var url = '{{route("chefchantier.st.getlot",":id")}}'
            url=url.replace(':id',id);

            $('#sstraitantlb').html(prest);

            $.get(url, function (data) {
                //console.log(data);
                var optionData ='';
                for (var i = 0; i < data.length; i++){
                    //optionData+='<option value="'+data[i].id+'">'+data[i].libelle +'</option>';
                    optionData += '<div class="col-xl-4 col-sm-6">' +
                        '<div class="card text-center">' +
                        '<div class="card-body">' +
                        '<h5 class="font-size-16 mb-1">Lot '+data[i].lot+'</h5> ' +
                        '<p class="text-muted mb-2">'+data[i].type+'</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                $('#listlot').html(optionData);
            });

        }

        function displayModalMotif(motif){
            $('#motifetatvalu').html(motif);
        }

    </script>

@endsection

