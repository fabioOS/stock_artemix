@extends('chefchantier.layouts.app')

@section('page_title')
    Transactions détail
@stop

@section('content_title')
    Transactions détail
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Transactions</li>
        <li class="breadcrumb-item active">Détail</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-duallistbox.css')}}">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-3 text-primary">IDENTIFIANT SOUS-TRAITANT</h4>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="text-muted">
                            <h5 class="font-size-15 mb-2">SOUS-TRAITANT</h5>
                            <p>{{$trans->soustraitant->nom}}</p>

                            <div>
                                <h5 class="font-size-16 mb-2">CONTACT SOUS-TRAITANT</h5>
                                <p>{{$trans->soustraitant->contact}}</p>
                            </div>

                            @if($trans->contrat)
                            <div>
                                <h5 class="font-size-16 mb-2">CONTRACT</h5>
                                <p> {{$trans->contrat->slug}} <br><a title="Voir le contrat" href="{{asset('assets/uploads/contrats/'.$trans->contrat->contrat)}}" target="_blank">{{$trans->contrat->libelle}}.pdf</a></p>
                            </div>
                            @endif

                            <div>
                                <h5 class="font-size-16 mb-2">MONTANT CONTRAT</h5>
                                <p> @price($trans->contrat->montantctr ??0) <sup>FCFA</sup></p>
                            </div>

                            <div>
                                <h5 class="font-size-16 mb-1">LOT/ILOT</h5>
                                @php $typ = \App\ProgrammeActif::where('id',$trans->lot->actif_id)->first() @endphp
                                <p>{{$trans->lot->lot}} / {{$trans->lot->ilot}} ({{$typ->libelle}})</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">AGENT</h5>
                                <p>{{$trans->user->name}}</p>
                            </div>
                        </div>

                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">DATE  :</h5>
                                <p>{{$trans->created_at->format('d-m-Y H:i')}}</p>
                            </div>
                        </div>

                        @if($trans->rattachment)
                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">FICHE D'ATTACHEMENT  :</h5>
                                <p><a title="Voir la fiche d'attachement" href="{{asset('assets/uploads/rattachment/'.$trans->rattachment)}}"  target="_blank">Voir la fiche d'attachement</a></p>
                            </div>
                        </div>@endif

                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">STATUS</h5>
                                <p>
                                    @if($trans->etat==1)
                                        <span class="text-warning">En attente de traitement du<br>
                                            {!! $trans->nivalid==1 ? 'CHEF PROJET' : 'DP' !!}
                                        </span>
                                    @elseif($trans->etat==2)
                                        <span class="text-success">Validé</span>
                                    @else
                                        <span class="text-danger">Rejeté</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                        @if($trans->etat==2)
                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">Etat de paiement</h5>
                                <p>
                                    @if($trans->status==1)
                                        <span class="text-danger">En attente de paiement</span>
                                    @else
                                        <span class="text-success">Payer</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                        @elseif ($trans->etat==0)
                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">MOTIF DE REJET</h5>
                                <p>
                                    {!! nl2br($trans->motif)!!}
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-3 text-primary">TAUX D'EVOLUTION</h4>
                <hr>
                <div class="py-2">
                    <div class="table-responsive">
                        <table class="table table-nowrap table-centered mb-0">
                            <thead>
                            <tr>
                                <th style="width: 70px;">No.</th>
                                <th>Corps d'etat</th>
                                <th>Montant total</th>
                                <th>Taux d'avancement</th>
                                <th>Coût à payer</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lists as $k=>$listran)
                                @php
                                    $couttotal = $listran->corpsetat->prix;
                                    if($listran->programme->type_rattachement == 2){
                                        $montant = \App\ProgrammeSoustraitantcontratMontant::where([['lot_id',$listran->lot_id],['soustraitant_id',$listran->soustraitant_id],['corpsetats_id',$listran->corpsetat_id]])->orderBy('id','desc')->first();
                                        $couttotal =$montant?$montant->montant : 0;
                                    }
                                @endphp

                                <tr>
                                    <th scope="row">{{$k+1}}</th>
                                    <td>
                                        <h5 class="font-size-15 mb-1">{{$listran->corpsetat->libelle}}</h5>
                                    </td>
                                    <td>@price($couttotal) <sup>Fcfa</sup></td>
                                    <td>{{round($listran->taux_execute,2)}} %</td>
                                    <td>@price($listran->cout) <sup>Fcfa</sup></td>
                                </tr>
                            @endforeach

                            <tr>
                                <th scope="row" colspan="4" class="border-0 text-right mt-5">Total</th>
                                <td class="border-0 text -right"><h4 class="m-0">@price($trans->montant) Fcfa</h4></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-print-none mt-4">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('other_js')
    {{--select soustraitant select --}}
    {{--    <script src="{{asset('assets/js/slim.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.js')}}"></script>
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){


            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cette transaction",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        var demo1 = $('select[name="lot[]"]').bootstrapDualListbox({
            nonSelectedListLabel: 'Liste des villas',
            selectedListLabel: 'Villas Sélectionnés',
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Tout déplacer',
            removeAllLabel: 'Enlever tout'
        });
        /*$("#demoform").submit(function() {
            alert($('[name="duallistbox_demo1[]"]').val());
            return false;
        });*/

        function displayModalLot(id,prest){
            //console.log(id);
            var url = '{{route("chefchantier.st.getlot",":id")}}'
            url=url.replace(':id',id);

            $('#sstraitantlb').html(prest);

            $.get(url, function (data) {
                //console.log(data);
                var optionData ='';
                for (var i = 0; i < data.length; i++){
                    //optionData+='<option value="'+data[i].id+'">'+data[i].libelle +'</option>';
                    optionData += '<div class="col-xl-4 col-sm-6">' +
                        '<div class="card text-center">' +
                        '<div class="card-body">' +
                        '<h5 class="font-size-16 mb-1">Villa '+data[i].lot+'</h5> ' +
                        '<p class="text-muted mb-2">'+data[i].type+'</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                $('#listlot').html(optionData);
            });

        }

        function displayModalMotif(motif){
            $('#motifetatvalu').html(motif);
        }

    </script>

@endsection

