@extends('chefchantier.layouts.app')

@section('page_title')
    Transactions détail
@stop

@section('content_title')
    Transactions détail
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Transactions</li>
        <li class="breadcrumb-item active">Détail</li>
    </ol>
@stop

@section('other_css')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-3 text-primary">IDENTIFIANT SOUS-TRAITANT</h4>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="text-muted">
                            <h5 class="font-size-15 mb-2">SOUS-TRAITANT</h5>
                            <p>{{$trans->soustraitant->nom}}</p>

                            <div>
                                <h5 class="font-size-16 mb-2">CONTACT SOUS-TRAITANT</h5>
                                <p>{{$trans->soustraitant->contact}}</p>
                            </div>

                            @if($trans->contrat)
                                <div>
                                    <h5 class="font-size-16 mb-2">CONTRACT</h5>
                                    <p> {{$trans->contrat->slug}} <br><a title="Voir le contrat" href="{{asset('assets/uploads/contrats/'.$trans->contrat->contrat)}}" target="_blank">{{$trans->contrat->libelle}}.pdf</a></p>
                                </div>
                            @endif

                            <div>
                                <h5 class="font-size-16 mb-2">MONTANT CONTRAT</h5>
                                <p> @price($trans->contrat->montantctr ??0) <sup>FCFA</sup></p>
                            </div>

                            <div>
                                <h5 class="font-size-16 mb-1">LOT/ILOT</h5>
                                @php $typ = \App\ProgrammeActif::where('id',$trans->lot->actif_id)->first() @endphp
                                <p>{{$trans->lot->lot}} / {{$trans->lot->ilot}} ({{$typ->libelle}})</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">AGENT</h5>
                                <p>{{$trans->user->name}}</p>
                            </div>
                        </div>

                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">DATE</h5>
                                <p>{{$trans->created_at->format('d-m-Y H:i')}}</p>
                            </div>
                        </div>

                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">STATUS</h5>
                                <p>
                                    @if($trans->etat==1)
                                        <span class="text-warning">En attente de traitement du<br>
                                            {!! $trans->nivalid==1 ? 'CHEF PROJET' : 'DP' !!}
                                        </span>
                                    @elseif($trans->etat==2)
                                        <span class="text-success">Validé</span>
                                    @else
                                        <span class="text-danger">Rejeté</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                        @if($trans->etat==2)
                        <div class="text-muted text-sm-right">
                            <div class="mt">
                                <h5 class="font-size-16 mb-1">ETAT DE PAIEMENT</h5>
                                <p>
                                    @if($trans->status==2)
                                        <span class="badge badge-pill badge-success font-size-12">Payer</span>
                                    @else
                                        <span class="badge badge-pill badge-danger font-size-12">En attente de paiement</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="py-2">
                    <div class="table-responsive">
                        <table class="table table-nowrap table-centered mb-0">
                            <thead>
                            <tr>
                                <th style="width: 70px;">No.</th>
                                <th>Corps d'etat</th>
                                <th>Montant total</th>
                                <th>Taux d'avancement</th>
                                <th>Coût payé</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($listrans as $k=>$listran)
                            @php
                                $couttotal = $listran->corpsetat->prix;
                                if($listran->programme->type_rattachement == 2){
                                    $montant = \App\ProgrammeSoustraitantcontratMontant::where([['lot_id',$listran->lot_id],['soustraitant_id',$listran->soustraitant_id],['corpsetats_id',$listran->corpsetat_id]])->orderBy('id','desc')->first();
                                    $couttotal =$montant?$montant->montant : 0;
                                }
                            @endphp

                                <tr>
                                    <th scope="row">{{$k+1}}</th>
                                    <td> <h5 class="font-size-15 mb-1">{{$listran->corpsetat->libelle}}</h5> </td>
                                    <td>@price($couttotal) <sup>Fcfa</sup></td>
                                    <td>{{round($listran->taux_execute,2)}} %</td>
                                    <td>@price($listran->cout) <sup>Fcfa</sup></td>
                                </tr>
                            @endforeach

                            <tr>
                                <th scope="row" colspan="4" class="border-0 text-right">Total</th>
                                <td class="border-0 text -right"><h4 class="m-0">@price($trans->montant) Fcfa</h4></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-print-none mt-4">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('other_js')
@endsection

