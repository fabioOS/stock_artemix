@extends('daf.layout')
@section('page_title')
    Liste des affectations produits
@stop

@section('content_title')
    Liste des affectations produits
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Affections</li>
        <li class="breadcrumb-item active">Liste des affectations produits </li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="mb-5">
                    <a href="#!" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#addProg">
                        <i class="mdi mdi-plus mr-2"></i> Ajouter une affectation
                    </a>

                    <a href="#!" class="btn btn-danger waves-effect waves-light float-right" data-toggle="modal" data-target="#addProgExcel">
                        <i class="fa fa-file-excel mr-2"></i> Importer le modèle
                    </a>
                    <p><i>Télécharge le modèle d'importation <a href="{{route('daf.affectations.model')}}" >ici</a></i></p>
                </div>

                <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Code</th>
                        <th>Catégorie</th>
                        <th>Nom</th>
                        <th>Affections</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($affectations as $k=>$item)
                        <tr>
                            <td>{{$k+1}}</td>
                            <td>{{$item->code}}</td>
                            <td>
                                @if($item->type_materiel == "materiel")
                                    <span class="badge badge-primary badge-sm">Matériel</span>
                                @else
                                    <span class="badge badge-success badge-sm">Matériau</span>
                                @endif
                            </td>
                            <td>{{$item->libelle}} <sup class="badge badge-info">{{$item->type ? $item->type->libelle : ''}}</sup></td>
                            <td><span class="font-weight-bold text-uppercase">{{$item->code_affect ?? '-'}}</span></td>
                            <td>
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="#" onclick="displayModalE(`{{$item->id}}`)" class="editerProgramme px-2 text-primary" data-toggle="modal" data-target="#updateProg" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div>

 <!-- modal affectation -->
<div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addProgLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="addProglLabel">Ajouter une affectation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form enctype="multipart/form-data" action="{{route('daf.affectations.store')}}" method="post" id="progForm">
                @csrf
                <div class="modal-body">
                    <div id="selectpdt">
                        <div class="form-group">
                            <label>Produit <span class="text-danger">*</span></label>
                            <select id="pdt" name="pdt" class="select2 form-control pdtchang" style="width: 100%">
                                <option value="" disabled selected>Selectionnez un produit</option>
                                @php $produits = $affectations->where('code_affect',null); @endphp
                                @foreach($produits as $produit)
                                    <option value="{{$produit->id}}" data-type="{{$produit->type->libelle}}" data-cat="{{$produit->type_materiel}}">{{$produit->libelle}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Catégorie </label>
                                    <input type="text" id="cat" readonly class="form-control" style="background-color: #e7651ade;color:#FFF">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Unité </label>
                                    <input type="text" id="type" readonly class="form-control" style="background-color: #e7651ade;color:#FFF">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Affection CG <span class="text-danger">*</span></label>
                        <input type="text" name="affectation" class="form-control" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                    <button type="button" id="sendpdt" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="addProgExcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addProgExcelLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="addProglLabel">Importer le modèle renseigner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('daf.affectations.import')}}" method="post" id="progForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label id="ufile">Fichier</label>
                            <input type="file" id="ufile" name="ufile" accept=".XLSX" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Importer</button>
                    </div>
                </form>
            </div>
        </div>
</div>

<div id="updateProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="updateProglLabel">Modifier une affection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form enctype="multipart/form-data" action="{{route('daf.affectations.update')}}" method="post" id="formEdit">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Produit <span class="text-danger">*</span></label>
                        <input type="text" id="pdtedit" name="pdt" class="form-control" readonly style="background-color: #e7651ade;color:#FFF">
                        {{-- <div id="secpdtedit">
                            <select id="pdtedit" name="pdt" class="select2 form-control pdtchang" style="width: 100%">
                            </select>
                        </div> --}}
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Catégorie </label>
                                <input type="text" id="catedit" readonly class="form-control" style="background-color: #e7651ade;color:#FFF">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Unité </label>
                                <input type="text" id="typedit" readonly class="form-control" style="background-color: #e7651ade;color:#FFF">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Affection CG <span class="text-danger">*</span></label>
                        <input type="text" id="affectationedit" name="affectation" class="form-control" required="">
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" id="idslug" name="slug">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                </div>
            </form>

        </div>
    </div>
</div>

@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    {{-- <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script> --}}
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });

        $('#pdt').change(function() {
            var actiflibel = $("#pdt").find(':selected').data('type');
            var catlibel = $("#pdt").find(':selected').data('cat');
            $('#type').val(actiflibel);
            $('#cat').val(catlibel);
        });

        $('#sendpdt').click(function() {
            var pdt = $('#pdt').val();
            var affectation = $('input[name="affectation"]').val();
            if(pdt == null || affectation == ''){
                swal("Erreur", "Veuillez renseigner tous les champs", "error");
            }else{
                $('#progForm').submit();
            }
        });

        function displayModalE(pdtid){
            $("#formEdit")[0].reset();
            $("#idslug").val(pdtid);
            // var optionData ='';

            var data ={!! json_encode($affectations) !!};
            // $("#typedit").val(type);
            // $("#catedit").val(type_materiel);

            for (var i = 0; i < data.length; i++){
                var select = '';
                if(data[i].id == pdtid){
                    select = 'selected';
                    $("#affectationedit").val(data[i].code_affect);
                    $("#pdtedit").val(data[i].libelle);
                    $("#typedit").val(data[i].type.libelle);
                    $("#catedit").val(data[i].type_materiel);
                }
                // optionData+='<option value="'+data[i].id+'" data-type="'+data[i].type.libelle+'" data-cat="'+data[i].type_materiel+'" '+select+'>'+data[i].libelle +'</option>';
            }
            // $('#pdtedit').html(optionData);

        }
    </script>
@endsection
