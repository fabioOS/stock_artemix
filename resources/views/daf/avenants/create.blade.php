@extends('daf.layout')

@section('page_title')
    Créer avenant
@stop

@section('content_title')
    Créer avenant
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        {{-- <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li> --}}
        <li class="breadcrumb-item active">Créer avenant</li>
    </ol>
@stop

@section('content')

{{-- @dd(Request::get('code')) --}}
<div class="row">
    <div class="col-lg-12">
        <form id="addappelfonds" action="{{route('daf.avenants.store')}}" class="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT CONTRAT & SOUS-TRAITANT</h4>
                    <div class="form-group">
                        <label for="">CONTRAT<span class="text-danger">*</span></label>
                        <select onchange="selectSt()" name="demande" id="demande" class="form-control select2" style="min-width: 300px;" required >
                            <option selected disabled value="">Sélectionner le bon de demande</option>
                            @foreach ($contrats as $item)
                                <option value="{{encrypt($item->id)}}" data-mtt="{{$item->montantctr}}" data-ttpaie="{{$item->totalpayer}}" data-demandeur="{{$item->straitant->nom}}">{{Str::upper($item->slug)}} | {{Str::upper($item->libelle)}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">SOUS-TRAITANT</label>
                        <input type="text" readonly id="demandeur" class="form-control">
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">STATS. MONTANT</h4>

                    <div class="form-group">
                        <label for="">MONTANT DU CONTRAT</label>
                        <input type="text" id="mttctr" class="form-control" readonly disabled>
                    </div>

                    <div class="form-group">
                        <label for="">MONTANT PAYER</label>
                        <input type="text" id="mttpayer" class="form-control" readonly disabled>
                    </div>

                    <div class="form-group">
                        <label for="">MONTANT RESTANT A PAYER</label>
                        <input type="text" id="mttrestpayer" class="form-control" readonly disabled>
                    </div>

                    {{-- <div class="form-group">
                        <label for="">MODE DE PAIEMENT <span class="text-danger">*</span></label>
                        <select name="mode" id="mode" class="form-control select_plugin" style="min-width: 300px;" required>
                            <option selected disabled>Sélectionner un mode</option>
                            <option value="ESPECE">ESPECE</option>
                            <option value="CHEQUE">CHEQUE</option>
                            <option value="VIREMENT">VIREMENT</option>
                        </select>
                    </div> --}}

                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">AVENANT</h4>

                    <div class="form-group">
                        <label for="price">MONTANT DE L'AVENANT <span class="text-danger">*</span></label>
                        <input type="text" id="price" name="price" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="photo">FICHE NUMERIQUE DE L'AVENANT <span class="text-danger">*</span></label>
                        <input type="file" class="form-control-file" name="file" required="" accept=".pdf" id="file">
                        <p class="help-block mt-1 text-muted">Fichier .PDF accepté</p>
                    </div>

                    <div class="form-group">
                        <label for="note">NOTE</label>
                        <textarea name="note" id="note" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="loader" class="dispayNone">
                    <div class="spinner-border text-primary m-1" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Enregistrer</button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('other_css')
{{-- <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
@endsection

@section('other_js')
<script src="{{ asset('assets/js/imask.js') }}"></script>
{{-- <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script> --}}
    <script>
        var valueSelect = '',
            inputs = [],
            arr = [],
            valuemontantt = 0,
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();

            IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });
        });

        function selectSt(){
            //$("#demandeur").empty();
            var actiflibel = $("#demande").find(':selected').data('demandeur');
            $("#demandeur").val(actiflibel);

            var mttctr = $("#demande").find(':selected').data('mtt');
            var ttpaie = $("#demande").find(':selected').data('ttpaie');
            var mttrestpayer = mttctr - ttpaie;

            //change max value to Imask
            var imask = IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: mttrestpayer,
                thousandsSeparator: ' '
            });

            $("#mttctr").val(mttctr.toLocaleString());
            $("#mttpayer").val(ttpaie.toLocaleString());
            $("#mttrestpayer").val(mttrestpayer.toLocaleString());
        }

        function submitForm() {
            event.preventDefault();
            if($('#demande').val()==''){
                swal("Oups!", "Veuillez selectionnez le contrat.");
            }else if($('#price').val()==''){
                swal("Oups!", "Veuillez renseigner le montant de l'avenant.");
            }else if($('#file').val()==''){
                swal("Oups!", "Veuillez renseigner la fiche numérique.");
            }else{
                //var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de créer un avenant ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#loader').show();
                        document.getElementById('addbtn').disabled = true;
                        $('#addbtn').hide();
                        document.getElementById('addappelfonds').submit();
                    }
                });
            }

        }

    </script>

@endsection
