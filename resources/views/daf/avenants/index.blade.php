@extends('daf.layout')
@section('page_title')
    Liste des avenants
@stop

@section('content_title')
    Liste des avenants @isset($typelibel) {{$typelibel}} @endisset
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Gestion des demandes</li>
        <li class="breadcrumb-item active">Liste des avenants @isset($typelibel) {{$typelibel}} @endisset</li>
    </ol>
@stop

@section('content')
    @include('_stats_evolution')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table id="datatable-buttons" class="gstock table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>N°</th>
                            <th>Sous-traitant</th>
                            <th>Contrat</th>
                            <th>Montant du contrat</th>
                            <th>Montant payé</th>
                            <th>Montant de l'avenant</th>
                            <th>Date</th>
                            <th>Agent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @isset($avenants)
                            @foreach($avenants as $k=>$list)
                                @php
                                @endphp
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td>{{$list->soustraitant->nom}}</td>
                                    <td>{{$list->contrat->slug}} <br>
                                        {{$list->contrat->libelle}} <a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank"><i class="fa fa-file-pdf"></i></a></td>
                                    <td>@price($list->contrat->montantctr) <sup>FCFA</sup></td>
                                    <td>@price($list->totalpayer) <sup>FCFA</sup></td>
                                    <td>@price($list->montant) <sup>FCFA</sup></td>
                                    <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                    <td>{{$list->user->name}}</td>
                                    <td>
                                        @if($list->status == 0)
                                            <span class="badge badge-pill badge-danger">Rejeter</span></td>
                                        @elseif ($list->status == 2)
                                            <span class="badge badge-pill badge-success">Valider</span></td>
                                        @elseif ($list->status == 1)
                                            <span class="badge badge-pill badge-warning">En attente</span></td>
                                        @else
                                        @endif
                                    <td>
                                        <a href="{{route('daf.avenants.show',encrypt($list->id))}}" class="btn btn-info waves-effect waves-light btn-sm">
                                            <i class="fa fa-eye"></i> Détail
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
        }

        $(document).ready(function(){
        });
    </script>
@endsection
