@extends('daf.layout')

@section('page_title')
    Avenant détail
@stop

@section('content_title')
    Avenant détail
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Avenant</li>
        <li class="breadcrumb-item active">Détail</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT CONTRAT & SOUS-TRAITANT</h4>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="text-muted">
                                <h5 class="font-size-15 mb-2">SOUS-TRAITANT</h5>
                                <p>{{$item->soustraitant->nom}}</p>

                                <div>
                                    <h5 class="font-size-16 mb-2">CONTACT SOUS-TRAITANT</h5>
                                    <p>{{$item->soustraitant->contact}}</p>
                                </div>

                                @if($item->contrat)
                                <div>
                                    <h5 class="font-size-16 mb-2">CONTRAT</h5>
                                    <p> {{$item->contrat->slug}} <br><a title="Voir le contrat" href="{{asset('assets/uploads/contrats/'.$item->contrat->contrat)}}" target="_blank">{{$item->contrat->libelle}}.pdf</a></p>
                                </div>
                                @endif

                                <div>
                                    <h5 class="font-size-16 mb-2">NOTE</h5>
                                    <p> {!! $item->note??'-' !!} </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">AGENT</h5>
                                    <p>{{$item->user->name}}</p>
                                </div>
                            </div>

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">DATE</h5>
                                    <p>{{$item->created_at->format('d-m-Y H:i')}}</p>
                                </div>
                            </div>

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">STATUS</h5>
                                    <p>
                                        @if($item->status==1)
                                            <span class="text-warning">En attente</span>
                                        @elseif($item->status==2)
                                            <span class="text-success">Validé</span>
                                        @elseif($item->status==0)
                                            <span class="text-danger">Rejeté</span>
                                        @else
                                        @endif
                                    </p>
                                </div>
                            </div>

                            @if($item->status == 0)
                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">MOTIF DE REJET</h5>
                                    <p>
                                        {!! $item->motif !!}
                                    </p>
                                </div>
                            </div>
                            @endif

                            @if($item->etat==2)
                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">ETAT DE PAIEMENT</h5>
                                    <p>
                                        @if($item->status==1)
                                            <span class="text-danger">En attente de paiement</span>
                                        @else
                                            <span class="text-success">Payer</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                            @endif

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    @if($item->status == 1)
                                        @if($item->actionbtn)
                                        <h5 class="font-size-15 mb-3 text-uppercase">Action </h5>

                                        <div class="gstock" id="btnaction">

                                            <button type="button" onclick='displayModalPaie(`{{encrypt($item->id)}}`)' data-toggle="modal" data-target="#paietrans" class="btn btn-success mr-2" title="Valider la demande"> <i class="fa fa-check-circle"></i> </button>
                                            <button type="button" onclick='displayModalRejet("{{encrypt($item->id)}}")' data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter l'avenant"> <i class="fa fa-trash"></i> </button>
                                        </div>
                                        <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status" style="float: right;">
                                            <span class="sr-only">Chargement...</span>
                                        </div>
                                        @endif

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">AVENANT</h4>
                    <hr>
                    <div class="py-2">
                        <div class="row">
                            <div class="col-md-3 col-xl-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <h4 class="mb-1 mt-1 font-size-24"> @price($item->contrat->montantctr) <sup>FCFA</sup></h4>
                                            <p class="text-muted mb-0">MONTANT DU CONTRAT</p>
                                        </div>

                                    </div>
                                </div>
                            </div> <!-- end col-->

                            <div class="col-md-3 col-xl-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <h4 class="mb-1 mt-1 font-size-24"> @price($item->totalpayer) <sup>FCFA</sup></h4>
                                            <p class="text-muted mb-0">MONTANT PAYER</p>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end col-->

                            <div class="col-md-3 col-xl-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <h4 class="mb-1 mt-1 font-size-24"> @price($item->contrat->montantctr - $item->totalpayer) <sup>FCFA</sup></h4>
                                            <p class="text-muted mb-0 text-uppercase">Montant exigible au sous-traitant</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-xl-3">
                                <div class="card bg-success">
                                    <div class="card-body">
                                        <div>
                                            <h4 class="mb-1 mt-1 font-size-24 text-white"> @price($item->montant) <sup>FCFA</sup></h4>
                                            <p class="text-white mb-0 text-uppercase">Montant de l'avenant</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-print-none mt-4">
                            <div class="float-right">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($item->actionbtn)
    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter la transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEditRejet" action="{{route('daf.avenants.rejet')}}" method="post">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif du rejet <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="stid" id="stid" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div id="paietrans" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Effectuer un paiement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formPaiement" action="{{route('daf.avenants.valid')}}" method="post" enctype="multipart/form-data">@csrf
                    <input type="hidden" name="transid" id="transid" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Montant à payer <span class="text-danger">*</span></label>
                            <input name="montant" value="@price($item->montant)" id="montant" min="1" type="text" class="form-control mt-2" readonly="" disabled="">
                        </div>

                        <div class="form-group">
                            <label>Moyen de paiement <span class="text-danger">*</span></label>
                            <select name="moyen" id="moyen" class="form-control" required="">
                                <option selected="" disabled="" value="0">Moyen de paiement</option>
                                <option value="Espece">Espèce</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Virement">Virement</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Bordereau de paiement </label>
                            <input name="bordereau" id="bordereau" class="dropify form-control" type="file">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div id="blockbtn">
                            {{-- <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button> --}}
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Payer</button>
                        </div>
                        <div id="loader2" class="spinner-border text-primary m-1 dispayNone" style="float: right;" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    @endif

@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        function displayModalPaie($slug){
            $('#transid').val($slug);
        }

        function submitform(){
            // alert('ii');
            var desc = $('#moyen').val();
            //console.log(desc);
            if(desc == 'undefined' || desc == null || desc == 0){
                swal("Oups!", "Veuillez renseignez la Moyen de paiement.");
                return false;
            }
            $('#blockbtn').hide();
            $('#loader2').show();
            document.getElementById('formPaiement').submit();

        }

    </script>
@endsection
