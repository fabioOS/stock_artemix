@extends('daf.layout')

@section('page_title')
    Bon de commande en décaissement en attente de traitement
@stop

@section('content_title')
    Bon de commande en décaissement en attente de traitement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de commande en décaissement en attente de traitement</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($bcs) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>N°</th>
                            <th>BC N°</th>
                            <th>Agent</th>
                            <th>Bon demande</th>
                            <th>Fournisseur</th>
                            <th>Montant total</th>
                            <th>Statut</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bcs as $k=>$bc)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a class="text-dark font-weight-bold">{{$bc->ref}}</a> </td>
                                <td>{{$bc->user->name}}</td>
                                <td>{{$bc->demande->ref}}</td>
                                <td>{{$bc->fournisseur->nom}}</td>
                                <td>@price($bc->montant) Fcfa</td>
                                <td>
                                    <span class="badge badge-pill badge-warning">En attente de traitement</span>
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($bc->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="{{route('daf.show', encrypt($bc->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucun bon de commande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
