@extends('daf.layout')

@section('page_title')
    Détails du contrat
@stop

@section('content_title')
    Contrat N° <span class="text-primary">{{strtoupper($contrat->slug)}}</span>
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails du contrat</li>
    </ol>
@stop

@section('content')

    <div class="row mb-4">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT SOUS-TRAITANT</h4>
                    <div class="row">
                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-15 mb-2">SOUS-TRAITANT</h5>
                            <p>{{$contrat->straitant->nom}}</p>
                        </div>

                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16 mb-2">CONTACT SOUS-TRAITANT</h5>
                            <p>{{$contrat->straitant->contact}}</p>
                        </div>

                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16 mb-2">REFERENCE CONTRACT</h5>
                            <p> {{$contrat->slug}} </p>
                        </div>

                        @if($contrat->slug)
                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16 mb-2">CONTRACT</h5>
                            <p> <a title="Voir le contrat" href="{{asset('assets/uploads/contrats/'.$contrat->contrat)}}" target="_blank">{{$contrat->libelle}}.pdf</a></p>
                        </div>
                        @endif

                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16 mb-2">NOMBRE DE VILLA</h5>
                            <p>{{$contrat->lots->count()}}</p>
                        </div>

                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16 mb-2">DATE DE VALIDATION CONTRAT</h5>
                            <p> {{$contrat->updated_at->format('d/m/Y H:i')}} </p>
                        </div>

                        {{-- <div class="text-muted col-lg-12">
                            <h5 class="font-size-16 mb-2">MONTANT DU CONTRAT</h5>
                            <p>@price($contrat->montantctr) <sup>Fcfa</sup></p>
                        </div>

                        <div class="text-success col-lg-12">
                            <h5 class="font-size-16 mb-2">MONTANT PAYER</h5>
                            <p>@price($contrat->totalpaye) <sup>Fcfa</sup></p>
                        </div>

                        <div class="text-danger col-lg-12">
                            <h5 class="font-size-16 mb-2">MONTANT RESTANT A PAYER</h5>
                            <p>@price($contrat->montantctr - $contrat->totalpaye) <sup>Fcfa</sup></p>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            <div class="row">
                <div class="col-md-4 col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <h4 class="mb-1 mt-1 font-size-24">@if($contrat->montantctr == null) 0 @else @price($contrat->montantctr) @endif<sup>FCFA</sup></h4>
                                <p class="text-muted mb-0">MONTANT DU CONTRAT</p>
                            </div>

                        </div>
                    </div>
                </div> <!-- end col-->

                <div class="col-md-4 col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <h4 class="mb-1 mt-1 font-size-24">@if($contrat->totalpaye == null) 0 @else @price($contrat->totalpaye) @endif<sup>FCFA</sup></h4>
                                <p class="text-muted mb-0">MONTANT PAYER</p>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col-->

                <div class="col-md-4 col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <h4 class="mb-1 mt-1 font-size-24">@if($contrat->montantctr-$contrat->totalpaye == null) 0 @else @price($contrat->montantctr-$contrat->totalpaye) @endif<sup>FCFA</sup></h4>
                                <p class="text-muted mb-0 text-uppercase">Montant exigible au sous-traitant</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                {{-- Avoir la liste des transactions du contrat --}}
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <div class="table-responsive mb-4 gstock">
                                    <table id="datatable-buttons" class="table table-centered datatable " >
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>REF</th>
                                                <th>Villa</th>
                                                <th>Montant à payer</th>
                                                <th>Taux d'avancement</th>
                                                <th>Date de l'attachement</th>
                                                <th>Agent</th>
                                                <th>Status</th>
                                                <th>Date de paiement</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($paiements as $k=>$item)
                                            @php
                                                $typlot = \App\ProgrammeLot::where('id',$item->lot_id)->first();
                                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot?$typlot->actif_id:null)->where('soustraitant_id',$item->soustraitant->id)->where('soustraitantcontrat_id',$item->soustraitantcontrat_id)->where('etatdel',1)->count();
                                            @endphp
                                            <tr class="font-size-16">
                                                <td>{{$k+1}}</td>
                                                <td class="font-weight-bold">{{Str::upper($item->slug)}}</td>
                                                <td>{{$item->lot?$item->lot->lot:'-'}}</td>
                                                <td>@price($item->montant) <sup>FCFA</sup></td>
                                                <td>
                                                    <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats !=0 ? round($item->taux/$corpsetats).'%' : '-'!!}</span>
                                                </td>
                                                <td>{{$item->created_at->format("d/m/Y H:i")}}</td>
                                                <td>{{$item->user?$item->user->name : ''}}</td>
                                                <td>
                                                    @if($item->status == 1)
                                                        <span class="badge badge-pill badge-soft-warning font-size-12"> En attente de validation</span>
                                                    @elseif($item->status == 3)
                                                        <span class="badge badge-pill badge-soft-danger font-size-12"> En attente de paiement</span>
                                                    @else
                                                        <span class="badge badge-pill badge-soft-success font-size-12"> Payer</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item->status == 1)
                                                        -
                                                    @else
                                                        {{$item->updated_at->format("d/m/Y H:i")}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- end table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('other_js')

@endsection
