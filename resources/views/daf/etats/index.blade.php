@extends('daf.layout')

@section('page_title')
    Statistiques par villa
@stop

@section('content_title')
    Statistiques par villa
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Statistiques par villa</li>
    </ol>
@stop

@section('other_css')
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row mb-2">

        <div class="col-lg-12 card p-3">

            <form class="form-inline" action="{{route('daf.etats.search')}}" method="GET">

                <div class="form-group col-lg-3">
                    <select name="tranche" class="select2 form-control" style="width: 100%">
                        <option value="">-- Choisir une tranche --</option>
                        @foreach($tranches as $tranche)
                            <option value="{{$tranche->tranche}}" {{Request::get('tranche')==$tranche->tranche ? 'selected' : ''}}>{{$tranche->tranche}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-lg-3">
                    <select name="bloc" class="select2 form-control" style="width: 100%;">
                        <option value="">-- Choisir un bloc --</option>
                    @foreach($blocks as $block)
                            <option value="{{$block->bloc}}" {{Request::get('bloc')==$block->bloc ? 'selected' : ''}}>{{$block->bloc}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-lg-3">
                    <select name="soustraitant" class="select2 form-control" style="width: 100%;">
                        <option value="">-- Choisir un Sous-traitant --</option>
                    @foreach($soustraitants as $soustraitant)
                            <option value="{{$soustraitant->id}}" {{Request::get('soustraitant')==$soustraitant->id ? 'selected' : ''}}>{{$soustraitant->nom}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-3">
                    <button type="submit" class="btn btn-primary mb-2">Rechercher</button>
                </div>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="card-title mb-4">Dernières transactions par sous-traitant</h4>
                    <table id="datatable" class="table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Villa</th>
                            <th>Bloc</th>
                            <th>Tranche</th>
                            <th>Evolution</th>
                            <th style="width: 200px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listlots as $listlot)
                        @php
                            // $pcent = $listlot->montantLot ? round((($listlot->montbuy)*100) / $listlot->montantLot,1) : 0
                            $nbcorpetat = \App\ProgrammeCorpsetat::where('actif_id',$listlot->actif_id)->count();
                            $tteval = \App\Avancement::where([['programme_id',$listlot->programme_id],['lot_id',$listlot->id]])->sum('taux_execute');
                            $pcent = $nbcorpetat > 0 ? round($tteval / $nbcorpetat,2) : 0;
                        @endphp

                            <tr>
                                <td>{{$listlot->lot}} <br><small> {{$listlot->actifs->libelle}}</span></td>
                                <td>{{$listlot->bloc}}</td>
                                <td>{{$listlot->tranche}}</td>
                                <td>
                                    @if(0 <= $pcent && $pcent <= 30)
                                        <span class="btn btn-danger btn-sm">{{ $pcent }} %</span>
                                    @elseif (31 <= $pcent && $pcent <= 76)
                                        <span class="btn btn-warning btn-sm">{{ $pcent }} %</span>
                                    @elseif (77 <= $pcent && $pcent <= 100)
                                    <span class="btn btn-success btn-sm">{{ $pcent }} %</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('daf.etats.item',$listlot->id)}}" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">
                                        Voir les Statistiques
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
    <!-- select2 init js -->

    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });

    </script>

<script>
    $('#datatable').dataTable({
        "ordering": false,
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty: "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix: "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable: "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo: "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
</script>

@endsection
