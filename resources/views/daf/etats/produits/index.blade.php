@extends('daf.layout')

@section('page_title')
    Etats des produits
@stop

@section('content_title')
    Etats des produits
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Etats des produits</li>
    </ol>
@stop

@section('other_css')
    <!-- Responsive datatable examples -->
    {{-- <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
@endsection

@section('content')
<div class="row">
    <div class="col-lg-8 offset-2">
        <form action="{{route('etats.pdt.store')}}" class="" method="post" id="etatsbc">
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4 text-primary">Edition d'un etat </h4>

                    <div class="form-group">
                        <label for="">NIVEAU </label>
                        <select name="nveau" id="nveau" class="form-control" style="min-width: 300px;">
                            <option selected disabled>Sélectionner un niveau</option>
                            <option value="2">DISPONIBLE</option>
                            <option value="1">FAIBLE</option>
                            <option value="3">INDISPONIBLE</option>
                        </select>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio4" value="pdf" name="typexport" checked="">
                                <label for="inlineRadio4"> PDF </label>
                            </div>
                        </div>
                        <div class="col-md-">
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio5" value="excel" name="typexport">
                                <label for="inlineRadio5"> EXCEL </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div id="loader" class="dispayNone">
                    <div class="spinner-border text-primary m-1" role="loader">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Générer</button>
            </div>
        </form>

    </div>
</div>
@endsection


@section('other_js')
    {{-- <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script> --}}
    {{-- <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script> --}}
    <!-- select2 init js -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });

        function submitForm() {
            event.preventDefault();
            //$('#loader').show();
            //document.getElementById('addbtn').disabled = true;
            document.getElementById('etatsbc').submit();
        }

    </script>


@endsection
