@extends('daf.layout')

@section('page_title')
    Etablie un paiement
@stop

@section('content_title')
    Etablie un paiement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        {{-- <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li> --}}
        <li class="breadcrumb-item active">Etablie un paiement</li>
    </ol>
@stop

@section('content')


<div class="row">
    <div class="col-lg-12">
        <form action="{{route('daf.etbpaie.store')}}" class="" method="post" id="addappelfonds">
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT ATTACHEMENT</h4>
                    <div class="form-group">
                        <label for="">N° TRANS <span class="text-danger">*</span></label>
                        <select onchange="selectSt()" name="demande" id="demande" class="form-control select2" style="min-width: 300px;" required >
                            <option selected disabled>Sélectionner l'attachement</option>
                            @foreach ($paiements as $i=>$paie)
                                <option value="{{$paie->id}}" data-key="{{$i}}" data-demandeur="{{$paie->user->name}} {{$paie->user->prenom}}">{{$paie->slug}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div id="loader1" class="dispayNone text-center mb-2">
                <div class="spinner-border text-primary m-1" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

           <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3 text-primary">ATTACHEMENT</h4>

                            <div class="text-muted">
                                <h5 class="font-size-16">Trans.ID</h5>
                                <p id="tansid"></p>
                                <div class="table-responsive">
                                    <div>
                                        <p class="mb-1">Lot / Ilot:</p>
                                        <h5 class="font-size-16" id="lotid"></h5>
                                    </div>
                                    <div class="mt-4">
                                        <p class="mb-1">Montant de l'attachement :</p>
                                        <h5 class="font-size-16" id="mttattach"></h5>
                                    </div>
                                    <div class="mt-4">
                                        <p class="mb-1">Taux d'avancement :</p>
                                        <h5 class="font-size-16" id="tavance"></h5>
                                    </div>
                                    <div class="mt-4">
                                        <p class="mb-1">Date :</p>
                                        <h5 class="font-size-16" id="ddate"></h5>
                                    </div>
                                    <div class="mt-4">
                                        <p class="mb-1">Agent :</p>
                                        <h5 class="font-size-16" id="agent"></h5>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3 text-primary">SOUS-TRAITANT</h4>

                            <div class="text-muted">
                                <h5 class="font-size-16">Nom </h5>
                                <p id="nomsous"></p>
                                <div class="table-responsive">
                                    <div class="mt-4">
                                        <p class="mb-1">Contact :</p>
                                        <h5 class="font-size-16" id="cttsous"></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-5 text-primary">PRELEVEMENTS SUR ATTACHEMENT</h4>
                    <input type="hidden" id="montanttrans">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-12">Type de prélevement</label>
                                <select name="pdt" id="pdt" class="form-control select2">
                                    <option selected disabled>Sélectionner le type de prélevement</option>
                                    @foreach ($typeprelements as $type)
                                        <option value="{{$type->id}}" data-libelle="{{$type->libelle}}">{{$type->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-12">Montant <span class="text-danger">*</span></label>
                                <input type="text" min="1" id="price" name="price" class="form-control col-md-12" required>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-md-12" style="opacity: 0">Remise (%) </label>
                                <a onclick="add_element_to_array()" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-plus"></i> AJOUTER</a>
                            </div>
                        </div>

                        <div class="col-md-12 mt-3">
                            <input type="hidden" id="varieMontant">
                            <table class="table metable" id="data_session">
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="loader" class="dispayNone">
                    <div class="spinner-border text-primary m-1" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Enregistrer</button>
            </div>
        </form>

    </div>
</div>

<!-- modal update programme -->
{{-- <div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title mt-0" id="updateProglLabel">Ajouter le fournisseur</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form enctype="multipart/form-data" action="{{route('sa.fours.store')}}" method="post">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label>Nom <span class="text-danger">*</span></label>
                    <input type="text" name="nom" class="form-control" required="" value="">
                </div>

                <div class="form-group">
                    <label>Contact <span class="text-danger">*</span></label>
                    <input type="text" name="contact" class="form-control" required="" value="">
                </div>

                <div class="form-group">
                    <label>Adresse</label>
                    <input type="text" name="adresse" class="form-control" value="">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
            </div>
        </form>

    </div><!-- /.modal-content -->
    </div><!-- /.modal add programme-->
</div> --}}

@endsection

@section('other_css')
{{-- <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
@endsection

@section('other_js')
<script src="{{ asset('assets/js/imask.js') }}"></script>
{{-- <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script> --}}
    <script>
        var valueSelect = '',
            inputs = [],
            arr = [],
            valuemontantt = 0,
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();

            IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' ',
                scale: 6,
            });

            var seach = "{{Request::get('code')}}";

            if(typeof seach !== 'undefined' ){
                //alert('ok 0');
                @if($check)
                    //alert("{{$check->id}}");

                    $('#demande').empty();
                    var optionData = '';
                    var selected = '';
                    optionData+='<option disabled>Sélectionner le bon de demande</option>';
                    @foreach($paiements as $i=>$paie)
                        selected = "{{$paie->id == $check->id ? 'selected' : ''}}"
                        optionData+='<option value="{{$paie->id}}" '+selected+' data-key="{{$i}} data-demandeur={{$paie->user->name}}">{{$paie->slug}}</option>';
                    @endforeach

                    $('#demande').html(optionData);

                    $('#data_session').empty();
                    $('#qte').val("");
                    var idarrond = {{$check->id}};
                    var url ="{{ route('daf.etbpaie.get', ':id')}}";
                    url=url.replace(':id',idarrond);
                    SendGet(url);

                @endif
            };

        });


        function selectSt(){
            //$("#demandeur").empty();
            $('#loader1').show();
            $('#data_session').empty();
            var idarrond = $('#demande :selected').val();
            var url ="{{ route('daf.etbpaie.get', ':id')}}";
            url=url.replace(':id',idarrond);

            SendGet(url);
        }

        function SendGet(url){
            $.get(url, function (data) {
                // console.log(data);
                //Put in #id value to data
                $('#tansid').html(data.slug);
                $('#lotid').html(data.lot.lot + ' / ' + data.lot.ilot);
                $('#mttattach').html(Number(data.montant).toLocaleString('fr-FR') +' Fcfa');
                $('#tavance').html(data.taux + ' %');
                $('#ddate').html(data.datefr);
                $('#agent').html(data.user.name);
                $('#nomsous').html(data.soustraitant.nom);
                $('#cttsous').html(data.soustraitant.contact);

                $('#data_session').empty();
                inputs = [];
                $('#loader1').hide();
                $('#montanttrans').val(data.montant);

                valuemontantt = data.montant;
                showhtml();
                $('#mttotal').html(Number(valuemontantt).toLocaleString('fr-FR') +' Fcfa');
            });
        }

        //si #pdt change on ajoute le prix unitaire dans le input #price

        function add_element_to_array(){
            var qtite = 0;
            var priceold = $("#price").val();
            var mtttrans = $("#montanttrans").val();
            var mtttrans = parseFloat(mtttrans.replace(/\s/g,'').replace(',', '.'));
            var price = parseFloat(priceold.replace(/\s/g,'').replace(',', '.'));

            var qtiterest = 0;
            var produits = $("#pdt").val();
            //console.log(produits);

            if(produits == ""){
                swal("Oups!", "Veuillez choisir un type de prélevement ", "error");
                return false ;
            }else if(price == ""){
                swal("Oups!", "Veuillez renseigner le montant du prélevement ", "error");
                return false ;
            }else if(parseFloat(price) > mtttrans){
                swal("Oups!", "Le montant du prélevement ne peut pas être supérieur au montant de l'attachement ", "error");
                return false ;
            } else{
                var valueSelect = $('#pdt');

                var selectId = valueSelect.val(),
                    transid = $('#demande').val(),
                    selectLibell = valueSelect.find(':selected').attr('data-libelle');


                //Check l'existance du item
                var checkexit = inputs.filter(element => {
                    return element.id == selectId
                    // return element.selectId == selectId && element.proprios == proprios
                })

                if(checkexit.length==0){
                    valuemontantt = valuemontantt - price;
                    // console.log(valuemontantt);

                    if(valuemontantt < 0){
                        swal("Oups!", "Le montant du prélevement ne peut pas être supérieur au montant restant de l'attachement ", "error");
                        return false ;
                    }

                    arr = {id:selectId, libelle:selectLibell, price:price};
                    inputs.push(arr);
                    showhtml();
                    $('#mttotal').html(Number(valuemontantt).toLocaleString('fr-FR') +' Fcfa');

                    $('#price').val('');
                }else{
                    swal("Oups!", "Cette ligne existe déjà.", "error");
                    return false ;
                }
            }
        }

        function showhtml() {
            $("#data_session").empty();
            var table = "<tbody>";
            //console.log(inputs);
            table += '<tr class="bg-primary text-white">';
            table += '<td class="border-b">NUM</td>';
            table += '<td class="border-b text-lg font-medium">PRELEVEMENT</td>';
            table += '<td class="border-b text-lg font-medium">MONTANT</td>';
            table += '<td class="border-b">ACTION</td>';
            table += '</tr>';

            // {id:selectId, libelle:selectLibell, price:price};

            for (var i = 0; i < inputs.length; i++) {
                var y = i + 1;
                // console.log(inputs[i].price);
                var priceformat = Number(inputs[i].price).toLocaleString('fr-FR');

                table += '<tr data-row-id='+i+'>';
                table += '<td class="border-b">' + y + '</td>';
                table += '<td class="border-b">' + inputs[i].libelle + '</td>';
                table += '<td class="border-b">' + priceformat + '</td>';
                table += '<td class="border-b"><a style="cursor: pointer" onclick="deleteSession('+i+','+inputs[i].price+')" class="inline-block text-1xl text-orange-500 mx-2" title="Supprimer la ligne"><i data-feather="trash" class="w-4 h-4 mr-2"></i> Supprimer</a></div>';
                table +='<input type="hidden" value="' + inputs[i].id + ';' + inputs[i].libelle + ';' + inputs[i].price +'" name="datasession[]">';
                table += '</tr>';
            };

            table += '<tr class="mt-3">';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b text-lg font-medium">Montant Total à Payer</td>';
            table += '<td class="border-b text-lg font-medium" id="mttotal"></td>';
            table += '<td class="border-b"></td>';
            table += '</tr>';
            table += '</tbody>';

            $("#data_session").append(table);
        }

        function deleteSession(id,mtt) {
            valuemontantt = valuemontantt + mtt;
            inputs.splice(id,1);
            showhtml();
            $('#mttotal').empty();
            // $('#mttotal').html(valuemontantt.toLocaleString()+' Fcfa');
            $('#mttotal').html(Number(valuemontantt).toLocaleString('fr-FR') +' Fcfa');

        }

        function submitForm() {
            event.preventDefault();
            if($('#demande').val()==''){
                swal("Oups!", "Veuillez renseigner le champ N° TRANS.");
            }else{
                //var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de faire d'établie un paiement ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#loader').show();
                        document.getElementById('addbtn').disabled = true;
                        $('#addbtn').hide();
                        document.getElementById('addappelfonds').submit();
                    }
                });
            }

        }

    </script>

@endsection
