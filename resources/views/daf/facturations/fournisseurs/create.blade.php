@extends('daf.layout')
@section('page_title')
    Ajouter une facture de fournisseur
@stop

@section('content_title')
    Ajouter une facture de fournisseur
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Facturations et Paiements</li>
        <li class="breadcrumb-item">Facturation des fournisseurs</li>
        <li class="breadcrumb-item active">Ajouter une facture de fournisseur</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        <form action="{{route('daf.facturation.storeFactFourns')}}" class="" method="post" id="addappelfonds" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3 text-primary">IDENTIFIANT BC</h4>
                            <div class="form-group">
                                <label for="">N° BON COMMANDE <span class="text-danger">*</span></label>
                                <select onchange="selectSt()" name="demande" id="demande" class="form-control select2" style="min-width: 300px;" required >
                                    <option selected disabled>Sélectionner le bon de commande</option>
                                    @foreach ($boncommandes as $demande)
                                        <option value="{{$demande->id}}">{{$demande->ref}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3 text-primary">IDENTIFIANT DE LA FACTURE</h4>

                            <div class="form-group">
                                <label for="">N° FACTURE <span class="text-danger">*</span></label>
                                <input type="text" name="numfacture" id="numfacture" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="date">DATE FACTURE <span class="text-danger">*</span></label>
                                <input type="date" max="{{date('Y-m-d')}}" name="datefacture" id="datefacture" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="date" style="margin: 0;">MONTANT TOTAL (FCFA) <span class="text-danger">*</span></label>
                                <p class="help-block font-italic text-danger">Saisir le montant TTC de la facture</p>
                                <input type="text" name="montant" id="price" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Joindre la facture <span class="text-danger">*</span></label>
                                <input name="file" id="file" class="form-control" type="file" accept="application/pdf" required>
                                <p class="help-block font-italic text-danger">Le fichier doit être au format PDF</p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button id="blockbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Enregistrer</button>
                    </div>

                </div>

                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3 text-primary">PREVISUALISATION DU BON DE COMMANDE</h4>

                            <div id="data_session" class="table-responsive">
                                <span class="text-center">Sélectionner un bon de commande pour voir les détails</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection


@section('other_js')
<script src="{{ asset('assets/js/imask.js') }}"></script>
    <script>
        var mtttotal = 0;

        $(document).ready(function(){
            $('.select2').select2();

            IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: 100000000000000,
                thousandsSeparator: ' ',
                scale: 6,
            });
        });

        function selectSt(){
            var idarrond = $("#demande").val();
            $('#data_session').html('<div class="text-center"><div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div></div>');
            var url ="{{ route('getBCView', ':id')}}";
            url=url.replace(':id',idarrond);

            SendGet(url);
        }

        function SendGet(url){
            $.get(url, function (data) {
                // console.log(data.mtt);
                mtttotal = data.mtt;
                $('#data_session').empty();
                $('#data_session').html(data.vues);
            });
        }

        function submitForm(){
            var idarrond = $("#demande").val();
            var numfacture = $("#numfacture").val();
            var datefacture = $("#datefacture").val();
            var montant = $("#price").val();
            var file = $("#file").val();

            if(idarrond == 'undefined' || idarrond == null || idarrond == 0){
                swal("Oups!", "Veuillez renseignez le bon de commande.");
                return false;
            }

            if(numfacture == 'undefined' || numfacture == null || numfacture == 0){
                swal("Oups!", "Veuillez renseignez le numéro de la facture.");
                return false;
            }

            if(datefacture == 'undefined' || datefacture == null || datefacture == 0){
                swal("Oups!", "Veuillez renseignez la date de la facture.");
                return false;
            }

            if(montant == 'undefined' || montant == null || montant == 0){
                swal("Oups!", "Veuillez renseignez le montant de la facture.");
                return false;
            }

            if(file == 'undefined' || file == null || file == 0){
                swal("Oups!", "Veuillez joindre la facture.");
                return false;
            }

            montant = montant.replace(/\s/g, '');

            if(parseFloat(montant) > mtttotal){
                var mtttotalform = new Intl.NumberFormat().format(mtttotal);
                var montantform = new Intl.NumberFormat().format(montant);
                swal("Oups!", "Le montant de la facture ("+ montantform +") est supérieur au montant total du bon de commande "+ mtttotalform);
                return false;
            }

            var btn = document.getElementById("blockbtn");
            btn.disabled = true;
            btn.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="margin-right: 5px;"> </span> En cours...';
            document.getElementById("addappelfonds").submit();
        }

    </script>
@endsection
