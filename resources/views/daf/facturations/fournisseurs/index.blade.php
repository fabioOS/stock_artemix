@extends('daf.layout')
@section('page_title')
    Facturation des fournisseurs
@stop

@section('content_title')
    Facturation des fournisseurs
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Facturations et Paiements</li>
        <li class="breadcrumb-item active">Facturation des fournisseurs</li>
    </ol>
@stop

@section('content')

<div>
    <a href="{{route('daf.facturation.paie.waiting')}}?type=fournisseur" class="btn btn-primary waves-effect waves-light mb-3"><i class="mdi mdi-plus me-1"></i> Effectuer un paiement</a>
</div>

<div class="row">
    <div class="col-lg-9">

        @include('_stats_evolution_facturation_founis')

        <div class="table-responsive mb-4 gstock">
            <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                <tr class="bg-transparent">
                    <th>N°</th>
                    <th>REF</th>
                    <th>BC</th>
                    <th>N° Facture</th>
                    <th>Fournisseur</th>
                    <th>Montant</th>
                    <th>Document</th>
                    <th>Statut</th>
                    <th>Agent</th>
                    <th>Date reception</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $key => $facture)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td class="text-dark font-weight-bold">{{$facture->ref}}</td>
                            <td><a target="_blank" href="{{route('daf.show', encrypt($facture->boncommande->id))}}" class="font-weight-bold">{{$facture->boncommande->ref}}</a></td>
                            <td>{{$facture->numfacture}}
                                <br>
                                <span class="badge badge-soft-dark font-size-10">{{$facture->created_at->format('d/m/Y')}}</span>
                            </td>
                            <td>{{$facture->fournisseur->nom}}</td>
                            <td>@price($facture->montant) Fcfa</td>
                            <td>
                                @if($facture->fichier != null)
                                    <a href="{{asset('assets/uploads/factures/fournisseurs/'.$facture->fichier)}}" target="_blank" class="btn btn-primary btn-sm waves-effect waves-light"><i class="mdi mdi-file-pdf"></i> Voir</a>
                                @else
                                    <span class="badge bg-danger">Aucun document</span>
                                @endif
                            </td>
                            <td>
                                {!! $facture->getLibelleStatut() !!}
                            </td>
                            <td>{{$facture->user->name}} </td>
                            <td>{{$facture->created_at->format('d/m/Y H:i')}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- end table -->
    </div>

    <div class="col-lg-3">
        <div class="col-lg-12">
            <a href="{{route('daf.facturation.createFactFourns')}}" class="btn btn-primary btn-block mb-3 waves-effect waves-light mb-3"><i class="mdi mdi-plus me-1"></i> Ajouter une facture</a>
        </div>
        <div class="col-md-12">
            {{-- @foreach ($last10 as $item) --}}
                <div class="card">
                    <div class="card-body pb-0">
                        <h4 class="card-title mb-4">Dernières Transactions</h4>

                        @if(count($last10)>0)
                            @foreach ($last10 as $item)
                                <div class="card">
                                    <div class="card-body pb-0">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="text-muted">
                                                    <h5 class="font-size-15 mb-2">{{$item->facture->fournisseur ? $item->facture->fournisseur->nom : ''}}
                                                    <h6 class="text-muted">{{$item->facture ? $item->facture->ref : ''}} <br>
                                                    <span class="small">{{$item->created_at->format("d/m/Y H:i")}} </span>
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="text-success">
                                                    <div class="mt">
                                                        <p>@price($item->montant) FCFA</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center">
                                    <a href="{{route('daf.facturation.historique','fournisseur')}}" class="text-primary font-size-14 fw-medium">Voir toutes les transactions <i class="mdi mdi-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center">
                                    <h6 class="font-size-15 mb-2">Aucune transaction</h6>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            {{-- @endforeach --}}
        </div>
    </div>
</div>

@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        $(document).ready(function(){

            $('.gstock').on('click', '.valid', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment valider cette transaction.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#btnaction').hide();
                        $('#loader').show();
                        window.location = href;
                    }
                });
            });

        });
    </script>
@endsection
