@extends('daf.layout')
@section('page_title')
    Historique des paiements
@stop

@section('content_title')
    Historique des paiements
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Facturations et Paiements</li>
        <li class="breadcrumb-item active">Historique des paiements</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">

        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Paiement des {{$type}}s</h4>
            </div>
            <div class="card-body table-responsive">
                @if($type == 'soustraitant')
                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock">
                        <thead>
                            <tr class="bg-transparent">
                                <th>N°</th>
                                <th>Trans.ID</th>
                                <th>Sous-traitant</th>
                                <th>Montant payer</th>
                                <th>Mode paiement</th>
                                <th>Date paiement</th>
                                <th>Agent</th>
                                <th>Preuve paiement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datas as $key => $data)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td class="text-primary font-weight-bold">{{$data->transaction->slug}}</td>
                                    <td>{{$data->transaction->soustraitant->nom}}</td>
                                    <td>@price($data->montant) Fcfa</td>
                                    <td>{{$data->moyen}}</td>
                                    <td>{{$data->created_at->format('d/m/Y')}}</td>
                                    <td>{{$data->user->name}}</td>
                                    <td>
                                        @if($data->file != null)
                                            <a href="{{asset('assets/uploads/paiements/'.$data->file)}}" target="_blank" class="btn btn-primary btn-sm waves-effect waves-light"><i class="mdi mdi-file-pdf"></i> Voir</a>
                                        @else
                                            <span class="badge bg-danger">Aucun document</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                @elseif($type == 'fournisseur')
                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock">
                        <thead>
                            <tr class="bg-transparent">
                                <th>N°</th>
                                <th>Facture.ID</th>
                                <th>Num Facture</th>
                                <th>Fournisseur</th>
                                <th>Montant payer</th>
                                <th>Mode paiement</th>
                                <th>Date paiement</th>
                                <th>Agent</th>
                                <th>Preuve paiement</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datas as $key => $data)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td class="text-primary font-weight-bold">{{$data->facture->ref}}</td>
                                    <td class="text-dark font-weight-bold">{{$data->facture->numfacture}}</td>
                                    <td>{{$data->facture->fournisseur->nom}}</td>
                                    <td>@price($data->montant) Fcfa</td>
                                    <td>{{$data->moyen}}</td>
                                    <td>{{$data->created_at->format('d/m/Y')}}</td>
                                    <td>{{$data->user->name}}</td>
                                    <td>
                                        @if($data->file != null)
                                            <a href="{{asset('assets/uploads/paiements/'.$data->file)}}" target="_blank" class="btn btn-primary btn-sm waves-effect waves-light"><i class="mdi mdi-file-pdf"></i> Voir</a>
                                        @else
                                            <span class="badge bg-danger">Aucun document</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                @else
                {{-- demandespec --}}
                <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th>Montant</th>
                            <th>Motif</th>
                            <th>Date de demande</th>
                            {{-- <th>Agent</th> --}}
                            <th>Preuve paiement</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datas as $key => $dt)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><a href="#" class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>@price($dt->montant)</td>
                                <td>{!! nl2br($dt->description) !!}
                                    @if($dt->preuve != null)<br>
                                    <a href="{{asset('assets/uploads/demandespeciale/'.$dt->preuve)}}" target="_blank">Voir Pièce-Jointe</a>@endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @php $fill = count($dt->transaction)>= 1 ? $dt->transaction[0] : null; @endphp
                                    @if($fill->file != null)
                                        <a href="{{asset('assets/demandespecials/'.$fill->file)}}" target="_blank" class="btn btn-primary btn-sm waves-effect waves-light"><i class="mdi mdi-file-pdf"></i> Voir</a>
                                    @else
                                        <span class="badge bg-danger">Aucun document</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

                @endif

            </div>
        </div>
    </div>

</div>

@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        $(document).ready(function(){

            $('.gstock').on('click', '.valid', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment valider cette transaction.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#btnaction').hide();
                        $('#loader').show();
                        window.location = href;
                    }
                });
            });

        });
    </script>
@endsection
