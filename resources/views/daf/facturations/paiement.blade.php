@extends('daf.layout')
@section('page_title')
    Effectuer un paiement
@stop

@section('content_title')
    Effectuer un paiement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Facturations et Paiements</li>
        <li class="breadcrumb-item active">Effectuer un paiement </li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form id="listusers" method="get" action="{{route('daf.facturation.create.paie')}}" autocomplete="off">
                        <input type="hidden" name="type" value="{{$type}}">
                        <div class="row align-items-start">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <label class="form-label" for="form-sm-input">Selectionner un {{$type=="soustraitant" ? "soustraitant" : "fournisseur"}} </label>
                                    <select name="userid" id="userid" class="form-control select2" required>
                                        <option selected disabled>Sélectionner un {{$type=="soustraitant" ? "soustraitant" : "fournisseur"}}</option>
                                        @foreach($datas as $item)
                                        <option value="{{encrypt($item['id'])}}">{{$item['nom']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if(isset($paiements) && $paiements != null)
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    @if($type=="soustraitant")
                        <div class="card-title bg-primary">
                            <h5 class="text-white p-3">IDENTIFIANT SOUS-TRAITANT</h5>
                        </div>
                        <div class="card-body">
                            @php $trans = $paiements->first() @endphp

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-muted">
                                        <h5 class="font-size-15 mb-2">SOUS-TRAITANT</h5>
                                        <p>{{$trans->soustraitant->nom}}</p>

                                        <div>
                                            <h5 class="font-size-16 mb-2">CONTACT SOUS-TRAITANT</h5>
                                            <p>{{$trans->soustraitant->contact}}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @else
                    <div class="card-title bg-primary">
                        <h5 class="text-white p-3">IDENTIFIANT FOURNISSEUR</h5>
                    </div>
                    <div class="card-body">
                        @php $fours = $paiements[0] @endphp

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-muted">
                                    <h5 class="font-size-15 mb-2">FOURNISSEUR</h5>
                                    <p>{{$fours->fournisseur->nom}}</p>

                                    <h5 class="font-size-16 mb-2">CONTACT</h5>
                                    <p>{{$fours->fournisseur->contact}}</p>

                                    <h5 class="font-size-16 mb-2">EMAIL</h5>
                                    <p>{{$fours->fournisseur->email}}</p>

                                    <h5 class="font-size-16 mb-2">REGIME</h5>
                                    <p>{{$fours->fournisseur->regime ?? '-'}}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    @endif
                </div>
            </div>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-title bg-primary">
                        <h5 class="text-white p-3 text-uppercase">Listes des paiements en attente</h5>
                    </div>

                    <div class="card-body table-responsive">
                        @if($type=="soustraitant")
                            <table id="datatable" class="gstock table table-centered datatable table-card-list mb-0">
                                <thead>
                                    <tr>
                                        <th>Trans.ID</th>
                                        <th>Contrat</th>
                                        <th>Villa</th>
                                        <th>Montant à payer</th>
                                        <th>Date</th>
                                        <th>PAYER</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($paiements as $k=>$list)
                                    @php
                                        $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                                        $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                                    @endphp
                                    <tr>
                                        <td class="text-body font-weight-bold"><a target="_blank" href="{{route('daf.paie.show',encrypt($list->id))}}">{{$list->slug}}</a></td>
                                        <td><a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank">{{$list->contrat->slug}}</a></td>
                                        <td>{{$list->lot->lot}}</td>
                                        <td>@price($list->montant) <sup>FCFA</sup></td>
                                        <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                        <td>
                                            <button type="button" onclick='displayModalPaie(`{{encrypt($list->id)}}`,`@price($list->montant)`)' data-toggle="modal" data-target="#paietrans" class="btn btn-success" title="Effectuer le paiement"> <i class="fa fa-money-bill"></i> Effectuer le paiement</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <table id="datatable" class="gstock table table-centered datatable table-card-list mb-0">
                                <thead>
                                    <tr>
                                        <th>BC</th>
                                        <th>Fact.ID</th>
                                        <th>Montant à payer</th>
                                        <th>Date</th>
                                        <th>Document</th>
                                        <th>PAYER</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($paiements as $k=>$list)
                                    <tr>
                                        <td class="text-body font-weight-bold"><a target="_blank" href="{{route('daf.show', encrypt($list->boncommande->id))}}">{{$list->boncommande->ref}}</a></td>
                                        <td>{{$list->numfacture}}</td>
                                        <td>@price($list->montant) <sup>FCFA</sup></td>
                                        <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                        <td>
                                            @if($list->fichier != null)
                                                <a href="{{asset('assets/uploads/factures/fournisseurs/'.$list->fichier)}}" target="_blank" class="btn btn-primary btn-sm waves-effect waves-light"><i class="mdi mdi-file-pdf"></i> Voir</a>
                                            @else
                                                <span class="badge bg-danger">Aucun document</span>
                                            @endif
                                        </td>
                                        <td>
                                            <button type="button" onclick='displayModalPaie(`{{encrypt($list->id)}}`,`{{$list->montant}}`)' data-toggle="modal" data-target="#paietrans" class="btn btn-success" title="Effectuer le paiement"> <i class="fa fa-money-bill"></i> Effectuer le paiement</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif


    <div id="paietrans" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Effectuer un paiement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @if($type=="soustraitant")
                    <form id="formPaiement" action="{{route('daf.paie.validpaie')}}" method="post" enctype="multipart/form-data">
                @else
                    <form id="formPaiement" action="{{route('daf.facturation.storepaiefourn')}}" method="post" enctype="multipart/form-data">
                @endif
                @csrf
                    <input type="hidden" name="transid" id="transid" value="">
                    <input type="hidden" name="type" value="{{$type}}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Montant à payer <span class="text-danger">*</span></label>
                            <input name="montant" id="montant" min="1" type="text" class="form-control mt-2" readonly="" disabled="">
                        </div>

                        <div class="form-group">
                            <label>Moyen de paiement <span class="text-danger">*</span></label>
                            <select name="moyen" id="moyen" class="form-control" required="">
                                <option selected="" disabled="" value="0">Moyen de paiement</option>
                                <option value="Espece">Espèce</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Virement">Virement</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Bordereau de paiement </label>
                            <input name="bordereau" id="bordereau" class="dropify form-control" type="file">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div id="blockbtn">
                            {{-- <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button> --}}
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Payer</button>
                        </div>
                        <div id="loader2" class="spinner-border text-primary m-1 dispayNone" style="float: right;" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
<script>
    $(document).ready(function () {
        $('.select2').select2();

        $('#userid').change(function() {
            $("#listusers").submit();
        });

    });

    function displayModalPaie($slug,mtt){
        $('#transid').val($slug);
        //Separation des milliers
        var mtt = mtt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        $('#montant').val(mtt);

    }

    function submitform(){
        var desc = $('#moyen').val();
        if(desc == 'undefined' || desc == null || desc == 0){
            swal("Oups!", "Veuillez renseignez la Moyen de paiement.");
            return false;
        }
        $('#blockbtn').hide();
        $('#loader2').show();
        document.getElementById('formPaiement').submit();

    }

</script>
@endsection
