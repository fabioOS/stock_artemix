@extends('daf.layout')
@section('page_title')
    Facturation des soustraitants
@stop

@section('content_title')
    Facturation des soustraitants
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Facturations et Paiements</li>
        <li class="breadcrumb-item active">Facturation des soustraitants</li>
    </ol>
@stop

@section('content')

<div>
    <a href="{{route('daf.facturation.paie.waiting')}}?type=soustraitant" class="btn btn-primary waves-effect waves-light mb-3"><i class="mdi mdi-plus me-1"></i> Effectuer un paiement</a>
</div>
    <div class="row">
        <div class="col-lg-9">

            @include('_stats_evolution_facturation')

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-0 ">Facture en attente de paiement</h4>
                </div>
                <div class="card-body table-responsive">
                    <table id="datatable" class="gstock table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                            <tr>
                                <th>Trans. ID</th>
                                <th>Sous-traitant</th>
                                <th>Contrat</th>
                                <th>Villa</th>
                                <th>Montant à payer</th>
                                {{-- <th>Taux d'avancement</th> --}}
                                <th>Date</th>
                                <th>Agent</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($paiements as $k=>$list)
                                @php
                                    $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                                    $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                                @endphp
                                <tr>
                                    <td class="text-body font-weight-bold"><a href="{{route('daf.paie.show',encrypt($list->id))}}">{{$list->slug}}</a></td>
                                    <td>{{$list->soustraitant->nom}}</td>
                                    <td><a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank">{{$list->contrat->slug}}</a></td>
                                    <td>{{$list->lot->lot}}</td>
                                    <td>@price($list->montant) <sup>FCFA</sup></td>
                                    {{-- <td>
                                        <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats !=0 ? round($list->taux/$corpsetats).'%' : '-'!!}</span>
                                    </td> --}}
                                    <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                    <td>{{$list->user->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <h5 class="bread_firsttext-uppercase mb-3">Dernières Transactions </h5>
            @if(count($last10)>0)
            @foreach ($last10 as $item)
                <div class="card">
                    <div class="card-body pb-0">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="text-muted">
                                    <h5 class="font-size-15 mb-2">{{$item->transaction->soustraitant ? $item->transaction->soustraitant->nom : ''}}
                                    <h6 class="text-muted">{{$item->transaction ? $item->transaction->slug : ''}} <br>
                                    <span class="small">{{$item->created_at->format("d/m/Y H:i")}} </span>
                                    </h6>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="text-success">
                                    <div class="mt">
                                        <p>@price($item->montant) FCFA</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <a href="{{route('daf.facturation.historique','soustraitant')}}" class="text-primary font-size-14 fw-medium">Voir toutes les transactions <i class="mdi mdi-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            @else
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h6 class="font-size-15 mb-2">Aucune transaction</h6>
                    </div>
                </div>
            </div>
            @endif

        </div>
    </div>



@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        $(document).ready(function(){

            $('.gstock').on('click', '.valid', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment valider cette transaction.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#btnaction').hide();
                        $('#loader').show();
                        window.location = href;
                    }
                });
            });

        });
    </script>
@endsection
