@extends('daf.layout')
@section('page_title')
    Fournisseurs
@stop

@section('content_title')
    Fournisseurs
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Gestion du stock</li>
        <li class="breadcrumb-item active">Fournisseurs</li>
    </ol>
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive mb-4 gstock">
            <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                <tr class="bg-transparent">
                    <th>N°</th>
                    <th>Nom</th>
                    <th>Contact</th>
                    <th>Adresse</th>
                    <th>Email</th>
                    <th>Delais de paiement</th>
                    <th>Regime fiscal</th>
                    <th>Ligne crédit</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($fournisseurs as $k=>$fournisseur)
                    <tr class="font-size-16">
                        <td>{{$k+1}}</td>
                        <td>{{$fournisseur->nom}}</td>
                        <td>{{$fournisseur->contact}}</td>
                        <td>{{$fournisseur->adresse}}</td>
                        <td>{{$fournisseur->email}}</td>
                        <td>{{$fournisseur->delais_paiement}}</td>
                        <td>{{$fournisseur->regime}}</td>
                        <td>{{$fournisseur->credit}}</td>
                        <td style="width: 150px;">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a href="{{route('stats.fournisseur',encrypt($fournisseur->id))}}" class="Programme px-2 " data-toggle="tooltip" data-placement="top" title="" data-original-title="Voir plus"><i class="fa fa-eye font-size-18"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- end table -->
    </div>
</div>



@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        $(document).ready(function(){

            $('.gstock').on('click', '.valid', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment valider cette transaction.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#btnaction').hide();
                        $('#loader').show();
                        window.location = href;
                    }
                });
            });

        });
    </script>
@endsection
