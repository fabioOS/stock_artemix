<style>
    .text-bleu{
        color: #234699
    }
</style>

<ul class="metismenu list-unstyled" id="side-menu">
    <li>
        <a href="{{route('daf.index')}}">
            <i class="uil-home-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li>

    @if(in_array(Auth::user()->role_id,[7,8]))
    <li class="menu-title mt-3 text-bleu">Affection CG</li>
    @php $nbaffect = (new \App\Http\Controllers\Daf\AffectationController())->getAffectionCG(); @endphp
    <li class="{{Route::is('daf.affectations') ? 'mm-active' : ''}}">
        <a href="{{route('daf.affectations')}}" class="{{Route::is('daf.affectations') ? 'active' : ''}}">
            <i class="uil-notes"></i>
            @if($nbaffect)
                <span class="badge badge-pill badge-warning float-right">{{$nbaffect}}</span>
            @endif

            <span>Affectations</span>
        </a>
    </li>
    @endif


    <li class="menu-title mt-3 text-bleu">Gestion du stock</li>

    <li>
        <a href="{{route('daf.fournisseur')}}">
            <i class="uil-user-circle"></i>
            <span>Fournisseurs</span>
        </a>
    </li>

    @if(in_array(Auth::user()->role_id,[7,8]))
    <li class="{{Route::is('daf.etats') || Route::is('daf.etats.search')|| Route::is('daf.etats.item') ? 'mm-active' : ''}}">
        <a href="{{route('daf.etats')}}" class="{{Route::is('daf.etats') || Route::is('daf.etats.search')|| Route::is('daf.etats.item') ? 'active' : ''}}">
            <i class="uil-home"></i>
            <span>Etats par villa</span>
        </a>
    </li>
    @endif

    @if(!in_array(Auth::user()->role_id,[9]))
    <li class="{{Route::is('daf.gproduits') || Route::is('daf.etats.pdt') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="{{Route::is('daf.gproduits') ? 'mm-active' : ''}} has-arrow waves-effect">
            <i class="uil-shop"></i>
            <span>Matières</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li>
                <a href="{{route('daf.gproduits')}}" class="">Liste des matières</a>
            </li>
        </ul>
    </li>

    <li class="{{Route::is('daf.stocks') ? 'mm-active' : ''}}">
        <a href="{{route('daf.stocks')}}" class="{{Route::is('daf.stocks') ? 'active' : ''}}">
            <i class="uil-notes"></i>
            <span>Stock</span>
        </a>
    </li>

    <li class="{{Route::is('daf.stocks.rotations') || Route::is('daf.stocks.rotations.store') ? 'mm-active' : ''}}">
        <a href="{{route('daf.stocks.rotations')}}" class="{{Route::is('daf.stocks.rotations') || Route::is('daf.stocks.rotations.store') ? 'active' : ''}}">
            <i class="uil-pause"></i>
            <span>Rotation</span>
        </a>
    </li>

    <li class="{{Route::is('daf.etats.pdt') ? 'mm-active' : ''}}">
        <a href="{{route('daf.etats.pdt')}}" class="{{Route::is('daf.etats.pdt') ? 'active' : ''}}">
            <i class="uil-dashboard"></i>
            <span>Etats</span>
        </a>
    </li>
    @endif

    <li class="menu-title mt-3 text-bleu">Gestion des commandes</li>
    <li>
        <a href="{{route('lignebudgetaire.index')}}">
            <i class="uil-chart"></i>
            <span>Ligne budgetaire</span>
        </a>
    </li>

    @if(Auth::user()->role_id==7)
        <li class="{{Route::is('daf.waiting')||Route::is('daf.valider')  ? 'mm-active' : ''}}">
            @php
                $waitingct = (new \App\Http\Controllers\Daf\BonCommandeController())->getByStatus(1);
            @endphp

            <a href="javascript: void(0);" class="{{$waitingct==0 ? 'has-arrow' : ''}} {{Route::is('daf.waiting')||Route::is('daf.valider') ? 'mm-active' : ''}} waves-effect">
                <i class="uil-file-alt"></i>
                <span>Bon de commande</span>
                @if($waitingct>0 )
                    <span class="badge badge-pill badge-primary float-right">{{$waitingct}}</span>
                @endif
            </a>
            <ul class="sub-menu" aria-expanded="false">
                <li>
                    <a href="{{route('daf.waiting')}}" class="">Bon en attente</a>
                </li>
                <li>
                    <a href="{{route('daf.valider')}}" class="">Bon validé</a>
                </li>
                {{-- <li>
                    <a href="{{route('daf.etats.bc')}}" class="">Etats</a>
                </li> --}}
            </ul>
        </li>
    @else
        <li class="{{Route::is('daf.waiting')||Route::is('daf.valider')  ? 'mm-active' : ''}}">
            <a href="javascript: void(0);" class="has-arrow {{Route::is('daf.waiting')||Route::is('daf.valider') ? 'mm-active' : ''}} waves-effect">
                <i class="uil-file-alt"></i>
                <span>Bon de commande</span>
            </a>
            <ul class="sub-menu" aria-expanded="false">
                <li>
                    <a href="{{route('daf.valider')}}" class="">Bon validé</a>
                </li>
                {{-- <li>
                    <a href="{{route('daf.etats.bc')}}" class="">Etats</a>
                </li> --}}
            </ul>
        </li>
    @endif

    <li class="{{Route::is('daf.etats.bc') ? 'mm-active' : ''}}">
        <a href="{{route('daf.etats.bc')}}" class="{{Route::is('daf.etats.bc') ? 'active' : ''}}">
            <i class="uil-dashboard"></i>
            <span>Etats</span>
        </a>
    </li>

    <li class="menu-title mt-3 text-bleu">Gestion des demandes</li>

    <li class="{{Route::is('daf.stat.contrats') || Route::is('daf.stat.contrats.show') ? 'mm-active' : ''}}">
        <a href="{{route('daf.stat.contrats')}}" class="{{Route::is('daf.stat.contrats.show') ? 'active' : ''}}">
            <i class="uil-sliders-v"></i>
            <span>Stats Contrats</span>
        </a>
    </li>

    @php
        //Daf
        $trnb = (new \App\Http\Controllers\Daf\HomeController())->getPaiementByStatus();
        $trnbpai = (new \App\Http\Controllers\Daf\HomeController())->getPaiementByStatus(3);

    @endphp
    <li class="{{Route::is('daf.paie') || Route::is('daf.paie.show') || Route::is('daf.paie.waiting') || Route::is('daf.paie.valid') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class=" waves-effect">
            <i class="uil-money-withdrawal"></i>
            @if($trnb)
                <span class="badge badge-pill badge-warning float-right">{{$trnb}}</span>
            @endif

            <span>Main d'oeuvre</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            @if(Auth::user()->role_id==8)
            <li><a href="{{route('daf.etbpaie')}}" class="{{Route::is('daf.etbpaie') ? 'active' : ''}}">Etablie un paiement</a></li>
            <li><a href="{{route('daf.paie')}}" class="{{Route::is('daf.paie') ? 'active' : ''}}">Attachements validés
                @if($trnb)
                    <span class="badge badge-pill badge-warning float-right">{{$trnb}}</span>
                @endif
            </a></li>
            @endif
            <li><a href="{{route('daf.paie.waiting')}}" class="{{Route::is('daf.paie.waiting') ? 'active' : ''}}">Paiements en attente
                @if($trnbpai)
                    <span class="badge badge-pill badge-warning float-right">{{$trnbpai}}</span>
                @endif
            </a></li>
            <li><a href="{{route('daf.paie.valid')}}" class="{{Route::is('daf.paie.valid') ? 'active' : ''}}">Paiements effectués </a></li>
        </ul>
    </li>

    @php
        //Daf
        $nbavnt = (new \App\Http\Controllers\Daf\AvenantsController())->getAvenantStatus();

    @endphp

    {{-- <li class="{{Route::is('daf.avenants') || Route::is('daf.avenants') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="{{Route::is('daf.avenants') ? 'mm-active' : ''}} waves-effect">
            <i class="uil-money-withdrawal"></i>
            @if($nbavnt)
                <span class="badge badge-pill badge-warning float-right">{{$nbavnt}}</span>
            @endif
            <span>Avenements</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li>
                <a href="{{route('daf.avenants.create')}}" class="">Ajouter un avenant</a>
            </li>
            <li>
                <a href="{{route('daf.avenants','waiting')}}" class="">Avenants en attente @if($nbavnt)
                    <span class="badge badge-pill badge-warning float-right">{{$nbavnt}}</span>
                @endif</a>
            </li>
            <li>
                <a href="{{route('daf.avenants','validate')}}" class="">Avenants validés</a>
            </li>
            <li>
                <a href="{{route('daf.avenants','rejected')}}" class="">Avenants Rejeté</a>
            </li>
        </ul>
    </li> --}}

    <li class="menu-title mt-3 text-bleu">Facturations et Paiements</li>

    @if(in_array(Auth::user()->role_id,[9]))
    <li class="{{Route::is('daf.facturation.soustraitant') || Route::is('daf.facturation.fournisseur') || Route::is('daf.facturation.paie.waiting')|| Route::is('daf.facturation.create.paie') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class=" waves-effect">
            <i class="fa fa-credit-card"></i>
            <span>Facturations</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('daf.facturation.soustraitant')}}" class="{{Route::is('daf.facturation.soustraitant') ? 'active' : ''}}">Soustraitant </a></li>
            <li><a href="{{route('daf.facturation.fournisseur')}}" class="{{Route::is('daf.facturation.fournisseur') ? 'active' : ''}}">Fournisseur </a> </li>
            <li><a href="{{route('daf.facturation.dmdSpec')}}" class="{{Route::is('daf.facturation.dmdSpec') ? 'active' : ''}}">Demandes spéciales </a> </li>
        </ul>
    </li>
    @endif

    <li class="{{Route::is('daf.facturation.historique') || Route::is('daf.facturation.createFactFourns') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class=" waves-effect">
            <i class="fa fa-history"></i>
            <span>Historique paiement</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('daf.facturation.historique','soustraitant')}}" class="">Soustraitant </a></li>
            <li><a href="{{route('daf.facturation.historique','fournisseur')}}" class="">Fournisseur </a> </li>
            <li><a href="{{route('daf.facturation.historique','demandespec')}}" class="">Demandes spéciales </a> </li>
        </ul>
    </li>

    <li class="{{Route::is('etats.facturations') ? 'mm-active' : ''}}">
        <a href="{{route('etats.facturations')}}" class="{{Route::is('etats.facturations') ? 'active' : ''}}">
            <i class="uil-dashboard"></i>
            <span>Etats</span>
        </a>
    </li>
</ul>
