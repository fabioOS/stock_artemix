@extends('daf.layout')

@section('page_title')
    Tableau de bord
@stop

@section('content_title')
    Tableau de bord
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Tableau de bord</li>
    </ol>
@stop

@section('content')
<div class="row">
    {{-- <div class="col-md-2 col-xl-2">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-cubes fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span data-plugin="counterup">{{$stock->sum('qte')}}</span></h2>
                    <p style="font-size: 20px;" class="text-white mb-0">Stock total</p>
                </div>
            </div>
        </div>
    </div> --}}

    {{-- <div class="col-md-2 col-xl-2">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-cart-plus fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span data-plugin="counterup">{{$entree->sum('qte')}}</span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Total des entrée</p>
                </div>
            </div>
        </div>
    </div> --}}

    {{-- <div class="col-md-2 col-xl-2">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-cart-arrow-down fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span data-plugin="counterup">{{$sortie->sum('qte')}}</span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Total des sorties</p>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="col-md-4 col-xl-4">
        <div class="card bg-primary2 text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-money-bill fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span>@price($entree->sum('price')) <sup>FCFA</sup></span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Coût du stock livré</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card bg-primary2 text-white">
            <div class="card-body">
                <div class="float-right mt-2">
                    <i class="fa fa-money-bill-wave fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span>@price($sortie) <sup>FCFA</sup></span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Coût du stock utilisé</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card bg-primary2 text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-money-bill fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span>@price($stock->sum('price')) <sup>FCFA</sup></span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Valeur de stock</p>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6 col-xl-3">
        <div class="card bg-primary2 text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-money-bill-wave fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span>@if($coutglobal == null) 0 @else @price($coutglobal) @endif <sup>FCFA</sup></span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Coût global de la main d'oeuvre</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-money-bill-wave fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span>@if($payes->coutpaye == null) 0 @else @price($payes->coutpaye) @endif <sup>FCFA</sup></span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Montant payé sur la main d'oeuvre</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card bg-primary2 text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-money-bill-wave fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span>@if($coutglobal-$payes->coutpaye == 0) {{$coutglobal-$payes->coutpaye}} @else @price($coutglobal-$payes->coutpaye) @endif <sup>FCFA</sup></span></h4>
                    <p style="font-size: 18px;" class="text-white mb-0">Montant exigible sur la main d'oeuvre</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="float-right mt-2">

                    <i class="fa fa-money-bill-wave fa-2x text-white"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1 text-white font-bold"><span>{{ $evol}}%</span></h4>
                    <p style="font-size: 20px;" class="text-white mb-0">Evolution des travaux</p>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12 col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Top 8 des matières les plus commandées</h4>

                @php $color = ['primary','primary2','success','warning','purple','dark','info','danger'];
                    $max = $top3cmd->max('price');
                @endphp

                <div class="row align-items-center no-gutters mt-3">
                    <div class="col-sm-6">
                        <p class="text-truncate mt-1 mb-0"><b>MATIERE</b> </p>
                    </div>

                    <div class="col-sm-6 text-right">
                        <p class="text-truncate mt-1 mb-0"><b>QUANTITES</b> </p>
                    </div>

                    {{-- <div class="col-sm-6">
                        <b>EVOLUTION</b>
                    </div> --}}
                </div>

                @foreach ($top3cmd as $k=>$item)
                    @php
                    $pcent = $max ? ($item->price *100) / $max  : 0;
                    @endphp
                    <div class="row align-items-center no-gutters mt-3">
                        <div class="col-sm-6">
                            <p class="text-truncate mt-1 mb-0"><i class="mdi mdi-circle-medium text-{{$color[$k]}} mr-2"></i> {{$item->produits->libelle}} </p>
                        </div>

                        <div class="col-sm-6 text-right">
                            <p class="text-truncate mt-1 mb-0">{{$item->total}} <sup class="badge badge-info">{{$item->produits->type->libelle}}</sup></p>
                        </div>

                        {{-- <div class="col-sm-6">
                            <div class="progress mt-1" style="height: 17px;">
                                <div class="progress-bar progress-bar bg-{{$color[$k]}}" role="progressbar"
                                    style="width: {{$pcent}}%;font-size:16px" aria-valuenow="{{$pcent}}" aria-valuemin="0"
                                    aria-valuemax="{{$pcent}}"> @price($item->price) Fcfa
                                </div>
                            </div>
                        </div> --}}
                    </div>
                @endforeach

            </div> <!-- end card-body-->
        </div>
    </div>

    {{-- <div class="col-md-4 col-xl-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Total des entrées par mois</h4>
                <div class="mt-3">
                    <div id="ttentreemois" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-xl-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Total des sorties par mois</h4>
                <div class="mt-3">
                    <div id="ttsortiemois" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div> --}}
</div>

<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Valeurs des entrées par mois</h4>
                <div class="mt-3">
                    <div id="vleurentree" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Valeurs des sorties par mois</h4>
                <div class="mt-3">
                    <div id="vleursortie" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Matières à approvisionner</h4>
                <div class="card-body table-responsive">
                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Code</th>
                            <th>Matière</th>
                            <th>Quantités disponible</th>
                            {{-- <th>Action</th> --}}
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($mtlimit as $k=>$item)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$item->produits->code}}</td>
                                <td>{{$item->produits->libelle}} <sup class="text-muted font-size-13">{{$item->produits->type ? $item->produits->type->libelle : ''}}</sup></td>
                                <td>{{$item->qte}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Paiement de la main d'oeuvre</h4>
                <div class="mt-3">
                    <div id="paiestraitant" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection


@section('other_js')
<script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

<script>
    // GRAF ENTREE PAR MOIS
    // ttentreemois = {
    //     chart: { height: 339, type: "line", stacked: !1, toolbar: { show: !1 } },
    //     stroke: { width: [0, 2, 4], curve: "smooth" },
    //     plotOptions: { bar: { columnWidth: "30%" } },
    //     colors: ["#5b73e8"],
    //     series: [
    //         {
    //             name: "Entrée",
    //             type: "column",
    //             data: [
    //                 @foreach($statsEntree as $key=>$stat)
    //                 {{$stat['qte']}},
    //                 @endforeach
    //                 ] },
    //     ],
    //     fill: { opacity: [0.85, 0.25, 1], gradient: { inverseColors: !1, shade: "light", type: "vertical", opacityFrom: 0.85, opacityTo: 0.55, stops: [0, 100, 100, 100] } },
    //     labels: ["01/01/{{date('Y')}}", "02/01/{{date('Y')}}", "03/01/{{date('Y')}}", "04/01/{{date('Y')}}", "05/01/{{date('Y')}}", "06/01/{{date('Y')}}", "07/01/{{date('Y')}}", "08/01/{{date('Y')}}", "09/01/{{date('Y')}}", "10/01/{{date('Y')}}", "11/01/{{date('Y')}}", "12/01/{{date('Y')}}"],
    //     markers: { size: 0 },
    //     xaxis: { type: "datetime" },
    //     yaxis: { title: { text: "Quantitées" } },
    //     tooltip: {
    //         shared: !0,
    //         intersect: !1,
    //         y: {
    //             formatter: function (e) {
    //                 return void 0 !== e ? e.toFixed(0) + " entrées" : e;
    //             },
    //         },
    //     },
    //     grid: { borderColor: "#f1f1f1" },
    // };
    // (chart = new ApexCharts(document.querySelector("#ttentreemois"), ttentreemois)).render();

    // GRAF SORTIE PAR MOIS
    // ttsortiemois = {
    //     chart: { height: 339, type: "line", stacked: !1, toolbar: { show: !1 } },
    //     stroke: { width: [0, 2, 4], curve: "smooth" },
    //     plotOptions: { bar: { columnWidth: "30%" } },
    //     colors: ["#5b73e8"],
    //     series: [
    //         {
    //             name: "Sortie",
    //             type: "column",
    //             data: [
    //                 @foreach($statsSortie as $key=>$stat)
    //                 {{$stat['qte']}},
    //                 @endforeach
    //                 ]
    //         },
    //     ],
    //     fill: {
    //         opacity: [0.85, 0.25, 1],
    //         gradient: { inverseColors: !1, shade: "light", type: "vertical", opacityFrom: 0.85, opacityTo: 0.55, stops: [0, 100, 100, 100] } },
    //         labels: ["01/01/{{date('Y')}}", "02/01/{{date('Y')}}", "03/01/{{date('Y')}}", "04/01/{{date('Y')}}", "05/01/{{date('Y')}}", "06/01/{{date('Y')}}", "07/01/{{date('Y')}}", "08/01/{{date('Y')}}", "09/01/{{date('Y')}}", "10/01/{{date('Y')}}", "11/01/{{date('Y')}}", "12/01/{{date('Y')}}"],
    //         markers: { size: 0 },
    //         xaxis: { type: "datetime" },
    //         yaxis: { title: { text: "Quantitées" } },
    //         tooltip: {
    //             shared: !0,
    //             intersect: !1,
    //             y: {
    //                 formatter: function (e) {
    //                     return void 0 !== e ? e.toFixed(0) + " quantités" : e;
    //                 },
    //             },
    //     },
    //     grid: { borderColor: "#f1f1f1" },
    // };
    // (chart = new ApexCharts(document.querySelector("#ttsortiemois"), ttsortiemois)).render();


    //VALEUR ENTREE
    vleurentree = {
        chart: { height: 339, type: "line", stacked: !1, toolbar: { show: !1 } },
        stroke: { width: [4,0], curve: "smooth" },
        plotOptions: { bar: { columnWidth: "30%" } },
        colors: ["#f1b44c"],
        series: [
            { name: "Montants", type: "line",
            data: [
                    @foreach($statsEntree as $key=>$stat)
                    {{$stat['price']}},
                    @endforeach
                    ]
        },
        ],
        fill: { opacity: [0.85, 0.25, 1], gradient: { inverseColors: !1, shade: "light", type: "vertical", opacityFrom: 0.85, opacityTo: 0.55, stops: [0, 100, 100, 100] } },
        labels: ["01/01/{{date('Y')}}", "02/01/{{date('Y')}}", "03/01/{{date('Y')}}", "04/01/{{date('Y')}}", "05/01/{{date('Y')}}", "06/01/{{date('Y')}}", "07/01/{{date('Y')}}", "08/01/{{date('Y')}}", "09/01/{{date('Y')}}", "10/01/{{date('Y')}}", "11/01/{{date('Y')}}", "12/01/{{date('Y')}}"],
        markers: { size: 0 },
        xaxis: { type: "datetime" },
        yaxis: { title: { text: "Montants" } },
        tooltip: {
            shared: !0,
            intersect: !1,
            y: {
                formatter: function (e) {
                    return void 0 !== e ? e.toFixed(0) + " F" : e;
                },
            },
        },
        grid: { borderColor: "#f1f1f1" },
    };
    (chart = new ApexCharts(document.querySelector("#vleurentree"), vleurentree)).render();


    // VALEUR SORTIE
    vleursortie = {
        chart: { height: 339, type: "line", stacked: !1, toolbar: { show: !1 } },
        stroke: { width: [4,0], curve: "smooth" },
        plotOptions: { bar: { columnWidth: "30%" } },
        colors: ["#f1b44c"],
        series: [
            { name: "Montants", type: "line",
            data: [
                    @foreach($statsSortie as $key=>$stat)
                    {{$stat['price']}},
                    @endforeach
                    ]
            },
        ],
        fill: { opacity: [0.85, 0.25, 1], gradient: { inverseColors: !1, shade: "light", type: "vertical", opacityFrom: 0.85, opacityTo: 0.55, stops: [0, 100, 100, 100] } },
        labels: ["01/01/{{date('Y')}}", "02/01/{{date('Y')}}", "03/01/{{date('Y')}}", "04/01/{{date('Y')}}", "05/01/{{date('Y')}}", "06/01/{{date('Y')}}", "07/01/{{date('Y')}}", "08/01/{{date('Y')}}", "09/01/{{date('Y')}}", "10/01/{{date('Y')}}", "11/01/{{date('Y')}}", "12/01/{{date('Y')}}"],
        markers: { size: 0 },
        xaxis: { type: "datetime" },
        yaxis: { title: { text: "Montants" } },
        tooltip: {
            shared: !0,
            intersect: !1,
            y: {
                formatter: function (e) {
                    return void 0 !== e ? e.toFixed(0) + " F" : e;
                },
            },
        },
        grid: { borderColor: "#f1f1f1" },
    };
    (chart = new ApexCharts(document.querySelector("#vleursortie"), vleursortie)).render();

    //PAIEMENT SOUSTRAITANT
    paiestraitant = {
        chart: { height: 339, type: "line", stacked: !1, toolbar: { show: !1 } },
        stroke: { width: [4,0], curve: "smooth" },
        plotOptions: { bar: { columnWidth: "30%" } },
        colors: ["#5b73e8"],
        series: [
            { name: "Montants", type: "line",
            data: [
                    @foreach($statsPaie as $key=>$stat)
                    {{$stat}},
                    @endforeach
                ]
            },
        ],
        fill: { opacity: [0.85, 0.25, 1], gradient: { inverseColors: !1, shade: "light", type: "vertical", opacityFrom: 0.85, opacityTo: 0.55, stops: [0, 100, 100, 100] } },
        labels: ["01/01/{{date('Y')}}", "02/01/{{date('Y')}}", "03/01/{{date('Y')}}", "04/01/{{date('Y')}}", "05/01/{{date('Y')}}", "06/01/{{date('Y')}}", "07/01/{{date('Y')}}", "08/01/{{date('Y')}}", "09/01/{{date('Y')}}", "10/01/{{date('Y')}}", "11/01/{{date('Y')}}", "12/01/{{date('Y')}}"],
        markers: { size: 0 },
        xaxis: { type: "datetime" },
        yaxis: { title: { text: "Montants" } },
        tooltip: {
            shared: !0,
            intersect: !1,
            y: {
                formatter: function (e) {
                    return void 0 !== e ? e.toFixed(0) + " F" : e;
                },
            },
        },
        grid: { borderColor: "#f1f1f1" },
    };
    (chart = new ApexCharts(document.querySelector("#paiestraitant"), paiestraitant)).render();
</script>


@endsection
