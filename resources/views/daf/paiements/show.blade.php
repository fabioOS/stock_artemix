@extends('daf.layout')

@section('page_title')
Détail de l'attachement
@stop

@section('content_title')
Détail de l'attachement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Main d'oeuvre</li>
        <li class="breadcrumb-item active">Détail de l'attachement</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT SOUS-TRAITANT</h4>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="text-muted">
                                <h5 class="font-size-15 mb-2">SOUS-TRAITANT</h5>
                                <p>{{$trans->soustraitant->nom}}</p>

                                <div>
                                    <h5 class="font-size-16 mb-2">CONTACT SOUS-TRAITANT</h5>
                                    <p>{{$trans->soustraitant->contact}}</p>
                                </div>

                                @if($trans->contrat)
                                <div>
                                    <h5 class="font-size-16 mb-2">CONTRACT</h5>
                                    <p> {{$trans->contrat->slug}} <br><a title="Voir le contrat" href="{{asset('assets/uploads/contrats/'.$trans->contrat->contrat)}}" target="_blank">{{$trans->contrat->libelle}}.pdf</a></p>
                                </div>
                                @endif

                                <div>
                                    <h5 class="font-size-16 mb-1">LOT/ILOT</h5>
                                    @php $typ = \App\ProgrammeActif::where('id',$trans->lot->actif_id)->first() @endphp
                                    <p>{{$trans->lot->lot}} / {{$trans->lot->ilot}} ({{$typ->libelle}})</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">AGENT</h5>
                                    <p>{{$trans->user->name}}</p>
                                </div>
                            </div>

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">DATE</h5>
                                    <p>{{$trans->created_at->format('d-m-Y H:i')}}</p>
                                </div>
                            </div>

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16 mb-1">ETAT</h5>
                                    <p>
                                        @if($trans->status==1 OR $trans->status==3)
                                            <span class="text-warning">En attente de paiement </span>
                                        @else
                                            <span class="text-success">Payé</span>
                                        @endif
                                    </p>
                                </div>
                            </div>

                            <div class="text-muted text-sm-right">
                                <div class="mt">
                                    <h5 class="font-size-16">ACTION</h5>
                                    @if(Auth::user()->role_id==8)
                                        @if($trans->status == 1)
                                        <div class="gstock" id="btnaction">
                                            <a href="{{route('daf.etbpaie')}}?code={{encrypt($trans->id)}}" class="btn btn-success valid" title="Etablir le paiement"> <i class="fa fa-check-circle"></i> Etablir le paiement</a>
                                            {{-- <button type="button" onclick='displayModalRejet("{{encrypt($trans->id)}}")' data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter la demande"> <i class="fa fa-trash"></i> </button> --}}
                                        </div>
                                        <div id="loader" class="spinner-border text-primary m-1 dispayNone" style="float: right;" role="status">
                                            <span class="sr-only">Chargement...</span>
                                        </div>
                                        @endif
                                    @else
                                        @if($trans->status == 3)
                                            {{-- <div class="gstock" id="btnaction">
                                                <button type="button" onclick='displayModalPaie(`{{encrypt($trans->id)}}`)' data-toggle="modal" data-target="#paietrans" class="btn btn-success" title="Effectuer le paiement"> <i class="fa fa-money-bill"></i> Effectuer le paiement</button>
                                            </div> --}}
                                            <span class="text-danger">Paiement en attente</span>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">ATTACHEMENT</h4>

                    <div class="py-2">
                        <div class="table-responsive">
                            <table class="table table-nowrap table-centered mb-0">
                                <thead>
                                <tr>
                                    <th style="width: 70px;">No.</th>
                                    <th>Corps d'etat</th>
                                    <th>Montant attachement</th>
                                    <th>Taux d'avancement</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listrans as $k=>$listran)
                                    <tr>
                                        <th scope="row">{{$k+1}}</th>
                                        <td>
                                            <h5 class="font-size-15 mb-1">{{$listran->corpsetat->libelle}}</h5>
                                        </td>
                                        <td>@price($listran->cout) Fcfa</td>
                                        <td>{{round($listran->taux_execute,2)}} %</td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <th scope="row" colspan="3" class="border-0 text-right">Total attachement</th>
                                    <td class="border-0"><h4 class="m-0">@price($trans->montant) Fcfa</h4></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        {{-- <div class="d-print-none mt-4">
                            <div class="float-right">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>

            @if($trans->status == 3)
                @if(count($trans->prelevement) > 0)
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3 text-primary">PRELEVEMENT</h4>

                        <div class="py-2">
                            <div class="table-responsive">
                                <table class="table table-nowrap table-centered mb-0">
                                    <thead>
                                    <tr>
                                        <th style="width: 70px;">No.</th>
                                        <th>Type de prélévement</th>
                                        <th>Montant du prélévement</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($trans->prelevement as $k=>$prelev)
                                        <tr>
                                            <th scope="row">{{$k+1}}</th>
                                            <td>
                                                <h5 class="font-size-15 mb-1">{{$prelev->typePrelevement ? $prelev->typePrelevement->libelle : ''}}</h5>
                                            </td>
                                            <td>@price($prelev->montant) Fcfa</td>
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <th scope="row" colspan="2" class="border-0 text-right">Total Prélévement</th>
                                        <td class="border-0"><h4 class="m-0">@price($trans->prelevement->sum('montant')) Fcfa</h4></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            {{-- <div class="d-print-none mt-4">
                                <div class="float-right">
                                    <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                @endif

                <div class="card border shadow-none">
                    <div class="card-header bg-transparent border-bottom py-3 px-4">
                        <h4 class="card-title mb-3 text-primary text-uppercase">Récapitulatif de l'attachement <span class="float-right">#{{$trans->slug}}</span></h4>
                    </div>
                    <div class="card-body p-4">

                        <div class="table-responsive">
                            <table class="table mb-0">
                                <tbody>
                                    <tr>
                                        <td>Montant attachement :</td>
                                        <td class="text-right">@price($trans->montant) Fcfa</td>
                                    </tr>
                                    <tr>
                                        <td>Montant total prélèvement : </td>
                                        <td class="text-right">@price($trans->prelevement->sum('montant')) Fcfa</td>
                                    </tr>

                                    <tr class="bg-light">
                                        <th>Montant à payer :</th>
                                        <td class="text-right">
                                            <span class="font-weight-bold">
                                                @price($trans->montant - $trans->prelevement->sum('montant')) FCFA
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- end table-responsive -->
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter la transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEditRejet" action="{{route('cphome.transac.reject')}}" method="post">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif du rejet <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="stid" id="stid" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    @if (1==2)
    <div id="paietrans" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Effectuer un paiement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formPaiement" action="{{route('daf.paie.validpaie')}}" method="post" enctype="multipart/form-data">@csrf
                    <input type="hidden" name="transid" id="transid" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Montant à payer <span class="text-danger">*</span></label>
                            <input name="montant" value="@price($trans->montant)" id="montant" min="1" type="text" class="form-control mt-2" readonly="" disabled="">
                        </div>

                        <div class="form-group">
                            <label>Moyen de paiement <span class="text-danger">*</span></label>
                            <select name="moyen" id="moyen" class="form-control" required="">
                                <option selected="" disabled="" value="0">Moyen de paiement</option>
                                <option value="Espece">Espèce</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Virement">Virement</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Bordereau de paiement </label>
                            <input name="bordereau" id="bordereau" class="dropify form-control" type="file">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div id="blockbtn">
                            {{-- <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button> --}}
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Payer</button>
                        </div>
                        <div id="loader2" class="spinner-border text-primary m-1 dispayNone" style="float: right;" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    @endif

@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
            $('#stid').val($slug);
        }

        function displayModalPaie($slug){
            $('#transid').val($slug);
        }

        function submitform(){
            // alert('ii');
            var desc = $('#moyen').val();
            //console.log(desc);
            if(desc == 'undefined' || desc == null || desc == 0){
                swal("Oups!", "Veuillez renseignez la Moyen de paiement.");
                return false;
            }
            $('#blockbtn').hide();
            $('#loader2').show();
            document.getElementById('formPaiement').submit();

        }

        // $(document).ready(function(){
        //     $('.gstock').on('click', '.valid', function (event) {
        //         event.preventDefault();
        //         var href = $(this).attr('href');
        //         swal({
        //             title: "Êtes-vous sûr?",
        //             text: "Voulez-vous vraiment mettre en paiement cet attachement.",
        //             icon: "warning",
        //             buttons: true,
        //             buttons: ["Annuler", "Oui"],
        //             dangerMode: true,
        //         }).then((willDelete) => {
        //             if (willDelete) {
        //                 $('#btnaction').hide();
        //                 $('#loader').show();
        //                 window.location = href;
        //             }
        //         });
        //     });
        // });


    </script>
@endsection
