@extends('daf.layout')
@section('page_title')
    Paiements effectués
@stop

@section('content_title')
    Paiements effectués
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item">Main d'oeuvre</li>
        <li class="breadcrumb-item active">Paiements effectués</li>
    </ol>
@stop

@section('content')
    {{-- @include('_stats_evolution') --}}

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table id="datatable-buttons" class="gstock table table-centered datatable table-card-list mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Trans. ID</th>
                            <th>Sous-traitant</th>
                            <th>Contrat</th>
                            <th>Villa</th>
                            <th>Montant payé</th>
                            <th>Taux d'avancement</th>
                            <th>Date</th>
                            <th>Agent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($paiements as $k=>$list)
                            @php
                                $typlot = \App\ProgrammeLot::where('id',$list->lot_id)->first();
                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$typlot->actif_id)->where('soustraitant_id',$list->soustraitant->id)->where('soustraitantcontrat_id',$list->soustraitantcontrat_id)->with('corpslots')->where('etatdel',1)->count();
                            @endphp
                            <tr>
                                <td class="text-body font-weight-bold">{{$list->slug}}</td>
                                <td>{{$list->soustraitant->nom}}</td>
                                <td><a href="{{asset('assets/uploads/contrats/'.$list->contrat->contrat)}}" target="_blank">{{$list->contrat->slug}}</a></td>
                                <td>{{$list->lot->lot}}</td>
                                <td>@price($list->montant) <sup>FCFA</sup></td>
                                <td>
                                    <span class="badge badge-pill badge-soft-primary font-size-12">{!! $corpsetats !=0 ? round($list->taux/$corpsetats).'%' : '-'!!}</span>
                                </td>
                                <td>{{$list->created_at->format("d/m/Y H:i")}}</td>
                                <td>{{$list->user->name}}</td>
                                <td>
                                    <span class="badge badge-pill badge-success">Payé</span></td>
                                <td>
                                    <a href="{{route('daf.paie.show',encrypt($list->id))}}" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-eye"></i> Détail</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('other_js')
    <!-- Chart JS -->
    <script src="{{asset('assets/libs/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/chartjs.init.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        function displayModalRejet($slug){
            //formEditRejet
        }

        $(document).ready(function(){
        });
    </script>
@endsection
