@extends('daf.layout')

@section('page_title')
    Gestions des matières
@stop

@section('content_title')
    Gestions des matières
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Gestions des matières</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Code</th>
                            <th>Matière</th>
                            <th>Prix</th>
                            <th>Quantités</th>
                            {{-- <th>Action</th> --}}
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($listproduits as $k=>$item)
                        @if(!$item->produit)
                            @continue
                        @endif
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$item->produit->code}}</td>
                                <td>{{$item->produit->libelle}} <sup class="badge badge-info">{{$item->produit->type ? $item->produit->type->libelle : ''}}</sup></td>
                                <td>@price($item->prix) Fcfa</td>
                                <td>{{$item->qte}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    {{-- <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script> --}}
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>


    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet utilisateur",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        $('#pdt').change(function() {
            var actiflibel = $("#pdt").find(':selected').data('type');
            $('#type').val(actiflibel);
        });

        $('#pdtedit').change(function() {
            var actiflibel = $("#pdtedit").find(':selected').data('type');
            $('#typedit').val(actiflibel);
        });


        function displayModalE(id,prix,pdtid,type){
            $("#formEdit")[0].reset();
            $("#idslug").val(id);
            $("#typedit").val(type);
            $("#prix").val(prix);
            var optionData ='';

            var data ={!! json_encode($produits) !!};
            for (var i = 0; i < data.length; i++){
                var select = '';
                if(data[i].id == pdtid){
                    select = 'selected';
                }
                optionData+='<option value="'+data[i].id+'" data-type="'+data[i].type.libelle+'" '+select+'>'+data[i].libelle +'</option>';
            }
            $('#pdtedit').html(optionData);

        }

    </script>
@endsection
