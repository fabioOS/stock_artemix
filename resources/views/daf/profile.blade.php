@extends('daf.layout')

@section('page_title')
    Mon profil
@stop

@section('content_title')
    Mon profil
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Mon profil</li>
    </ol>
@stop

@section('content')
    <div class="row mb-4">
        <div class="col-xl-4">
            <div class="card h-100">
                <div class="card-body">
                    <div class="text-center">
                        <div>
                            <img src="{{isset($user->img) ? asset('assets/uploads/users/'.$user->img) : asset('assets/user.jpeg')}}" alt="" class="avatar-lg rounded-circle img-thumbnail">
                        </div>
                        <h5 class="mt-3 mb-1">{{$user->name}}</h5>
                        <p class="text-muted">{{$user->role ? $user->role->libelle : '-'}}</p>
                    </div>

                    <hr class="my-4">

                    <div class="text-muted">
                        <div class="table-responsive mt-4">
                            <div>
                                <p class="mb-1">Nom :</p>
                                <h5 class="font-size-16">{{$user->name}}</h5>
                            </div>
                            <div class="mt-4">
                                <p class="mb-1">E-mail :</p>
                                <h5 class="font-size-16">{{$user->email}}</h5>
                            </div>
                            <div class="mt-4">
                                <p class="mb-1">Contact :</p>
                                <h5 class="font-size-16">{{$user->contact}}</h5>
                            </div>
                            <div class="mt-4">
                                <p class="mb-1">Programmes :</p>
                                @foreach($user->programmes as $program)
                                    <h5 class="font-size-16">{{$program->libelle}}</h5>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-8">
            <div class="card mb-0">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#about" role="tab">
                            <i class="uil uil-user-circle font-size-20"></i>
                            <span class="d-none d-sm-block">Modifier mon profil</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tasks" role="tab">
                            <i class="uil uil-lock font-size-20"></i>
                            <span class="d-none d-sm-block">Changer de mot de passe</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                            <i class="uil uil-image font-size-20"></i>
                            <span class="d-none d-sm-block">Mon avatar</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab content -->
                <div class="tab-content p-4">
                    <div class="tab-pane active" id="about" role="tabpanel">
                        <div class="mt-4">
                            <form action="{{route('daf.profil.update')}}" method="post">@csrf
                                <div class="form-group">
                                    <label for="formrow-firstname-input">Nom&Prénoms <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="name" value="{{$user->name}}" id="formrow-firstname-input">
                                </div>

                                <div class="form-group">
                                    <label>Contact </label>
                                    <input type="text" class="form-control" name="contact" value="{{$user->contact}}">
                                </div>

                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md">Enregistrer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="tasks" role="tabpanel">
                        <div class="mt-4">
                            <form action="{{route('daf.profil.update.pass')}}" method="post">@csrf
                                <div class="form-group">
                                    <label>Mot de passe actuel <span class="text-danger">*</span></label>
                                    <input type="password" name="motpass" required="required" class="form-control" placeholder="Mot de passe actuel">
                                </div>

                                <div class="form-group">
                                    <label>Nouveau mot de passe</label>
                                    <input type="password" name="newpass" required="required" class="form-control" placeholder="Nouveau mot de passe">
                                </div>

                                <div class="form-group">
                                    <label>Confirmer mot de passe</label>
                                    <input type="password" name="confirmpass" required="required" class="form-control" placeholder="Confirmer mot de passe">
                                </div>

                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md">Enregistrer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="messages" role="tabpanel">
                        <div class="mt-4">
                            <form action="{{route('daf.profil.update.avatar')}}" method="post" enctype="multipart/form-data">@csrf
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="file" id="fileUser" name="fileUser" class="dropify"/>
                                    </div>
                                </div>

                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md">Enregistrer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('other_js')
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>
@endsection
