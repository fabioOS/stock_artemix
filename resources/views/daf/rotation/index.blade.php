@extends('daf.layout')

@section('page_title')
    Rotation des produits
@stop

@section('content_title')
Rotation des produits
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Rotation des produits</li>
    </ol>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <h4 class="card-title">Filtrer</h4>
                <form class="form-inline" action="{{ route('daf.stocks.rotations.store') }}" method="POST">
                    @csrf
                    <div class="col-md-8">
                        <label class="sr-only" for="nbjrs">Nombre de jour</label>
                        <input type="number" min="1" {!! $day!=15 ? 'value="'.$day.'"' : '' !!} class="form-control mb-2 mr-sm-3 w-100" id="nbjrs" name="nbjrs" required placeholder="15 jours">
                    </div>

                    <div class="col-md-4">
                        <button type="submit" name="btn" value="trie" class="btn btn-success mb-2 mr-2"><i class="uil-search-alt"></i> Trier</button>
                        <button type="submit" name="btn" value="export" class="btn btn-primary mb-2"><i class="uil-export"></i> Exporter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <h4 class="card-title p-3">Matière non utilisée depuis <span class="text-danger">{{$day}}</span> jours</h4>
            <div class="card-body table-responsive">
                <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Matière</th>
                            <th title="Prix unitaire">P.U</th>
                            <th title="Quantité en stock">Q.Stock</th>
                            <th title="Montant en stock">M.Stock</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($rotations as $k=>$item)
                        <tr>
                            <td>{{$item->produits->code}}</td>
                            <td>{{$item->produits->libelle}} </td>
                            <td>@price($item->price) Fcfa</td>
                            <td>{{$item->qte}} <sup class="text-muted">{{$item->produits->type ? $item->produits->type->libelle : ''}}</sup></td>
                            <td><span class="badge badge-info">@price($item->qte * $item->price) F</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div>

@endsection


@section('other_js')

@endsection
