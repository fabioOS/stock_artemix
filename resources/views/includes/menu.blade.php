<ul class="metismenu list-unstyled" id="side-menu">

    <li class="menu-title">Administration</li>
    <li class="{{Route::is('home') ? 'mm-active' : ''}}">
        <a href="{{route('index')}}" class="{{Route::is('home') ? 'active' : ''}}">
            <i class="uil-home-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li>

    @if(Auth::user()->look == 0)
    <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-shop"></i>
            <span>Matières</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('gproduits.types')}}" class="{{Route::is('gproduits.types') ? 'active' : ''}}">Types</a></li>
            <li><a href="{{route('gproduits')}}" class="{{Route::is('gproduits') ? 'active' : ''}}">Matières</a></li>
        </ul>
    </li>

    <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-user"></i>
            <span>Utilisateurs</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('gusers.roles')}}" class="{{Route::is('gusers.roles') ? 'active' : ''}}">Roles</a></li>
            <li><a href="{{route('gusers')}}" class="{{Route::is('gusers') ? 'active' : ''}}">Utilisateurs</a></li>
        </ul>
    </li>
    @endif

    {{-- <li>
        <a href="{{route('gcorpetat')}}">
            <i class="far fa-window-maximize"></i>
            <span>Corps d'Etat</span>
        </a>
    </li> --}}

    <li>
        <a href="{{route('gprogramme')}}">
            <i class="uil-home-alt"></i>
            <span>Programmes</span>
        </a>
    </li>

    {{-- <li>
        <a href="{{route('importe')}}">
            <i class="uil-file-upload-alt"></i>
            <span>Importation</span>
        </a>
    </li> --}}

</ul>
