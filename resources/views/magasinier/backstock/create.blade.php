@extends('magasinier.layouts.app')

@section('page_title')
    Fait un retour
@stop

@section('content_title')
    Fait un retour <br>&nbsp; <br> Etape 1/2
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item active">Fait un retour</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form id="addformDC" class="repeater AddStocks" action="{{route('magasinier.stock.back.create_step2')}}" method="get">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3 text-primary">MOTIF </h4>
                        <div class="form-group">
                            <label for="">MOTIF DE RETOUR <span class="text-danger">*</span></label>
                            <textarea name="description" id="description" cols="30" rows="2" class="form-control" required></textarea>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3 text-primary">BON DE SORTIE</h4>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label id="bsid">Sélectionner le BS <span class="text-danger">*</span></label>
                                    <select name="bs_id" id="bsid" class="form-control select2" required>
                                        <option selected disabled>Sélectionner le BS</option>
                                        @foreach($bons as $item)
                                            <option value="{{$item->id}}" >{{$item->ref}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Suivant &nbsp; <i class="fas fa-angle-double-right"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- end row -->


@endsection




@section('other_js')
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        });

        // check last statut of dossier
        $('#order-selectinput').change(function(){
            $('#form-filtre').submit();
        });

        function submitForm() {
            event.preventDefault();
            if($('#description').val()==''){
                swal("Oups!", "Veuillez renseigner le champ description.");
            }else{
                //var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de faire de demande de commande ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        document.getElementById('addbtn').disabled = true;
                        $('#addbtn').hide();
                        $('#loader').show();
                        document.getElementById('addformDC').submit();
                    }
                });
            }

        }

    </script>

@endsection


@section('other_css')

@endsection
