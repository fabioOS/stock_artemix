@extends('magasinier.layouts.app')

@section('page_title')
    Liste des retour en stock
@stop

@section('content_title')
Liste des retour en stock
<a href="{{route('magasinier.stock.back.create')}}" class="btn btn-success btn-sm">Fait un retour</a>
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Liste des retour en stock</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($lists) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Fichier</th>
                            <th>N°Référence BS</th>
                            <th>Agent BS</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lists as $u=>$bc)
                            <tr class="font-size-16">
                                <td>{{$u+1}}</td>
                                <td><a href="!#" class="text-dark font-weight-bold">{{$bc->ref}}</a> </td>
                                <td><a target="_blank" href="{{asset('assets/uploads/bonretours/'.$bc->file)}}">bon_retour.pdf</a></td>
                                <td><a target="_blank" href="{{route('magasinier.livraison.details', encrypt($bc->demande->id))}}" class="">{{$bc->demande->ref}}</a></td>
                                <td>{{$bc->demande->user->name}}</td>
                                <td>
                                    @if ($bc->status == 1)
                                        <span class="badge badge-pill badge-warning">En attente</span>
                                    @elseif($bc->status == 2)
                                        <span class="badge badge-pill badge-success">Validé</span>
                                    @elseif($bc->status == 3)
                                        <span class="badge badge-pill badge-danger">Rejeté</span>
                                    @endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($bc->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="{{route('magasinier.stock.back.show', encrypt($bc->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucun retour en stock pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
