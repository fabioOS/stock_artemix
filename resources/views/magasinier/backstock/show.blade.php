@extends('magasinier.layouts.app')

@section('page_title')
    Bon de retour
@stop

@section('content_title')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">
                    Détails du bon de retour N° <span class="text-primary">{{strtoupper($bon->ref)}}</span>
                @if ($bon->status == 1)
                    <span class="badge badge-pill badge-warning">En attente</span>
                @endif
                @if ($bon->status == 2)
                    <span class="badge badge-pill badge-info">Validé</span>
                @endif
                @if ($bon->status == 4)
                    <span class="badge badge-pill badge-danger">Rejété</span>
                @endif
            </h4>
        </div>
    </div>
</div>
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item"><a href="{{route('magasinier.stock.back.index')}}"></a>Retour en stock </li>
        <li class="breadcrumb-item active">Détails du bon</li>
    </ol>
@stop

@section('content')

    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">N°Référence</h5>
                            <p>{{strtoupper($bon->ref)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Agent BS</h5>
                            <p>{{strtoupper($bon->demande->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">N°Réference BS</h5>
                            <p><a target="_blank" href="{{route('magasinier.livraison.details', encrypt($bon->demande->id))}}" >{{strtoupper($bon->demande->ref)}}</a></p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Fichier</h5>
                            @if(!empty($bon->file))
                            <p><a href="{{asset('assets/uploads/bonretours/'.$bon->file)}}" target="_blank" download>bon_de_retour.pdf</a></p>
                            @else
                            -
                            @endif
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de livraison</h5>
                            <p>{{Illuminate\Support\Carbon::parse($bon->demande->updated_at)->format('d/m/Y H:i')}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de retour</h5>
                            <p>{{Illuminate\Support\Carbon::parse($bon->created_at)->format('d/m/Y H:i')}}</p>
                        </div>

                        {{-- <div class="col-lg-2 row">
                            @if($bon->status == 1)
                                <div class="row gstock" id="btnaction">
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-success valid" title="Valider le retour de stock"> <i class="fa fa-check-circle"></i></a>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter le retour de stock"> <i class="fa fa-trash"></i></button>
                                    </div>
                                    <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                                        <span class="sr-only">Chargement...</span>
                                    </div>
                                </div>
                            @endif

                        </div> --}}
                    </div>
                </div>
            </div>
        </div>


        @if($bon->status == 4)
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="font-size-16">Motif</h5>
                                <p>{{$bon->motif}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        <div class="col-xl-3">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Résumé par matière retourner</h5>
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            @php
                                $distinct_matiers = \App\Bon_retour_item::where('bon_retour_id', $bon->id)
                                ->join('produits', 'produits.id', '=', 'bon_retour_items.produit_id')
                                ->selectRaw('DISTINCT(produits.id) AS id')
                                ->pluck('id');
                            @endphp
                            <tbody>
                                @foreach ($distinct_matiers as $matiere_id)
                                    <tr>
                                        @php
                                            $unite = mb_strtoupper(\App\Produit::where('produits.id', $matiere_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                        @endphp
                                        <td class="font-size-12"><b>{{\App\Produit::where('id',$matiere_id)->pluck('libelle')->first()}}</b></td>
                                        <td class="text-right">
                                            @php
                                                $mat_qte_total = \App\Bon_retour_item::where('bon_retour_id', $bon->id)->where('produit_id', $matiere_id)->selectRaw('SUM(qte) AS qte')->pluck('qte')->first();
                                            @endphp
                                            {{$mat_qte_total ? $mat_qte_total : '0'}} <sup><small>{{$unite}}</small></sup>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            <div class="card mb-0">
               <div class="card-body">
                <h4 class="card-title mb-5">
                    Résumé du bon de retour par villa
                </h4>
                @php
                    $row_lot_id = \App\Bon_retour_item::where('bon_retour_id', $bon->id)->selectRaw('DISTINCT(lot_id) AS lot_id')->pluck('lot_id');
                    $programme_id = Session::get('program');
                    $vars = [
                        'programme_id' => $bon->programme_id,
                        'lot_id' => $row_lot_id,
                    ];

                    $lots = \App\ProgrammeLot::whereIn('id',$row_lot_id)->with('actifs')->get();

                @endphp
                <div class="row">
                    <div class="col-md-3">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            @foreach($lots as $k=>$item)
                                <a class="nav-link mb-2 border {{$k==0 ? 'active' :''}}" id="v-pills-{{$k}}-tab" data-toggle="pill" href="#v-pills-{{$k}}" role="tab" aria-controls="v-pills-{{$k}}" aria-selected="{{$k==0 ? 'true' : 'false'}}">
                                    <i class="uil uil-home font-size-20"></i>
                                    Villa {{$item->lot}}<br><span class="font-size-12">{{$item->actifs ?$item->actifs->libelle:''}}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-9">

                    <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabCentent">
                        @foreach($lots as $k=>$item)
                            @php
                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$item->actif_id)->where('soustraitant_id',$bon->demande->soustraitant_id)->with('corpslots')->get();
                            @endphp
                            <div class="tab-pane fade {{$k==0 ? 'active show' :''}}" id="v-pills-{{$k}}" role="tabpanel" aria-labelledby="v-pills-{{$k}}-tab">
                                <input type="hidden" value="{{$item->id}}" name="lot_id[]">
                                @if (count($corpsetats)>0)
                                    @foreach($corpsetats as $j=>$corps)
                                        <div class="card border border-primary">
                                            <a href="#main_phase{{$corps->id}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$corps->id}}">
                                                <div class="p-4">
                                                    <div class="media align-items-center">
                                                        <div class="mr-3">
                                                            <div class="avatar-xs">
                                                                <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                                            </div>
                                                        </div>
                                                        <div class="media-body  overflow-hidden">
                                                            <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                        </div>
                                                        <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                                    </div>
                                                </div>
                                            </a>
                                            <div id="main_phase{{$corps->id}}" class="collapse show" data-parent="#main_phase{{$corps->id}}">
                                                <div class="p-4 border-top">
                                                    <!-- item sous phase  -->
                                                    @php
                                                        $libelcorpsetatids = \App\ProgrammeSouscorpsetatLot::where([['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['lot_id',$item->id]])->orderBy('id','asc')->pluck('corpsetatlibel_id')->toArray();
                                                        $souscorpetat = \App\ProgrammeCorpsetatLibelle::whereIn('id',$libelcorpsetatids)->orderBy('id','asc')->get();
                                                    @endphp
                                                    @foreach($souscorpetat as $i=>$sscorp)
                                                        <div class="sub_sub_2 border-top d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                                            <div class="tilte mr-auto p-2">{{$sscorp->libelle}}</div>
                                                        </div>

                                                        {{-- les LIBELLES des scorps d'etat --}}
                                                        @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',$sscorp->id]])->with('matieres')->get();
                                                        @endphp

                                                        @foreach ($scorpsmtt as $scorps)
                                                            @php
                                                                $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                $qte_retour = \App\Bon_retour_item::where('bon_retour_id', $bon->id)
                                                                    ->where('produit_id', $scorps->produit_id)->where('lot_id', $item->id)
                                                                    ->where('corpsetats_id', $corps->corpsetats_id)->where('scorpsetats_id', $scorps->id)
                                                                    ->pluck('qte')->first();
                                                                $qte_livree = \App\Bon_demande_item::where('bon_demande_id', $bon->demande->id)
                                                                    ->where('produit_id', $scorps->produit_id)->where('lot_id', $item->id)
                                                                    ->where('corpsetats_id', $corps->corpsetats_id)->where('scorpsetats_id', $scorps->id)
                                                                    ->pluck('qte')->first();
                                                            @endphp

                                                            @if(isset($qte_retour) and $qte_retour != 0)
                                                                <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                    <div class="tilte mr-auto p-2 text-body">
                                                                        {{$scorps->matieres->libelle}}
                                                                        <div class="form-label mr-auto p-2 text-warning">
                                                                            <span class="text-default font-size-13">
                                                                                Quantité livrée : <b> {{$qte_livree}}</b> <sup>{{$unite}}</sup> <br>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="somme p-2">
                                                                        <span class="text-body">Qté retournée:</span> {{$qte_retour}}
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    @endforeach

                                                </div><!-- fin border-top -->
                                                <div class="p-2 border-top">
                                                    {{-- Ceux qui nont pas de LIBELLE --}}
                                                    @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                                                    @foreach ($scorpsmtt as $scorps)
                                                        @php
                                                            $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                            $qte_retour = \App\Bon_retour_item::where('bon_retour_id', $bon->id)
                                                                ->where('produit_id', $scorps->produit_id)->where('lot_id', $item->id)
                                                                ->where('corpsetats_id', $corps->corpsetats_id)->where('scorpsetats_id', $scorps->id)
                                                                ->pluck('qte')->first();
                                                            $qte_livree = \App\Bon_demande_item::where('bon_demande_id', $bon->demande->id)
                                                                ->where('produit_id', $scorps->produit_id)->where('lot_id', $item->id)
                                                                ->where('corpsetats_id', $corps->corpsetats_id)->where('scorpsetats_id', $scorps->id)
                                                                ->pluck('qte')->first();
                                                        @endphp

                                                        @if(isset($qte_retour) and $qte_retour != 0)
                                                            <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                <div class="tilte mr-auto p-2 text-body">
                                                                    {{$scorps->matieres->libelle}}
                                                                    <div class="form-label mr-auto p-2 text-warning">
                                                                        <span class="text-default font-size-13">
                                                                            Quantité livrée: <b> {{$qte_livree}}</b> <sup>{{$unite}}</sup> <br>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="somme p-2">
                                                                        <span class="text-body">Qté retournée : </span> {{$qte_retour}} <br>&nbsp;<br>&nbsp;
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div> <!-- fin main_phase -->
                                        </div><!-- card -->

                                        {{-- @endisset --}}

                                    @endforeach
                                @endif
                            </div>
                            {{-- @endif --}}
                        @endforeach
                    </div>



                    </div>

                </div>
               </div>
            </div>
        </div>
    </div>
    <!-- end row -->


    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter le bon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEdit" action="{{route('magasinier.demande.reject')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif du rejet <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="bonid" value="{{$bon->ref}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="loader2" class="spinner-border text-primary m-1 dispayNone" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                        <div id="blockbtn">
                            <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                        </div>
                </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.valid', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment livrer cet bon ?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#btnaction').hide();
                    $('#loader').show();
                    window.location = href;
                }
            });
        });

    });

    function submitform(){
        var desc = $('#motif').val();
        if(desc == ''){
            swal("Oups!", "Veuillez renseignez la motif.");
            return false;
        }
        $('#blockbtn').hide();
        $('#loader2').show();
        document.getElementById('formEdit').submit();

    }
</script>
@endsection
