@extends('magasinier.layouts.app')

@section('page_title')
    Bon de commande validé
@stop

@section('content_title')
    Bon de commande validé
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de commande validé</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($bcs) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>BC N°</th>
                            <th>Agent</th>
                            <th>Bon demande</th>
                            <th>Fournisseur</th>
                            <th>Montant total</th>
                            <th>Status</th>
                            <th title="Qantités commandés / quantités livrés">CMD / LIV</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bcs as $bc)
                            <tr class="font-size-16">
                                <td><a href="!#" class="text-dark font-weight-bold">{{$bc->ref}}</a> </td>
                                <td>{{$bc->user->name}}</td>
                                <td><a target="_blank" href="{{route('mcommande.show', encrypt($bc->demande->id))}}" class="">{{$bc->demande->ref}}</a></td>
                                <td>{{$bc->fournisseur->nom}}</td>
                                <td>@price($bc->montant - $bc->retenu??0) Fcfa</td>
                                <td>
                                    <span class="badge badge-pill badge-success">Validé</span>
                                </td>
                                <td>
                                    @php $cmder = $bc->items->sum('qte'); @endphp
                                    @if($cmder<=$bc->qtelivree)
                                    <span class="text-success">{{$cmder}} / {{$bc->qtelivree}}</span>
                                    @else
                                    <span class="text-danger">{{$cmder}} / {{$bc->qtelivree}}</span>
                                    @endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($bc->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="{{route('mbc.show', encrypt($bc->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucun bon de commande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
