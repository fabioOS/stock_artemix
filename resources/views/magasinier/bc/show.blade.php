@extends('magasinier.layouts.app')

@section('page_title')
    Détails du bon de commande
@stop

@section('content_title')
Bon de commande N°  <span class="text-body">{{strtoupper($demande->ref)}}</span>
    @if ($demande->status == 1)
        <span class="badge badge-pill badge-warning">En attente</span>
    @endif
    @if ($demande->status == 2)
        <span class="badge badge-pill badge-info">Validé</span>
    @endif
    @if ($demande->status == 3)
        <span class="badge badge-pill badge-success">Livré</span>
    @endif
    @if ($demande->status == 4)
        <span class="badge badge-pill badge-danger">Rejété</span>
    @endif
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails du bon de commande</li>
    </ol>
@stop

@section('content')

    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">BC N°</h5>
                            <p>{{strtoupper($demande->ref)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Agent</h5>
                            <p>{{strtoupper($demande->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Bon de demande</h5>
                            <p><a target="_blank" href="{{route('mcommande.show', encrypt($demande->demande->id))}}">{{$demande->demande->ref}}</a></p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Fournisseur</h5>
                            <p>{{$demande->fournisseur->nom}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Montant total</h5>
                            <p>@price($demande->montant - $demande->retenu??0) Fcfa</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de demande</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->created_at)->format('d/m/Y')}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de paiement</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->datepaiement)->format('d/m/Y')}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Mode paiement</h5>
                            <p>{{$demande->mode}}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Matières commandées </h5>
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Num</td>
                                    <td>Matière</td>
                                    <td>Unité</td>
                                    <td>Quantité commandée</td>
                                    <td class="text-right">Quantité livrée</td>
                                </tr>
                                @foreach ($demande->items as $i=>$item)
                                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                                    {{-- @dump((($item->remise /100) * $item->price)) --}}
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td><b>{{$pdt->libelle}}</b></td>
                                        <td>{{$pdt->type->libelle}}</td>
                                        <td>{{$item->qte}} </td>
                                        @php
                                            $nb =\App\StockFournisseurItem::where('bc_item_id',$item->id)->where('produit_id',$item->produit_id)
                                                //->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                                                //->where('bon_commandes.status',2)
                                                ->sum('qte')
                                        @endphp
                                        <td class="text-right"><span class="{{$nb>=$item->qte ? 'text-success' : 'text-warning'}}">{{$nb}}</span></td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

@endsection


@section('other_js')

@endsection
