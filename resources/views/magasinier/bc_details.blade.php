@extends('magasinier.layouts.app')

@section('page_title')
    Bon de sortie
@stop

@section('content_title')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">
                @if ($data->status == 3)
                    Détails du bon de sortie N° <span class="text-primary">{{strtoupper($data->ref)}}</span>
                @else
                    Détails du bon de sortie N°  <span class="text-primary">{{strtoupper($data->ref)}}</span>
                @endif
                @if ($data->status == 1)
                    <span class="badge badge-pill badge-warning">En attente</span>
                @endif
                @if ($data->status == 2)
                    <span class="badge badge-pill badge-info">Validé</span>
                @endif
                @if ($data->status == 3)
                    <span class="badge badge-pill badge-success">Livré</span>
                @endif
                @if ($data->status == 4)
                    <span class="badge badge-pill badge-danger">Rejété</span>
                @endif
            </h4>
        </div>
    </div>
</div>
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails bon de demande</li>
    </ol>
@stop

@section('content')

    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Référence</h5>
                            <p>{{strtoupper($data->ref)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Chef chantier</h5>
                            <p>{{strtoupper($data->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Sous-traitant</h5>
                            <p>{{strtoupper($data->soustraitant->nom)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Fichier</h5>
                            @if(!empty($data->file))
                            <p><a href="{{asset('assets/uploads/bondemandes/'.$data->file)}}" target="_blank" download>bon_de_demande.pdf</a></p>
                            @else
                            -
                            @endif
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de demande</h5>
                            <p>{{Illuminate\Support\Carbon::parse($data->created_at)->format('d/m/Y H:i')}}</p>
                        </div>

                        @if($data->status==3)
                        <div class="text-muted col-lg-2">
                            <a href="{{route('pdfbsvalider',$data->ref)}}" title="Télécharger le bon" class="btn btn-success btn-sm waves-effect waves-light">
                                <i class="bx bxs-file-pdf"></i>
                            </a>
                        </div>
                        @endif

                        <div class="col-lg-12">
                            @if($data->status == 2)
                                @if (Auth::user()->role_id == 4) <!-- SI Magasinier -->
                                    <div class="row gstock" id="btnaction" style="float: right;">
                                        <div class="">
                                            <a href="{{route('magasinier.demande.delivering',encrypt($data->id))}}" class="btn btn-success valid"> <i class="fa fa-check-circle"></i> Livrer le bon</a>
                                        </div>
                                        <div class="ml-1">
                                            <button type="button" data-toggle="modal" data-target="#rejectbon" class="btn btn-danger"> <i class="fa fa-trash"></i> Rejeter le bon</button>
                                        </div>
                                    </div>
                                    <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                                        <span class="sr-only">Chargement...</span>
                                    </div>
                                @endif
                            @endif
                            @if($data->status == 3)
                                @if (Auth::user()->role_id == 4) <!-- SI Magasinier -->
                                    <div class="row">
                                        <div class="">
                                            <a style="display: none;" href="{{route('magasinier.livraison.export',encrypt($data->id))}}" class="btn btn-secondary"> <i class="fa fa-download"></i> Télécharger le bon</a>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @if($data->status == 4)
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="font-size-16">Motif</h5>
                                <p>{{$data->motif}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        <div class="col-xl-3">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Résumé par matière </h5>
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            @php
                                $distinct_matiers = \App\Bon_demande_item::where('bon_demande_id', $data->id)
                                ->join('produits', 'produits.id', '=', 'bon_demande_items.produit_id')
                                ->selectRaw('DISTINCT(produits.id) AS id')
                                ->pluck('id');
                            @endphp
                            <tbody>
                                @foreach ($distinct_matiers as $matiere_id)
                                    <tr>
                                        @php
                                            $unite = mb_strtoupper(\App\Produit::where('produits.id', $matiere_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                        @endphp
                                        <td class="font-size-12"><b>{{\App\Produit::where('id',$matiere_id)->pluck('libelle')->first()}}</b></td>
                                        <td class="text-right">
                                            @php
                                                $mat_qte_total = \App\Bon_demande_item::where('bon_demande_id', $data->id)->where('produit_id', $matiere_id)->selectRaw('SUM(qte) AS qte')->pluck('qte')->first();
                                            @endphp
                                            {{$mat_qte_total ? $mat_qte_total : '0'}} <sup><small>{{$unite}}</small></sup>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>

        <div class="col-xl-9">
            <div class="card mb-0">
               <div class="card-body">
                <h4 class="card-title mb-5">
                    @if ($data->status == 3)
                    Résumé du bon de livraison par villa
                    @else
                    Résumé du bon de demande par villa
                    @endif
                </h4>
                @php
                    $row_lot_id = \App\Bon_demande_item::where('bon_demande_id', $data->id)->selectRaw('DISTINCT(lot_id) AS lot_id')->pluck('lot_id');
                    $programme_id = Session::get('program');
                    $vars = [
                        'programme_id' => $data->programme_id,
                        'lot_id' => $row_lot_id,
                        'soustraitant_id' => $data->soustraitant_id
                    ];

                    $lots = \App\ProgrammeLot::whereIn('id',$row_lot_id)->with('actifs')->get();

                @endphp
                <div class="row">
                    <div class="col-md-3">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            @foreach($lots as $k=>$item)
                                <a class="nav-link mb-2 border {{$k==0 ? 'active' :''}}" id="v-pills-{{$k}}-tab" data-toggle="pill" href="#v-pills-{{$k}}" role="tab" aria-controls="v-pills-{{$k}}" aria-selected="{{$k==0 ? 'true' : 'false'}}">
                                    <i class="uil uil-home font-size-20"></i>
                                    Villa {{$item->lot}}<br><span class="font-size-12">{{$item->actifs ?$item->actifs->libelle:''}}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-9">

                    <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabCentent">
                        @foreach($lots as $k=>$item)
                            @php
                                $corpsetats = \App\LotSoustraitantCorpsetat::where('actif_id',$item->actif_id)->where('soustraitant_id',$data->soustraitant_id)->where('soustraitantcontrat_id',$data->contrat_id)->with('corpslots')->get();
                            @endphp
                                <div class="tab-pane fade {{$k==0 ? 'active show' :''}}" id="v-pills-{{$k}}" role="tabpanel" aria-labelledby="v-pills-{{$k}}-tab">
                                    <input type="hidden" value="{{$item->id}}" name="lot_id[]">
                                    @if (count($corpsetats)>0)
                                        @foreach($corpsetats as $j=>$corps)
                                             {{-- @php
                                                $checkitem = \App\Bon_demande::where('programme_id', Session::get('program'))
                                                ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                ->where('bon_demande_items.lot_id', $item->id)
                                                ->where('bon_demande_items.corpsetats_id', $corps->id)
                                                ->first();
                                                dump($checkitem,$item->id,$corps->id,$vars['soustraitant_id']) ;
                                            @endphp --}}
                                            {{-- @isset($checkitem) --}}

                                            <input type="hidden" name="corps_id[{{$item->id}}][]" value="{{$corps->id}}">

                                            <div class="card border border-primary">
                                                <a href="#main_phase{{$corps->id}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="main_phase{{$corps->id}}">
                                                    <div class="p-4">
                                                        <div class="media align-items-center">
                                                            <div class="mr-3">
                                                                <div class="avatar-xs">
                                                                    <div class="avatar-title rounded-circle bg-soft-primary text-primary">{{$j+1}}</div>
                                                                </div>
                                                            </div>
                                                            <div class="media-body  overflow-hidden">
                                                                <h5 class="font-size-16 mb-1">{{$corps->corpslots->libelle}}</h5>
                                                            </div>
                                                            <i class="mdi mdi-chevron-up accor-down-icon font-size-24"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div id="main_phase{{$corps->id}}" class="collapse show" data-parent="#main_phase{{$corps->id}}">
                                                    <div class="p-4 border-top">
                                                        <!-- item sous phase  -->
                                                        @php
                                                            $libelcorpsetatids = \App\ProgrammeSouscorpsetatLot::where([['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['lot_id',$item->id]])->orderBy('id','asc')->pluck('corpsetatlibel_id')->toArray();
                                                            $souscorpetat = \App\ProgrammeCorpsetatLibelle::whereIn('id',$libelcorpsetatids)->orderBy('id','asc')->get();
                                                        @endphp
                                                        @foreach($souscorpetat as $i=>$sscorp)
                                                            <div class="sub_sub_2 border-top d-flex align-items-center" style="background-color: #a8a8a8;color: #fff;">
                                                                <div class="tilte mr-auto p-2">{{$sscorp->libelle}}</div>
                                                            </div>

                                                            {{-- les LIBELLES des scorps d'etat --}}
                                                            @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',$sscorp->id]])->with('matieres')->get(); @endphp
                                                            @foreach ($scorpsmtt as $scorps)
                                                                @php
                                                                    $qte_demande = \App\Bon_demande::where('bon_demandes.id', $data->id)
                                                                    ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                    ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                    ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                    ->where('bon_demande_items.lot_id', $item->id)
                                                                    ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                    ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                    ->pluck('qte')
                                                                    ->first();
                                                                @endphp

                                                                @if(isset($qte_demande) and $qte_demande != 0)
                                                                    <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                        <input type="hidden" name="sous_corps_id[{{$item->id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->id}}">
                                                                        @php
                                                                            $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                            $total_livre = \App\Bon_demande::where('status', 3)
                                                                            ->where('programme_id', Session::get('program'))
                                                                            // ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                            ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                            ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                            ->where('bon_demande_items.lot_id', $item->id)
                                                                            ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                            ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                            ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                                            ->pluck('total_livre')
                                                                            ->first();
                                                                            $restant = $scorps->qte - $total_livre;
                                                                            $stock_dispo = \App\Stock::where('produit_id',$scorps->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                                                                        @endphp

                                                                        <div class="tilte mr-auto p-2 text-body">
                                                                            {{$scorps->matieres->libelle}}
                                                                            <input type="hidden" name="produit_id[{{$item->id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->matieres->id}}">
                                                                            <div class="form-label mr-auto p-2 text-warning">
                                                                                <span class="text-default font-size-13">
                                                                                    Stock déjà livré: <b> {{$total_livre != '' ? $total_livre : 0}}</b> / {{$scorps->qte}} <sup>{{$unite}}</sup> <br>
                                                                                    Stock actuel du programme: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} <sup>{{$unite}}</sup></b>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="somme p-2">
                                                                            <span class="text-body">Qté demandée:</span> {{$qte_demande}}
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        @endforeach

                                                    </div><!-- fin border-top -->

                                                    <div class="p-2 border-top">
                                                        {{-- Ceux qui nont pas de LIBELLE --}}
                                                        @php $scorpsmtt = \App\ProgrammeSouscorpsetatLot::where([['lot_id',$item->id],['corpsetat_id',$corps->corpsetats_id],['actif_id',$item->actif_id],['corpsetatlibel_id',null]])->with('matieres')->get(); @endphp
                                                        @foreach ($scorpsmtt as $scorps)
                                                            @php
                                                                $qte_demande = \App\Bon_demande::where('bon_demandes.id', $data->id)
                                                                ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                ->where('bon_demande_items.lot_id', $item->id)
                                                                ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                ->pluck('qte')
                                                                ->first();
                                                            @endphp

                                                            @if(isset($qte_demande) and $qte_demande != 0)
                                                                <div class="sub_sub_2 border-top d-flex align-items-center">
                                                                    <input type="hidden" name="sous_corps_id[{{$item->id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->id}}">
                                                                    @php
                                                                        $unite = mb_strtoupper(\App\Produit::where('produits.id', $scorps->produit_id)->join('typeproduits', 'typeproduits.id', '=', 'produits.type_id')->pluck('typeproduits.libelle')->first());
                                                                        $total_livre = \App\Bon_demande::where('status', 3)
                                                                                        ->where('programme_id', Session::get('program'))
                                                                                        // ->where('bon_demandes.soustraitant_id', $vars['soustraitant_id'])
                                                                                        ->join('bon_demande_items', 'bon_demande_items.bon_demande_id', '=', 'bon_demandes.id')
                                                                                        ->where('bon_demande_items.produit_id', $scorps->produit_id)
                                                                                        ->where('bon_demande_items.lot_id', $item->id)
                                                                                        ->where('bon_demande_items.corpsetats_id', $corps->corpsetats_id)
                                                                                        ->where('bon_demande_items.scorpsetats_id', $scorps->id)
                                                                                        ->selectRaw('SUM(bon_demande_items.qte) AS total_livre')
                                                                                        ->pluck('total_livre')
                                                                                        ->first();

                                                                        $restant = $scorps->qte - $total_livre;
                                                                        $stock_dispo = \App\Stock::where('produit_id',$scorps->produit_id)->where('programme_id',Session::get('program'))->pluck('qte')->first();
                                                                    @endphp

                                                                    <div class="tilte mr-auto p-2 text-body">
                                                                        {{$scorps->matieres->libelle}}
                                                                        <input type="hidden" name="produit_id[{{$item->id}}][{{$corps->corpsetats_id}}][]" value="{{$scorps->matieres->id}}">
                                                                        <div class="form-label mr-auto p-2 text-warning">
                                                                            <span class="text-default font-size-13">
                                                                                Stock déjà livré: <b> {{$total_livre != '' ? $total_livre : 0}}</b> / {{$scorps->qte}} <sup>{{$unite}}</sup> <br>
                                                                                Stock actuel du programme: <b class="text-warning">{{$stock_dispo != '' ? $stock_dispo : 0}} <sup>{{$unite}}</sup></b>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="somme p-2">
                                                                        @if($data->status == 3)
                                                                            <span class="text-body">Qté livrée : </span> {{$qte_demande}} <br>&nbsp;<br>&nbsp;
                                                                        @else
                                                                            <span class="text-body">Qté demandée : </span> {{$qte_demande}} <br>&nbsp;<br>&nbsp;
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div> <!-- fin main_phase -->
                                            </div><!-- card -->

                                            {{-- @endisset --}}

                                        @endforeach
                                    @else
                                    Ce prestataire n'a aucune matière pour cette villa {{$lot->lot->lot}} !
                                    @endif

                                </div>
                            {{-- @endif --}}
                        @endforeach
                    </div>



                    </div>

                </div>
               </div>
            </div>
        </div>
    </div>
    <!-- end row -->


    <div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Rejeter le bon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formEdit" action="{{route('magasinier.demande.reject')}}" method="post" enctype="multipart/form-data">@csrf
                    <div class="modal-body">
                        <div  class="form-group">
                            <label class="control-label">Motif du rejet <span class="text-danger">*</span></label>
                            <br>
                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                            <input type="hidden" name="bonid" value="{{$data->ref}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="loader2" class="spinner-border text-primary m-1 dispayNone" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                        <div id="blockbtn">
                            <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                            <button type="button" onclick="submitform()" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                        </div>
                </div>

                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.valid', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment livrer cet bon ?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#btnaction').hide();
                    $('#loader').show();
                    window.location = href;
                }
            });
        });

    });

    function submitform(){
        var desc = $('#motif').val();
        if(desc == ''){
            swal("Oups!", "Veuillez renseignez la motif.");
            return false;
        }
        $('#blockbtn').hide();
        $('#loader2').show();
        document.getElementById('formEdit').submit();

    }
</script>
@endsection
