@extends('magasinier.layouts.app')

@section('page_title')
    Ajouter un bon de reception
@stop

@section('content_title')
    Ajouter un bon de reception
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Ajouter un bon de reception</li>
    </ol>
@stop

@section('content')


<div class="row">
    <div class="col-lg-12">
        <form action="{{route('mbr.store')}}" class="" method="post" id="addappelfonds" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT BC</h4>
                    <div class="form-group">
                        <label for="">BON DE COMMANDE <span class="text-danger">*</span></label>
                        <select onchange="selectSt()" name="bc" id="bc" class="form-control select2" style="min-width: 300px;" required >
                            <option selected disabled>Sélectionner le bon de commande</option>
                            @foreach ($bcs as $bc)
                                <option value="{{$bc->id}}">{{$bc->ref}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            @if(isset($itembc))
            <input type="hidden" id="demande" name="bdc" value="{{$itembc->id}}">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">DETAIL DU BC</h4>

                    <div class="row">
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">BC N°</h5>
                            <p>{{$itembc->ref}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Agent</h5>
                            <p>{{$itembc->user->name}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Bon de demande</h5>
                            <p><a target="_blank" href="{{route('mcommande.show', encrypt($itembc->demande->id))}}">{{$itembc->demande->ref}}</a></p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Fournisseur</h5>
                            <p>{{$itembc->fournisseur->nom}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de demande</h5>
                            <p>{{$itembc->created_at->format('d/m/Y')}}</p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">BON DE LIVRAISON</h4>

                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label>N° Bon de livraison <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="nbl" required id="nbl">
                            </div>
                        </div>

                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label for="photo">Charger le fichier numérique du bon de livraison <span class="text-danger">*</span></label>
                                <input type="file" class="form-control-file" name="file" required="" accept=".pdf" id="file">
                                <p class="help-block mt-1 text-muted">Fichier .PDF accepté</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-12">Produits <span class="text-danger">*</span></label>
                                <select name="pdt" id="pdt" class="form-control select2">
                                    <option selected disabled>Sélectionner un produit</option>
                                    @foreach($itembc->items as $item)
                                        @php $produit = \App\Produit::where('id',$item->produit_id)->with('type')->first();
                                            $nb =\App\StockFournisseurItem::where('bc_item_id',$item->id)->where('produit_id',$item->produit_id)->sum('qte');
                                        @endphp
                                        @if($nb < $item->qte)
                                            <option value="{{$item->id}}" data-qtecmd="{{$item->qte-$nb}}" data-typepdt="{{$produit? $produit->type->libelle : '-'}}" data-libelle="{{$produit? $produit->libelle : '-'}}">{{$produit? $produit->libelle : '-'}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-12">Quantités livrées <span class="text-danger">*</span></label>
                                <input type="number" min="1" id="qte" name="qte" class="form-control col-md-12" required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-12" style="opacity: 0">Ajouter dans le tableau </label>
                                <a onclick="add_element_to_array()" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-plus"></i> AJOUTER</a>
                            </div>
                        </div>

                        <div class="col-md-12 mt-3">
                            <input type="hidden" id="varieMontant">
                            <table class="table metable" id="data_session">
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Enregistrer</button>
            </div>
            @endif
        </form>

    </div>
</div>


@endsection


@section('other_js')
<script>
    var valueSelect = '',
        inputs = [],
        arr = [],
        valuemontantt = 0,
        x = 0;

    $(document).ready(function () {
        $('.select2').select2();
    });

    function selectSt(){
        $('#data_session').empty();
        $('#qte').val("");

        var idarrond = $('#bc :selected').val();
        var url ="{{ route('mbr.get.bc', ':id')}}";
        window.location.href
        url=url.replace(':id',idarrond);
        window.location = url;
    }

    function add_element_to_array(){
        var qtite = $("#qte").val();

        var qtiterest = 0;
        var produits = $("#pdt").val();
        //console.log(produits);

        if(qtite == "" || qtite<1){
            swal("Oups!", "Veuillez renseigner la quantité livré", "error");
            return false ;
        }else if(produits == ""){
            swal("Oups!", "Veuillez choisir un produit ", "error");
            return false ;
        }else{
            var valueSelect = $('#pdt');
            var selectmax = valueSelect.find(':selected').attr('data-qtecmd');
            //console.log("qtte " +qtite);
            //console.log("qtte max " +selectmax);

            if(parseInt(qtite) > parseInt(selectmax)){
                swal("Oups!", "La quantité commandée est de "+selectmax+".", "error");
                return false ;
            }

            var selectId = valueSelect.val(),
                pjtid = $('#demande').val(),
                selectLibellType = valueSelect.find(':selected').attr('data-typepdt'),
                selectLibell = valueSelect.find(':selected').attr('data-libelle');

            var request = getQteDispo(qtite,selectId,pjtid);

            $.when(request).done(function(data) {
                //console.log(data);
                if(data == "error"){
                    swal("Oups!", "Une erreur s'est produite, veuillez recommencez ", "error");
                    return false ;
                }else if (data != 'ok' && data != 'error'){
                    swal("Oups!", "Quantité restante à livrer : "+data , "error");
                }else{
                    //Check l'existance du item
                    var checkexit = inputs.filter(element => {
                        return element.id == selectId
                        // return element.selectId == selectId && element.proprios == proprios
                    })

                    if(checkexit.length==0){
                        arr = {id:selectId, libelletype:selectLibellType, libelle:selectLibell, qtelvre:qtite,qtecmd:selectmax,qterest:0};
                        inputs.push(arr);
                        showhtml();
                        $('#qte').val("");
                        $('#pdt').val("").trigger('change');
                        //$('#mttotal').html(valuemontantt.toLocaleString()+' Fcfa');
                    }else{
                        swal("Oups!", "Cette ligne existe déjà.", "error");
                        return false ;
                    }


                }
            });
        }
    }

    function getQteDispo(quantite,idbstock,idpjt) {

        var url ="{{ route('mbr.get.dispo',[':qte',':itempdt',':cmd'])}}";
        url=url.replace(':qte',quantite);
        url=url.replace(':itempdt',idbstock);
        url=url.replace(':cmd',idpjt);

        return $.ajax({url:url, type:'GET'});
    }

    function showhtml() {
        $("#data_session").empty();
        var table = "<tbody>";
        //console.log(inputs);
        table += '<tr class="bg-primary text-white">';
        table += '<td class="border-b">NUM</td>';
        table += '<td class="border-b text-lg font-medium">PRODUIT</td>';
        table += '<td class="border-b text-lg font-medium">U</td>';
        table += '<td class="border-b text-lg font-medium">QUANTITES LIVREES</td>';
        table += '<td class="border-b text-lg font-medium">QUANTITES COMMANDEES</td>';
        // table += '<td class="border-b text-lg font-medium">QUANTITES RESTANTES A LIVRER</td>';
        table += '<td class="border-b">ACTION</td>';
        table += '</tr>';

        for (var i = 0; i < inputs.length; i++) {
            var y = i + 1;
            var sluglibelle = inputs[i].libelle.replace(/\s+/g, '-').toLowerCase();
            //supprimer les apostrophes, les guillemets et les caractères spéciaux
            sluglibelle = sluglibelle.replace(/'/g, '');
            sluglibelle = sluglibelle.replace(/"/g, '');
            sluglibelle = sluglibelle.replace(/[^a-zA-Z0-9]/g, '-');

            table += '<tr data-row-id='+i+'>';
            table += '<td class="border-b">' + y + '</td>';
            table += '<td class="border-b">' + inputs[i].libelle + '</td>';
            table += '<td class="border-b">' + inputs[i].libelletype + '</td>';
            table += '<td class="border-b">' + inputs[i].qtelvre + '</td>';
            table += '<td class="border-b">' + inputs[i].qtecmd + '</td>';
            // table += '<td class="border-b">' + inputs[i].qterest + '</td>';
            table += '<td class="border-b"><a href="#" onclick="deleteSession('+i+')" class="inline-block text-1xl text-orange-500 mx-2" title="Supprimer la ligne"><i data-feather="trash" class="w-4 h-4 mr-2"></i> Supprimer</a></div>';
            table +='<input type="hidden" value="' + inputs[i].id + ';' + sluglibelle + ';' + inputs[i].libelletype + ';' + inputs[i].qtelvre + '" name="datasession[]">';
            table += '</tr>';
        };

        table += '<tr class="mt-3">';
        table += '<td class="border-b"></td>';
        table += '<td class="border-b"></td>';
        table += '<td class="border-b"></td>';
        table += '<td class="border-b"></td>';
        table += '<td class="border-b"></td>';
        // table += '<td class="border-b"></td>';
        table += '<td class="border-b"></td>';
        table += '</tr>';
        table += '</tbody>';

        $("#data_session").append(table);
    }

    function deleteSession(id) {
        inputs.splice(id,1);
        showhtml();

    }

    function submitForm() {
        event.preventDefault();
        if($('#demande').val()==''){
            swal("Oups!", "Veuillez renseigner le champ Bon de commande.");
        }else if($('#file').get(0).files.length == 0){
            swal("Oups!", "Veuillez renseigner le champ Bon de livraison.");
        }else{
            //var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Vous êtes sur le point d'entree un bon de reception ! Cette action est irréversible ? ",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    document.getElementById('addbtn').disabled = true;
                    document.getElementById('addappelfonds').submit();
                }
            });
        }

    }

</script>

@endsection
