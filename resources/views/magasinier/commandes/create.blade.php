@extends('magasinier.layouts.app')

@section('page_title')
    Passer une commande
@stop

@section('content_title')
    Passer une commande
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item active">Passer une commande</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form id="addformDC" class="repeater AddStocks" action="{{route('commandeStore')}}" method="post" enctype="multipart/form-data">
                @csrf
                {{-- LiSTE DES LIGNES BUDGETAIRES --}}
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3 text-primary">LIGNE BUDGETAIRE</h4>
                        <div class="form-group">
                            <label for="">SELECTIONNEZ LA LIGNE <span class="text-danger">*</span></label>
                            <select name="lignid" id="lignid" class="form-control select2" required>
                                <option selected disabled>Sélectionner une ligne</option>
                                @foreach($lignbudgets->ligneBudgetaires as $item)
                                <option value="{{$item->id}}">{{$item->libelle}}</option>
                                @endforeach
                                {{-- <option value="999">AUTRE</option> --}}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3 text-primary">DESCRIPTION </h4>
                        <div class="form-group">
                            <label for="">DESCRIPTION DE LA COMMANDE <span class="text-danger">*</span></label>
                            <textarea name="description" id="description" cols="30" rows="2" class="form-control" required></textarea>
                        </div>
                    </div>
                </div>

                @php $sousprograms = \App\ProgrammeSous::where('programme_id',Session::get('program'))->get() @endphp
                @if(count($sousprograms)>0)
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3 text-primary">SOUS-PROGRAMME </h4>
                        <div class="form-group">
                            <label for="">SELECTIONNEZ LE SOUS-PROGRAMME <span class="text-danger">*</span></label>
                            <select name="sousprogramme_id" id="sousprogramme_id" class="form-control select2" required>
                                <option selected disabled>Sélectionner un sous-programme</option>
                                @foreach($sousprograms as $item)
                                    <option value="{{$item->id}}">{{$item->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>@endif

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3 text-primary">LISTE DES PRODUITS</h4>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-12">Produits <span class="text-danger">*</span></label>
                                    <select name="produit_id" id="pdt" class="form-control select2">
                                        <option selected disabled>Sélectionner un produit</option>
                                        {{-- @foreach($listproduits as $item)
                                            <option value="{{$item->produit->id}}" data-typepdt="{{$item->produit->type? $item->produit->type->libelle : '-'}}" data-libelle="{{$item->produit->libelle}}">{{$item->produit->libelle}}</option>
                                        @endforeach --}}
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-12">Quantités à commander <span class="text-danger">*</span></label>
                                    <input type="number" step="any" id="qte" name="qte" required class="form-control"/>
                                    <small class="text-muted">Ex: 500 ; 1,5</small>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-12" style="opacity: 0">Ajouter dans le tableau </label>
                                    <a onclick="add_element_to_array()" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-plus"></i> AJOUTER</a>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <input type="hidden" id="varieMontant">
                                <table class="table metable" id="data_session">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                        <span class="sr-only">Chargement...</span>
                    </div>
                    <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
    <!-- end row -->


@endsection




@section('other_js')
    <script>
        var valueSelect = '',
            inputs = [],
            arr = [],
            valuemontantt = 0,
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();
        });

        //on change select ligne budgetaire
        $('#lignid').change(function(){
            $("#data_session").empty();
            //vider le tableau inputs
            inputs = [];

            var id = $(this).val();
            var route = "{{ route('getProduitByLigne', 'val') }}";
            route = route.replace('val',id);
            $.ajax({
                url: route,
                type: 'GET',
                success: function (data) {
                    $('#pdt').empty();
                    $('#pdt').append('<option selected disabled>Sélectionner un produit</option>');
                    $.each(data, function (index, value) {
                        $('#pdt').append('<option value="'+value.produit.id+'" data-typepdt="'+value.produit.type.libelle+'" data-libelle="'+value.produit.libelle+'">'+value.produit.libelle+'</option>');
                    });
                }
            });
        });

        // check last statut of dossier
        $('#order-selectinput').change(function(){
            $('#form-filtre').submit();
        });

        function getQteDispo(quantite,idbstock) {
            var url ="{{ url('get/'.'valmontant'.'/'.'attribut'.'/pdt/dispo')}}";
            url=url.replace('valmontant',quantite);
            url=url.replace('attribut',idbstock);

            return $.ajax({url:url, type:'GET'});
        }


        function add_element_to_array(){
            var qtite = $("#qte").val();
            var produits = $("#pdt").val();
            // console.log(produits);

            if(qtite == "" || qtite==0){
                swal("Oups!", "Veuillez renseigner la quantité à commander", "error");
                return false ;
            }else if(produits == "" || !produits){
                swal("Oups!", "Veuillez choisir un produit ", "error");
                return false ;
            }else{

                //Verifier si la quantité a commander est superieur a la quantité restante
                var request = getQteDispo(qtite,produits);
                $.when(request).done(function(data) {
                    // console.log(data);
                    if(data == 'ok'){
                        //vider les champs
                        $("#qte").val('');

                        var valueSelect = $('#pdt');

                        var selectId = valueSelect.val(),
                            selectLibellType = valueSelect.find(':selected').attr('data-typepdt'),
                            selectLibell = valueSelect.find(':selected').attr('data-libelle');

                        //Check l'existance du item
                        var checkexit = inputs.filter(element => {
                            return element.id == selectId
                            // return element.selectId == selectId && element.proprios == proprios
                        })

                        if(checkexit.length==0){
                            arr = {id:selectId, libelletype:selectLibellType, libelle:selectLibell, qtelvre:qtite};
                            inputs.push(arr);
                            showhtml();
                        }else{
                            swal("Oups!", "Cette ligne existe déjà.", "error");
                            return false ;
                        }
                    }else{
                        swal("Oups!", "La quantité à commander est supérieur à la quantité restante, quantité restante : "+data, "error");
                        return false ;
                    }
                });


            }
        }

        function showhtml() {
            $("#data_session").empty();
            var table = "<tbody>";
            //console.log(inputs);
            table += '<tr class="bg-primary text-white">';
            table += '<td class="border-b">NUM</td>';
            table += '<td class="border-b text-lg font-medium">PRODUIT</td>';
            table += '<td class="border-b text-lg font-medium">TYPE</td>';
            table += '<td class="border-b text-lg font-medium">QUANTITES COMMANDEES</td>';
            table += '<td class="border-b">ACTION</td>';
            table += '</tr>';

            for (var i = 0; i < inputs.length; i++) {
                var y = i + 1;
                //slugs libelle value
                var sluglibelle = inputs[i].libelle.replace(/\s+/g, '-').toLowerCase();
                //supprimer les apostrophes, les guillemets et les caractères spéciaux
                sluglibelle = sluglibelle.replace(/'/g, '');
                sluglibelle = sluglibelle.replace(/"/g, '');
                sluglibelle = sluglibelle.replace(/[^a-zA-Z0-9]/g, '-');

                table += '<tr data-row-id='+i+'>';
                table += '<td class="border-b">' + y + '</td>';
                table += '<td class="border-b">' + inputs[i].libelle + '</td>';
                table += '<td class="border-b">' + inputs[i].libelletype + '</td>';
                table += '<td class="border-b">' + inputs[i].qtelvre + '</td>';
                // table += '<td class="border-b">' + inputs[i].qterest + '</td>';
                table += '<td class="border-b"><a style="cursor:pointer" onclick="deleteSession('+i+')" class="inline-block text-1xl text-orange-500 mx-2" title="Supprimer la ligne"><i data-feather="trash" class="w-4 h-4 mr-2"></i> Supprimer</a></div>';
                table +='<input type="hidden" value="' + inputs[i].id + ';' + sluglibelle + ';' + inputs[i].libelletype + ';' + inputs[i].qtelvre + '" name="datasession[]">';
                table += '</tr>';
            };

            table += '<tr class="mt-3">';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '</tr>';
            table += '</tbody>';

            $("#data_session").append(table);
        }

        function deleteSession(id) {
            inputs.splice(id,1);
            showhtml();

        }

        function submitForm() {
            event.preventDefault();
            if($('#description').val()==''){
                swal("Oups!", "Veuillez renseigner le champ description.");
            }else if($('#lignid').val()==''){
                swal("Oups!", "Veuillez choisir une ligne budgétaire.");
            }else{
                //var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de faire de demande de commande ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        document.getElementById('addbtn').disabled = true;
                        $('#addbtn').hide();
                        $('#loader').show();
                        document.getElementById('addformDC').submit();
                    }
                });
            }

        }

    </script>

@endsection


@section('other_css')

@endsection
