@extends('magasinier.layouts.app')

@section('page_title')
    Commandes rejetées
@stop

@section('content_title')
    Commandes rejetées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Commandes rejetées</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($demandes) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>N°</th>
                            <th>N° Ref</th>
                            <th>Ligne Budgetaire</th>
                            <th>Demandeur</th>
                            <th>Nombre de produit</th>
                            <th>Statut</th>
                            <th>Date de commande</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $i=>$dt)
                            <tr class="font-size-16">
                                <td>{{$i+1}}</td>
                                <td><a href="{{route('mcommande.show', encrypt($dt->id))}}" class="text-primary font-weight-bold">{{$dt->ref}}</a> </td>
                                <td>
                                    @if($dt->lignebudget)
                                    @php $lignb = $dt->lignebudget @endphp
                                        {{strtoupper($lignb->libelle)}} <br>
                                        @if($lignb->hors_ds == 1)
                                            <span class="badge badge-pill badge-warning">Hors DS</span>
                                        @elseif ($lignb->type == 1)
                                            <span class="badge badge-pill badge-primary">Batiments</span>
                                        @elseif ($lignb->type == 2)
                                            <span class="badge badge-pill badge-dark">VRD</span>
                                        @else
                                            <span class="badge badge-pill badge-success">-</span>
                                        @endif
                                    @else
                                        <span class="badge badge-pill badge-danger">Budget non défini</span>
                                    @endif
                                </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{count($dt->produits)}}</td>
                                <td>
                                    <span class="badge badge-pill badge-danger">Rejété</span>
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="{{route('mcommande.show', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                    {{--@if($dt->status == 4)
                                        <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" data-toggle="modal" data-target="#delPrevent-{{$dt->id}}" title="Supprimer"> <i class="fas fa-trash"></i> </button>
                                    @endif

                                     <div id="delPrevent-{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0">Suppression</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-center-">
                                                    <h5>Voulez-vous supprimer <br> cette demande de commande N° <b class="text-warning">{{$dt->ref}}</b> &nbsp; ?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{route('mcommande.delete', encrypt($dt->id))}}" class="btn btn-primary btn-rounded waves-effect waves-light">
                                                        &nbsp; &nbsp; Oui &nbsp; &nbsp;
                                                    </a> &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <button type="button" class="btn btn-rounded" data-dismiss="modal">Annuler</button>
                                                </div>

                                            </div><!-- /.modal-content -->
                                        </div>
                                    </div> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
