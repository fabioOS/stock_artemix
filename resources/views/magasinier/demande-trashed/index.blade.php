@extends('magasinier.layouts.app')

@section('page_title')
    Bon de demande
@stop

@section('content_title')
    Bon de demande
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item active">Bon de demande</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if ($data->count() > 0)
                <div class="table-responsive mb-4">
                    <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                        <thead>
                            <tr class="bg-transparent">
                                <th>N° Ref</th>
                                <th>Fichier</th>
                                <th>Chef chantier</th>
                                <th>Sous-traitant</th>
                                <th>lot(s)</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th style="width: 120px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="font-size-16">
                                <td><a href="!#" class="text-dark font-weight-bold">#MN0132</a> </td>
                                <td><a href="#" download>bon_de_demande.pdf</a></td>
                                <td>Ibrahima SORO</td>
                                <td>READY WEAR</td>
                                <td>
                                    <span class="badge badge-pill badge-info">1</span>
                                    <span class="badge badge-pill badge-info">2</span>
                                    <span class="badge badge-pill badge-info">10</span>
                                </td>
                                <td>
                                    <span class="badge badge-pill badge-warning">En attente</span>
                                    <span class="badge badge-pill badge-success">livré</span>
                                    <span class="badge badge-pill badge-danger">Rejeté</span>
                                </td>
                                <td>29/08/2021</td>
                                <td>
                                    <a href="{{route('magasinier.demande.show',encrypt('$dt->id'))}}" class="btn btn-primary waves-effect waves-light">
                                         <i class="uil uil-eye mr-2"></i> Détails
                                    </a>

                                </td>
                            </tr>
                            @foreach ($data as $dt)
                                <tr class="font-size-16">
                                    <td>{{$dt->libelle}}</td>
                                    <td>
                                        @php
                                            $seuil = $dt->seuil;
                                            if ( isset($dt->stocks[0]) && $dt->stocks[0]->qte != null) $stock = $dt->stocks[0]->qte;
                                            else $stock = 0;
                                        @endphp
                                        @if($stock  == 0)
                                            <span class="badge badge-pill badge-danger font-size-14">0</span>
                                        @else
                                            @if ($seuil == 0)
                                                <span class="badge badge-pill badge-success font-size-14">{{$stock}}</span>
                                            @else
                                                @if($stock > $seuil)
                                                    <span class="badge badge-pill badge-success font-size-14">{{$stock}}</span>
                                                @else
                                                    <span class="badge badge-pill badge-warning font-size-14">{{$stock}}</span>
                                                @endif
                                            @endif
                                        @endif
                                    </td>
                                    <td>{{$dt->type->libelle}}</td>
                                    <td>
                                        <button type="button" data-toggle="modal" data-target="#addItemStock-{{$dt->id}}" title="Approvisionner" class="btn btn-primary btn-sm waves-effect waves-light">
                                            <i class="uil uil-plus mr-2"></i> Approvisionner
                                        </button>
                                        @if($stock  == 0)
                                            <button type="button" class="btn btn-danger btn-sm waves-effect waves-light disabled" disabled="disabled">
                                                <i class="uil uil-plus mr-2"></i> Reduire
                                            </button>
                                        @else
                                            <button type="button" data-toggle="modal" data-target="#ReduiceItemStock-{{$dt->id}}" title="Reduire" class="btn btn-danger btn-sm waves-effect waves-light">
                                                <i class="uil uil-minus mr-2"></i> Reduire
                                            </button>
                                        @endif


                                        <!--  modal ITEM appro -->
                                        <div id="addItemStock-{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form action="{{route('magasinier.stock.appro',encrypt($dt->id))}}" method="get" id="progForm">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Approvisionner la matière <b class="text-primary">{{$dt->libelle}}</b></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label>Quanité en stock ({{$dt->type->libelle}})</label>
                                                                    <input type="text" name="" class="form-control"
                                                                        required="" value="{{$stock}}" disabled>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Quanité à approvisionner  ({{$dt->type->libelle}}) <span class="text-danger">*</span></label>
                                                                    <input type="number" step="any" name="qte" class="form-control"
                                                                        required="">
                                                                </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light waves-effect waves-light  ml-4" data-dismiss="modal">Annuler</button>
                                                            <button type="submit" class="btn btn-primary waves-effect waves-light ml-4">Ajouter</button>
                                                        </div>
                                                    </form>
                                                </div><!-- /.modal-content -->
                                            </div>
                                        </div><!-- /.modal update type de bien  -->


                                        <!--  modal ITEM reduire -->
                                        <div id="ReduiceItemStock-{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0">Reduire la matière <b class="text-primary">{{$dt->libelle}}</b></h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('magasinier.stock.reduire',encrypt($dt->id))}}" method="get" id="R-progForm">

                                                            <div class="form-group">
                                                                <label>Quanité en stock ({{$dt->type->libelle}})</label>
                                                                <input type="text" name="" class="form-control"
                                                                    required="" value="{{$stock}}" disabled>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Quanité à reduire  ({{$dt->type->libelle}}) <span class="text-danger">*</span></label>
                                                                <input type="number" step="any" name="qte" class="form-control"
                                                                    required="">
                                                            </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-light waves-effect mr-4" data-dismiss="modal">Annuler</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ml-4">Reduire</button>
                                                    </div>
                                                </form>
                                            </div><!-- /.modal-content -->
                                            </div>
                                        </div><!-- /.modal update type de bien  -->
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end table -->

                <!--  modal ITEM addSousPhase -->
                <div id="addStock" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Ajouter un stock</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="repeater AddStocks" action="{{route('magasinier.stock.store')}}" method="post">
                                @csrf
                                <div class="modal-body">
                                    <div data-repeater-list="main_phase2">
                                        <div data-repeater-item class="row">
                                            <div  class="form-group col-lg-5">
                                                <label class="control-label">Sélectionner une matière <span class="text-danger">*</span></label>
                                                <br>
                                                <select class="form-control select2" style="width: 100%;" name="produit_id" required>
                                                    <option value=""></option>
                                                    @foreach ($types as $type)
                                                        @if (count($type->produits)>0)
                                                            <optgroup label="{{$type->libelle}}">
                                                                @foreach ($type->produits as $produit)
                                                                    <option value="{{$produit->id}}">{{$produit->libelle}}</option>
                                                                @endforeach
                                                            </optgroup>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div  class="form-group col-lg-3">
                                                <label for="">Quanité <span class="text-danger">*</span></label>
                                                <input type="number" step="any" name="qte" required class="form-control"/>
                                                <small class="text-muted">Ex: 500 ; 1,5</small>
                                            </div>

                                            <div class="col-lg-2 align-self-center" style="margin-top: -10px;">
                                                <button data-repeater-delete type="button" class="btn btn-danger">
                                                    <i class="uil uil-trash font-size-18"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                                    <button data-repeater-create type="button" class="btn btn-primary">
                                        <i class="uil uil-plus font-size-14"></i>
                                    </button>

                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light waves-effect mr-4" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light ml-4">Ajouter</button>
                                </div>
                            </form>

                        </div><!-- /.modal-content -->
                    </div>
                </div><!-- /.modal update type de bien  -->

            @else
                <h5 class="text-warning"> Il n'y a aucune matière pour l'instant !</h5>
            @endif
        </div>
    </div>
    <!-- end row -->

@endsection




@section('other_js')

@endsection


@section('other_css')

@endsection
