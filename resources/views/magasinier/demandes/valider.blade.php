@extends('magasinier.layouts.app')

@section('page_title')
    Demandes spéciales validées
@stop

@section('content_title')
    Demandes spéciales validées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciales validées</li>
    </ol>
@stop

@section('content')

<div class="row gstock">
    <div class="col-lg-12">
        @if (count($demandes) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>N°Ref</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th>Motif</th>
                            <th style="width: 100px;">Montant</th>
                            <th>Statut</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $dt)
                            <tr class="font-size-16">
                                <td><a href="!#" class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>{{$dt->description}}</td>
                                <td>@price($dt->montant)</td>
                                <td>
                                    @if ($dt->status == 2)
                                        <span class="badge badge-pill badge-success">Validé</span>
                                    @endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @if($dt->status==2)
                                        <a href="{{route('pdfDmdSpecial', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Télécharger le BC"><i class="uil-file-download"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

<div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Rejeter la demande</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formEdit" action="{{route('cpdemande.rejeter')}}" method="post" enctype="multipart/form-data">@csrf
                <div class="modal-body">
                    <div  class="form-group">
                        <label class="control-label">Motif <span class="text-danger">*</span></label>
                        <br>
                        <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" required></textarea>
                        <input type="hidden" name="bonid" value="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Rejeter</button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div>
</div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.generate', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment valider ce BC",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

    });

    function showrejet(id){
        $("#formEdit")[0].reset();
        $("#bonid").val(id);
    }
</script>
@endsection
