@extends('magasinier.layouts.app')

@section('page_title')
    Demandes spéciales en attente de traitement
@stop

@section('content_title')
    Demandes spéciales en attente de traitement
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciales en attente de traitement</li>
    </ol>
@stop

@section('content')

<div class="row gstock">
    <div class="col-lg-12">
        @if (count($demandes) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>N°Ref</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th>Motif</th>
                            <th style="width: 100px">Montant</th>
                            <th>Statut</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td><a href="#" class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>{{$dt->description}}</td>
                                <td>@price($dt->montant)</td>
                                <td>
                                    @if($dt->genere==1)
                                    <span class="badge badge-pill badge-warning">En attente <br>de validation du DO</span>
                                    @else
                                    <span class="badge badge-pill badge-warning">En attente</span>
                                    @endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @if($dt->user_id == Auth::user()->id and $dt->genere == 0)
                                        <a href="{{route('delDmdSpecial',encrypt($dt->id))}}" class="delet btn btn-danger btn-sm" title="Supprimer la demande"> <i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>


@endsection


@section('other_js')
<script>
    $(document).ready(function(){
        $('.gstock').on('click', '.delet', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cette demande.",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

    });

</script>
@endsection
