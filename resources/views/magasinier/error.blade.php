@extends('magasinier.layouts.error')

@section('page_title')
    Erreur 500 | {{ env('APP_NAME') }}
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <div>
                    <div class="row justify-content-center">
                        <div class="col-sm-4">
                            <div class="error-img">
                                <img src="{{asset('assets/images/500-error.png')}}" alt="" class="img-fluid mx-auto d-block">
                            </div>
                        </div>
                    </div>
                </div>
                <h4 class="text-uppercase mt-4">Erreur 500</h4>
                <p class="text-muted text-danger">{{$message}}</p>
                <div class="mt-5">
                    <a class="btn btn-primary waves-effect waves-light" href="@if(isset($_SERVER['HTTP_REFERER'])) {{$_SERVER['HTTP_REFERER']}} @else {{route('home')}} @endif"> <i class="fas fa-chevron-left"></i> Retour </a>
                </div>
            </div>

        </div>
    </div>
    <!-- end row --> &nbsp;

@endsection

