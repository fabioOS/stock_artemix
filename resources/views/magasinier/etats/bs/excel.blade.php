<table>
    <thead>
        <tr>
            <td>NUM</td>
            <td>VILLA</td>
            <td>MATIERE</td>
            <td>TYPE</td>
            <td>QUANTITE DEMANDEE</td>
            <td>DATE DE DEMANDE</td>
        </tr>
    </thead>
    <tbody>
        @foreach($data->items as $e=>$item)
            @php $type =\App\Produit::where('id',$item->produit_id)->has('type')->first()  @endphp
            <tr class="">
                <td>{{$e+1}}</td>
                <td>Lot {{\App\ProgrammeLot::where('id',$item->lot_id)->pluck('lot')->first()}}</td>
                <td><strong>{{\App\Produit::where('id',$item->produit_id)->pluck('libelle')->first()}}</strong></td>
                <td>{{$type ? $type->type->libelle : ''}}</td>
                <td>{{$item->qte}}</td>
                <td>{{$item->created_at->format('d/m/Y')}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
