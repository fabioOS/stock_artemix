<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

    .page-break {
        page-break-after: always;
    }

</style>
<body>
<div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td width="50%">
                <div class="col">
                    <img src="{{base_path('assets/images/logoartemis.png')}}" data-holder-rendered="true" height="50px" />
                </div>
            </td>
            <td width="50%" style="text-align: right">
                {{date('d/m/Y h:i:s')}}
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: right">
                <p style="font-size: 14px;font-weight: bold">PROGRAMME : {{$datas[0]->programme->libelle}}</p>
            </td>
        </tr>
    </table>


    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold;margin-bottom: 15px;">ETAT DES BONS DE SORTIR</p>
                @if(isset($date) and isset($date2))
                    <span style="font-size: 14px">Du {{date('d/m/Y',strtotime($date))}} au {{date('d/m/Y',strtotime($date2))}}</span>
                @endif
            </td>
        </tr>

    </table>

    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>NUM</strong></td>
            <td class="td"><strong>N°REF</strong></td>
            <td class="td"><strong>DEMANDEUR</strong></td>
            <td class="td"><strong>SOUS-TRAITANT</strong></td>
            <td class="td"><strong>STATUS</strong></td>
            <td class="td"><strong>DATE DE DEMANDE</strong></td>
        </tr>
        @foreach($datas as $k=>$data)
            <tr class="">
                <td class="td">{{$k+1}}</td>
                <td class="td"><p><strong>{{$data->ref}}</strong></p></td>
                <td class="td">{{$data->user ? $data->user->name : '-'}}</td>
                <td class="td">{{$data->soustraitant ? $data->soustraitant->nom : '-'}}</td>
                <td class="td">@if($data->status==1) EN ATTENTE @elseif ($data->status==3) LIVRE @elseif ($data->status==2) VALIDER @else REJETER @endif</td>
                <td class="td">{{$data->created_at->format('d/m/Y')}}</td>
            </tr>
        @endforeach

    </table>
</div>
<div class="page-break"></div>
</body>
