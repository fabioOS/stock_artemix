<style>
    .page-break {
        page-break-after: always;
    }

    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold;margin-bottom: 15px;">DETAILS DES BONS DE SORTIE #{{$data->ref}}</p>
            </td>
        </tr>
    </table>

    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>NUM</strong></td>
            <td class="td"><strong>VILLA</strong></td>
            <td class="td"><strong>MATIERE</strong></td>
            <td class="td"><strong>TYPE</strong></td>
            <td class="td"><strong>QUANTITE DEMANDEE</strong></td>
            <td class="td"><strong>DATE DE DEMANDE</strong></td>
        </tr>
        @foreach ($data->items as $e=>$item)
            @php $type =\App\Produit::where('id',$item->produit_id)->has('type')->first()  @endphp
            <tr class="">
                <td class="td">{{$e+1}}</td>
                <td class="td">Lot {{\App\ProgrammeLot::where('id',$item->lot_id)->pluck('lot')->first()}}</td>
                <td class="td"><strong>{{\App\Produit::where('id',$item->produit_id)->pluck('libelle')->first()}}</strong></td>
                <td class="td">{{$type ? $type->type->libelle : ''}}</td>
                <td class="td">{{$item->qte}}</td>
                <td class="td">{{$item->created_at->format('d/m/Y')}}</td>
            </tr>
        @endforeach
    </table>
</div>
<div class="page-break"></div>
</body>
