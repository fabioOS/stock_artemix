<?php
    $url = Request::url();
    // demande
    $url_is_demande = explode('/demande', $url);
    // livraison
    $url_is_livraison = explode('/livraison', $url);

?>
<style>
    .text-bleu{
        color: #234699
    }
</style>

<ul class="metismenu list-unstyled" id="side-menu">
    <li>
        <a href="{{route('magasinier.home')}}">
            <i class="uil-home-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li>

    <li class="menu-title text-bleu">Gestion du stock</li>

    <li class="{{Route::is('magasinier.etats') || Route::is('magasinier.etats.search') || Route::is('magasinier.etats.item') ? 'mm-active' : ''}}">
        <a href="{{route('magasinier.etats')}}" class="{{Route::is('magasinier.etats') || Route::is('magasinier.etats.search') || Route::is('magasinier.etats.item') ? 'active' : ''}}">
            <i class="uil-home"></i>
            <span>Etats par villa</span>
        </a>
    </li>

    {{-- <li>
        <a href="{{route('magasinier.stock.index')}}">
            <i class="uil-shop"></i>
            <span>Stock</span>
        </a>
    </li> --}}

    <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-shop"></i>
            <span>Stock</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('magasinier.stock.index')}}" class="{{Route::is('magasinier.stock.index') ? 'active' : ''}}">Stock interne</a></li>
            <li><a href="{{route('magasinier.stock.budget')}}" class="{{Route::is('magasinier.stock.budget') ? 'active' : ''}}">Stock budget</a></li>
            {{-- <li><a href="{{route('magasinier.stock.reference')}}" class="{{Route::is('magasinier.stock.reference') ? 'active' : ''}}">Stock par reference</a></li> --}}
        </ul>
    </li>

    @if(1==2)
    <li>
        <a href="{{route('magasinier.fours')}}">
            <i class="uil-user-circle"></i>
            <span>Fournisseurs</span>
        </a>
    </li>
    @endif

    <li class="{{Route::is('magasinier.livraison.details') ||Route::is('magasinier.demande.details') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="{{count((new \App\Http\Controllers\BonController())->getByStatus(2)) > 0 ? '' : 'has-arrow'}} waves-effect {{Route::is('magasinier.livraison.details')||Route::is('magasinier.demande.details') ? 'mm-active' : ''}}">
            <i class="uil-file-alt"></i>
            <span>Bon de sortie</span>
            @if(count((new \App\Http\Controllers\BonController())->getByStatus(2)) > 0)
                <span class="badge badge-pill badge-warning float-right">
                    {{count((new \App\Http\Controllers\BonController())->getByStatus(2))}}
                </span>
            @endif
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('magasinier.demande.index')}}" class="{{Route::is('magasinier.demande.index') ? 'active' : ''}}">Bon en attente
                @if(count((new \App\Http\Controllers\BonController())->getByStatus(2)) > 0)
                    <span class="badge badge-pill badge-warning float-right">
                        {{count((new \App\Http\Controllers\BonController())->getByStatus(2))}}
                    </span>
                @endif
            </a></li>

            <li><a href="{{route('magasinier.livraison.index')}}" class="{{Route::is('magasinier.livraison.index') ? 'active' : ''}}">Bon livré</a></li>
            <li><a href="{{route('magasinier.etats.bs')}}" class="{{Route::is('magasinier.etats.bs') ? 'active' : ''}}">Etats</a></li>

        </ul>
    </li>

    <li class="{{Route::is('magasinier.stock.back.index') ||Route::is('magasinier.stock.back.show') ||Route::is('magasinier.stock.back.create')||Route::is('magasinier.stock.back.create_step2') ? 'mm-active' : ''}}">
        <a class="{{Route::is('magasinier.stock.back.index') ||Route::is('magasinier.stock.back.show') ||Route::is('magasinier.stock.back.create')||Route::is('magasinier.stock.back.create_step2') ? 'active' : ''}}" href="{{route('magasinier.stock.back.index')}}">
            <i class="uil-step-backward-alt"></i>
            <span>Retour en stock</span>
        </a>
    </li>

    {{-- <li>
        <a href="{{route('magasinier.demande.index')}}" class="{{ count($url_is_demande) >= 2 ? 'active' : '' }}">
            <i class="uil-home-alt"></i>
            @if(count((new \App\Http\Controllers\BonController())->getByStatus(1)) > 0)
            <span class="badge badge-pill badge-info float-right">
                {{count((new \App\Http\Controllers\BonController())->getByStatus(1))}}
            </span>
            @endif
            <span>Bon de sortie</span>
        </a>
    </li>

    <li>
        <a href="{{route('magasinier.livraison.index')}}" class="{{Route::is('magasinier.livraison.index') ? 'active' : ''}} {{Route::is('magasinier.livraison.details') ? 'active' : ''}} ">
            <i class="uil-home-alt"></i>
            <span>Bon de sortie livré</span>
        </a>
    </li> --}}

    <li class="menu-title text-bleu">Gestion des commandes</li>

    {{-- <li>
        <a href="#">
            <i class="uil-home-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li> --}}

    @php
        //$validated = (new \App\Http\Controllers\Magasinier\BonController())->getByStatus(2);
        $canceled = (new \App\Http\Controllers\Magasinier\CommandeController())->getByStatus(4);
        $waiting = (new \App\Http\Controllers\Magasinier\CommandeController())->getByStatus(1);
        //$delivered = (new \App\Http\Controllers\Magasinier\BonController())->getByStatus(3);
    @endphp

    <li class="{{Route::is('mcommande.show') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="has-arrow waves-effect {{Route::is('mcommande.show') ? 'mm-active' : ''}}" >
            <i class="uil-file-alt"></i>
            <span>Commandes</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('commande.create')}}" class="{{Route::is('commande.create') ? 'active' : ''}}">Passer une commande</a></li>
            <li><a href="{{route('mcommande.waiting')}}" class="{{Route::is('mcommande.waiting') ? 'active' : ''}}">En attente
                @if($waiting>0 )
                    <span class="badge badge-pill badge-primary float-right">{{$waiting}}</span>
                @endif
            </a></li>
            <li><a href="{{route('mcommande.rejet')}}" class="{{Route::is('mcommande.rejet') ? 'active' : ''}}">Rejetées
                @if($canceled>0 )
                    <span class="badge badge-pill badge-danger float-right">{{$canceled}}</span>
                @endif
            </a></li>
            <li><a href="{{route('mcommande.valider')}}" class="{{Route::is('mcommande.valider') ? 'active' : ''}}">Validées
                {{-- @if($canceled>0 )
                    <span class="badge badge-pill badge-danger float-right">{{$canceled}}</span>
                @endif --}}
            </a></li>
        </ul>
    </li>

    <li>
        <a href="{{route('mbc.index')}}" class="{{Route::is('mbc.show') ? 'mm-active' : ''}}">
            <i class="uil-file-alt"></i>
            <span>Bon de commande</span>
        </a>
    </li>

    <li class="{{Route::is('mbr.show') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="has-arrow waves-effect {{Route::is('mbr.show') ? 'mm-active' : ''}}">
            <i class="uil-file-search-alt"></i>
            <span>Bon de reception</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('mbr.create')}}" class="{{Route::is('mbr.create') ? 'active' : ''}}">Ajouter un bon</a></li>
            <li><a href="{{route('mbr.index')}}" class="{{Route::is('mbr.index') ? 'active' : ''}}">Bon de reception</a></li>
        </ul>
    </li>

    <li class="menu-title text-bleu">Gestion des demandes</li>
        @php
            $waiting = (new \App\Http\Controllers\DemandeSpecialsController())->getByStatus(1);
        @endphp
    <li>
        <a href="javascript: void(0);" class="{{count($waiting) > 0 ? '' : 'has-arrow'}} waves-effect">
            <i class="uil-align-letter-right"></i>
            <span>Demandes spéciales</span>
            @if(count($waiting) > 0)
                <span class="badge badge-pill badge-warning float-right">{{count($waiting)}}</span>
            @endif
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('mdemandespec.create')}}" class="{{Route::is('mdemandespec.create') ? 'active' : ''}}">Créer une demande</a></li>
            <li><a href="{{route('mdemandespec.waiting')}}" class="{{Route::is('mdemandespec.waiting') ? 'active' : ''}}">En attente
                @if(count($waiting)>0 )
                    <span class="badge badge-pill badge-warning float-right">{{count($waiting)}}</span>
                @endif
            </a></li>
            <li><a href="{{route('mdemandespec.rejeter')}}" class="{{Route::is('mdemandespec.rejet') ? 'active' : ''}}">Rejetées </a></li>
            <li><a href="{{route('mdemandespec.valider')}}" class="{{Route::is('mdemandespec.valider') ? 'active' : ''}}">Validées </a></li>
        </ul>

    </li>


</ul>
