@extends('magasinier.layouts.app')

@section('page_title')
    Tableau de bord
@stop

@section('content_title')
    Tableau de bord
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item active">Tableau de bord</li>
    </ol>
@stop

@section('content')


    @php
        $data = new \App\Http\Controllers\BonController();
    @endphp

    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24">
                            <span data-plugin="counterup">{{count($data->getAll())}}</span>
                        </h4>
                        <p class="text-muted mb-0">Total bon de sortie</p>
                    </div>

                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24">
                            <span data-plugin="counterup">{{count($data->getByStatus(1))}}</span>
                        </h4>
                        <p class="text-muted mb-0">Total bon de sortie en attente</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24">
                            <span data-plugin="counterup">{{count($data->getByStatus(2))}}</span>
                        </h4>
                        <p class="text-muted mb-0">Total bon de sortie validé</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h4 class="mb-1 mt-1 font-size-24">
                            <span data-plugin="counterup">{{count($data->getByStatus(3))}}</span>
                        </h4>
                        <p class="text-muted mb-0">Total bon de sortie livré</p>
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

    </div> <!-- end row-->

    <div class="row">
        <div class="col-md-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Top 8 des matières les plus commandées</h4>

                    @php $color = ['primary','primary2','success','warning','purple','dark','info','danger'];
                        $max = $top3cmd->max('price');
                    @endphp

                    <div class="row align-items-center no-gutters mt-3">
                        <div class="col-sm-6">
                            <p class="text-truncate mt-1 mb-0"><b>MATIERE</b> </p>
                        </div>

                        <div class="col-sm-6 text-right">
                            <p class="text-truncate mt-1 mb-0"><b>QUANTITES</b> </p>
                        </div>

                        {{-- <div class="col-sm-6">
                            <b>EVOLUTION</b>
                        </div> --}}
                    </div>

                    @foreach ($top3cmd as $k=>$item)
                        @php
                        $pcent = $max ? ($item->price *100) / $max  : 0;
                        @endphp
                        <div class="row align-items-center no-gutters mt-3">
                            <div class="col-sm-6">
                                <p class="text-truncate mt-1 mb-0"><i class="mdi mdi-circle-medium text-{{$color[$k]}} mr-2"></i> {{$item->produits->libelle}}  </p>
                            </div>

                            <div class="col-sm-6 text-right">
                                <p class="text-truncate mt-1 mb-0">{{$item->total}} <sup class="badge badge-info">{{$item->produits->type->libelle}}</sup></p>
                            </div>

                            {{-- <div class="col-sm-6">
                                <div class="progress mt-1" style="height: 17px;">
                                    <div class="progress-bar progress-bar bg-{{$color[$k]}}" role="progressbar"
                                        style="width: {{$pcent}}%;font-size:16px" aria-valuenow="{{$pcent}}" aria-valuemin="0"
                                        aria-valuemax="{{$pcent}}">
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    @endforeach

                </div> <!-- end card-body-->
            </div>
        </div>
    {{-- </div>

    <div class="row"> --}}
        <div class="col-md-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Matières à approvisionner</h4>
                    <div class="card-body table-responsive">
                        <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>N°</th>
                                <th>Code</th>
                                <th>Matière</th>
                                <th>Quantités disponible</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($mtlimit as $k=>$item)
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td>{{$item->produits->code}}</td>
                                    <td>{{$item->produits->libelle}} <sup class="text-muted font-size-13">{{$item->produits->type ? $item->produits->type->libelle : ''}}</sup></td>
                                    <td>{{$item->qte}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Liste total des demandes à traiter</h4>

                    @php
                        $data = $data->getByStatus(2);
                        $prefix = 'magasinier.demande';
                    @endphp
                    {!!(new \App\Http\Controllers\BonController())->getDataTable($data, $prefix)!!}
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection


@section('other_js')
    <script>

    </script>

@endsection
