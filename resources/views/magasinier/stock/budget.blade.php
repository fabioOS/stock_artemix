@extends('magasinier.layouts.app')

@section('page_title')
    Gestion du stock budget
@stop

@section('content_title')
    Gestion du stock budget
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item active">Stock budget</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>Matière</th>
                        <th>Quantités nécessaires</th>
                        <th>Quantités livrées</th>
                        <th>Quantités restantes</th>
                        <th>Unité de mésure</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($produits as $k=> $produit)
                        @php
                            $types = \App\ProgrammeActif::where('programme_id',Session::get('program'))->get();
                            //dump($types);
                            $ttpdtype = 0 ;
                            foreach ($types as $type){
                                $nbtyp = \App\ProgrammeSouscorpsetat::where('programme_id',Session::get('program'))->where('produit_id',$produit->id)->where('actif_id',$type->id)->sum('qte');
                                //multiplier par le nombre de lots
                                $nblot = \App\ProgrammeLot::where('programme_id',Session::get('program'))->where('actif_id',$type->id)->count();
                                //dd($nbtyp,$nblot);
                                $tt = $nblot * $nbtyp ;
                                $ttpdtype = $ttpdtype + $tt ;
                            }
                            $qtecmd = \App\StockFournisseurItem::where('programme_id',Session::get('program'))->where('produit_id',$produit->id)->sum('qte');
                        @endphp
                        <tr class="font-size-16">
                            <td>{{$produit->libelle}}</td>
                            <td>
                                <span class="badge badge-pill badge-success font-size-14">{{round($ttpdtype,2)}}</span>
                            </td>
                            <td>
                                <span class="badge badge-pill badge-success font-size-14">{{round($qtecmd),2}}</span>
                            </td>
                            <td>
                                <span class="badge badge-pill badge-success font-size-14">{{round($ttpdtype-$qtecmd,2)}}</span>
                            </td>
                            <td>{{$produit->type->libelle}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>
@endsection




@section('other_js')

@endsection


@section('other_css')

@endsection
