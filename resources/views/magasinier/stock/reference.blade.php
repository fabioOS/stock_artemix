@extends('magasinier.layouts.app')

@section('page_title')
    Stock par reference
@stop

@section('content_title')
    Stock par reference
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item active">Stock par reference</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if ($data->count() > 0)
                <div class="table-responsive mb-4">
                    <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                        <thead>
                            <tr class="bg-transparent">
                                <th>Reference</th>
                                <th>Fournisseur</th>
                                <th>Fichier</th>
                                <th>Date</th>
                                <th style="width: 120px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $dt)
                                <tr class="font-size-16">
                                    <td>{{$dt->ref}}</td>
                                    <td>{{$dt->fournisseur->nom}}</td>
                                    <td><a href="{{asset('assets/uploads/stocks/'.$dt->file)}}" target="_blank" class="text-primary">Bon de livraison</a></td>
                                    <td>{{$dt->created_at->format('d/m/Y')}}</td>
                                    <td>
                                        <a href="{{route('magasinier.stock.reference.item',$dt->ref)}}" type="button" title="détail" class="btn btn-primary btn-sm waves-effect waves-light">
                                            <i class="fa fa-eye mr-2"></i> Détail
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end table -->
            @else
                <h5 class="text-warning"> Il n'y a aucun bon pour l'instant !</h5>
            @endif
        </div>
    </div>
    <!-- end row -->
@endsection




@section('other_js')

@endsection


@section('other_css')

@endsection
