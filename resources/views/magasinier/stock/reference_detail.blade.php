@extends('magasinier.layouts.app')

@section('page_title')
    Stock reference {{$reference->ref}}
@stop

@section('content_title')
    Stock reference {{$reference->ref}}
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li>
        <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Stock par reference</a></li>
        <li class="breadcrumb-item active">{{$reference->ref}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if ($data->count() > 0)
                <div class="table-responsive mb-4">
                    <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                        <thead>
                            <tr class="bg-transparent">
                                <th>Matière</th>
                                <th>Quantités</th>
                                <th>Unité de mésure</th>
                                <th>Date</th>
                                <th>Fournisseur</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $dt)
                                @php $typ = \App\Typeproduit::where('id',$dt->produits->type_id)->first(); @endphp
                                <tr class="font-size-16">
                                    <td>{{$dt->produits->libelle}}</td>
                                    <td>{{$dt->qte}}</td>
                                    <td>{{$typ ? $typ->libelle : ''}}</td>
                                    <td>{{$dt->created_at->format('d/m/Y')}}</td>
                                    <td>{{$reference->fournisseur->nom}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end table -->
            @else
                <h5 class="text-warning"> Il n'y a aucune matière pour l'instant !</h5>
            @endif
        </div>
    </div>
    <!-- end row -->

@endsection




@section('other_js')
    <script>
        // check last statut of dossier
        $('#order-selectinput').change(function(){
            $('#form-filtre').submit();
        });
    </script>
@endsection


@section('other_css')

@endsection
