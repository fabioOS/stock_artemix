@extends('service_achat.layout')

@section('page_title')
    Créer un bon de commande
@stop

@section('content_title')
    Créer un bon de commande
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        {{-- <li class="breadcrumb-item"><a href="{{url('./magasinier/')}}">Magasinier</a></li> --}}
        <li class="breadcrumb-item active">Créer un bon de commande</li>
    </ol>
@stop

@section('content')

{{-- @dd(Request::get('code')) --}}
<div class="row">
    <div class="col-lg-12">
        <form action="{{route('sabc.store')}}" class="" method="post" id="addappelfonds">
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">IDENTIFIANT BC</h4>
                    <div class="form-group">
                        <label for="">N° DEMANDE <span class="text-danger">*</span></label>
                        <select onchange="selectSt()" name="demande" id="demande" class="form-control select2" style="min-width: 300px;" required >
                            <option selected disabled>Sélectionner le bon de demande</option>
                            @foreach ($demandes as $demande)
                                <option value="{{$demande->id}}" data-demandeur="{{$demande->user->name}} {{$demande->user->prenom}}">{{$demande->ref}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">DEMANDEUR</label>
                        <input type="text" readonly id="demandeur" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">TYPE DE BON <span class="text-danger">*</span></label>
                        <select onchange="selectTypbc()" name="typebc" id="typebc" class="form-control select_plugin" style="min-width: 300px;" required>
                            <option selected disabled>Sélectionner un type</option>
                            <option value="1">A LIVRER</option>
                            <option value="2">A DECAISSER</option>
                        </select>
                    </div>

                    <div class="form-group" id="adecaisser" style="display: none">
                        <label for="">PRIORITER <span class="text-danger">*</span></label>
                        <select name="prioriter" id="prioriter" class="form-control">
                            <option selected disabled>Sélectionner une priorité</option>
                            <option value="FAIBLE">FAIBLE</option>
                            <option value="MOYEN">MOYEN</option>
                            <option value="URGENT">URGENT</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">FOURNISSEUR & MODE DE PAIEMENT</h4>

                    <div class="form-group">
                        <label for="">N° PROFORMA</label>
                        <input type="text" name="proforma" id="profoma" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">FOURNISSEUR <span class="text-danger">*</span></label>
                        <select name="fournisseur" id="fournisseur" class="form-control select2" style="min-width: 300px;" required >
                            <option selected disabled>Sélectionner le fournisseur</option>
                            @foreach ($fournisseurs as $fournisseur)
                                <option value="{{$fournisseur->id}}">{{$fournisseur->nom}}</option>
                            @endforeach
                        </select>
                        <p class="help-block font-italic">Si le fournisseur n'existe pas, veuillez le créer <a href="#" type="button" data-toggle="modal" data-target="#addProg">ici</a></p>
                    </div>

                    <div class="form-group">
                        <label for="">MODE DE PAIEMENT <span class="text-danger">*</span></label>
                        <select name="mode" id="mode" class="form-control select_plugin" style="min-width: 300px;" required>
                            <option selected disabled>Sélectionner un mode</option>
                            <option value="ESPECE">ESPECE</option>
                            <option value="CHEQUE">CHEQUE</option>
                            <option value="VIREMENT">VIREMENT</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="date">DATE PAIEMENT PROVISOIRE <span class="text-danger">*</span></label>
                        <input type="date" min="{{date('Y-m-d')}}" name="date" id="date" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="date">TVA <span class="text-danger">*</span></label>
                        <select onchange="selectva()" name="tva" id="tva" class="form-control" required>
                            <option selected disabled>Sélectionner une option</option>
                            <option value="AVEC">AVEC</option>
                            <option value="SANS">SANS</option>
                        </select>
                    </div>

                    <div class="form-group" id="retenu2" style="display: none">
                        <label for="">RETENU 2% <span class="text-danger">*</span></label>
                        <select name="retenu" id="retenu" class="form-control">
                            <option selected disabled>Sélectionner une priorité</option>
                            <option value="OUI">OUI</option>
                            <option value="NON">NON</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-5 text-primary">COMMANDE</h4>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12">Produits <span class="text-danger">*</span></label>
                                <select name="pdt" id="pdt" class="form-control select2">
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-md-12">Quantités <span class="text-danger">*</span></label>
                                <input type="number" min="1" id="qte" name="qte" class="form-control col-md-12" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12">Prix unitaire <span class="text-danger">*</span></label>
                                <input type="text" min="1" id="price" name="price" class="form-control col-md-12" required>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-md-12">Remise (%)</label>
                                <input type="number" min="1" id="remise" name="remise" class="form-control col-md-12">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-md-12" style="opacity: 0">Remise (%) </label>
                                <a onclick="add_element_to_array()" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-plus"></i> AJOUTER</a>
                            </div>
                        </div>

                        <div class="col-md-12 mt-3">
                            <input type="hidden" id="varieMontant">
                            <table class="table metable" id="data_session">
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="loader" class="dispayNone">
                    <div class="spinner-border text-primary m-1" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Enregistrer</button>
            </div>
        </form>

    </div>
</div>

<!-- modal update programme -->
<div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title mt-0" id="updateProglLabel">Ajouter le fournisseur</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form enctype="multipart/form-data" action="{{route('sa.fours.store')}}" method="post">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label>Nom <span class="text-danger">*</span></label>
                    <input type="text" name="nom" class="form-control" required="" value="">
                </div>

                <div class="form-group">
                    <label>Contact <span class="text-danger">*</span></label>
                    <input type="text" name="contact" class="form-control" required="" value="">
                </div>

                <div class="form-group">
                    <label>Adresse</label>
                    <input type="text" name="adresse" class="form-control" value="">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
            </div>
        </form>

    </div><!-- /.modal-content -->
    </div><!-- /.modal add programme-->
</div>

@endsection

@section('other_css')
{{-- <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
@endsection

@section('other_js')
<script src="{{ asset('assets/js/imask.js') }}"></script>
{{-- <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script> --}}
    <script>
        var valueSelect = '',
            inputs = [],
            arr = [],
            valuemontantt = 0,
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();

            IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' ',
                scale: 6,
            });

            var seach = "{{Request::get('code')}}";

            if(typeof seach !== 'undefined' ){
                //alert('ok 0');
                @if($check)
                    //alert("{{$check->id}}");

                    $('#demande').empty();
                    var optionData = '';
                    var selected = '';
                    optionData+='<option disabled>Sélectionner le bon de demande</option>';
                    //$("#demande > option[value='{{$check->id}}']").prop('selected', 'selected');
                    @foreach($demandes as $k=>$dmd)
                        selected = "{{$dmd->id == $check->id ? 'selected' : ''}}"
                        optionData+='<option value="{{$dmd->id}}" '+selected+' data-typepdt="{{$dmd->user->name}} {{$dmd->user->prenom}}">{{$dmd->ref}}</option>';
                    @endforeach

                    $('#demande').html(optionData);

                    //alert('ddd 2');
                    $("#demandeur").val("{{$check->user->name}} {{$check->user->prenom}}");

                    $('#data_session').empty();
                    $('#qte').val("");

                    var idarrond = {{$check->id}};
                    var url ="{{ route('sabc.get', ':id')}}";
                    url=url.replace(':id',idarrond);

                    SendGet(url);

                @endif
            };

        });

        function selectTypbc(){
            //$("#demandeur").empty();
            var selctbc = $("#typebc").val();
            if(selctbc == '2'){
                $('#adecaisser').show();
            }else{
                $('#adecaisser').hide();
            }
        }

        function selectva(){
            var selctva = $("#tva").val();
            if(selctva == 'SANS'){
                $('#retenu2').show();
            }else{
                $('#retenu2').hide();
            }
        }

        function selectSt(){
            //$("#demandeur").empty();
            var actiflibel = $("#demande").find(':selected').data('demandeur');
            $("#demandeur").val(actiflibel);

            $('#data_session').empty();
            $('#qte').val("");

            var idarrond = $('#demande :selected').val();
            var url ="{{ route('sabc.get', ':id')}}";
            url=url.replace(':id',idarrond);

            SendGet(url);
        }

        function SendGet(url){
            $.get(url, function (data) {
                //console.log(data);
                optionData ='';
                optionData+='<option>Choisir une matiere</option>';
                for (var i = 0; i < data.length; i++){
                    if(data[i].show == 1){
                        optionData+='<option value="'+data[i].id+'" data-pdtpx="'+data[i].programprice+'" data-typepdt="'+data[i].produit.type.libelle+'" data-libelle="'+data[i].produit.libelle+'" data-maxqte="'+data[i].maxcmd+'">'+data[i].produit.libelle+'</option>';
                    }
                }
                $('#pdt').html(optionData);
            });
        }

        //si #pdt change on ajoute le prix unitaire dans le input #price
        $('#pdt').change(function(){
            var price = $(this).find(':selected').attr('data-pdtpx');
            $('#price').val(price);
            var maxqte = $(this).find(':selected').attr('data-maxqte');
            $('#qte').val(maxqte);
        });

        function add_element_to_array(){
            var qtite = $("#qte").val();
            var priceold = $("#price").val();
            var remise = $("#remise").val();
            var price = parseFloat(priceold.replace(/\s/g,'').replace(',', '.'));
            // var price = priceold.replace(/\s/g, '');
            //console.log(priceold);
            //console.log(price);

            var qtiterest = 0;
            var produits = $("#pdt").val();
            //console.log(produits);

            if(qtite == "" || qtite<1){
                swal("Oups!", "Veuillez renseigner la quantité", "error");
                return false ;
            }else if(produits == ""){
                swal("Oups!", "Veuillez choisir un produit ", "error");
                return false ;
            }else if(price == ""){
                swal("Oups!", "Veuillez renseigner le prix unitaire ", "error");
                return false ;
            }else if(parseInt(remise) > 100){
                swal("Oups!", "Veuillez renseigner une remise en dessous ou égale à 100", "error");
                return false ;
            } else{
                var valueSelect = $('#pdt');
                var selectmax = valueSelect.find(':selected').attr('data-maxqte');
                //console.log("qtte " +qtite);
                //console.log("qtte max " +selectmax);

                if(parseInt(qtite) > parseInt(selectmax)){
                    swal("Oups!", "La quantité maximum a commandé est de "+selectmax+".", "error");
                    return false ;
                }

                var selectId = valueSelect.val(),
                    pjtid = $('#demande').val(),
                    selectLibellType = valueSelect.find(':selected').attr('data-typepdt'),
                    selectLibell = valueSelect.find(':selected').attr('data-libelle');

                if(remise=='' || remise==0){
                    remise = 0;
                }
                //console.log("remise "+ remise);
                var request = getQteDispo(qtite,selectId,pjtid);

                $.when(request).done(function(data) {
                    //console.log(data);
                    if(data == "error"){
                        swal("Oups!", "Une erreur s'est produite, veuillez recommencez ", "error");
                        return false ;
                    }else if (data != 'ok' && data != 'error'){
                        swal("Oups!", "Quantité restante à commander : "+data , "error");
                    }else{
                        //Check l'existance du item
                        var checkexit = inputs.filter(element => {
                            return element.id == selectId
                            // return element.selectId == selectId && element.proprios == proprios
                        })

                        if(checkexit.length==0){
                            var pxht = price - ((remise /100) * price),
                                mttble = pxht * qtite;
                            valuemontantt = valuemontantt + parseFloat(mttble);

                            arr = {id:selectId, libelletype:selectLibellType, libelle:selectLibell, qtecmde:qtite, price:price, remise:remise, mttt:mttble};
                            inputs.push(arr);
                            showhtml();
                            $('#mttotal').html(Number(valuemontantt).toLocaleString('fr-FR') +' Fcfa');
                            // $('#mttotal').html(valuemontantt.toLocaleString()+' Fcfa');
                        }else{
                            swal("Oups!", "Cette ligne existe déjà.", "error");
                            return false ;
                        }


                    }
                });
            }
        }

        function getQteDispo(quantite,idbstock,idpjt) {

            var url ="{{ route('sabc.get.dispo',[':qte',':itempdt',':cmd'])}}";
            url=url.replace(':qte',quantite);
            url=url.replace(':itempdt',idbstock);
            url=url.replace(':cmd',idpjt);

            return $.ajax({url:url, type:'GET'});
        }

        function showhtml() {
            $("#data_session").empty();
            var table = "<tbody>";
            //console.log(inputs);
            table += '<tr class="bg-primary text-white">';
            table += '<td class="border-b">NUM</td>';
            table += '<td class="border-b text-lg font-medium">DESIGNATION</td>';
            table += '<td class="border-b text-lg font-medium">U</td>';
            table += '<td class="border-b text-lg font-medium">QUANTITES</td>';
            table += '<td class="border-b text-lg font-medium">P.U</td>';
            table += '<td class="border-b text-lg font-medium">R (%)</td>';
            table += '<td class="border-b text-lg font-medium">PRIX U.HT</td>';
            table += '<td class="border-b text-lg font-medium">MONTANT</td>';
            table += '<td class="border-b">ACTION</td>';
            table += '</tr>';

            for (var i = 0; i < inputs.length; i++) {
                var y = i + 1;
                // console.log(inputs[i].price);
                var pxht = inputs[i].price - ((inputs[i].remise /100) * inputs[i].price),
                    mttble = pxht * inputs[i].qtecmde,
                    pxhtformat = Number(pxht).toLocaleString('fr-FR'),
                    mttbleformat = Number(mttble).toLocaleString('fr-FR'),
                    priceformat = Number(inputs[i].price).toLocaleString('fr-FR');

                table += '<tr data-row-id='+i+'>';
                table += '<td class="border-b">' + y + '</td>';
                table += '<td class="border-b">' + inputs[i].libelle + '</td>';
                table += '<td class="border-b">' + inputs[i].libelletype + '</td>';
                table += '<td class="border-b">' + inputs[i].qtecmde + '</td>';
                table += '<td class="border-b">' + priceformat + '</td>';
                table += '<td class="border-b">' + inputs[i].remise + '</td>';
                table += '<td class="border-b">' + pxhtformat + '</td>';
                table += '<td class="border-b">' + mttbleformat + '</td>';
                table += '<td class="border-b"><a style="cursor: pointer" onclick="deleteSession('+i+','+inputs[i].mttt+')" class="inline-block text-1xl text-orange-500 mx-2" title="Supprimer la ligne"><i data-feather="trash" class="w-4 h-4 mr-2"></i> Supprimer</a></div>';
                table +='<input type="hidden" value="' + inputs[i].id + ';' + inputs[i].libelle + ';' + inputs[i].libelletype + ';' + inputs[i].qtecmde + ';' + inputs[i].price +';' + inputs[i].remise +'" name="datasession[]">';
                table += '</tr>';
            };

            table += '<tr class="mt-3">';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b text-lg font-medium">Montant Total HT</td>';
            table += '<td class="border-b text-lg font-medium" id="mttotal"></td>';
            table += '<td class="border-b"></td>';
            table += '</tr>';
            table += '</tbody>';

            $("#data_session").append(table);
        }

        function deleteSession(id,mtt) {
            valuemontantt = valuemontantt - mtt;
            inputs.splice(id,1);
            showhtml();
            $('#mttotal').empty();
            // $('#mttotal').html(valuemontantt.toLocaleString()+' Fcfa');
            $('#mttotal').html(Number(valuemontantt).toLocaleString('fr-FR') +' Fcfa');

        }

        function submitForm() {
            event.preventDefault();
            if($('#demande').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Demande.");
            }else if($('#typebc').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Type de bon.");
            }else if($('#fournisseur').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Fournisseur.");
            }else if($('#mode').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Mode.");
            }else if($('#date').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Date.");
            }else{
                //var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de faire un bon de commande ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#loader').show();
                        document.getElementById('addbtn').disabled = true;
                        $('#addbtn').hide();
                        document.getElementById('addappelfonds').submit();
                    }
                });
            }

        }

    </script>

@endsection
