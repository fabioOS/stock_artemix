@extends('service_achat.layout')

@section('page_title')
    Détails du bon de commande
@stop

@section('content_title')
Bon de commande N°  <span class="text-primary">{{strtoupper($demande->ref)}}</span>
    @if ($demande->status == 1)
        <span class="badge badge-pill badge-warning">En attente</span>
    @endif
    @if ($demande->status == 2)
        <span class="badge badge-pill badge-info">Validé</span>
    @endif
    @if ($demande->status == 3)
        <span class="badge badge-pill badge-success">Livré</span>
    @endif
    @if ($demande->status == 4)
        <span class="badge badge-pill badge-danger">Rejété</span>
    @endif
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails du bon de commande</li>
    </ol>
@stop

@section('content')

    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">BC N°</h5>
                            <p>{{strtoupper($demande->ref)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Agent</h5>
                            <p>{{strtoupper($demande->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Fournisseur</h5>
                            <p>{{$demande->fournisseur->nom}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Montant total</h5>
                            <p>@price($demande->montant - $demande->retenu??0) Fcfa</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de commande</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->created_at)->format('d/m/Y')}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            @if($demande->status == 1)
                                <div class="row gstock">
                                    {{-- <div class="mr-1">
                                        <a href="{{route('sabc.show.pdf',encrypt($demande->ref))}}" class="btn btn-info" title="Imprimer le bon"> <i class="fa fa-print"></i> </a>
                                    </div> --}}
                                    {{-- <div class="mr-1">
                                        <a href="{{route('cpcommande.valid',encrypt($demande->id))}}" class="btn btn-success valid" title="Valider la demande"> <i class="fa fa-check-circle"></i> </a>
                                    </div> --}}
                                    {{-- <div>
                                        <button type="button" data-toggle="modal" data-target="#rejectbon" class="btn btn-danger" title="Rejeter la demande"> <i class="fa fa-trash"></i> </button>
                                    </div> --}}
                                </div>
                            @elseif($demande->status== 2)
                                <div class="row gstock">
                                    <div class="mr-1">
                                        <a target="_blank" href="{{route('sabc.show.pdf',encrypt($demande->ref))}}" class="btn btn-info" title="Imprimer le bon"> <i class="fa fa-file-pdf"></i> </a>
                                        @if($demande->ibution_id)
                                            <a target="_blank" href="{{route('sa.transversale.pdf',encrypt($demande->id))}}" class="btn btn-dark" title="Imprimer le bon réa"> <i class="fa fa-file-pdf"></i> </a>
                                        @endif
                                    </div>
                                </div>
                            @elseif ($demande->status== 4)
                                <div class="mr-1">
                                    <a href="{{route('sabc.delete',encrypt($demande->id))}}" class="btn btn-danger" title="Supprimer le BC"> <i class="fa fa-trash"></i> </a>
                                </div>
                            @endif
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Date de paiement</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->datepaiement)->format('d/m/Y')}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">Mode paiement</h5>
                            <p>{{$demande->mode}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">N° proforma</h5>
                            <p>{{$demande->proforma ?? '-'}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">TVA</h5>
                            <p>{{$demande->tva ?? '-'}}</p>
                        </div>

                        <div class="text-muted col-lg-4">
                            <h5 class="font-size-16">RETENUE 2%</h5>
                            <p>{{$demande->retenu ? 'OUI' :'NON'}}</p>
                        </div>

                        <div class="text-muted col-lg-2">
                            <h5 class="font-size-16">N° Commande</h5>
                            <p><a target="_blank" href="{{route('sacommande.show', encrypt($demande->demande->id))}}">{{$demande->demande->ref}}</a></p>
                        </div>

                        @if($demande->demande->programmesous_id != null)
                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Sous-programme</h5>
                            <p>{{strtoupper($demande->demande->programmesous->libelle)}}</p>
                        </div>@endif

                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16">Motif de la commande</h5>
                            <p>{!! nl2br($demande->demande->description) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($demande->status== 4)
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5 class="font-size-16">Motif</h5>
                            <p>{{$demande->motif}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if($demande->bonreception == null && Auth::user()->id == $demande->user_id && $demande->tva == "AVEC" && $demande->ibution_id == null )
        <div class="col-xl-12 gstock">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5 class="font-size-16">Réattribution</h5>
                            <a href="{{route('sa.transversale',encrypt($demande->id))}}" class="btn btn-primary float-left delete">Réattribuer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="col-xl-12">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Matières commandées </h5>
                </div>
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr style="background-color: #c6baba2b;">
                                    <td>Num</td>
                                    <td>Code</td>
                                    <td>Matière</td>
                                    <td>U</td>
                                    <td>Quantité</td>
                                    <td>P.U</td>
                                    <td>Remise</td>
                                    <td>Prix U.HT</td>
                                    <td>Montant</td>
                                </tr>
                                @foreach ($demande->items as $i=>$item)
                                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                                    {{-- @dump((($item->remise /100) * $item->price)) --}}
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td>{{$pdt->code}}</td>
                                        <td><b>{{$pdt->libelle}}</b></td>
                                        <td>{{$pdt->type->libelle}}</td>
                                        <td>{{$item->qte}} </td>
                                        <td>@price($item->price)</td>
                                        <td>{{$item->remise}} %</td>
                                        <td>@price($item->price - (($item->remise /100) * $item->price))</td>
                                        <td>@price($item->mtt)</td>
                                    </tr>
                                @endforeach
                                <tr><td colspan="8"></td></tr>

                                <tr>
                                    <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL HT</td>
                                    <td class="font-weight-bold">@price($demande->montant)</td>
                                </tr>
                                @if($demande->tva == "AVEC")
                                <tr>
                                    <td colspan="8" class="text-right font-weight-bold">TVA 18%</td>
                                    {{-- <td class="font-weight-bold">{{($demande->montant *0.18)}}</td> --}}
                                    <td class="font-weight-bold">@price(($demande->montant *0.18))</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL TTC</td>
                                    {{-- <td class="font-weight-bold">{{($demande->montant *0.18)+$demande->montant}}</td> --}}
                                    <td class="font-weight-bold">@price(($demande->montant *0.18)+$demande->montant)</td>
                                </tr>
                                @else
                                    @if($demande->retenu)
                                    <tr>
                                        <td colspan="8" class="text-right font-weight-bold">RETENUE 2%</td>
                                        <td class="font-weight-bold">@price($demande->retenu)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL TTC</td>
                                        <td class="font-weight-bold">@price($demande->montant-$demande->retenu)</td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td colspan="8" class="text-right font-weight-bold">MONTANT TOTAL TTC</td>
                                        <td class="font-weight-bold">@price($demande->montant)</td>
                                    </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){
        $('.gstock').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment effectuer une réattribution ?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });
    });
</script>
@endsection
