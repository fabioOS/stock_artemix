@extends('service_achat.layout')

@section('page_title')
    Bon de commande validé
@stop

@section('content_title')
    Bon de commande validé
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de commande validé</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($bcs) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list gstock" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>N°</th>
                            <th>BC N°</th>
                            <th>Agent</th>
                            <th>Bon demande</th>
                            <th>Fournisseur</th>
                            <th>Montant total</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th style="width: 120px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bcs as $k=>$bc)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="{{route('sabc.show', encrypt($bc->id))}}" class="text-primary font-weight-bold">{{$bc->ref}}</a> </td>
                                <td>{{$bc->user->name}}</td>
                                <td><a target="_blank" href="{{route('sacommande.show', encrypt($bc->demande->id))}}" class="">{{$bc->demande->ref}}</a></td>
                                <td>{{$bc->fournisseur->nom}}</td>
                                <td>@price($bc->montant - $bc->retenu ??0) Fcfa</td>
                                <td>
                                    <span class="badge badge-pill badge-success">Validé</span>
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($bc->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="{{route('sabc.show', encrypt($bc->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                    <a href="{{route('sabc.show.pdf',encrypt($bc->ref))}}" target="_blank" class="btn btn-sm btn-info" title="Imprimer le bon"> <i class="fa fa-file-pdf"></i> </a>
                                    @if($bc->bonreception == null && Auth::user()->id == $bc->user_id)
                                    <a href="{{route('sabc.delete', encrypt($bc->id))}}" class="btn btn-danger btn-sm waves-effect waves-light delete" title="Supprimer"><i class="fa fa-trash"></i></a>
                                    @endif
                                    @if($bc->ibution_id)
                                    <a href="{{route('sa.transversale.pdf',encrypt($bc->id))}}" target="_blank" class="btn btn-sm btn-dark" title="Imprimer le bon réa"> <i class="fa fa-file-pdf"></i> </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucun bon de commande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){
        $('.gstock').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet bon de commande?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

    });
</script>
@endsection
