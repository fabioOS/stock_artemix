@extends('service_achat.layout')

@section('page_title')
    Bon de reception conforme
@stop

@section('content_title')
    Bon de reception conforme
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Bon de reception conforme</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($bons) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>BC N°</th>
                            <th>Agent</th>
                            <th>Bon de commande</th>
                            <th>Fournisseur</th>
                            <th>Nombre de produit</th>
                            <th>Date</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bons as $bon)
                            <tr class="font-size-16">
                                <td><a href="!#" class="text-dark font-weight-bold">{{$bon->ref}}</a> </td>
                                <td>{{$bon->user->name}}</td>
                                <td><a target="_blank" href="{{route('sabc.show', encrypt($bon->bc->id))}}" class="">{{$bon->bc->ref}}</a></td>
                                <td>{{$bon->fournisseur->nom}}</td>
                                <td>{{$bon->items->count()}}</td>
                                <td>{{Illuminate\Support\Carbon::parse($bon->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="{{route('sabr.show', encrypt($bon->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Détails"><i class="uil uil-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucun bon pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
