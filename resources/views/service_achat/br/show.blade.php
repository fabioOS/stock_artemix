@extends('service_achat.layout')

@section('page_title')
    Détails du bon de reception
@stop

@section('content_title')
    Détails du bon de reception
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails du bon de reception</li>
    </ol>
@stop

@section('content')

<div class="row mb-4">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">BR N°</h5>
                        <p>{{strtoupper($bon->ref)}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Agent</h5>
                        <p>{{strtoupper($bon->user->name)}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Bon de commande</h5>
                        <p><a target="_blank" href="{{route('sabc.show', encrypt($bon->bc->id))}}">{{$bon->bc->ref}}</a></p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Fournisseur</h5>
                        <p>{{$bon->fournisseur->nom}}</p>
                    </div>

                    <div class="text-muted col-lg-2">
                        <h5 class="font-size-16">Date de reception</h5>
                        <p>{{Illuminate\Support\Carbon::parse($bon->created_at)->format('d/m/Y')}}</p>
                    </div>

                    <div class="text-muted col-lg-4">
                        <h5 class="font-size-16">N° BON DE LIVRAISON</h5>
                        <p>{{$bon->nbl}}</p>
                    </div>

                    <div class="text-muted col-lg-4">
                        <h5 class="font-size-16">BON DE LIVRAISON</h5>
                        @if(!empty($bon->file))
                        <p><a href="{{asset('assets/uploads/stocks/'.$bon->file)}}" target="_blank" download>bon_de_livraison.pdf</a></p>
                        @else
                        -
                        @endif
                    </div>

                    @if($bon->conform == 1)
                    <div class="text-muted col-lg-4">
                        <div class="row gstock" id="btnaction">
                            <div class="">
                                <a href="{{route('sabr.confirm',encrypt($bon->id))}}" class="btn btn-success valid" title="Confirmer la conformité"> <i class="fa fa-edit"></i> Confirmer la conformité</a>
                            </div>
                        </div>
                        <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card border shadow-none">
            <div class="card-header bg-transparent border-bottom py-3 px-4">
                <h5 class="font-size-16 mb-0">Matières livrées </h5>
            </div>
            <div class="card-body p-4">

                <div class="table-responsive">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>Num</td>
                                <td>Matière</td>
                                <td>Unité</td>
                                <td class="text-right">Quantité commandé</td>
                                <td class="text-right table-success">Quantité livré</td>
                                <td class="text-right">Quantité restant à livrer</td>
                            </tr>
                            @foreach ($bon->bc->items as $i=>$item)
                            @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first();
                                $britem = \App\StockFournisseurItem::where('stock_fournisseur_id',$bon->id)->where('produit_id',$item->produit_id)->first();
                                $qtelivre = $britem? $britem->qte : 0;
                            @endphp
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td><b>{{$pdt->libelle}}</b></td>
                                    <td>{{$pdt->type->libelle}}</td>
                                    <td class="text-right">{{$item->qte}} </td>
                                    <td class="text-right">{!! $qtelivre >= $item->qte ? '<span class="badge badge-success">'.$qtelivre.'</span>' : '<span class="badge badge-warning">'.$qtelivre.'</span>' !!} </td>
                                    <td class="text-right">{{$item->qte - $qtelivre}} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end table-responsive -->
            </div>
        </div>
    </div>
</div>
@endsection


@section('other_js')

<script>
    $(document).ready(function(){

        $('.gstock').on('click', '.valid', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment effectuer cette action ?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#btnaction').hide();
                    $('#loader').show();
                    window.location = href;
                }
            });
        });

});
</script>

@endsection
