@extends('service_achat.layout')

@section('page_title')
    Commandes à mettre à jour
@stop

@section('content_title')
    Commandes à mettre à jour
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Commandes à mettre à jour</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (count($commandes) > 0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>N°</th>
                            <th>N° Référence</th>
                            <th>Demandeur</th>
                            <th>Nombre de matière</th>
                            <th>Date</th>
                            <th style="width: 200px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($commandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="{{route('sacommande.show', encrypt($dt->id))}}" class="text-primary font-weight-bold">{{$dt->ref}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{count($dt->produits)}}</td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="{{route('sacommande.show', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Mettre à jour la ligne budgetaire"><i class="uil uil-eye"></i> MAJ ligne budgetaire</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
