@extends('service_achat.layout')

@section('page_title')
    Détails demande de commande
@stop

@section('content_title')
Commande N°  <span class="text-primary">{{strtoupper($demande->ref)}}</span>
{{-- <span class="badge badge-pill badge-warning">En attente de traitement</span> --}}
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Détails de la commande</li>
    </ol>
@stop

@section('content')

    <div class="row mb-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Référence</h5>
                            <p>{{strtoupper($demande->ref)}}</p>
                        </div>

                        @if($demande->programmesous_id != null)
                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Sous-programme</h5>
                            <p>{{strtoupper($demande->programmesous->libelle)}}</p>
                        </div>@endif

                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Demandeur</h5>
                            <p>{{strtoupper($demande->user->name)}}</p>
                        </div>

                        <div class="text-muted col-lg-3">
                            <h5 class="font-size-16">Date de la commande</h5>
                            <p>{{Illuminate\Support\Carbon::parse($demande->created_at)->format('d/m/Y')}}</p>
                        </div>

                        <div class="text-muted col-lg-3">
                            <div class="row gstock">
                                <div class="">
                                    <a href="{{route('sabc.create')}}?code={{$demande->id}}" class="btn btn-success valid" title="Etablir un BC"> <i class="fa fa-edit"></i> Etablir un bon de commande</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16">Motif de la commande</h5>
                            <p>{!! $demande->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="text-muted col-lg-12">
                            <h5 class="font-size-16">Ligne budgetaire</h5>
                            @php $lignb = $demande->lignebudget @endphp
                            @if($lignb)
                            <p>{{strtoupper($lignb->libelle)}}
                                @if($lignb->hors_ds == 1)
                                    <span class="badge badge-pill badge-warning">Hors DS</span>
                                @elseif ($lignb->type == 1)
                                    <span class="badge badge-pill badge-primary">Batiments</span>
                                @elseif ($lignb->type == 2)
                                    <span class="badge badge-pill badge-dark">VRD</span>
                                @else
                                    <span class="badge badge-pill badge-success">-</span>
                                @endif

                                @if($demande->datalignebudget)
                                <div class="progress mt-1" style="height: 20px;">
                                    <div class="progress-bar progress-bar bg-danger" role="progressbar" style="width: {{$demande->datalignebudget->original['pcent']}}%" aria-valuenow="{{$demande->datalignebudget->original['pcent']}}" aria-valuemin="0" aria-valuemax="{{$demande->datalignebudget->original['pcent']}}">
                                        @price($demande->datalignebudget->original['pcent']) %
                                    </div>
                                </div>
                                <span class="card-title font-weight-bolder text-{{$demande->datalignebudget->original['color']}}">@price($demande->datalignebudget->original['consommer']) / @price($demande->lignebudget->montant)</span>
                                @endif
                            </p>
                            @else
                            @php $lignbudgets = App\Programme::where('id',$demande->programme_id)->with('ligneBudgetaires')->first(); @endphp
                                <h4 class="card-title mb-3 text-primary">MAJ LIGNE BUDGETAIRE</h4>
                                <form id="maj_lign" method="POST">
                                    @csrf
                                    <input type="hidden" name="cmdid" value="{{encrypt($demande->id)}}">
                                    <div class="form-group">
                                        <label for="">SELECTIONNEZ LA LIGNE <span class="text-danger">*</span></label>
                                        <select name="lignid" id="lignid" class="form-control select2" required>
                                            <option selected disabled>Sélectionner une ligne</option>
                                            @foreach($lignbudgets->ligneBudgetaires as $item)
                                            <option value="{{$item->id}}">{{$item->libelle}}</option>
                                            @endforeach
                                            {{-- <option value="999">AUTRE</option> --}}
                                        </select>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-12">
            <div class="card border shadow-none">
                <div class="card-header bg-transparent border-bottom py-3 px-4">
                    <h5 class="font-size-16 mb-0">Matières demandées </h5>
                    @if($showcloturer)
                    <div class="float-right gstock">
                        <a href="{{route('sabc.cloture')}}?code={{encrypt($demande->id)}}" class="btn btn-sm btn-danger cloture" title="Clôturer cette commande"> <i class="fa fa-check-square"></i> Clôturer cette commande</a>
                    </div>@endif
                </div>
                <div class="card-body p-4">

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr style="background-color: #c6baba2b;">
                                    <td>Code</td>
                                    <td>Matière</td>
                                    <td class="text-right table-success">Qte demandée</td>
                                    <td class="text-right">Qte totale commandée</td>
                                </tr>
                                @foreach ($demande->produits as $item)
                                @php $pdt = \App\Produit::with('type')->where('id',$item->produit_id)->first() @endphp
                                {{-- @dump($item) --}}
                                    <tr>
                                        <td>{{$pdt->code}}</td>
                                        <td><b>{{$pdt->libelle}}</b> <sup class="text-primary">{{$pdt->type->libelle}}</sup></td>
                                        <td class="text-right text-success"> {{$item->qte}} </td>
                                        @php
                                            $nb =\App\BCitem::where('commande_item_id',$item->id)->where('produit_id',$item->produit_id)
                                                ->join('bon_commandes', 'bon_commandes.id', '=', 'bon_commandes_items.bon_commande_id')
                                                ->where('bon_commandes.status',2)
                                                ->sum('qte')
                                        @endphp
                                        <td class="text-right"><span class="{{$nb>=$item->qte ? 'text-success' : 'text-warning'}}">{{$nb}}</span></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

@endsection


@section('other_js')
<script>
    $(document).ready(function(){
        $('.gstock').on('click', '.cloture', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment clôturer cette commande?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    console.log(href);
                    //mettre un style de chargement sur le bouton
                    $(this).html('<i class="fa fa-spinner fa-spin"></i> Clôture en cours ...');
                    $(this).attr('disabled', 'disabled');
                    window.location = href;
                }
            });
        });

    });

    $("#lignid").change(function(){
        var val = $(this).val();
        if(!val){
            return swal('Erreur', 'Veuillez selectionner une ligne', 'error');
        }else{
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment changer la ligne budgétaire de cette commande?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $('#maj_lign').attr('action', '{{route('sabc.majlign')}}');
                    $('#maj_lign').submit();
                }
            });
        }
    });
</script>
@endsection
