@extends('service_achat.layout')

@section('page_title')
    Créer une demande
@stop

@section('content_title')
Créer une demande
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demande spéciale</li>
    </ol>
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card mb-5">
            <div class="card-body">
                <form id="DemForm" autocomplete="off" action="{{route('storeDmdSpecial')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="titre">Titre <span class="text-danger">*</span></label>
                            <input type="text" name="titre" class="form-control" id="titre">
                        </div>

                        <div class="form-group">
                            <label for="montant">Montant <span class="text-danger">*</span></label>
                            <input type="text" name="montant" class="form-control" id="montant">
                        </div>

                        <div class="form-group">
                            <label for="des">Motif <span class="text-danger">*</span></label>
                            <textarea name="des" id="des" class="form-control" cols="30" rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="file">Fichier (optionnel)</label>
                            <input type="file" name="file" class="form-control" id="file">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>

                        <div id="sectbtn">
                            <button id="addbtn" onclick="sendForm()" class="btn btn-primary waves-effect waves-light mr-1">Ajouter</button>
                            <button type="reset" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@section('other_js')
<script src="{{ asset('assets/js/imask.js') }}"></script>

<script>

    $(document).ready(function(){
        IMask(document.getElementById('montant'), {
            mask: Number,
            min: 1,
            max: 100000000000,
            thousandsSeparator: ' '
        });

    });

    function sendForm() {
        var titre = document.getElementById('titre').value;
        var montant = document.getElementById('montant').value;
        var des = document.getElementById('des').value;

        if(!titre){
            swal("Oups!", "Veuillez renseignez le champs Titre");
            return false;
        }else if(!montant){
            swal("Oups!", "Veuillez renseignez le champs Montant");
            return false;
        }else if(!des){
            swal("Oups!", "Veuillez renseignez le champs Motif");
            return false;
        }else{
            document.getElementById('addbtn').disabled = true;
            $('#sectbtn').hide();
            $('#loader').show();
            document.getElementById('DemForm').submit();
        }
    }

</script>
@endsection
