<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

    .card {
        display: inline-block;
        width: 100%;
        margin-bottom: 2px;
        box-shadow: 0 2px 4px rgba(15,34,58,.12);
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid #f6f6f6;
        border-radius: .25rem;
        font-size: 12px;
    }

    .card-body {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-height: 1px;
        /*padding: 1.25rem;*/
    }

    #demandes{
        /* margin-top: 30px; */
        /* border: 1px solid; */
    }

    #head td {
        border: 1px solid #000;
        text-align: center;
        padding: 2px;
        font-size: 12px;
    }

    #tbodydmd td {
        border: 1px solid #000;
        padding: 2px;
        text-align: center;
        font-size: 12px;
    }

    #cdtion{
        /* margin-top: 10px; */
    }

    .text-right{
        text-align: right;
    }

    #tbodydmd td {
        border: 1px solid #000;
    }

</style>
<body style="border: 1px solid;">
<div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="col">
                            <img src="{{asset('assets/images/logoartemis.png')}}" data-holder-rendered="true" height="50px" />
                        </div>
                    </td>
                </tr>
            </table>

            <hr>

            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>DEMANDE SPECIALE N° : </td>
                                <td>{{$data->slug}}</td>
                            </tr>
                            <tr>
                                <td>Date : </td>
                                <td>{{$data->created_at->format('d/m/Y')}}</td>
                            </tr>
                            <tr>
                                <td>Chantier: </td>
                                <td>{{$data->programme->libelle}}</td>
                            </tr>

                            <tr>
                                <td>Motif: </td>
                                <td>{!! nl2br($data->description) !!}</td>
                            </tr>
                            @if($data->status!=2)
                            <tr>
                                <td>STATUS: </td>
                                <td style="color: red">EN ATTENTE DE VALIDATION</td>
                            </tr>
                            @endif
                        </table>
                    </td>
                </tr>
            </table>

            <br><br>

            <table border="1" cellspacing="0" cellpadding="0" width="100%" id="demandes" style="border: 1px solid">
                <thead>
                    <tr id="head">
                        <td>Désignation</td>
                        <td>Montant</td>
                    </tr>
                </thead>
                <tbody id="tbodydmd">
                    <tr>
                        <td><b>{{$data->titre}}</b></td>
                        <td>@price($data->montant)</td>
                    </tr>
                </tbody>
            </table>

            <br> <br><br>

            <table border="0" cellspacing="0" cellpadding="6" width="100%" id="" style="border: 1px solid;">
                <tr>
                    <td>Nom du demandeur :</td>
                    <td>{{$data->user ? $data->user->name :'N/A'}}</td>
                </tr>
            </table>
            <br>
            <table border="0" cellspacing="0" cellpadding="6" width="100%" id="" style="border: 1px solid;text-align:center">
                <tr>
                    <td>
                        Abidjan Cocody Riviera Jardin, 08 BP 2553 ABIDJAN 08, ABJ-03-2022-M-40228, CC I 1631155W, Capital social: 450 000 000
                        FCFA, Régime d'imposition : Réel Normal d'imposition, BRIDGE BANK C1131 01009 018751820005 63, Tel : 00225 27 21 32 96 90 / 00225 07 87 33 12 03
                    </td>
                </tr>
            </table>
        </tbody>
        {{-- <tfoot style="vertical-align:bottom">
            <tr style="height:100px">
                <td>Abidjan Cocody Riviera Jardin, 08 BP 2553 ABIDJAN 08, RCCM I CI-ABJ-2019-M-15316, CC I 1631155W, Capital social: 1 000 000
                    FCFA, Régime d'imposition : Réel simplifié, BRIDGE BANK C1131 01009 018751820005 63, Tel : 00225 21 32 96 90 / 00225 87 33 12 03
                </td>
            </tr>
        </tfoot> --}}
    </table>
</div>

</body>
</html>
