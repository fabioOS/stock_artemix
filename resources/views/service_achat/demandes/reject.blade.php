@extends('service_achat.layout')

@section('page_title')
    Demandes spéciales rejetées
@stop

@section('content_title')
    Demandes spéciales rejetées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciales rejetées</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if(count($demandes)>0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th style="width: 100px">Montant</th>
                            <th>Motif de demande</th>
                            <th>Statut</th>
                            <th>Motif de rejet</th>
                            <th>Date</th>
                            {{-- <th style="width: 100px;">Action</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="#" class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>@price($dt->montant)</td>
                                <td>{!! nl2br($dt->description) !!}
                                    @if($dt->preuve != null)<br>
                                    <a href="{{asset('assets/uploads/demandespeciale/'.$dt->preuve)}}" target="_blank">Voir Pièce-Jointe</a>@endif
                                </td>
                                <td>
                                    <span class="badge badge-pill badge-danger">Rejété</span>
                                </td>
                                <td>{!! nl2br($dt->motif) !!}</td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                {{-- <td>
                                    @if($dt->status == 4 OR $dt->status == 1)
                                        <button type="button" class="btn btn-danger btn-sm waves-effect waves-light mr-1" data-toggle="modal" data-target="#delPrevent-{{$dt->id}}" title="Supprimer"> <i class="fas fa-trash"></i> </button>
                                    @endif
                                </td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

@endsection


@section('other_js')

@endsection
