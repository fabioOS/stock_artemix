@extends('service_achat.layout')

@section('page_title')
    Demandes spéciales validées
@stop

@section('content_title')
    Demandes spéciales validées
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Demandes spéciale validées</li>
    </ol>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if(count($demandes)>0)
            <div class="table-responsive mb-4">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th>Num</th>
                            <th>N°Référence</th>
                            <th>Demandeur</th>
                            <th>Titre</th>
                            <th style="width: 100px">Montant</th>
                            <th>Statut du BC</th>
                            <th>Motif</th>
                            <th>Date de demande</th>
                            <th style="width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($demandes as $k=>$dt)
                            <tr class="font-size-16">
                                <td>{{$k+1}}</td>
                                <td><a href="#" class="text-dark font-weight-bold">{{$dt->slug}}</a> </td>
                                <td>{{$dt->user->name}}</td>
                                <td>{{$dt->titre}}</td>
                                <td>@price($dt->montant)</td>
                                <td>
                                    <span class="badge badge-pill badge-success">Validé</span>
                                </td>
                                <td>{!! nl2br($dt->description) !!}
                                    @if($dt->preuve != null)<br>
                                    <a href="{{asset('assets/uploads/demandespeciale/'.$dt->preuve)}}" target="_blank">Voir Pièce-Jointe</a>@endif
                                </td>
                                <td>{{Illuminate\Support\Carbon::parse($dt->created_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @if($dt->status == 2 OR $dt->genere == 1)
                                        <a href="{{route('pdfDmdSpecial', encrypt($dt->id))}}" class="btn btn-primary btn-sm waves-effect waves-light" title="Télécharger le BC"><i class="uil-file-download"></i></a>

                                        {{-- <button type="button" onclick="showrejet('{{$dt->slug}}')" data-toggle="modal" data-target="#rejectbon" class="btn btn-primary btn-sm" title="Effectuer le paiement"> <i class="fa fa-money-bill"></i> </button> --}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end table -->


        @else
            <h5 class="text-warning"> Il n'y a aucune demande pour l'instant !</h5>
        @endif
    </div>
</div>

<div id="rejectbon" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Effectuer le paiement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formEdit" action="{{route('sademande.paiement')}}" method="post" enctype="multipart/form-data">@csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Moyen de paiement <span class="text-danger">*</span></label>
                        <select name="moyen" id="moyen" class="form-control input-default">
                            <option selected disabled value="">Moyen de paiement</option>
                            <option value="Espece">Espèce</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Virement">Virement</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Bordereau de paiement <span class="text-danger">*</span></label>
                        <input name="bordereau" id="bordereau" class="dropify form-control" type="file">
                        <input type="hidden" name="bonid" id="bonid" value="">
                    </div>
                </div>

                <div class="modal-footer">
                    <div id="loader" class="spinner-border text-primary m-1 dispayNone" role="status">
                        <span class="sr-only">Chargement...</span>
                    </div>

                    <button onclick="sendForm()" id="addbtn" type="button" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div>
</div>


@endsection


@section('other_js')
<script>

    function showrejet(id){
        $("#formEdit")[0].reset();
        $("#bonid").val(id);
    }

    function sendForm() {
        var moyen = document.getElementById('moyen').value;
        var bordereau = document.getElementById('bordereau').value;

        if(!moyen){
            swal("Oups!", "Veuillez renseignez le champs Moyen de paiement");
            return false;
        }else if(!bordereau){
            swal("Oups!", "Veuillez renseignez le champs Bordereau de paiement");
            return false;
        }else{
            swal({
                title: "Êtes-vous sûr?",
                text: "Vous êtes sur le point de faire un paiement ! Cette action est irréversible ? ",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    document.getElementById('addbtn').disabled = true;
                    $('#addbtn').hide();
                    $('#loader').show();
                    document.getElementById('formEdit').submit();
                }
            });
        }
    }

</script>
@endsection
