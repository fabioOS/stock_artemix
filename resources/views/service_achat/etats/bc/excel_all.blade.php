<table>
    <thead>
        <tr>
            @php $sousprograms = \App\ProgrammeSous::where('programme_id',Session::get('program'))->get() @endphp
            <th>NUM</th>
            <th>BC N°</th>
            @if(count($sousprograms)>0)
                <th>SOUS-PROGRAMME</th>
            @endif
            <th>FOURNISSEUR</th>
            <th>CODE</th>
            <th>MATIERE</th>
            <th>TYPE</th>
            <th>QUANTITE</th>
            <th>P.U</th>
            <th>REMISE</th>
            <th>PRIX U.HT</th>
            <th>MONTANT TOTAL</th>
            <th>TAUX</th>
            <th>MONTANT TTC</th>
            <th>DATE COMMANDE</th>
            <th>LIGNE BUDGETAIRE</th>
            <th>MOTIF DE LA COMMANDE</th>
            <th>AFFECTATION CG</th>
        </tr>
    </thead>

    <tbody>
        @php $y = 1; @endphp
        @foreach($data as $e=>$bc)
            @foreach($bc->items as $k=>$item)
                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                <tr>
                    <td>{{$y}}</td>
                    <td>{{$item->bc->ref}}</td>
                    @if(count($sousprograms)>0)
                        <td>{{$bc->demande->programmesous ? $bc->demande->programmesous->libelle : '-'}}</td>
                    @endif
                    <td>{{$item->bc->fournisseur? $item->bc->fournisseur->nom : '-'}}</td>
                    <td>{{$pdt->code}}</td>
                    <td><b>{{$pdt->libelle}}</b></td>
                    <td>{{$pdt->type->libelle}}</td>
                    <td>{{$item->qte}} </td>
                    <td>@price($item->price)</td>
                    <td>{{$item->remise}} %</td>
                    <td>@price($item->price - (($item->remise /100) * $item->price))</td>
                    <td>@price($item->mtt)</td>

                    @php
                        if($bc->tva == "AVEC"){
                            $tauxlt = '18%';
                            $mttctaux = (0.18 * $item->mtt) + $item->mtt;
                        }elseif($bc->retenu){
                            $tauxlt = '2%';
                            $mttctaux = $item->mtt - (0.02 * $item->mtt);
                        }else{
                            $tauxlt = '0';
                            $mttctaux = $item->mtt;
                        }
                    @endphp

                    <td>{{$tauxlt}}</td>
                    <td>@price($mttctaux)</td>
                    <td>{{$item->created_at->format('d-m-Y')}}</td>
                    <td>@if($bc->demande && $bc->demande->lignebudget)
                        {{$bc->demande->lignebudget->libelle}}
                        @endif
                    </td>
                    <td>
                        @if($bc->demande && $bc->demande->lignebudget)
                        {{$bc->demande->description}}
                        @endif
                    </td>

                    <td>{{$pdt->code_affect ?? '-'}}</td>
                </tr>
                @php $y++; @endphp
            @endforeach

            <tr>
                <td></td>
                <td></td>
                @if(count($sousprograms)>0)
                    <td></td>
                @endif
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

        @endforeach
    </tbody>
</table>
