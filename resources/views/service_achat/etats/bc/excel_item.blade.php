<table>
    <thead>
        <tr>
            <th>NUM</th>
            <th>CODE</th>
            <th>MATIERE</th>
            <th>TYPE</th>
            <th>QUANTITE</th>
            <th>P.U</th>
            <th>REMISE</th>
            <th>PRIX U.HT</th>
            <th>MONTANT</th>
        </tr>
    </thead>

    <tbody>
        @foreach($data->items as $k=>$item)
            @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
            <tr>
                <td>{{$k+1}}</td>
                <td>{{$pdt->code}}</td>
                <td><b>{{$pdt->libelle}}</b></td>
                <td>{{$pdt->type->libelle}}</td>
                <td>{{$item->qte}} </td>
                <td>@price($item->price)</td>
                <td>{{$item->remise}} %</td>
                <td>@price($item->price - (($item->remise /100) * $item->price))</td>
                <td>@price($item->mtt)</td>
            </tr>
        @endforeach

        <tr>
            <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL HT</b></td>
            <td class="td">@price($data->montant)</td>
        </tr>
        @if($data->tva == "AVEC")
        <tr>
            <td class="td" colspan="8" style="text-align: right"><b>TVA 18%</b></td>
            <td class="td">{{($data->montant *0.18)}}</td>
        </tr>
        <tr>
            <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL TTC</b></td>
            <td class="td">{{($data->montant *0.18)+$data->montant}}</td>
        </tr>
        @else
            @if($data->retenu)
                <tr>
                    <td class="td" colspan="8" style="text-align: right"><b>RETENUE 2%</b></td>
                    <td class="td">@price($data->retenu)</td>
                </tr>
                <tr>
                    <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL TTC</b></td>
                    <td class="td">@price($data->montant-$data->retenu)</td>
                </tr>
            @else
                <tr>
                    <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL TTC</b></td>
                    <td class="td">@price($data->montant)</td>
                </tr>
            @endif
        @endif

    </tbody>
</table>
