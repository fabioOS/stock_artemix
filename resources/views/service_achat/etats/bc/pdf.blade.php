<style>
    .page-break {
        page-break-after: always;
    }

    @page {
        /* size: A4; */
        margin: 10px;
    }


    body{
        font-size: 8px;
        margin: 1px;
    }
    table {
        font-size: 8px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 0px;
        font-size: 8px;
    }
    .tdd{
        padding: 1px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 2px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }



</style>
<body>
<table width="100%" style="vertical-align: top;">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td width="50%">
                <div class="col">
                    <img src="{{base_path('assets/images/logoartemis.png')}}" data-holder-rendered="true" height="50px" />
                </div>
            </td>
            <td width="50%" style="text-align: right">
                {{date('d/m/Y h:i:s')}}
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: right">
                <p style="font-size: 12px;font-weight: bold">PROGRAMME : {{$datas[0]->programme->libelle}}</p>
            </td>
        </tr>
    </table>


    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 12px;font-weight: bold">ETAT DES BONS DE COMMANDE</p>
                @if(isset($date) and isset($date2))
                    <span style="font-size: 10px">Du {{date('d/m/Y',strtotime($date))}} au {{date('d/m/Y',strtotime($date2))}}</span>
                @endif
            </td>
        </tr>

    </table>

    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>N°</strong></td>
            <td class="td"><strong>BC N°</strong></td>
            <td class="td"><strong>BON DEMANDE</strong></td>
            @php $sousprograms = \App\ProgrammeSous::where('programme_id',Session::get('program'))->get() @endphp
            @if(count($sousprograms)>0)
                <td class="td"><strong>SOUS-PROGRAMME</strong></td>
            @endif
            <td class="td"><strong>FOURNISSEUR</strong></td>
            <td class="td"><strong>MODE</strong></td>
            {{-- <td class="td"><strong>TVA</strong></td> --}}
            <td class="td"><strong>RETENUE</strong></td>
            <td class="td"><strong>MONTANT TOTAL HT</strong></td>
            <td class="td"><strong>MONTANT TVA</strong></td>
            <td class="td"><strong>MONTANT TOTAL TTC</strong></td>
            <td class="td"><strong>DATE COMMANDE</strong></td>
            <td class="td"><strong>LIGNE BUDGETAIRE</strong></td>
            <td class="td"><strong>MOTIF DE LA COMMANDE</strong></td>
        </tr>
        @php $sstotal = 0 @endphp
        @foreach($datas as $k=>$data)
        @php $rttn = $data->retenu ?: 0;
            $mtht = $data->montant - $rttn;
        @endphp
            <tr class="">
                <td class="td">{{$k+1}}</td>
                <td class="td"><p><strong>{{$data->ref}}</strong></p></td>
                <td class="td">{{$data->demande ? $data->demande->ref : '-'}}</td>
                @if(count($sousprograms)>0)
                    <td class="td">{{$data->demande->programmesous ? $data->demande->programmesous->libelle : '-'}}</td>
                @endif
                <td class="td">{{$data->fournisseur? $data->fournisseur->nom : '-'}}</td>
                <td class="td">{{$data->mode}}</td>
                {{-- <td class="td">{{$data->tva}}</td> --}}
                <td class="td">@price($rttn)</td>
                <td class="td">@price($mtht)</td>
                <td class="td">@if($data->tva == 'AVEC') @price(($mtht *0.18)) @else 0 @endif</td>
                <td class="td">@if($data->tva == 'AVEC') @price(($mtht *0.18)+$mtht) @else @price($mtht) @endif</td>
                <td class="td">{{$data->created_at->format('d-m-Y')}}</td>
                <td class="td">{{ $data->demande->lignebudget ? $data->demande->lignebudget->libelle : '-'  }}</td>
                <td class="td">{{ $data->demande->description }}</td>
            </tr>
        @endforeach
    </table>
</table>
<div class="page-break"></div>
</body>
