<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

    .page-break {
        page-break-after: always;
    }

</style>
<body>
    <table width="100%" style="vertical-align: top;">
        <table cellspacing="0" cellpadding="0" width="100%" height="100%">
            <tr>
                <td align="center">
                    <p style="font-size: 16px;font-weight: bold">ETAT PAR BON DE COMMANDE
                        <br><span style="font-size: 12px"><b>{{$data->ref}}</b></span>
                        <br><span style="font-size: 10px"><b>{{$data->created_at->format('d-m-Y')}}</b></span>
                    </p>
                </td>
            </tr>
        </table>
        <br>
        <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tr style="background-color: #e1e1e8">
                <td class="td"><strong>NUM</strong></td>
                <td class="td"><strong>CODE</strong></td>
                <td class="td"><strong>MATIERE</strong></td>
                <td class="td"><strong>TYPE</strong></td>
                <td class="td"><strong>QUANTITE</strong></td>
                <td class="td"><strong>P.U</strong></td>
                <td class="td"><strong>REMISE</strong></td>
                <td class="td"><strong>PRIX U.HT</strong></td>
                <td class="td"><strong>MONTANT</strong></td>
            </tr>
            @php $sstotal = 0 @endphp
            @foreach($data->items as $k=>$item)
                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                @if($pdt)
                <tr class="">
                    <td class="td">{{$k+1}}</td>
                    <td class="td">{{$pdt->code}}</td>
                    <td class="td"><b>{{$pdt->libelle}}</b></td>
                    <td class="td">{{$pdt->type->libelle}}</td>
                    <td class="td">{{$item->qte}} </td>
                    <td class="td">@price($item->price)</td>
                    <td class="td">{{$item->remise}} %</td>
                    <td class="td">@price($item->price - (($item->remise /100) * $item->price))</td>
                    <td class="td">@price($item->mtt)</td>
                </tr>
                @endif
            @endforeach
            <tr>
                <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL HT</b></td>
                <td class="td">@price($data->montant)</td>
            </tr>
            @if($data->tva == "AVEC")
            <tr>
                <td class="td" colspan="8" style="text-align: right"><b>TVA 18%</b></td>
                <td class="td">{{($data->montant *0.18)}}</td>
            </tr>
            <tr>
                <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL TTC</b></td>
                <td class="td">{{($data->montant *0.18)+$data->montant}}</td>
            </tr>
            @else
                @if($data->retenu)
                    <tr>
                        <td class="td" colspan="8" style="text-align: right"><b>RETENUE 2%</b></td>
                        <td class="td">@price($data->retenu)</td>
                    </tr>
                    <tr>
                        <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL TTC</b></td>
                        <td class="td">@price($data->montant-$data->retenu)</td>
                    </tr>
                @else
                    <tr>
                        <td class="td" colspan="8" style="text-align: right"><b>MONTANT TOTAL TTC</b></td>
                        <td class="td">@price($data->montant)</td>
                    </tr>
                @endif
            @endif

        </table>
    </table>
    <div class="page-break"></div>
</body>
