<table>
    <thead>
        <tr>
            <th>NUM</th>
            <th>CODE</th>
            <th>MATIERE</th>
            <th>TYPE</th>
            <th>QUANTITE</th>
            <th>P.U</th>
            <th>MONTANT</th>
        </tr>
    </thead>

    <tbody>
        @foreach($datas as $k=>$data)
            <tr>
                <td>{{$k+1}}</td>
                <td><p><strong>{{$data->produits->code}}</strong></p></td>
                <td>{{$data->produits->libelle}}</td>
                <td>{{$data->produits->type ? $data->produits->type->libelle : '-'}}</td>
                <td>{{$data->qte}}</td>
                <td>@price($data->price)</td>
                <td>@price($data->price * $data->qte)</td>
            </tr>
        @endforeach
    </tbody>
</table>
