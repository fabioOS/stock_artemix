<html>
    <style>
        body{
            font-size: 10px;
            margin: 1.5px;
        }
        table {
            font-size: 11px;
        }

        .td{
            border: 1px solid #000000;
            padding-left: 5px;
            padding: 10px;
        }
        .tdd{
            padding: 3px;
            border-bottom: 1px solid #e2e2e2;
        }
        .txt-center{
            text-align: center;
        }
        .txt-right{
            text-align: right;
        }
        #resultscol td{
            border: 1px solid #000000;
            padding: 3px;
        }

        #fin ul li{
            list-style: none;
            text-align: left;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            color: #000;
            font-size: 10px;
            text-align: center;
        }

    </style>
<body>
<div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td width="50%">
                <div class="col">
                    <img src="{{base_path('assets/images/logoartemis.png')}}" data-holder-rendered="true" height="50px" />
                </div>
            </td>
            <td width="50%" style="text-align: right">
                {{date('d/m/Y h:i:s')}}
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: right">
                <p style="font-size: 14px;font-weight: bold">PROGRAMME : {{$datas[0]->programme->libelle}}</p>
            </td>
        </tr>
    </table>


    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES PRODUITS</p>
            </td>
        </tr>

    </table>

    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>NUM</strong></td>
            <td class="td"><strong>CODE</strong></td>
            <td class="td"><strong>MATIERE</strong></td>
            <td class="td"><strong>TYPE</strong></td>
            <td class="td"><strong>QUANTITE</strong></td>
            <td class="td"><strong>P.U</strong></td>
            <td class="td"><strong>MONTANT TOTAL</strong></td>
        </tr>
        @foreach($datas as $k=>$data)
            <tr class="">
                <td class="td">{{$k+1}}</td>
                <td class="td"><p><strong>{{$data->produits->code}}</strong></p></td>
                <td class="td">{{$data->produits->libelle}}</td>
                <td class="td">{{$data->produits->type ? $data->produits->type->libelle : '-'}}</td>
                <td class="td">{{$data->qte}}</td>
                <td class="td">@price($data->price)</td>
                <td class="td">@price($data->price * $data->qte)</td>
            </tr>
        @endforeach

    </table>
    <br>
</div>
{{-- <footer>
    Abidjan Cocody Riviera Jardin, 08 BP 2553 ABIDJAN 08, RCCM I CI-ABJ-2019-M-15316, CC I 1631155W, Capital social: 1 000 000 <br>
    FCFA, Régime d'imposition : Réel simplifié, BRIDGE BANK C1131 01009 018751820005 63, Tel : 00225 21 32 96 90 / 00225 87 33 12 03
</footer> --}}
</body>
</html>
