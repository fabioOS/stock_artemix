@extends('service_achat.layout')

@section('page_title')
    Fournisseurs
@stop

@section('content_title')
    Fournisseurs
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Fournisseurs</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div>
                <button type="button" data-toggle="modal" data-target="#addProg" class="btn btn-success waves-effect waves-light mb-5"><i class="mdi mdi-plus mr-1"></i> Ajouter un fournisseur</button>
            </div>

            <div class="table-responsive mb-4 gstock">
                <table id="datatable-buttons" class="table table-centered datatable table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                    <tr class="bg-transparent">
                        <th>N°</th>
                        <th>Nom</th>
                        <th>Contact</th>
                        <th>Adresse</th>
                        <th>Email</th>
                        <th>Delais de paiement</th>
                        <th>Regime fiscal</th>
                        <th>Ligne crédit</th>
                        <th style="width: 100px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($fournisseurs as $k=>$fournisseur)
                        <tr class="font-size-16">
                            <td>{{$k+1}}</td>
                            <td>{{$fournisseur->nom}}</td>
                            <td>{{$fournisseur->contact}}</td>
                            <td>{{$fournisseur->adresse}}</td>
                            <td>{{$fournisseur->email}}</td>
                            <td>{{$fournisseur->delais_paiement}}</td>
                            <td>{{$fournisseur->regime}}</td>
                            <td>{{$fournisseur->credit}}</td>
                            <td style="width: 150px;">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="{{route('stats.fournisseur',encrypt($fournisseur->id))}}" class="Programme px-2 " data-toggle="tooltip" data-placement="top" title="" data-original-title="Voir plus"><i class="fa fa-eye font-size-18"></i></a>
                                    </li>

                                    <li class="list-inline-item">
                                        <a href="#" data-toggle="modal" data-target="#updateProg" onclick="displayModalEdt(`{{$fournisseur->id}}`,`{{$fournisseur->nom}}`,`{{$fournisseur->contact}}`,`{{$fournisseur->adresse}}`,`{{$fournisseur->email}}`,`{{$fournisseur->delais_paiement}}`,`{{$fournisseur->regime}}`,`{{$fournisseur->credit}}`)" class="editerProgramme px-2 text-primary" data-placement="top" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                    </li>

                                    <li class="list-inline-item">
                                        <a href="{{route('sa.fours.delete',$fournisseur->id)}}" class="delete Programme px-2 text-danger delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"><i class="uil uil-trash-alt font-size-18"></i></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- end table -->
        </div>
    </div>

    <!-- modal update programme -->
    <div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateProglLabel">Ajouter le fournisseur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('sa.fours.store')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nom <span class="text-danger">*</span></label>
                            <input type="text" name="nom" class="form-control" required="" value="">
                        </div>

                        <div class="form-group">
                            <label>Contact <span class="text-danger">*</span></label>
                            <input type="text" name="contact" class="form-control" required="" value="">
                        </div>

                        <div class="form-group">
                            <label>Adresse</label>
                            <input type="text" name="adresse" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Delais de paiement</label>
                            <input type="text" name="delais_paiement" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Regisme fiscal</label>
                            <input type="text" name="regime" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Ligne crédit</label>
                            <input type="text" name="credit" class="form-control" value="">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal add programme-->
    </div>


    <div id="updateProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateProglLabel">Modifier le fournisseur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('sa.fours.update')}}" method="post" id="formEdit">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nom <span class="text-danger">*</span></label>
                            <input type="text" id="nom" name="nom" class="form-control" required="" value="">
                        </div>

                        <div class="form-group">
                            <label>Contact <span class="text-danger">*</span></label>
                            <input type="text" id="contact" name="contact" class="form-control" required="" value="">
                        </div>

                        <div class="form-group">
                            <label>Adresse</label>
                            <input type="text" id="adresse" name="adresse" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" id="email" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Delais de paiement</label>
                            <input type="text" name="delais_paiement" id="delais_paiement" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Regisme fiscal</label>
                            <input type="text" name="regime" id="regime" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Ligne crédit</label>
                            <input type="text" name="credit" id="credit" class="form-control" value="">
                        </div>

                        <input type="hidden" name="slug" id="idslug">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal add programme-->
    </div>

@endsection


@section('other_js')
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

    <script>
        $(document).ready(function(){

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet fournisseur",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        function displayModalEdt(id,nom,cont,add,email,delais,regime,credit){
            //console.log(id,nom,cont,add)
            $("#formEdit")[0].reset();
            $("#idslug").val(id);
            $("#nom").val(nom);
            $("#contact").val(cont);
            $("#adresse").val(add);
            $("#email").val(email);
            $("#delais_paiement").val(delais);
            $("#regime").val(regime);
            $("#credit").val(credit);
        }
    </script>
@endsection
