<style>
    .text-bleu{
        color: #234699
    }
</style>

<ul class="metismenu list-unstyled" id="side-menu">
    <li>
        <a href="{{route('sa.index')}}">
            <i class="uil-home-alt"></i>
            <span>Tableau de bord</span>
        </a>
    </li>

    <li class="menu-title mt-3 text-bleu">Gestion du stock</li>

    <li class="{{Route::is('sa.etats') || Route::is('sa.etats.search')|| Route::is('sa.etats.item') ? 'mm-active' : ''}}">
        <a href="{{route('sa.etats')}}" class="{{Route::is('sa.etats') || Route::is('sa.etats.search')|| Route::is('sa.etats.item') ? 'active' : ''}}">
            <i class="uil-home"></i>
            <span>Etats par villa</span>
        </a>
    </li>

    {{-- <li>
        <a href="{{route('sa.gproduits')}}">
            <i class="uil-shop"></i>
            <span>Matières</span>
        </a>
    </li> --}}

    <li class="{{Route::is('sa.gproduits') || Route::is('sa.etats.pdt') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="{{Route::is('sa.gproduits') ? 'mm-active' : ''}} has-arrow waves-effect">
            <i class="uil-shop"></i>
            <span>Matières</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li>
                <a href="{{route('sa.gproduits')}}" class="">Liste des matières</a>
            </li>
            <li>
                <a href="{{route('sa.etats.pdt')}}" class="">Etats</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{route('sa.stocks')}}">
            <i class="uil-notes"></i>
            <span>Stock</span>
        </a>
    </li>

    <li class="{{Route::is('sa.stocks.rotations') || Route::is('sa.stocks.rotations.store') ? 'mm-active' : ''}}">
        <a href="{{route('sa.stocks.rotations')}}" class="{{Route::is('sa.stocks.rotations') || Route::is('sa.stocks.rotations.store') ? 'active' : ''}}">
            <i class="uil-pause"></i>
            <span>Rotation</span>
        </a>
    </li>

    {{-- <li>
        <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-shop"></i>
            <span>Matières</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('sa.gproduits.types')}}" class="{{Route::is('sa.gproduits.types') ? 'active' : ''}}">Types</a></li>
            <li><a href="{{route('sa.gproduits')}}" class="{{Route::is('sa.gproduits') ? 'active' : ''}}">Matières</a></li>
        </ul>
    </li> --}}

    <li>
        <a href="{{route('sa.fours')}}">
            <i class="uil-user-circle"></i>
            <span>Fournisseurs</span>
        </a>
    </li>

    <li class="menu-title mt-3 text-bleu">Gestion des commandes</li>

    <li>
        <a href="{{route('lignebudgetaire.index')}}">
            <i class="uil-chart"></i>
            <span>Ligne budgetaire</span>
        </a>
    </li>

    @php
        $waiting = (new \App\Http\Controllers\Serviceachat\CommandeController())->getByStatus(2,1);
        $nbwaitinglign = (new \App\Http\Controllers\Serviceachat\CommandeController())->getByStatus(2,1,'ligne');
    @endphp

    <li class="{{Route::is('sacommande.show') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="{{$waiting==0 ? 'has-arrow' : ''}} {{Route::is('sacommande.show') || Route::is('sacommande.create') || Route::is('sacommande.majcmd') ? 'mm-active' : ''}} waves-effect">
            <i class="uil-file-alt"></i>
            @if($waiting>0 )
                <span class="badge badge-pill badge-warning float-right">{{$waiting}}</span>
            @endif
            <span>Commandes</span>
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li> <a href="{{route('commande.create')}}" class="{{Route::is('commande.create') ? 'active' : ''}}">Passer une commande</a> </li>
            <li> <a href="{{route('sacommande.waiting')}}" class="">
                @if($waiting>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$waiting}}</span>
                @endif
                <span>Commandes validées</span>
                </a>
            </li>
            <li> <a href="{{route('sabc.majcmd')}}" class="">
                @if($nbwaitinglign>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$nbwaitinglign}}</span>
                @endif
                <span>MAJ Commandes validées</span>
                </a>
            </li>
            <li> <a href="{{route('cp.etats.cmd')}}" class="{{Route::is('cp.etats.cmd') ? 'active' : ''}}">Etats</a> </li>
        </ul>
    </li>

    <li class="{{Route::is('sabc.show') ? 'mm-active' : ''}}">
        @php
            $waitingct = (new \App\Http\Controllers\Serviceachat\BonCommandeController())->getByStatus(1);
            $rejetct = (new \App\Http\Controllers\Serviceachat\BonCommandeController())->getByStatus(4);
        @endphp

        <a href="javascript: void(0);" class="{{$waitingct==0 ? 'has-arrow' : ''}} {{Route::is('sabc.show') ? 'mm-active' : ''}} waves-effect">
            <i class="uil-file-alt"></i>
            <span>Bon de commande</span>
            @if($waitingct>0 )
                <span class="badge badge-pill badge-warning float-right">{{$waitingct}}</span>
            @endif
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li>
                <a href="{{route('sabc.create')}}" class="">Crée un bon</a>
            </li>
            {{-- <li>
                <a href="{{route('sabc.waiting')}}" class="">En attente
                    @if($waitingct>0 )
                        <span class="badge badge-pill badge-primary float-right">{{$waitingct}}</span>
                    @endif
                </a>
            </li> --}}
            <li>
                <a href="{{route('sabc.valider')}}" class="">Liste des bons</a>
            </li>
            {{-- <li><a href="{{route('sabc.rejeter')}}" class="">Bon rejeté
                @if($rejetct>0 )
                    <span class="badge badge-pill badge-primary float-right">{{$rejetct}}</span>
                @endif
            </a></li> --}}
            <li>
                <a href="{{route('sa.etats.bc')}}" class="">Etats</a>
            </li>
        </ul>
    </li>

    <li class="{{Route::is('sabr.show') ? 'mm-active' : ''}}">
        @php
            $wagct = (new \App\Http\Controllers\Serviceachat\BonReceptionController())->getBonStatus();
        @endphp

        <a href="javascript: void(0);" class="{{$wagct==0 ? 'has-arrow' : ''}} {{Route::is('sabr.show') ? 'mm-active' : ''}} waves-effect">
            <i class="uil-file-alt"></i>
            <span>Bon de reception</span>
            @if($wagct>0 )
                <span class="badge badge-pill badge-warning float-right">{{$wagct}}</span>
            @endif
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li>
                <a href="{{route('sabr.index')}}" class="">Bon en attente
                @if($wagct>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$wagct}}</span>
                @endif
                </a>
            </li>
            <li>
                <a href="{{route('sabr.indexconform')}}" class="">Bon conforme</a>
            </li>
        </ul>
    </li>

    <li class="menu-title mt-3 text-bleu">Gestion des demandes</li>
    @php
        $waitingdmd = (new \App\Http\Controllers\Serviceachat\DemandesController())->getByStatus(2,0);
    @endphp
    <li class="{{Route::is('sademande.show') ? 'mm-active' : ''}}">
        <a href="javascript: void(0);" class="{{$waitingdmd==0 ? 'has-arrow' : ''}} {{Route::is('sademande.show') ? 'mm-active' : ''}} waves-effect">
            <i class="uil-align-letter-right"></i>
            <span>Demandes spéciales</span>
            @if($waitingdmd>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$waitingdmd}}</span>
                @endif
        </a>
        <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{route('sademande.create')}}" class="">Créer une demande </a></li>
            <li><a href="{{route('sademande.waiting')}}" class="">En attente
                @if($waitingdmd>0 )
                    <span class="badge badge-pill badge-warning float-right">{{$waitingdmd}}</span>
                @endif
            </a></li>
            <li><a href="{{route('sademande.rejet')}}" class="">Rejetées </a></li>
            <li><a href="{{route('sademande.valid')}}" class="">Validées </a></li>
            <li><a href="{{route('sademande.traiter')}}" class="">Cloturées / Payées</a></li>
        </ul>

    </li>

</ul>
