@extends('service_achat.layout')

@section('page_title')
    Gestions des matières
@stop

@section('content_title')
    Gestions des matières
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">Matières</a></li>
        <li class="breadcrumb-item active">Liste des matières</li>
    </ol>
@stop

@section('other_css')
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-5">
                        <a href="#!" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#addProg">
                            <i class="mdi mdi-plus mr-2"></i> Ajouter une matière
                        </a>

                        <a href="#!" class="btn btn-danger waves-effect waves-light float-right" data-toggle="modal" data-target="#addProgExcel">
                            <i class="fa fa-file-excel mr-2"></i> Importer le modèle
                        </a>
                        <p><i>Télécharge le modèle d'importation <a href="{{route('sa.gproduits.model')}}" >ici</a></i></p>
                    </div>

                    <table id="datatable-buttons" class="table table-centered table-striped table-bordered dt-responsive gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Code</th>
                            <th>Catégorie</th>
                            <th>Nom</th>
                            {{-- <th>Type</th> --}}
                            <th>Prix</th>
                            <th>Quantités</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($listproduits as $k=>$item)
                        @if($item->produit)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$item->produit->code}}</td>
                                <td>
                                    @if($item->produit->type_materiel == "materiel")
                                        <span class="badge badge-primary badge-sm">Matériel</span>
                                    @else
                                        <span class="badge badge-success badge-sm">Matériau</span>
                                    @endif
                                </td>
                                <td>{{$item->produit->libelle}} <sup class="badge badge-info">{{$item->produit->type ? $item->produit->type->libelle : ''}}</sup></td>
                                {{-- <td>{{ucfirst($item->produit->type_materiel)}}</td> --}}
                                <td>@price($item->prix) Fcfa</td>
                                <td>{{$item->qte}}</td>
                                <td>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <a href="#" onclick="displayModalE('{{$item->id}}','{{$item->prix}}','{{$item->produit->id}}','{{$item->produit->type->libelle}}','{{$item->produit->type_materiel}}')" class="editerProgramme px-2 text-primary" data-toggle="modal" data-target="#updateProg" title="" data-original-title="Editer"><i class="uil uil-pen font-size-18"></i></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    <!-- modal add user -->
    <div id="addProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="addProglLabel">Ajouter une matières</h5>
                    {{-- <p class="card-title-desc">Merci de verifier si la matière existe déjà dans la liste a</p> --}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('sa.gproduits.store')}}" method="post" id="progForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Type de produit <span class="text-danger">*</span></label>
                            <select id="typdt" name="typdt" class="form-control" required style="width: 100%">
                                <option value="" disabled>Selectionnez le type</option>
                                <option value="materiaux" selected>Materiaux</option>
                                <option value="materiel">Matériel</option>
                            </select>
                        </div>

                        <div id="selectpdt">
                            <div class="form-group">
                                <label>Produit <span class="text-danger">*</span></label>
                                <select id="pdt" name="pdt" class="select2 form-control pdtchang" style="width: 100%">
                                    <option value="" disabled selected>Selectionnez un produit</option>
                                    @foreach($produits as $produit)
                                        <option value="{{$produit->id}}" data-type="{{$produit->type->libelle}}">{{$produit->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Type </label>
                                <input type="text" id="type" readonly class="form-control">
                            </div>
                        </div>

                        <div id="selectmate" style="display: none">
                            <div class="form-group">
                                <label>Code <span class="text-danger">*</span></label>
                                <input type="text" id="codemateriel" name="codemateriel" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Produit <span class="text-danger">*</span></label>
                                <input type="text" id="pdtmateriel" name="pdtmateriel" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Type </label>
                                <select id="typemateriel" name="typemateriel" class="select2 form-control pdtchang" style="width: 100%">
                                    <option value="" disabled selected>Selectionnez un type</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Prix <span class="text-danger">*</span></label>
                            <input type="number" min="0" step="any" name="prix" class="form-control" required="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="button" id="sendpdt" class="btn btn-primary waves-effect waves-light">Ajouter</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal add user-->
    </div>

    <div id="updateProg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateProgLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="updateProglLabel">Modifier une matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('sa.gproduits.update')}}" method="post" id="formEdit">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Produit <span class="text-danger">*</span></label>
                            <div id="secpdtedit">
                                <select id="pdtedit" name="pdt" class="select2 form-control pdtchang" style="width: 100%">
                                </select>
                            </div>
                            <input type="text" id="pdtmaterieledit" name="pdtmateriel" class="form-control" style="display: none">
                        </div>

                        <div class="form-group">
                            <label>Type </label>
                            <input type="text" id="typedit" readonly class="form-control">
                            <div id="divmaterieledit" style="display: none">
                                <select id="typematerieledit" name="typemateriel" class="select2 form-control pdtchang" style="width: 100%">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Prix <span class="text-danger">*</span></label>
                            <input type="number" min="0" step="any" id="prix" name="prix" class="form-control" required="">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="idslug" name="slug">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Modifier</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <div id="addProgExcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addProgExcelLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="addProglLabel">Importer le modèle renseigner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('sa.gproduits.import')}}" method="post" id="progForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label id="ufile">Fichier</label>
                            <input type="file" id="ufile" name="ufile" accept=".XLSX" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Importer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection


@section('other_js')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    {{-- <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script> --}}
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>


    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $('.gstock').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet utilisateur",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });

        $('#pdt').change(function() {
            var actiflibel = $("#pdt").find(':selected').data('type');
            $('#type').val(actiflibel);
        });

        $('#pdtedit').change(function() {
            var actiflibel = $("#pdtedit").find(':selected').data('type');
            $('#typedit').val(actiflibel);
        });


        function displayModalE(id,prix,pdtid,type,type_materiel){
            $("#formEdit")[0].reset();
            $("#idslug").val(id);
            $("#prix").val(prix);
            var optionData ='';

            // console.log(type_materiel);
            var data ={!! json_encode($produits) !!};

            if(type_materiel == 'materiel'){
                $('#typedit').hide();
                $('#divmaterieledit').show();
                $('#pdtmaterieledit').show();
                $('#secpdtedit').hide();

                var libel = data.find(x => x.id == pdtid).libelle;
                $('#pdtmaterieledit').val(libel);

                var types ={!! json_encode($types) !!};
                for (var i = 0; i < types.length; i++){
                    var select = '';
                    if(types[i].libelle == type){
                        select = 'selected';
                    }
                    optionData+='<option value="'+types[i].id+'" '+select+'>'+types[i].libelle +'</option>';
                }

                $('#typematerieledit').html(optionData);
            }else{
                $("#typedit").val(type);

                $('#divmaterieledit').hide();
                $('#typedit').show();

                for (var i = 0; i < data.length; i++){
                    var select = '';
                    if(data[i].id == pdtid){
                        select = 'selected';
                    }
                    optionData+='<option value="'+data[i].id+'" data-type="'+data[i].type.libelle+'" '+select+'>'+data[i].libelle +'</option>';
                }
                $('#pdtedit').html(optionData);
            }

        }

        $('#typdt').change(function() {
            var actiflibel = $("#typdt").val();
            if(actiflibel == 'materiel'){
                $('#selectpdt').hide();
                $('#selectmate').show();
            }else{
                $('#selectpdt').show();
                $('#selectmate').hide();
            }
        });

        $('#sendpdt').click(function() {
            var actiflibel = $("#typdt").val();
            if(actiflibel == 'materiel'){
                var codemateriel = $("#codemateriel").val();
                var pdtmateriel = $("#pdtmateriel").val();
                var typemateriel = $("#typemateriel").val();
                var prix = $("input[name='prix']").val();
                if(codemateriel == '' || pdtmateriel == '' || typemateriel == '' || prix == ''){
                    swal("Erreur", "Veuillez renseigner tous les champs", "error");
                }else{
                    $("#progForm").submit();
                }
            }else{
                var pdt = $("#pdt").val();
                var prix = $("input[name='prix']").val();
                if(pdt == '' || prix == ''){
                    swal("Erreur", "Veuillez renseigner tous les champs", "error");
                }else{
                    $("#progForm").submit();
                }
            }
        });
    </script>
@endsection
