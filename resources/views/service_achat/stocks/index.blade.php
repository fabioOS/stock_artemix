@extends('service_achat.layout')

@section('page_title')
    Stock du programme
@stop

@section('content_title')
    Stock du programme
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Stock du programme</li>
    </ol>
@stop

@section('content')
<div class="row">
    <div class="col-md-3 col-xl-3">
        <div class="card">
            <div class="card-body">
                <div class="float-right mt-2">
                    <i class="fa fa-money-bill fa-2x text-success"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1">@price(round($produits->sum('montant_qwaiting'),2))</h4>
                    <p class="text-muted mb-0">Montant total attendue</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-2 col-xl-2">
        <div class="card">
            <div class="card-body">
                <div class="float-right mt-2">
                    <i class="fa fa-money-bill fa-2x text-success"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1">@price(round($produits->sum('montant_qdelivre'),2))</h4>
                    <p class="text-muted mb-0">Montant total commandé</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-xl-3">
        <div class="card">
            <div class="card-body">
                <div class="float-right mt-2">
                    <i class="fa fa-money-bill fa-2x text-success"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1">@price(round($produits->sum('montant_qwaiting')-$produits->sum('montant_qdelivre'),2))</h4>
                    <p class="text-muted mb-0">Montant total restant à commander</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-2 col-xl-2">
        <div class="card">
            <div class="card-body">
                <div class="float-right mt-2">
                    <i class="fa fa-money-bill fa-2x text-success"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1">@price(round($produits->sum('mstock'),2))</h4>
                    <p class="text-muted mb-0">Montant total du stock</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-2 col-xl-2">
        <div class="card">
            <div class="card-body">
                <div class="float-right mt-2">
                    <i class="fa fa-percent fa-2x text-success"></i>
                </div>
                <div>
                    <h4 class="mb-1 mt-1">{{round($produits->avg('taux'),2)}}</h4>
                    <p class="text-muted mb-0">Taux d'evolution</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table id="datatable-buttons" class="table table-centered table-striped table-bordered gstock" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Matière</th>
                        <th title="Prix unitaire">P.U</th>
                        <th title="Quantité attendue">Q.Attendue</th>
                        <th title="Montant attendue">M.Attendue</th>
                        <th title="Quantité commandé">Q.Commandé</th>
                        <th title="Quantité restante à commander">Q.Restant</th>
                        <th title="Taux avancement">Taux</th>
                        <th title="Quantité en stock">Q.Stock</th>
                        <th title="Montant en stock">M.Stock</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($produits as $k=>$item)
                        <tr>
                            <td>{{$item->produits->code}}</td>
                            <td>{{$item->produits->libelle}} </td>
                            <td>@price($item->price) Fcfa</td>
                            <td>{{round($item->qwaiting,2)}} <sup class="text-muted">{{$item->produits->type ? $item->produits->type->libelle : ''}}</sup></td>
                            <td><span class="badge badge-info">@price($item->montant_qwaiting) F</span></td>
                            <td>{{round($item->qdelivre,2)}} <sup class="text-muted">{{$item->produits->type ? $item->produits->type->libelle : ''}}</sup></td>
                            <td>{{$item->qrestant}} <sup class="text-muted">{{$item->produits->type ? $item->produits->type->libelle : ''}}</sup></td>
                            <td>
                                @if(round($item->taux) < 33)
                                    <span class="badge badge-danger">@price($item->taux)%</span>
                                @elseif (round($item->taux) > 34 and round($item->taux) < 99)
                                    <span class="badge badge-warning">@price($item->taux)%</span>
                                @else
                                    <span class="badge badge-success">@price($item->taux)%</span>
                                @endif
                            </td>
                            <td>{{$item->qte}} <sup class="text-muted">{{$item->produits->type ? $item->produits->type->libelle : ''}}</sup></td>
                            <td><span class="badge badge-info">@price($item->mstock) F</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div>

@endsection


@section('other_js')

@endsection
