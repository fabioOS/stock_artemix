@extends('service_achat.layout')

@section('page_title')
    Réattribution
@stop

@section('content_title')
    Réattribution
@stop

@section('content_breadcrumb')
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item active">Réattribution</li>
    </ol>
@stop

@section('content')

{{-- @dd(Request::get('code')) --}}
<div class="row">
    <div class="col-lg-12">
        <form action="{{route('sa.transversale.store')}}" class="" method="post" id="addappelfonds">
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-primary">TYPE REATTRIBUTION</h4>
                    <div class="form-group">
                        <label for="">N° BC <span class="text-danger">*</span></label>
                        <input type="text" readonly id="demandeur" value="{{$bc->ref}}" class="form-control">
                    </div>

                    @php $listpjt = \App\Programme::whereIn('id',[2])->where('id','!=',Session::get('program'))->get(); @endphp

                    <div class="form-group">
                        <label for="">PROJET<span class="text-danger">*</span></label>
                        <input type="hidden" name="slug" id="slug" value="{{encrypt($bc->id)}}">
                        <select name="typepjt" id="typepjt" class="form-control select_plugin" style="min-width: 300px;" required>
                            <option selected disabled>Sélectionner un projet</option>
                            @foreach ($listpjt as $pjt)
                                <option value="{{$pjt->id}}">{{$pjt->libelle}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-eye"></i> Aperçu</button>
                    </div>
                </div>
            </div>
        </form>

        <div id="loader" class="dispayNone text-center">
            <div class="spinner-border text-primary m-1" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div id="showcontent"></div>

        <div id="btnendsend" class="form-group dispayNone">
            <button id="sendbtn" type="button" onclick="validForm()" class="btn btn-primary waves-effect waves-light"><i class="fas fa-save"></i> Enregistrer</button>
        </div>

    </div>
</div>


@endsection

@section('other_css')
{{-- <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
@endsection

@section('other_js')
<script src="{{ asset('assets/js/imask.js') }}"></script>
{{-- <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script> --}}
    <script>
        var valueSelect = '',
            inputs = [],
            arr = [],
            valuemontantt = 0,
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();

            IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' ',
                scale: 6,
            });
        });

        function submitForm() {
            event.preventDefault();
            if($('#typepjt').val()==''){
                swal("Oups!", "Veuillez renseigner le Projet.");
            }else{
                var url = "{{route('sa.transversale.showbc')}}";
                var data = {
                    'bc': '{{encrypt($bc->id)}}',
                    'typepjt': $('#typepjt').val(),
                };

                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        $('#loader').show();
                        document.getElementById('addbtn').disabled = true;
                        $('#addbtn').hide();
                    },
                    success: function(data) {
                        $('#showcontent').html(data);
                        $('#loader').hide();
                        $('#btnendsend').show();
                        document.getElementById('addbtn').disabled = false;
                        $('#addbtn').show();
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }

        }

        function validForm() {
            event.preventDefault();
            event.preventDefault();
            if($('#typepjt').val()==''){
                swal("Oups!", "Veuillez renseigner le Projet.");
            }else{
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment réattribuer ce BC.",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var btn = document.getElementById('sendbtn');
                        btn.disabled = true;
                        btn.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="margin-right: 5px;"> </span> En cours...';
                        document.getElementById("addappelfonds").submit();
                    }
                });
            }
        }

    </script>

@endsection
