<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }
</style>


<div class="card">
    <div class="card-body">
        <div>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td>
                                <div class="col">
                                    <img src="{{asset('assets/images/logoartemis.png')}}" data-holder-rendered="true" height="50px" />
                                </div>
                            </td>
                            <td style="text-align: right;font-size: 17px;font-weight: bold; padding:8px">{{$data->typebc==1? "A LIVRER" : "A DECAISSER"}}</td>
                        </tr>
                    </table>

                    <hr>

                    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="">
                        <tr>
                            <td style="width: 70%">
                                <table>
                                    <tr>
                                        <td>BC N° : </td>
                                        <td>{{$data->ref}}</td>
                                    </tr>
                                    <tr>
                                        <td>Date : </td>
                                        <td>{{$data->created_at->format('d/m/Y')}}</td>
                                    </tr>
                                    <tr>
                                        <td>N° Proforma: </td>
                                        <td>{{$data->proforma}}</td>
                                    </tr>
                                    <tr>
                                        <td>Programme: </td>
                                        <td>{{strtoupper($data->programm_rea->libelle)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Chantier: </td>
                                        <td>{{strtoupper($data->programme->reactrib_name)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Motif de la commande: </td>
                                        <td>
                                            {!! $data->demande ? nl2br($data->demande->description) : '' !!}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 30%;text-align: center;font-size: 17px;font-weight: bold;border: 1px solid;">
                                {{$data->fournisseur->nom}}
                            </td>
                        </tr>
                    </table>

                    <br><br>

                    <table border="1" cellspacing="0" cellpadding="0" width="100%" id="demandes" style="border: 1px solid;text-align: center;">
                        <thead>
                            <tr id="head">
                                <td style="width: 8%">Code Article</td>
                                <td style="width: 32%">Désignation</td>
                                <td>U</td>
                                <td>Qté</td>
                                <td>P.U</td>
                                <td>R(%)</td>
                                <td>Prix U.HT</td>
                                <td>Montant</td>
                            </tr>
                        </thead>
                        <tbody id="tbodydmd">
                            @foreach($data->items as $i=>$item)
                                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                                <tr>
                                    <td>{{$pdt->code}}</td>
                                    <td><b>{{$pdt->libelle}}</b></td>
                                    <td>{{$pdt->type->libelle}}</td>
                                    <td>{{$item->qte}} </td>
                                    <td>@price($item->price)</td>
                                    <td>{{$item->remise}} %</td>
                                    <td>@price($item->price - (($item->remise /100) * $item->price))</td>
                                    <td>@price($item->mtt)</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <br> <br><br>

                    <table border="0" cellspacing="0" cellpadding="0" width="100%" id="cdtion">
                        <tr>
                            <td style="width: 40%;font-size: 17px;border: 1px solid; padding:10px">
                                {{-- Condition de paiement : <br> <b>{{$data->mode}} - {{date('d/m/Y',strtotime($data->datepaiement))}}</b> --}}
                                Condition de paiement : <br> <b>{{$data->mode}}</b>
                            </td>
                            <td style="width: 20%"></td>
                            <td style="width: 40%;text-align: center;font-size: 17px;border: 1px solid;line-height: 1.5;">
                                <table border="0" cellspacing="0" cellpadding="4" width="100%" style="font-size: 17px;">
                                    <tr>
                                        <td class="text-right">Montant Total HT :</td>
                                        <td>@price($data->montant)</td>
                                    </tr>
                                    @if($data->tva =="AVEC")
                                    <tr>
                                        <td class="text-right">TVA 18% :</td>
                                        <td>TVA NON FACTURÉE</td>
                                    </tr>
                                    @endif
                                    @if($data->retenu)
                                    <tr>
                                        <td class="text-right">Retenue 2% :</td>
                                        <td>@price($data->retenu)</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td class="text-right">Montant Total TTC :</td>
                                        @if($data->tva =="AVEC")
                                            <td>@price($data->montant)</td>
                                        @else
                                            @if($data->retenu)
                                            <td>@price($data->montant - $data->retenu)</td>
                                            @else
                                            <td>@price($data->montant )</td>
                                            @endif
                                        @endif
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <br>

                    <table border="0" cellspacing="0" cellpadding="6" width="100%" id="" style="border: 1px solid;">
                        <tr>
                            <td>Nom du demandeur :</td>
                            @php $user = \App\User::where('id',$data->demande->user_id)->first() @endphp
                            <td>{{$user ? $user->name :'N/A'}}</td>
                        </tr>
                    </table>
                    <br>

                    <table border="1" cellspacing="0" cellpadding="6" width="100%" id="" style="border: 1px solid #000;">
                        <tr id="head">
                            <td>Service Achat</td>
                            <td>Direction financière</td>
                            <td>Direction Générale</td>
                        </tr>
                        <tr id="head">
                            <td style="padding:10px">
                                {{$sa ? $sa->name.''.$sa->prenom : ''}}
                            </td>
                            <td style="padding:10px">
                                {{$daf ? $daf->name.''.$daf->prenom : ''}}
                            </td>
                            <td style="padding:10px"></td>
                        </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="6" width="100%" id="" style="border: 1px solid;">
                        <tr>
                            <td style="padding: 10px;border-bottom: 1px solid"><span style="color:red"> Original à joindre à votre facture</span> </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px;border-bottom: 1px solid"><span style="color:black">
                                Achat en exonération de la TVA conformément à l'arrêté Nº0227/MFB/DGI/DGD du 08 mars 2024, pour le projet « LES RESIDENCES KOTIBES ».
                            </span> </td>
                        </tr>
                        <tr>
                            <td style="text-align:center">
                                Abidjan Cocody Riviera Jardin, 08 BP 2553 ABIDJAN 08, ABJ-03-2022-M-40228, CC I 1631155W, Capital social: 450 000 000
                                FCFA, Régime d'imposition : Réel Normal d'imposition, BRIDGE BANK C1131 01009 018751820005 63, Tel : 00225 27 21 32 96 90 / 00225 07 87 33 12 03
                            </td>
                        </tr>
                    </table>
                </tbody>
            </table>
        </div>
    </div>
</div>
