<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| liste des new route pour la version 2
|
*/


Route::middleware(['auth', 'isAdmin'])->group(function(){

    //DQE (DEVIS QUANTITATIFS ET ESTIMATIFS)
    Route::get('/programmes/n-setting/{pjt}/dqe/{actifs}', 'Admin\ProgrammesSettingController@createDqe')->name('gprogramme.n-setting.create.dqe');
    Route::post('/programmes/n-setting/dqe/store/ce', 'Admin\ProgrammesSettingController@storeDqeCe')->name('gprogramme.n-setting.store.dqe.ce');
    Route::post('/programmes/n-setting/dqe/store/libelle', 'Admin\ProgrammesSettingController@storeDqeCeLibel')->name('gprogramme.n-setting.store.dqe.ce.libel');
    Route::post('/programmes/n-setting/dqe/update/libelle', 'Admin\ProgrammesSettingController@updateDqeCeLibel')->name('gprogramme.n-setting.update.dqe.ce.libel');

    //DQE PAR LOT
    Route::get('/programmes/n-setting/dqe/{lotid}/config', 'Admin\ProgrammesSettingController@lotDqe')->name('gprogramme.n-setting.dqe.lot');
    Route::post('/programmes/n-setting/dqe/store/matiere/lot', 'Admin\ProgrammesSettingController@storeDqeMatiereLot')->name('gprogramme.setting.store.dqe.matiere.lot');
    Route::post('/programmes/n-setting/dqe/update/matiere/lot', 'Admin\ProgrammesSettingController@updateDqeMatiereLot')->name('gprogramme.setting.update.dqe.matiere.lot');
    Route::get('/programmes/n-setting/dqe/{id}/delete/matiere/lot', 'Admin\ProgrammesSettingController@deleteDqeMatiereLot')->name('gprogramme.setting.delete.dqe.matiere.lot');
    Route::get('/get/sous/{id}/matiere/lot', 'Admin\ProgrammesSettingController@getSmatiereLot');

    //TYPE DE RATTACHEMENT
    Route::get('/programmes/n-setting/{pjt}/{type}/type-rattachement', 'Admin\ProgrammesSettingController@editTypeRattachement')->name('gprogramme.n-setting.edit.type-rattachement');

    //SOUS-PROGRAMME
    Route::post('/programmes/n-setting/sousprogramme/save', 'Admin\ProgrammesSettingController@storeSousprogramme')->name('gprogramme.n-setting.store.sprogramme');
    Route::post('/programmes/n-setting/sousprogramme/update', 'Admin\ProgrammesSettingController@updateSousprogramme')->name('gprogramme.n-setting.update.sprogramme');
    Route::get('/programmes/n-setting/sousprogramme/{id}/delete', 'Admin\ProgrammesSettingController@deleteSousprogramme')->name('gprogramme.n-setting.delete.sprogramme');

});


Route::prefix('chefchantier')->name('chefchantier.')->namespace('Chefchantier')->middleware(['auth', 'isChefchantier'])->group(function(){

    //SOUS TRAITANT
    Route::get('/sous-traitant/{slug}/n-contract', 'HomeController@contratStraitant')->name('st.n-contrat');
    Route::get('/sous-traitant/contrat/store', 'HomeController@storeStraitantContrat')->name('st.contrat.store');
    Route::get('/sous-traitant/contrats/{slug}/end', 'HomeController@contratStraitantEnd')->name('st.n-contrat.end');
    Route::post('/sous-traitant/contrat/store/end', 'HomeController@storeStraitantContratEnd')->name('st.n-contrat.store');
    //CONTRAT ST
    Route::get('/sous-traitant/contrats/{slug}/ce', 'HomeController@contratStraitantCe')->name('st.n-contrat.ce');

    //BON DE DEMANDE
        //CREATE
        Route::get('/sous-traitant/{slug}/get/contract', 'HomeController@getContratSt')->name('get.contrat.st');


});

Route::prefix('chef-projet')->namespace('Chefprojet')->middleware(['auth','isChefprojet'])->group(function(){

    //BON DE DEMANDE
        //CREATE
        Route::get('/sous-traitant/{slug}/get/contract', 'HomeController@getContratSt')->name('cphome.get.contrat.st');


    //soustraitant
    Route::get('/sous-traitant/{slug}/show/contrat', 'HomeController@showStraitant')->name('cphome.st.show');
    Route::get('/sous-traitant/contrat/en_attente', 'HomeController@stcontratwaiting')->name('cphome.st.contrat.waiting');
    Route::get('/sous-traitant/contrat/relete', 'HomeController@stcontratcancel')->name('cphome.st.contrat.cancel');
    Route::get('/sous-traitant/contrat/valider', 'HomeController@stcontratvalid')->name('cphome.st.contrat.valider');
    Route::get('/sous-traitant/contrat/{slug}/valid', 'HomeController@stcontratvalider')->name('cphome.st.contrat.valid');
    Route::post('/sous-traitant/contrat/rejeter', 'HomeController@stcontratrejeter')->name('cphome.st.contrat.rejeter');

    //listes des transactions
    Route::get('/sous-traitant/maindoeuvre/', 'HomeController@listctst')->name('cphome.st.mainoeuvre');

    //Retour en stock
    Route::get('/retour-en-stock', 'BackstockController@index')->name('cphome.back.index');
    Route::get('/retour-en-stock/{slug}/detail', 'BackstockController@show')->name('cphome.back.show');
    Route::get('/retour-en-stock/{slug}/valid', 'BackstockController@valid')->name('cphome.back.valid');
    Route::post('/retour-en-stock/reject', 'BackstockController@reject')->name('cphome.back.reject');


});


//GESTION DES LIGNES BUDGETAIRES
Route::get('/lignes-budgetaires/redirect/{id?}', 'LigneBudgetaireController@redirect')->name('lignebudgetaire.redirect');

Route::get('/lignes-budgetaires', 'LigneBudgetaireController@index')->name('lignebudgetaire.index');
Route::get('/lignes-budgetaires/{slug}/show', 'LigneBudgetaireController@show')->name('lignebudgetaire.show');
