<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/params/metrics', 'MonitoringController@getMetrics');
Route::get('/fab/sendmail/me', 'HomeController@sendmemail');

Route::get('/', function () {
    return redirect()->route('login');
})->name('index');

Auth::routes([
    'register' => false, // Registration Routes...
    //'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'isAdmin'])->group(function(){


    //profile
    Route::get('/profile', 'HomeController@profil')->name('home.profil');
    Route::post('/profile/update', 'HomeController@profilUpdate')->name('home.profil.update');
    Route::post('/profile/update/pass', 'HomeController@profilUpdatePass')->name('home.profil.update.pass');
    Route::post('/profile/update/avatar', 'HomeController@profilUpdateAvatar')->name('home.profil.update.avatar');

    //ADMIN USERS
    Route::get('/gestions-users', 'Admin\UsersController@index')->name('gusers');
    Route::get('/gestions-users/editer/{id}', 'Admin\UsersController@edit')->name('gusers.edit');
    Route::get('/gestions-users/supprimer/{id}', 'Admin\UsersController@destroy')->name('gusers.destroy');
    Route::post('/gestions-users/store', 'Admin\UsersController@store')->name('gusers.store');
    Route::post('/gestions-users/update/profil', 'Admin\UsersController@updateProfil')->name('gusers.update.profil');
    Route::post('/gestions-users/update/pass', 'Admin\UsersController@updatePass')->name('gusers.update.pass');
    Route::post('/gestions-users/status', 'Admin\UsersController@status')->name('gusers.status');

    Route::get('/gestions-users/roles', 'Admin\UsersController@roleIndex')->name('gusers.roles');
    Route::post('/gestions-users/roles', 'Admin\UsersController@roleUpdade')->name('gusers.roles.update');

    //PRODUITS
    Route::get('produits', 'Admin\ProduitsController@index')->name('gproduits');
    Route::get('/get/produit/{id}/type', 'Admin\ProduitsController@gettype');
    //Route::get('produits/creer', 'Admin\ProduitsController@create')->name('gproduits.create');
    //Route::get('produits/{slug}/modifier', 'Admin\ProduitsController@edit')->name('gproduits.edit');
    //Route::get('produits/{slug}/delete', 'Admin\ProduitsController@destroy')->name('gproduits.destroy');
    Route::post('produits/store', 'Admin\ProduitsController@store')->name('gproduits.store');
    Route::post('produits/update', 'Admin\ProduitsController@update')->name('gproduits.update');
    Route::post('produits/store/file', 'Admin\ProduitsController@import')->name('gproduits.store.import');

    Route::get('produits/types', 'Admin\ProduitsController@types')->name('gproduits.types');
    Route::post('produits/types/store', 'Admin\ProduitsController@storeType')->name('gproduits.types.store');
    Route::post('produits/types/update', 'Admin\ProduitsController@updateType')->name('gproduits.types.update');

    //GESTION DES CORPS D'ETATS
    Route::get('/corps-etats', 'Admin\CorpsEtatsController@index')->name('gcorpetat');
    Route::get('/corps-etats/editer/{id}', 'Admin\CorpsEtatsController@edit')->name('gcorpetat.edit');
    Route::get('/corps-etats/supprimer/{id}', 'Admin\CorpsEtatsController@destroy')->name('gcorpetat.destroy');
    Route::post('/corps-etats/store', 'Admin\CorpsEtatsController@store')->name('gcorpetat.store');
    Route::post('/corps-etats/update', 'Admin\CorpsEtatsController@update')->name('gcorpetat.update');

    //PROGRAMMES
    Route::get('/programmes', 'Admin\ProgrammesController@index')->name('gprogramme');
    Route::get('/programmes/supprimer/{id}', 'Admin\ProgrammesController@destroy')->name('gprogramme.destroy');
    Route::get('/programmes/setting/{id}', 'Admin\ProgrammesController@show')->name('gprogramme.show');
    Route::get('/get/{id}/programme', 'Admin\ProgrammesController@edit');
    Route::post('/programmes/store', 'Admin\ProgrammesController@store')->name('gprogramme.store');
    Route::post('/programmes/update', 'Admin\ProgrammesController@update')->name('gprogramme.update');
    //PROGRAMMES SETTING
        //USERS
    Route::post('/programmes/setting/users/store', 'Admin\ProgrammesSettingController@storeUser')->name('gprogramme.setting.store.users');
    Route::get('/programmes/setting/users/{user}/{pjt}/delete', 'Admin\ProgrammesSettingController@delUser')->name('gprogramme.setting.delete.users');
        //TYPES ACTIFS
    Route::post('/programmes/setting/actifs', 'Admin\ProgrammesSettingController@storeActifs')->name('gprogramme.setting.store.actifs');
    Route::post('/programmes/setting/actifs/update', 'Admin\ProgrammesSettingController@updateActifs')->name('gprogramme.setting.update.actifs');
    Route::get('/programmes/setting/actifs/{user}/delete', 'Admin\ProgrammesSettingController@delActifs')->name('gprogramme.setting.delete.actifs');

    Route::post('/programmes/setting/majcorpetat', 'Admin\ProgrammesSettingController@majCorpEtats')->name('gprogramme.setting.maj.corpetats');
    Route::get('/programmes_get_list_corpetat', 'Admin\ProgrammesSettingController@GetListeCorpEtat')->name('gprogramme.setting.get_list_corpetat');

        //LOTS
    Route::post('/programmes/setting/lots', 'Admin\ProgrammesSettingController@storeLots')->name('gprogramme.setting.store.lots');
        //DQE (DEVIS QUANTITATIFS ET ESTIMATIFS)
    Route::get('/programmes/setting/{pjt}/dqe/{actifs}', 'Admin\ProgrammesSettingController@createDqe')->name('gprogramme.setting.create.dqe');
    Route::post('/programmes/setting/dqe/store/ce', 'Admin\ProgrammesSettingController@storeDqeCe')->name('gprogramme.setting.store.dqe.ce');
    Route::post('/programmes/setting/dqe/update/ce', 'Admin\ProgrammesSettingController@updateDqeCe')->name('gprogramme.setting.update.dqe.ce');
    Route::post('/programmes/setting/dqe/store/matiere', 'Admin\ProgrammesSettingController@storeDqeMatiere')->name('gprogramme.setting.store.dqe.matiere');
    Route::post('/programmes/setting/dqe/update/matiere', 'Admin\ProgrammesSettingController@updateDqeMatiere')->name('gprogramme.setting.update.dqe.matiere');
    Route::get('/programmes/setting/dqe/{id}/delete/matiere', 'Admin\ProgrammesSettingController@deleteDqeMatiere')->name('gprogramme.setting.delete.dqe.matiere');
    Route::get('/get/sous/{id}/matiere', 'Admin\ProgrammesSettingController@getSmatiere');
            //LIBELLE ET PREFIX
    Route::get('/programmes/setting/{pjt}/dqe/{actifs}/libelle', 'Admin\ProgrammesSettingController@createDqeLibelle')->name('gprogramme.setting.create.dqe.libelle');
    Route::get('/get/corpsetat/souscorps/{id}/matiere', 'Admin\ProgrammesSettingController@getCorpsSmatiere')->name('get.corpsetat.scorpsetat.matiere');
    Route::post('/programmes/setting/dqe/store/libelle/matiere', 'Admin\ProgrammesSettingController@storeDqeLibelleMatiere')->name('gprogramme.setting.store.dqe.libelle.matiere');
    Route::post('/programmes/setting/dqe/store/libelle/ce', 'Admin\ProgrammesSettingController@storeDqeLibelleCe')->name('gprogramme.setting.store.dqe.libelle.ce');

    //PROGRAMME LIGNES BUDGETAIRES
    Route::post('/programmes/setting/lignes/assign', 'Admin\ProgrammesSettingController@storeLignes')->name('gprogramme.setting.lignes.store');
    Route::post('/programmes/setting/lignes/update', 'Admin\ProgrammesSettingController@updateLignes')->name('gprogramme.setting.lignes.update');
    Route::get('/programmes/setting/lignes/{id}/delet', 'Admin\ProgrammesSettingController@delLignes')->name('gprogramme.setting.lignes.delt');

    //IMPORTATION
    Route::get('/importations', 'Admin\ImportController@index')->name('importe');
    Route::post('/importations', 'Admin\ImportController@store')->name('importe.store');
    Route::post('/export/modele', 'Admin\ImportController@export')->name('importe.export');

});
/* ================== ROUTE CHEF PROJECT  ================== */


Route::prefix('chef-projet')->namespace('Chefprojet')->middleware(['auth','isChefprojet'])->group(function(){
    Route::get('mon-programme', 'HomeController@listpgram')->name('cpprogram');
    Route::get('mon-programme/{slug}/item', 'HomeController@itempgram')->name('cpprogram.item');
    Route::get('/', 'HomeController@index')->name('cphome');

    Route::get('/stock', 'HomeController@stock')->name('cphome.stock');
    Route::get('/stock/budget', 'HomeController@stockBudget')->name('cphome.stock.budget');

    Route::get('/bon-de-demande/create', 'HomeController@createDemande')->name('cphome.bc.create');
    Route::get('/bon-de-demande/create-step-2', 'HomeController@step2createDemande')->name('cphome.bc.step2create');
    Route::get('/bon-de-demande/create-step-3', 'HomeController@step3createDemande')->name('cphome.bc.step3create');

    Route::post('/bon-de-demande/store', 'HomeController@storeDemande')->name('cphome.bc.store');
    Route::get('/bon-de-demande/edit/{id}', 'HomeController@editDemande')->name('cphome.bc.edit');
    Route::post('/bon-de-demande/update/{id}', 'HomeController@updateDemande')->name('cphome.bc.update');
    Route::get('/bon-de-demande/delete/{id}/delete', 'HomeController@destroyDemande')->name('cphome.bc.destroy');

    Route::get('/bon-de-sortie/en_attente', 'HomeController@bcWaiting')->name('cphome.bc.waiting');
    Route::get('/bon-de-sortie/relete', 'HomeController@bcCancel')->name('cphome.bc.cancel');
    Route::get('/bon-de-sortie/valider', 'HomeController@bcValid')->name('cphome.bc.valider');
    Route::get('/bon-de-sortie/{slug}/detail', 'HomeController@showBc')->name('cphome.bc.show');
    Route::get('/bon-de-sortie/{slug}/pdf', 'HomeController@pdfBc')->name('cphome.bc.pdf');
    Route::get('/bon-de-sortie/{slug}/check', 'HomeController@validBc')->name('cphome.bc.valid');
    Route::post('/bon-de-sortie/reject/bon', 'HomeController@rejetBc')->name('cphome.bc.reject');

    Route::get('/bon-de-livraison', 'HomeController@bl')->name('cphome.bl');

    //SOUS TRAITANT
    Route::get('/sous-traitant', 'HomeController@straitantindex')->name('cphome.st.index');

    Route::get('/sous-traitant/{slug}/n-contract', 'HomeController@contratStraitant')->name('cphome.st.n-contrat');
    Route::get('/sous-traitant/contrat/store', 'HomeController@storeStraitantContrat')->name('cphome.st.contrat.store');
    Route::get('/sous-traitant/contrats/{slug}/end', 'HomeController@contratStraitantEnd')->name('cphome.st.n-contrat.end');
    Route::any('/sous-traitant/contrat/store/end', 'HomeController@storeStraitantContratEnd')->name('cphome.st.n-contrat.store');
    Route::any('/sous-traitant/contrat/store/3/end', 'HomeController@storeStraitantContratPhaseEnd')->name('cphome.st.n-contrat.store.end');
    //CONTRAT ST
    Route::get('/sous-traitant/contrats/{slug}/ce', 'HomeController@contratStraitantCe')->name('cphome.st.n-contrat.ce');


    Route::get('/sous-traitant/waiting', 'HomeController@straitant')->name('cphome.st');
    Route::get('/get/{id}/sous-traitant/lot', 'HomeController@getLotStraitant')->name('cphome.st.getlot');
    Route::get('/sous-traitant/{slug}/editer', 'HomeController@editStraitant')->name('cphome.st.edit');
    Route::get('/sous-traitant/{slug}/valid', 'HomeController@validStraitant')->name('cphome.st.valid');
    Route::post('/sous-traitant/rejet', 'HomeController@rejetStraitant')->name('cphome.st.reject');
    Route::get('/sous-traitant/{slug}/supprime', 'HomeController@deltraitant')->name('cphome.st.delete');
    Route::get('/sous-traitant/{slug}/contract', 'HomeController@contratStraitant')->name('cphome.st.contrat');
    Route::post('/sous-traitant/store', 'HomeController@storeStraitant')->name('cphome.st.store');
    Route::post('/sous-traitant/update', 'HomeController@updateStraitant')->name('cphome.st.update');
    Route::post('/sous-traitant/lot/dqe/store', 'HomeController@storeStraitantLotDqe')->name('cphome.st.lot.dqe.store');
    Route::get('/sous-traitant/lot/dqe/{item}/delete', 'HomeController@deleteStraitantLotDqe')->name('cphome.st.lot.dqe.delete');
    Route::get('/get/corps/{slug}/{lot}/lot', 'HomeController@getCorps')->name('cphome.get.corps');
    Route::get('/sous-traitant/{slug}/paye', 'HomeController@payeStraitant')->name('cphome.st.paye');
    Route::get('/sous-traitant/{slug}/{lot}/paye', 'HomeController@payeLotStraitant')->name('cphome.st.lot.paye');
    Route::post('/sous-traitant/paye/store', 'HomeController@storepayeStraitant')->name('cphome.st.paye.store');
    Route::get('/sous-traitant/paye/{slug}/delete/item', 'HomeController@delpayeStraitant')->name('cphome.st.paye.delete');
    Route::get('/sous-traitant/paye/store/{presta}/{contratid}', 'HomeController@storeSavePayeStraitant')->name('cphome.st.paye.save.store');

    //fournisseur
    Route::get('/fournisseurs', 'HomeController@fours')->name('cphome.fours');
    Route::post('/fournisseurs/store', 'HomeController@storeFours')->name('cphome.fours.store');
    Route::post('/fournisseurs/update', 'HomeController@updateFours')->name('cphome.fours.update');
    Route::get('/fournisseurs/{update}/delete', 'HomeController@delFours')->name('cphome.fours.delete');

    //TRANSACTION
    Route::get('/transactions', 'HomeController@transaction')->name('cphome.transac');
    Route::get('/transactions/waiting', 'HomeController@transactionWaiting')->name('cphome.transac.waiting');
    Route::get('/transactions/rejeter', 'HomeController@transactionRejeter')->name('cphome.transac.rejeter');
    Route::get('/transactions/waiting/{slug}/vaalid', 'HomeController@transactionWaitingValid')->name('cphome.transac.waiting.valid');
    Route::get('/transactions/waiting/{slug}/detail', 'HomeController@transactionWaitingShow')->name('cphome.transac.waiting.show');
    Route::post('/transactions/waiting/rejet', 'HomeController@rejetTransaction')->name('cphome.transac.reject');
    Route::get('/transactions/{slug}/detail', 'HomeController@transactionItem')->name('cphome.transac.item');
    Route::get('/transactions/{slug}/delte', 'HomeController@transactionDel')->name('cphome.transac.delete');

    //ETATS
    Route::get('/etats-lots', 'HomeController@etats')->name('cphome.etats');
    Route::get('/etats-lots/search', 'HomeController@etatsSearch')->name('cphome.etats.search');
    Route::get('/etats-lots/{slug}/detail', 'HomeController@etatsItem')->name('cphome.etats.item');


    //profile
    Route::get('/profile', 'HomeController@profil')->name('cphome.profil');
    Route::post('/profile/update', 'HomeController@profilUpdate')->name('cphome.profil.update');
    Route::post('/profile/update/pass', 'HomeController@profilUpdatePass')->name('cphome.profil.update.pass');
    Route::post('/profile/update/avatar', 'HomeController@profilUpdateAvatar')->name('cphome.profil.update.avatar');
});

/* ================== ROUTE SORO  ================== */
require __DIR__.'/neweb.php';
require __DIR__.'/web_soro.php';
require __DIR__.'/web_commande.php';

