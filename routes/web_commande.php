<?php

Route::get('/dmdspec/delte/{slug}', 'DemandeSpecialsController@delDmdSpecial')->name('delDmdSpecial');
Route::post('demande/spec/store', 'DemandeSpecialsController@store')->name('storeDmdSpecial');
Route::get('demande/spec/{slug}/pdf', 'DemandeSpecialsController@pdf')->name('pdfDmdSpecial');
Route::get('commandes/creer', 'CommandesController@create')->name('commande.create');
Route::post('commandes/store', 'CommandesController@store')->name('commandeStore');
Route::get('/bon-de-sortie/{slug}/pdf', 'HomeController@pdfBc')->name('pdfbsvalider');
Route::get('/get/{id}/sous-traitant/lot', 'HomeController@getLotStraitant')->name('getLotStraitant');
Route::get('/get/{qte}/{idpdt}/pdt/dispo', 'HomeController@getQteDispo')->name('getQteDispo');
Route::get('/get/{idlign}/listproduit', 'HomeController@getProduitByLigne')->name('getProduitByLigne');
Route::get('/get/bs/{sluf}/livre', 'HomeController@getBsLivre')->name('getBsLivre');
Route::get('/get/bc/{sluf}/view', 'HomeController@getBCView')->name('getBCView');
Route::post('dmdspec/paiement', 'DemandeSpecialsController@paiement')->name('sademande.paiement');
Route::get('transversale/{slug}/pdf', 'Serviceachat\TransversaleController@showPdf')->name('sa.transversale.pdf');
Route::get('bc/{slug}/pdf', 'Serviceachat\BonCommandeController@showPdf')->name('sabc.show.pdf');

Route::prefix('magasinier')->namespace('Magasinier')->middleware(['auth','isMagasinier'])->group(function(){
    //ETATS
    Route::get('/etats-lots', 'IndexController@etats')->name('magasinier.etats');
    Route::get('/etats-lots/search', 'IndexController@etatsSearch')->name('magasinier.etats.search');
    Route::get('/etats-lots/{slug}/detail', 'IndexController@etatsItem')->name('magasinier.etats.item');


    //COMMANDE
    Route::get('commandes/creer', 'CommandeController@create')->name('mcommande.create');
    Route::post('commandes/store', 'CommandeController@store')->name('mcommande.store');
    Route::get('commandes/detail/{slug}', 'CommandeController@show')->name('mcommande.show');
    Route::get('commandes/delte/{slug}', 'CommandeController@delete')->name('mcommande.delete');

    Route::get('commandes/en_attente', 'CommandeController@waiting')->name('mcommande.waiting');
    Route::get('commandes/rejete', 'CommandeController@rejet')->name('mcommande.rejet');
    Route::get('commandes/valider', 'CommandeController@valider')->name('mcommande.valider');

    //BON COMMANDE
    Route::get('bon-commandes', 'BonCommandeController@index')->name('mbc.index');
    Route::get('bon-commande/{slug}/detail', 'BonCommandeController@show')->name('mbc.show');

    //BON DE REPECTION
    Route::get('bon-receptions', 'BonReceptionController@index')->name('mbr.index');
    Route::get('bon-receptions/ajouter', 'BonReceptionController@create')->name('mbr.create');
    Route::get('bon-receptions/{slug}/detail', 'BonReceptionController@show')->name('mbr.show');
    Route::get('bon-receptions/{slug}/check', 'BonReceptionController@getbc')->name('mbr.get.bc');
    Route::get('bon-receptions/get/{qte}/{item}/{cmd}/dispo', 'BonReceptionController@getDispo')->name('mbr.get.dispo');
    Route::post('bon-receptions/store', 'BonReceptionController@store')->name('mbr.store');

    //DEMANDE SPECIALE
    Route::get('demande/spec/creer', 'DemandeController@createDmspec')->name('mdemandespec.create');
    Route::get('demande/spec/en_attente', 'DemandeController@waitingDmspec')->name('mdemandespec.waiting');
    Route::get('demande/spec/valider', 'DemandeController@validerDmspec')->name('mdemandespec.valider');
    Route::get('demande/spec/rejeter', 'DemandeController@rejeterDmspec')->name('mdemandespec.rejeter');


});

Route::prefix('chefchantier')->namespace('Chefchantier')->middleware(['auth', 'isChefchantier'])->group(function(){
    //ETATS
    Route::get('/etats-lots', 'HomeController@etats')->name('chefchantier.etats');
    Route::get('/etats-lots/search', 'HomeController@etatsSearch')->name('chefchantier.etats.search');
    Route::get('/etats-lots/{slug}/detail', 'HomeController@etatsItem')->name('chefchantier.etats.item');

    //COMMANDE
    Route::get('commandes/creer', 'CommandeController@create')->name('ccommande.create');
    Route::post('commandes/store', 'CommandeController@store')->name('ccommande.store');
    Route::get('commandes/detail/{slug}', 'CommandeController@show')->name('ccommande.show');
    Route::get('commandes/delte/{slug}', 'CommandeController@delete')->name('ccommande.delete');

    Route::get('commandes/en_attente', 'CommandeController@waiting')->name('ccommande.waiting');
    Route::get('commandes/rejete', 'CommandeController@rejet')->name('ccommande.rejet');
    Route::get('commandes/valider', 'CommandeController@valider')->name('ccommande.valider');

    //DEMANDE SPEC
    Route::get('demande/spec/creer', 'DemandesController@create')->name('cdemande.create');
    Route::post('demande/spec/store', 'DemandesController@store')->name('cdemande.store');
    Route::get('demande/spec/en_attente', 'DemandesController@waiting')->name('cdemande.waiting');
    Route::get('demande/spec/{id}/delete', 'DemandesController@delete')->name('cdemande.delete');
    Route::get('demande/spec/valider', 'DemandesController@valid')->name('cdemande.valid');
    Route::get('demande/spec/rejeter', 'DemandesController@rejet')->name('cdemande.rejet');
    Route::get('demande/spec/{slug}/pdf', 'DemandesController@pdf')->name('cdemande.pdf');


});

Route::prefix('chef-projet')->namespace('Chefprojet')->middleware(['auth','isChefprojet'])->group(function(){
    //COMMANDE
    Route::get('commandes/en_attente', 'CommandeController@waiting')->name('cpcommande.waiting');
    Route::get('commandes/rejetees', 'CommandeController@rejetees')->name('cpcommande.rejetees');
    Route::get('commandes/valider', 'CommandeController@valider')->name('cpcommande.valider');
    Route::get('commandes/detail/{slug}', 'CommandeController@show')->name('cpcommande.show');

    Route::get('commandes/valid/{slug}', 'CommandeController@valid')->name('cpcommande.valid');
    Route::post('commandes/rejeter', 'CommandeController@rejeter')->name('cpcommande.rejeter');

    //BON DE COMMANDE (BC)
    Route::get('bc/en_attente', 'BonCommandeController@waiting')->name('cpbc.waiting');
    Route::get('bc/valider', 'BonCommandeController@valider')->name('cpbc.valider');
    Route::get('bc/detail/{slug}', 'BonCommandeController@show')->name('cpbc.show');
    Route::get('bc/{slug}/pdf', 'BonCommandeController@showPdf')->name('cpbc.show.pdf');

    Route::get('bc/valid/{slug}', 'BonCommandeController@valid')->name('cpbc.valid');
    Route::post('bc/rejeter', 'BonCommandeController@rejeter')->name('cpbc.rejeter');

    //DEMANDE SPECIALE
    Route::get('demande/spec/creer', 'DemandesController@create')->name('cpdemande.create');
    // Route::post('demande/spec/store', 'DemandesController@store')->name('cpdemande.store');
    Route::get('demande/spec/en_attente', 'DemandesController@waiting')->name('cpdemande.waiting');
    Route::get('demande/spec/valider', 'DemandesController@valider')->name('cpdemande.valider');
    Route::post('demande/spec/rejeter', 'DemandesController@rejeter')->name('cpdemande.rejeter');
    Route::get('demande/spec/traiter', 'DemandesController@traiter')->name('cpdemande.traiter');
    // Route::get('demande/spec/{slug}/pdf', 'DemandesController@pdf')->name('cpdemande.pdf');
    Route::get('demande/spec/{slug}/valid', 'DemandesController@valid')->name('cpdemande.valid');
    Route::get('demande/spec/rejeter', 'DemandesController@rejet')->name('cpdemande.rejet');
    // Route::get('demande/spec/{id}/delete', 'DemandesController@delete')->name('sademande.delete');

});

Route::prefix('service-achat')->namespace('Serviceachat')->middleware(['auth','isServiceachat'])->group(function(){
    //HOME
    Route::get('mes-programmes', 'HomeController@home')->name('sa.home');
    Route::get('mon-programme/{slug}/', 'HomeController@itempgram')->name('sa.itempgram');
    Route::get('tableau-de-bord', 'HomeController@index')->name('sa.index');

    //MON PROFIL
    Route::get('mon-profil', 'HomeController@profil')->name('sa.profil');
    Route::post('/mon-profil/update', 'HomeController@profilUpdate')->name('sa.profil.update');
    Route::post('/mon-profil/update/pass', 'HomeController@profilUpdatePass')->name('sa.profil.update.pass');
    Route::post('/mon-profil/update/avatar', 'HomeController@profilUpdateAvatar')->name('sa.profil.update.avatar');

    //PRODUITS
    Route::get('produits', 'ProduitsController@index')->name('sa.gproduits');
    Route::get('produits/modele', 'ProduitsController@model')->name('sa.gproduits.model');
    Route::get('/get/produit/{id}/type', 'ProduitsController@gettype');
    Route::post('produits/import', 'ProduitsController@import')->name('sa.gproduits.import');
    Route::post('produits/store', 'ProduitsController@store')->name('sa.gproduits.store');
    Route::post('produits/update', 'ProduitsController@update')->name('sa.gproduits.update');

    Route::get('produits/types', 'ProduitsController@types')->name('sa.gproduits.types');
    Route::post('produits/types/store', 'ProduitsController@storeType')->name('sa.gproduits.types.store');
    Route::post('produits/types/update', 'ProduitsController@updateType')->name('sa.gproduits.types.update');

    //FOURNISSEURS
    Route::get('fournisseurs', 'FournisseursController@index')->name('sa.fours');
    Route::post('/fournisseurs/store', 'FournisseursController@store')->name('sa.fours.store');
    Route::post('/fournisseurs/update', 'FournisseursController@update')->name('sa.fours.update');
    Route::get('/fournisseurs/{update}/delete', 'FournisseursController@del')->name('sa.fours.delete');


    //COMMANDE
    Route::get('commandes/en_attente', 'CommandeController@waiting')->name('sacommande.waiting');
    Route::get('commandes/create', 'CommandeController@create')->name('sacommande.create');
    Route::get('commandes/detail/{slug}', 'CommandeController@show')->name('sacommande.show');
    Route::get('commandes/traiter/{slug}', 'CommandeController@traiter')->name('sacommande.traiter');
    Route::get('commandes/cloture', 'CommandeController@cloture')->name('sabc.cloture');
    Route::get('commandes/majcmande', 'CommandeController@majcmd')->name('sabc.majcmd');
    Route::post('commandes/majcmande', 'CommandeController@majlign')->name('sabc.majlign');

    //BON DE COMMANDE
    Route::get('bc/get/{slug}/demandes', 'BonCommandeController@getDemande')->name('sabc.get');
    Route::get('bc/get/{qte}/{item}/{cmd}/dispo', 'BonCommandeController@getDispo')->name('sabc.get.dispo');
    Route::get('bc/create', 'BonCommandeController@create')->name('sabc.create');
    Route::post('bc/store', 'BonCommandeController@store')->name('sabc.store');

    Route::get('bc/{slug}/detail', 'BonCommandeController@show')->name('sabc.show');
    Route::get('bc/{slug}/delete', 'BonCommandeController@delete')->name('sabc.delete');
    Route::get('bc/en_attente', 'BonCommandeController@waiting')->name('sabc.waiting');
    Route::get('bc/valider', 'BonCommandeController@valider')->name('sabc.valider');
    Route::get('bc/rejete', 'BonCommandeController@rejeter')->name('sabc.rejeter');

    //BON DE RECEPETION
    Route::get('bon-de-reception', 'BonReceptionController@index')->name('sabr.index');
    Route::get('bon-de-reception/conforme', 'BonReceptionController@indexconform')->name('sabr.indexconform');
    Route::get('bon-de-reception/{slug}/', 'BonReceptionController@show')->name('sabr.show');
    Route::get('bon-de-reception/{slug}/confirm', 'BonReceptionController@confirm')->name('sabr.confirm');

    //DEMANDE SPECIALE
    Route::get('demande/spec/create', 'DemandesController@create')->name('sademande.create');
    Route::get('demande/spec/en_attente', 'DemandesController@waiting')->name('sademande.waiting');
    Route::get('demande/spec/{id}/delete', 'DemandesController@delete')->name('sademande.delete');
    Route::get('demande/spec/valider', 'DemandesController@valid')->name('sademande.valid');
    Route::get('demande/spec/rejeter', 'DemandesController@rejet')->name('sademande.rejet');
    Route::get('demande/spec/traiter', 'DemandesController@traiter')->name('sademande.traiter');
    Route::post('demande/spec/rejeter', 'DemandesController@rejeter')->name('sademande.rejeter');

    Route::get('demande/spec/{slug}/generate', 'DemandesController@validBC')->name('sademande.valid.bc');
    Route::get('demande/spec/{slug}/pdf', 'DemandesController@pdf')->name('sademande.pdf');

    //ETATS
    Route::get('/etats-lots', 'HomeController@etats')->name('sa.etats');
    Route::get('/etats-lots/search', 'HomeController@etatsSearch')->name('sa.etats.search');
    Route::get('/etats-lots/{slug}/detail', 'HomeController@etatsItem')->name('sa.etats.item');

    Route::get('/etats-bons-commandes', 'EtatController@etatsBc')->name('sa.etats.bc');
    Route::get('/etats-produits', 'EtatController@etatsPdt')->name('sa.etats.pdt');

    //STOCKS
    Route::get('stock-programme', 'StocksController@index')->name('sa.stocks');
    //ROTATION
    Route::get('rotations', 'StocksController@rotations')->name('sa.stocks.rotations');
    Route::any('rotations/search', 'StocksController@rotationsSearch')->name('sa.stocks.rotations.store');

    //TRANSVERSALE
    Route::get('transversale/{id}/reattribu', 'TransversaleController@make')->name('sa.transversale');
    Route::get('transversale/reattribu', 'TransversaleController@show')->name('sa.transversale.showbc');
    Route::post('transversale/reattribu/store', 'TransversaleController@store')->name('sa.transversale.store');
});

Route::prefix('daf')->namespace('Daf')->middleware(['auth','isDaf'])->group(function(){
    //HOME
    Route::get('mes-programmes', 'HomeController@home')->name('daf.home');
    Route::get('mon-programme/{slug}/', 'HomeController@itempgram')->name('daf.itempgram');
    Route::get('tableau-de-bord', 'HomeController@index')->name('daf.index');

    //MON PROFIL
    Route::get('mon-profil', 'HomeController@profil')->name('daf.profil');
    Route::post('/mon-profil/update', 'HomeController@profilUpdate')->name('daf.profil.update');
    Route::post('/mon-profil/update/pass', 'HomeController@profilUpdatePass')->name('daf.profil.update.pass');
    Route::post('/mon-profil/update/avatar', 'HomeController@profilUpdateAvatar')->name('daf.profil.update.avatar');

    //ETATS
    Route::get('/etats-lots', 'HomeController@etats')->name('daf.etats');
    Route::get('/etats-lots/search', 'HomeController@etatsSearch')->name('daf.etats.search');
    Route::get('/etats-lots/{slug}/detail', 'HomeController@etatsItem')->name('daf.etats.item');

    //PRODUITS
    Route::get('produits', 'ProduitsController@index')->name('daf.gproduits');

    //BC
    Route::get('bc/en_attente', 'BonCommandeController@waiting')->name('daf.waiting');
    Route::get('bc/valider', 'BonCommandeController@valider')->name('daf.valider');
    Route::get('bc/{slug}/detail', 'BonCommandeController@show')->name('daf.show');
    Route::get('bc/{slug}/pdf', 'BonCommandeController@showPdf')->name('daf.show.pdf');

    //STOCKS
    Route::get('stock-programme', 'StocksController@index')->name('daf.stocks');

    //ROTATION
    Route::get('rotations', 'StocksController@rotations')->name('daf.stocks.rotations');
    Route::any('rotations/search', 'StocksController@rotationsSearch')->name('daf.stocks.rotations.store');

    //ACTION FOR CONTROLEUR DE GESTION
    //ACTION FOR CONTROLEUR DE GESTION
    //ACTION FOR CONTROLEUR DE GESTION
    //ACTION FOR CONTROLEUR DE GESTION

    //RETENUE SUR ATTACHEMENT
    Route::get('etblie-paiement', 'PaiementController@etbpaie')->name('daf.etbpaie');
    Route::get('etblie-paiement/get/{id}', 'PaiementController@getTrans')->name('daf.etbpaie.get');
    Route::post('etblie-paiement', 'PaiementController@etbpaieStore')->name('daf.etbpaie.store');

    //ATTACHEMENT
    Route::get('rattachement', 'PaiementController@index')->name('daf.paie');
    Route::get('rattachement/{slug}/detail', 'PaiementController@show')->name('daf.paie.show');
    Route::get('rattachement/show_paiement/{slug}', 'PaiementController@validPaie')->name('daf.paie.validation');
    Route::get('paiements/enattente', 'PaiementController@waiting')->name('daf.paie.waiting');
    Route::get('paiements/valider', 'PaiementController@valid')->name('daf.paie.valid');
    Route::post('paiements/valider', 'PaiementController@validspaie')->name('daf.paie.validpaie');

    //STATISTIQUES CONTRATS
    Route::get('statistiques/contrats', 'PaiementController@statctr')->name('daf.stat.contrats');
    Route::get('statistiques/{slug}/contrats', 'PaiementController@statctrshow')->name('daf.stat.contrats.show');

    //AVENANTS SOUSTRAITANT
    Route::get('avenants/ajouter-un-avenant', 'AvenantsController@create')->name('daf.avenants.create');
    Route::get('avenants/{type}', 'AvenantsController@index')->name('daf.avenants');
    Route::get('avenants/{type}/detail', 'AvenantsController@show')->name('daf.avenants.show');
    Route::post('avenants/store', 'AvenantsController@store')->name('daf.avenants.store');
    Route::post('avenants/valid', 'AvenantsController@valid')->name('daf.avenants.valid');
    Route::post('avenants/rejet', 'AvenantsController@rejeter')->name('daf.avenants.rejet');

    //DEMANDE SPECIALE
    Route::get('facturation/dmd-spec', 'FacturactionController@dmdSpec')->name('daf.facturation.dmdSpec');

    //FOURNISSERS
    Route::get('fournisseur', 'HomeController@fournisseur')->name('daf.fournisseur');

    //FACTURATION & PAIEMENT
    Route::get('facturation/soustraitant', 'FacturactionController@soustraitant')->name('daf.facturation.soustraitant');
    Route::get('facturation/fournisseur', 'FacturactionController@fournisseur')->name('daf.facturation.fournisseur');
    Route::get('facturation/fournisseur/create', 'FacturactionController@createFactFourns')->name('daf.facturation.createFactFourns');
    Route::post('facturation/fournisseur/store', 'FacturactionController@storeFactFourns')->name('daf.facturation.storeFactFourns');

    //EFFECTUER UN PAIEMENT
    Route::get('facturation/effectuer-un-paiement', 'FacturactionController@waiting')->name('daf.facturation.paie.waiting');
    Route::get('facturation/get/effectuer-un-paiement', 'FacturactionController@getAppel')->name('daf.facturation.create.paie');
    Route::post('facturation/paie/fournisseur', 'FacturactionController@storepaiefourn')->name('daf.facturation.storepaiefourn');

    //HISTOTIQUE DES PAIEMENTS
    Route::get('facturation/{type}/historique-des-paiements', 'FacturactionController@historique')->name('daf.facturation.historique');

    //AFFECTATIONS CG PRODUITS
    Route::get('affectations', 'AffectationController@index')->name('daf.affectations');
    Route::post('affectations/store', 'AffectationController@store')->name('daf.affectations.store');
    Route::post('affectations/update', 'AffectationController@update')->name('daf.affectations.update');
    Route::get('affectations/modele', 'AffectationController@model')->name('daf.affectations.model');
    Route::post('affectations/import/modele', 'AffectationController@import')->name('daf.affectations.import');

});

//FOURNISSERS EVERYONE
Route::get('fournisseur/{ids}/stats', 'Serviceachat\FournisseursController@show')->name('stats.fournisseur');

Route::get('/etats/facturations', 'Serviceachat\EtatController@etatsFactus')->name('etats.facturations');
Route::post('/etats/facturations/store', 'Serviceachat\EtatController@etatsFactuStore')->name('etats.facturations.store');

Route::post('/etats-bons-commandes/store', 'Serviceachat\EtatController@etatsBcStore')->name('etats.bc.store');
Route::post('/etats-produits/store', 'Serviceachat\EtatController@etatsPdtStore')->name('etats.pdt.store');
Route::get('/etats-produits/daf', 'Serviceachat\EtatController@etatsPdtDaf')->name('daf.etats.pdt');
Route::get('/etats-bons-commandes/daf', 'Serviceachat\EtatController@etatsBcDaf')->name('daf.etats.bc');
Route::get('commandes/etats', 'Serviceachat\EtatController@etatCmdCp')->name('cp.etats.cmd');
Route::post('commandes/etats/store', 'Serviceachat\EtatController@etatCmdCpStore')->name('etats.cmd.store');

Route::get('/magasinier/etats-bon-sortie/gs', 'Serviceachat\EtatController@etatsBsGs')->name('magasinier.etats.bs');
Route::post('/etats-bon-sortie/store', 'Serviceachat\EtatController@etatsBsGsStore')->name('etats.bs.store');


