<?php
//Auth::routes();
Route::get('/quicklogout', function () {
    Auth::logout();
    return redirect( url('/login') );
})->name('quicklogout');

Route::get('/forbiden', function(){
    // return view('forbiden')->with('error', "Interdiction d'accès, vous ne disposez pas des droits d'accès à cette page !");
    return redirect()->route('home')->with('error', "Interdiction d'accès, vous ne disposez pas des droits d'accès à cette page !");
})->name('forbiden');

/**
 * Magasinier routes group
 **/
Route::name('magasinier.')->prefix('magasinier')->namespace('Magasinier')->middleware(['auth', 'isMagasinier'])->group(function(){
    Route::get('/', 'IndexController@index')->name('home');
    Route::get('mon-programme', 'IndexController@listpgram')->name('cpprogram');
    Route::get('mon-programme/{slug}/item', 'IndexController@itempgram')->name('cpprogram.item');
    //Profil user
    /* Route::get('/showprofil', 'ProfilController@showprofil')->name('showprofil');
    Route::get('/editprofil', 'ProfilController@editprofil')->name('editprofil');
    Route::put('/updateprofil/{id}', 'ProfilController@updateprofil')->name('updateprofil');
     *///Gestion stock
    Route::get('/stock/budget', 'StockController@stockBudget')->name('stock.budget');
    Route::get('/stock/referencesme', 'StockController@referenceStock')->name('stock.reference');
    Route::get('/stock/referencesme/{slug}/detail', 'StockController@referenceStockItem')->name('stock.reference.item');

    Route::resource('stock', StockController::class);
    Route::get('/stock/appro/{id}', 'StockController@appro')->name('stock.appro');
    Route::get('/stock/reduire/{id}', 'StockController@reduire')->name('stock.reduire');
    Route::post('/stock/filtrer/', 'StockController@filtrer')->name('stock.filtrer');

    //Retour en stock
    Route::get('/retour-en-stock', 'BackstockController@index')->name('stock.back.index');
    Route::get('/retour-en-stock/{slug}/detail', 'BackstockController@show')->name('stock.back.show');
    Route::get('/retour-en-stock/create', 'BackstockController@create')->name('stock.back.create');
    Route::get('/retour-en-stock/create/step2', 'BackstockController@create_step2')->name('stock.back.create_step2');
    Route::post('/retour-en-stock/store', 'BackstockController@store')->name('stock.back.store');

    //Bon de demande

    Route::get('/bon-de-demande/details/{id}', 'DemandeController@detailsDemande')->name('demande.details');
    //Route::post('/bon-de-demande/store', 'DemandeController@storeDemande')->name('demande.store');
    //Route::post('/bon-de-demande/update', 'DemandeController@updateDemande')->name('demande.update');
    Route::get('/bon-de-demande/delete/{id}/del', 'DemandeController@destroyDemande')->name('livraison.destroy');
    Route::get('/bon-de-demande', 'DemandeController@indexDemande')->name('demande.index');
    Route::get('/bon-de-demande/delete/{id}', 'DemandeController@destroyDemande')->name('demande.destroy');

    //demande.delivering
    Route::get('/bon-de-commande/attente', 'DemandeController@waitingDemande')->name('demande.waiting');
    Route::get('/bon-de-commande/rejet', 'DemandeController@canceledDemande')->name('demande.canceled');
    Route::get('/bon-de-commande/{slug}/detail', 'DemandeController@showBc')->name('cphome.bc.show');
    //
    Route::get('/bon-de-demande/valider', 'DemandeController@validatedDemande')->name('demande.validated');
    Route::get('/bon-de-demande/livrer', 'DemandeController@delivredDemande')->name('demande.delivred');

    //demande rejeter
    Route::post('/bon-de-commande/reject/bon', 'DemandeController@rejetBc')->name('demande.reject');


    Route::get('/bon-de-demande/delivering/{id}', 'DemandeController@deliveringDemande')->name('demande.delivering');
    Route::get('/bon-de-demande/canceling/{id}', 'DemandeController@cancelingDemande')->name('demande.canceling');
    //Bon de livraison
    Route::get('/bon-de-livraison/', 'DemandeController@indexLivraison')->name('livraison.index');
    Route::get('/bon-de-livraison/details/{id}', 'DemandeController@detailsLivraison')->name('livraison.details');
    Route::get('/bon-de-livraison/export/{id}', 'DemandeController@exportLivraison')->name('livraison.export');

    //profile
    Route::get('/profile', 'IndexController@profil')->name('cphome.profil');
    Route::post('/profile/update', 'IndexController@profilUpdate')->name('cphome.profil.update');
    Route::post('/profile/update/pass', 'IndexController@profilUpdatePass')->name('cphome.profil.update.pass');
    Route::post('/profile/update/avatar', 'IndexController@profilUpdateAvatar')->name('cphome.profil.update.avatar');

    //fournisseur
    Route::get('/fournisseurs', 'IndexController@fours')->name('fours');
    Route::post('/fournisseurs/store', 'IndexController@storeFours')->name('fours.store');
    Route::post('/fournisseurs/update', 'IndexController@updateFours')->name('fours.update');
    Route::get('/fournisseurs/{update}/delete', 'IndexController@delFours')->name('fours.delete');

});


Route::name('chefchantier.')->prefix('chefchantier')->namespace('Chefchantier')->middleware(['auth', 'isChefchantier'])->group(function(){
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('mon-programme', 'HomeController@listpgram')->name('cpprogram');
    Route::get('mon-programme/{slug}/item', 'HomeController@itempgram')->name('cpprogram.item');
    //Route::get('/', 'HomeController@index')->name('cphome');

    Route::get('/stock', 'HomeController@stockList')->name('stock');
    Route::get('/stock/budget', 'HomeController@stockBudget')->name('stock.budget');
    Route::get('/stock/filtre', 'HomeController@stockFilter')->name('stock.filtre');

    Route::get('/bon-de-demande/details/{id}', 'HomeController@detailsDemande')->name('demande.details');
    Route::get('/bon-de-demande/create', 'HomeController@createDemande')->name('demande.create');
    Route::post('/bon-de-demande/store', 'HomeController@storeDemande')->name('demande.store');
    Route::get('/bon-de-demande/edit/{id}', 'HomeController@editDemande')->name('demande.edit');
    Route::post('/bon-de-demande/update/{id}', 'HomeController@updateDemande')->name('demande.update');
    Route::get('/bon-de-demande/delete/{id}/delete', 'HomeController@destroyDemande')->name('demande.destroy');
    Route::get('/n-demande-step-2', 'HomeController@step2createDemande')->name('step2createDemande');
    Route::get('/n-demande-step-3', 'HomeController@step3createDemande')->name('step3createDemande');
    Route::get('/list-matiere-of-soustraitant', 'HomeController@list_matiere_of_soustraitant')->name('list_matiere_of_soustraitant');
    Route::get('/lots-of-soustraitant', 'HomeController@select_lots_soustraitant')->name('select_lots_soustraitant');
    //Route::get('/bon-de-demande/', 'HomeController@indexDemande')->name('demande');
    //
    Route::get('/bon-de-demande/delete/{id}', 'DemandeController@destroyDemande')->name('livraison.destroy');

    Route::get('/bon-de-commande/attente', 'HomeController@waitingDemande')->name('demande.waiting');
    Route::get('/bon-de-commande/rejet', 'HomeController@canceledDemande')->name('demande.canceled');
    Route::get('/bon-de-commande/{slug}/detail', 'HomeController@showBc')->name('cphome.bc.show');
    //
    Route::get('/bon-de-demande/valider', 'HomeController@validatedDemande')->name('demande.validated');
    Route::get('/bon-de-demande/livrer', 'HomeController@delivredDemande')->name('demande.delivred');
    Route::get('/bon-de-demande/{slug}/detail', 'HomeController@showBc')->name('demande.show');


    Route::get('/bon-de-livraison/', 'HomeController@indexLivraison')->name('livraison.index');
    Route::get('/bon-de-livraison/details/{id}', 'HomeController@detailsLivraison')->name('livraison.details');
    Route::get('/bon-de-livraison/export/{id}', 'HomeController@exportLivraison')->name('livraison.export');


    //profile
    Route::get('/profile', 'HomeController@profil')->name('cphome.profil');
    Route::post('/profile/update', 'HomeController@profilUpdate')->name('cphome.profil.update');
    Route::post('/profile/update/pass', 'HomeController@profilUpdatePass')->name('cphome.profil.update.pass');
    Route::post('/profile/update/avatar', 'HomeController@profilUpdateAvatar')->name('cphome.profil.update.avatar');


    //SOUS TRAITANT
    Route::get('/sous-traitant', 'HomeController@straitant')->name('st');
    Route::get('/get/{id}/sous-traitant/lot', 'HomeController@getLotStraitant')->name('st.getlot');
    Route::get('/sous-traitant/{slug}/editer', 'HomeController@editStraitant')->name('st.edit');
    Route::get('/sous-traitant/{slug}/supprime', 'HomeController@deltraitant')->name('st.delete');
    Route::get('/sous-traitant/{slug}/contract', 'HomeController@contratStraitant')->name('st.contrat');
    Route::post('/sous-traitant/store', 'HomeController@storeStraitant')->name('st.store');
    Route::post('/sous-traitant/update', 'HomeController@updateStraitant')->name('st.update');
    Route::post('/sous-traitant/lot/dqe/store', 'HomeController@storeStraitantLotDqe')->name('st.lot.dqe.store');
    Route::get('/sous-traitant/lot/dqe/{item}/delete', 'HomeController@deleteStraitantLotDqe')->name('st.lot.dqe.delete');
    Route::get('/get/corps/{slug}/{lot}/lot', 'HomeController@getCorps')->name('get.corps');
    Route::get('/sous-traitant/{slug}/paye', 'HomeController@payeStraitant')->name('st.paye');
    Route::get('/sous-traitant/{slug}/{lot}/paye', 'HomeController@payeLotStraitant')->name('st.lot.paye');
    Route::post('/sous-traitant/paye/store', 'HomeController@storepayeStraitant')->name('st.paye.store');
    Route::get('/sous-traitant/paye/{slug}/delete/item', 'HomeController@delpayeStraitant')->name('st.paye.delete');
    Route::get('/sous-traitant/paye/{presta}/{idcontrat}/store', 'HomeController@storeSavePayeStraitant')->name('st.paye.save.store');
    Route::post('/sous-traitant/paye_store', 'HomeController@storeSavePayeStraitant2')->name('st.paye.save.store2');

    //MAIN D'OEUVRE
    Route::get('/sous-traitant/maindoeuvre/', 'HomeController@listctst')->name('st.mainoeuvre');

    //TRANSACTIONS
    Route::get('/transactions/etat', 'HomeController@transactions')->name('transactions');
    Route::get('/transactions/valid', 'HomeController@transactionsValid')->name('transactions.valid');
    Route::get('/transactions/reject', 'HomeController@transactionsReject')->name('transactions.reject');
    Route::get('/transactions/{id}/show', 'HomeController@transactionShow')->name('transactions.show');
    Route::get('/transactions/valid/{slug}/show', 'HomeController@transactionItem')->name('transactions.valid.show');
    Route::get('/transactions/{id}/delete', 'HomeController@transactionsDel')->name('transactions.delete');

    Route::post('/profile/update', 'HomeController@profilUpdate')->name('profil.update');
    Route::post('/profile/update/pass', 'HomeController@profilUpdatePass')->name('profil.update.pass');
    Route::post('/profile/update/avatar', 'HomeController@profilUpdateAvatar')->name('profil.update.avatar');

});

Route::middleware(['auth', 'isChefchantier'])->group(function(){
    Route::post('/bon-de-demande/validating/{id}', 'BonController@validating')->name('demande.validating');
});
Route::middleware(['auth', 'isMagasinier'])->group(function(){
    Route::post('/bon-de-demande/delivering/{id}', 'BonController@delivering')->name('demande.delivering');
});
Route::middleware(['auth', 'isChefchantier', 'isMagasinier'])->group(function(){
    Route::post('/bon-de-demande/canceling/{id}', 'BonController@canceling')->name('demande.canceling');
});


